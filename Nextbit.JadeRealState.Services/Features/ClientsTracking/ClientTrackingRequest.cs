using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.ClientsTracking
{
    public class ClientTrackingRequest : RequestBase
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime? FechaCita { get; set; }
        public int ClientId { get; set; }
        public string Comentarios { get; set; }
        public int TipoCita { get; set; }
        public int MeetingCategoryId { get; set; }
        public string CreatedBy { get; set; }
    }
    public class ClientTrackingPagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int ClientId { get; set; }
        public string Value { get; set; }
    }
}