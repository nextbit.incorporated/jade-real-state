using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.MeetingCategories;
using Nextbit.JadeRealState.Services.Features.SupplierServices;

namespace Nextbit.JadeRealState.Services.Features.ClientsTracking
{
    public class ClientTrackingAppService : IClientTrackingAppService
    {
        private readonly RealStateContext _context;
        private readonly IClientTrackingDomainService _clientTrackingDomainService;

        public ClientTrackingAppService(
            RealStateContext context, IClientTrackingDomainService clientTrackingDomainService)
        {
            _clientTrackingDomainService = clientTrackingDomainService ?? throw new ArgumentNullException(nameof(clientTrackingDomainService));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public ClientTrackingDTO Create(ClientTrackingRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));

            var clientTracking = _clientTrackingDomainService.Create(request);

            _context.ClientsTracking.Add(clientTracking);

            _context.SaveChanges();

            return new ClientTrackingDTO
            {
                Id = clientTracking.Id,
                FechaCita = clientTracking.FechaCita,
                Description = clientTracking.Description,
                Comentarios = clientTracking.Comentarios,
                ClientId = clientTracking.ClientId,
                Client = clientTracking.Client,
                CreatedBy = clientTracking.CreatedBy,
                CreationDate = clientTracking.CreationDate
            };
        }

        public IEnumerable<ClientTrackingDTO> Get(int clientId)
        {
            var clientTrackings = _context.ClientsTracking.Where(s => s.ClientId == clientId).ToList();
            var listaServicios = clientTrackings.Select(s => s.TipoServicioId).Distinct().ToList();
            var servicios = _context.SupplierService.Where(s => listaServicios.Contains(s.Id) && s.Status == "ACTIVO").ToList();
            var listaTiposCitas = clientTrackings.Select(s => s.MeetingCategoryId).Distinct().ToList();
            var tiposCitas = _context.MeetingCategories.Where(s => listaTiposCitas.Contains(s.Id)).ToList();

            return clientTrackings.Select(s => new ClientTrackingDTO
            {
                Id = s.Id,
                FechaCita = s.FechaCita,
                Description = s.Description,
                Comentarios = s.Comentarios,
                Orden = s.Orden,
                ProcesoId = s.ProcesoId,
                Estado = s.Estado ?? "N/A",
                ClientId = s.ClientId,
                Client = s.Client,
                Servicio = ObtenerNombreServicio(s.TipoServicioId, servicios),
                MeetingCategoryId = ObtenerCodigoTipoCita(s.MeetingCategoryId, tiposCitas),
                MeetingCategoryName = ObtenerNombreTipoCita(s.MeetingCategoryId, tiposCitas),
                CitaPendiente = s.FechaCita >= DateTime.Now,
                CreatedBy = s.CreatedBy,
                CreationDate = s.CreationDate
            }).OrderBy(s => s.Orden).ToList();
        }
        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var clientTracking = _context.ClientsTracking.FirstOrDefault(s => s.Id == id);
            if (clientTracking == null) throw new Exception("Numero de seguimiento not existe");
            _context.ClientsTracking.Remove(clientTracking);
            _context.SaveChanges();

            return string.Empty;
        }

        public string CompletarCita(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var clientTracking = _context.ClientsTracking.FirstOrDefault(s => s.Id == id);
            if (clientTracking == null) throw new Exception("Numero de seguimiento not existe");
            var client = clientTracking.UpdateEstado(DateTime.Now, "Completado", "Service");

            _context.ClientsTracking.Update(client);
            _context.SaveChanges();

            return string.Empty;
        }
        public ClientTrackingPagedDTO GetPaged(ClientTrackingPagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            var count = Convert.ToDecimal(_context.ClientsTracking.Count());
            var totalPage = Math.Ceiling(count / request.PageSize);
            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;
            if (string.IsNullOrEmpty(request.Value))
            {
                var lista = _context.ClientsTracking.OrderByDescending(s => s.TransactionDate).Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ClientTrackingPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    ClientTrackings = lista.Select(s => new ClientTrackingDTO
                    {
                        Id = s.Id,
                        Description = s.Description,
                        Comentarios = s.Comentarios,
                        FechaCita = s.FechaCita,
                        ClientId = s.ClientId,
                        Client = s.Client,
                        CreatedBy = s.CreatedBy,
                        CreationDate = s.CreationDate
                    }).ToList()
                };
            }
            else
            {
                var lista = _context.ClientsTracking.Where(s => s.ClientId == request.ClientId)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ClientTrackingPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    ClientTrackings = lista.Select(s => new ClientTrackingDTO
                    {
                        Id = s.Id,
                        Description = s.Description,
                        Comentarios = s.Comentarios,
                        FechaCita = s.FechaCita,
                        ClientId = s.ClientId,
                        Client = s.Client,
                        CreatedBy = s.CreatedBy,
                        CreationDate = s.CreationDate
                    }).ToList()
                };
            }
        }

        public ClientTrackingDTO Update(ClientTrackingRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var clientTracking = _context.ClientsTracking.FirstOrDefault(s => s.Id == request.Id);
            if (clientTracking == null) return new ClientTrackingDTO { ValidationErrorMessage = "Proyecto inexistente" };

            _clientTrackingDomainService.Update(request, clientTracking);

            _context.ClientsTracking.Update(clientTracking);

            _context.SaveChanges();

            return new ClientTrackingDTO
            {
                Id = clientTracking.Id,
                FechaCita = clientTracking.FechaCita,
                Description = clientTracking.Description,
                Comentarios = clientTracking.Comentarios,
                ClientId = clientTracking.ClientId,
                Client = clientTracking.Client,
                CreatedBy = clientTracking.CreatedBy,
                CreationDate = clientTracking.CreationDate
            };
        }
        public void Dispose()
        {
            // Release managed resources.
            _context?.Dispose();
        }

        public IEnumerable<ClientTrackingDTO> CrearNuevoSeguimiento(NuevoSeguimientoRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.ClienteId == 0) throw new ArgumentException(nameof(request.ClienteId));
            if (request.SupplierServiceId == 0) throw new ArgumentException(nameof(request.SupplierServiceId));

            var trackingsPendientes = _context.ClientsTracking.Where(s => s.ClientId == request.ClienteId).ToList();
            if (trackingsPendientes.Any())
            {
                foreach (var item in trackingsPendientes)
                {
                    var updateRegistro = item.UpdateEstado(DateTime.Now, "Completado", "Service");
                    _context.ClientsTracking.Update(updateRegistro);
                }
            }

            var tipoServicio = _context.SupplierService
                                               .Include(p => p.ServiceProcesses).FirstOrDefault(s => s.Id == request.SupplierServiceId);
            if (tipoServicio != null)
            {
                var tracking = new List<ClientTracking>();
                if (tipoServicio.ServiceProcesses.Any())
                {
                    var fechaCita = DateTime.Now;
                    foreach (var item in tipoServicio.ServiceProcesses.OrderBy(s => s.Order))
                    {
                        fechaCita = fechaCita.AddDays(10);
                        var newTracking = new ClientTracking.Builder()
                            .WithClientId(request.ClienteId)
                            .WithOrden(item.Order)
                            .WithDescription(item.Name)
                            .WithProceso(item.Id)
                            .WithTipoServicio(tipoServicio.Id)
                            .WithEstado("Pendiente")
                            .WithDate(fechaCita)
                            .WithAuditFields(request.User)
                            .WithCreatedBy(request.CreatedBy)
                            .Build();

                        _context.ClientsTracking.Add(newTracking);
                    }
                    _context.SaveChanges();
                }
            }

            IEnumerable<ClientTracking> clientTrackings = _context.ClientsTracking.Where(s => s.ClientId == request.ClienteId);

            var respuesta = clientTrackings.Select(s => new ClientTrackingDTO
            {
                Id = s.Id,
                FechaCita = s.FechaCita,
                Description = s.Description,
                Comentarios = s.Comentarios,
                Orden = s.Orden,
                ProcesoId = s.ProcesoId,
                Estado = s.Estado ?? "N/A",
                ClientId = s.ClientId,
                Client = s.Client,
                Servicio = tipoServicio == null ? string.Empty : tipoServicio.Name ?? string.Empty,
                CitaPendiente = s.FechaCita >= DateTime.Now,
                CreatedBy = s.CreatedBy,
                CreationDate = s.CreationDate
            }).OrderBy(s => s.FechaCita);

            return respuesta.ToList();
        }

        private string ObtenerNombreTipoCita(int meetingCategoryId, List<MeetingCategory> tipoCitas)
        {

            if (meetingCategoryId == 0) return string.Empty;
            var servicio = tipoCitas.FirstOrDefault(s => s.Id == meetingCategoryId);

            return servicio == null ? string.Empty : servicio.Name ?? string.Empty;
        }
        private int ObtenerCodigoTipoCita(int tipoServicioId, List<MeetingCategory> tipoCitas)
        {

            if (tipoServicioId == 0) return 0;
            var servicio = tipoCitas.FirstOrDefault(s => s.Id == tipoServicioId);

            return servicio == null ? 0 : servicio.Id;
        }

        private string ObtenerNombreServicio(int tipoServicioId, List<SupplierService> servicios)
        {
            if (tipoServicioId == 0) return string.Empty;
            var servicio = servicios.FirstOrDefault(s => s.Id == tipoServicioId);

            return servicio == null ? string.Empty : servicio.Name ?? string.Empty;
        }

        public IEnumerable<ClientTrackingDTO> GetNextTrackingDates()
        {
            IEnumerable<ClientTracking> trakings = _context.ClientsTracking
                        .Where(s => s.Estado == "Pendiente")
                        .OrderByDescending(s => s.FechaCita)
                        .Take(5).ToList();

            var tipoServiciosId = trakings.Select(s => s.TipoServicioId).ToList();
            var clientesId = trakings.Select(s => s.ClientId).ToList();

            IEnumerable<SupplierService> servicios = _context.SupplierService.Where(s => tipoServiciosId.Contains(s.Id)).ToList();
            IEnumerable<Client> clientes = _context.Clients.Where(s => clientesId.Contains(s.Id)).ToList();

            var respuesta = trakings.Select(s => new ClientTrackingDTO
            {
                Id = s.Id,
                FechaCita = s.FechaCita,
                Description = s.Description,
                Comentarios = s.Comentarios,
                Orden = s.Orden,
                ProcesoId = s.ProcesoId,
                Estado = s.Estado ?? "N/A",
                ClientId = s.ClientId,
                ClienteNombre = ObtenerNombreCliente(s.ClientId, clientes),
                TipoServicioId = s.TipoServicioId,
                Servicio = ObtenerNombreServicio(s.TipoServicioId, servicios.ToList()),
                CitaPendiente = s.FechaCita >= DateTime.Now ? true : false,
                CreatedBy = s.CreatedBy,
                CreationDate = s.CreationDate
            }).OrderBy(s => s.FechaCita);

            return respuesta.ToList();
        }

        private string ObtenerNombreCliente(int clientId, IEnumerable<Client> clientes)
        {
            var cliente = clientes.FirstOrDefault(s => s.Id == clientId);
            if (cliente == null) return string.Empty;

            return cliente.FirstName + " " + cliente.MiddleName ?? ""
                   + " " + cliente.FirstSurname ?? "" + " " + cliente.SecondSurname ?? "";
        }


    }
}