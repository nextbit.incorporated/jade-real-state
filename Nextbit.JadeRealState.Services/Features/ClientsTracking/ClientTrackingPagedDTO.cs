using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.ClientsTracking
{
    public class ClientTrackingPagedDTO
    {
            public int PageSize { get; set; }
            public int PageIndex { get; set; }
            public int PageCount { get; set; }
            public List<ClientTrackingDTO> ClientTrackings { get; set; }  
    }
}