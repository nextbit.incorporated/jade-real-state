using System;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.ClientsTracking
{
    public class ClientTrackingDomainService : IClientTrackingDomainService
    {
        public ClientTracking Create(ClientTrackingRequest request)
        {

            ClientTracking clienTracking = new ClientTracking.Builder()
            .WithDate(request.FechaCita)
            .WithDescription(request.Description ?? string.Empty)
            .WithComentarios(request.Comentarios ?? string.Empty)
            .WithClientId(request.ClientId)
            .WithTipoCita(request.TipoCita)
            .WithEstado("Pendiente")
            .WithAuditFields(request.User)
            .WithCreatedBy(request.CreatedBy)
            .Build();

            return clienTracking;
        }

        public void Update(ClientTrackingRequest request, ClientTracking clientTracking)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (clientTracking == null) throw new ArgumentException(nameof(clientTracking));

            clientTracking
                .Update(request.FechaCita, request.Description, request.Comentarios, clientTracking.ClientId, request.TipoCita, request.User);
        }

    }
}