using System;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.ClientsTracking
{
    public class ClientTrackingDTO : ResponseBase
    {
        public DateTime? FechaCita { get; set; }
        public string Description { get; set; }
        public string Comentarios { get; set; }
        public bool CitaPendiente { get; set; }
        public int ClientId { get; set; }
        public int Orden { get; set; }
        public int TipoServicioId { get; set; }
        public int ProcesoId { get; set; }
        public string Estado { get; set; }
        public string Servicio { get; set; }

        public int MeetingCategoryId { get; set; }
        public string MeetingCategoryName { get; set; }
        public Client Client { get; set; }


        public string ClienteNombre { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreatedBy { get; set; }

    }
}