using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.ClientsTracking
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ClientTrackingController : ControllerBase
    {
        private readonly IClientTrackingAppService _clientTrackingAppService;

        public ClientTrackingController(IClientTrackingAppService clientTrackingAppService)
        {
            if (clientTrackingAppService == null) throw new ArgumentException(nameof(clientTrackingAppService));

            _clientTrackingAppService = clientTrackingAppService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<IEnumerable<ClientTrackingDTO>> Get(int clientId)
        {
            return Ok(_clientTrackingAppService.Get(clientId));
        }

        [HttpGet]
        [Route("top-citas")]
        [Authorize]
        public ActionResult<IEnumerable<ClientTrackingDTO>> GetTopTrackingDates()
        {
            var response = _clientTrackingAppService.GetNextTrackingDates();
            return Ok(response);
        }

        [HttpGet]
        [Route("completar-cita")]
        [Authorize]
        public ActionResult<IEnumerable<ClientTrackingDTO>> CompletarCita(int clientId)
        {
            return Ok(_clientTrackingAppService.CompletarCita(clientId));
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<IEnumerable<ClientTrackingDTO>> GetPaged([FromQuery]ClientTrackingPagedRequest request)
        {
            return Ok(_clientTrackingAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ClientTrackingDTO> Post([FromBody]ClientTrackingRequest request)
        {
            return Ok(_clientTrackingAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<ClientTrackingDTO> Put([FromBody]ClientTrackingRequest request)
        {
            return Ok(_clientTrackingAppService.Update(request));
        }

        [HttpPut]
        [Route("agregar-nuevo-seguimiento")]
        [Authorize]
        public ActionResult<IEnumerable<ClientTrackingDTO>> CrearNuevoSeguimiento([FromBody]NuevoSeguimientoRequest request)
        {
            return Ok(_clientTrackingAppService.CrearNuevoSeguimiento(request));
        }


        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_clientTrackingAppService.Delete(Id));
        }

    }
}