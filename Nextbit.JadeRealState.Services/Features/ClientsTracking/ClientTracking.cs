using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.MeetingCategories;
using Nextbit.JadeRealState.Services.Features.ServicesTracking;

namespace Nextbit.JadeRealState.Services.Features.ClientsTracking
{
    [Table("ClientsTracking")]
    public sealed class ClientTracking : Entity
    {
        private ClientTracking() { }

        public DateTime? FechaCita { get; set; }
        public string Description { get; set; }
        public string Comentarios { get; set; }
        public int ClientId { get; set; }
        public int Orden { get; private set; }
        public int TipoServicioId { get; set; }
        public int ProcesoId { get; set; }
        public string Estado { get; set; }
        public int MeetingCategoryId { get; set; }
        public string CreatedBy { get; private set; }
        public Client Client { get; set; }
        public MeetingCategory MeetingCategory { get; set; }

        public ClientTracking Update(DateTime? _date, string _description, string _comentarios, int _clientId, int meetingCategory, string user)
        {
            FechaCita = _date;
            Description = _description;
            Comentarios = _comentarios;
            ClientId = _clientId;
            MeetingCategoryId = meetingCategory;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedSupplierService";
            ModifiedBy = user ?? "Service";
            TransactionUId = Guid.NewGuid();
            return this;
        }

        public ClientTracking UpdateEstado(DateTime? _date, string estado, string user)
        {
            FechaCita = _date;
            Estado = estado;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedSupplierService";
            ModifiedBy = user ?? "Service";
            TransactionUId = Guid.NewGuid();
            return this;
        }

        public class Builder
        {
            private readonly ClientTracking _clientTracking = new ClientTracking();

            public Builder WithDate(DateTime? fechaCita)
            {
                _clientTracking.FechaCita = fechaCita;
                return this;
            }

            public Builder WithDescription(string description)
            {
                _clientTracking.Description = description;
                return this;
            }

            public Builder WithClientId(int clientId)
            {
                _clientTracking.ClientId = clientId;
                return this;
            }

            public Builder WithComentarios(string comentarios)
            {
                _clientTracking.Comentarios = comentarios;
                return this;
            }

            public Builder WithOrden(int orden)
            {
                _clientTracking.Orden = orden;
                return this;
            }
            public Builder WithTipoServicio(int tipoServicioId)
            {
                _clientTracking.TipoServicioId = tipoServicioId;
                return this;
            }

            public Builder WithTipoCita(int tipoCita)
            {
                _clientTracking.MeetingCategoryId = tipoCita;
                return this;
            }

            public Builder WithProceso(int procesoId)
            {
                _clientTracking.ProcesoId = procesoId;
                return this;
            }

            public Builder WithEstado(string estado)
            {
                _clientTracking.Estado = estado;
                return this;
            }

            public Builder WithCreatedBy(string createdBy)
            {
                _clientTracking.CreatedBy = createdBy;
                return this;
            }



            public Builder WithAuditFields(string user)
            {
                _clientTracking.CrudOperation = "Added";
                _clientTracking.TransactionDate = DateTime.Now;
                _clientTracking.TransactionType = "NewDateClient";
                _clientTracking.ModifiedBy = user ?? "Service";
                _clientTracking.TransactionUId = Guid.NewGuid();

                return this;
            }

            public ClientTracking Build()
            {
                return _clientTracking;
            }
        }
    }
}