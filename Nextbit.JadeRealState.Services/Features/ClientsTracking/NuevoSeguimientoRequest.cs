using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.ClientsTracking
{
    public class NuevoSeguimientoRequest : RequestBase
    {
        public int ClienteId { get; set; }
        public int SupplierServiceId { get; set; }
        public string CreatedBy { get; set; }
    }
}