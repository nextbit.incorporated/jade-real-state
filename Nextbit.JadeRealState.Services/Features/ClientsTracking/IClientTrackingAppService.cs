using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.ClientsTracking
{
    public interface IClientTrackingAppService : IDisposable
    {
        ClientTrackingPagedDTO GetPaged(ClientTrackingPagedRequest request);
        ClientTrackingDTO Create(ClientTrackingRequest request);
        ClientTrackingDTO Update(ClientTrackingRequest request);
        IEnumerable<ClientTrackingDTO> Get(int clientId);
        string CompletarCita(int id);
        string Delete(int id);
        IEnumerable<ClientTrackingDTO> CrearNuevoSeguimiento(NuevoSeguimientoRequest request);
        IEnumerable<ClientTrackingDTO> GetNextTrackingDates();
    }
}