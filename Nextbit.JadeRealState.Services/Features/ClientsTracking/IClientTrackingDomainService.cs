using System;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.ClientsTracking
{
    public interface IClientTrackingDomainService
    {
        ClientTracking Create(ClientTrackingRequest request);
        void Update(ClientTrackingRequest request, ClientTracking clientTrackingOldInfo);
    }
}