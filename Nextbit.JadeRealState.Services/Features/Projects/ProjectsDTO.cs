using System;
using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;
using System.Linq;

namespace Nextbit.JadeRealState.Services.Features.Projects
{
    public class ProjectsDTO : ResponseBase
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }

        internal static List<ProjectsDTO> From(IEnumerable<Project> projects)
        {
            return (from qry in projects
                    select From(qry)).ToList()
                ;
        }

        internal static ProjectsDTO From(Project project)
        {
            if (project == null) return null;
            
            return new ProjectsDTO
            {
                Id = project.Id,
                Name = project.Name,
                Country = project.Country,
                State = project.State,
                City = project.City,
                Address = project.Address
            };
        }
    }
}