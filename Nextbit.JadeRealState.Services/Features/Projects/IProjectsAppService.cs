using System.Collections.Generic;
using System;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Features.Projects
{
    public interface IProjectsAppService : IDisposable
    {
        Task<List<ProjectsDTO>> GetProjectsAsync();
        ProjectPagedDTO GetPagedProject(ProjectPagedRequest request);
        ProjectsDTO CreateProject(ProjectRequest request);
        ProjectsDTO UpdateProject(ProjectRequest request);
        string DeleteProject(int id);
    }
}