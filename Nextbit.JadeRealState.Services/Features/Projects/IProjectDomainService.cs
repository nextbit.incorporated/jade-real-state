using System;

namespace Nextbit.JadeRealState.Services.Features.Projects
{
    public interface IProjectDomainService : IDisposable
    {
        Project CreateProject(ProjectRequest request);
        Project UpdateProject(ProjectRequest request, Project projectOldInfo);
    }
}