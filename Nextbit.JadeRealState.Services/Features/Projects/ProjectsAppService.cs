﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Projects
{
    public class ProjectsAppService : IProjectsAppService
    {
        private RealStateContext _context;
        private readonly IProjectDomainService _projectDomainService;

        public ProjectsAppService(RealStateContext context, IProjectDomainService projectDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (projectDomainService == null) throw new ArgumentException(nameof(projectDomainService));

            _context = context;
            _projectDomainService = projectDomainService;
        }


        public ProjectPagedDTO GetPagedProject(ProjectPagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            var count = Convert.ToDecimal(_context.Projects.Count());
            var totalPage = Math.Ceiling(count / request.PageSize);
            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;
            if (string.IsNullOrEmpty(request.Value))
            {
                var lista = _context.Projects.OrderByDescending(s => s.TransactionDate).Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ProjectPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Projects = lista.Select(s => new ProjectsDTO
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Country = s.Country,
                        State = s.State,
                        City = s.City,
                        Address = s.Address,
                    }).ToList()
                };
            }
            else
            {
                var lista = _context.Projects.Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ProjectPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Projects = lista.Select(s => new ProjectsDTO
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Country = s.Country,
                        State = s.State,
                        City = s.City,
                        Address = s.Address,
                    }).ToList()
                };
            }
        }


        public async Task<List<ProjectsDTO>> GetProjectsAsync()
        {
            IEnumerable<Project> projects = await _context.Projects.ToListAsync();

            return ProjectsDTO.From(projects);
        }

        public Project GetProjectById(int id)
        {
            return _context.Projects.FirstOrDefault(s => s.Id == id);
        }

        public Project CreateProject(int id, string name, string country, string state, string city,
                                     string address)
        {
            if (id == 0) throw new ArgumentNullException(nameof(id));
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentNullException(nameof(name));
            if (string.IsNullOrWhiteSpace(country)) throw new ArgumentNullException(nameof(country));
            if (string.IsNullOrWhiteSpace(state)) throw new ArgumentNullException(nameof(state));
            if (string.IsNullOrWhiteSpace(city)) throw new ArgumentNullException(nameof(city));
            if (string.IsNullOrWhiteSpace(address)) throw new ArgumentNullException(nameof(address));


            Project project = new Project.Builder()
            .WithName(name)
            .WithCountry(country)
            .WithState(state)
            .WithCity(city)
            .WithAddress(address)
            .Build();

            _context.Projects.Add(project);
            _context.SaveChanges();

            return project;
        }

        public ProjectsDTO CreateProject(ProjectRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newProject = _projectDomainService.CreateProject(request);

            _context.Projects.Add(newProject);
            _context.SaveChanges();

            return new ProjectsDTO
            {
                Id = newProject.Id,
                Name = newProject.Name,
                Country = newProject.Country,
                State = newProject.State,
                City = newProject.City,
                Address = newProject.Address
            };
        }

        public ProjectsDTO UpdateProject(ProjectRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldProject = _context.Projects.FirstOrDefault(s => s.Name == request.Name);
            if (oldProject == null) return new ProjectsDTO { ValidationErrorMessage = "Proyecto inexistente" };

            var rolUpdate = _projectDomainService.UpdateProject(request, oldProject);

            _context.Projects.Update(oldProject);
            _context.SaveChanges();


            return ProjectsDTO.From(rolUpdate);

        }

        public string DeleteProject(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var project = _context.Projects.FirstOrDefault(s => s.Id == id);
            if (project == null) throw new Exception("Project not exists");
            _context.Projects.Remove(project);
            _context.SaveChanges();

            return string.Empty;
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_projectDomainService != null) _projectDomainService.Dispose();
        }
    }
}
