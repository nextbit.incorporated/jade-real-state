﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.HouseDesigns;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.Prospects;

namespace Nextbit.JadeRealState.Services.Features.Projects
{
    [Table("Projects")]
    public class Project : Entity
    {
        public Project()
        {
            HouseDesigns = new HashSet<HouseDesign>();
        }

        public string Name { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Address { get; set; }

        public ICollection<Client> Clients { get; set; }
        public ICollection<Negotiation> Negotiations { get; set; }
        public ICollection<Prospect> Prospects { get; set; }


        public ICollection<HouseDesign> HouseDesigns
        {
            get { return _houseDesigns ?? (_houseDesigns = new HashSet<HouseDesign>()); }
            private set { _houseDesigns = value; }
        }
        private ICollection<HouseDesign> _houseDesigns;

        public void Update(string _name, string _country, string _state, string _city, string _address)
        {
            Name = _name;
            Country = _country;
            State = _state;
            City = _city;
            Address = _address;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedProject";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
        }


        public class Builder
        {
            private readonly Project _project = new Project();

            public Builder WithName(string name)
            {
                _project.Name = name;
                return this;
            }

            public Builder WithCountry(string country)
            {
                _project.Country = country;
                return this;
            }

            public Builder WithState(string state)
            {
                _project.State = state;
                return this;
            }

            public Builder WithCity(string city)
            {
                _project.City = city;
                return this;
            }

            public Builder WithAddress(string address)
            {
                _project.Address = address;
                return this;
            }

            public Builder WithAuditFields()
            {
                _project.CrudOperation = "Added";
                _project.TransactionDate = DateTime.Now;
                _project.TransactionType = "NewProject";
                _project.ModifiedBy = "Service";
                _project.TransactionUId = Guid.NewGuid();

                return this;
            }

            public Project Build()
            {
                return _project;
            }
        }
    }
}
