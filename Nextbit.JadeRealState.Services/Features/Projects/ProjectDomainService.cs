using System;

namespace Nextbit.JadeRealState.Services.Features.Projects
{
    public class ProjectDomainService : IProjectDomainService
    {
        public Project CreateProject(ProjectRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));
            if (string.IsNullOrWhiteSpace(request.Country)) throw new ArgumentNullException(nameof(request.Country));
            if (string.IsNullOrWhiteSpace(request.State)) throw new ArgumentNullException(nameof(request.State));
            if (string.IsNullOrWhiteSpace(request.City)) throw new ArgumentNullException(nameof(request.City));
            if (string.IsNullOrWhiteSpace(request.Address)) throw new ArgumentNullException(nameof(request.Address));

            Project project = new Project.Builder()
            .WithName(request.Name)
            .WithCountry(request.Country)
            .WithState(request.State)
            .WithCity(request.City)
            .WithAddress(request.Address)
            .WithAuditFields()
            .Build();

            return project;
        }

        public Project UpdateProject(ProjectRequest request, Project projectOldInfo)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (projectOldInfo == null) throw new ArgumentException(nameof(projectOldInfo));

            projectOldInfo.Update(request.Name,request.Country, request.State, request.City, request.Address);
            return projectOldInfo;
        }

        public void Dispose()
        {
        }

       
    }
}