﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Projects
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsAppService _projectsAppService;

        public ProjectsController(IProjectsAppService projectsAppService)
        {
            if (projectsAppService == null) throw new ArgumentException(nameof(projectsAppService));

            _projectsAppService = projectsAppService;
        }


        [HttpGet]
        [Route("")]
        [Authorize]
        public async Task<IActionResult> GetProjectsAsync()
        {
            return Ok(await _projectsAppService.GetProjectsAsync());
        }


        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<IEnumerable<ProjectsDTO>> GetPaged([FromQuery] ProjectPagedRequest request)
        {
            return Ok(_projectsAppService.GetPagedProject(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ProjectsDTO> Post([FromBody] ProjectRequest request)
        {
            return Ok(_projectsAppService.CreateProject(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<ProjectsDTO> Put([FromBody] ProjectRequest request)
        {
            return Ok(_projectsAppService.UpdateProject(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_projectsAppService.DeleteProject(Id));
        }

    }
}
