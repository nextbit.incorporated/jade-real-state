using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Projects
{
    public class ProjectPagedDTO
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<ProjectsDTO> Projects { get; set; }
    }
}