using System;

namespace Nextbit.JadeRealState.Services.Features.HouseDesigns
{
    public interface IHouseDesignDomainService : IDisposable
    {
        HouseDesign CreateHouseDesign(HouseDesignRequest request);
        HouseDesign UpdateHouseDesign(HouseDesignRequest request, HouseDesign builderCompanyOldInfo);
    }
}