namespace Nextbit.JadeRealState.Services.Features.HouseDesigns
{
    public sealed class GetHouseDesignsRequest
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
    }
}