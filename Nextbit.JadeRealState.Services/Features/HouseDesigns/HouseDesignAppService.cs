using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.Projects;

namespace Nextbit.JadeRealState.Services.Features.HouseDesigns
{
    public class HouseDesignAppService : IHouseDesignAppService
    {
        private RealStateContext _context;
        private readonly IHouseDesignDomainService _houseDesignDomainService;

        public HouseDesignAppService(RealStateContext context, IHouseDesignDomainService houseDesignDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (houseDesignDomainService == null) throw new ArgumentException(nameof(houseDesignDomainService));

            _context = context;
            _houseDesignDomainService = houseDesignDomainService;
        }

        public HouseDesignDTO CreateHouseDesign(HouseDesignRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newHouseDesign = _houseDesignDomainService.CreateHouseDesign(request);

            var project = _context.Projects.FirstOrDefault(s => s.Id == request.ProjectId);
            if (project == null) return new HouseDesignDTO { ValidationErrorMessage = "Modelo inexistente" };

            _context.HouseDesign.Add(newHouseDesign);
            _context.SaveChanges();

            return new HouseDesignDTO
            {
                ProjectId = newHouseDesign.ProjectId,
                Name = newHouseDesign.Name,
                Description = newHouseDesign.Description,
                HouseSpecifications = newHouseDesign.HouseSpecifications
            };
        }

        public string DeleteHouseDesign(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var houseDesign = _context.HouseDesign.FirstOrDefault(s => s.Id == id);
            if (houseDesign == null) throw new Exception("Modelo not existe");
            _context.HouseDesign.Remove(houseDesign);
            _context.SaveChanges();

            return string.Empty;
        }

        public HouseDesignPagedDTO GetPagedHouseDesign(HouseDesignPagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            var count = Convert.ToDecimal(_context.HouseDesign.Count());
            var totalPage = Math.Ceiling(count / request.PageSize);
            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;
            if (string.IsNullOrEmpty(request.Value))
            {
                var lista = _context.HouseDesign
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                var listProjectsId = lista.Select(s => s.ProjectId.ToString()).Distinct();
                var projects = _context.Projects.Where(s => listProjectsId.Contains(s.Id.ToString()));
                return new HouseDesignPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Designs = lista.Select(s => new HouseDesignDTO
                    {
                        Id = s.Id,
                        ProjectId = s.ProjectId,
                        Name = s.Name,
                        Description = s.Description,
                        HouseSpecifications = s.HouseSpecifications,
                        ProjectName = GetProjectName(s.ProjectId, projects),
                        Status = s.Status
                    }).ToList()
                };
            }
            else
            {
                var lista = _context.HouseDesign.Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                var listProjectsId = lista.Select(s => s.ProjectId.ToString()).Distinct();
                var projects = _context.Projects.Where(s => listProjectsId.Contains(s.Id.ToString()));
                return new HouseDesignPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Designs = lista.Select(s => new HouseDesignDTO
                    {
                        Id = s.Id,
                        Name = s.Name,
                        ProjectId = s.ProjectId,
                        Description = s.Description,
                        HouseSpecifications = s.HouseSpecifications,
                        ProjectName = GetProjectName(s.ProjectId, projects),
                        Status = s.Status
                    }).ToList()
                };
            }
        }

        private string GetProjectName(int projectId, IQueryable<Project> projects)
        {
            var project = projects.FirstOrDefault(s => s.Id == projectId);
            if (project == null) return string.Empty;

            return project.Name;
        }

        public HouseDesignDTO UpdateHouseDesign(HouseDesignRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldHouseDesign = _context.HouseDesign.FirstOrDefault(s => s.Id == request.Id);
            if (oldHouseDesign == null) return new HouseDesignDTO { ValidationErrorMessage = "Constructora inexistente" };

            var rolUpdate = _houseDesignDomainService.UpdateHouseDesign(request, oldHouseDesign);

            _context.HouseDesign.Update(oldHouseDesign);
            _context.SaveChanges();

            return new HouseDesignDTO
            {
                Id = oldHouseDesign.Id,
                Name = oldHouseDesign.Name,
                Description = oldHouseDesign.Description,
                HouseSpecifications = oldHouseDesign.HouseSpecifications,
                Status = oldHouseDesign.Status
            };
        }

        public async Task<HouseDesignDTO> GetHouseDesignAsync(int id)
        {
            HouseDesign houseDesign = await _context.HouseDesign.FindAsync(id);

            return houseDesign != null
                ? HouseDesignDTO.From(houseDesign)
                : null;
        }

        public async Task<List<HouseDesignDTO>> GetHouseDesignsAsync(int? projectId)
        {
            List<HouseDesign> houseDesigns = projectId != null
                ? await _context.HouseDesign.Where(h => h.ProjectId == projectId).ToListAsync()
                : await _context.HouseDesign.ToListAsync();

            return HouseDesignDTO.From(houseDesigns);
        }




        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_houseDesignDomainService != null) _houseDesignDomainService.Dispose();
        }
    }
}