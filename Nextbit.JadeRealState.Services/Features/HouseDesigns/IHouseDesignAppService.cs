using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Features.HouseDesigns
{
    public interface IHouseDesignAppService : IDisposable
    {
        HouseDesignPagedDTO GetPagedHouseDesign(HouseDesignPagedRequest request);
        HouseDesignDTO CreateHouseDesign(HouseDesignRequest request);
        HouseDesignDTO UpdateHouseDesign(HouseDesignRequest request);
        string DeleteHouseDesign(int id);
        Task<List<HouseDesignDTO>> GetHouseDesignsAsync(int? projectId);
        Task<HouseDesignDTO> GetHouseDesignAsync(int id);
    }
}