using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.HouseDesigns
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class HouseDesignController : ControllerBase
    {
        private readonly IHouseDesignAppService _houseDesignAppService;
        public HouseDesignController(IHouseDesignAppService houseDesignAppService)
        {
            if (houseDesignAppService == null) throw new ArgumentException(nameof(houseDesignAppService));

            _houseDesignAppService = houseDesignAppService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get([FromQuery] GetHouseDesignsRequest getHouseDesignsRequest)
        {
            if (getHouseDesignsRequest?.Id > 0)
            {
                HouseDesignDTO houseDesign = await _houseDesignAppService.GetHouseDesignAsync(getHouseDesignsRequest.Id);

                if (houseDesign != null)
                {
                    return Ok(houseDesign);
                }

                return NotFound();
            }

            List<HouseDesignDTO> houseDesigns = await _houseDesignAppService.GetHouseDesignsAsync(getHouseDesignsRequest?.ProjectId);

            return Ok(houseDesigns);
        }


        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<HouseDesignPagedDTO> GetPaged([FromQuery] HouseDesignPagedRequest request)
        {
            return Ok(_houseDesignAppService.GetPagedHouseDesign(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<HouseDesignDTO> Post([FromBody] HouseDesignRequest request)
        {
            return Ok(_houseDesignAppService.CreateHouseDesign(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<HouseDesignDTO> Put([FromBody] HouseDesignRequest request)
        {
            return Ok(_houseDesignAppService.UpdateHouseDesign(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_houseDesignAppService.DeleteHouseDesign(Id));
        }
    }
}