using System;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Projects;

namespace Nextbit.JadeRealState.Services.Features.HouseDesigns
{
    [Table("HouseDesign")]
    public class HouseDesign : Entity
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HouseSpecifications { get; set; }
        public string Status { get; set; }

        public Project Project { get; set; }

        public void Update(
            int _projectId, string _name, string _description, string _houseSpecifications)
        {
            ProjectId = _projectId;
            Name = _name;
            Description = _description;
            HouseSpecifications = _houseSpecifications;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedProject";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
            Status = "ACTIVO";
        }

        public class Builder
        {
            private readonly HouseDesign _houseDesign = new HouseDesign();

            public Builder WithProjectId(int projectId)
            {
                _houseDesign.ProjectId = projectId;
                return this;
            }

            public Builder WithName(string name)
            {
                _houseDesign.Name = name;
                return this;
            }

            public Builder WithDescription(string description)
            {
                _houseDesign.Description = description;
                return this;
            }

            public Builder WithHouseSpecifications(string houseSpecifications)
            {
                _houseDesign.HouseSpecifications = houseSpecifications;
                return this;
            }

            public Builder WithAuditFields()
            {
                _houseDesign.CrudOperation = "Added";
                _houseDesign.TransactionDate = DateTime.Now;
                _houseDesign.TransactionType = "NewProject";
                _houseDesign.ModifiedBy = "Service";
                _houseDesign.TransactionUId = Guid.NewGuid();
                _houseDesign.Status = "ACTIVO";

                return this;
            }

            public HouseDesign Build()
            {
                return _houseDesign;
            }
        }
    }
}