using System;

namespace Nextbit.JadeRealState.Services.Features.HouseDesigns
{
    public class HouseDesignDomainService : IHouseDesignDomainService
    {
        public HouseDesign CreateHouseDesign(HouseDesignRequest request)
        {
            if(request == null) throw new ArgumentNullException(nameof(request));
            if (request.ProjectId == 0) throw new ArgumentNullException(nameof(request.ProjectId));
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));
            if (string.IsNullOrWhiteSpace(request.Description)) throw new ArgumentNullException(nameof(request.Description));
            if (string.IsNullOrWhiteSpace(request.HouseSpecifications)) throw new ArgumentNullException(nameof(request.HouseSpecifications));

            HouseDesign houseDesign = new HouseDesign.Builder()
            .WithName(request.Name)
            .WithProjectId(request.ProjectId)
            .WithDescription(request.Description ?? string.Empty)
            .WithHouseSpecifications(request.HouseSpecifications ?? string.Empty)
            .WithAuditFields()
            .Build();

            return houseDesign;
        }

        public HouseDesign UpdateHouseDesign(HouseDesignRequest request, HouseDesign houseDesignOldInfo)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (houseDesignOldInfo == null) throw new ArgumentException(nameof(houseDesignOldInfo));

            houseDesignOldInfo.Update(
                request.ProjectId, request.Name, request.Description, request.HouseSpecifications);

            return houseDesignOldInfo;
        }

        public void Dispose()
        {
        }  
    }
}