using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.HouseDesigns
{
    public class HouseDesignPagedDTO
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<HouseDesignDTO> Designs { get; set; }
    }
}