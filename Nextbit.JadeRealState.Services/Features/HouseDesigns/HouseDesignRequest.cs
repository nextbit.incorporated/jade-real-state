using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.HouseDesigns
{
    public class HouseDesignRequest : RequestBase
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HouseSpecifications { get; set; }
    }

    public class HouseDesignPagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
    }
}