using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.HouseDesigns
{
    public class HouseDesignDTO : ResponseBase
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string HouseSpecifications { get; set; }
        public string ProjectName { get; set; }

        internal static List<HouseDesignDTO> From(List<HouseDesign> houseDesigns)
        {
            return (from query in houseDesigns select From(query)).ToList();
        }

        internal static HouseDesignDTO From(HouseDesign houseDesign)
        {
            return new HouseDesignDTO
            {
                Id = houseDesign.Id,
                CreationDate = houseDesign.CreationDate,
                Name = houseDesign.Name,
                Description = houseDesign.Description,
                ProjectId = houseDesign.ProjectId
            };
        }
    }
}