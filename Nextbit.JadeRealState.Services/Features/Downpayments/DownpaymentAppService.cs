using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    public class DownpaymentAppService : IDownpaymentAppService
    {
        private RealStateContext _context;
        private readonly IDownpaymentDomainService _reserveDomainService;
        public DownpaymentAppService(RealStateContext context, IDownpaymentDomainService reserveDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (reserveDomainService == null) throw new ArgumentException(nameof(reserveDomainService));

            _context = context;
            _reserveDomainService = reserveDomainService;
        }

        public DownpaymentDto Create(DownpaymentRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newProject = _reserveDomainService.Create(request);

            _context.Downpayments.Add(newProject);
            _context.SaveChanges();

            var newReserves = _context.Downpayments
                .Where(s => s.NegotiationId == request.NegotiationId)
                .Include(x => x.Detalles)
                .Include(x => x.Negotiation).ThenInclude(t => t.Project)
                .Include(x => x.Negotiation).ThenInclude(x => x.NegotiationClients).ThenInclude(x => x.Client);

            var nReserve = newReserves.FirstOrDefault();
            return DownpaymentDto.From(nReserve);
        }

        public DownpaymentDto Update(DownpaymentRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldReserve = _context.Downpayments.FirstOrDefault(s => s.Id == request.Id);
            if (oldReserve == null) return new DownpaymentDto { ValidationErrorMessage = "Proyecto inexistente" };

            var newReserve = _reserveDomainService.Update(request, oldReserve);

            _context.Downpayments.Update(oldReserve);
            _context.SaveChanges();

            return new DownpaymentDto
            {
                Id = newReserve.Id,
                NegotiationId = newReserve.NegotiationId,
                LotNumber = newReserve.LotNumber,
                Zone = newReserve.Zone,
                Block = newReserve.Block,
                TotalAmount = newReserve.TotalAmount,
                AmountOfPayments = newReserve.AmountOfPayments,
                DownpaymentStatus = newReserve.DownpaymentStatus,
                Address = newReserve.Address,
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var project = _context.Reserves.FirstOrDefault(s => s.Id == id);
            if (project == null) throw new Exception("Project not exists");
            _context.Reserves.Remove(project);
            _context.SaveChanges();

            return string.Empty;
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_reserveDomainService != null) _reserveDomainService.Dispose();
        }

    }
}