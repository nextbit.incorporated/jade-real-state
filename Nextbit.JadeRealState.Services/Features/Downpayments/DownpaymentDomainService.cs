using System;
using System.Linq;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    public class DownpaymentDomainService : IDownpaymentDomainService
    {
        public Downpayment Create(DownpaymentRequest request)
        {
            if (request.NegotiationId <= 0) throw new ArgumentNullException(nameof(request.NegotiationId));

            Downpayment registro = new Downpayment.Builder()
            .WithNegotiation(request.NegotiationId)
            .WithLotNumber(request.LotNumber)
            .WithZone(request.Zone)
            .WithBlock(request.Block)
            .WithAddress(request.Address)
            .WithTotalAmount(request.TotalAmount)
            .WithAmountPayments(request.AmountOfPayments)
            .WithDownpaymentStatus(request.DownpaymentStatus)
            .WithAuditFields(request.User)
            .Build();

            if (request.Detalle == null) return registro;
            if (!request.Detalle.Any()) return registro;

            foreach (var item in request.Detalle)
            {
                DownpaymentDetail detalle = new DownpaymentDetail.Builder()
                 .WithDownpaymentDeatil(0, item.Date, item.Payment)
                 .WithComment(item.Comment)
                 .WithAuditFields(request.User)
                 .Build();

                registro.Detalles.Add(detalle);
            }
            return registro;
        }

        public Downpayment Update(DownpaymentRequest request, Downpayment _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.NegotiationId, request.SalesAdvisorId,
                        request.LotNumber, request.Zone, request.Block, request.Address,
                        request.TotalAmount, request.AmountOfPayments, request.DownpaymentStatus, request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}