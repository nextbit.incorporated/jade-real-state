using System;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    public interface IDownpaymentDetailDomainService : IDisposable
    {
        DownpaymentDetail Create(DownpaymentDetailRequest request);
        DownpaymentDetail Update(DownpaymentDetailRequest request, DownpaymentDetail _oldRegister);
    }
}