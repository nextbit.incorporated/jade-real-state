using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    public class DownpaymentDetailRequest : RequestBase
    {
        public int Id { get; set; }
        public int DownpaymentId { get; set; }
        public DateTime Date { get; set; }
        public Decimal Payment { get; set; }
        public string Comment { get; set; }
    }
}