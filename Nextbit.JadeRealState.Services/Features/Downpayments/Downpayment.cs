using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    [Table("Downpayments")]
    public class Downpayment : Entity
    {
        public int NegotiationId { get; private set; }
        public int LotNumber { get; private set; }
        public string Zone { get; private set; }
        public string Block { get; private set; }
        public string Address { get; private set; }
        public decimal TotalAmount { get; private set; }
        public int AmountOfPayments { get; private set; }
        public string DownpaymentStatus { get; private set; }
        
        public User SalesAdvisor { get; set; }
        public Negotiation Negotiation { get; set; }
        public ICollection<DownpaymentDetail> Detalles { get; set; }

        public void Update(int projectId, int salesAdvisorId,
           int lotNumber, string zone, string block, string address,
           decimal totalAmount, int amountOfPayments, string downpaymentStatus, string user)
        {
            LotNumber = lotNumber;
            Zone = zone;
            Block = block;
            Address = address;
            TotalAmount = totalAmount;
            AmountOfPayments = amountOfPayments;
            DownpaymentStatus = downpaymentStatus;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = string.IsNullOrEmpty(user) ? "Service" : user;
            TransactionUId = Guid.NewGuid();
        }

        public class Builder
        {
            private readonly Downpayment _downpayment = new Downpayment();

            public Builder WithNegotiation(int negotiationId)
            {
                _downpayment.NegotiationId = negotiationId;
                return this;
            }
            public Builder WithLotNumber(int lotNumber)
            {
                _downpayment.LotNumber = lotNumber;
                return this;
            }
            public Builder WithZone(string zone)
            {
                _downpayment.Zone = zone;
                return this;
            }
            public Builder WithBlock(string block)
            {
                _downpayment.Block = block;
                return this;
            }
            public Builder WithAddress(string address)
            {
                _downpayment.Address = address;
                return this;
            }
            public Builder WithTotalAmount(decimal total)
            {
                _downpayment.TotalAmount = total;
                return this;
            }
            public Builder WithAmountPayments(int amountPayments)
            {
                _downpayment.AmountOfPayments = amountPayments;
                return this;
            }
            public Builder WithDownpaymentStatus(string downpaymentStatus)
            {
                _downpayment.DownpaymentStatus = downpaymentStatus;
                return this;
            }
            public Builder WithAuditFields(string user)
            {
                _downpayment.Detalles = new List<DownpaymentDetail>();
                _downpayment.CrudOperation = "Added";
                _downpayment.TransactionDate = DateTime.Now;
                _downpayment.TransactionType = "NewProject";
                _downpayment.ModifiedBy = string.IsNullOrEmpty(user) ? "Service" : user;
                _downpayment.TransactionUId = Guid.NewGuid();

                return this;
            }

            public Downpayment Build()
            {
                return _downpayment;
            }

        }
    }
}