using System;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    [Table("DownpaymentsDetail")]
    public class DownpaymentDetail : Entity
    {
        public int DownpaymentId { get; private set; }
        public DateTime Date { get; private set; }
        public Decimal Payment { get; private set; }
        public string Comment { get; private set; }
        public bool Active { get; private set; }
        public Downpayment Downpayment { get; set; }
        public void Update(DateTime _date, Decimal _payment, string _comment, string _user)
        {
            Date = _date;
            Payment = _payment;
            Comment = _comment;
            Active = true;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = string.IsNullOrEmpty(_user) ? "Service" : _user;
            TransactionUId = Guid.NewGuid();
        }

        public void Inactive(string _user)
        {
            Active = false;
            CrudOperation = "Inactive";
            TransactionDate = DateTime.Now;
            TransactionType = "Disabled";
            ModifiedBy = string.IsNullOrEmpty(_user) ? "Service" : _user;
            TransactionUId = Guid.NewGuid();
        }

        public class Builder
        {
            private readonly DownpaymentDetail _downpaymentDetail = new DownpaymentDetail();

            public Builder WithDownpaymentDeatil(int downpaymentId, DateTime date, Decimal Payment)
            {
                _downpaymentDetail.DownpaymentId = downpaymentId;
                _downpaymentDetail.Date = date;
                _downpaymentDetail.Payment = Payment;
                return this;
            }
            public Builder WithComment(string comment)
            {
                _downpaymentDetail.Comment = comment;
                return this;
            }
            public Builder WithAuditFields(string user)
            {
                _downpaymentDetail.Active = true;
                _downpaymentDetail.CrudOperation = "Added";
                _downpaymentDetail.TransactionDate = DateTime.Now;
                _downpaymentDetail.TransactionType = "NewProject";
                _downpaymentDetail.ModifiedBy = string.IsNullOrEmpty(user) ? "Service" : user;
                _downpaymentDetail.TransactionUId = Guid.NewGuid();

                return this;
            }
            public DownpaymentDetail Build()
            {
                return _downpaymentDetail;
            }
        }
    }
}