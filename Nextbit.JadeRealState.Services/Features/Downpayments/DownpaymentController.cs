using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DownpaymentController : ControllerBase
    {
        private readonly IDownpaymentAppService _downpaymentAppService;

        public DownpaymentController(IDownpaymentAppService downpaymentAppService)
        {
            if (downpaymentAppService == null) throw new ArgumentException(nameof(downpaymentAppService));

            _downpaymentAppService = downpaymentAppService;
        }

        [HttpPost]
        [Authorize]
        public ActionResult<DownpaymentDto> Post([FromBody]DownpaymentRequest request)
        {
            return Ok(_downpaymentAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<DownpaymentDto> Put([FromBody] DownpaymentRequest request)
        {
            return Ok(_downpaymentAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_downpaymentAppService.Delete(Id));
        }

    }
}