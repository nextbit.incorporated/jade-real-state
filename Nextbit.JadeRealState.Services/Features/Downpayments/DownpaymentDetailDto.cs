using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    public class DownpaymentDetailDto : ResponseBase
    {
        public int DownpaymentId { get; set; }
        public DateTime Date { get; set; }
        public Decimal Payment { get; set; }
        public string Comment { get; set; }
        public bool Active { get; set; }

        internal static List<DownpaymentDetailDto> FromNegotiations(IEnumerable<DownpaymentDetail> detail)
        {
            if (detail == null || !detail.Any()) return new List<DownpaymentDetailDto>();
            return (from qry in detail
                    select From(qry)).ToList();
        }

        internal static DownpaymentDetailDto From(DownpaymentDetail s)
        {
            if (s == null) return new DownpaymentDetailDto();
            return new DownpaymentDetailDto
            {
                Id = s.Id,
                DownpaymentId = s.DownpaymentId,
                Date = s.Date,
                Payment = s.Payment,
                Comment = s.Comment,
                Active = s.Active
            };
        }

    }
}