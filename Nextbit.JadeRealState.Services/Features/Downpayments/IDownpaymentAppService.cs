using System;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    public interface IDownpaymentAppService : IDisposable
    {
        // ReservePagedDto GetPaged(ReservePagedRequest request);
        DownpaymentDto Create(DownpaymentRequest request);
        DownpaymentDto Update(DownpaymentRequest request);
        string Delete(int id);
    }
}