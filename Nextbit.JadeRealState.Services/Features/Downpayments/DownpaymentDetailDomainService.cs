using System;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    public class DownpaymentDetailDomainService : IDownpaymentDetailDomainService
    {
        public DownpaymentDetail Create(DownpaymentDetailRequest request)
        {
            if (request.DownpaymentId <= 0) throw new ArgumentNullException(nameof(request.DownpaymentId));

            DownpaymentDetail registro = new DownpaymentDetail.Builder()
            .WithDownpaymentDeatil(request.DownpaymentId, request.Date, request.Payment)
            .WithComment(request.Comment)
            .WithAuditFields(request.User)
            .Build();

            return registro;
        }
        public DownpaymentDetail Update(DownpaymentDetailRequest request, DownpaymentDetail _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.Date, request.Payment, request.Comment, request.User);
            return _oldRegister;
        }
        public void Dispose()
        {
        }
    }
}