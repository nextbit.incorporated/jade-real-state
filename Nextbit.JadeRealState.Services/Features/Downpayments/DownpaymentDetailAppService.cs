using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    public class DownpaymentDetailAppService : IDownpaymentDetailAppService
    {
        private RealStateContext _context;
        private readonly IDownpaymentDetailDomainService _reserveDetailDomainService;
        public DownpaymentDetailAppService(RealStateContext context, IDownpaymentDetailDomainService reserveDetailDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (reserveDetailDomainService == null) throw new ArgumentException(nameof(reserveDetailDomainService));

            _context = context;
            _reserveDetailDomainService = reserveDetailDomainService;
        }
        public IEnumerable<DownpaymentDetailDto> Create(DownpaymentDetailRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newRow = _reserveDetailDomainService.Create(request);

            _context.DownpaymentDetails.Add(newRow);
            _context.SaveChanges();

            var detalles = _context.DownpaymentDetails
                .Where(s => s.DownpaymentId == request.DownpaymentId);
            var nuevoDetalle = DownpaymentDetailDto.FromNegotiations(detalles);
            return nuevoDetalle;
        }

        public IEnumerable<DownpaymentDetailDto> GetById(DownpaymentDetailRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var detalles = _context.DownpaymentDetails
                .Where(s => s.Id == request.DownpaymentId);
            var nuevoDetalle = DownpaymentDetailDto.FromNegotiations(detalles);
            return nuevoDetalle;
        }
        public IEnumerable<DownpaymentDetailDto> GetDownpaymentDetails(DownpaymentDetailRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.Id == 0) throw new ArgumentException(nameof(request.Id));
            var details = _context.DownpaymentDetails.Where(s => s.Id == request.Id);

            if (details == null) return new List<DownpaymentDetailDto>();
            if (!details.Any()) return new List<DownpaymentDetailDto>();
            return DownpaymentDetailDto.FromNegotiations(details);
        }

        public string Inactive(DownpaymentDetailRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.Id == 0) throw new ArgumentException(nameof(request.Id));
            var oldReserveDetail = _context.DownpaymentDetails.FirstOrDefault(s => s.Id == request.Id);
            if (oldReserveDetail == null) return "Proyecto inexistente";

            oldReserveDetail.Inactive(request.User);
            _context.DownpaymentDetails.Update(oldReserveDetail);
            _context.SaveChanges();

            return string.Empty;
        }

        public DownpaymentDetailDto Update(DownpaymentDetailRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldReserveDetail = _context.DownpaymentDetails.FirstOrDefault(s => s.Id == request.Id);
            if (oldReserveDetail == null) return new DownpaymentDetailDto { ValidationErrorMessage = "Proyecto inexistente" };

            var newReserveDetail = _reserveDetailDomainService.Update(request, oldReserveDetail);

            _context.DownpaymentDetails.Update(oldReserveDetail);
            _context.SaveChanges();

            return DownpaymentDetailDto.From(oldReserveDetail);
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_reserveDetailDomainService != null) _reserveDetailDomainService.Dispose();
        }


    }
}