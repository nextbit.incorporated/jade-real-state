using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DownpaymentDetailController : ControllerBase
    {
        private readonly IDownpaymentDetailAppService _downpaymentDetailAppService;

        public DownpaymentDetailController(IDownpaymentDetailAppService reserveDetailAppService)
        {
            if (reserveDetailAppService == null) throw new ArgumentException(nameof(reserveDetailAppService));

            _downpaymentDetailAppService = reserveDetailAppService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<IEnumerable<DownpaymentDetailDto>> Get([FromQuery]DownpaymentDetailRequest request)
        {
            return Ok(_downpaymentDetailAppService.GetDownpaymentDetails(request));
        }

        [HttpGet]
        [Route("byId")]
        [Authorize]
        public ActionResult<IEnumerable<DownpaymentDetailDto>> GetById([FromQuery]DownpaymentDetailRequest request)
        {
            return Ok(_downpaymentDetailAppService.GetById(request));
        }


        [HttpPost]
        [Authorize]
        public ActionResult<IEnumerable<DownpaymentDetailDto>> Post([FromBody]DownpaymentDetailRequest request)
        {
            return Ok(_downpaymentDetailAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<DownpaymentDto> Put([FromBody] DownpaymentDetailRequest request)
        {
            return Ok(_downpaymentDetailAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(DownpaymentDetailRequest request)
        {
            return Ok(_downpaymentDetailAppService.Inactive(request));
        }
    }
}