using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    public class DownpaymentDto : ResponseBase 
    {
        public int NegotiationId { get;  set; }
        public int LotNumber { get;  set; }
        public string Zone { get;  set; }
        public string Block { get;  set; }
        public string Address { get;  set; }
        public decimal TotalAmount { get;  set; }
        public int AmountOfPayments { get;  set; }
        public string DownpaymentStatus { get;  set; }

        public UserDTO SalesAdvisor { get; set; }
        public ICollection<DownpaymentDetailDto> Detalle { get; set; }

        internal static List<DownpaymentDto> FromReserves(IEnumerable<Downpayment> detail)
        {
            return (from qry in detail
                    select From(qry)).ToList();
        }

        internal static DownpaymentDto From(Downpayment s)
        {
            if (s == null) return new DownpaymentDto();
            return new DownpaymentDto
            {
                Id = s.Id,
                NegotiationId = s.NegotiationId,
                LotNumber = s.LotNumber,
                Zone = s.Zone,
                Block = s.Block,
                TotalAmount = s.TotalAmount,
                AmountOfPayments = s.AmountOfPayments,
                DownpaymentStatus = s.DownpaymentStatus,
                Address = s.Address,
                // Negotiation = s.Negotiation == null ? new NegotiationDto() : NegotiationDto.From(s.Negotiation),  
                Detalle = s.Detalles == null || !s.Detalles.Any() ? new List<DownpaymentDetailDto>() : DownpaymentDetailDto.FromNegotiations(s.Detalles)
            };
        }

    }
}