

using System;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    public interface IDownpaymentDomainService : IDisposable
    {
        Downpayment Create(DownpaymentRequest request);
        Downpayment Update(DownpaymentRequest request, Downpayment _oldRegister);
    }
}