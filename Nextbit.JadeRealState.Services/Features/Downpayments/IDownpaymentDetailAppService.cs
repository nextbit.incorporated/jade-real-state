

using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Downpayments
{
    public interface IDownpaymentDetailAppService : IDisposable
    {
        IEnumerable<DownpaymentDetailDto> GetDownpaymentDetails(DownpaymentDetailRequest request);
        IEnumerable<DownpaymentDetailDto> Create(DownpaymentDetailRequest request);
        IEnumerable<DownpaymentDetailDto> GetById(DownpaymentDetailRequest request);
        DownpaymentDetailDto Update(DownpaymentDetailRequest request);
        string Inactive(DownpaymentDetailRequest request);
    }
}