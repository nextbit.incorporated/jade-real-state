using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.GeneralInfo
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class InformationController : ControllerBase
    {
        private readonly IInformationAppService _parametroAppService;
        public InformationController(IInformationAppService parametroAppService)
        {
            _parametroAppService = parametroAppService ?? throw new ArgumentException(nameof(parametroAppService));
        }

        [HttpGet]
        [Authorize]
        public ActionResult<InformationDto> Get()
        {
            return Ok(_parametroAppService.Get());
        }


        [HttpPost]
        [Authorize]
        //[AllowAnonymous]
        public ActionResult<InformationDto> Post([FromBody] InformationRequest request)
        {
            return Ok(_parametroAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<InformationDto> Put([FromBody] InformationRequest request)
        {
            return Ok(_parametroAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_parametroAppService.Delete(Id));
        }
    }
}