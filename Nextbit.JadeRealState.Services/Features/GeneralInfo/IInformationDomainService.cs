using System;

namespace Nextbit.JadeRealState.Services.Features.GeneralInfo
{
    public interface IInformationDomainService : IDisposable
    {
        Information Create(InformationRequest request);
        Information Update(InformationRequest request, Information _oldRegister);
        
    }
}