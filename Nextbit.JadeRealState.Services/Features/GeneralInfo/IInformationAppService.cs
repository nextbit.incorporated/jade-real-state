using System;

namespace Nextbit.JadeRealState.Services.Features.GeneralInfo
{
    public interface IInformationAppService : IDisposable
    {
        InformationDto Get();
        InformationDto Create(InformationRequest request);
        InformationDto Update(InformationRequest request);
        string Delete(int id);
    }
}