using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.GeneralInfo
{
    public class InformationDto : ResponseBase
    {
        public string Empresa { get; set; }
        public string Direccion { get; set; }
        public string Contacto { get; set; }
        public decimal Impuesto { get; set; }
        public string Rtn { get; set; }
        public string RazonSocial { get; set; }
        public string Cai { get; set; }
        public string RangoFacturaInicio { get; set; }
        public string RangoFacturaFinal { get; set; }
        public DateTime FechaLimiteEmision { get; set; }
        public string UltimaFacturaGenerada { get; set; }
        public int UltimoCorrelativoUtilizado { get; set; }


        public static List<InformationDto> FromList(IEnumerable<Information> detail)
        {
            if (detail == null || !detail.Any()) return new List<InformationDto>();
            return (from qry in detail
                    select From(qry)).ToList();
        }

        public static InformationDto From(Information s)
        {
            if (s == null) return new InformationDto();

            return new InformationDto
            {
                Id = s.Id,
                Empresa = s.Empresa,
                Direccion = s.Direccion,
                Contacto = s.Contacto,
                Impuesto = s.Impuesto,
                Rtn = s.Rtn,
                RazonSocial = s.RazonSocial,
                Cai = s.Cai,
                RangoFacturaInicio = s.RangoFacturaInicio,
                RangoFacturaFinal = s.RangoFacturaFinal,
                FechaLimiteEmision = s.FechaLimiteEmision,
                UltimaFacturaGenerada = s.UltimaFacturaGenerada,
                UltimoCorrelativoUtilizado = s.UltimoCorrelativoUtilizado
            };
        }
    }
}