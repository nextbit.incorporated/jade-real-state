using System;
using System.Linq;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.GeneralInfo
{
    public class InformationAppService : IInformationAppService
    {
        private RealStateContext _context;
        private readonly IInformationDomainService _parametroDomainService;
        public InformationAppService(RealStateContext context, IInformationDomainService parametroDomainService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _parametroDomainService = parametroDomainService ?? throw new ArgumentException(nameof(parametroDomainService));
        }
        public InformationDto Create(InformationRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newRow = _parametroDomainService.Create(request);

            _context.GeneralInformation.Add(newRow);
            _context.SaveChanges();

            return InformationDto.From(newRow);
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var row = _context.GeneralInformation.FirstOrDefault(s => s.Id == id);
            if (row == null) throw new Exception("Error al intentar obtener puesto");
            _context.GeneralInformation.Remove(row);
            _context.SaveChanges();

            return string.Empty;
        }

        public InformationDto Get()
        {
            var result = _context.GeneralInformation.FirstOrDefault();
            if (result == null) return new InformationDto();

            return InformationDto.From(result);
        }

        public InformationDto Update(InformationRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRowInfo = _context.GeneralInformation.FirstOrDefault(s => s.Id == request.Id);
            if (oldRowInfo == null) return new InformationDto { ValidationErrorMessage = "Error al obtener info de los puestos" };

            var rolUpdate = _parametroDomainService.Update(request, oldRowInfo);

            _context.GeneralInformation.Update(oldRowInfo);
            _context.SaveChanges();

            return InformationDto.From(rolUpdate);
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_parametroDomainService != null) _parametroDomainService.Dispose();
        }
    }
}