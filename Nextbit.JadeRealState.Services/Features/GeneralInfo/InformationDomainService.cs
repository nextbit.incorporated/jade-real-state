using System;

namespace Nextbit.JadeRealState.Services.Features.GeneralInfo
{
    public class InformationDomainService : IInformationDomainService
    {
        public Information Create(InformationRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Empresa)) throw new ArgumentNullException(nameof(request.Empresa));

            Information registro = new Information.Builder()
            .ConEmpresa(
                request.Empresa, request.Direccion, request.Contacto,
                request.Impuesto, request.Rtn, request.RazonSocial, request.Cai,
                request.RangoFacturaInicio, request.RangoFacturaFinal, request.FechaLimiteEmision,
                request.UltimaFacturaGenerada, request.UltimoCorrelativoUtilizado)
            .WithAuditFields(request.User)
            .Build();

            return registro;
        }

        public Information Update(InformationRequest request, Information _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(
                request.Empresa, request.Direccion, request.Contacto,
                request.Impuesto, request.Rtn, request.RazonSocial,
                request.Cai, request.RangoFacturaInicio,
                request.RangoFacturaFinal, request.FechaLimiteEmision,
                request.UltimaFacturaGenerada, request.UltimoCorrelativoUtilizado, request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }

    }
}