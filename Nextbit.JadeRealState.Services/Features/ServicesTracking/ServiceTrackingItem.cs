using System;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.ServicesTracking
{
    public sealed class ServiceTrackingItem : Entity
    {
        private ServiceTrackingItem()
        {

        }

        public int ServiceTrackingId { get; private set; }
        public int? SalesAdvisorId { get; private set; }
        public int Order { get; private set; }
        public string Name { get; private set; }
        public string Comments { get; private set; }
        public string Status { get; private set; }
        public DateTime? CompletedDate { get; private set; }

        public DateTime? ReminderDate { get; private set; }

        public ServiceTracking ServiceTracking { get; set; }
        public User SalesAdvisor { get; set; }

        public class Builder
        {
            private readonly ServiceTrackingItem _serviceTrackingItem = new ServiceTrackingItem();

            public Builder WithServiceTracking(ServiceTracking serviceTracking)
            {
                _serviceTrackingItem.ServiceTracking = serviceTracking;
                _serviceTrackingItem.ServiceTrackingId = serviceTracking.Id;
                return this;
            }

            public Builder WithSalesAdvisor(User salesAdvisor)
            {
                _serviceTrackingItem.SalesAdvisor = salesAdvisor;
                _serviceTrackingItem.SalesAdvisorId = salesAdvisor?.Id;
                return this;
            }

            public Builder WithOrder(int order)
            {
                _serviceTrackingItem.Order = order;
                return this;
            }

            public Builder WithName(string name)
            {
                _serviceTrackingItem.Name = name;
                return this;
            }

            public Builder WithComments(string comments)
            {
                _serviceTrackingItem.Comments = comments;
                return this;
            }
            public Builder WithStatus(string status)
            {
                _serviceTrackingItem.Status = status;
                return this;
            }

            public Builder WithCompletedDate(DateTime? completedDate)
            {
                _serviceTrackingItem.CompletedDate = completedDate;
                return this;
            }

            public Builder WithReminderDate(DateTime reminderDate)
            {
                _serviceTrackingItem.ReminderDate = reminderDate;
                return this;
            }

            public ServiceTrackingItem Build()
            {
                return _serviceTrackingItem;
            }
        }

    }
}