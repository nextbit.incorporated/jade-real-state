using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.SupplierServices;

namespace Nextbit.JadeRealState.Services.Features.ServicesTracking
{
    [Table("ServicesTracking")]
    public sealed class ServiceTracking : Entity
    {
        private ServiceTracking()
        {
            ServiceTrackingItems = new HashSet<ServiceTrackingItem>();
        }

        public int ClientId { get; private set; }
        public int SupplierServiceId { get; private set; }
        public string Comments { get; private set; }
        public string Status { get; private set; }

        public Client Client { get; set; }
        public SupplierService SupplierService { get; set; }


        public ICollection<ServiceTrackingItem> ServiceTrackingItems
        {
            get { return _serviceTrackingItems ?? (_serviceTrackingItems = new HashSet<ServiceTrackingItem>()); }
            private set { _serviceTrackingItems = value; }
        }
        private ICollection<ServiceTrackingItem> _serviceTrackingItems;

        public class Builder
        {
            private readonly ServiceTracking _serviceTracking = new ServiceTracking();

            public Builder WithClient(Client client)
            {
                _serviceTracking.Client = client;
                _serviceTracking.ClientId = client.Id;
                return this;
            }

            public Builder WithSupplierService(SupplierService supplierService)
            {
                _serviceTracking.SupplierService = supplierService;
                _serviceTracking.SupplierServiceId = supplierService.Id;
                return this;
            }

            public Builder WithComments(string comments)
            {
                _serviceTracking.Comments = comments;
                return this;
            }
            public Builder WithStatus(string status)
            {
                _serviceTracking.Status = status;
                return this;
            }

            public Builder WithCreationDate(DateTime creationDate)
            {
                _serviceTracking.CreationDate = creationDate;
                return this;
            }

            public ServiceTracking Build()
            {
                return _serviceTracking;
            }
        }
    }
}