using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Features.Builders;
using Nextbit.JadeRealState.Services.Features.ClientsLevel;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.FinancialInstitutions;
using Nextbit.JadeRealState.Services.Features.FundTypes;
using Nextbit.JadeRealState.Services.Features.Origins;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.SupplierServices;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.ApplicationOptions
{
    public sealed class ApplicationOptionsContainer
    {
        public List<ContactTypeDto> ContactTypes { get; set; } = new List<ContactTypeDto>();
        public List<BuilderCompanyDTO> Builders { get; set; } = new List<BuilderCompanyDTO>();
        public List<FinancialInstitutionDto> FinancialInstitutions { get; set; } = new List<FinancialInstitutionDto>();
        public List<OriginDTO> Nationalities { get; set; } = new List<OriginDTO>();
        public List<ProjectsDTO> Projects { get; set; } = new List<ProjectsDTO>();
        public List<SupplierServiceDTO> SupplierServices { get; set; } = new List<SupplierServiceDTO>();
        public List<UserDTO> SalesAdvisors { get; set; } = new List<UserDTO>();
        public List<ClientLevelDto> ClientCategories { get; set; } = new List<ClientLevelDto>();
        public List<FundTypeDto> FundTypes { get; set; } = new List<FundTypeDto>();
        public string InfoClientLevel { get; set; }
        
        

    }
}