using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.Builders;
using Nextbit.JadeRealState.Services.Features.ClientsLevel;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.FinancialInstitutions;
using Nextbit.JadeRealState.Services.Features.FundTypes;
using Nextbit.JadeRealState.Services.Features.Origins;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.SupplierServices;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.ApplicationOptions
{
    public sealed class ApplicationOptionsAppService : BaseDisposable
    {
        private readonly ContactTypesAppService _contactTypesAppService;
        private readonly IFinancialInstitutionAppService _financialInstitutionAppService;
        private readonly IOriginAppService _nationalitiesAppService;
        private readonly IProjectsAppService _projectsAppService;
        private readonly ISupplierServiceAppService _supplierServiceAppService;
        private readonly IUserAppService _userAppService;
        private readonly IClientLevelAppService _clientLevelAppService;
        private readonly RealStateContext _context;

        public ApplicationOptionsAppService(
            ContactTypesAppService contactTypesAppService,
            IFinancialInstitutionAppService financialInstitutionAppService,
            IOriginAppService nationalitiesAppService,
            IProjectsAppService projectsAppService,
            ISupplierServiceAppService supplierServiceAppService,
            IUserAppService userAppService,
            IClientLevelAppService clientLevelAppService,
            RealStateContext context)
        {
            if (contactTypesAppService == null) throw new ArgumentNullException(nameof(contactTypesAppService));
            if (financialInstitutionAppService == null) throw new ArgumentNullException(nameof(financialInstitutionAppService));
            if (nationalitiesAppService == null) throw new ArgumentNullException(nameof(nationalitiesAppService));
            if (projectsAppService == null) throw new ArgumentException(nameof(projectsAppService));
            if (supplierServiceAppService == null) throw new ArgumentException(nameof(supplierServiceAppService));
            if (userAppService == null) throw new ArgumentNullException(nameof(userAppService));
            if (clientLevelAppService == null) throw new ArgumentNullException(nameof(clientLevelAppService));
            if (context == null) throw new ArgumentNullException(nameof(context));

            _contactTypesAppService = contactTypesAppService;
            _financialInstitutionAppService = financialInstitutionAppService;
            _nationalitiesAppService = nationalitiesAppService;
            _projectsAppService = projectsAppService;
            _supplierServiceAppService = supplierServiceAppService;
            _userAppService = userAppService;
            _clientLevelAppService = clientLevelAppService;
            _context = context;
        }

        internal async Task<ApplicationOptionsContainer> GetApplicationOptionsContainerAsync()
        {
            List<FundType> fundTypes = await _context.FundTypes.ToListAsync();
            List<BuilderCompany> builders = await _context.BuilderCompany.ToListAsync();

            var response = new ApplicationOptionsContainer
            {
                ContactTypes = await _contactTypesAppService.GetContactTypesAsync(),
                FinancialInstitutions = await _financialInstitutionAppService.GetFinancialInstitutionsAsync(),
                Nationalities = await _nationalitiesAppService.GetNationalitiesAsync(),
                Projects = await _projectsAppService.GetProjectsAsync(),
                SupplierServices = await _supplierServiceAppService.GetSupplierServicesAsync(),
                SalesAdvisors = UserDTO.From(await _userAppService.GetUsersAsync()),
                ClientCategories = await _clientLevelAppService.GetLevelsAsync(),
                FundTypes = FundTypeDto.From(fundTypes),
                Builders = BuilderCompanyDTO.From(builders)

               
            };

                var informacion = "\n";
             foreach (var item in response.ClientCategories.OrderBy(s => s.Id))
            {
                 informacion += item.PerfilCategoria + " \n";
            }

            response.InfoClientLevel = informacion;

            return response;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Release managed resources.
                _contactTypesAppService?.Dispose();
                _financialInstitutionAppService?.Dispose();
                _nationalitiesAppService?.Dispose();
                _projectsAppService?.Dispose();
                _supplierServiceAppService?.Dispose();
                _userAppService?.Dispose();
                _clientLevelAppService?.Dispose();
            }

            // Release unmanaged resources.
            // Set large fields to null.
            // Call Dispose on your base class.
            base.Dispose(disposing);
        }
    }
}