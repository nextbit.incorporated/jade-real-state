using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.ApplicationOptions
{
    [Route(@"api/application-options")]
    [ApiController]
    public sealed class ApplicationOptionsController : ControllerBase
    {
        private readonly ApplicationOptionsAppService _applicationOptionsAppService;
        public ApplicationOptionsController(
            ApplicationOptionsAppService applicationOptionsAppService)
        {
            if (applicationOptionsAppService == null) throw new ArgumentNullException(nameof(applicationOptionsAppService));

            _applicationOptionsAppService = applicationOptionsAppService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetApplicationOptionsContainerAsync()
        {
            ApplicationOptionsContainer applicationOptionsContainer = await _applicationOptionsAppService
                .GetApplicationOptionsContainerAsync();

            return Ok(applicationOptionsContainer);
        }


    }
}