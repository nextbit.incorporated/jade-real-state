using System;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsByBank
{
    public class ClientByBankRequest
    {
        public int IdBank { get; set; }
        public int SalesAdvisor { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}