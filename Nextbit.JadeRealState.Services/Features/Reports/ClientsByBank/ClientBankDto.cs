using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Negotiations;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsByBank
{
    public class ClientBankDto : ResponseBase
    {
        public int IdClient { get; set; }
        public string ClientCode { get; set; }
        public string IdentificationCard { get; set; }
        public string ClientName { get; set; }
        public int IdNegotiation { get; set; }
        public DateTime? NegotiationStartDate { get; set; }
        public string SalesAdvisor { get; set; }
        public int IdBank { get; set; }
        public string Bank { get; set; }

        internal static List<ClientBankDto> FromRegisters(IEnumerable<Negotiation> detail)
        {
            if (detail == null || !detail.Any()) return new List<ClientBankDto>();
            return (from qry in detail select From(qry)).ToList();
        }

        private static ClientBankDto From(Negotiation qry)
        {

            if (qry == null) return new ClientBankDto();



            return new ClientBankDto
            {
                IdClient = qry.NegotiationClients?.FirstOrDefault(n => n.Titular)?.Client?.Id ?? 0,
                ClientCode = qry.NegotiationClients?.FirstOrDefault(n => n.Titular)?.Client?.ClientCode,
                IdentificationCard = EncriptorHelper.DecryptString(qry.NegotiationClients?.FirstOrDefault(n => n.Titular)?.Client?.IdentificationCard),
                ClientName = qry.NegotiationClients?.FirstOrDefault(n => n.Titular)?.Client?.GetCompleteName() ?? string.Empty,
                IdNegotiation = qry.Id,
                NegotiationStartDate = qry.NegotiationStartDate,
                SalesAdvisor = qry.SalesAdvisor == null ? "" : qry.SalesAdvisor.UserId,
                IdBank = qry.FinancialInstitution == null ? 0 : qry.FinancialInstitution.Id,
                Bank = qry.FinancialInstitution == null ? "" : qry.FinancialInstitution.Name
            };
        }
    }
}