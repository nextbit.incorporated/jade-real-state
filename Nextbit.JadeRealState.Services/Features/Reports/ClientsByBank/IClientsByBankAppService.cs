using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsByBank
{
    public interface IClientsByBankAppService : IDisposable
    {
       List<ClientBankDto> GetReportClientByBank(ClientByBankRequest request);
    }
}