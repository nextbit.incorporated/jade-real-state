using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsByBank
{
    public class ClientsByBankAppService : IClientsByBankAppService
    {
        private readonly RealStateContext _context;
        public ClientsByBankAppService(RealStateContext context)
        {
            _context = context ??
                throw new ArgumentNullException(nameof(context));
        }
        public List<ClientBankDto> GetReportClientByBank(ClientByBankRequest request)
        {

            if (request.StartDate == null || request.EndDate == null)
            {
                if (request.IdBank > 0 && request.SalesAdvisor == 0)
                {
                    return GetNegotiationReportByBank(request.IdBank);
                }

                if (request.SalesAdvisor > 0 && request.IdBank == 0)
                {
                    return GetNegotiationReportBySalesAdvisor(request.SalesAdvisor);
                }

                if (request.IdBank > 0 && request.SalesAdvisor > 0)
                {
                    return GetNegotiationReportByIdBankAndSalesAdvisor(request.IdBank, request.SalesAdvisor);
                }

                return GetCompleteReport();
            }
            if (request.StartDate != null && request.EndDate != null
                    && request.StartDate != DateTime.MinValue && request.StartDate != DateTime.MaxValue
                    && request.EndDate != DateTime.MinValue && request.EndDate != DateTime.MaxValue)
            {
                if (request.IdBank > 0 && request.SalesAdvisor == 0)
                {
                    return GetNegotiationReportByBankBetweenDates(request);
                }

                if (request.SalesAdvisor > 0 && request.IdBank == 0)
                {
                    return GetNegotiationReportBySalesAdvisorDate(request);
                }

                if (request.IdBank > 0 && request.SalesAdvisor > 0)
                {
                    return GetNegotiationReportByIdBankAndSalesAdvisorDates(request);
                }

                return GetCompleteReportDates(request);
            }

            return GetCompleteReport();
        }

        private List<ClientBankDto> GetCompleteReportDates(ClientByBankRequest request)
        {
            var negotiations = _context.Negotiations
             .Where(s => s.NegotiationStartDate >= request.StartDate
                    && s.NegotiationStartDate <= request.EndDate)
             .Include(x => x.NegotiationClients).ThenInclude(t => t.Client)
             .Include(t => t.FinancialInstitution)
             .Include(t => t.SalesAdvisor)
             .OrderByDescending(s => s.CreationDate);

            return ClientBankDto.FromRegisters(negotiations);
        }

        private List<ClientBankDto> GetNegotiationReportByIdBankAndSalesAdvisorDates(ClientByBankRequest request)
        {
            var negotiations = _context.Negotiations
             .Where(s => s.SalesAdvisorId == request.SalesAdvisor
                    && s.FinancialInstitutionId == request.IdBank
                    && s.NegotiationStartDate <= request.StartDate
                    && s.NegotiationStartDate >= request.EndDate)
             .Include(x => x.NegotiationClients).ThenInclude(t => t.Client)
             .Include(t => t.FinancialInstitution)
             .Include(t => t.SalesAdvisor)
             .OrderByDescending(s => s.CreationDate);

            return ClientBankDto.FromRegisters(negotiations);
        }

        private List<ClientBankDto> GetNegotiationReportBySalesAdvisorDate(ClientByBankRequest request)
        {
            var negotiations = _context.Negotiations
              .Where(s => s.SalesAdvisorId == request.SalesAdvisor
                     && s.NegotiationStartDate >= request.StartDate
                     && s.NegotiationStartDate <= request.EndDate)
              .Include(x => x.NegotiationClients).ThenInclude(t => t.Client)
              .Include(t => t.FinancialInstitution)
              .Include(t => t.SalesAdvisor)
              .OrderByDescending(s => s.CreationDate);

            return ClientBankDto.FromRegisters(negotiations);
        }

        private List<ClientBankDto> GetNegotiationReportByBankBetweenDates(ClientByBankRequest request)
        {
            var negotiations = _context.Negotiations
               .Where(s => s.FinancialInstitutionId == request.IdBank
                      && s.NegotiationStartDate <= request.StartDate
                      && s.NegotiationStartDate >= request.EndDate)
               .Include(x => x.NegotiationClients).ThenInclude(t => t.Client)
               .Include(t => t.FinancialInstitution)
               .Include(t => t.SalesAdvisor)
               .OrderByDescending(s => s.CreationDate);

            return ClientBankDto.FromRegisters(negotiations);
        }

        private List<ClientBankDto> GetNegotiationReportByIdBankAndSalesAdvisor(int idBank, int salesAdvisor)
        {
            var negotiations = _context.Negotiations
                .Where(s => s.SalesAdvisorId == salesAdvisor
                           && s.FinancialInstitutionId == idBank)
                .Include(x => x.NegotiationClients).ThenInclude(t => t.Client)
                .Include(t => t.FinancialInstitution)
                .Include(t => t.SalesAdvisor)
                .OrderByDescending(s => s.CreationDate);

            return ClientBankDto.FromRegisters(negotiations);
        }

        private List<ClientBankDto> GetNegotiationReportBySalesAdvisor(int salesAdvisor)
        {
            var negotiations = _context.Negotiations
                .Where(s => s.SalesAdvisorId == salesAdvisor)
                .Include(x => x.NegotiationClients).ThenInclude(t => t.Client)
                .Include(t => t.FinancialInstitution)
                .Include(t => t.SalesAdvisor)
                .OrderByDescending(s => s.CreationDate);

            return ClientBankDto.FromRegisters(negotiations);
        }

        private List<ClientBankDto> GetNegotiationReportByBank(int idBank)
        {
            var negotiations = _context.Negotiations
                .Where(s => s.FinancialInstitutionId == idBank)
                .Include(x => x.NegotiationClients).ThenInclude(t => t.Client)
                .Include(t => t.FinancialInstitution)
                .Include(t => t.SalesAdvisor)
                .OrderByDescending(s => s.CreationDate);

            return ClientBankDto.FromRegisters(negotiations);
        }

        private List<ClientBankDto> GetCompleteReport()
        {
            var negotiations = _context.Negotiations
                .Include(x => x.NegotiationClients).ThenInclude(t => t.Client)
                .Include(t => t.FinancialInstitution)
                .Include(t => t.SalesAdvisor)
                .OrderByDescending(s => s.CreationDate);

            return ClientBankDto.FromRegisters(negotiations);

        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
        }
    }
}