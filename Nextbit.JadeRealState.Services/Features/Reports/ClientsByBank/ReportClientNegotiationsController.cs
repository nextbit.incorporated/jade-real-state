using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsByBank 
{
    [Route ("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReportClientNegotiationsController : ControllerBase 
    {
        private readonly IClientsByBankAppService _clientsByBankAppService;
        public ReportClientNegotiationsController (IClientsByBankAppService clientsByBankAppService) 
        {
            _clientsByBankAppService = clientsByBankAppService ??
                throw new ArgumentNullException (nameof (clientsByBankAppService));
        }

        [HttpGet]
        [Route("")]
        [Authorize]
        public ActionResult<IEnumerable<ClientBankDto>> Get([FromQuery] ClientByBankRequest request)
        {
            var result = _clientsByBankAppService.GetReportClientByBank(request);
            return Ok(result);
        }
    }
}