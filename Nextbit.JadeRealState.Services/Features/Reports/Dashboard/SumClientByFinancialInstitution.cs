namespace Nextbit.JadeRealState.Services.Features.Reports.Dashboard
{
    public class SumClientByFinancialInstitution
    {
        public string FinancialInstitution { get; set; }
        public int? FinancialInstitutionId { get; set; }
        public int SumClients { get; set; }
    }
}