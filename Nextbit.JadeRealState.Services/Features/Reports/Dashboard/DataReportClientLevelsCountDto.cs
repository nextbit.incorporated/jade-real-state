using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reports.Dashboard
{
    public class DataReportClientLevelsCountDto : ResponseBase
    {
        public List<string> Pojects { get; set; } 
        public List<string> Levels { get; set; }        
        public List<int> Count { get; set; }
    }
}