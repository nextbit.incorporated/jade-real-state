namespace Nextbit.JadeRealState.Services.Features.Reports.Dashboard
{
    public class NegotiationsBySalesAdvisor
    {
        public string SalesAdvisor { get; set; }
        public int CountNegotiations { get; set; }
    }
}