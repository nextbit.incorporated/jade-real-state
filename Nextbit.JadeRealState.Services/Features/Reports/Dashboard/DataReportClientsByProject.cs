using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reports.Dashboard
{
    public class DataReportClientsByProject : ResponseBase
    {
            public List<string> Projects { get; set; }
            public List<int> CountClients { get; set; }
    }
}