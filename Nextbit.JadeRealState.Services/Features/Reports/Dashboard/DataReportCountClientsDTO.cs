using System;
using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reports.Dashboard
{
    public class DataReportCountClientsDTO : ResponseBase
    {
        public List<DateTime> Fechas { get; set; }
        public List<int> CountClients { get; set; }
    }
}