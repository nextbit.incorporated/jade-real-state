using System;
using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reports.Dashboard
{
    public class SumClientByDateDTO : ResponseBase
    {
        public DateTime Fecha { get; set; }
        public int ConteoClientes { get; set; }
     
    }
}