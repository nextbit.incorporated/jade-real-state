using Nextbit.JadeRealState.Services.Core;
using System.Collections.Generic;
namespace Nextbit.JadeRealState.Services.Features.Reports.Dashboard
{
    public class SumNegotationsBySalesAdvisor : ResponseBase
    {
         public List<string> SalesAdvisor { get; set; }
         public List<int> CountNegotiations { get; set; }
    }
}