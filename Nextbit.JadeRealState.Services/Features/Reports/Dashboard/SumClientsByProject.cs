using System;

namespace Nextbit.JadeRealState.Services.Features.Reports.Dashboard
{
    public class SumClientsByProject
    {
        public int IdProyecto { get; set; }
        public string NombreProyecto { get; set; }
        public int ConteoClientes { get; set; }
    }
}