using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.Reports.Dashboard
{
    public class SumClientByFinancialInstitutionDto : ResponseBase
    {
        public string FinancialInstitution { get; set; }
        public int? FinancialInstitutionId { get; set; }
        public int SumClients { get; set; }

        public List<string> FinancialInstitutions { get; set; }
        public List<int> CountClients { get; set; }
    }
}