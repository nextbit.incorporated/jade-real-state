using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.Reports.Negotiations
{
    public class NegotiationProcessReportDto : ResponseBase
    {
        public List<string> Negotiations { get; set; }
        public List<int> CountNegotiations { get; set; }

        public int Opens { get; set; }
        public int Close { get; set; }
    }
}