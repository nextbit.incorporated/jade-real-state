using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsByProjectReport
{
    public class ClientsByProjectRequest : RequestBase
    {
        public int ProjectId { get; set; }
    }
}