using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsByProjectReport
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ClientsByProjectController : ControllerBase
    {
        private readonly IClientsByProjectReportAppService _clientsByProjectReportAppService;
        public ClientsByProjectController(IClientsByProjectReportAppService clientsByProjectReportAppService)
        {
            _clientsByProjectReportAppService = clientsByProjectReportAppService ?? throw new ArgumentNullException(nameof(clientsByProjectReportAppService));
        }

        [HttpGet]
        // [Route("alarms")]
        // [AllowAnonymous]
        [Authorize]
        public ActionResult<List<ClientByProjectDto>> Get([FromQuery] ClientsByProjectRequest request)
        {
            var result = _clientsByProjectReportAppService.GetReport(request);

            return Ok(result);
        }
    }
}   