using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsByProjectReport
{
    public interface IClientsByProjectReportAppService : IDisposable
    {
       List<ClientByProjectDto> GetReport(ClientsByProjectRequest request);
    }
}