using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsByProjectReport
{
    public class ClientByProjectDto : ResponseBase 
    {
        public int ClientId { get; set; }
        public string ClientCode { get; set; }  
        public string Client { get; set; }
        public int ProjectId { get; set; }
        public string Project { get; set; }
        public string Descripcion { get; set; }
    }
}