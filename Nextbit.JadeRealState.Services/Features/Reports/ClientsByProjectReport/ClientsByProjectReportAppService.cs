using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsByProjectReport
{
    public class ClientsByProjectReportAppService : IClientsByProjectReportAppService
    {
        private readonly RealStateContext _context;
        public ClientsByProjectReportAppService(RealStateContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public List<ClientByProjectDto> GetReport(ClientsByProjectRequest request)
        {
            if(request == null)
            {
                return GetAllData();
            }
            if(request.ProjectId == 0)
            {
                return GetAllData();
            }

            return GetAllDataByproject(request);
        }

        private List<ClientByProjectDto> GetAllData()
        {
            var data = _context.Negotiations
                    .Include(c => c.NegotiationClients).ThenInclude(x => x.Client)
                    .Include(n => n.Project).ToList();
            
            return  data.Select(item => new ClientByProjectDto
            {
                ClientId = item.NegotiationClients.FirstOrDefault().ClientId,
                ClientCode = item.NegotiationClients.FirstOrDefault().Client.ClientCode,
                Client = item.NegotiationClients.FirstOrDefault().Client.GetName(),
                ProjectId = item.Project.Id,
                Project = item.Project.Name
            }).ToList();
        }

        private List<ClientByProjectDto> GetAllDataByproject(ClientsByProjectRequest request)
        {
            var data = _context.Negotiations.Where(s => s.ProjectId == request.ProjectId)
                    .Include(c => c.NegotiationClients).ThenInclude(x => x.Client)
                    .Include(n => n.Project).ToList();

            return data.Select(item => new ClientByProjectDto
            {
                ClientId = item.NegotiationClients.FirstOrDefault().ClientId,
                ClientCode = item.NegotiationClients.FirstOrDefault().Client.ClientCode,
                Client = item.NegotiationClients.FirstOrDefault().Client.GetName(),
                ProjectId = item.Project.Id,
                Project = item.Project.Name
            }).ToList();
        }

        public void Dispose()
        {
            if(_context != null) _context.Dispose();
        }
    }
}