using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsAlarms
{
    public interface IClientAlarmReportAppService : IDisposable
    {
        List<ClientAlarmDto> GetReport();
    }
}