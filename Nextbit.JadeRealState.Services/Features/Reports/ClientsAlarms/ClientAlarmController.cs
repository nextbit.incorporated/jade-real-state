using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsAlarms
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ClientAlarmController : ControllerBase 
    {
        private readonly IClientAlarmReportAppService _clientAlarmReportAppService;
        public ClientAlarmController(IClientAlarmReportAppService clientAlarmReportAppService)
        {
            _clientAlarmReportAppService = clientAlarmReportAppService ?? throw new ArgumentNullException(nameof(clientAlarmReportAppService));
        }

        [HttpGet]
        // [Route("alarms")]
       //[AllowAnonymous]
         [Authorize]
        public ActionResult<List<ClientAlarmDto>> Get()
        {
            var result = _clientAlarmReportAppService.GetReport();

            return Ok(result);
        }
    }
}