using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.Alarms;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsAlarms
{
    public class ClientAlarmReportAppService : IClientAlarmReportAppService
    {
        private readonly RealStateContext _context;
        public ClientAlarmReportAppService(RealStateContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public List<ClientAlarmDto> GetReport()
        {
            var clientList = _context.Clients
                                .Include(s => s.Alarms)
                                .Include(s => s.SalesAdvisor).ToList();

           var clienWithAlarms  = clientList.Where(s => s.Alarms.Any()).ToList();

           var listResult = new List<ClientAlarmDto>();
           foreach (var item in clienWithAlarms)
           {
               foreach (var itemAlarm in item.Alarms)
               {
                   var cliente = new ClientAlarmDto
                   {
                       ClientId = item.Id,
                       ClientCode = item.ClientCode ?? string.Empty,
                       ClientName = item.GetCompleteName(),
                       AlarmNotification = itemAlarm.Information,
                       Date = itemAlarm.CreationDate,
                       NumberMonths = itemAlarm.MonthsNumber,
                       UserId = item.SalesAdvisor == null ? string.Empty : item.SalesAdvisor.GetName(),
                       FechaCumplimiento = GetDateRelease(itemAlarm)
                   };
                   listResult.Add(cliente);
               }
           }

           return listResult.OrderByDescending(s => s.FechaCumplimiento).ToList();
        }

        private DateTime? GetDateRelease(Alarm itemAlarm)
        {
            
            var dateCreation = itemAlarm.CreationDate;
            var NumberMonths = itemAlarm.MonthsNumber;
             var newdate = dateCreation.AddMonths(NumberMonths);
             return newdate;
        }

        public void Dispose()
        {
            if(_context != null) _context.Dispose();
        }
    }
}