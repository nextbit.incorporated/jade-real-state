using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reports.ClientsAlarms
{
    public class ClientAlarmDto : ResponseBase
    {
        public int ClientId { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string AlarmNotification { get; set; }
        public DateTime Date { get; set; }
        public int NumberMonths { get; set; }
        public string UserId { get; set; }
        public DateTime? FechaCumplimiento { get; set; }
    }
}