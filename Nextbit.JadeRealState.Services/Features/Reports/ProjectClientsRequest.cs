using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reports
{
    public class ProjectClientsRequest : RequestBase
    {
       public int ProjectId { get; set; } 
    }
}