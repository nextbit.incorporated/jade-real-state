using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reports.ProjectClients
{
    public class ProjectClientsDTO : ResponseBase
    {
        public List<ProjectClientDTO> ProjectClients { get; set; }
    }
}