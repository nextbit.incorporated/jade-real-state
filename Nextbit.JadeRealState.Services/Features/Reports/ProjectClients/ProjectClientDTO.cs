using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reports.ProjectClients
{
    public class ProjectClientDTO : ResponseBase
    {
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string IdentificationCard { get; set; }
        public string CelNumber { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
    }
}