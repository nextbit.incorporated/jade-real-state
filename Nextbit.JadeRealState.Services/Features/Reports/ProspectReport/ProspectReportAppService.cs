using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.Prospects;

namespace Nextbit.JadeRealState.Services.Features.Reports.ProspectReport 
{
    public class ProspectReportAppService : IProspectReportAppService 
    {
        private readonly RealStateContext _context;

        public ProspectReportAppService (RealStateContext context) 
        {
            _context = context ??
                throw new ArgumentNullException (nameof (context));
        }

        public List<ProspectDTO> GetReport (ProspectReportRequest request) 
        {
            if (request == null) throw new ArgumentException (nameof (request));
            if (request.StartDate == DateTime.MinValue || request.StartDate == DateTime.MinValue) return new List<ProspectDTO> ();
            if (request.FinalDate == DateTime.MinValue || request.FinalDate == DateTime.MaxValue) return new List<ProspectDTO> ();

            if (string.IsNullOrEmpty (request.User)) return new List<ProspectDTO> ();
            var usuario = _context.Users.FirstOrDefault (s => s.UserId == request.User);
            if (usuario == null) return new List<ProspectDTO> ();
            var usuarioRoles = _context.UserRol.FirstOrDefault (s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<ProspectDTO> ();

            if (usuarioRoles.RolId != 3) 
            {
                var prospectList = _context.Prospects
                    .Where (s => s.CreationDate >= request.StartDate &&
                        s.CreationDate <= request.FinalDate)
                    .Include (c => c.SalesAdvisor)
                    .Include (c => c.Project)
                    .Include (c => c.ContactTypes)
                    .OrderByDescending (s => s.TransactionDate).ToList ();

                return ProspectDTO.From (prospectList).ToList ();
            } else 
            {
                var prospectList = _context.Prospects
                    .Where (s => s.CreationDate >= request.StartDate &&
                        s.CreationDate <= request.FinalDate &&
                        s.SalesAdvisorId == usuario.Id)
                    .Include (c => c.SalesAdvisor)
                    .Include (c => c.Project)
                    .Include (c => c.ContactTypes)
                    .OrderByDescending (s => s.TransactionDate).ToList ();

                return ProspectDTO.From (prospectList).ToList ();
            }

        }

        public List<ProspectDTO> GetReportByProcess (ProspectReportRequest request) 
        {
            if (request == null) throw new ArgumentException (nameof (request));
            if (request.ProcessId == 0) throw new ArgumentNullException (nameof (request.ProcessId));
            if (string.IsNullOrEmpty (request.User)) return new List<ProspectDTO> ();
            var usuario = _context.Users.FirstOrDefault (s => s.UserId == request.User);
            if (usuario == null) return new List<ProspectDTO> ();
            var usuarioRoles = _context.UserRol.FirstOrDefault (s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<ProspectDTO> ();

            if (usuarioRoles.RolId != 3) 
            {
                var prospectList = _context.Prospects
                    .Where (s => s.ProjectId == request.ProcessId)
                    .Include (c => c.SalesAdvisor)
                    .Include (c => c.Project)
                    .Include (c => c.ContactTypes)
                    .OrderByDescending (s => s.TransactionDate).ToList ();

                return ProspectDTO.From (prospectList).ToList ();
            } else 
            {
                var prospectList = _context.Prospects
                    .Where (s => s.ProjectId == request.ProcessId && s.SalesAdvisorId == usuario.Id)
                    .Include (c => c.SalesAdvisor)
                    .Include (c => c.Project)
                    .Include (c => c.ContactTypes)
                    .OrderByDescending (s => s.TransactionDate).ToList ();

                return ProspectDTO.From (prospectList).ToList ();
            }

        }

        public List<ProspectDTO> GetProspectByContactTypeReport(ProspectReportRequest request)
        {
            if (request == null) throw new ArgumentException (nameof (request));
            if (string.IsNullOrEmpty (request.User)) return new List<ProspectDTO> ();
            var usuario = _context.Users.FirstOrDefault (s => s.UserId == request.User);
            if (usuario == null) return new List<ProspectDTO> ();
            var usuarioRoles = _context.UserRol.FirstOrDefault (s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<ProspectDTO> ();
            if (request.StartDate == DateTime.MinValue || request.StartDate == DateTime.MinValue) return new List<ProspectDTO> ();
            if (request.FinalDate == DateTime.MinValue || request.FinalDate == DateTime.MaxValue) return new List<ProspectDTO> ();
            
            if (usuarioRoles.RolId != 3) 
            {    
                    var prospectList = _context.Prospects
                        .Where (s => s.ContactType == request.ContactType && s.CreationDate >= request.StartDate &&
                            s.CreationDate <= request.FinalDate)
                        .Include (c => c.SalesAdvisor)
                        .Include (c => c.Project)
                        .Include (c => c.ContactTypes)
                        .OrderByDescending (s => s.TransactionDate).ToList ();

                    return ProspectDTO.From (prospectList).ToList ();
            }
            else 
            {
                var prospectList = _context.Prospects
                        .Where (s => s.ContactType == request.ContactType && s.SalesAdvisorId == usuario.Id && s.CreationDate >= request.StartDate &&
                            s.CreationDate <= request.FinalDate)
                        .Include (c => c.SalesAdvisor)
                        .Include (c => c.Project)
                        .Include (c => c.ContactTypes)
                        .OrderByDescending (s => s.TransactionDate).ToList ();

                    return ProspectDTO.From (prospectList).ToList ();
            }
        }

        public List<ProspectDTO> GetReportBySalesAdvisorId (ProspectReportRequest request) 
        {
            if (request == null) throw new ArgumentException (nameof (request));
            if (string.IsNullOrEmpty (request.User)) return new List<ProspectDTO> ();
            var usuario = _context.Users.FirstOrDefault (s => s.UserId == request.User);
            if (usuario == null) return new List<ProspectDTO> ();
            var usuarioRoles = _context.UserRol.FirstOrDefault (s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<ProspectDTO> ();
            if (request.StartDate == DateTime.MinValue || request.StartDate == DateTime.MinValue) return new List<ProspectDTO> ();
            if (request.FinalDate == DateTime.MinValue || request.FinalDate == DateTime.MaxValue) return new List<ProspectDTO> ();

            if (usuarioRoles.RolId != 3) 
            {
                if (!string.IsNullOrEmpty (request.UsuarioConsultado)) 
                {
                    var usuarioAConsultar = _context.Users.FirstOrDefault (s => s.UserId == request.UsuarioConsultado);
                    if (usuarioAConsultar == null) return new List<ProspectDTO> ();

                    var prospectList = _context.Prospects
                        .Where (s => s.SalesAdvisorId == usuarioAConsultar.Id && s.CreationDate >= request.StartDate &&
                            s.CreationDate <= request.FinalDate)
                        .Include (c => c.SalesAdvisor)
                        .Include (c => c.Project)
                        .Include (c => c.ContactTypes)
                        .OrderByDescending (s => s.TransactionDate).ToList ();

                    return ProspectDTO.From (prospectList).ToList ();
                } else 
                {

                    var prospectList = _context.Prospects
                        .Where (s => s.SalesAdvisorId == usuario.Id && s.CreationDate >= request.StartDate &&
                            s.CreationDate <= request.FinalDate)
                        .Include (c => c.SalesAdvisor)
                        .Include (c => c.Project)
                        .Include (c => c.ContactTypes)
                        .OrderByDescending (s => s.TransactionDate).ToList ();

                    return ProspectDTO.From (prospectList).ToList ();
                }
            } else 
            {
                var prospectList = _context.Prospects
                    .Where (s => s.SalesAdvisorId == usuario.Id)
                    .Include (c => c.SalesAdvisor)
                    .Include (c => c.Project)
                    .Include (c => c.ContactTypes)
                    .OrderByDescending (s => s.TransactionDate).ToList ();

                return ProspectDTO.From (prospectList).ToList ();
            }
        }

        public void Dispose () 
        {
            if (_context != null) _context.Dispose ();
        }


    }
}