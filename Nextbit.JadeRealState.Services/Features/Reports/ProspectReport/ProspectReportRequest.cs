using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reports.ProspectReport
{
    public class ProspectReportRequest : RequestBase
    {
        public DateTime? StartDate { get; set; }
        
        public DateTime? FinalDate { get; set; }

        public int ProcessId { get; set; }
        
        public string Process { get; set; }

        public string UsuarioConsultado { get; set; }  

        public int ContactType { get; set; }
        
    }
}