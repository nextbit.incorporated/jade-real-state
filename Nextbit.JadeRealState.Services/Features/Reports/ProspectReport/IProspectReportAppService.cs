using System;
using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Features.Prospects;

namespace Nextbit.JadeRealState.Services.Features.Reports.ProspectReport
{
    public interface IProspectReportAppService : IDisposable
    {
        List<ProspectDTO> GetReport(ProspectReportRequest request);
        List<ProspectDTO> GetReportByProcess(ProspectReportRequest request);
        List<ProspectDTO> GetReportBySalesAdvisorId(ProspectReportRequest request);
        List<ProspectDTO> GetProspectByContactTypeReport(ProspectReportRequest request);
    }
}