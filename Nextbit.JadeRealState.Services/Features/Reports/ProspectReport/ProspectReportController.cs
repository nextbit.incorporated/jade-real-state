using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nextbit.JadeRealState.Services.Features.Prospects;

namespace Nextbit.JadeRealState.Services.Features.Reports.ProspectReport
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProspectReportController : ControllerBase
    {
        private readonly IProspectReportAppService _prospectReportAppService;
        public ProspectReportController(IProspectReportAppService prospectReportAppService)
        {
            _prospectReportAppService = prospectReportAppService ?? throw new ArgumentException(nameof(prospectReportAppService));
        }

        [HttpGet]
        [Authorize]
        public ActionResult<List<ProspectDTO>> GetReport([FromQuery] ProspectReportRequest request)
        {
            return Ok(_prospectReportAppService.GetReport(request));
        }

        [HttpGet]
        [Route("ByProject")]
        [Authorize]
        public ActionResult<List<ProspectDTO>> GetReportByProcess([FromQuery] ProspectReportRequest request)
        {
            return Ok(_prospectReportAppService.GetReportByProcess(request));
        }

        [HttpGet]
        [Route("ByUser")]
        [Authorize]
        public ActionResult<List<ProspectDTO>> GetReportBySalesAdvisorId([FromQuery] ProspectReportRequest request)
        {
            return Ok(_prospectReportAppService.GetReportBySalesAdvisorId(request));
        }

        [HttpGet]
        [Route("ByContactType")]
        [AllowAnonymous]
        public ActionResult<List<ProspectDTO>> GetReportByContactType([FromQuery] ProspectReportRequest request)
        {
             return Ok(_prospectReportAppService.GetProspectByContactTypeReport(request));
        }
    }
}