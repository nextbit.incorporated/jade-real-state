using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.NegotiationPhases
{
    public class NegotiationPhaseReportDto : ResponseBase
    {
        public int NegotiationId { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ProcessName { get; set; }
        public string SalesAdvisor { get; set; }
        public int SaleAdvisorId { get; set; }
        
        
    }
}