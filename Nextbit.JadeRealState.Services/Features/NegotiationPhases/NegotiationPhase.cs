using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.SupplierServices;
using Nextbit.JadeRealState.Services.Features.NegotiationAttachments;

namespace Nextbit.JadeRealState.Services.Features.NegotiationPhases
{
    [Table("NegotiationPhases")]
    public sealed class NegotiationPhase : Entity
    {
        private NegotiationPhase()
        {
            NegotiationAttachments = new HashSet<NegotiationAttachment>();
        }

        public int NegotiationId { get; private set; }
        public int ProcessId { get; private set; }
        public string Status { get; private set; }
        public DateTime? Date { get; private set; }
        public DateTime? DateComplete { get; private set; }
        public string Comment { get; private set; }
        public int Order { get; private set; }

        public Negotiation Negotiation { get; set; }

        public ICollection<NegotiationAttachment> NegotiationAttachments
        {
            get { return _negotiationAttachments ?? (_negotiationAttachments = new HashSet<NegotiationAttachment>()); }
            set { _negotiationAttachments = value; }
        }
        private ICollection<NegotiationAttachment> _negotiationAttachments;

        public ServiceProcess ServiceProcess { get; set; }
        public void Update(
        int _negotiationId, int _processId, string _status, DateTime? _date,
                        DateTime? _dateComplete, string _comment, int _order, string _user)
        {
            NegotiationId = _negotiationId;
            ProcessId = _processId;
            Status = _status;
            Date = _date;
            DateComplete = _dateComplete;
            Comment = _comment;
            Order = _order;

            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = _user ?? "Service";
            TransactionUId = Guid.NewGuid();
        }

        public class Builder
        {
            private readonly NegotiationPhase _negotiationPhase = new NegotiationPhase();

            public Builder WithNegotiationId(int negotiationId)
            {
                _negotiationPhase.NegotiationId = negotiationId;
                return this;
            }

            public Builder WithProcessId(int processId)
            {
                _negotiationPhase.ProcessId = processId;
                return this;
            }
            public Builder WithStatus(string status)
            {
                _negotiationPhase.Status = status;
                return this;
            }
            public Builder WithDate(DateTime? date)
            {
                _negotiationPhase.Date = date;
                return this;
            }
            public Builder WithDateComplete(DateTime? date)
            {
                _negotiationPhase.DateComplete = date;
                return this;
            }
            public Builder WithComment(string comment)
            {
                _negotiationPhase.Comment = comment;
                return this;
            }
            public Builder WithOrder(int order)
            {
                _negotiationPhase.Order = order;
                return this;
            }
            public Builder WithAuditFields(string user)
            {
                _negotiationPhase.CrudOperation = "Added";
                _negotiationPhase.TransactionDate = DateTime.Now;
                _negotiationPhase.TransactionType = "New";
                _negotiationPhase.ModifiedBy = user ?? "Service";
                _negotiationPhase.TransactionUId = Guid.NewGuid();

                return this;
            }

            public NegotiationPhase Build()
            {
                return _negotiationPhase;
            }
        }
    }
}