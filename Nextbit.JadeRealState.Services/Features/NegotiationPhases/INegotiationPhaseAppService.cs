using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.NegotiationPhases
{
    public interface INegotiationPhaseAppService : IDisposable
    {
        IEnumerable<NegotiationPhaseDto> GetLevels(NegotiationPhaseRequest request);
        IEnumerable<NegotiationPhaseDto> GetPhases(int negotiationId);
        NegotiationPhaseDto Create(NegotiationPhaseRequest request);
        IEnumerable<NegotiationPhaseDto> Update(NegotiationPhaseRequest request);

        IEnumerable<NegotiationPhaseReportDto> GetTop5Client(NegotiationPhaseRequest request);
        string Delete(int id);
    }
}