using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.NegotiationPhases
{
    public class NegotiationPhaseRequest : RequestBase
    {
        public int Id { get; set; }
        public int NegotiationId { get; set; }
        public int ProcessId { get; set; }
        public string Status { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? DateComplete { get; set; }
        public string Comment { get; set; }
        public int Order { get; set; }

    }

    public class NegotiationPhasePagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
        public int ValueId { get; set; }
    }
}