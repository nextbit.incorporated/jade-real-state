using System;

namespace Nextbit.JadeRealState.Services.Features.NegotiationPhases
{
    public interface INegotiationPhaseDomainService : IDisposable 
    {
        NegotiationPhase Create(NegotiationPhaseRequest request);
        NegotiationPhase Update(NegotiationPhaseRequest request, NegotiationPhase _oldRegister);
    }
}