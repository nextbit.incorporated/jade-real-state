using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.NegotiationPhases
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NegotiationPhaseController : ControllerBase
    {
        private readonly INegotiationPhaseAppService _negotiationPhaseAppService;

        public NegotiationPhaseController(INegotiationPhaseAppService negotiationPhaseAppService)
        {
            _negotiationPhaseAppService = negotiationPhaseAppService ?? throw new ArgumentException(nameof(negotiationPhaseAppService));
        }

        [HttpGet]
        [Authorize]
        public ActionResult<IEnumerable<NegotiationPhaseDto>> Get(NegotiationPhaseRequest request)
        {
            return Ok(_negotiationPhaseAppService.GetLevels(request));
        }

        [HttpGet]
        [Route("top")]
        [Authorize]
        //[AllowAnonymous]
        public ActionResult<IEnumerable<NegotiationPhaseReportDto>> GetTOP([FromQuery]NegotiationPhaseRequest request)
        {
            return Ok(_negotiationPhaseAppService.GetTop5Client(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<NegotiationPhaseDto> Post([FromBody]NegotiationPhaseRequest request)
        {
            return Ok(_negotiationPhaseAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<NegotiationPhaseDto> Put([FromBody] NegotiationPhaseRequest request)
        {
            return Ok(_negotiationPhaseAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_negotiationPhaseAppService.Delete(Id));
        }
    }
}