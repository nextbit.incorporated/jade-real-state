using System;

namespace Nextbit.JadeRealState.Services.Features.NegotiationPhases
{
    public class NegotiationPhaseDomainService : INegotiationPhaseDomainService
    {
        public NegotiationPhase Create(NegotiationPhaseRequest request)
        {
            if (request.NegotiationId == 0 ) throw new ArgumentNullException(nameof(request.NegotiationId));

            NegotiationPhase registro = new NegotiationPhase.Builder()
            .WithNegotiationId(request.NegotiationId)
            .WithProcessId(request.ProcessId)
            .WithDate(request.Date)
            .WithDateComplete(request.DateComplete)
            .WithStatus(request.Status)
            .WithComment(request.Comment)
            .WithOrder(request.Order)
            .WithAuditFields(request.User)
            .Build();

            return registro;
        }

        public NegotiationPhase Update(NegotiationPhaseRequest request, NegotiationPhase _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.NegotiationId, request.ProcessId, request.Status, 
                request.Date, request.DateComplete,request.Comment, request.Order, request.User);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}