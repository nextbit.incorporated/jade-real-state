using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.NegotiationPhases
{
    public class NegotiationPhasePagedDto : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<NegotiationPhaseDto> NegotiationPhases { get; set; }
    }
}