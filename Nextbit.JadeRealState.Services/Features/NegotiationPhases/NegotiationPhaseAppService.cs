using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;


namespace Nextbit.JadeRealState.Services.Features.NegotiationPhases
{
    public class NegotiationPhaseAppService : INegotiationPhaseAppService
    {
        private RealStateContext _context;
        private readonly INegotiationPhaseDomainService _negotiationPhaseDomainService;

        public NegotiationPhaseAppService(RealStateContext context, INegotiationPhaseDomainService negotiationPhaseDomainService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _negotiationPhaseDomainService = negotiationPhaseDomainService ?? throw new ArgumentException(nameof(negotiationPhaseDomainService));
        }

        public NegotiationPhaseDto Create(NegotiationPhaseRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newRegister = _negotiationPhaseDomainService.Create(request);

            _context.NegotiationPhases.Add(newRegister);
            _context.SaveChanges();

            return new NegotiationPhaseDto
            {
                Id = newRegister.Id,
                NegotiationId = newRegister.NegotiationId,
                Comment = newRegister.Comment

            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var clienLevel = _context.NegotiationPhases.FirstOrDefault(s => s.Id == id);
            if (clienLevel == null) throw new Exception("Nivel not exists");
            _context.NegotiationPhases.Remove(clienLevel);
            _context.SaveChanges();

            return string.Empty;
        }


        public IEnumerable<NegotiationPhaseDto> Update(NegotiationPhaseRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldClientLevel = _context.NegotiationPhases.FirstOrDefault(s => s.Id == request.Id);
            if (oldClientLevel == null) return new List<NegotiationPhaseDto> {
                new NegotiationPhaseDto{ ValidationErrorMessage = "Nivel de cliente inexistente" }};

            var newRegister = _negotiationPhaseDomainService.Update(request, oldClientLevel);

            _context.NegotiationPhases.Update(oldClientLevel);
            _context.SaveChanges();

            var listaPhases = _context.NegotiationPhases.Where(s => s.NegotiationId == request.NegotiationId)
                    .Include(c => c.ServiceProcess).ThenInclude(x => x.SupplierService)
                    .Include(c => c.Negotiation).ThenInclude(x => x.NegotiationClients).ThenInclude(x => x.Client)
                    .Include(c => c.Negotiation).ThenInclude(x => x.Project)
                    .Include(c => c.Negotiation).ThenInclude(x => x.Reserve).ThenInclude(t => t.Detalles)
                    .OrderBy(c => c.Order);

            var lista = NegotiationPhaseDto.FromNegotiations(listaPhases);
            return lista;
        }

        public IEnumerable<NegotiationPhaseDto> GetLevels(NegotiationPhaseRequest request)
        {
            if (request.NegotiationId == 0) throw new ArgumentException(nameof(request.NegotiationId));
            var lista = _context.NegotiationPhases.Where(s => s.NegotiationId == request.Id)
                .Include(c => c.Negotiation)
                .Include(c => c.ServiceProcess)
                .OrderBy(s => s.Order);

            var resultado = NegotiationPhaseDto.FromNegotiations(lista);
            return resultado.OrderBy(s => s.Order).ToList();

        }
        public IEnumerable<NegotiationPhaseDto> GetPhases(int negotiationId)
        {
            if (negotiationId == 0) throw new ArgumentException(nameof(negotiationId));
            var lista = _context.NegotiationPhases.Where(s => s.NegotiationId == negotiationId)
                .Include(c => c.Negotiation)
                .Include(c => c.ServiceProcess)
                .OrderBy(s => s.Order);

            if (lista == null) return new List<NegotiationPhaseDto>();
            if (!lista.Any()) return new List<NegotiationPhaseDto>();
            var resultado = NegotiationPhaseDto.FromNegotiations(lista);

            return resultado.OrderBy(s => s.Order).ToList();
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_negotiationPhaseDomainService != null) _negotiationPhaseDomainService.Dispose();
        }

        public IEnumerable<NegotiationPhaseReportDto> GetTop5Client(NegotiationPhaseRequest request)
        {
            if (string.IsNullOrEmpty(request.User)) return new List<NegotiationPhaseReportDto>();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new List<NegotiationPhaseReportDto>();
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<NegotiationPhaseReportDto>();

            if (usuarioRoles.RolId != 3)
            {
                var result = _context.NegotiationPhases
                   .Include(x => x.ServiceProcess)
                   .Include(x => x.Negotiation).ThenInclude(x => x.NegotiationClients).ThenInclude(x => x.Client)
                   .Include(x => x.Negotiation).ThenInclude(x => x.SalesAdvisor)
                   .AsEnumerable()
                   .Where(s => s.Status == "PENDIENTE")
                   .OrderBy(s => s.Date).OrderBy(t => t.Order)
                   .GroupBy(t => new { t.NegotiationId })
                   .Select(t => new NegotiationPhaseReportDto
                   {
                       NegotiationId = t.Key.NegotiationId,
                       ClientId = t.FirstOrDefault()?.Negotiation?.NegotiationClients?.FirstOrDefault(n => n.Titular)?.ClientId ?? 0,
                       ClientName = t.FirstOrDefault()?.Negotiation.NegotiationClients?.FirstOrDefault(n => n.Titular)?.Client?.GetCompleteName(),
                       ProcessName = t.FirstOrDefault()?.ServiceProcess.Name,
                       Status = "PENDIENTE",
                       SalesAdvisor = t.FirstOrDefault()?.Negotiation?.SalesAdvisor?.GetName() ?? string.Empty
                   }).ToList();

                return result;
            }
            else
            {
                var result = _context.NegotiationPhases
                   .Include(x => x.ServiceProcess)
                   .Include(x => x.Negotiation).ThenInclude(x => x.NegotiationClients).ThenInclude(x => x.Client)
                   .Include(x => x.Negotiation).ThenInclude(x => x.SalesAdvisor)
                   .AsEnumerable()
                   .Where(s => s.Status == "PENDIENTE")
                   .OrderBy(s => s.Date).OrderBy(t => t.Order)
                   .GroupBy(t => new { t.NegotiationId })
                   .Select(t => new NegotiationPhaseReportDto
                   {
                       NegotiationId = t.Key.NegotiationId,
                       ClientId = t.FirstOrDefault()?.Negotiation?.NegotiationClients?.FirstOrDefault(n => n.Titular)?.ClientId ?? 0,
                       ClientName = t.FirstOrDefault()?.Negotiation.NegotiationClients?.FirstOrDefault(n => n.Titular)?.Client?.GetCompleteName(),
                       ProcessName = t.FirstOrDefault()?.ServiceProcess.Name,
                       Status = "PENDIENTE",
                       SalesAdvisor = t.FirstOrDefault()?.Negotiation?.SalesAdvisor?.GetName() ?? string.Empty,
                       SaleAdvisorId = t.FirstOrDefault()?.Negotiation?.SalesAdvisor?.Id ?? 0
                   }).ToList();

                var r = result.Where(s => s.SaleAdvisorId == usuario.Id).ToList();   
                return r;
            }
           
        }
    }
}