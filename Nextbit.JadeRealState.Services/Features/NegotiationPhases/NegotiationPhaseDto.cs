using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.NegotiationAttachments;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.SupplierServices;

namespace Nextbit.JadeRealState.Services.Features.NegotiationPhases
{
    public class NegotiationPhaseDto : ResponseBase
    {
        public int NegotiationId { get; set; }
        public NegotiationDto Negotiation { get; set; }
        public string Project { get; set; }
        public ClientDto Client { get; set; }
        public int ProcessId { get; set; }
        public ServiceProcessDto ServiceProcess { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? DateComplete { get; set; }
        public string Comment { get; set; }
        public int Order { get; set; }
        public bool IsEdit { get; set; } = false;

        public bool IsOpenViewFileAttachment { get; set; } = false;

        public List<NegotiationAttachmentDto> Attachments { get; set; }

        internal static List<NegotiationPhaseDto> FromNegotiations(IEnumerable<NegotiationPhase> phases)
        {
            if (phases == null || !phases.Any()) return new List<NegotiationPhaseDto>();

            return (from qry in phases
                    select From(qry)).ToList();
        }

        internal static NegotiationPhaseDto From(NegotiationPhase s)
        {
            if (s == null) return new NegotiationPhaseDto();

            return new NegotiationPhaseDto
            {

                Id = s.Id,
                NegotiationId = s.NegotiationId,
                Negotiation = s.Negotiation == null
                    ? new NegotiationDto()
                    : new NegotiationDto
                    {
                        Id = s.Negotiation.Id
                    },
                Project = s.Negotiation?.Project?.Name,
                ProcessId = s.ProcessId,
                ServiceProcess = s.ServiceProcess == null
                                ? new ServiceProcessDto()
                                : new ServiceProcessDto
                                {
                                    Id = s.ServiceProcess.Id,
                                    Name = s.ServiceProcess.Name,
                                    Order = s.ServiceProcess.Order,
                                    SupplierServiceId = s.ServiceProcess.SupplierServiceId,
                                    SupplierServiceName = s.ServiceProcess.SupplierService.Name,
                                },
                Attachments = NegotiationAttachmentDto.From(s.NegotiationAttachments),
                Date = s.Date,
                DateComplete = s.DateComplete,
                Comment = s.Comment,
                Status = s.Status,
                Order = s.Order
            };
        }

        internal static List<NegotiationPhaseDto> From(ICollection<NegotiationPhase> negotiationPhases)
        {
            return (from qry in negotiationPhases select From(qry)).ToList();
        }
    }
}