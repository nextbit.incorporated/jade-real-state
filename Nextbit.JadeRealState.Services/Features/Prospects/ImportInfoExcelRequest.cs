using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Prospects
{
    public class ImportInfoExcelRequest : RequestBase
    {
        public List<ColumnsXLsRequest> ListInfXls { get; set; }
        public int ProjectId { get; set; }
        public int ContactTypeId { get; set; }
    }


    public class ColumnsXLsRequest 
    {
        public string Identidad { get; set; }
        public string Email { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
    }
}