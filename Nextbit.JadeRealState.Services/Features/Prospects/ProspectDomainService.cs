using System;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Prospects
{
    public class ProspectDomainService : IProspectDomainService
    {
        public Prospect Create(ProspectRequest request, User salesAdvisor, Project project, ContactType contactType)
        {
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));

            Prospect origin = new Prospect.Builder()
            .WithRegisterDate(request.RegisterDate)
            .WithProjectId(request.projectId)
            .WithContactTypeId(request.ContactType)
            .WithContactNumberOne(request.ContactNumberOne ?? string.Empty)
            .WithcontactNumbertwo(request.ContactNumberTwo ?? string.Empty)
           .WithName(request.Name)
           .WithIdNumber(request.IdNumber)
           .WithTransactionOrigin("Ingreso")
           .WithEmail(request.Email ?? string.Empty)
           .WithSalesAdvisor(salesAdvisor)
           .WithProject(project)
           .WithContactType(contactType)
           .WithComments(request.Comments)
           .WithAuditFields()
           .Build();

            return origin;
        }

        public Prospect Update(ProspectRequest request, Prospect prospectOldInfo, User salesAdvisor)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (prospectOldInfo == null) throw new ArgumentException(nameof(prospectOldInfo));

            Prospect updatedProspect = new Prospect.Builder()
            .WithCreationDate(request.CreationDate)
            .WithRegisterDate(request.RegisterDate)
            .WithProjectId(request.projectId)
            .WithContactTypeId(request.ContactType)
            .WithContactNumberOne(request.ContactNumberOne ?? string.Empty)
            .WithcontactNumbertwo(request.ContactNumberTwo ?? string.Empty)
           .WithName(request.Name)
           .WithIdNumber(request.IdNumber)
           .WithTransactionOrigin(request.TransactionOrigin)
           .WithEmail(request.Email)
           .WithSalesAdvisor(salesAdvisor)
           .WithComments(request.Comments)
           .WithAuditFields()
            .Build();

            var prospectUpdated = prospectOldInfo.Update(updatedProspect);

            return prospectUpdated;
        }

        public void Dispose()
        {
        }
    }
}