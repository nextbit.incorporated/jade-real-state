using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Prospects
{
    public class ProspectPagedDTO
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<ProspectDTO> Prospects { get; set; }
    }
}