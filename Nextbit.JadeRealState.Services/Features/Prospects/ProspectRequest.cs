using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Prospects
{
    public class ProspectRequest : RequestBase
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime RegisterDate { get; set; }
        public int projectId { get; set; }
        public int ContactType { get; set; }
        public string Name { get; set; }
        public string IdNumber { get; set; }
        public string ContactNumberOne { get; set; }
        public string ContactNumberTwo { get; set; }
        public string Email { get; set; }
        public string TransactionOrigin { get; set; }
        public string Comments { get; set; }
        public int? SalesAdvisorId { get; set; }
        public string SalesAdvisor { get; set; }
    }
    public class ProspectPagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
        public int IdFilter { get; set; }
        public bool IsActive { get; set; }
        public bool ExcludeNegotiationEnded { get; set; }
    }
}