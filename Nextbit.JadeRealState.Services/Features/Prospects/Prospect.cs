using System;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Prospects
{
    [Table("Prospects")]
    public class Prospect : Entity
    {
        public DateTime RegisterDate { get; private set; }
        public int? ProjectId { get; private set; }
        public int? ContactType { get; private set; }
        public string IdNumber { get; private set; }
        public string Name { get; private set; }
        public string ContactNumberOne { get; private set; }
        public string ContactNumberTwo { get; private set; }
        public string Email { get; private set; }
        public string TransactionOrigin { get; private set; }
        public string Status { get; private set; }
        public int? SalesAdvisorId { get; private set; }

        public string Comments { get; private set; }

        public Project Project { get; set; }
        public ContactType ContactTypes { get; set; }
        public User SalesAdvisor { get; set; }

        public Prospect Update(Prospect updatedProspect)
        {

            CreationDate = updatedProspect.CreationDate;
            RegisterDate = updatedProspect.RegisterDate;
            ProjectId = updatedProspect.ProjectId;
            ContactType = updatedProspect.ContactType;
            Name = updatedProspect.Name;
            IdNumber = updatedProspect.IdNumber;
            ContactNumberOne = updatedProspect.ContactNumberOne;
            ContactNumberTwo = updatedProspect.ContactNumberTwo;
            Email = updatedProspect.Email;
            TransactionOrigin = updatedProspect.TransactionOrigin;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedSupplierService";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
            Status = "ACTIVO";
            Comments = updatedProspect.Comments;

            SalesAdvisorId = updatedProspect.SalesAdvisorId;
            SalesAdvisor = updatedProspect.SalesAdvisor;

            return this;
        }

        public class Builder
        {
            private readonly Prospect _prospect = new Prospect();

            public Builder WithCreationDate(DateTime creationDate)
            {
                _prospect.CreationDate = creationDate;
                return this;
            }

            public Builder WithRegisterDate(DateTime registerDate)
            {
                _prospect.RegisterDate = registerDate;
                return this;
            }
            public Builder WithProjectId(int projectId)
            {
                _prospect.ProjectId = projectId;
                return this;
            }
            public Builder WithContactTypeId(int contactType)
            {
                _prospect.ContactType = contactType;
                return this;
            }
            public Builder WithName(string name)
            {
                _prospect.Name = name;
                return this;
            }

            public Builder WithIdNumber(string idNumber)
            {
                _prospect.IdNumber = idNumber;
                return this;
            }
            public Builder WithContactNumberOne(string contactNumberOne)
            {
                _prospect.ContactNumberOne = contactNumberOne;
                return this;
            }
            public Builder WithcontactNumbertwo(string contactNumberTwo)
            {
                _prospect.ContactNumberTwo = contactNumberTwo;
                return this;
            }

            public Builder WithEmail(string email)
            {
                _prospect.Email = email;
                return this;
            }

            public Builder WithTransactionOrigin(string transactionOrigin)
            {
                _prospect.TransactionOrigin = transactionOrigin;
                return this;
            }

            public Builder WithComments(string comments)
            {
                _prospect.Comments = comments;
                return this;
            }

            public Builder WithSalesAdvisor(User salesAdvisor)
            {
                _prospect.SalesAdvisorId = salesAdvisor?.Id;
                _prospect.SalesAdvisor = salesAdvisor;
                return this;
            }

            public Builder WithProject(Project project)
            {
                _prospect.ProjectId = project?.Id;
                _prospect.Project = project;
                return this;
            }
            public Builder WithContactType(ContactType contactType)
            {
                _prospect.ContactType = contactType?.Id;
                _prospect.ContactTypes = contactType;
                return this;
            }

            public Builder WithAuditFields()
            {
                _prospect.CrudOperation = "Added";
                _prospect.TransactionDate = DateTime.Now;
                _prospect.TransactionType = "NewSupplierService";
                _prospect.ModifiedBy = "Service";
                _prospect.TransactionUId = Guid.NewGuid();
                _prospect.Status = "ACTIVO";

                return this;
            }

            public Prospect Build()
            {
                return _prospect;
            }
        }
    }
}