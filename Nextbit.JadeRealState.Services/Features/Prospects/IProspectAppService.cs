using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Prospects {
    public interface IProspectAppService : IDisposable {
        ProspectPagedDTO GetPaged (ProspectPagedRequest request);
        ProspectDTO TryCreateNewClientProspect (ProspectRequest request);
        ProspectDTO Create (ProspectRequest request);
        ProspectDTO Update (ProspectRequest request);
        string Delete (int id);
        List<ProspectDTO> ImportExcelData (ImportInfoExcelRequest request);
        string ConvertProspectInClient (int id);

    }
}