using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.Operations;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Prospects
{
    public class ProspectAppService : IProspectAppService
    {
        private RealStateContext _context;
        private readonly IProspectDomainService _prospectDomainService;

        public ProspectAppService(RealStateContext context, IProspectDomainService prospectDomainService)
        {
            _context = context ??
                throw new ArgumentNullException(nameof(context));
            _prospectDomainService = prospectDomainService ??
                throw new ArgumentException(nameof(prospectDomainService));
        }

        public ProspectDTO TryCreateNewClientProspect(ProspectRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            IEnumerable<Prospect> prospects = _context.Prospects
                .Include(c => c.SalesAdvisor)
                .Include(c => c.Project)
                .Include(c => c.ContactTypes)
                .ToList();

            var cc = ProspectDTO.From(prospects);
            var prospectDto = new ProspectDTO();
            if (!string.IsNullOrEmpty(request.IdNumber))
            {
                if (VerifyExistsClientIdentificationCard(request.IdNumber, request, cc, ref prospectDto))
                {
                    prospectDto.ValidationErrorMessage = "Usuario con identidad: '" +
                        prospectDto.IdNumber + "' ya existe en sistema con el codigo: '" + prospectDto.Id +
                        "' " + "Nombre: " + prospectDto.Name;
                    return prospectDto;
                }
            }

            if (!string.IsNullOrEmpty(request.Name))
            {
                if (VerifyExistsClientName(request.Name, request, cc, ref prospectDto))
                {
                    prospectDto.ValidationErrorMessage = "Usuario con identidad: '" +
                        prospectDto.IdNumber + "' ya existe en sistema con el codigo: '" + prospectDto.Id +
                        "' " + "Nombre: " + request.Name;
                    return prospectDto;
                }
            }

            return new ProspectDTO();
        }

        private bool VerifyExistsClientName(
            string name, ProspectRequest prospect, List<ProspectDTO> cc, ref ProspectDTO prospectDto)
        {
            var exists = true;

            var clientExists = cc.FirstOrDefault(s => s.Name == name);
            if (clientExists == null) return !exists;

            prospectDto.Id = clientExists.Id;
            prospectDto.Name = clientExists.Name;
            prospectDto.IdNumber = clientExists.IdNumber;

            return exists;
        }

        private bool VerifyExistsClientIdentificationCard(
            string identificationCard, ProspectRequest prospect, List<ProspectDTO> cc, ref ProspectDTO prospectDto)
        {
            var exists = true;

            var clientExists = cc.FirstOrDefault(s => s.IdNumber == identificationCard);
            if (clientExists == null) return !exists;

            prospectDto.Id = clientExists.Id;
            prospectDto.Name = clientExists.Name;
            prospectDto.IdNumber = clientExists.IdNumber;

            return exists;
        }
        public ProspectDTO Create(ProspectRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));

            User salesAdvisor = _context.Users.FirstOrDefault(u => u.Id == request.SalesAdvisorId);
            Project project = _context.Projects.FirstOrDefault(u => u.Id == request.projectId);
            ContactType contactType = _context.ContactTypes.FirstOrDefault(u => u.Id == request.ContactType);

            Prospect newProspect = _prospectDomainService.Create(request, salesAdvisor, project, contactType);

            if (project == null) return new ProspectDTO { ValidationErrorMessage = "Proyecto invalido" };
            if (contactType == null) return new ProspectDTO { ValidationErrorMessage = "Tipo de Contacto invalido" };

            _context.Prospects.Add(newProspect);
            _context.SaveChanges();

            return ProspectDTO.From(newProspect);

        }

        public ProspectDTO Update(ProspectRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldProspects = _context.Prospects
                .Include(c => c.SalesAdvisor)
                .Include(c => c.Project)
                .Include(c => c.ContactTypes)
                .FirstOrDefault(s => s.Id == request.Id);

            if (oldProspects == null) return new ProspectDTO { ValidationErrorMessage = "Cliente inexistente" };

            User salesAdvisor = _context.Users.FirstOrDefault(u => u.Id == request.SalesAdvisorId);

            Prospect prospectUpdate = _prospectDomainService.Update(request, oldProspects, salesAdvisor);

            _context.Prospects.Update(prospectUpdate);
            _context.SaveChanges();

            return ProspectDTO.From(prospectUpdate);

        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var prospect = _context.Prospects.FirstOrDefault(s => s.Id == id);

            if (prospect != null)
            {
                _context.Prospects.Remove(prospect);
                _context.SaveChanges();
            }

            return string.Empty;
        }

        public ProspectPagedDTO GetPaged(ProspectPagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.User)) return new ProspectPagedDTO { Prospects = new List<ProspectDTO>() };
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new ProspectPagedDTO { Prospects = new List<ProspectDTO>() };
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new ProspectPagedDTO { Prospects = new List<ProspectDTO>() };

            if (usuarioRoles.RolId != 3)
            {

                if (request.PageIndex == 0) request.PageIndex = 1;
                var count = Convert.ToDecimal(_context.Prospects.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;

                if (string.IsNullOrEmpty(request.Value))
                {
                    IEnumerable<Prospect> prospects = _context.Prospects
                        .Include(c => c.SalesAdvisor)
                        .Include(c => c.Project)
                        .Include(c => c.ContactTypes)
                        .OrderByDescending(s => s.TransactionDate)
                        .Skip(request.PageSize * (request.PageIndex - 1))
                        .Take(request.PageSize)
                        .ToList();

                    IEnumerable<string> listProjectsId = prospects.Select(s => s.ProjectId.ToString()).Distinct();
                    IEnumerable<Project> projects = _context.Projects.Where(s => listProjectsId.Contains(s.Id.ToString()));
                    IEnumerable<string> listContactTypesId = prospects.Select(s => s.ContactType.ToString()).Distinct();
                    IEnumerable<ContactType> contactTypes = _context.ContactTypes.Where(s => listContactTypesId.Contains(s.Id.ToString()));

                    return new ProspectPagedDTO
                    {
                        PageIndex = request.PageIndex,
                        PageSize = request.PageSize,
                        PageCount = Convert.ToInt16(totalPage),
                        Prospects = ProspectDTO.From(prospects)
                    };
                }
                else
                {
                    if (request.IdFilter == 1)
                    {
                        return GetPagedByFirstNameFilter(request);
                    }

                    if (request.IdFilter == 2)
                    {
                        return GetPagedByLastNameFilter(request);
                    }

                    if (request.IdFilter == 2)
                    {
                        return GetPagedBySaleAdvisorFilter(request);
                    }

                    IEnumerable<Prospect> prospects = _context.Prospects
                        .Include(c => c.SalesAdvisor)
                        .Include(c => c.Project)
                        .Include(c => c.ContactTypes)
                        .Where(s => s.Name.Contains(request.Value))
                        .Skip(request.PageSize * (request.PageIndex - 1))
                        .Take(request.PageSize)
                        .ToList();

                    var listProjectsId = prospects.Select(s => s.ProjectId.ToString()).Distinct();
                    IEnumerable<Project> projects = _context.Projects.Where(s => listProjectsId.Contains(s.Id.ToString()));
                    IEnumerable<string> listContactTypesId = prospects.Select(s => s.ContactType.ToString()).Distinct();
                    IEnumerable<ContactType> contactTypes = _context.ContactTypes.Where(s => listContactTypesId.Contains(s.Id.ToString()));

                    return new ProspectPagedDTO
                    {
                        PageIndex = request.PageIndex,
                        PageSize = request.PageSize,
                        PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                        Prospects = ProspectDTO.From(prospects)
                    };
                }
            }
            else
            {
                if (request.PageIndex == 0) request.PageIndex = 1;
                var count = Convert.ToDecimal(_context.Prospects.Where(s => s.SalesAdvisorId == usuario.Id).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;

                if (string.IsNullOrEmpty(request.Value))
                {
                    IEnumerable<Prospect> prospects = _context.Prospects
                        .Where(s => s.SalesAdvisorId == usuario.Id)
                        .Include(c => c.SalesAdvisor)
                        .Include(c => c.Project)
                        .Include(c => c.ContactTypes)
                        .OrderByDescending(s => s.TransactionDate)
                        .Skip(request.PageSize * (request.PageIndex - 1))
                        .Take(request.PageSize)
                        .ToList();

                    IEnumerable<string> listProjectsId = prospects.Select(s => s.ProjectId.ToString()).Distinct();
                    IEnumerable<Project> projects = _context.Projects.Where(s => listProjectsId.Contains(s.Id.ToString()));
                    IEnumerable<string> listContactTypesId = prospects.Select(s => s.ContactType.ToString()).Distinct();
                    IEnumerable<ContactType> contactTypes = _context.ContactTypes.Where(s => listContactTypesId.Contains(s.Id.ToString()));

                    return new ProspectPagedDTO
                    {
                        PageIndex = request.PageIndex,
                        PageSize = request.PageSize,
                        PageCount = Convert.ToInt16(totalPage),
                        Prospects = ProspectDTO.From(prospects)
                    };
                }
                else
                {
                    if (request.IdFilter == 1)
                    {
                        return GetPagedByFirstNameFilterAndUser(request, usuario.Id);
                    }

                    if (request.IdFilter == 2)
                    {
                        return GetPagedByLastNameFilterAndUser(request, usuario.Id);
                    }

                    if (request.IdFilter == 2)
                    {
                        return GetPagedBySaleAdvisorFilter(request);
                    }

                    IEnumerable<Prospect> prospects = _context.Prospects
                        .Include(c => c.SalesAdvisor)
                        .Include(c => c.Project)
                        .Include(c => c.ContactTypes)
                        .Where(s => s.Name.Contains(request.Value) && s.SalesAdvisorId == usuario.Id)
                        .Skip(request.PageSize * (request.PageIndex - 1))
                        .Take(request.PageSize)
                        .ToList();

                    var listProjectsId = prospects.Select(s => s.ProjectId.ToString()).Distinct();
                    IEnumerable<Project> projects = _context.Projects.Where(s => listProjectsId.Contains(s.Id.ToString()));
                    IEnumerable<string> listContactTypesId = prospects.Select(s => s.ContactType.ToString()).Distinct();
                    IEnumerable<ContactType> contactTypes = _context.ContactTypes.Where(s => listContactTypesId.Contains(s.Id.ToString()));

                    return new ProspectPagedDTO
                    {
                        PageIndex = request.PageIndex,
                        PageSize = request.PageSize,
                        PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                        Prospects = ProspectDTO.From(prospects)
                    };
                }
            }
        }

        private ProspectPagedDTO GetPagedBySaleAdvisorFilter(ProspectPagedRequest request)
        {
            List<Prospect> prospects = _context.Prospects
                .Include(c => c.SalesAdvisor)
                .Include(c => c.Project)
                .Include(c => c.ContactTypes)
                .Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1))
                .Take(request.PageSize)
                .ToList();

            int count = prospects.Count;
            IEnumerable<string> listProjectsId = prospects.Select(s => s.ProjectId.ToString()).Distinct();
            IEnumerable<Project> projects = _context.Projects.Where(s => listProjectsId.Contains(s.Id.ToString()));
            IEnumerable<string> listContactTypesId = prospects.Select(s => s.ContactType.ToString()).Distinct();
            IEnumerable<ContactType> contactTypes = _context.ContactTypes.Where(s => listContactTypesId.Contains(s.Id.ToString()));

            return new ProspectPagedDTO
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                Prospects = ProspectDTO.From(prospects)
            };
        }

        private ProspectPagedDTO GetPagedByLastNameFilter(ProspectPagedRequest request)
        {
            List<Prospect> prospects = _context.Prospects
                .Include(c => c.SalesAdvisor)
                .Include(c => c.Project)
                .Include(c => c.ContactTypes)
                .Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1))
                .Take(request.PageSize)
                .ToList();

            int count = prospects.Count;
            IEnumerable<string> listProjectsId = prospects.Select(s => s.ProjectId.ToString()).Distinct();
            IEnumerable<Project> projects = _context.Projects.Where(s => listProjectsId.Contains(s.Id.ToString()));
            IEnumerable<string> listContactTypesId = prospects.Select(s => s.ContactType.ToString()).Distinct();
            IEnumerable<ContactType> contactTypes = _context.ContactTypes.Where(s => listContactTypesId.Contains(s.Id.ToString()));

            return new ProspectPagedDTO
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                Prospects = ProspectDTO.From(prospects)
            };
        }

        private ProspectPagedDTO GetPagedByLastNameFilterAndUser(ProspectPagedRequest request, int userId)
        {
            List<Prospect> prospects = _context.Prospects
                .Include(c => c.SalesAdvisor)
                .Include(c => c.Project)
                .Include(c => c.ContactTypes)
                .Where(s => s.Name.Contains(request.Value) && s.SalesAdvisorId == userId)
                .Skip(request.PageSize * (request.PageIndex - 1))
                .Take(request.PageSize)
                .ToList();

            int count = prospects.Count;
            IEnumerable<string> listProjectsId = prospects.Select(s => s.ProjectId.ToString()).Distinct();
            IEnumerable<Project> projects = _context.Projects.Where(s => listProjectsId.Contains(s.Id.ToString()));
            IEnumerable<string> listContactTypesId = prospects.Select(s => s.ContactType.ToString()).Distinct();
            IEnumerable<ContactType> contactTypes = _context.ContactTypes.Where(s => listContactTypesId.Contains(s.Id.ToString()));

            return new ProspectPagedDTO
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                Prospects = ProspectDTO.From(prospects)
            };
        }

        private ProspectPagedDTO GetPagedByFirstNameFilter(ProspectPagedRequest request)
        {
            List<Prospect> prospects = _context.Prospects
                .Include(c => c.SalesAdvisor)
                .Include(c => c.Project)
                .Include(c => c.ContactTypes)
                .Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1))
                .Take(request.PageSize)
                .ToList();

            int count = prospects.Count;
            IEnumerable<string> listProjectsId = prospects.Select(s => s.ProjectId.ToString()).Distinct();
            IEnumerable<Project> projects = _context.Projects.Where(s => listProjectsId.Contains(s.Id.ToString()));
            IEnumerable<string> listContactTypesId = prospects.Select(s => s.ContactType.ToString()).Distinct();
            IEnumerable<ContactType> contactTypes = _context.ContactTypes.Where(s => listContactTypesId.Contains(s.Id.ToString()));

            return new ProspectPagedDTO
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                Prospects = ProspectDTO.From(prospects)
            };
        }
        private ProspectPagedDTO GetPagedByFirstNameFilterAndUser(ProspectPagedRequest request, int userId)
        {
            List<Prospect> prospects = _context.Prospects
                .Include(c => c.SalesAdvisor)
                .Include(c => c.Project)
                .Include(c => c.ContactTypes)
                .Where(s => s.Name.Contains(request.Value) && s.SalesAdvisorId == userId)
                .Skip(request.PageSize * (request.PageIndex - 1))
                .Take(request.PageSize)
                .ToList();

            int count = prospects.Count;
            IEnumerable<string> listProjectsId = prospects.Select(s => s.ProjectId.ToString()).Distinct();
            IEnumerable<Project> projects = _context.Projects.Where(s => listProjectsId.Contains(s.Id.ToString()));
            IEnumerable<string> listContactTypesId = prospects.Select(s => s.ContactType.ToString()).Distinct();
            IEnumerable<ContactType> contactTypes = _context.ContactTypes.Where(s => listContactTypesId.Contains(s.Id.ToString()));

            return new ProspectPagedDTO
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                Prospects = ProspectDTO.From(prospects)
            };
        }

        public List<ProspectDTO> ImportExcelData(ImportInfoExcelRequest request)
        {
            if (request == null) return new List<ProspectDTO> { new ProspectDTO { ValidationErrorMessage = "request nulo" } };
            if (request.ListInfXls == null) return new List<ProspectDTO> { new ProspectDTO { ValidationErrorMessage = "lista nula" } };
            if (request.ListInfXls.Count <= 1) return new List<ProspectDTO> { new ProspectDTO { ValidationErrorMessage = "lista vacia" } };

            var contador = 1;
            var listProspects = new List<Prospect>();
            foreach (var item in request.ListInfXls)
            {
                if (contador == 1)
                {
                    contador += 1;
                    continue;
                }
                Prospect newProspect = new Prospect.Builder()
                    .WithRegisterDate(DateTime.Now)
                    .WithProjectId(request.ProjectId)
                    .WithContactTypeId(request.ContactTypeId)
                    .WithContactNumberOne(item.Telefono ?? string.Empty)
                    .WithName(item.Nombre)
                    .WithIdNumber(item.Identidad ?? string.Empty)
                    .WithEmail(item.Email ?? string.Empty)
                    .WithTransactionOrigin("Importacion")
                    .WithAuditFields()
                    .Build();

                listProspects.Add(newProspect);
                contador += 1;
            }
            _context.Prospects.AddRange(listProspects);
            _context.SaveChanges();

            return new List<ProspectDTO> {
                new ProspectDTO {
                    IdNumber = "Importacion",
                        Name = "Archivo",
                        TransactionOrigin = contador.ToString (),
                        ContactNumberOne = request.ListInfXls.Count.ToString ()
                }
            };
        }

        public string ConvertProspectInClient(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var prospect = _context.Prospects
                .Include(c => c.SalesAdvisor)
                .Include(c => c.Project)
                .Include(c => c.ContactTypes)
                .FirstOrDefault(s => s.Id == id);

            var origin = _context.Origin.OrderBy(s => s.TransactionDate).FirstOrDefault();
            if (prospect == null) throw new Exception("Cliente not existe");
            if (origin == null) throw new Exception("No existen nacionalidades");

            var client = new Client.Builder()
                .WithFirstName(prospect.Name)
                .WithFirstSurname(prospect.Name)
                .WithIdentificationCard(EncriptorHelper.EncryptString(prospect.IdNumber ?? string.Empty))
                .WithEmail(EncriptorHelper.EncryptString(prospect.Email ?? string.Empty))
                .WithCellPhoneNumber(EncriptorHelper.EncryptString(prospect.ContactNumberOne))
                .WithContactTypeId(prospect.ContactType)
                .WithProjectId(prospect.ProjectId)
                .WithNationalityId(origin.Id)
                .WithComments(prospect.Comments)
                .WithSalesAdvisor(prospect.SalesAdvisor)
                .WithIsActive(true)
                .WithAuditFields()
                .Build();

            Operation operation = new Operation.Builder()
                .WithOperationName(OperationTypes.PromoteClient)
                .WithOperationType(OperationTypes.PromoteClient)
                .Build();


            operation.SetClient(client);

            client.Operations.Add(operation);

            _context.Clients.Add(client);
            _context.Prospects.Remove(prospect);
            _context.SaveChanges();

            return string.Empty;
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_prospectDomainService != null) _prospectDomainService.Dispose();
        }
    }
}