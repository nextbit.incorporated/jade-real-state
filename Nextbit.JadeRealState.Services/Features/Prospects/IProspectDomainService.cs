using System;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Prospects
{
    public interface IProspectDomainService : IDisposable
    {
        Prospect Create(ProspectRequest request, User salesAdvisor, Project project, ContactType contactType);
        Prospect Update(ProspectRequest request, Prospect prospectOldInfo, User salesAdvisor);
    }
}