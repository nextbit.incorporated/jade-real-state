using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.Projects;

namespace Nextbit.JadeRealState.Services.Features.Prospects
{
    public class ProspectDTO : ResponseBase
    {
        public DateTime RegisterDate { get; set; }
        public int? ProjectId { get; set; }
        public int? ContactType { get; set; }
        public string Name { get; set; }
        public string IdNumber { get; set; }
        public string ContactNumberOne { get; set; }
        public string ContactNumberTwo { get; set; }
        public string Email { get; set; }
        public string ProjectName { get; set; }
        public string ContactTypeName { get; set; }
        public string TransactionOrigin { get; set; }
        public string Comments { get; set; }
        public int? SalesAdvisorId { get; set; }
        public string SalesAdvisor { get; set; }

        public ProjectsDTO Project { get; set; }
        public ContactTypeDto ContactTypes { get; set; }

        internal static ProspectDTO From(Prospect prospect)
        {
            return new ProspectDTO
            {
                Id = prospect.Id,
                CreationDate = prospect.CreationDate,
                RegisterDate = prospect.RegisterDate,
                ProjectId = prospect.ProjectId,
                ContactType = prospect.ContactType,
                Name = prospect.Name,
                IdNumber = prospect.IdNumber,
                ContactNumberOne = prospect.ContactNumberOne,
                ContactNumberTwo = prospect.ContactNumberTwo,
                Email = prospect.Email,
                ProjectName = prospect?.Project?.Name,
                ContactTypeName = prospect?.ContactTypes?.Name,
                TransactionOrigin = prospect.TransactionOrigin,
                Project = ProjectsDTO.From(prospect.Project),
                ContactTypes = ContactTypeDto.From(prospect.ContactTypes),
                SalesAdvisorId = prospect.SalesAdvisorId,
                SalesAdvisor = prospect.SalesAdvisor != null ? $"{prospect.SalesAdvisor?.FirstName} {prospect.SalesAdvisor?.LastName}" : null,
                Comments = prospect.Comments

            };
        }

        internal static List<ProspectDTO> From(IEnumerable<Prospect> prospects)
        {
            return (from s in prospects select ProspectDTO.From(s)).ToList();
        }

    }
}