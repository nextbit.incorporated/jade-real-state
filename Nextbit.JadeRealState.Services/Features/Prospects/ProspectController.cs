using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Prospects {
    [Route ("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProspectController : ControllerBase {
        private readonly IProspectAppService _prospectAppService;

        public ProspectController (IProspectAppService prospectAppService) {
            _prospectAppService = prospectAppService ?? throw new ArgumentException (nameof (prospectAppService));
        }

        [HttpGet]
        [Route ("paged")]
        [Authorize]
        public ActionResult<ProspectPagedDTO> GetPaged ([FromQuery] ProspectPagedRequest request) {
            return Ok (_prospectAppService.GetPaged (request));
        }

        [HttpGet]
        [Route ("move-to-client")]
        [Authorize]
        public ActionResult<ProspectPagedDTO> GetPaged (int Id) {
            return Ok (_prospectAppService.ConvertProspectInClient (Id));
        }

        [HttpPost]
        [Route ("validate-create-new-client")]
        [Authorize]
        public ActionResult<ProspectDTO> TryValidateNewClient ([FromBody] ProspectRequest newClient) {
            return _prospectAppService.TryCreateNewClientProspect(newClient);
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ProspectDTO> Post ([FromBody] ProspectRequest request) {
            return Ok (_prospectAppService.Create (request));
        }

        [HttpPost]
        [Route ("import-info")]
        [Authorize]
        public ActionResult<ProspectDTO> PostImport ([FromBody] ImportInfoExcelRequest request) {
            return Ok (_prospectAppService.ImportExcelData (request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<ProspectDTO> Put ([FromBody] ProspectRequest request) {
            return Ok (_prospectAppService.Update (request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete (int Id) {
            return Ok (_prospectAppService.Delete (Id));
        }
    }
}