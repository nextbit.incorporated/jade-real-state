using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.SupplierServices
{
    public class SupplierServiceAppService : ISupplierServiceAppService
    {
        private RealStateContext _context;
        private readonly ISupplierServiceDomainService _supplierServiceDomainService;

        public SupplierServiceAppService(RealStateContext context, ISupplierServiceDomainService supplierServiceDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (supplierServiceDomainService == null) throw new ArgumentException(nameof(supplierServiceDomainService));

            _context = context;
            _supplierServiceDomainService = supplierServiceDomainService;
        }

        public SupplierServiceDTO CreateSupplierService(SupplierServiceRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            SupplierService newSupplierService = _supplierServiceDomainService.CreateSupplierService(request);

            _context.SupplierService.Add(newSupplierService);
            _context.SaveChanges();

            return SupplierServiceDTO.From(newSupplierService);
        }

        public string DeleteSupplierService(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var supplierService = _context
            .SupplierService
            .Include(s => s.ServiceProcesses)
            .FirstOrDefault(s => s.Id == id);

            if (supplierService == null) throw new Exception("Servicio not exists");
            _context.SupplierService.Remove(supplierService);
            _context.SaveChanges();

            return string.Empty;
        }

        public SupplierServicePagedDTO GetPagedSupplierService(SupplierServicePagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            int count = _context.SupplierService.Count();
            var totalPage = Math.Ceiling((decimal)count / request.PageSize);

            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var lista = _context.SupplierService
                .Include(s => s.ServiceProcesses)
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1))
                .Take(request.PageSize)
                .ToList();

                return SupplierServicePagedDTO.From(request, totalPage, lista);
            }
            else
            {
                var lista = _context.SupplierService
                .Include(s => s.ServiceProcesses)
                .Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1))
                .Take(request.PageSize)
                .ToList();

                return SupplierServicePagedDTO.From(request, totalPage, lista);
            }
        }

        public SupplierServiceDTO UpdateSupplierService(SupplierServiceRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.ServiceProcesses == null) throw new ArgumentException(nameof(request.ServiceProcesses));

            var oldSupplierService = _context.SupplierService
            .Include(s => s.ServiceProcesses)
            .FirstOrDefault(s => s.Id == request.Id);
            if (oldSupplierService == null) return new SupplierServiceDTO { ValidationErrorMessage = "Servicio inexistente" };

            SupplierService supplierServiceUpdate = _supplierServiceDomainService.UpdateSupplierService(request, oldSupplierService);

            _context.SupplierService.Update(oldSupplierService);
            _context.SaveChanges();

            return SupplierServiceDTO.From(oldSupplierService);
        }

        public async Task<List<SupplierServiceDTO>> GetSupplierServicesAsync()
        {
            var lista = await _context.SupplierService
               .Include(s => s.ServiceProcesses)
               .ToListAsync();

            return lista.Select(s => SupplierServiceDTO.From(s)).ToList();
        }

        public IEnumerable<ServiceProcessDto> AddNewProcess(ServiceProcessDto request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            var newProcess = new ServiceProcess.Builder()
                                 .WithName(request.Name)
                                 .WithOrder(request.Order)
                                 .WithSupplierServiceId(request.SupplierServiceId)
                                 .WithAuditFields()
                                 .Build();
            _context.ServiceProcesses.Add(newProcess);
            _context.SaveChanges();

            var process = _context.ServiceProcesses
                        .Where(s => s.SupplierServiceId == request.SupplierServiceId)
                        .Include(s => s.SupplierService);
            return ServiceProcessDto.From(process);
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_supplierServiceDomainService != null) _supplierServiceDomainService.Dispose();
        }


    }
}