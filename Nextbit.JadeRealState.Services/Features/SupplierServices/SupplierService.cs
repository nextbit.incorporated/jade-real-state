using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.ServicesTracking;

namespace Nextbit.JadeRealState.Services.Features.SupplierServices
{
    [Table("SupplierServices")]
    public sealed class SupplierService : Entity
    {
        private SupplierService()
        {
            ServiceProcesses = new HashSet<ServiceProcess>();
            ServiceTrackings = new HashSet<ServiceTracking>();
            Clients = new HashSet<Client>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public ICollection<Negotiation> Negotiations { get; set; }
        public ICollection<ServiceProcess> ServiceProcesses
        {
            get { return _serviceProcesses ?? (_serviceProcesses = new HashSet<ServiceProcess>()); }
            private set { _serviceProcesses = value; }
        }
        private ICollection<ServiceProcess> _serviceProcesses;

        public ICollection<ServiceTracking> ServiceTrackings
        {
            get { return _serviceTrackings ?? (_serviceTrackings = new HashSet<ServiceTracking>()); }
            private set { _serviceTrackings = value; }
        }
        private ICollection<ServiceTracking> _serviceTrackings;


        public ICollection<Client> Clients
        {
            get { return _clients ?? (_clients = new HashSet<Client>()); }
            private set { _clients = value; }
        }
        private ICollection<Client> _clients;


        public void Update(string _name, string _description, List<ServiceProcessDto> serviceProcesseses)
        {
            Name = _name;
            Description = _description;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedSupplierService";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
            Status = "ACTIVO";


            // ServiceProcesses.Clear();

            // foreach (ServiceProcessDto serviceProcess in serviceProcesseses)
            // {
            //     ServiceProcess newServiceProcess = new ServiceProcess.Builder()
            //     .WithName(serviceProcess.Name)
            //     .WithOrder(serviceProcess.Order)
            //     .WithAuditFields()
            //     .Build();

            //     ServiceProcesses.Add(newServiceProcess);
            // }
        }

        public class Builder
        {
            private readonly SupplierService _supplierService = new SupplierService();

            public Builder WithName(string name)
            {
                _supplierService.Name = name;
                return this;
            }

            public Builder WithDescription(string description)
            {
                _supplierService.Description = description;
                return this;
            }

            public Builder WithAuditFields()
            {
                _supplierService.CrudOperation = "Added";
                _supplierService.TransactionDate = DateTime.Now;
                _supplierService.TransactionType = "NewSupplierService";
                _supplierService.ModifiedBy = "Service";
                _supplierService.TransactionUId = Guid.NewGuid();
                _supplierService.Status = "ACTIVO";

                return this;
            }

            public SupplierService Build()
            {
                return _supplierService;
            }
        }
    }
}