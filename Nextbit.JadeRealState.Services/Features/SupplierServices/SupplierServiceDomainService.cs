using System;

namespace Nextbit.JadeRealState.Services.Features.SupplierServices
{
    public class SupplierServiceDomainService : ISupplierServiceDomainService
    {
        public SupplierService CreateSupplierService(SupplierServiceRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));

            SupplierService supplierService = new SupplierService.Builder()
           .WithName(request.Name)
           .WithDescription(request.Description ?? string.Empty)
           .WithAuditFields()
           .Build();

            foreach (ServiceProcessDto serviceProcess in request.ServiceProcesses)
            {
                ServiceProcess newServiceProcess = new ServiceProcess.Builder()
                .WithName(serviceProcess.Name)
                .WithOrder(serviceProcess.Order)
                .WithAuditFields()
                .Build();

                supplierService.ServiceProcesses.Add(newServiceProcess);
            }

            return supplierService;
        }

        public SupplierService UpdateSupplierService(SupplierServiceRequest request, SupplierService supplierServiceOldInfo)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (supplierServiceOldInfo == null) throw new ArgumentException(nameof(supplierServiceOldInfo));

            supplierServiceOldInfo.Update(request.Name, request.Description ?? string.Empty, request.ServiceProcesses);
            return supplierServiceOldInfo;
        }

        public void Dispose()
        {
        }

    }
}