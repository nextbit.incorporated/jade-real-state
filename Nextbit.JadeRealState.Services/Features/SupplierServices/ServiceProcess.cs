using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.NegotiationPhases;

namespace Nextbit.JadeRealState.Services.Features.SupplierServices
{
    [Table("ServiceProcesses")]
    public sealed class ServiceProcess : Entity
    {
        private ServiceProcess()
        {

        }

        public int SupplierServiceId { get; private set; }
        public string Name { get; private set; }
        public int Order { get; private set; }

        public SupplierService SupplierService { get; set; }
        public ICollection<NegotiationPhase> NegotiationPhases { get; set; }

        private void SetAuditFields(string crudOperation)
        {
            CrudOperation = crudOperation;
            TransactionDate = DateTime.Now;
            TransactionType = "ServiceProcessesManagement";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
        }

        public class Builder
        {
            private readonly ServiceProcess _serviceProcess = new ServiceProcess();

            public Builder WithSupplierService(SupplierService supplierService)
            {
                _serviceProcess.SupplierService = supplierService;
                _serviceProcess.SupplierServiceId = supplierService.Id;

                return this;
            }

            public Builder WithSupplierServiceId(int supplierServiceId)
            {
                _serviceProcess.SupplierServiceId = supplierServiceId;

                return this;
            }

            public Builder WithName(string name)
            {
                _serviceProcess.Name = name;
                return this;
            }

            public Builder WithOrder(int order)
            {
                _serviceProcess.Order = order;
                return this;
            }

            public Builder WithAuditFields()
            {
                _serviceProcess.CrudOperation = "Added";
                _serviceProcess.TransactionDate = DateTime.Now;
                _serviceProcess.TransactionType = "NewClient";
                _serviceProcess.ModifiedBy = "Service";
                _serviceProcess.TransactionUId = Guid.NewGuid();
                return this;
            }

            public ServiceProcess Build()
            {
                return _serviceProcess;
            }
        }
    }
}