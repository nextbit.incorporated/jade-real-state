using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.SupplierServices
{
    public class SupplierServiceRequest : RequestBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public List<ServiceProcessDto> ServiceProcesses
        {
            get { return _serviceProcesseses ?? (_serviceProcesseses = new List<ServiceProcessDto>()); }
            set { _serviceProcesseses = value; }
        }
        private List<ServiceProcessDto> _serviceProcesseses;


    }

    public class SupplierServicePagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
    }
}