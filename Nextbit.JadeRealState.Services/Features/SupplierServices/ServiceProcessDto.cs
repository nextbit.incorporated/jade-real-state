using System.Collections.Generic;
using System.Linq;

namespace Nextbit.JadeRealState.Services.Features.SupplierServices
{
    public sealed class ServiceProcessDto
    {
        public int Id { get; set; }
        public int SupplierServiceId { get; set; }
        public string Name { get; set; }
        public int Order { get; set; }
        public string SupplierServiceName { get; set; }

        public static List<ServiceProcessDto> From(IEnumerable<ServiceProcess> serviceProcesses)
        {
            return (from qry in serviceProcesses select From(qry)).ToList();
        }

        public static ServiceProcessDto From(ServiceProcess serviceProcess)
        {
            return new ServiceProcessDto
            {
                Id = serviceProcess.Id,
                Name = serviceProcess.Name,
                Order = serviceProcess.Order
            };

        }
    }
}