using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.SupplierServices
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SupplierServiceController : ControllerBase
    {
        private readonly ISupplierServiceAppService _supplierServiceAppService;

        public SupplierServiceController(ISupplierServiceAppService supplierServiceAppService)
        {
            if (supplierServiceAppService == null) throw new ArgumentException(nameof(supplierServiceAppService));

            _supplierServiceAppService = supplierServiceAppService;
        }


        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<IEnumerable<SupplierServiceDTO>> GetPaged([FromQuery] SupplierServicePagedRequest request)
        {
            return Ok(_supplierServiceAppService.GetPagedSupplierService(request));
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetSupplierServicesAsync()
        {
            return Ok(await _supplierServiceAppService.GetSupplierServicesAsync());
        }


        [HttpPost]
        [Authorize]
        public ActionResult<SupplierServiceDTO> Post([FromBody] SupplierServiceRequest request)
        {
            return Ok(_supplierServiceAppService.CreateSupplierService(request));
        }

        [HttpPost]
        [Route("add-process")]
        [Authorize]
        public ActionResult<IEnumerable<ServiceProcessDto>> AddNewProcess([FromBody] ServiceProcessDto request)
        {
            return Ok(_supplierServiceAppService.AddNewProcess(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<SupplierServiceDTO> Put([FromBody] SupplierServiceRequest request)
        {
            return Ok(_supplierServiceAppService.UpdateSupplierService(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_supplierServiceAppService.DeleteSupplierService(Id));
        }
    }
}