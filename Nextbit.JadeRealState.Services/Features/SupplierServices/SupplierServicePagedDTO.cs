using System;
using System.Collections.Generic;
using System.Linq;

namespace Nextbit.JadeRealState.Services.Features.SupplierServices
{
    public class SupplierServicePagedDTO
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<SupplierServiceDTO> SupplierServices { get; set; }

        internal static SupplierServicePagedDTO From(SupplierServicePagedRequest request, decimal totalPage, List<SupplierService> lista)
        {
            return new SupplierServicePagedDTO
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                PageCount = (int)totalPage == 0 ? 1 : (int)totalPage,
                SupplierServices = lista.Select(s => new SupplierServiceDTO
                {
                    Id = s.Id,
                    Name = s.Name,
                    Description = s.Description,
                    Status = s.Status,
                    ServiceProcesses = ServiceProcessDto.From(s.ServiceProcesses.OrderBy(o => o.Order))

                }).ToList()
            };
        }
    }
}