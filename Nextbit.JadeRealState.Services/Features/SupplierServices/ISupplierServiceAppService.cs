using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Features.SupplierServices
{
    public interface ISupplierServiceAppService : IDisposable
    {
        SupplierServicePagedDTO GetPagedSupplierService(SupplierServicePagedRequest request);
        SupplierServiceDTO CreateSupplierService(SupplierServiceRequest request);
        SupplierServiceDTO UpdateSupplierService(SupplierServiceRequest request);
        Task<List<SupplierServiceDTO>> GetSupplierServicesAsync();
        string DeleteSupplierService(int id);

        IEnumerable<ServiceProcessDto> AddNewProcess(ServiceProcessDto request);
    }
}