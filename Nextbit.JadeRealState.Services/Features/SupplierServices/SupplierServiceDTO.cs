using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.SupplierServices
{
    public class SupplierServiceDTO : ResponseBase
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public List<ServiceProcessDto> ServiceProcesses
        {
            get { return _serviceProcesseses ?? (_serviceProcesseses = new List<ServiceProcessDto>()); }
            set { _serviceProcesseses = value; }
        }
        private List<ServiceProcessDto> _serviceProcesseses;

        public static SupplierServiceDTO From(SupplierService supplierService)
        {
            if (supplierService == null)
            {
                return null;
            }

            return new SupplierServiceDTO
            {
                Id = supplierService.Id,
                Name = supplierService.Name,
                Description = supplierService.Description,
                ServiceProcesses = ServiceProcessDto.From(supplierService.ServiceProcesses)
            };
        }

    }
}