using System;

namespace Nextbit.JadeRealState.Services.Features.SupplierServices
{
    public interface ISupplierServiceDomainService : IDisposable
    {
        SupplierService CreateSupplierService(SupplierServiceRequest request);
        SupplierService UpdateSupplierService(SupplierServiceRequest request, SupplierService supplierServiceOldInfo); 
    }
}