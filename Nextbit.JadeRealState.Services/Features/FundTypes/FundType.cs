using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.FinancialPrequalifications;

namespace Nextbit.JadeRealState.Services.Features.FundTypes
{
    [Table("FundTypes")]
    public sealed class FundType : Entity
    {
        private FundType()
        {
            FinancialPrequalifications = new HashSet<FinancialPrequalification>();
        }

        public FundType(string name)
        {
            Name = name;

            SetAuditFields("Added");
        }

        public string Name { get; private set; }


        public ICollection<FinancialPrequalification> FinancialPrequalifications
        {
            get { return _financialPrequalifications ?? (_financialPrequalifications = new HashSet<FinancialPrequalification>()); }
            private set { _financialPrequalifications = value; }
        }
        private ICollection<FinancialPrequalification> _financialPrequalifications;
    }
}