using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.FundTypes
{
    [Route(@"api/fund-types")]
    [ApiController]
    public class FundTypesController : ControllerBase
    {
        private readonly RealStateContext _context;

        public FundTypesController(RealStateContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get([FromQuery] int id)
        {
            if (id > 0)
            {
                FundType fundType = await _context.FundTypes.FindAsync(id);

                if (fundType != null)
                {
                    return Ok(new FundTypeDto { Id = fundType.Id, Name = fundType.Name });
                }

                return NotFound();
            }

            List<FundType> fundTypes = await _context.FundTypes.ToListAsync();

            List<FundTypeDto> fundTypeDtos = (from qry in fundTypes select new FundTypeDto { Id = qry.Id, Name = qry.Name }).ToList();

            return Ok(fundTypeDtos);
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Post([FromBody] FundTypeDto fundType)
        {
            FundType existingfundType = await _context.FundTypes.FirstOrDefaultAsync(f => f.Name == fundType.Name);

            if (existingfundType == null)
            {
                existingfundType = new FundType(fundType.Name);
                await _context.FundTypes.AddAsync(existingfundType);

                await _context.SaveChangesAsync();
            }

            return Ok(new FundTypeDto { Id = existingfundType.Id, Name = existingfundType.Name });
        }
    }
}