using System;
using System.Collections.Generic;
using System.Linq;

namespace Nextbit.JadeRealState.Services.Features.FundTypes
{
    public sealed class FundTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        internal static List<FundTypeDto> From(List<FundType> fundTypes)
        {
            return (from qry in fundTypes select From(qry)).ToList();
        }

        private static FundTypeDto From(FundType fundType)
        {
            return new FundTypeDto { Id = fundType.Id, Name = fundType.Name };
        }
    }
}