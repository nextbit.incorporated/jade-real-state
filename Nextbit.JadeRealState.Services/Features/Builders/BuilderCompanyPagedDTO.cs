using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Builders
{
    public class BuilderCompanyPagedDTO
    {
            public int PageSize { get; set; }
            public int PageIndex { get; set; }
            public int PageCount { get; set; }
            public List<BuilderCompanyDTO> BuilderCompanies { get; set; }
    }
}