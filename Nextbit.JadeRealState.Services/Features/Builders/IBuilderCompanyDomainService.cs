using System;

namespace Nextbit.JadeRealState.Services.Features.Builders
{
    public interface IBuilderCompanyDomainService : IDisposable
    {
        BuilderCompany CreateBuilderCompany(BuilderCompanyRequest request);
        BuilderCompany UpdateBuilderCompany(BuilderCompanyRequest request, BuilderCompany builderCompanyOldInfo);   
    }
}