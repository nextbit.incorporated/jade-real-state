using System;

namespace Nextbit.JadeRealState.Services.Features.Builders
{
    public interface IBuilderCompanyAppService : IDisposable
    {
        BuilderCompanyPagedDTO GetPagedBuilderCompany(BuilderCompanyPagedRequest request);
        BuilderCompanyDTO CreateBuilderCompany(BuilderCompanyRequest request);
        BuilderCompanyDTO UpdateBuilderCompany(BuilderCompanyRequest request);
        string DeleteBuilderCompany(int id);
    }
}