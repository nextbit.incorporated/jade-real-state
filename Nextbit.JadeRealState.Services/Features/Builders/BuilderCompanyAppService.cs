using System;
using System.Linq;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Builders
{
    public class BuilderCompanyAppService : IBuilderCompanyAppService
    {
        private RealStateContext _context;
        private readonly IBuilderCompanyDomainService _builderCompanyDomainService;

        public BuilderCompanyAppService(RealStateContext context, IBuilderCompanyDomainService builderCompanyDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (builderCompanyDomainService == null) throw new ArgumentException(nameof(builderCompanyDomainService));

            _context = context;
            _builderCompanyDomainService = builderCompanyDomainService;
        }
        public BuilderCompanyDTO CreateBuilderCompany(BuilderCompanyRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newBuilderCompany = _builderCompanyDomainService.CreateBuilderCompany(request);

            _context.BuilderCompany.Add(newBuilderCompany);
            _context.SaveChanges();

            return new BuilderCompanyDTO
            {
                Name = newBuilderCompany.Name,
                Description = newBuilderCompany.Description,
                BusinessName = newBuilderCompany.BusinessName,
                ContactNumber = newBuilderCompany.ContactNumber,
                ElectronicAddresses = newBuilderCompany.ElectronicAddresses,
                Email = newBuilderCompany.Email,
                Address = newBuilderCompany.Address,
                Owner = newBuilderCompany.Owner,
                OwnerContactNumber = newBuilderCompany.OwnerContactNumber,
                IdNumber = newBuilderCompany.IdNumber,
                Status = newBuilderCompany.Status
            };
        }

        public BuilderCompanyDTO UpdateBuilderCompany(BuilderCompanyRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldBuilderCompany = _context.BuilderCompany.FirstOrDefault(s => s.Id == request.Id);
            if (oldBuilderCompany == null) return new BuilderCompanyDTO { ValidationErrorMessage = "Constructora inexistente" };

            var rolUpdate = _builderCompanyDomainService.UpdateBuilderCompany(request, oldBuilderCompany);

            _context.SaveChanges();

            return new BuilderCompanyDTO
            {
                Id = oldBuilderCompany.Id,
                Name = oldBuilderCompany.Name,
                Description = oldBuilderCompany.Description,
                BusinessName = oldBuilderCompany.BusinessName,
                ContactNumber = oldBuilderCompany.ContactNumber,
                ElectronicAddresses = oldBuilderCompany.ElectronicAddresses,
                Email = oldBuilderCompany.Email,
                Address = oldBuilderCompany.Address,
                Owner = oldBuilderCompany.Owner,
                OwnerContactNumber = oldBuilderCompany.OwnerContactNumber,
                IdNumber = oldBuilderCompany.IdNumber,
                Status = oldBuilderCompany.Status
            };
        }

        public string DeleteBuilderCompany(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var builderCompany = _context.BuilderCompany.FirstOrDefault(s => s.Id == id);
            if (builderCompany == null) throw new Exception("Constructora not exists");
            _context.BuilderCompany.Remove(builderCompany);
            _context.SaveChanges();

            return string.Empty;
        }

        public BuilderCompanyPagedDTO GetPagedBuilderCompany(BuilderCompanyPagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            var count = Convert.ToDecimal(_context.BuilderCompany.Count());
            var totalPage = Math.Ceiling(count / request.PageSize);
            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;
            if (string.IsNullOrEmpty(request.Value))
            {
                var lista = _context.BuilderCompany
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new BuilderCompanyPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    BuilderCompanies = lista.Select(s => new BuilderCompanyDTO
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description,
                        BusinessName = s.BusinessName,
                        ContactNumber = s.ContactNumber,
                        ElectronicAddresses = s.ElectronicAddresses,
                        Email = s.Email,
                        Address = s.Address,
                        Owner = s.Owner,
                        OwnerContactNumber = s.OwnerContactNumber,
                        IdNumber = s.IdNumber,
                        Status = s.Status
                    }).ToList()
                };
            }
            else
            {
                var lista = _context.BuilderCompany.Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new BuilderCompanyPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    BuilderCompanies = lista.Select(s => new BuilderCompanyDTO
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description,
                        BusinessName = s.BusinessName,
                        ContactNumber = s.ContactNumber,
                        ElectronicAddresses = s.ElectronicAddresses,
                        Email = s.Email,
                        Address = s.Address,
                        Owner = s.Owner,
                        OwnerContactNumber = s.OwnerContactNumber,
                        IdNumber = s.IdNumber,
                        Status = s.Status
                    }).ToList()
                };
            }
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_builderCompanyDomainService != null) _builderCompanyDomainService.Dispose();
        }
    }
}