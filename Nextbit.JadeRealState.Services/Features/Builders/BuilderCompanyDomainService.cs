using System;

namespace Nextbit.JadeRealState.Services.Features.Builders
{
    public class BuilderCompanyDomainService : IBuilderCompanyDomainService
    {
        public BuilderCompany CreateBuilderCompany(BuilderCompanyRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));

            BuilderCompany builderCompany = new BuilderCompany.Builder()
            .WithName(request.Name)
            .WithDescription(request.Description)
            .WithBusinessName(request.BusinessName)
            .WithContactNumber(request.ContactNumber)
            .WithEmail(request.Email)
            .WithElectronicAddresses(request.ElectronicAddresses)
            .WithAddress(request.Address)
            .WithOwner(request.Owner)
            .WithOwnerContactNumber(request.OwnerContactNumber)
            .WithIdNumber(request.IdNumber)
            .WithAuditFields()
            .Build();

            return builderCompany;
        }

        public BuilderCompany UpdateBuilderCompany(BuilderCompanyRequest request, BuilderCompany builderCompanyOldInfo)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (builderCompanyOldInfo == null) throw new ArgumentException(nameof(builderCompanyOldInfo));

            builderCompanyOldInfo.Update(
                request.Name, request.Description, request.BusinessName, request.ContactNumber, 
                request.ElectronicAddresses, request.Email, request.Address, request.Owner, request.OwnerContactNumber, request.IdNumber);
            return builderCompanyOldInfo;
        }

        public void Dispose()
        {
            
        }
    }
}