using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Builders
{
    public class BuilderCompanyRequest : RequestBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string BusinessName { get; set; }
        public string ContactNumber { get; set; }
        public string ElectronicAddresses { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Owner { get; set; }
        public string OwnerContactNumber { get; set; }
        public string IdNumber { get; set; }
    }

    public class BuilderCompanyPagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
    }
}