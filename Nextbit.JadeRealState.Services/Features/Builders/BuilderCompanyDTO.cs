using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Builders
{
    public class BuilderCompanyDTO : ResponseBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string BusinessName { get; set; }
        public string ContactNumber { get; set; }
        public string ElectronicAddresses { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Owner { get; set; }
        public string OwnerContactNumber { get; set; }
        public string IdNumber { get; set; }

        internal static BuilderCompanyDTO From(BuilderCompany builderCompany)
        {
            if (builderCompany == null)
            {
                return new BuilderCompanyDTO();
            }

            return new BuilderCompanyDTO
            {
                Id = builderCompany.Id,
                Name = builderCompany.Name,
                Description = builderCompany.Description,
                BusinessName = builderCompany.BusinessName,
                ContactNumber = builderCompany.ContactNumber,
                ElectronicAddresses = builderCompany.ElectronicAddresses,
                Email = builderCompany.Email,
                Address = builderCompany.Address,
                Owner = builderCompany.Owner,
                OwnerContactNumber = builderCompany.OwnerContactNumber,
                IdNumber = builderCompany.IdNumber,
            };
        }

        internal static List<BuilderCompanyDTO> From(List<BuilderCompany> builders)
        {
            return (from query in builders select From(query)).ToList();
        }
    }
}