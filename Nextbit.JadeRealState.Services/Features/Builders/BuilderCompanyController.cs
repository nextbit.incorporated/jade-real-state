using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Builders
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BuilderCompanyController : ControllerBase
    {
        private readonly IBuilderCompanyAppService _builderCompanyAppService;
        public BuilderCompanyController(IBuilderCompanyAppService builderCompanyAppService)
        {
            if (builderCompanyAppService == null) throw new ArgumentException(nameof(builderCompanyAppService));

            _builderCompanyAppService = builderCompanyAppService;
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<BuilderCompanyPagedDTO> GetPaged([FromQuery]BuilderCompanyPagedRequest request)
        {
            return Ok(_builderCompanyAppService.GetPagedBuilderCompany(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<BuilderCompanyDTO> Post([FromBody]BuilderCompanyRequest request)
        {
            return Ok(_builderCompanyAppService.CreateBuilderCompany(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<BuilderCompanyDTO> Put([FromBody] BuilderCompanyRequest request)
        {
            return Ok(_builderCompanyAppService.UpdateBuilderCompany(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_builderCompanyAppService.DeleteBuilderCompany(Id));
        }
    }
}