using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.NegotiationContracts;

namespace Nextbit.JadeRealState.Services.Features.Builders
{
    [Table("BuilderCompany")]
    public class BuilderCompany : Entity
    {
        public BuilderCompany()
        {
            NegotiationContracts = new HashSet<NegotiationContract>();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string BusinessName { get; set; }
        public string ContactNumber { get; set; }
        public string ElectronicAddresses { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Owner { get; set; }
        public string OwnerContactNumber { get; set; }
        public string Status { get; set; }
        public string IdNumber { get; set; }


        public ICollection<NegotiationContract> NegotiationContracts
        {
            get { return _negotiationContracts ?? (_negotiationContracts = new HashSet<NegotiationContract>()); }
            private set { _negotiationContracts = value; }
        }
        private ICollection<NegotiationContract> _negotiationContracts;

        public void Update(
            string _name, string _description, string _businessName, string _contactNumber,
            string _electronicAddresses, string _email, string _address, string _owner, string _ownerContactNumber, string _idNumber)
        {
            Name = _name;
            Description = _description;
            BusinessName = _businessName;
            ContactNumber = _contactNumber;
            ElectronicAddresses = _electronicAddresses;
            Email = _email;
            Address = _address;
            Owner = _owner;
            IdNumber = _idNumber;
            OwnerContactNumber = _ownerContactNumber;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedProject";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
            Status = "ACTIVO";
        }

        public class Builder
        {
            private readonly BuilderCompany _company = new BuilderCompany();

            public Builder WithName(string name)
            {
                _company.Name = name;
                return this;
            }

            public Builder WithDescription(string description)
            {
                _company.Description = description;
                return this;
            }

            public Builder WithBusinessName(string businessName)
            {
                _company.BusinessName = businessName;
                return this;
            }

            public Builder WithContactNumber(string contactNumber)
            {
                _company.ContactNumber = contactNumber;
                return this;
            }

            public Builder WithElectronicAddresses(string electronicAddresses)
            {
                _company.ElectronicAddresses = electronicAddresses;
                return this;
            }

            public Builder WithEmail(string email)
            {
                _company.Email = email;
                return this;
            }
            public Builder WithAddress(string address)
            {
                _company.Address = address;
                return this;
            }

            public Builder WithOwner(string owner)
            {
                _company.Owner = owner;
                return this;
            }

            public Builder WithOwnerContactNumber(string ownerContactNumber)
            {
                _company.OwnerContactNumber = ownerContactNumber;
                return this;
            }

            public Builder WithIdNumber(string idNumber)
            {
                _company.IdNumber = idNumber;
                return this;
            }

            public Builder WithAuditFields()
            {
                _company.CrudOperation = "Added";
                _company.TransactionDate = DateTime.Now;
                _company.TransactionType = "NewProject";
                _company.ModifiedBy = "Service";
                _company.TransactionUId = Guid.NewGuid();
                _company.Status = "ACTIVO";

                return this;
            }

            public BuilderCompany Build()
            {
                return _company;
            }
        }
    }
}