using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.ClientTransactions
{
    [Route("api/client-transactions")]
    [ApiController]
    public class ClientTransactionsController : ControllerBase
    {
        private readonly ClientTransactionsAppService _clientTransactionsAppService;

        public ClientTransactionsController(ClientTransactionsAppService clientTransactionsAppService)
        {
            if (clientTransactionsAppService == null) throw new ArgumentNullException(nameof(clientTransactionsAppService));

            _clientTransactionsAppService = clientTransactionsAppService;
        }

        [HttpGet]
        [Route("")]
        [Authorize]
        public ActionResult<PagedClientTransactions> GetClientTransactions([FromQuery] GetClientTransactionsRequest getClientTransactionsRequest)
        {
            PagedClientTransactions pagedClientTransactions = _clientTransactionsAppService.GetClientTransactions(getClientTransactionsRequest);

            return Ok(pagedClientTransactions);
        }
    }
}