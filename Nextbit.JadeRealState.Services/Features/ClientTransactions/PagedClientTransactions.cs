using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.ClientTransactions
{
    public class PagedClientTransactions
    {
        public int PageIndex { get; internal set; }
        public int PageSize { get; internal set; }
        public int TotalItems { get; internal set; }
        public int PageCount { get; internal set; }
        public string SearchValue { get; internal set; }
        public int SalesAdvisorId { get; internal set; }

        public List<ClientTransactionDto> Items
        {
            get { return _items ?? (_items = new List<ClientTransactionDto>()); }
            set { _items = value; }
        }
        private List<ClientTransactionDto> _items;
    }
}