using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.ClientTransactions
{
    public class ClientTransactionDto
    {
        public int UId { get; protected set; }
        public int Id { get; protected set; }
        public int? ParentClientId { get; set; }
        public string ClientCode { get; set; }
        public int? ProjectId { get; set; }
        public string Project { get; set; }
        public int? ContactTypeId { get; set; }
        public string ContactType { get; set; }
        public int? NationalityId { get; set; }
        public string Nationality { get; set; }
        public int? FinancialInstitutionId { get; set; }
        public string FinancialInstitution { get; set; }
        public int? ClientCategoryId { get; set; }
        public int? SalesAdvisorId { get; set; }
        public string SalesAdvisor { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string FirstSurname { get; set; }
        public string SecondSurname { get; set; }
        public string FullTextSearchField { get; set; }
        public string IdentificationCard { get; set; }
        public string PhoneNumber { get; set; }
        public string CellPhoneNumber { get; set; }
        public string Email { get; set; }
        public string Workplace { get; set; }
        public string DebtorCurrency { get; set; }
        public decimal GrossMonthlyIncome { get; set; }
        public string HomeAddress { get; set; }
        public bool OwnHome { get; set; }
        public bool ContributeToRap { get; set; }
        public string Comments { get; set; }
        public string ClientCreationComments { get; set; }
        public string CreditOfficer { get; set; }
        public string Profile { get; set; }
        public DateTime CreationDate { get; set; }
        public Guid TransactionUId { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime TransactionDate { get; set; }
        public string TransactionType { get; set; }
        public string CrudOperation { get; set; }

        internal static List<ClientTransactionDto> From(IEnumerable<ClientTransaction> clientTransactions)
        {
            return (from query in clientTransactions select From(query)).ToList();
        }

        private static ClientTransactionDto From(ClientTransaction clientTransaction)
        {
            return new ClientTransactionDto
            {
                UId = clientTransaction.UId,
                Id = clientTransaction.Id,
                ClientCode = clientTransaction.ClientCode,
                ProjectId = clientTransaction.ProjectId,
                Project = clientTransaction.Project,
                ContactTypeId = clientTransaction.ContactTypeId,
                ContactType = clientTransaction.ContactType,
                NationalityId = clientTransaction.NationalityId,
                Nationality = clientTransaction.Nationality,
                FinancialInstitutionId = clientTransaction.FinancialInstitutionId,
                FinancialInstitution = clientTransaction.FinancialInstitution,
                ClientCategoryId = clientTransaction.ClientCategoryId,
                SalesAdvisorId = clientTransaction.SalesAdvisorId,
                SalesAdvisor = clientTransaction.SalesAdvisor,
                FirstName = clientTransaction.FirstName,
                MiddleName = clientTransaction.MiddleName,
                FirstSurname = clientTransaction.FirstSurname,
                SecondSurname = clientTransaction.SecondSurname,
                FullTextSearchField = clientTransaction.FullTextSearchField,
                IdentificationCard = !string.IsNullOrWhiteSpace(clientTransaction.IdentificationCard) ? EncriptorHelper.DecryptString(clientTransaction.IdentificationCard) : null,
                PhoneNumber = !string.IsNullOrWhiteSpace(clientTransaction.PhoneNumber) ? EncriptorHelper.DecryptString(clientTransaction.PhoneNumber) : null,
                CellPhoneNumber = !string.IsNullOrWhiteSpace(clientTransaction.CellPhoneNumber) ? EncriptorHelper.DecryptString(clientTransaction.CellPhoneNumber) : null,
                Email = !string.IsNullOrWhiteSpace(clientTransaction.Email) ? EncriptorHelper.DecryptString(clientTransaction.Email) : null,
                HomeAddress = !string.IsNullOrWhiteSpace(clientTransaction.HomeAddress) ? EncriptorHelper.DecryptString(clientTransaction.HomeAddress) : null,
                Workplace = clientTransaction.Workplace,
                DebtorCurrency = clientTransaction.DebtorCurrency,
                GrossMonthlyIncome = clientTransaction.GrossMonthlyIncome,
                OwnHome = clientTransaction.OwnHome,
                ContributeToRap = clientTransaction.ContributeToRap,
                Comments = clientTransaction.Comments,
                ClientCreationComments = clientTransaction.ClientCreationComments,
                CreditOfficer = clientTransaction.CreditOfficer,
                CreationDate = clientTransaction.CreationDate,
                TransactionUId = clientTransaction.TransactionUId,
                ModifiedBy = clientTransaction.ModifiedBy,
                TransactionDate = clientTransaction.TransactionDate,
                TransactionType = clientTransaction.TransactionType,
                CrudOperation = clientTransaction.CrudOperation,
            };
        }
    }
}