using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.ClientTransactions
{
    public class ClientTransactionsAppService
    {
        private readonly RealStateContext _context;
        public ClientTransactionsAppService(RealStateContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            _context = context;

        }

        internal PagedClientTransactions GetClientTransactions(
            GetClientTransactionsRequest getClientTransactionsRequest)
        {
            int salesAdvisorId = getClientTransactionsRequest.SalesAdvisorId;
            string searchValue = getClientTransactionsRequest.SearchValue;
            int pageIndex = getClientTransactionsRequest.PageIndex;
            int pageSize = getClientTransactionsRequest.PageSize;

            IEnumerable<ClientTransaction> clientTransactionsQuery = _context.ClientTransactions;

            clientTransactionsQuery = salesAdvisorId != 0
                ? clientTransactionsQuery.Where(c => c.SalesAdvisorId == salesAdvisorId)
                : clientTransactionsQuery;

            clientTransactionsQuery = !string.IsNullOrWhiteSpace(searchValue)
                ? clientTransactionsQuery.Where(c => c.FullTextSearchField.Contains(searchValue))
                : clientTransactionsQuery;

            int totalItems = clientTransactionsQuery.Count();

            int totalPage = pageSize > 0 ? (int)Math.Ceiling((decimal)totalItems / (decimal)pageSize) : 0;

            if (pageIndex > totalPage)
            {
                pageIndex = totalPage;
            }

            if (pageIndex < 0)
            {
                pageIndex = 0;
            }

            IEnumerable<ClientTransaction> clientTransactions = clientTransactionsQuery
                .OrderByDescending(s => s.TransactionDate)
                .Skip(pageSize * pageIndex)
                .Take(pageSize)
                .ToList();


            List<ClientTransactionDto> clientList = ClientTransactionDto.From(clientTransactions);

            return new PagedClientTransactions
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItems = totalItems,
                PageCount = totalPage,
                SearchValue = searchValue,
                SalesAdvisorId = salesAdvisorId,
                Items = clientList
            };
        }
    }
}