using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    [Table("ClientTransactions")]
    public class ClientTransaction
    {
        private ClientTransaction()
        {

        }

        public ClientTransaction(Client client, string crudOperation, string user)
        {
            Id = client.Id;
            ClientCode = client.ClientCode;
            ProjectId = client.ProjectId;
            Project = client.Project?.Name;
            ContactTypeId = client.ContactTypeId;
            ContactType = client.ContactType?.Name;
            NationalityId = client.NationalityId;
            Nationality = client.Nationality?.Name;
            FinancialInstitutionId = client.FinancialInstitutionId;
            FinancialInstitution = client.FinancialInstitution?.Name;
            SalesAdvisorId = client.SalesAdvisorId;
            ClientCategoryId = client.ClientCategoryId;
            SalesAdvisor = client.SalesAdvisor?.GetName();
            FirstName = client.FirstName;
            MiddleName = client.MiddleName;
            FirstSurname = client.FirstSurname;
            SecondSurname = client.SecondSurname;
            FullTextSearchField = client.FullTextSearchField;
            IdentificationCard = client.IdentificationCard;
            PhoneNumber = client.PhoneNumber;
            CellPhoneNumber = client.CellPhoneNumber;
            Email = client.Email;
            Workplace = client.Workplace;
            WorkStartDate = client.WorkStartDate;
            DebtorCurrency = client.DebtorCurrency;
            GrossMonthlyIncome = client.GrossMonthlyIncome;
            HomeAddress = client.HomeAddress;
            OwnHome = client.OwnHome;
            ContributeToRap = client.ContributeToRap;
            Comments = client.Comments;
            ClientCreationComments = client.ClientCreationComments;
            CreditOfficer = client.CreditOfficer;
            CreationDate = client.CreationDate;
            TransactionUId = client.TransactionUId;
            ModifiedBy = user;
            TransactionDate = client.TransactionDate;
            TransactionType = client.TransactionType;
            CrudOperation = crudOperation;
            IsActive = client.IsActive;
            NegotiationEnded = client.NegotiationEnded;
            Profile = client.Profile;
        }

        [Key]
        public int UId { get; protected set; }
        public int Id { get; protected set; }
        public int? ParentClientId { get; private set; }
        public string ClientCode { get; private set; }
        public int? ProjectId { get; private set; }
        public string Project { get; private set; }
        public int? ContactTypeId { get; private set; }
        public string ContactType { get; private set; }
        public int? NationalityId { get; private set; }
        public string Nationality { get; private set; }
        public int? FinancialInstitutionId { get; private set; }
        public string FinancialInstitution { get; private set; }
        public int? ClientCategoryId { get; private set; }
        public int? SalesAdvisorId { get; private set; }
        public string SalesAdvisor { get; private set; }
        public string FirstName { get; set; }
        public string MiddleName { get; private set; }
        public string FirstSurname { get; set; }
        public string SecondSurname { get; private set; }
        public string FullTextSearchField { get; private set; }
        public string IdentificationCard { get; private set; }
        public string PhoneNumber { get; private set; }
        public string CellPhoneNumber { get; private set; }
        public string Email { get; set; }
        public string Workplace { get; private set; }
        public DateTime? WorkStartDate { get; set; }
        public string DebtorCurrency { get; private set; }
        public decimal GrossMonthlyIncome { get; private set; }
        public string HomeAddress { get; private set; }
        public bool OwnHome { get; private set; }
        public bool ContributeToRap { get; private set; }
        public string Comments { get; private set; }
        public string ClientCreationComments { get; private set; }
        public string CreditOfficer { get; private set; }
        public DateTime CreationDate { get; private set; } = DateTime.Now;

        public bool IsActive { get; private set; }
        public bool NegotiationEnded { get; private set; }
        public string Profile { get; private set; }

        public Guid TransactionUId { get; private set; }
        public string ModifiedBy { get; private set; }
        public DateTime TransactionDate { get; private set; }
        public string TransactionType { get; private set; }
        public string CrudOperation { get; private set; }
        [Timestamp]
        public byte[] RowVersion { get; private set; }

    }
}