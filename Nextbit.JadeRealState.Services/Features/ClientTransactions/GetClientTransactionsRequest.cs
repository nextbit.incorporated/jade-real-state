namespace Nextbit.JadeRealState.Services.Features.ClientTransactions
{
    public class GetClientTransactionsRequest
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string SearchValue { get; set; }
        public int SalesAdvisorId { get; set; }
    }
}