using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.ClientsLevel
{

    [Table("ClientLevels")]
    public class ClientLevel : Entity
    {
        public string Level { get; private set; }
        public string Description { get; private set; }
        
        public string PerfilCategoria { get; set; }
        
        public string Comment { get; private set; }

        public ICollection<Client> Clients { get; set; }

        public void Update(
          string _level, string _description, string _comment, string _user, string _perfilCategoria)
        {
            Level = _level;
            Description = _description;
            Comment = _comment;
            PerfilCategoria = _perfilCategoria;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = _user ?? "Service";
            TransactionUId = Guid.NewGuid();
        }

        public class Builder
        {
            private readonly ClientLevel _clientLevel = new ClientLevel();

            public Builder WithLevel(string level)
            {
                _clientLevel.Level = level;
                return this;
            }

            public Builder WithDescription(string description)
            {
                _clientLevel.Description = description;
                return this;
            }

            public Builder WithComment(string comment)
            {
                _clientLevel.Comment = comment;
                return this;
            }

            public Builder WithPerfilCategoria(string perfilCategoria)
            {
                _clientLevel.PerfilCategoria = perfilCategoria;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _clientLevel.CrudOperation = "Added";
                _clientLevel.TransactionDate = DateTime.Now;
                _clientLevel.TransactionType = "NewProject";
                _clientLevel.ModifiedBy = user ?? "Service";
                _clientLevel.TransactionUId = Guid.NewGuid();

                return this;
            }

            public ClientLevel Build()
            {
                return _clientLevel;
            }
        }
    }
}