using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.ClientsLevel
{
    public class ClientLevelDto : ResponseBase
    {
        public string Name { get; set; }
        public string Level { get; set; }
        public string Description { get; set; }
        public string PerfilCategoria { get; set; }
        public string Comment { get; set; }

        internal static List<ClientLevelDto> From(IEnumerable<ClientLevel> clientLevels)
        {
            return (from clientLevel in clientLevels select From(clientLevel)).ToList();
        }

        internal static ClientLevelDto From(ClientLevel clientLevel)
        {
            return new ClientLevelDto
            {
                Id = clientLevel.Id,
                Name = clientLevel.Level,
                Level = clientLevel.Level,
                Description = clientLevel.Description,
                Comment = clientLevel.Comment,
                PerfilCategoria = clientLevel.PerfilCategoria
            };
        }
    }
}