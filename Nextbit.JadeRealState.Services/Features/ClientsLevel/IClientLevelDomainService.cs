using System;

namespace Nextbit.JadeRealState.Services.Features.ClientsLevel
{
    public interface IClientLevelDomainService : IDisposable
    {
        ClientLevel Create(ClientLevelRequest request);
        ClientLevel Update(ClientLevelRequest request, ClientLevel _oldRegister);
    }
}