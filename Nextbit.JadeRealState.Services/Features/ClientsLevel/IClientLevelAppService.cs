using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Features.ClientsLevel
{
    public interface IClientLevelAppService : IDisposable
    {
        Task<List<ClientLevelDto>> GetLevelsAsync();
        ClientLevelPagedDto GetPaged(ClientLevelPagedRequest request);
        ClientLevelDto Create(ClientLevelRequest request);
        ClientLevelDto Update(ClientLevelRequest request);
        string Delete(int id);
    }
}