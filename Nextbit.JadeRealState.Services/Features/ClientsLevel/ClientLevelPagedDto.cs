using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.ClientsLevel
{
    public class ClientLevelPagedDto : ResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<ClientLevelDto> ClientLevels { get; set; }
    }
}