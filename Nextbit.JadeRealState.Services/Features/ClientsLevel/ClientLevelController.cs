using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.ClientsLevel
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class ClientLevelController : ControllerBase
    {
        private readonly IClientLevelAppService _clientLevelAppService;

        public ClientLevelController(IClientLevelAppService clientLevelAppService)
        {
            if (clientLevelAppService == null) throw new ArgumentException(nameof(clientLevelAppService));

            _clientLevelAppService = clientLevelAppService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetLevelsAsync()
        {
            return Ok(await _clientLevelAppService.GetLevelsAsync());
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<IEnumerable<ClientLevelDto>> GetPaged([FromQuery] ClientLevelPagedRequest request)
        {
            return Ok(_clientLevelAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ClientLevelDto> Post([FromBody] ClientLevelRequest request)
        {
            return Ok(_clientLevelAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<ClientLevelDto> Put([FromBody] ClientLevelRequest request)
        {
            return Ok(_clientLevelAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_clientLevelAppService.Delete(Id));
        }
    }
}