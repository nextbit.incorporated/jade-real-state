using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.ClientsLevel
{
    public class ClientLevelRequest : RequestBase
    {
        public string Level { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }  
        public int Id { get; set; }
        public string PerfilCategoria { get; set; }
    }

    public class ClientLevelPagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
        public int ValueId { get; set; }
    }
}