using System;

namespace Nextbit.JadeRealState.Services.Features.ClientsLevel
{
    public class ClientLevelDomainService : IClientLevelDomainService
    {
        public ClientLevel Create(ClientLevelRequest request)
        {
            if (string.IsNullOrEmpty(request.Level)) throw new ArgumentNullException(nameof(request.Level));

            ClientLevel registro = new ClientLevel.Builder()
            .WithLevel(request.Level)
            .WithDescription(request.Description)
            .WithComment(request.Comment)
            .WithAuditFields(request.User)
            .WithPerfilCategoria(request.PerfilCategoria)
            .Build();

            return registro;
        }

        public ClientLevel Update(ClientLevelRequest request, ClientLevel _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.Level, request.Description, request.Comment, request.User, request.PerfilCategoria);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}