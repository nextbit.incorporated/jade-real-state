using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.ClientsLevel
{
    public class ClientLevelAppService : IClientLevelAppService
    {
        private RealStateContext _context;
        private readonly IClientLevelDomainService _clientLevelDomainService;

        public ClientLevelAppService(RealStateContext context, IClientLevelDomainService clientLevelDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (clientLevelDomainService == null) throw new ArgumentException(nameof(clientLevelDomainService));

            _context = context;
            _clientLevelDomainService = clientLevelDomainService;
        }

        public ClientLevelDto Create(ClientLevelRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));

            var clientLevel = _clientLevelDomainService.Create(request);

            _context.ClientLevels.Add(clientLevel);
            _context.SaveChanges();

            return ClientLevelDto.From(clientLevel);
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var clienLevel = _context.ClientLevels.FirstOrDefault(s => s.Id == id);
            if (clienLevel == null) throw new Exception("Nivel not exists");
            _context.ClientLevels.Remove(clienLevel);
            _context.SaveChanges();

            return string.Empty;
        }

        public ClientLevelPagedDto GetPaged(ClientLevelPagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (string.IsNullOrEmpty(request.Value))
            {
                var count = Convert.ToDecimal(_context.Reserves.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;

                List<ClientLevel> clientLevels = _context.ClientLevels
                  .OrderByDescending(s => s.TransactionDate)
                  .Skip(request.PageSize * (request.PageIndex - 1))
                  .Take(request.PageSize)
                  .ToList();

                return new ClientLevelPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    ClientLevels = ClientLevelDto.From(clientLevels)
                };
            }
            else
            {
                List<ClientLevel> clientLevels = _context.ClientLevels
                .Where(s => s.Level.ToUpper().StartsWith(request.Value.ToUpper()))
                .Skip(request.PageSize * (request.PageIndex - 1))
                .Take(request.PageSize)
                .ToList();

                var count = Convert.ToDecimal(clientLevels.Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;

                return new ClientLevelPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    ClientLevels = ClientLevelDto.From(clientLevels)
                };
            }
        }

        public ClientLevelDto Update(ClientLevelRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));

            ClientLevel clientLevel = _context.ClientLevels.FirstOrDefault(s => s.Id == request.Id);
            if (clientLevel == null) return new ClientLevelDto { ValidationErrorMessage = "Nivel de cliente inexistente" };

            var newClientLevel = _clientLevelDomainService.Update(request, clientLevel);

            _context.ClientLevels.Update(clientLevel);
            _context.SaveChanges();

            return ClientLevelDto.From(clientLevel);
        }

        public async Task<List<ClientLevelDto>> GetLevelsAsync()
        {
            List<ClientLevel> clientLevels = await _context.ClientLevels.OrderBy(s => s.Level).ToListAsync();

            return ClientLevelDto.From(clientLevels);

        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_clientLevelDomainService != null) _clientLevelDomainService.Dispose();
        }
    }
}