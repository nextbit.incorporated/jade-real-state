using System;

namespace Nextbit.JadeRealState.Services.Features.NegotiationAttachments
{
    public class NegotiationAttachmentDomainService : INegotiationAttachmentDomainService
    {
        public NegotiationAttachment Create(NegotiationAttachmentRequest request)
        {
            if (request.NegotiationPhaseId == 0) throw new ArgumentNullException(nameof(request.NegotiationPhaseId));

            NegotiationAttachment registro = new NegotiationAttachment.Builder()
            .WithAttachment(request.Title, request.Attachment, request.ExtensionFile, request.FileType)
            .WithNegotiationPhaseId(request.NegotiationPhaseId)
            .WithDate(request.Date)
            .WithAuditFields(request.User)
            .Build();

            return registro;
        }

        public NegotiationAttachment Update(NegotiationAttachmentRequest request, NegotiationAttachment _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.Date, request.Title, request.User);

            return _oldRegister;
        }

        public void Dispose()
        {

        }

    }
}