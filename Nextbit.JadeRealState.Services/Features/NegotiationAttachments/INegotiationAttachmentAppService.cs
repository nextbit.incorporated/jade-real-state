using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.NegotiationAttachments
{
    public interface INegotiationAttachmentAppService : IDisposable
    {
        IEnumerable<NegotiationAttachmentDto> GetFiles(NegotiationAttachmentRequest request);
        NegotiationAttachmentDto Create(NegotiationAttachmentRequest request);
        IEnumerable<NegotiationAttachmentDto> Update(NegotiationAttachmentRequest request);
        string Delete(int id);
    }
}