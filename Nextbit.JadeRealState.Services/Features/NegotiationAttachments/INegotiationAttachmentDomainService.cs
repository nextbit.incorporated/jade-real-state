using System;

namespace Nextbit.JadeRealState.Services.Features.NegotiationAttachments
{
    public interface INegotiationAttachmentDomainService : IDisposable
    {
        NegotiationAttachment Create(NegotiationAttachmentRequest request);
        NegotiationAttachment Update(NegotiationAttachmentRequest request, NegotiationAttachment _oldRegister);
    }
}