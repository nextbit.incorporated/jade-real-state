using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.NegotiationPhases;

namespace Nextbit.JadeRealState.Services.Features.NegotiationAttachments
{
    public class NegotiationAttachmentDto : RequestBase
    {
        public int Id { get; set; }
        public int NegotiationPhaseId { get; set; }
        public DateTime Date { get; set; }
        public Byte[] Attachment { get; set; }
        public string ExtensionFile { get; set; }
        public string FileType { get; set; }
        public string Title { get; set; }

        public string ValidationErrorMessage { get; set; }

        public NegotiationPhaseDto NegotiationPhase { get; set; }

        internal static List<NegotiationAttachmentDto> From(
            IEnumerable<NegotiationAttachment> negotiationAttachments)
        {
            return (from qry in negotiationAttachments select From(qry)).ToList();
        }

        internal static NegotiationAttachmentDto From(NegotiationAttachment negotiationAttachment)
        {
            if (negotiationAttachment == null) return new NegotiationAttachmentDto();

            return new NegotiationAttachmentDto
            {
                Id = negotiationAttachment.Id,
                NegotiationPhaseId = negotiationAttachment.NegotiationPhaseId,
                Attachment = negotiationAttachment.Attachment,
                ExtensionFile = negotiationAttachment.ExtensionFile,
                Title = negotiationAttachment.Title,
                Date = negotiationAttachment.Date,
                FileType = negotiationAttachment.FileType,
                NegotiationPhase = new NegotiationPhaseDto()
            };
        }
    }
}