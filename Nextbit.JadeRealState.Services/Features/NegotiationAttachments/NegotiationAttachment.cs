using System;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.NegotiationPhases;

namespace Nextbit.JadeRealState.Services.Features.NegotiationAttachments
{
    [Table("NegotiationAttachments")]
    public class NegotiationAttachment : Entity
    {
        public int NegotiationPhaseId { get; private set; }
        public DateTime Date { get; private set; }
        public Byte[] Attachment { get; private set; }
        public string ExtensionFile { get; private set; }
        public string FileType { get; private set; }
        public string Title { get; private set; }
        public NegotiationPhase NegotiationPhase { get; set; }

        internal void Update(DateTime _newDate, string _newTitle, string _user)
        {
            Date = _newDate;
            Title = _newTitle;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = _user ?? "Service";
            TransactionUId = Guid.NewGuid();
        }

        public class Builder
        {
            private readonly NegotiationAttachment _negotiationAttachment = new NegotiationAttachment();

            public Builder WithNegotiationPhaseId(int negotiationPhaseId)
            {
                _negotiationAttachment.NegotiationPhaseId = negotiationPhaseId;
                return this;
            }

            public Builder WithDate(DateTime date)
            {
                _negotiationAttachment.Date = date;
                return this;
            }

            public Builder WithAttachment(string title, Byte[] file, string extension, string _fileType)
            {
                _negotiationAttachment.Title = title;
                _negotiationAttachment.Attachment = file;
                _negotiationAttachment.ExtensionFile = extension;
                _negotiationAttachment.FileType = _fileType;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _negotiationAttachment.CrudOperation = "Added";
                _negotiationAttachment.TransactionDate = DateTime.Now;
                _negotiationAttachment.TransactionType = "New";
                _negotiationAttachment.ModifiedBy = user ?? "Service";
                _negotiationAttachment.TransactionUId = Guid.NewGuid();

                return this;
            }

            public NegotiationAttachment Build()
            {
                return _negotiationAttachment;
            }
        }
    }
}