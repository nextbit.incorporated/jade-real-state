using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.NegotiationAttachments
{
    public class NegotiationAttachmentAppService : INegotiationAttachmentAppService
    {
        private RealStateContext _context;
        private readonly INegotiationAttachmentDomainService _negotiationAttachmentDomainService;
        public NegotiationAttachmentAppService(INegotiationAttachmentDomainService negotiationAttachmentDomainService, RealStateContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (negotiationAttachmentDomainService == null) throw new ArgumentException(nameof(negotiationAttachmentDomainService));

            _context = context;
            _negotiationAttachmentDomainService = negotiationAttachmentDomainService;

        }

        public NegotiationAttachmentDto Create(NegotiationAttachmentRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newRegister = _negotiationAttachmentDomainService.Create(request);

            _context.NegotiationAtachments.Add(newRegister);
            _context.SaveChanges();

            return new NegotiationAttachmentDto
            {
                Id = newRegister.Id,
                NegotiationPhaseId = newRegister.NegotiationPhaseId,
                Date = newRegister.Date,
                Attachment = newRegister.Attachment,
                Title = newRegister.Title,

            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var clienLevel = _context.NegotiationAtachments.FirstOrDefault(s => s.Id == id);
            if (clienLevel == null) throw new Exception("Nivel not exists");
            _context.NegotiationAtachments.Remove(clienLevel);
            _context.SaveChanges();

            return string.Empty;
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_negotiationAttachmentDomainService != null) _negotiationAttachmentDomainService.Dispose();
        }

        public IEnumerable<NegotiationAttachmentDto> GetFiles(NegotiationAttachmentRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.NegotiationPhaseId == 0) throw new ArgumentException(nameof(request.NegotiationPhaseId));
            var lista = _context.NegotiationAtachments.Where(s => s.NegotiationPhaseId == request.NegotiationPhaseId)
                .Include(c => c.NegotiationPhase);
            // .Include(c => c.ServiceProcess)
            // .OrderBy(s => s.Order);

            if (lista == null) return new List<NegotiationAttachmentDto>();
            if (!lista.Any()) return new List<NegotiationAttachmentDto>();
            var resultado = NegotiationAttachmentDto.From(lista);

            return resultado;
        }

        public IEnumerable<NegotiationAttachmentDto> Update(NegotiationAttachmentRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldClientLevel = _context.NegotiationAtachments.FirstOrDefault(s => s.Id == request.Id);
            if (oldClientLevel == null) return new List<NegotiationAttachmentDto> {
                new NegotiationAttachmentDto { ValidationErrorMessage = "Nivel de cliente inexistente" }
            };

            var newRegister = _negotiationAttachmentDomainService.Update(request, oldClientLevel);

            _context.NegotiationAtachments.Update(oldClientLevel);
            _context.SaveChanges();

            return null;
        }
    }
}