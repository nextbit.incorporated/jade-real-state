using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.NegotiationAttachments
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NegotiationAttachmentController : ControllerBase
    {
        private readonly INegotiationAttachmentAppService _negotiationAttachmentAppService;

        public NegotiationAttachmentController(INegotiationAttachmentAppService negotiationAttachmentAppService)
        {
            if (negotiationAttachmentAppService == null) throw new ArgumentException(nameof(negotiationAttachmentAppService));

            _negotiationAttachmentAppService = negotiationAttachmentAppService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<IEnumerable<NegotiationAttachmentDto>> Get([FromQuery]NegotiationAttachmentRequest request)
        {
            return Ok(_negotiationAttachmentAppService.GetFiles(request));
        }


        [HttpPost]
        [Authorize]
        public ActionResult<NegotiationAttachmentDto> Post([FromBody]NegotiationAttachmentRequest request)
        {
            return Ok(_negotiationAttachmentAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<NegotiationAttachmentDto> Put([FromBody]NegotiationAttachmentRequest request)
        {
            return Ok(_negotiationAttachmentAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_negotiationAttachmentAppService.Delete(Id));
        }
    }
}