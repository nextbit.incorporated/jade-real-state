using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.NegotiationAttachments
{
    public class NegotiationAttachmentRequest : RequestBase
    {
        public int Id { get; set; }
        public int NegotiationPhaseId { get;  set; }
        public DateTime Date { get;  set; }
        public string File { get; set; }
        public Byte[] Attachment { get;  set; }
        public string Title { get;  set; }
        public string ExtensionFile { get; set; }
        public string FileType { get; set; }
    }
}