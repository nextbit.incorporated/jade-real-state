using Nextbit.JadeRealState.Services.Features.FinancialApprovals;

namespace Nextbit.JadeRealState.Services.Features.FinancialPrequalifications
{
    public sealed class FinancialPrequalificationDto
    {
        public int Id { get; set; }
        public int NegotiationId { get; set; }
        public int? FundTypeId { get; set; }
        public int FinancialApprovalId { get; set; }
        public decimal InterestRate { get; set; }
        public decimal LoanAmount { get; set; }
        public int LoanTerm { get; set; }
        public decimal DownPaymentPercent { get; set; }
        public decimal DownPayment { get; set; }
        public decimal MonthlyPayment { get; set; }
        public string Comments { get; set; }
        public FinancialApprovalDto FinancialApproval { get; set; }

        internal static FinancialPrequalificationDto From(FinancialPrequalification financialPrequalification)
        {
            if (financialPrequalification == null)
            {
                return new FinancialPrequalificationDto();
            }

            return new FinancialPrequalificationDto
            {
                Id = financialPrequalification.Id,
                NegotiationId = financialPrequalification.NegotiationId,
                FundTypeId = financialPrequalification.FundTypeId,
                FinancialApprovalId = financialPrequalification.FinancialApprovalId,
                InterestRate = financialPrequalification.InterestRate,
                LoanAmount = financialPrequalification.LoanAmount,
                LoanTerm = financialPrequalification.LoanTerm,
                DownPaymentPercent = financialPrequalification.DownPaymentPercent,
                DownPayment = financialPrequalification.DownPayment,
                MonthlyPayment = financialPrequalification.MonthlyPayment,
                Comments = financialPrequalification.Comments,
                FinancialApproval = FinancialApprovalDto.From(financialPrequalification.FinancialApproval)
            };
        }
    }
}