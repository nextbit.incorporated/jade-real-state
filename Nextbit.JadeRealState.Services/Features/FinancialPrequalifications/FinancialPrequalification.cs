using System;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.FinancialApprovals;
using Nextbit.JadeRealState.Services.Features.FinancialInstitutions;
using Nextbit.JadeRealState.Services.Features.FundTypes;
using Nextbit.JadeRealState.Services.Features.Negotiations;

namespace Nextbit.JadeRealState.Services.Features.FinancialPrequalifications
{
    [Table("FinancialPrequalifications")]
    public sealed class FinancialPrequalification : Entity
    {
        private FinancialPrequalification()
        {
            SetAuditFields();
        }

        public int NegotiationId { get; private set; }
        public int? FundTypeId { get; private set; }
        public int FinancialApprovalId { get; private set; }
        public decimal InterestRate { get; private set; }
        public decimal LoanAmount { get; private set; }
        public int LoanTerm { get; private set; }
        public decimal DownPaymentPercent { get; private set; }
        public decimal DownPayment { get; private set; }
        public decimal MonthlyPayment { get; private set; }
        public string Comments { get; private set; }

        public Negotiation Negotiation { get; set; }
        public FundType FundType { get; set; }
        public FinancialApproval FinancialApproval { get; set; }

        internal void SetFundType(FundType fundType)
        {
            FundTypeId = fundType?.Id;
            FundType = fundType;
        }

        internal void SetFinancialApproval(FinancialApproval financialApproval)
        {
            FinancialApproval = financialApproval;

            if (financialApproval != null)
            {
                financialApproval.SetFinancialPrequalification(this);
            }
        }

        internal void SetComments(string comments)
        {
            Comments = comments;
        }

        internal void SetMonthlyPayment(decimal monthlyPayment)
        {
            MonthlyPayment = monthlyPayment;
        }

        internal void SetDownPayment(decimal downPayment)
        {
            DownPayment = downPayment;
        }

        internal void SetDownPaymentPercent(decimal downPaymentPercent)
        {
            DownPaymentPercent = downPaymentPercent;
        }

        internal void SetLoanTerm(int loanTerm)
        {
            LoanTerm = loanTerm;
        }

        internal void SetLoanAmount(decimal loanAmount)
        {
            LoanAmount = loanAmount;
        }

        internal void SetInterestRate(decimal interestRate)
        {
            InterestRate = interestRate;
        }

        public class Builder
        {
            private readonly FinancialPrequalification _financialPrequalification = new FinancialPrequalification();

            public Builder WithNegotiation(Negotiation negotiation)
            {
                _financialPrequalification.Negotiation = negotiation;

                return this;
            }

            public Builder WithFundType(FundType fundType)
            {
                _financialPrequalification.FundTypeId = fundType?.Id;
                _financialPrequalification.FundType = fundType;

                return this;
            }

            public Builder WithFinancialApproval(FinancialApproval financialApproval)
            {
                _financialPrequalification.FinancialApproval = financialApproval;

                return this;
            }

            public Builder WithInterestRate(decimal interestRate)
            {
                _financialPrequalification.InterestRate = interestRate;

                return this;
            }

            public Builder WithLoanAmount(decimal loanAmount)
            {
                _financialPrequalification.LoanAmount = loanAmount;

                return this;
            }

            public Builder WithLoanTerm(int loanTerm)
            {
                _financialPrequalification.LoanTerm = loanTerm;

                return this;
            }

            public Builder WithDownPaymentPercent(decimal downPaymentPercent)
            {
                _financialPrequalification.DownPaymentPercent = downPaymentPercent;

                return this;
            }

            public Builder WithDownPayment(decimal downPayment)
            {
                _financialPrequalification.DownPayment = downPayment;

                return this;
            }

            public Builder WithMonthlyPayment(decimal monthlyPayment)
            {
                _financialPrequalification.MonthlyPayment = monthlyPayment;

                return this;
            }

            public Builder WithComments(string comments)
            {
                _financialPrequalification.Comments = comments;

                return this;
            }

            public FinancialPrequalification Build()
            {
                return _financialPrequalification;
            }
        }


    }
}