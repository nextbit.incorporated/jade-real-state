using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Alarms
{
    public class AlarmRequest : RequestBase
    {
        public int Id { get; set; }
        public DateTime Date { get;  set; }
        public string Information { get;  set; }
        public int MonthsNumber { get;  set; }
        public int ClientId { get;  set; }
    }
}