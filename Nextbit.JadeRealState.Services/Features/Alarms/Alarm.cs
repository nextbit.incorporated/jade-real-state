using System;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.Alarms
{
    [Table("Alarms")]
    public class Alarm : Entity
    {
        public DateTime Date { get; private set; }
        public string Information { get; private set; }
        public int MonthsNumber { get; private set; }
        public int ClientId { get; private set; }

        public Client Client { get; set; }

        public void Update(DateTime _date, string _information, int _monthsNumber, string _user)
        {
            Date = _date;
            Information = _information;
            MonthsNumber = _monthsNumber;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = string.IsNullOrEmpty(_user) ?  "ModifiedProject" : _user;
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
        }


        public class Builder
        {
            private readonly Alarm _alarm = new Alarm();

            public Builder WithClientId(int clientId)
            {
                _alarm.ClientId = clientId;
                return this;

            }

            public Builder WithAlarmInfo(DateTime date, string info, int monthsNumber)
            {
                _alarm.Date = date;
                _alarm.Information = info;
                _alarm.MonthsNumber = monthsNumber;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _alarm.CrudOperation = "Added";
                _alarm.TransactionDate = DateTime.Now;
                _alarm.TransactionType = "NewProject";
                _alarm.ModifiedBy = string.IsNullOrEmpty(user) ?  "Service" : user;
                _alarm.TransactionUId = Guid.NewGuid();

                return this;
            }

            public Alarm Build()
            {
                return _alarm;
            }
        }
    }
}