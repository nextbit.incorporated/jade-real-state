using System;

namespace Nextbit.JadeRealState.Services.Features.Alarms
{
    public interface IAlarmDomainService : IDisposable
    {
        Alarm CreateAlarm(AlarmRequest request);
        Alarm UpdateAlarm(AlarmRequest request, Alarm AlarmOldInfo);
    }
}