using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.Alarms {
    public class AlarmDto : ResponseBase {
        public DateTime Date { get; set; }
        public string Information { get; set; }
        public int MonthsNumber { get; set; }
        public int ClientId { get; set; }

        public string ClientName { get; set; }
        public string SalesAdvisor { get; set; }
        public string SalesAdvisorUserCode { get; set; }

        public ClientDto Client { get; set; }

        internal static List<AlarmDto> FromAlarms (IEnumerable<Alarm> detail) {
            return (from qry in detail select From (qry)).ToList ();
        }

        internal static AlarmDto From (Alarm s) {
            if (s == null) return new AlarmDto ();
            return new AlarmDto {
                Id = s.Id,
                    ClientId = s.ClientId,
                    Date = s.Date,
                    Information = s.Information,
                    MonthsNumber = s.MonthsNumber,
                    // Negotiation = s.Negotiation == null ? new NegotiationDto() : NegotiationDto.From(s.Negotiation),  
                    // Client = s.Client == null ? new ClientDto() : ClientDto.From(s.Client)
            };
        }

        internal static List<AlarmDto> FromAlarmNotification (IEnumerable<Alarm> detail) {
            return (from qry in detail select FromNotification (qry)).ToList ();
        }

        internal static AlarmDto FromNotification (Alarm s) {
            if (s == null) return new AlarmDto ();
            return new AlarmDto {
                Id = s.Id,
                    ClientId = s.ClientId,
                    Date = s.Date,
                    Information = s.Information,
                    MonthsNumber = s.MonthsNumber,
                    ClientName = s.Client.GetCompleteName(),
                    SalesAdvisor = s.Client?.SalesAdvisor?.GetName(),
                    SalesAdvisorUserCode = s.Client?.SalesAdvisor?.UserId
                // Negotiation = s.Negotiation == null ? new NegotiationDto() : NegotiationDto.From(s.Negotiation),  
                // Client = s.Client == null ? new ClientDto() : ClientDto.From(s.Client)
            };
        }

    }
}