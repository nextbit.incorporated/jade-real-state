using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Alarms {
    public interface IAlarmAppService : IDisposable {
        IEnumerable<AlarmDto> GetByClientId (AlarmRequest request);
        // ProjectPagedDTO GetPagedProject(ProjectPagedRequest request);
        IEnumerable<AlarmDto> CreateAlarmDto (AlarmRequest request);
        IEnumerable<AlarmDto> UpdateAlarmDto (AlarmRequest request);
        IEnumerable<AlarmDto> DeleteAlarm (int id);

        IEnumerable<AlarmDto> GetAlarmsNotification (AlarmRequest request);
    }
}