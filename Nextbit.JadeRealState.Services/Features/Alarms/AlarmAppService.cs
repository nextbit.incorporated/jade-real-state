using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Alarms {
    public class AlarmAppService : IAlarmAppService {
        private readonly RealStateContext _context;
        private readonly IAlarmDomainService _alarmDomainService;
        public AlarmAppService (RealStateContext context, IAlarmDomainService AlarmDtoDomainService) {
            _context = context ??
                throw new ArgumentNullException (nameof (context));
            _alarmDomainService = AlarmDtoDomainService ??
                throw new ArgumentException (nameof (AlarmDtoDomainService));
        }

        public IEnumerable<AlarmDto> GetByClientId (AlarmRequest request) {
            IEnumerable<Alarm> Alarms = _context.Alarms
                .Where (s => s.ClientId == request.ClientId)
                .Include (s => s.Client);

            return AlarmDto.FromAlarms (Alarms);
        }

        public IEnumerable<AlarmDto> CreateAlarmDto (AlarmRequest request) {
            if (request == null) throw new ArgumentException (nameof (request));
            var newAlarmDto = _alarmDomainService.CreateAlarm (request);

            _context.Alarms.Add (newAlarmDto);
            _context.SaveChanges ();

            var alarms = _context.Alarms
                .Where (s => s.ClientId == request.ClientId).Include (s => s.Client);

            return AlarmDto.FromAlarms (alarms);
        }

        public IEnumerable<AlarmDto> UpdateAlarmDto (AlarmRequest request) {
            if (request == null) throw new ArgumentException (nameof (request));
            var oldAlarmDto = _context.Alarms.FirstOrDefault (s => s.Id == request.Id);
            if (oldAlarmDto == null) return new List<AlarmDto> { new AlarmDto { ValidationErrorMessage = "Proyecto inexistente" } };

            var rolUpdate = _alarmDomainService.UpdateAlarm (request, oldAlarmDto);

            _context.Alarms.Update (oldAlarmDto);
            _context.SaveChanges ();

            var alarms = _context.Alarms
                .Where (s => s.ClientId == request.ClientId).Include (s => s.Client);

            return AlarmDto.FromAlarms (alarms);
        }

        public IEnumerable<AlarmDto> DeleteAlarm (int id) {
            if (id <= 0) throw new ArgumentException (nameof (id));

            var alarm = _context.Alarms.FirstOrDefault (s => s.Id == id);
            if (alarm == null) throw new Exception ("AlarmDto not exists");
            _context.Alarms.Remove (alarm);
            var clientId = alarm.ClientId;
            _context.SaveChanges ();

            var alarms = _context.Alarms
                .Where (s => s.ClientId == clientId).Include (s => s.Client);

            return AlarmDto.FromAlarms (alarms);
        }

        public void Dispose () {
            if (_context != null) _context.Dispose ();
            if (_alarmDomainService != null) _alarmDomainService.Dispose ();
        }

        public IEnumerable<AlarmDto> GetAlarmsNotification (AlarmRequest request) 
        {
            if (request == null) return new List<AlarmDto> ();
            if (string.IsNullOrEmpty (request.User)) return new List<AlarmDto> ();
            var usuario = _context.Users.FirstOrDefault (s => s.UserId == request.User);
            if (usuario == null) return new List<AlarmDto> ();
            var usuarioRoles = _context.UserRol.FirstOrDefault (s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<AlarmDto> ();
            DateTime currentDate = Convert.ToDateTime (DateTime.Now.AddMonths (-1));
            var newDate = Convert.ToDateTime (currentDate.AddMonths (1));

            if (usuarioRoles.RolId != 3) 
            {
                var notification = _context.Alarms
                    .Include(x => x.Client).ThenInclude(t => t.SalesAdvisor)
                    .AsEnumerable()
                    .Where(s => Convert.ToDateTime(s.Date.AddMonths (s.MonthsNumber)) >= currentDate &&
                        Convert.ToDateTime(s.Date.AddMonths (s.MonthsNumber)) < newDate);

                var notifications = AlarmDto.FromAlarmNotification (notification);
                return notifications;
            } 
            else 
            {
                var notification = _context.Alarms
                    .Include(x => x.Client).ThenInclude(t => t.SalesAdvisor)
                    .AsEnumerable()
                    .Where(s => Convert.ToDateTime(s.Date.AddMonths(s.MonthsNumber)) >= currentDate &&
                        Convert.ToDateTime(s.Date.AddMonths(s.MonthsNumber)) < newDate);

                var notifications = AlarmDto.FromAlarmNotification(notification);
                var filterNotifications = notifications.Where(s => s.SalesAdvisorUserCode == usuario.UserId).ToList();
                return filterNotifications;
            }

        }
    }
}