using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Alarms {
    [Route ("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AlarmController : ControllerBase {
        private readonly IAlarmAppService _alarmAppService;

        public AlarmController (IAlarmAppService alarmAppService) {
            if (alarmAppService == null) throw new ArgumentException (nameof (alarmAppService));

            _alarmAppService = alarmAppService;
        }

        [HttpGet]
        // [Route("")]
        [Authorize]
        public ActionResult<IEnumerable<AlarmDto>> GetProjects ([FromQuery] AlarmRequest request) {
            return Ok (_alarmAppService.GetByClientId (request));
        }

        [HttpGet]
        [Route ("notifications")]
        [Authorize]
        public ActionResult<IEnumerable<AlarmDto>> GetNotifications([FromQuery] AlarmRequest request) 
        {
            return Ok(_alarmAppService.GetAlarmsNotification(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<AlarmDto> Post ([FromBody] AlarmRequest request) {
            return Ok (_alarmAppService.CreateAlarmDto (request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<IEnumerable<AlarmDto>> Put ([FromBody] AlarmRequest request) {
            return Ok (_alarmAppService.UpdateAlarmDto (request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<IEnumerable<AlarmDto>> Delete (int Id) {
            return Ok (_alarmAppService.DeleteAlarm (Id));
        }
    }
}