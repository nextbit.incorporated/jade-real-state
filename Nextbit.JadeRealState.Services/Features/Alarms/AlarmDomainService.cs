using System;

namespace Nextbit.JadeRealState.Services.Features.Alarms
{
    public class AlarmDomainService : IAlarmDomainService
    {
        public Alarm CreateAlarm(AlarmRequest request)
        {
            if (request.ClientId == 0) throw new ArgumentNullException(nameof(request.ClientId));
 
            Alarm Alarm = new Alarm.Builder()
            .WithClientId(request.ClientId)
            .WithAlarmInfo(request.Date, request.Information, request.MonthsNumber)
            .WithAuditFields(request.User)
            .Build();

            return Alarm;
        }

        public Alarm UpdateAlarm(AlarmRequest request, Alarm AlarmOldInfo)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (AlarmOldInfo == null) throw new ArgumentException(nameof(AlarmOldInfo));

            AlarmOldInfo.Update(request.Date, request.Information, request.MonthsNumber, request.User);
            return AlarmOldInfo;
        }

        public void Dispose()
        {
        }
    }
}