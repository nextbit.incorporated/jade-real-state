using System;

namespace Nextbit.JadeRealState.Services.Features.FinancialApprovals
{
    public sealed class FinancialApprovalDto
    {
        public int Id { get; set; }
        public int FinancialPrequalificationId { get; set; }
        public DateTime CreationDate { get; set; }
        public bool Jointloan { get; set; }
        public string ApprovalState { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? RejectionDate { get; set; }
        public string RejectionReason { get; set; }
        public string CreditOfficerComments { get; set; }
        public string SalesAdvisorComments { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DueDate { get; set; }

        public string FollowUp { get; set; }

        internal static FinancialApprovalDto From(FinancialApproval financialApproval)
        {
            if (financialApproval == null)
            {
                return new FinancialApprovalDto();
            }

            return new FinancialApprovalDto
            {
                Id = financialApproval.Id,
                FinancialPrequalificationId = financialApproval.FinancialPrequalificationId,
                CreationDate = financialApproval.CreationDate,
                Jointloan = financialApproval.Jointloan,
                ApprovalState = financialApproval.ApprovalState,
                ApprovalDate = financialApproval.ApprovalDate,
                RejectionDate = financialApproval.RejectionDate,
                RejectionReason = financialApproval.RejectionReason,
                CreditOfficerComments = financialApproval.CreditOfficerComments,
                SalesAdvisorComments = financialApproval.SalesAdvisorComments,
                StartDate = financialApproval.StartDate,
                DueDate = financialApproval.DueDate,
                FollowUp = financialApproval.FollowUp
            };
        }
    }
}