using System;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.FinancialPrequalifications;

namespace Nextbit.JadeRealState.Services.Features.FinancialApprovals
{
    [Table("FinancialApprovals")]
    public sealed class FinancialApproval : Entity
    {
        public FinancialApproval()
        {
            SetAuditFields();
        }
        public int FinancialPrequalificationId { get; private set; }
        public bool Jointloan { get; private set; }
        public string ApprovalState { get; private set; }
        public DateTime? ApprovalDate { get; private set; }
        public DateTime? RejectionDate { get; private set; }
        public string RejectionReason { get; private set; }
        public string CreditOfficerComments { get; private set; }
        public string SalesAdvisorComments { get; private set; }

        public DateTime? StartDate { get; private set; }
        public DateTime? DueDate { get; private set; }

        public string FollowUp { get; private set; }

        public FinancialPrequalification FinancialPrequalification { get; set; }

        public class Builder
        {
            private readonly FinancialApproval _financialApproval = new FinancialApproval();

            public Builder WithFinancialPrequalification(FinancialPrequalification financialPrequalification)
            {
                _financialApproval.FinancialPrequalification = financialPrequalification;

                return this;
            }

            public Builder WithJointloan(bool jointloan)
            {
                _financialApproval.Jointloan = jointloan;

                return this;
            }

            public Builder WithApprovalState(string approvalState)
            {
                _financialApproval.ApprovalState = approvalState;

                return this;
            }

            public Builder WithApprovalDate(DateTime? approvalDate)
            {
                _financialApproval.ApprovalDate = approvalDate;

                return this;
            }

            public Builder WithRejectionDate(DateTime? rejectionDate)
            {
                _financialApproval.RejectionDate = rejectionDate;

                return this;
            }

            public Builder WithRejectionReason(string rejectionReason)
            {
                _financialApproval.RejectionReason = rejectionReason;

                return this;
            }

            public Builder WithCreditOfficerComments(string creditOfficerComments)
            {
                _financialApproval.CreditOfficerComments = creditOfficerComments;

                return this;
            }

            public Builder WithSalesAdvisorComments(string salesAdvisorComments)
            {
                _financialApproval.SalesAdvisorComments = salesAdvisorComments;

                return this;
            }

            public Builder WithStartDate(DateTime? startDate)
            {
                _financialApproval.StartDate = startDate;

                return this;
            }

            public Builder WithDueDate(DateTime? dueDate)
            {
                _financialApproval.DueDate = dueDate;

                return this;
            }

            public Builder WithFollowUp(string followUp)
            {
                _financialApproval.FollowUp = followUp;

                return this;
            }

            public FinancialApproval Build()
            {
                return _financialApproval;
            }
        }

        internal void SetFinancialPrequalification(FinancialPrequalification financialPrequalification)
        {
            FinancialPrequalification = financialPrequalification;
        }

        internal void SetJointloan(bool jointloan)
        {
            Jointloan = jointloan;
        }

        internal void SetApprovalState(string approvalState)
        {
            ApprovalState = approvalState;
        }

        internal void SetApprovalDate(DateTime? approvalDate)
        {
            ApprovalDate = approvalDate;
        }

        internal void SetRejectionDate(DateTime? rejectionDate)
        {
            RejectionDate = rejectionDate;
        }

        internal void SetRejectionReason(string rejectionReason)
        {
            RejectionReason = rejectionReason;
        }

        internal void SetCreditOfficerComments(string creditOfficerComments)
        {
            CreditOfficerComments = creditOfficerComments;
        }

        internal void SetSalesAdvisorComments(string salesAdvisorComments)
        {
            SalesAdvisorComments = salesAdvisorComments;
        }

        internal void SetStartDate(DateTime? startDate)
        {
            StartDate = startDate;
        }

        internal void SetDueDate(DateTime? dueDate)
        {
            DueDate = dueDate;
        }

        internal void SetFollowUp(string followUp)
        {
            FollowUp = followUp;
        }

        internal void SetCreationDate(DateTime creationDate)
        {
            CreationDate = creationDate;
        }
    }
}