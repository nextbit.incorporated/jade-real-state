using System;

namespace Nextbit.JadeRealState.Services.Features.FinancialInstitutions
{
    public class FinancialInstitutionDomainService : IFinancialInstitutionDomainService
    {
        public FinancialInstitution Create(FinancialInstitutionRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));

            FinancialInstitution financialInstitution = new FinancialInstitution.Builder()
            .WithName(request.Name)
            .WithDescription(request.Description)
            .WithBusinessName(request.BusinessName)
            .WithContactNumber(request.ContactNumber)
            .WithEmail(request.Email)
            .WithElectronicAddresses(request.ElectronicAddresses)
            .WithAddress(request.Address)
            .WithRTN(request.RTN)
            .WithAuditFields()
            .Build();

            return financialInstitution;
        }
        public FinancialInstitution Update(FinancialInstitutionRequest request, FinancialInstitution financialInstitutionOldInfo)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (financialInstitutionOldInfo == null) throw new ArgumentException(nameof(financialInstitutionOldInfo));

            financialInstitutionOldInfo.Update(
                request.Name, request.Description, request.BusinessName,request.RTN, request.ContactNumber,
                request.ElectronicAddresses, request.Email, request.Address);
            return financialInstitutionOldInfo;
        }

        public void Dispose()
        {
        }


    }
}