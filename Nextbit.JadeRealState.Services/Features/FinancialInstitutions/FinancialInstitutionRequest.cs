using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.FinancialInstitutions
{
    public class FinancialInstitutionRequest : RequestBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string BusinessName { get; set; }
        public string RTN { get; set; }
        public string ContactNumber { get; set; }
        public string ElectronicAddresses { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }

    public class FinancialInstitutionPagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
    }
}