using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.FinancialInstitutions
{
    public class FinancialInstitutionAppService : IFinancialInstitutionAppService
    {
        private RealStateContext _context;
        private readonly IFinancialInstitutionDomainService _financialInstitutionDomainService;

        public FinancialInstitutionAppService(RealStateContext context, IFinancialInstitutionDomainService financialInstitutionDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (financialInstitutionDomainService == null) throw new ArgumentException(nameof(financialInstitutionDomainService));

            _context = context;
            _financialInstitutionDomainService = financialInstitutionDomainService;
        }

        public async Task<List<FinancialInstitutionDto>> GetFinancialInstitutionsAsync()
        {
            IEnumerable<FinancialInstitution> institutions = await _context.FinancialInstitutions.ToListAsync();

            if (institutions == null) return new List<FinancialInstitutionDto>();
            return institutions.Select(s => new FinancialInstitutionDto
            {
                Id = s.Id,
                Name = s.Name,
                Description = s.Description,
                BusinessName = s.BusinessName,
                ContactNumber = s.ContactNumber,
                ElectronicAddresses = s.ElectronicAddresses,
                Email = s.Email,
                Address = s.Address,
                RTN = s.RTN,
                Status = s.Status
            }).ToList();
        }

        public FinancialInstitutionDto Create(FinancialInstitutionRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newFinancialInstitution = _financialInstitutionDomainService.Create(request);

            _context.FinancialInstitutions.Add(newFinancialInstitution);
            _context.SaveChanges();

            return new FinancialInstitutionDto
            {
                Name = newFinancialInstitution.Name,
                Description = newFinancialInstitution.Description,
                BusinessName = newFinancialInstitution.BusinessName,
                ContactNumber = newFinancialInstitution.ContactNumber,
                ElectronicAddresses = newFinancialInstitution.ElectronicAddresses,
                Email = newFinancialInstitution.Email,
                Address = newFinancialInstitution.Address,
                RTN = newFinancialInstitution.RTN,
                Status = newFinancialInstitution.Status
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var financialInstitutions = _context.FinancialInstitutions.FirstOrDefault(s => s.Id == id);
            if (financialInstitutions == null) throw new Exception("Constructora not exists");
            _context.FinancialInstitutions.Remove(financialInstitutions);
            _context.SaveChanges();

            return string.Empty;
        }

        public FinancialInstitutionPagedDto GetPaged(FinancialInstitutionPagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            var count = Convert.ToDecimal(_context.FinancialInstitutions.Count());
            var totalPage = Math.Ceiling(count / request.PageSize);
            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;
            if (string.IsNullOrEmpty(request.Value))
            {
                var lista = _context.FinancialInstitutions
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new FinancialInstitutionPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    FinancialInstitutions = lista.Select(s => new FinancialInstitutionDto
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description,
                        BusinessName = s.BusinessName,
                        ContactNumber = s.ContactNumber,
                        ElectronicAddresses = s.ElectronicAddresses,
                        Email = s.Email,
                        Address = s.Address,
                        RTN = s.RTN,
                        Status = s.Status
                    }).ToList()
                };
            }
            else
            {
                var lista = _context.FinancialInstitutions.Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new FinancialInstitutionPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    FinancialInstitutions = lista.Select(s => new FinancialInstitutionDto
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description,
                        BusinessName = s.BusinessName,
                        ContactNumber = s.ContactNumber,
                        ElectronicAddresses = s.ElectronicAddresses,
                        Email = s.Email,
                        Address = s.Address,
                        RTN = s.RTN,
                        Status = s.Status
                    }).ToList()
                };
            }
        }

        public FinancialInstitutionDto Update(FinancialInstitutionRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldFinancialInstitution = _context.FinancialInstitutions.FirstOrDefault(s => s.Id == request.Id);
            if (oldFinancialInstitution == null) return new FinancialInstitutionDto { ValidationErrorMessage = "Institucion inexistente" };

            var financialInstitutionUpdate = _financialInstitutionDomainService.Update(request, oldFinancialInstitution);

            _context.FinancialInstitutions.Update(oldFinancialInstitution);
            _context.SaveChanges();

            return new FinancialInstitutionDto
            {
                Id = oldFinancialInstitution.Id,
                Name = oldFinancialInstitution.Name,
                Description = oldFinancialInstitution.Description,
                BusinessName = oldFinancialInstitution.BusinessName,
                ContactNumber = oldFinancialInstitution.ContactNumber,
                ElectronicAddresses = oldFinancialInstitution.ElectronicAddresses,
                Email = oldFinancialInstitution.Email,
                Address = oldFinancialInstitution.Address,
                RTN = oldFinancialInstitution.RTN,
                Status = oldFinancialInstitution.Status
            };
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_financialInstitutionDomainService != null) _financialInstitutionDomainService.Dispose();
        }
    }
}