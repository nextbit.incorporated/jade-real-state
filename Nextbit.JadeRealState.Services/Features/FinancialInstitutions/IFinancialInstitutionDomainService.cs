using System;

namespace Nextbit.JadeRealState.Services.Features.FinancialInstitutions
{
    public interface IFinancialInstitutionDomainService : IDisposable
    {
        FinancialInstitution Create(FinancialInstitutionRequest request);
        FinancialInstitution Update(FinancialInstitutionRequest request, FinancialInstitution financialInstitutionOldInfo);
    }
}