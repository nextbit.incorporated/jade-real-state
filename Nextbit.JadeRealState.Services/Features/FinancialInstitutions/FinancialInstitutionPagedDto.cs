using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.FinancialInstitutions
{
    public class FinancialInstitutionPagedDto
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<FinancialInstitutionDto> FinancialInstitutions { get; set; }
    }
}