using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Features.FinancialInstitutions
{
    public interface IFinancialInstitutionAppService : IDisposable
    {
        Task<List<FinancialInstitutionDto>> GetFinancialInstitutionsAsync();
        FinancialInstitutionPagedDto GetPaged(FinancialInstitutionPagedRequest request);
        FinancialInstitutionDto Create(FinancialInstitutionRequest request);
        FinancialInstitutionDto Update(FinancialInstitutionRequest request);
        string Delete(int id);
    }
}