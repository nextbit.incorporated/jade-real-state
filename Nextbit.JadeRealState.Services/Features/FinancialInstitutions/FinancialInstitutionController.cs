using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.FinancialInstitutions
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FinancialInstitutionController : ControllerBase
    {
        private readonly IFinancialInstitutionAppService _financialInstitutionAppService;
        public FinancialInstitutionController(IFinancialInstitutionAppService financialInstitutionAppService)
        {
            if (financialInstitutionAppService == null) throw new ArgumentException(nameof(financialInstitutionAppService));

            _financialInstitutionAppService = financialInstitutionAppService;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetFinancialInstitutionsAsync()
        {
            return Ok(await _financialInstitutionAppService.GetFinancialInstitutionsAsync());
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<FinancialInstitutionPagedDto> GetPaged([FromQuery] FinancialInstitutionPagedRequest request)
        {
            return Ok(_financialInstitutionAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<FinancialInstitutionDto> Post([FromBody] FinancialInstitutionRequest request)
        {
            return Ok(_financialInstitutionAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<FinancialInstitutionDto> Put([FromBody] FinancialInstitutionRequest request)
        {
            return Ok(_financialInstitutionAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_financialInstitutionAppService.Delete(Id));
        }
    }
}