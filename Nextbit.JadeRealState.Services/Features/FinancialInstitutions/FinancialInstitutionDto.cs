using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.FinancialInstitutions
{
    public class FinancialInstitutionDto : ResponseBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string BusinessName { get; set; }
        public string RTN { get; set; }
        public string ContactNumber { get; set; }
        public string ElectronicAddresses { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }

        internal static FinancialInstitutionDto From(FinancialInstitution financialInstitution)
        {
            if (financialInstitution == null)
            {
                return null;
            }

            return new FinancialInstitutionDto
            {
                Id = financialInstitution.Id,
                CreationDate = financialInstitution.CreationDate,
                Name = financialInstitution.Name,
                Description = financialInstitution.Description,
                BusinessName = financialInstitution.BusinessName,
                RTN = financialInstitution.RTN,
                ContactNumber = financialInstitution.ContactNumber,
                ElectronicAddresses = financialInstitution.ElectronicAddresses,
                Email = financialInstitution.Email,
                Address = financialInstitution.Address
            };
        }
    }
}