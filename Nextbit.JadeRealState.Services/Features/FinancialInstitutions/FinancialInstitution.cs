using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.Negotiations;

namespace Nextbit.JadeRealState.Services.Features.FinancialInstitutions
{
    [Table("FinancialInstitutions")]
    public class FinancialInstitution : Entity
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public string BusinessName { get; private set; }
        public string RTN { get; private set; }
        public string ContactNumber { get; private set; }
        public string ElectronicAddresses { get; private set; }
        public string Email { get; private set; }
        public string Address { get; private set; }
        public string Status { get; private set; }

        public ICollection<Client> Clients { get; set; }
        public ICollection<Negotiation> Negotiations { get; set; }

        public void Update(
          string _name, string _description, string _businessName, string _rtn,
          string _contactNumber, string _electronicAddresses, string _email, string _address)
        {
            Name = _name;
            Description = _description;
            BusinessName = _businessName;
            RTN = _rtn;
            ContactNumber = _contactNumber;
            ElectronicAddresses = _electronicAddresses;
            Email = _email;
            Address = _address;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedProject";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
            Status = "ACTIVO";
        }

        public class Builder
        {
            private readonly FinancialInstitution _financialInstitution = new FinancialInstitution();

            public Builder WithName(string name)
            {
                _financialInstitution.Name = name;
                return this;
            }

            public Builder WithDescription(string description)
            {
                _financialInstitution.Description = description;
                return this;
            }

            public Builder WithBusinessName(string businessName)
            {
                _financialInstitution.BusinessName = businessName;
                return this;
            }

            public Builder WithContactNumber(string contactNumber)
            {
                _financialInstitution.ContactNumber = contactNumber;
                return this;
            }

            public Builder WithElectronicAddresses(string electronicAddresses)
            {
                _financialInstitution.ElectronicAddresses = electronicAddresses;
                return this;
            }

            public Builder WithEmail(string email)
            {
                _financialInstitution.Email = email;
                return this;
            }
            public Builder WithAddress(string address)
            {
                _financialInstitution.Address = address;
                return this;
            }

            public Builder WithRTN(string rtn)
            {
                _financialInstitution.RTN = rtn;
                return this;
            }

            public Builder WithAuditFields()
            {
                _financialInstitution.CrudOperation = "Added";
                _financialInstitution.TransactionDate = DateTime.Now;
                _financialInstitution.TransactionType = "NewProject";
                _financialInstitution.ModifiedBy = "Service";
                _financialInstitution.TransactionUId = Guid.NewGuid();
                _financialInstitution.Status = "ACTIVO";

                return this;
            }

            public FinancialInstitution Build()
            {
                return _financialInstitution;
            }
        }
    }
}