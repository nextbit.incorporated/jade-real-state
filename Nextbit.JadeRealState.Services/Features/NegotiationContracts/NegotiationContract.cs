using System;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Builders;
using Nextbit.JadeRealState.Services.Features.Negotiations;

namespace Nextbit.JadeRealState.Services.Features.NegotiationContracts
{
    [Table("NegotiationContracts")]
    public sealed class NegotiationContract : Entity
    {
        private NegotiationContract()
        {
            SetAuditFields();
        }

        public int NegotiationId { get; set; }
        public int? BuilderCompanyId { get; private set; }
        public DateTime? ContractSigningDate { get; private set; }
        public DateTime? EffectiveDate { get; private set; }
        public string BlockNumber { get; private set; }
        public string LotNumber { get; private set; }
        public decimal ConstructionMeters { get; private set; }
        public decimal LandSize { get; private set; }
        public string Registration { get; private set; }
        public decimal ReservationValue { get; private set; }
        public decimal DownPaymentValue { get; private set; }
        public string DownPaymentMethod { get; private set; }
        public decimal LandValue { get; private set; }
        public decimal ConstructionValue { get; private set; }
        public decimal ImprovementsValue { get; private set; }
        public decimal HouseTotalValue { get; private set; }


        public Negotiation Negotiation { get; set; }
        public BuilderCompany BuilderCompany { get; set; }

        internal void SetBuilderCompany(BuilderCompany builderCompany)
        {
            BuilderCompanyId = builderCompany?.Id;
            BuilderCompany = builderCompany;
        }

        internal void SetContractSigningDate(DateTime? contractSigningDate)
        {
            ContractSigningDate = contractSigningDate;
        }

        internal void SetEffectiveDate(DateTime? effectiveDate)
        {
            EffectiveDate = effectiveDate;
        }

        internal void SetBlockNumber(string blockNumber)
        {
            BlockNumber = blockNumber;
        }

        internal void SetLotNumber(string lotNumber)
        {
            LotNumber = lotNumber;
        }

        internal void SetConstructionMeters(decimal constructionMeters)
        {
            ConstructionMeters = constructionMeters;
        }

        internal void SetLandSize(decimal landSize)
        {
            LandSize = landSize;
        }

        internal void SetRegistration(string registration)
        {
            Registration = registration;
        }

        internal void SetReservationValue(decimal reservationValue)
        {
            ReservationValue = reservationValue;
        }

        internal void SetDownPaymentValue(decimal downPaymentValue)
        {
            DownPaymentValue = downPaymentValue;
        }

        internal void SetDownPaymentMethod(string downPaymentMethod)
        {
            DownPaymentMethod = downPaymentMethod;
        }

        internal void SetLandValue(decimal landValue)
        {
            LandValue = landValue;
        }

        internal void SetConstructionValue(decimal constructionValue)
        {
            ConstructionValue = constructionValue;
        }

        internal void SetImprovementsValue(decimal improvementsValue)
        {
            ImprovementsValue = improvementsValue;
        }

        internal void SetHouseTotalValue(decimal houseTotalValue)
        {
            HouseTotalValue = houseTotalValue;
        }

        public class Builder
        {
            private readonly NegotiationContract _negotiationContract = new NegotiationContract();

            public Builder WithNegotiation(Negotiation negotiation)
            {
                _negotiationContract.NegotiationId = negotiation.Id;
                _negotiationContract.Negotiation = negotiation;

                return this;
            }

            public Builder WithBuilderCompany(BuilderCompany builderCompany)
            {
                _negotiationContract.BuilderCompanyId = builderCompany?.Id;
                _negotiationContract.BuilderCompany = builderCompany;

                return this;
            }

            public Builder WithContractSigningDate(DateTime? contractSigningDate)
            {
                _negotiationContract.ContractSigningDate = contractSigningDate;

                return this;
            }

            public Builder WithEffectiveDate(DateTime? effectiveDate)
            {
                _negotiationContract.EffectiveDate = effectiveDate;

                return this;
            }

            public Builder WithBlockNumber(string blockNumber)
            {
                _negotiationContract.BlockNumber = blockNumber;

                return this;
            }

            public Builder WithLotNumber(string lotNumber)
            {
                _negotiationContract.LotNumber = lotNumber;

                return this;
            }

            public Builder WithConstructionMeters(decimal lotNumber)
            {
                _negotiationContract.ConstructionMeters = lotNumber;

                return this;
            }

            public Builder WithLandSize(decimal landSize)
            {
                _negotiationContract.LandSize = landSize;

                return this;
            }

            public Builder WithRegistration(string registration)
            {
                _negotiationContract.Registration = registration;

                return this;
            }

            public Builder WithReservationValue(decimal reservationValue)
            {
                _negotiationContract.ReservationValue = reservationValue;

                return this;
            }

            public Builder WithDownPaymentValue(decimal downPaymentValue)
            {
                _negotiationContract.DownPaymentValue = downPaymentValue;

                return this;
            }

            public Builder WithDownPaymentMethod(string downPaymentMethod)
            {
                _negotiationContract.DownPaymentMethod = downPaymentMethod;

                return this;
            }

            public Builder WithLandValue(decimal landValue)
            {
                _negotiationContract.LandValue = landValue;

                return this;
            }

            public Builder WithConstructionValue(decimal constructionValue)
            {
                _negotiationContract.ConstructionValue = constructionValue;

                return this;
            }

            public Builder WithImprovementsValue(decimal improvementsValue)
            {
                _negotiationContract.ImprovementsValue = improvementsValue;

                return this;
            }

            public Builder WithHouseTotalValue(decimal houseTotalValue)
            {
                _negotiationContract.HouseTotalValue = houseTotalValue;

                return this;
            }

            public NegotiationContract Build()
            {
                return _negotiationContract;
            }
        }


    }
}