using System;
using Nextbit.JadeRealState.Services.Features.Builders;

namespace Nextbit.JadeRealState.Services.Features.NegotiationContracts
{
    public class NegotiationContractDto
    {
        public int Id { get; set; }
        public int NegotiationId { get; set; }
        public int? BuilderCompanyId { get; set; }
        public DateTime? ContractSigningDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string BlockNumber { get; set; }
        public string LotNumber { get; set; }
        public decimal ConstructionMeters { get; set; }
        public decimal LandSize { get; set; }
        public string Registration { get; set; }
        public decimal ReservationValue { get; set; }
        public decimal DownPaymentValue { get; set; }
        public string DownPaymentMethod { get; set; }
        public decimal LandValue { get; set; }
        public decimal ConstructionValue { get; set; }
        public decimal ImprovementsValue { get; set; }
        public decimal HouseTotalValue { get; set; }

        public BuilderCompanyDTO BuilderCompany { get; set; }

        internal static NegotiationContractDto From(NegotiationContract negotiationContract)
        {
            if (negotiationContract == null)
            {
                return new NegotiationContractDto();
            }

            return new NegotiationContractDto
            {
                Id = negotiationContract.Id,
                NegotiationId = negotiationContract.NegotiationId,
                BuilderCompanyId = negotiationContract.BuilderCompanyId,
                ContractSigningDate = negotiationContract.ContractSigningDate,
                EffectiveDate = negotiationContract.EffectiveDate,
                BlockNumber = negotiationContract.BlockNumber,
                LotNumber = negotiationContract.LotNumber,
                ConstructionMeters = negotiationContract.ConstructionMeters,
                LandSize = negotiationContract.LandSize,
                Registration = negotiationContract.Registration,
                ReservationValue = negotiationContract.ReservationValue,
                DownPaymentValue = negotiationContract.DownPaymentValue,
                DownPaymentMethod = negotiationContract.DownPaymentMethod,
                LandValue = negotiationContract.LandValue,
                ConstructionValue = negotiationContract.ConstructionValue,
                ImprovementsValue = negotiationContract.ImprovementsValue,
                HouseTotalValue = negotiationContract.HouseTotalValue,
                BuilderCompany = BuilderCompanyDTO.From(negotiationContract.BuilderCompany)
            };
        }
    }
}