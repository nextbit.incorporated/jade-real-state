using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Origins
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OriginController : ControllerBase
    {
        private readonly IOriginAppService _originAppService;
        public OriginController(IOriginAppService originAppService)
        {
            if (originAppService == null) throw new ArgumentException(nameof(originAppService));

            _originAppService = originAppService;
        }

        [HttpGet]
        [Route("")]
        [Authorize]
        public async Task<IActionResult> GetNationalitiesAsync()
        {
            return Ok(await _originAppService.GetNationalitiesAsync());
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<IEnumerable<OriginDTO>> GetPaged([FromQuery] OriginPagedRequest request)
        {
            return Ok(_originAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<OriginDTO> Post([FromBody] OriginRequest request)
        {
            return Ok(_originAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<OriginDTO> Put([FromBody] OriginRequest request)
        {
            return Ok(_originAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_originAppService.Delete(Id));
        }
    }
}