using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Origins
{
    public class OriginDTO : ResponseBase
    {
        public string Name { get; set; }
        public string Description { get; set; }

    }
}