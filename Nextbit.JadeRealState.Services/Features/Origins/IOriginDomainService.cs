using System;

namespace Nextbit.JadeRealState.Services.Features.Origins
{
    public interface IOriginDomainService : IDisposable
    {
        Origin Create(OriginRequest request);
        Origin Update(OriginRequest request, Origin originOldInfo);
    }
}