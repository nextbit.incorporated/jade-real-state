using System;

namespace Nextbit.JadeRealState.Services.Features.Origins
{
    public class OriginDomainService : IOriginDomainService
    {
        public Origin Create(OriginRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));

            Origin origin = new Origin.Builder()
           .WithName(request.Name)
           .WithDescription(request.Description ?? string.Empty)
           .WithAuditFields()
           .Build();

            return origin;
        }

        public Origin Update(OriginRequest request, Origin originOldInfo)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (originOldInfo == null) throw new ArgumentException(nameof(originOldInfo));

            originOldInfo.Update(request.Name, request.Description ?? string.Empty);
            return originOldInfo;
        }

        public void Dispose()
        {

        }
    }
}