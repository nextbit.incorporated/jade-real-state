using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Origins
{
    public class OriginAppService : IOriginAppService
    {
        private RealStateContext _context;
        private readonly IOriginDomainService _originDomainService;

        public OriginAppService(RealStateContext context, IOriginDomainService originDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (originDomainService == null) throw new ArgumentException(nameof(originDomainService));

            _context = context;
            _originDomainService = originDomainService;
        }

        public async Task<List<OriginDTO>> GetNationalitiesAsync()
        {
            IEnumerable<Origin> origins = await _context.Origin.ToListAsync();
            if (origins == null) return new List<OriginDTO>();
            return origins.Select(s => new OriginDTO
            {
                Id = s.Id,
                Name = s.Name,
                Description = s.Description,
            }).ToList();
        }

        public OriginDTO Create(OriginRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newOrigin = _originDomainService.Create(request);

            _context.Origin.Add(newOrigin);
            _context.SaveChanges();

            return new OriginDTO
            {
                Name = newOrigin.Name,
                Description = newOrigin.Description,
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var origin = _context.Origin.FirstOrDefault(s => s.Id == id);
            if (origin == null) throw new Exception("Nacionalidad not existe");
            _context.Origin.Remove(origin);
            _context.SaveChanges();

            return string.Empty;
        }

        public OriginPagedDTO GetPaged(OriginPagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            var count = Convert.ToDecimal(_context.Origin.Count());
            var totalPage = Math.Ceiling(count / request.PageSize);
            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;
            if (string.IsNullOrEmpty(request.Value))
            {
                var lista = _context.Origin.OrderByDescending(s => s.TransactionDate).Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new OriginPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Origins = lista.Select(s => new OriginDTO
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description
                    }).ToList()
                };
            }
            else
            {
                var lista = _context.Origin.Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new OriginPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Origins = lista.Select(s => new OriginDTO
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Description = s.Description,
                    }).ToList()
                };
            }
        }

        public OriginDTO Update(OriginRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldOrigin = _context.Origin.FirstOrDefault(s => s.Id == request.Id);
            if (oldOrigin == null) return new OriginDTO { ValidationErrorMessage = "Nacionalidad inexistente" };

            var originUpdate = _originDomainService.Update(request, oldOrigin);

            _context.Origin.Update(oldOrigin);
            _context.SaveChanges();

            return new OriginDTO
            {
                Id = originUpdate.Id,
                Name = originUpdate.Name,
                Description = originUpdate.Description,
            };
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_originDomainService != null) _originDomainService.Dispose();
        }
    }
}