using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Origins
{
    public class OriginPagedDTO
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<OriginDTO> Origins { get; set; }
    }
}