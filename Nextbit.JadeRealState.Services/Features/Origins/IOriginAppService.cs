using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Features.Origins
{
    public interface IOriginAppService : IDisposable
    {
        OriginPagedDTO GetPaged(OriginPagedRequest request);
        OriginDTO Create(OriginRequest request);
        OriginDTO Update(OriginRequest request);
        string Delete(int id);
        Task<List<OriginDTO>> GetNationalitiesAsync();
    }
}