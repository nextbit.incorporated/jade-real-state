using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using clients = Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.Origins
{
    [Table("Origins")]
    public class Origin : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

        public ICollection<Negotiation> Negotiations { get; set; }
        public ICollection<clients.Client> Clients { get; set; }

        public void Update(string _name, string _description)
        {
            Name = _name;
            Description = _description;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedSupplierService";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
            Status = "ACTIVO";
        }

        public class Builder
        {
            private readonly Origin _origin = new Origin();

            public Builder WithName(string name)
            {
                _origin.Name = name;
                return this;
            }

            public Builder WithDescription(string description)
            {
                _origin.Description = description;
                return this;
            }

            public Builder WithAuditFields()
            {
                _origin.CrudOperation = "Added";
                _origin.TransactionDate = DateTime.Now;
                _origin.TransactionType = "NewSupplierService";
                _origin.ModifiedBy = "Service";
                _origin.TransactionUId = Guid.NewGuid();
                _origin.Status = "ACTIVO";

                return this;
            }

            public Origin Build()
            {
                return _origin;
            }
        }

    }
}