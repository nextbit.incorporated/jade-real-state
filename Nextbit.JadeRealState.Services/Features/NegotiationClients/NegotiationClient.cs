using System;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.Negotiations;

namespace Nextbit.JadeRealState.Services.Features.NegotiationClients
{
    [Table("NegotiationClients")]
    public class NegotiationClient : Entity
    {
        public NegotiationClient()
        {
            SetAuditFields();
        }
        public int NegotiationId { get; private set; }
        public int ClientId { get; private set; }
        public bool Titular { get; private set; }
        public string Relationship { get; private set; }
        public string Workplace { get; private set; }
        public DateTime? WorkStartDate { get; private set; }
        public string HomeAddress { get; private set; }
        public bool OwnHome { get; private set; }
        public bool FirstHome { get; private set; }
        public bool ContributeToRap { get; private set; }
        public string Currency { get; private set; }
        public decimal GrossMonthlyIncome { get; private set; }
        public bool PreQualify { get; private set; }
        public DateTime? PrequalificationDate { get; private set; }
        public DateTime? NextPrequalificationDate { get; private set; }
        public decimal PrequalificationAmount { get; private set; }
        public bool HasDebts { get; private set; }
        public decimal DebtsAmount { get; private set; }

        public Negotiation Negotiation { get; set; }
        public Client Client { get; set; }

        internal void SetGrossMonthlyIncome(decimal grossMonthlyIncome)
        {
            GrossMonthlyIncome = grossMonthlyIncome;
            Client.SetGrossMonthlyIncome(grossMonthlyIncome);
        }

        internal void SetCurrency(string currency)
        {
            Currency = currency;
            Client.SetCurrency(currency);
        }

        internal void SetContributeToRap(bool contributeToRap)
        {
            ContributeToRap = contributeToRap;
            Client.SetContributeToRap(contributeToRap);
        }

        internal void SetOwnHome(bool ownHome)
        {
            OwnHome = ownHome;
            Client.SetOwnHome(ownHome);
        }

        internal void SetFirstHome(bool firstHome)
        {
            FirstHome = firstHome;
            Client.SetFirstHome(firstHome);
        }

        internal void SetHomeAddress(string homeAddress)
        {
            string encrypteHomeAddress = string.IsNullOrEmpty(homeAddress) ? "" : EncriptorHelper.EncryptString(homeAddress);

            HomeAddress = encrypteHomeAddress;
            Client.SetHomeAddress(encrypteHomeAddress);
        }

        internal void SetWorkStartDate(DateTime? workStartDate)
        {
            WorkStartDate = workStartDate;
            Client.SetWorkStartDate(workStartDate);
        }

        internal void SetActualWorkplace(string workplace)
        {
            Workplace = workplace;
            Client.SetWorkplace(workplace);
        }

        internal void SetTitular(bool titular)
        {

            Client.SetTitular(titular);
            Titular = titular;

        }

        internal void SetClient(Client client)
        {
            ClientId = client.Id;
            Client = client;
        }

        internal void SetRelationship(string relationship)
        {

            Client.SetRelationship(relationship);
            Relationship = relationship;

        }

        internal void SetWorkplace(string workplace)
        {

            Client.SetWorkplace(workplace);
            Workplace = workplace;

        }

        internal void SetPreQualify(bool preQualify)
        {
            PreQualify = preQualify;
        }

        internal void SetPrequalificationDate(DateTime? prequalificationDate)
        {
            PrequalificationDate = prequalificationDate;
        }

        internal void SetNextPrequalificationDate(DateTime? nextPrequalificationDate)
        {
            NextPrequalificationDate = nextPrequalificationDate;
        }

        internal void SetPrequalificationAmount(decimal prequalificationAmount)
        {
            PrequalificationAmount = prequalificationAmount;
        }

        internal void SetHasDebts(bool hasDebts)
        {
            HasDebts = hasDebts;
        }

        internal void SetDebtsAmount(decimal debtsAmount)
        {
            DebtsAmount = debtsAmount;
        }

        public class Builder
        {
            private readonly NegotiationClient _negotiationClient = new NegotiationClient();

            public Builder WithNegotiation(Negotiation negotiation)
            {
                _negotiationClient.NegotiationId = negotiation.Id;
                _negotiationClient.Negotiation = negotiation;

                return this;
            }

            public Builder WithClient(Client client)
            {
                _negotiationClient.SetClient(client);

                return this;
            }

            public Builder WithTitular(bool titular)
            {
                _negotiationClient.SetTitular(titular);

                return this;
            }

            public Builder WithRelationship(string relationship)
            {
                _negotiationClient.SetRelationship(relationship);

                return this;
            }

            public Builder WithWorkplace(string workplace)
            {
                _negotiationClient.SetWorkplace(workplace);

                return this;
            }

            internal Builder WithCurrency(string currency)
            {
                _negotiationClient.SetCurrency(currency);

                return this;
            }

            internal Builder WithGrossMonthlyIncome(decimal grossMonthlyIncome)
            {
                _negotiationClient.SetGrossMonthlyIncome(grossMonthlyIncome);

                return this;
            }

            public Builder WithWorkStartDate(DateTime? workStartDate)
            {
                _negotiationClient.WorkStartDate = workStartDate;

                return this;
            }

            public Builder WithHomeAddress(string homeAddress)
            {
                _negotiationClient.SetHomeAddress(homeAddress);

                return this;
            }

            public Builder WithOwnHome(bool ownHome)
            {
                _negotiationClient.SetOwnHome(ownHome);

                return this;
            }

            internal Builder WithNextPrequalificationDate(DateTime? nextPrequalificationDate)
            {
                _negotiationClient.NextPrequalificationDate = nextPrequalificationDate;

                return this;
            }

            internal Builder WithDebtsAmount(decimal debtsAmount)
            {
                _negotiationClient.DebtsAmount = debtsAmount;

                return this;
            }

            internal Builder WithHasDebts(bool hasDebts)
            {
                _negotiationClient.HasDebts = hasDebts;

                return this;
            }

            internal Builder WithPrequalificationAmount(decimal prequalificationAmount)
            {
                _negotiationClient.PrequalificationAmount = prequalificationAmount;

                return this;
            }

            internal Builder WithPrequalificationDate(DateTime? prequalificationDate)
            {
                _negotiationClient.PrequalificationDate = prequalificationDate;

                return this;
            }

            internal Builder WithPreQualify(bool preQualify)
            {
                _negotiationClient.PreQualify = preQualify;

                return this;
            }

            public Builder WithFirstHome(bool firstHome)
            {
                _negotiationClient.SetFirstHome(firstHome);

                return this;
            }


            public Builder WithContributeToRap(bool contributeToRap)
            {
                _negotiationClient.SetContributeToRap(contributeToRap);

                return this;
            }
            public NegotiationClient Build()
            {
                return _negotiationClient;
            }
        }


    }
}