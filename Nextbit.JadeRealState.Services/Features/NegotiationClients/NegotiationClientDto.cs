using System;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Negotiations;

namespace Nextbit.JadeRealState.Services.Features.NegotiationClients
{
    public sealed class NegotiationClientDto
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int NegotiationId { get; set; }
        public int ClientId { get; set; }
        public bool Titular { get; set; }
        public string Relationship { get; set; }
        public DateTime? WorkStartDate { get; set; }
        public string HomeAddress { get; set; }
        public bool OwnHome { get; set; }
        public bool FirstHome { get; set; }
        public bool ContributeToRap { get; set; }
        public string Currency { get; set; }
        public decimal GrossMonthlyIncome { get; set; }
        public bool PreQualify { get; set; }
        public DateTime? PrequalificationDate { get; set; }
        public DateTime? NextPrequalificationDate { get; set; }
        public decimal PrequalificationAmount { get; set; }
        public bool HasDebts { get; set; }
        public decimal DebtsAmount { get; set; }
        public int? ParentClientId { get; set; }
        public string ClientCode { get; set; }
        public int? NationalityId { get; set; }
        public string Nationality { get; set; }
        public int? ClientCategoryId { get; set; }
        public string ClientCategory { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string FirstSurname { get; set; }
        public string SecondSurname { get; set; }
        public string IdentificationCard { get; set; }
        public string PhoneNumber { get; set; }
        public string CellPhoneNumber { get; set; }
        public string Email { get; set; }
        public string Workplace { get; set; }
        public string Comments { get; set; }

        public DateTime TransactionDate { get; set; }

        public string ClientCreationComments { get; set; }

        internal static NegotiationClientDto From(Negotiation negotiation, bool titular)
        {
            NegotiationClient negotiationClient = negotiation?.GetNegotiationClient(titular);

            if (negotiationClient == null)
            {
                return null;
            }

            return new NegotiationClientDto
            {
                Id = negotiationClient.Id,
                CreationDate = negotiationClient.CreationDate,
                NegotiationId = negotiationClient.NegotiationId,
                ClientId = negotiationClient.ClientId,
                Titular = negotiationClient.Titular,
                Relationship = negotiationClient.Relationship,
                Workplace = negotiationClient.Workplace,
                WorkStartDate = negotiationClient.WorkStartDate,
                OwnHome = negotiationClient.OwnHome,
                FirstHome = negotiationClient.FirstHome,
                ContributeToRap = negotiationClient.ContributeToRap,
                Currency = negotiationClient.Currency,
                GrossMonthlyIncome = negotiationClient.GrossMonthlyIncome,
                PreQualify = negotiationClient.PreQualify,
                PrequalificationDate = negotiationClient.PrequalificationDate,
                NextPrequalificationDate = negotiationClient.NextPrequalificationDate,
                PrequalificationAmount = negotiationClient.PrequalificationAmount,
                HasDebts = negotiationClient.HasDebts,
                DebtsAmount = negotiationClient.DebtsAmount,
                ParentClientId = negotiationClient.Client?.ParentClientId,
                ClientCode = negotiationClient.Client?.ClientCode,
                NationalityId = negotiationClient.Client?.NationalityId,
                Nationality = negotiationClient.Client?.Nationality?.Name,
                ClientCategoryId = negotiationClient.Client?.ClientCategoryId,
                FirstName = negotiationClient.Client?.FirstName,
                MiddleName = negotiationClient.Client?.MiddleName,
                FirstSurname = negotiationClient.Client?.FirstSurname,
                SecondSurname = negotiationClient.Client?.SecondSurname,
                Comments = negotiationClient.Client?.Comments,
                TransactionDate = negotiation.TransactionDate,
                ClientCreationComments = negotiationClient.Client?.ClientCreationComments,
                IdentificationCard = EncriptorHelper.DecryptString(negotiationClient.Client?.IdentificationCard),
                PhoneNumber = EncriptorHelper.DecryptString(negotiationClient.Client?.PhoneNumber),
                CellPhoneNumber = EncriptorHelper.DecryptString(negotiationClient.Client?.CellPhoneNumber),
                Email = EncriptorHelper.DecryptString(negotiationClient.Client?.Email),
                HomeAddress = EncriptorHelper.DecryptString(negotiationClient.HomeAddress)
            };
        }
    }
}