namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    public class GetContactTypesRequest
    {

        public int Id { get; set; }
        public string Query { get; set; }
    }
}