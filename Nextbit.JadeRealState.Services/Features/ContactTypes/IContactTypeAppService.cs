using System;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    public interface IContactTypeAppService : IDisposable
    {
        ContactTypePagedDTO GetPaged(ContactTypePagedRequest request);
        ContactTypesDTO Create(ContactTypeRequest request);
        ContactTypesDTO Update(ContactTypeRequest request);
        string Delete(int id);
    }
}