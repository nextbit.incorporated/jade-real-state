using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.Prospects;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    [Table("ContactTypes")]
    public class ContactType : BaseContactType
    {
        private ContactType()
        {

        }

        public ICollection<Negotiation> Negotiations { get; set; }
        public ICollection<Client> Clients { get; set; }
        public ICollection<Prospect> Prospects { get; set; }

        internal void UpdateContactType(ContactTypeDto updatedContactType)
        {
            Name = updatedContactType.Name;

            SetAuditFields("Modified");
        }

        internal void Update(string name)
        {
            Name = name;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedSupplierService";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
        }



        public class Builder
        {
            private readonly ContactType _contactType = new ContactType();

            public Builder WithName(string name)
            {
                _contactType.Name = name;
                return this;
            }

            public Builder WithAuditFields()
            {
                _contactType.SetAuditFields("Added");

                return this;
            }

            public ContactType Build()
            {
                return _contactType;
            }
        }
    }
}