using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    public class ContactTypesDTO : ResponseBase
    {
        public string Name { get; set; }
    }
}