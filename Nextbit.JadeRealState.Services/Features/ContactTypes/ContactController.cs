using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ContactController : ControllerBase
    {
        private readonly IContactTypeAppService _contactTypeAppService;
        public ContactController(IContactTypeAppService contactTypeAppService)
        {
            if (contactTypeAppService == null) throw new ArgumentException(nameof(contactTypeAppService));

            _contactTypeAppService = contactTypeAppService;
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<IEnumerable<ContactTypePagedDTO>> GetPaged([FromQuery]ContactTypePagedRequest request)
        {
            return Ok(_contactTypeAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<ContactTypesDTO> Post([FromBody]ContactTypeRequest request)
        {
            return Ok(_contactTypeAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<ContactTypesDTO> Put([FromBody] ContactTypeRequest request)
        {
            return Ok(_contactTypeAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_contactTypeAppService.Delete(Id));
        }
    }
}