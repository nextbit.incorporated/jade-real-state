using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    public class ContactTypePagedDTO
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<ContactTypesDTO> ContactTypes { get; set; }
    }
}