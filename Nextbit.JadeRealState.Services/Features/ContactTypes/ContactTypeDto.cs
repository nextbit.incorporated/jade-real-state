using System.Collections.Generic;
using System.Linq;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    public sealed class ContactTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        internal static List<ContactTypeDto> From(IEnumerable<ContactType> contactTypes)
        {
            return (from qry in contactTypes select From(qry)).ToList();
        }

        internal static ContactTypeDto From(ContactType contactType)
        {
            return contactType != null
                ? new ContactTypeDto { Id = contactType.Id, Name = contactType.Name }
                : null;
        }
    }
}