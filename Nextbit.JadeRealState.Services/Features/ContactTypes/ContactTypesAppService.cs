using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    public class ContactTypesAppService : BaseDisposable
    {
        private readonly RealStateContext _context;

        public ContactTypesAppService(RealStateContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            _context = context;
        }

        public async Task<List<ContactTypeDto>> GetContactTypesAsync()
        {

            IEnumerable<ContactType> contactTypes = await _context.ContactTypes.ToListAsync();

            return ContactTypeDto.From(contactTypes);
        }

        public async Task<ContactTypeDto> GetContactTypeAsync(int id)
        {

            ContactType contactType = await _context.ContactTypes.FindAsync(id);
            return ContactTypeDto.From(contactType); ;
        }

        internal async Task<List<ContactTypeDto>> GetContactTypesAsync(string query)
        {

            IEnumerable<ContactType> contactTypes = string.IsNullOrWhiteSpace(query)
                ? await _context.ContactTypes.ToListAsync()
                : await FilterContactTypesAsync(query);

            return ContactTypeDto.From(contactTypes);
        }

        internal async Task<ContactTypeDto> UpdateContactTypeAsync(ContactTypeDto updatedContactType)
        {

            ContactType contactType = await _context.ContactTypes.FindAsync(updatedContactType.Id);

            if (contactType != null)
            {


                contactType.UpdateContactType(updatedContactType);



                await _context.SaveChangesAsync();

                return ContactTypeDto.From(contactType);
            }

            return null;
        }

        internal async Task<bool> DeleteContactTypeAsync(int id)
        {
            ContactType contactType = await _context.ContactTypes.FindAsync(id);

            if (contactType != null)
            {
                _context.ContactTypes.Remove(contactType);


                await _context.SaveChangesAsync();

                return true;

            }

            return false;
        }



        internal async Task<ContactTypeDto> CreateContactTypeAsync(ContactTypeDto contactType)
        {

            ContactType newContactType = new ContactType.Builder()
              .WithName(contactType.Name)
              .WithAuditFields()
              .Build();

            _context.ContactTypes.Add(newContactType);



            await _context.SaveChangesAsync();

            return ContactTypeDto.From(newContactType);
        }

        private async Task<IEnumerable<ContactType>> FilterContactTypesAsync(string query)
        {

            Expression<Func<ContactType, bool>> filterExpression = c => c.Name.Contains(query);


            return await _context.ContactTypes.Where(filterExpression).ToListAsync();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Release managed resources.
                _context?.Dispose();

            }

            // Release unmanaged resources.
            // Set large fields to null.
            // Call Dispose on your base class.
            base.Dispose(disposing);
        }

    }
}