using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    public abstract class BaseContactType : Entity
    {
        public string Name { get; protected set; }


    }
}