using System;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    public interface IContactTypeDomainService : IDisposable
    {
        ContactType Create(ContactTypeRequest request);
        ContactType Update(ContactTypeRequest request, ContactType contactTypeOldInfo);
    }
}