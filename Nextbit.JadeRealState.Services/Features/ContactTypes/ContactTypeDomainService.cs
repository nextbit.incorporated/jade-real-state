using System;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    public class ContactTypeDomainService : IContactTypeDomainService
    {
        public ContactType Create(ContactTypeRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));

            ContactType type = new ContactType.Builder()
           .WithName(request.Name)
           .WithAuditFields()
           .Build();

            return type;
        }

        public ContactType Update(ContactTypeRequest request, ContactType typeOldInfo)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (typeOldInfo == null) throw new ArgumentException(nameof(typeOldInfo));

            typeOldInfo.Update(request.Name);
            return typeOldInfo;
        }

        public void Dispose()
        {
        }

    }
}