using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    public class ContactTypeRequest : RequestBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class ContactTypePagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
    }
}