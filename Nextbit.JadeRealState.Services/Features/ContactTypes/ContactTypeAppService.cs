using System;
using System.Linq;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    public class ContactTypeAppService : IContactTypeAppService
    {
        private RealStateContext _context;
        private readonly IContactTypeDomainService _contactTypeDomainService;
        public ContactTypeAppService(RealStateContext context, IContactTypeDomainService contactTypeDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (contactTypeDomainService == null) throw new ArgumentException(nameof(contactTypeDomainService));

            _context = context;
            _contactTypeDomainService = contactTypeDomainService;
        }
        public ContactTypesDTO Create(ContactTypeRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newType = _contactTypeDomainService.Create(request);

            _context.ContactTypes.Add(newType);
            _context.SaveChanges();

            return new ContactTypesDTO
            {
                Name = newType.Name
            };
        }
        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var type = _context.ContactTypes.FirstOrDefault(s => s.Id == id);
            if (type == null) throw new Exception("Tipo de contacto not existe");
            _context.ContactTypes.Remove(type);
            _context.SaveChanges();

            return string.Empty;
        }
        public ContactTypePagedDTO GetPaged(ContactTypePagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            var count = Convert.ToDecimal(_context.ContactTypes.Count());
            var totalPage = Math.Ceiling(count / request.PageSize);
            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;
            if (string.IsNullOrEmpty(request.Value))
            {
                var lista = _context.ContactTypes.OrderByDescending(s => s.TransactionDate).Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ContactTypePagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    ContactTypes = lista.Select(s => new ContactTypesDTO
                    {
                        Id = s.Id,
                        Name = s.Name
                    }).ToList()
                };
            }
            else
            {
                var lista = _context.ContactTypes.Where(s => s.Name.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ContactTypePagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    ContactTypes = lista.Select(s => new ContactTypesDTO
                    {
                        Id = s.Id,
                        Name = s.Name
                    }).ToList()
                };
            }
        }

        public ContactTypesDTO Update(ContactTypeRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldType = _context.ContactTypes.FirstOrDefault(s => s.Id == request.Id);
            if (oldType == null) return new ContactTypesDTO { ValidationErrorMessage = "Tipo contacto inexistente" };

            var originUpdate = _contactTypeDomainService.Update(request, oldType);

            _context.SaveChanges();

            return new ContactTypesDTO
            {
                Id = originUpdate.Id,
                Name = originUpdate.Name
            };
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_contactTypeDomainService != null) _contactTypeDomainService.Dispose();
        }

    }
}