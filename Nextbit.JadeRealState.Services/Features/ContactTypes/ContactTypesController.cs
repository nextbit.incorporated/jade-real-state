using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.ContactTypes
{
    [Route(@"api/contact-types")]
    [ApiController]
    public sealed class ContactTypesController : ControllerBase
    {
        private readonly ContactTypesAppService _contactTypesAppService;

        public ContactTypesController(ContactTypesAppService contactTypesAppService)
        {
            if (contactTypesAppService == null) throw new ArgumentNullException(nameof(contactTypesAppService));

            _contactTypesAppService = contactTypesAppService;
        }


        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get([FromQuery] GetContactTypesRequest getContactTypesRequest)
        {
            if (getContactTypesRequest?.Id > 0)
            {
                ContactTypeDto contactType = await _contactTypesAppService.GetContactTypeAsync(getContactTypesRequest.Id);

                if (contactType != null)
                {
                    return Ok(contactType);
                }

                return NotFound();
            }

            List<ContactTypeDto> contactTypes = await _contactTypesAppService.GetContactTypesAsync(getContactTypesRequest?.Query);

            return Ok(contactTypes);
        }

        [HttpPost]
        // [Route("")]
        public async Task<IActionResult> Post([FromBody] ContactTypeDto contactType)
        {
            ContactTypeDto newContactType = await _contactTypesAppService.CreateContactTypeAsync(contactType);

            return Ok(newContactType);
        }

        [HttpPut]
        [Route("")]
        public async Task<IActionResult> Put([FromBody] ContactTypeDto updatedContactType)
        {
            ContactTypeDto contactType = await _contactTypesAppService.UpdateContactTypeAsync(updatedContactType);

            if (contactType != null)
            {
                return Ok(contactType);
            }

            return NotFound();
        }

        [HttpDelete]
        [Route("")]
        public async Task<IActionResult> Delete([FromQuery] int id)
        {
            bool deletedContactType = await _contactTypesAppService.DeleteContactTypeAsync(id);

            if (deletedContactType)
            {
                return Ok();
            }

            return NotFound();
        }




    }
}