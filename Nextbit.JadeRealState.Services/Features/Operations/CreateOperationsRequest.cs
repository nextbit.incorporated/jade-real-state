using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public class CreateOperationsRequest
    {
        public int ClientId { get; set; }
        public string OperationName { get; set; }
        public string OperationType { get; set; }

        public int? ClientCategoryId { get; set; }
        public string Profile { get; set; }

        public int? FinancialInstitutionId { get; set; }
        public string CreditOfficer { get; set; }

        public int? SalesAdvisorId { get; set; }
        public bool IsActive { get; set; }

        public List<string> SelectedOptions { get; set; } = new List<string>();
        public string Comments { get; set; }
        public bool ApplyOperationToCoDebtop { get; set; }
        public string User { get; set; }
    }
}