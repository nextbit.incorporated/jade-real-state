using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Operations
{
    [Route("api/operations")]
    [ApiController]
    [Authorize]

    public class OperationsController : ControllerBase
    {
        private readonly OperationsAppService _gestionesAppService;

        public OperationsController(OperationsAppService gestionesAppService)
        {
            _gestionesAppService = gestionesAppService ?? throw new ArgumentNullException(nameof(gestionesAppService));
        }

      
         [HttpGet]
         [Route("")]
         public async Task<IActionResult> Get([FromQuery] OperationReportRequest request)
        {
            if(request.TypeReport == "detail")
            {
                var response = await _gestionesAppService.GetCategoryReport(request);
                return Ok(response);
            }
            else
            {
                var gestiones = await _gestionesAppService.GetOperationsByDate(request);
                return Ok(gestiones);
            }
        }

         [HttpGet]
         [Route("gestiones")]
         public async Task<IActionResult> GetGestiones([FromQuery] OperationReportRequest request)
        {
            if(request.TypeReport == "detail")
            {
                var response = await _gestionesAppService.GetCategoryReport(request);
                return Ok(response);
            }
            else
            {
                var gestiones = await _gestionesAppService.GetOperationsByDate(request);
                return Ok(gestiones);
            }
        }

        
         [HttpGet]
         [Route("categorias-clientes")]
         [AllowAnonymous]
         public  ActionResult<IEnumerable<ResumenMensualCategorias>> GetCategiasClientesPorAnio([FromQuery] int anio)
        {
            var categoriasLista =  _gestionesAppService.ObtenerDetalleClientesCategoriaPorAnio(anio);
                return Ok(categoriasLista);
        }

        [HttpGet]
         [Route("categorias-clientes-por-agente")]
         [AllowAnonymous]
         public  ActionResult<IEnumerable<ResumenMensualCategorias>> GetCategiasClientesPorAnio([FromQuery] GetCategiasClientesPorAnioRequest request)
        {
            var categoriasLista =  _gestionesAppService.ObtenerDetalleClientesCategoriaPorAnioPorAgente(request);
                return Ok(categoriasLista);
        }

        [HttpGet]
         [Route("categorias-clientes-por-semana")]
         [AllowAnonymous]
         public  ActionResult<IEnumerable<ResumenMensualCategorias>> GetCategiasClientesPorSemana([FromQuery] GetCategiasClientesPorAnioRequest request)
        {
            var categoriasLista =  _gestionesAppService.GetCategoriesClientsByMonth(request);
                return Ok(categoriasLista);
        }




        [HttpPost]
        [Route("")]
        public async Task<IActionResult> Post([FromBody] CreateOperationsRequest createOperationsRequest)
        {
            OperationDto gestion = await _gestionesAppService.CreateOperationAsync(createOperationsRequest);

            return Ok(gestion);
        }

    }
}