using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public static class OperationTypes
    {
        public const string ChangeCategory = "Cambiar Categoria";
        public const string ChangeProfile = "Cambiar Perfil";
        public const string ChangeFinancialInstitution = "Cambiar Institucion Financiera";
        public const string ChangeSalesAdvisor = "Cambiar Asesor";
        public const string PromoteClient = "Promover Cliente";
        public const string ChangeObjections = "Cambiar Objeciones";


    }
}