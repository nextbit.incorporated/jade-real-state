using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public class ClientLevelDetailDto : ResponseBase
    {
        public int mes { get; set; }
        public int anio { get; set; }
        public string Clientlevel { get; set; }
        public int ClientCounts { get; set; }
        public List<OperationDetailDto> Children { get; set; }
        
    }
}