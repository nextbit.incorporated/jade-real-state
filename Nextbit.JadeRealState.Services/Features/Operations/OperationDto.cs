using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public class OperationDto
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string OperationName { get; set; }
        public string OperationType { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public List<string> SelectedOptions { get; set; } = new List<string>();
        public string Comments { get; set; }
        public string Error { get; set; }
        public string ClientName { get; set; }
        public string ModifiedBy { get; set; }
        
        public string OptionsOperation { get; set; }
        
        
        
        public DateTime? FechaTransaccion { get; set; }
        
        
        
        

        internal static OperationDto From(Operation operation)
        {
            return new OperationDto
            {
                Id = operation.Id,
                ClientId = operation.ClientId,
                OperationName = operation.OperationName,
                OperationType = operation.OperationType,
                OldValue = operation.OldValue,
                NewValue = operation.NewValue,
                SelectedOptions = operation.SelectedOptions.ConvertToStringList(),
                Comments = operation.Comments,
                ClientName = operation?.Client?.GetCompleteName(),
                FechaTransaccion = operation?.TransactionDate,
                ModifiedBy = operation?.ModifiedBy
            };
        }

        internal static List<OperationDto> FromList(IEnumerable<Operation> operations)
        {
              return (from query in operations select From(query)).ToList();
        }
    }
}