using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.ClientsLevel;
using Nextbit.JadeRealState.Services.Features.FinancialInstitutions;
using Nextbit.JadeRealState.Services.Features.Origins;
using Nextbit.JadeRealState.Services.Features.Prospects;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public class OperationsAppService : BaseDisposable
    {
        private readonly RealStateContext _context;
        int numero = 0;
        string categoria = "";
        public OperationsAppService(RealStateContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            _context = context;

        }

        internal async Task<OperationDto> CreateOperationAsync(CreateOperationsRequest createOperationsRequest)
        {
            if (createOperationsRequest.OperationType == OperationTypes.PromoteClient)
            {
                Operation operation = new Operation.Builder()
                    .WithOperationName(createOperationsRequest.OperationName)
                    .WithOperationType(createOperationsRequest.OperationType)
                    .WithSelectedOptions(createOperationsRequest.SelectedOptions.ConvertToCommaSeparatedString())
                    .WithComments(createOperationsRequest.Comments)
                    .Build();

                await PromoteClientAsync(operation, createOperationsRequest);

                return OperationDto.From(operation);
            }
            else
            {
                int clientId = createOperationsRequest.ClientId;

                Client client = ObtenerClient(clientId);

                if (client != null)
                {

                    Operation operation = await RegisterOperationAsync(client, createOperationsRequest);

                    if (createOperationsRequest.ApplyOperationToCoDebtop)
                    {
                        int id = client.ChildrenClients.FirstOrDefault()?.Id ?? 0;

                        if (id > 0)
                        {
                            Client coClient = ObtenerClient(id);

                            if (coClient != null)
                            {
                                await RegisterOperationAsync(coClient, createOperationsRequest);

                            }
                        }
                    }

                    await _context.SaveChangesAsync();

                    return OperationDto.From(operation);
                }

                return new OperationDto { Error = $"Cliente con id: {clientId} not existe" };
            }

        }

        private Client ObtenerClient(int id)
        {
            return _context.Clients
                            .Include(c => c.ClientLevel)
                            .Include(c => c.FinancialInstitution)
                            .Include(c => c.SalesAdvisor)
                            .Include(c => c.Operations)
                            .Include(c => c.ChildrenClients)
                            .FirstOrDefault(c => c.Id == id);
        }

        private async Task<Operation> RegisterOperationAsync(Client client, CreateOperationsRequest createOperationsRequest)
        {
            Operation operation = new Operation.Builder()
                .WithClient(client)
                .WithOperationName(createOperationsRequest.OperationName)
                .WithOperationType(createOperationsRequest.OperationType)
                .WithSelectedOptions(createOperationsRequest.SelectedOptions.ConvertToCommaSeparatedString())
                .WithComments(createOperationsRequest.Comments)
                .WithModifiedBy(createOperationsRequest.User)
                .Build();

            _context.Operations.Add(operation);
            client.Operations.Add(operation);

            await ApplyClientOperationAsync(client, operation, createOperationsRequest);

            ClientTransaction clientTransaction = new ClientTransaction(client, "Modified", createOperationsRequest.User);
            _context.ClientTransactions.Add(clientTransaction);

            return operation;
        }

        private async Task ApplyClientOperationAsync(Client client, Operation operation, CreateOperationsRequest createOperationsRequest)
        {
            switch (operation.OperationType)
            {
                case OperationTypes.ChangeCategory:
                    await ChangeClientCategoryAsync(client, operation, createOperationsRequest);
                    break;

                case OperationTypes.ChangeProfile:
                    ChangeProfile(client, operation, createOperationsRequest.Profile);
                    break;

                case OperationTypes.ChangeFinancialInstitution:
                    await ChangeFinancialInstitutionAsync(client, operation, createOperationsRequest);
                    break;

                case OperationTypes.ChangeSalesAdvisor:
                    await ChangeSalesAdvisorAsync(client, operation, createOperationsRequest);
                    break;

                case OperationTypes.PromoteClient:
                    await PromoteClientAsync(operation, createOperationsRequest);
                    break;

                case OperationTypes.ChangeObjections:
                    ChangeObjections(client, operation);
                    break;


                default:
                    return;
            }
        }

        private async Task PromoteClientAsync(Operation operation, CreateOperationsRequest createOperationsRequest)
        {
            Prospect prospect = await _context.Prospects
                .Include(c => c.SalesAdvisor)
                .Include(c => c.Project)
                .Include(c => c.ContactTypes)
                .FirstOrDefaultAsync(s => s.Id == createOperationsRequest.ClientId);

            Origin origin = _context.Origin.OrderBy(s => s.TransactionDate).FirstOrDefault();

            if (prospect != null && origin != null)
            {
                Client client = new Client.Builder()
                         .WithFirstName(prospect.Name)
                         .WithFirstSurname(prospect.Name)
                         .WithIdentificationCard(EncriptorHelper.EncryptString(prospect.IdNumber ?? string.Empty))
                         .WithEmail(EncriptorHelper.EncryptString(prospect.Email ?? string.Empty))
                         .WithCellPhoneNumber(EncriptorHelper.EncryptString(prospect.ContactNumberOne))
                         .WithContactTypeId(prospect.ContactType)
                         .WithProjectId(prospect.ProjectId)
                         .WithNationalityId(origin.Id)
                         .WithComments(prospect.Comments)
                         .WithSalesAdvisor(prospect.SalesAdvisor)
                         .WithIsActive(true)
                         .WithAuditFieldsByUser(createOperationsRequest.User, "PromoteClient")
                         .Build();

                operation.SetClient(client);

                client.Operations.Add(operation);

                _context.Clients.Add(client);
                _context.Prospects.Remove(prospect);
            }



        }

        private async Task ChangeSalesAdvisorAsync(Client client, Operation operation, CreateOperationsRequest createOperationsRequest)
        {
            User salesAdvisor = await _context.Users
                .FirstOrDefaultAsync(c => c.Id == createOperationsRequest.SalesAdvisorId);

            if (salesAdvisor != null)
            {
                string oldSalesAdvisor = $"{client.SalesAdvisor?.FirstName} {client.SalesAdvisor?.LastName}";
                string oldStatus = client.IsActive ? "Activo" : "Inactivo";

                string oldValue = $"Asesor: {oldSalesAdvisor}, Estado Cliente: {oldStatus}";

                string newSalesAdvisor = $"{salesAdvisor?.FirstName} {salesAdvisor?.LastName}";
                string newStatus = createOperationsRequest.IsActive ? "Activo" : "Inactivo";

                string newValue = $"Asesor: {newSalesAdvisor}, Estado Cliente: {newStatus}";

                operation.SetOldValue(oldValue);
                operation.SetNewValue(newValue);

                client.SetSalesAdvisor(salesAdvisor);
                client.SetIsActive(createOperationsRequest.IsActive);
            }
        }

        private async Task ChangeFinancialInstitutionAsync(Client client, Operation operation, CreateOperationsRequest createOperationsRequest)
        {
            int financialInstitutionId = createOperationsRequest.FinancialInstitutionId ?? 0;

            FinancialInstitution financialInstitution = await _context.FinancialInstitutions
                .FirstOrDefaultAsync(c => c.Id == createOperationsRequest.FinancialInstitutionId);

            string newValue = $"Institucion Financiera: {financialInstitution?.Name}, Oficial de Credito: {createOperationsRequest.CreditOfficer}";
            string oldValue = $"Institucion Financiera: {client.FinancialInstitution?.Name}, Oficial de Credito: {client.CreditOfficer}";

            operation.SetNewValue(newValue);
            operation.SetOldValue(oldValue);

            client.SetFinancialInstitution(financialInstitution);
            client.SetCreditOfficer(createOperationsRequest.CreditOfficer);
        }

        private void ChangeProfile(Client client, Operation operation, string newProfile)
        {
            operation.SetOldValue(client.Profile);
            operation.SetNewValue(newProfile);

            client.SetProfile(newProfile);
        }

        private void ChangeObjections(Client client, Operation operation)
        {
            Operation lastOperation = client.Operations
                .OrderByDescending(o => o.TransactionDate)
                .FirstOrDefault(o => o.OperationType == OperationTypes.ChangeObjections);

            operation.SetOldValue(lastOperation?.SelectedOptions);
            operation.SetNewValue(operation.SelectedOptions);

            client.SetObjections(operation.SelectedOptions);
        }

        private async Task ChangeClientCategoryAsync(Client client, Operation operation, CreateOperationsRequest createOperationsRequest)
        {
            int clientCategoryId = createOperationsRequest.ClientCategoryId ?? 0;

            ClientLevel clientLevel = await _context.ClientLevels.FirstOrDefaultAsync(c => c.Id == clientCategoryId);

            operation.SetOldValue(client.ClientLevel?.Description);
            operation.SetNewValue(clientLevel?.Description);

            client.SetIsActive(clientLevel?.Description != "Nivel D");
            client.SetClientLevel(clientLevel);
            client.SetOperationDate(DateTime.Now);
            client.SetCategoryItems(operation.SelectedOptions);
        }

        public async Task<IEnumerable<OperationDto>> GetOperationsByDate(OperationReportRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.User)) return new List<OperationDto>();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new List<OperationDto>();
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<OperationDto>();

            if (usuarioRoles.RolId != 3)
            {
                if (request.OnlyLastTop)
                {
                    var lastOperations = _context.Operations.Include(c => c.Client).OrderByDescending(s => s.TransactionDate).Take(20);
                    return OperationDto.FromList(lastOperations);
                }
                else
                {
                    if (string.IsNullOrEmpty(request.ClienteName))
                    {
                        var lastOperations = _context.Operations
                          .Where(s => s.TransactionDate >= request.InitialDate && s.TransactionDate <= request.FinalDate)
                          .Include(c => c.Client);
                        return OperationDto.FromList(lastOperations);
                    }
                    else
                    {
                        var lastOperations = _context.Operations
                         .Where(s => s.TransactionDate >= request.InitialDate
                                             && s.TransactionDate <= request.FinalDate)
                         .Include(c => c.Client);
                        var response = OperationDto.FromList(lastOperations);

                        return response.Where(s => s.ClientName.Contains(request.ClienteName)).ToList();
                    }
                }
            }
            else
            {
                if (request.OnlyLastTop)
                {
                    var lastOperations = _context.Operations.Where(s => s.ModifiedBy == request.User)
                    .Include(c => c.Client).OrderByDescending(s => s.TransactionDate).Take(20);
                    return OperationDto.FromList(lastOperations);
                }
                else
                {
                    if (string.IsNullOrEmpty(request.ClienteName))
                    {
                        var lastOperations = _context.Operations
                          .Where(s => s.TransactionDate >= request.InitialDate && s.TransactionDate <= request.FinalDate
                          && s.ModifiedBy == request.User)
                          .Include(c => c.Client);
                        return OperationDto.FromList(lastOperations);
                    }
                    else
                    {
                        var lastOperations = _context.Operations
                         .Where(s => s.TransactionDate >= request.InitialDate
                                             && s.TransactionDate <= request.FinalDate
                                             && s.ModifiedBy == request.User)
                         .Include(c => c.Client);
                        var response = OperationDto.FromList(lastOperations);

                        return response.Where(s => s.ClientName.Contains(request.ClienteName)).ToList();
                    }
                }
            }
        }


        public async Task<IEnumerable<ClientLevelDetailDto>> GetCategoryReport(OperationReportRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.User)) return new List<ClientLevelDetailDto>();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new List<ClientLevelDetailDto>();
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<ClientLevelDetailDto>();

            numero = 0;
            if (usuarioRoles.RolId != 3)
            {

                if(request.SaleAdvisor == "Todos")
                {
                    var clients = await _context.Clients.Where(s => (s.OperationDate >= request.InitialDate 
                                                                    && s.OperationDate <= request.FinalDate)
                                                                    ||
                                                                    (s.CreationDate >= request.InitialDate 
                                                                    && s.CreationDate <= request.FinalDate)
                                                                    )
                                            .Include(c => c.Negotiations)
                                            .Include(c => c.Alarms)
                                            .Include(c => c.Operations)
                                            .Include(c => c.FinancialInstitution)
                                            .Include(c => c.ClientsTracking)
                                            .Include(c => c.ParentClient)
                                            .Include(c => c.Project)
                                            .Include(c => c.ClientLevel).ToListAsync();


                var response = (from client in clients
                                group client by new { client.ClientCategoryId, client.ClientLevel } into g
                                select new ClientLevelDetailDto
                                {
                                    Clientlevel = g.Key.ClientLevel?.Level,
                                    ClientCounts = g.Count(),
                                    Children = g.Select(s => new OperationDetailDto
                                    {
                                        ClientId = s.Id,
                                        Numero = ObtenerNumero(g.Key.ClientLevel?.Level),
                                        Alarm = s.Alarms.OrderByDescending(s => s.TransactionDate).FirstOrDefault()?.Date,
                                        Category = g.Key.ClientLevel?.Description,
                                        ClientCode = s.ClientCode,
                                        Perfil = s.Profile ?? string.Empty,
                                        RegisteredDate = s.TransactionDate,
                                        ClientName = s.GetCompleteName(),
                                        CoDebName = s.ParentClient?.GetCompleteName(),
                                        FinancialInstitution = s.FinancialInstitution?.Name,
                                        OfficialName = s.CreditOfficer,
                                        NegotiationPhase = s.Negotiations.FirstOrDefault()?
                                                    .NegotiationPhases.Where(t => t.DateComplete == null)
                                                        .OrderBy(s => s.Order).FirstOrDefault()?.ServiceProcess?.Name,
                                        ObservacionesSeguimiento = s.ClientsTracking?.OrderByDescending(t => t.TransactionDate).FirstOrDefault()?.Comentarios,
                                        Objecciones = s.Objections,
                                        ProjectName = s.Project?.Name,
                                        Children = s.Operations?.Select(t => new OperationDto
                                        {
                                            ClientId = s.Id,
                                            OperationName = t.OperationName,
                                            OperationType = t.OperationType,
                                            OldValue = t.OldValue,
                                            NewValue = t.NewValue,
                                            Comments = t.Comments,
                                            ClientName = s.GetCompleteName(),
                                            OptionsOperation = t.SelectedOptions,
                                            ModifiedBy = t.ModifiedBy,
                                            FechaTransaccion = t.TransactionDate
                                        }).ToList()
                                    }).ToList()

                                }).ToList();

                 return response;                 
                }
                else
                {
                    var sAdvisor = _context.Users.FirstOrDefault(s => s.UserId == request.SaleAdvisor);
                    if (sAdvisor == null) return new List<ClientLevelDetailDto>();

                    var clients = await _context.Clients.Where(s => (s.OperationDate >= request.InitialDate 
                                                                    && s.OperationDate <= request.FinalDate 
                                                                     && s.SalesAdvisorId == sAdvisor.Id)
                                                                    ||
                                                                    (
                                                                        s.CreationDate >= request.InitialDate 
                                                                    && s.CreationDate <= request.FinalDate 
                                                                     && s.SalesAdvisorId == sAdvisor.Id
                                                                    )
                                                                   )
                                            .Include(c => c.Negotiations)
                                            .Include(c => c.Alarms)
                                            .Include(c => c.Operations)
                                            .Include(c => c.FinancialInstitution)
                                            .Include(c => c.ClientsTracking)
                                            .Include(c => c.ParentClient)
                                            .Include(c => c.Project)
                                            .Include(c => c.ClientLevel).ToListAsync();


                var response = (from client in clients
                                group client by new { client.ClientCategoryId, client.ClientLevel } into g
                                select new ClientLevelDetailDto
                                {
                                    Clientlevel = g.Key.ClientLevel?.Level,
                                    ClientCounts = g.Count(),
                                    Children = g.Select(s => new OperationDetailDto
                                    {
                                        ClientId = s.Id,
                                         Numero = ObtenerNumero(g.Key.ClientLevel?.Level),
                                        Alarm = s.Alarms.OrderByDescending(s => s.TransactionDate).FirstOrDefault()?.Date,
                                        Category = g.Key.ClientLevel?.Description,
                                        ClientCode = s.ClientCode,
                                        Perfil = s.Profile ?? string.Empty,
                                        RegisteredDate = s.TransactionDate,
                                        ClientName = s.GetCompleteName(),
                                        CoDebName = s.ParentClient?.GetCompleteName(),
                                        FinancialInstitution = s.FinancialInstitution?.Name,
                                        OfficialName = s.CreditOfficer,
                                        NegotiationPhase = s.Negotiations.FirstOrDefault()?
                                                    .NegotiationPhases.Where(t => t.DateComplete == null)
                                                        .OrderBy(s => s.Order).FirstOrDefault()?.ServiceProcess?.Name,
                                        ObservacionesSeguimiento = s.ClientsTracking?.OrderByDescending(t => t.TransactionDate).FirstOrDefault()?.Comentarios,
                                        Objecciones = s.Objections,
                                        ProjectName = s.Project?.Name,
                                        Children = s.Operations?.Select(t => new OperationDto
                                        {
                                            ClientId = s.Id,
                                            OperationName = t.OperationName,
                                            OperationType = t.OperationType,
                                            OldValue = t.OldValue,
                                            NewValue = t.NewValue,
                                            Comments = t.Comments,
                                            ClientName = s.GetCompleteName(),
                                            OptionsOperation = t.SelectedOptions,
                                            ModifiedBy = t.ModifiedBy,
                                            FechaTransaccion = t.TransactionDate
                                            
                                        }).ToList()
                                    }).ToList()

                                }).ToList();

                return response;
                }
                

            }
            else
            {
                var clients = await _context.Clients.Where(s => 
                                                        (s.OperationDate >= request.InitialDate
                                                       && s.OperationDate <= request.FinalDate && s.SalesAdvisorId == usuario.Id)
                                                       ||
                                                       ( (s.CreationDate >= request.InitialDate
                                                       && s.CreationDate <= request.FinalDate && s.SalesAdvisorId == usuario.Id))
                                                       )
                                           .Include(c => c.Negotiations)
                                           .Include(c => c.Alarms)
                                           .Include(c => c.Operations)
                                           .Include(c => c.FinancialInstitution)
                                           .Include(c => c.ClientsTracking)
                                           .Include(c => c.ParentClient)
                                           .Include(c => c.Project) 
                                           .Include(c => c.ClientLevel).ToListAsync();


                var response = (from client in clients
                                group client by new { client.ClientCategoryId, client.ClientLevel } into g
                                select new ClientLevelDetailDto
                                {
                                    Clientlevel = g.Key.ClientLevel?.Level,
                                    ClientCounts = g.Count(),
                                    Children = g.Select(s => new OperationDetailDto
                                    {
                                        ClientId = s.Id,
                                         Numero = ObtenerNumero(g.Key.ClientLevel?.Level),
                                        Alarm = s.Alarms.OrderByDescending(s => s.TransactionDate).FirstOrDefault()?.Date,
                                        Category = g.Key.ClientLevel?.Description,
                                        ClientCode = s.ClientCode,
                                        Perfil = s.Profile ?? string.Empty,
                                        RegisteredDate = s.TransactionDate,
                                        ClientName = s.GetCompleteName(),
                                        CoDebName = s.ParentClient?.GetCompleteName(),
                                        FinancialInstitution = s.FinancialInstitution?.Name,
                                        OfficialName = s.CreditOfficer,
                                        NegotiationPhase = s.Negotiations.FirstOrDefault()?
                                                    .NegotiationPhases.Where(t => t.DateComplete == null)
                                                        .OrderBy(s => s.Order).FirstOrDefault()?.ServiceProcess?.Name,
                                        ObservacionesSeguimiento = s.ClientsTracking?.OrderByDescending(t => t.TransactionDate).FirstOrDefault()?.Comentarios,
                                        Objecciones = s.Objections,
                                        ProjectName = s.Project?.Name,
                                        Children = s.Operations?.Select(t => new OperationDto
                                        {
                                            ClientId = s.Id,
                                            OperationName = t.OperationName,
                                            OperationType = t.OperationType,
                                            OldValue = t.OldValue,
                                            NewValue = t.NewValue,
                                            Comments = t.Comments,
                                            ClientName = s.GetCompleteName(),
                                            OptionsOperation = t.SelectedOptions,
                                            ModifiedBy = t.ModifiedBy,
                                            FechaTransaccion = t.TransactionDate
                                        }).ToList()
                                    }).ToList()

                                }).ToList();

                return response;
            }
        }

        public IEnumerable<ResumenMensualCategorias> ObtenerDetalleClientesCategoriaPorAnio(int anio)
        {
            var mesActual =  DateTime.Now.Month;

           var listaCategoriasMes = new List<ResumenMensualCategorias>();

           var categorias =  _context.ClientLevels.ToList();
           var categoriasDto =  ClientLevelDto.From(categorias);
           categoriasDto.Add(new ClientLevelDto
           {
               Id = 0,
               Description = "N/A"
           });

           var fechaMes1 = new DateTime(anio, 1, 1);
           var fechaMes2 = new DateTime(anio, 2, 1);
           var fechaMes3 = new DateTime(anio, 3, 1);
           var fechaMes4 = new DateTime(anio, 4, 1);
           var fechaMes5 = new DateTime(anio, 5, 1);
           var fechaMes6 = new DateTime(anio, 6, 1);
           var fechaMes7 = new DateTime(anio, 7, 1);
           var fechaMes8 = new DateTime(anio, 8, 1);
           var fechaMes9 = new DateTime(anio, 9, 1);
           var fechaMes10 = new DateTime(anio, 10, 1);
           var fechaMes11 = new DateTime(anio, 11, 1);
           var fechaMes12 = new DateTime(anio, 12, 1);
           var fechaMes12Final = new DateTime(anio, 12, 31);

           foreach (var item in categoriasDto)
           {
               if(item.Id == 0)
               {
                   var clientes = _context.Clients.Where(s => s.ClientCategoryId == null && s.CreationDate >= fechaMes1).ToList();

                   var clientesMes1 = clientes.Where(s => s.CreationDate >= fechaMes1 && s.CreationDate < fechaMes2);
                   var clientesMes2 = clientes.Where(s => s.CreationDate >= fechaMes2 && s.CreationDate < fechaMes3);
                   var clientesMes3 = clientes.Where(s => s.CreationDate >= fechaMes3 && s.CreationDate < fechaMes4);
                   var clientesMes4 = clientes.Where(s => s.CreationDate >= fechaMes4 && s.CreationDate < fechaMes5);
                   var clientesMes5 = clientes.Where(s => s.CreationDate >= fechaMes5 && s.CreationDate < fechaMes6);
                   var clientesMes6 = clientes.Where(s => s.CreationDate >= fechaMes6 && s.CreationDate < fechaMes7);
                   var clientesMes7 = clientes.Where(s => s.CreationDate >= fechaMes7 && s.CreationDate < fechaMes8);
                   var clientesMes8 = clientes.Where(s => s.CreationDate >= fechaMes8 && s.CreationDate < fechaMes9);
                   var clientesMes9 = clientes.Where(s => s.CreationDate >= fechaMes9 && s.CreationDate < fechaMes10);
                   var clientesMes10 = clientes.Where(s => s.CreationDate >= fechaMes10 && s.CreationDate < fechaMes11);
                   var clientesMes11 = clientes.Where(s => s.CreationDate >= fechaMes11 && s.CreationDate < fechaMes12);
                   var clientesMes12 = clientes.Where(s => s.CreationDate >= fechaMes12 && s.CreationDate < fechaMes12Final);

                   listaCategoriasMes.Add(new ResumenMensualCategorias
                   {
                       Anio = anio,
                       Clasificacion = item.Description,
                       Mes1 = clientesMes1.Count(),
                       Mes2 = clientesMes2.Count(),
                       Mes3 = clientesMes3.Count(),
                       Mes4 = clientesMes4.Count(),
                       Mes5 = clientesMes5.Count(),
                       Mes6 = clientesMes6.Count(),
                       Mes7 = clientesMes7.Count(),
                       Mes8 = clientesMes8.Count(),
                       Mes9 = clientesMes9.Count(),
                       Mes10 = clientesMes10.Count(),
                       Mes11 = clientesMes11.Count(),
                       Mes12 = clientesMes12.Count(),
                   });
               }
               else
               {
                    var clientes = _context.Clients.Where(s => s.ClientCategoryId == item.Id && s.CreationDate >= fechaMes1).ToList();
                        var clientesMes1 = clientes.Where(s => s.CreationDate >= fechaMes1 && s.CreationDate < fechaMes2);
                   var clientesMes2 = clientes.Where(s => s.CreationDate >= fechaMes2 && s.CreationDate < fechaMes3);
                   var clientesMes3 = clientes.Where(s => s.CreationDate >= fechaMes3 && s.CreationDate < fechaMes4);
                   var clientesMes4 = clientes.Where(s => s.CreationDate >= fechaMes4 && s.CreationDate < fechaMes5);
                   var clientesMes5 = clientes.Where(s => s.CreationDate >= fechaMes5 && s.CreationDate < fechaMes6);
                   var clientesMes6 = clientes.Where(s => s.CreationDate >= fechaMes6 && s.CreationDate < fechaMes7);
                   var clientesMes7 = clientes.Where(s => s.CreationDate >= fechaMes7 && s.CreationDate < fechaMes8);
                   var clientesMes8 = clientes.Where(s => s.CreationDate >= fechaMes8 && s.CreationDate < fechaMes9);
                   var clientesMes9 = clientes.Where(s => s.CreationDate >= fechaMes9 && s.CreationDate < fechaMes10);
                   var clientesMes10 = clientes.Where(s => s.CreationDate >= fechaMes10 && s.CreationDate < fechaMes11);
                   var clientesMes11 = clientes.Where(s => s.CreationDate >= fechaMes11 && s.CreationDate < fechaMes12);
                   var clientesMes12 = clientes.Where(s => s.CreationDate >= fechaMes12 && s.CreationDate < fechaMes12Final);
                   

                   listaCategoriasMes.Add(new ResumenMensualCategorias
                   {
                       Anio = anio,
                       Clasificacion = item.Description,
                       Mes1 = clientesMes1.Count(),
                       Mes2 = clientesMes2.Count(),
                       Mes3 = clientesMes3.Count(),
                       Mes4 = clientesMes4.Count(),
                       Mes5 = clientesMes5.Count(),
                       Mes6 = clientesMes6.Count(),
                       Mes7 = clientesMes7.Count(),
                       Mes8 = clientesMes8.Count(),
                       Mes9 = clientesMes9.Count(),
                       Mes10 = clientesMes10.Count(),
                       Mes11 = clientesMes11.Count(),
                       Mes12 = clientesMes12.Count(),
                   });
               }
           }

           return listaCategoriasMes;
        }

       public IEnumerable<ResumenMensualCategorias> ObtenerDetalleClientesCategoriaPorAnioPorAgente(GetCategiasClientesPorAnioRequest request)
        {

             if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.User)) return new List<ResumenMensualCategorias>();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new List<ResumenMensualCategorias>();
            var mesActual =  DateTime.Now.Month;

           var listaCategoriasMes = new List<ResumenMensualCategorias>();

           var categorias =  _context.ClientLevels.ToList();
           var categoriasDto =  ClientLevelDto.From(categorias);
           categoriasDto.Add(new ClientLevelDto
           {
               Id = 0,
               Description = "N/A"
           });

           var fechaMes1 = new DateTime(request.Anio, 1, 1);
           var fechaMes2 = new DateTime(request.Anio, 2, 1);
           var fechaMes3 = new DateTime(request.Anio, 3, 1);
           var fechaMes4 = new DateTime(request.Anio, 4, 1);
           var fechaMes5 = new DateTime(request.Anio, 5, 1);
           var fechaMes6 = new DateTime(request.Anio, 6, 1);
           var fechaMes7 = new DateTime(request.Anio, 7, 1);
           var fechaMes8 = new DateTime(request.Anio, 8, 1);
           var fechaMes9 = new DateTime(request.Anio, 9, 1);
           var fechaMes10 = new DateTime(request.Anio, 10, 1);
           var fechaMes11 = new DateTime(request.Anio, 11, 1);
           var fechaMes12 = new DateTime(request.Anio, 12, 1);
           var fechaMes12Final = new DateTime(request.Anio, 12, 31);

           foreach (var item in categoriasDto)
           {
               if(item.Id == 0)
               {
                   var clientes = _context.Clients.Where(s => s.ClientCategoryId == null 
                        && s.CreationDate >= fechaMes1
                        && s.SalesAdvisorId == usuario.Id).ToList();

                   var clientesMes1 = clientes.Where(s => s.CreationDate >= fechaMes1 && s.CreationDate < fechaMes2);
                   var clientesMes2 = clientes.Where(s => s.CreationDate >= fechaMes2 && s.CreationDate < fechaMes3);
                   var clientesMes3 = clientes.Where(s => s.CreationDate >= fechaMes3 && s.CreationDate < fechaMes4);
                   var clientesMes4 = clientes.Where(s => s.CreationDate >= fechaMes4 && s.CreationDate < fechaMes5);
                   var clientesMes5 = clientes.Where(s => s.CreationDate >= fechaMes5 && s.CreationDate < fechaMes6);
                   var clientesMes6 = clientes.Where(s => s.CreationDate >= fechaMes6 && s.CreationDate < fechaMes7);
                   var clientesMes7 = clientes.Where(s => s.CreationDate >= fechaMes7 && s.CreationDate < fechaMes8);
                   var clientesMes8 = clientes.Where(s => s.CreationDate >= fechaMes8 && s.CreationDate < fechaMes9);
                   var clientesMes9 = clientes.Where(s => s.CreationDate >= fechaMes9 && s.CreationDate < fechaMes10);
                   var clientesMes10 = clientes.Where(s => s.CreationDate >= fechaMes10 && s.CreationDate < fechaMes11);
                   var clientesMes11 = clientes.Where(s => s.CreationDate >= fechaMes11 && s.CreationDate < fechaMes12);
                   var clientesMes12 = clientes.Where(s => s.CreationDate >= fechaMes12 && s.CreationDate < fechaMes12Final);

                   listaCategoriasMes.Add(new ResumenMensualCategorias
                   {
                       Usuario = usuario.UserId,
                       Anio = request.Anio,
                       Clasificacion = item.Description,
                       Mes1 = clientesMes1.Count(),
                       Mes2 = clientesMes2.Count(),
                       Mes3 = clientesMes3.Count(),
                       Mes4 = clientesMes4.Count(),
                       Mes5 = clientesMes5.Count(),
                       Mes6 = clientesMes6.Count(),
                       Mes7 = clientesMes7.Count(),
                       Mes8 = clientesMes8.Count(),
                       Mes9 = clientesMes9.Count(),
                       Mes10 = clientesMes10.Count(),
                       Mes11 = clientesMes11.Count(),
                       Mes12 = clientesMes12.Count(),
                   });
               }
               else
               {
                    var clientes = _context.Clients.Where(s => s.ClientCategoryId == item.Id 
                            && s.CreationDate >= fechaMes1
                            && s.SalesAdvisorId == usuario.Id).ToList();

                   var clientesMes1 = clientes.Where(s => s.CreationDate >= fechaMes1 && s.CreationDate < fechaMes2);
                   var clientesMes2 = clientes.Where(s => s.CreationDate >= fechaMes2 && s.CreationDate < fechaMes3);
                   var clientesMes3 = clientes.Where(s => s.CreationDate >= fechaMes3 && s.CreationDate < fechaMes4);
                   var clientesMes4 = clientes.Where(s => s.CreationDate >= fechaMes4 && s.CreationDate < fechaMes5);
                   var clientesMes5 = clientes.Where(s => s.CreationDate >= fechaMes5 && s.CreationDate < fechaMes6);
                   var clientesMes6 = clientes.Where(s => s.CreationDate >= fechaMes6 && s.CreationDate < fechaMes7);
                   var clientesMes7 = clientes.Where(s => s.CreationDate >= fechaMes7 && s.CreationDate < fechaMes8);
                   var clientesMes8 = clientes.Where(s => s.CreationDate >= fechaMes8 && s.CreationDate < fechaMes9);
                   var clientesMes9 = clientes.Where(s => s.CreationDate >= fechaMes9 && s.CreationDate < fechaMes10);
                   var clientesMes10 = clientes.Where(s => s.CreationDate >= fechaMes10 && s.CreationDate < fechaMes11);
                   var clientesMes11 = clientes.Where(s => s.CreationDate >= fechaMes11 && s.CreationDate < fechaMes12);
                   var clientesMes12 = clientes.Where(s => s.CreationDate >= fechaMes12 && s.CreationDate < fechaMes12Final);
                   

                   listaCategoriasMes.Add(new ResumenMensualCategorias
                   {
                       Usuario = usuario.UserId,
                       Anio = request.Anio,
                       Clasificacion = item.Description,
                       Mes1 = clientesMes1.Count(),
                       Mes2 = clientesMes2.Count(),
                       Mes3 = clientesMes3.Count(),
                       Mes4 = clientesMes4.Count(),
                       Mes5 = clientesMes5.Count(),
                       Mes6 = clientesMes6.Count(),
                       Mes7 = clientesMes7.Count(),
                       Mes8 = clientesMes8.Count(),
                       Mes9 = clientesMes9.Count(),
                       Mes10 = clientesMes10.Count(),
                       Mes11 = clientesMes11.Count(),
                       Mes12 = clientesMes12.Count(),
                   });
               }
           }

           return listaCategoriasMes;
        }

        public IEnumerable<CategoryClientWeekNumber> GetCategoriesClientsByMonth(GetCategiasClientesPorAnioRequest request)
        {
             var inicioFechaMes = new DateTime(request.Anio, request.Mes, 1);
             var finalFechaMes = new DateTime(request.Anio, request.Mes + 1, 1);

             var clientes = _context.Clients.Where(s =>
                            s.CreationDate >= inicioFechaMes
                            && s.CreationDate < finalFechaMes)
                            .Include(c => c.ClientLevel).ToList();

            CultureInfo myCI = new CultureInfo("en-US");
            Calendar myCal = myCI.Calendar;
             CalendarWeekRule myCWR = myCI.DateTimeFormat.CalendarWeekRule;
      DayOfWeek myFirstDOW = myCI.DateTimeFormat.FirstDayOfWeek;
             var listaClientes = new List<ClientNumeroSemana>();
             foreach (var item in clientes)
             {
                 listaClientes.Add(new ClientNumeroSemana
                 {
                     Id = item.Id,
                     Categoria = item.ClientLevel?.Description,
                     Semana = myCal.GetWeekOfYear(item.CreationDate, myCWR, myFirstDOW )
                 });
             }      

            var lista =  (from client in listaClientes
                                group client by client.Semana into g
                                select new CategoryClientWeekNumber
                                {
                                    Year = request.Anio,
                                    Week = g.Key,
                                    LevelA = g.Select(s => s.Categoria == "Nivel A").Count(),
                                    LevelB = g.Select(s => s.Categoria == "Nivel B").Count(),
                                    LevelC = g.Select(s => s.Categoria == "Nivel C").Count(),
                                    LevelD = g.Select(s => s.Categoria == "Nivel D").Count(),
                                    LevelNA = g.Select(s => s.Categoria == "Null").Count()
                                }).OrderBy(s => s.Week).ToList();
            
            return lista;
        }

        private int ObtenerNumero(string level)
        {
           if(string.IsNullOrEmpty(level))
           {
               categoria = level;
               numero = 0;
                numero = numero + 1;
                 return numero;
           }
           else
           {
               if(categoria == level)
               {
                   numero = numero + 1;
                    return numero;
               }
               else
               {
                    categoria = level;
                    numero = 0;
                numero = numero + 1;
                 return numero;
               }
           }
        }

    }
}