using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public class ResumenMensualCategorias 
    {
        public string Usuario { get; set; }
        public int Anio { get; set; }
        public string Clasificacion { get; set; }
        public int Mes1 { get; set; }
        public int Mes2 { get; set; }
        public int Mes3 { get; set; }
        public int Mes4 { get; set; }
        public int Mes5 { get; set; }
        public int Mes6 { get; set; }
        public int Mes7 { get; set; }
        public int Mes8 { get; set; }
        public int Mes9 { get; set; }
        public int Mes10 { get; set; }
        public int Mes11 { get; set; }
        public int Mes12 { get; set; }
    }
}