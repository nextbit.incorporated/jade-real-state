using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.Features.Operations
{
    [Table("Operations")]

    public class Operation : Entity
    {
        private Operation() { }

        public int ClientId { get; set; }
        public string OperationName { get; set; }
        public string OperationType { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string SelectedOptions { get; set; }
        public string Comments { get; set; }

        public Client Client { get; set; }

        internal void SetClient(Client client)
        {
            ClientId = client.Id;
            Client = client;
        }

        internal void SetNewValue(string newValue)
        {
            NewValue = newValue;
        }

        internal void SetOldValue(string oldValue)
        {
            OldValue = oldValue;
        }

        public class Builder
        {
            private readonly Operation _operation = new Operation
            {
                CrudOperation = "Added",
                TransactionDate = DateTime.Now,
                TransactionType = "new-operation",
                ModifiedBy = "Service",
                TransactionUId = Guid.NewGuid()
            };

            public Builder WithClient(Client client)
            {
                _operation.SetClient(client);
                return this;
            }

            public Builder WithOperationName(string operationName)
            {
                _operation.OperationName = operationName;
                return this;
            }

            public Builder WithOperationType(string operationType)
            {
                _operation.OperationType = operationType;
                return this;
            }

            public Builder WithOldValue(string oldValue)
            {
                _operation.SetOldValue(oldValue);
                return this;
            }

            public Builder WithNewValue(string newValue)
            {
                _operation.SetNewValue(newValue);
                return this;
            }

            public Builder WithSelectedOptions(string selectedOptions)
            {
                _operation.SelectedOptions = selectedOptions;
                return this;
            }

            public Builder WithComments(string comments)
            {
                _operation.Comments = comments;
                return this;
            }

            public Builder WithModifiedBy(string modifiedBy)
            {
                _operation.ModifiedBy = modifiedBy;
                return this;
            }


            public Operation Build() => _operation;
        }


    }
}