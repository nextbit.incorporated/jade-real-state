using System;
using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public class OperationDetailDto : ResponseBase
    {
        public int ClientId { get; set; }
        public int Numero { get; set; }
        public string Category { get; set; }
        public string ClientCode { get; set; }
        public string Perfil { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string ClientName { get; set; }
        public string CoDebName { get; set; }
        public string CoDeudorName { get; set; }
        public string FinancialInstitution { get; set; }
        public string OfficialName { get; set; }
        public string NegotiationPhase { get; set; }
        public DateTime? Alarm { get; set; }
        public string ObservacionesSeguimiento { get; set; }
        public string Objecciones { get; set; }
        public string ProjectName { get; set; }

        public List<OperationDto> Children { get; set; }
        
    }
}