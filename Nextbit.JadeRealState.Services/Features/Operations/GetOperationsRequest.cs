using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public class GetOperationsRequest
    {
        public int ClientId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }
}