namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public class CategoryClientWeekNumber
    {
        public int Year { get; set; }
        public int Week { get; set; }
        public int LevelA{ get; set; }
        public int LevelB{ get; set; }
        public int LevelC{ get; set; }
        public int LevelD{ get; set; }
        public int LevelNA{ get; set; }
    }
}