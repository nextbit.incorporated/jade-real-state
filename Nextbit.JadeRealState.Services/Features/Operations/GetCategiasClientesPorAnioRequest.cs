namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public class GetCategiasClientesPorAnioRequest
    {
        public int Anio { get; set; }
        public string User { get; set; }
        public int Mes { get; set; }
    }
}