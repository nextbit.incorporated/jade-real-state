using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public class OperationReportRequest : RequestBase
    {
        public DateTime InitialDate { get; set; }
        public DateTime FinalDate { get; set; }
        public bool OnlyLastTop { get; set; }
        public string ClienteName { get; set; }

        public string user { get; set; }

        public string TypeReport { get; set; }
        public string SaleAdvisor { get; set; }
        
    }
}