namespace Nextbit.JadeRealState.Services.Features.Operations
{
    public class ClientNumeroSemana
    {
        public int Semana { get; set; }
        public string Categoria { get; set; }
        public int Id { get; set; }
    }
}