using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public class ResumenClientesPorCategoriaRequest : RequestBase
    {
        public int CantidadMeses { get; set; }
    }
}