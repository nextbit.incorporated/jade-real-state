using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public class ClientLevelCountByProject
    {
        public List<string> Labels { get; set; }
        public List<DataCientLevel> Datasets { get; set; }
        
        
    }

    public class DataCientLevel
    {
        public string Label { get; set; }
        
        public List<int> Data { get; set; }
        public string BackgroundColor { get; set; }
        
    }
}