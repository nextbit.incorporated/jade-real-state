using System;
using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Alarms;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public sealed class BaseClientDto
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? OperationDate { get; set; }

        public int? ParentClientId { get; set; }
        public bool Titular { get; set; }
        public string Relationship { get; set; }
        public string ClientCode { get; set; }
        public int? ProjectId { get; set; }
        public string Project { get; set; }
        public int? SupplierServiceId { get; set; }
        public string SupplierService { get; set; }
        public int? ContactTypeId { get; set; }
        public string ContactType { get; set; }
        public int? NationalityId { get; set; }
        public string Nationality { get; set; }
        public int? ClientCategoryId { get; set; }
        public string ClientCategory { get; set; }
        public int? SalesAdvisorId { get; set; }
        public string SalesAdvisor { get; set; }
        public int? FinancialInstitutionId { get; set; }
        public string FinancialInstitution { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string FirstSurname { get; set; }
        public string SecondSurname { get; set; }
        public string IdentificationCard { get; set; }
        public string PhoneNumber { get; set; }
        public string CellPhoneNumber { get; set; }
        public string Email { get; set; }
        public string Workplace { get; set; }
        public DateTime? WorkStartDate { get; set; }
        public string DebtorCurrency { get; set; }
        public decimal GrossMonthlyIncome { get; set; }
        public string HomeAddress { get; set; }
        public bool OwnHome { get; set; }
        public bool FirstHome { get; set; }

        public bool ContributeToRap { get; set; }
        public string Comments { get; set; }
        public string CreditOfficer { get; set; }
        public DateTime TransactionDate { get; set; }
        public string ClientCreationComments { get; set; }
        public bool IsActive { get; set; }
        public bool NegotiationEnded { get; set; }
        public string Profile { get; set; }
        public List<string> CategoryItems { get; set; } = new List<string>();
        public List<string> Objections { get; set; } = new List<string>();

        public List<AlarmDto> Alarms { get; set; } = new List<AlarmDto>();

        internal static BaseClientDto From(Client client)
        {
            if (client == null)
            {
                return null;
            }

            return new BaseClientDto
            {
                Id = client.Id,
                Titular = client.Titular,
                Relationship = client.Relationship,
                ParentClientId = client.ParentClientId,
                ClientCode = client.ClientCode,
                CreationDate = client.CreationDate,
                OperationDate = client.OperationDate,
                FirstName = client.FirstName,
                MiddleName = client.MiddleName,
                FirstSurname = client.FirstSurname,
                SecondSurname = client.SecondSurname,
                IdentificationCard = !string.IsNullOrWhiteSpace(client.IdentificationCard) ? EncriptorHelper.DecryptString(client.IdentificationCard) : null,
                PhoneNumber = !string.IsNullOrWhiteSpace(client.PhoneNumber) ? EncriptorHelper.DecryptString(client.PhoneNumber) : null,
                CellPhoneNumber = !string.IsNullOrWhiteSpace(client.CellPhoneNumber) ? EncriptorHelper.DecryptString(client.CellPhoneNumber) : null,
                Email = !string.IsNullOrWhiteSpace(client.Email) ? EncriptorHelper.DecryptString(client.Email) : null,
                HomeAddress = !string.IsNullOrWhiteSpace(client.HomeAddress) ? EncriptorHelper.DecryptString(client.HomeAddress) : null,
                ProjectId = client.ProjectId,
                Project = client.Project?.Name,
                SupplierServiceId = client.SupplierServiceId,
                SupplierService = client.SupplierService?.Name,
                ContactTypeId = client.ContactTypeId,
                ContactType = client.ContactType?.Name,
                NationalityId = client.NationalityId,
                Nationality = client.Nationality?.Name,
                ClientCategoryId = client.ClientLevel?.Id,
                ClientCategory = client.ClientLevel?.Level,
                SalesAdvisorId = client.SalesAdvisorId,
                SalesAdvisor = client.SalesAdvisor != null ? $"{client.SalesAdvisor?.FirstName} {client.SalesAdvisor?.LastName}" : null,
                Workplace = client.Workplace,
                WorkStartDate = client.WorkStartDate,
                DebtorCurrency = client.DebtorCurrency,
                GrossMonthlyIncome = client.GrossMonthlyIncome,
                OwnHome = client.OwnHome,
                FirstHome = client.FirstHome,
                ContributeToRap = client.ContributeToRap,
                Comments = client.Comments,
                ClientCreationComments = client.ClientCreationComments,
                FinancialInstitutionId = client.FinancialInstitutionId,
                FinancialInstitution = client.FinancialInstitution?.Name,
                CreditOfficer = client.CreditOfficer,
                TransactionDate = client.TransactionDate,
                Alarms = AlarmDto.FromAlarms(client.Alarms),
                IsActive = client.IsActive,
                NegotiationEnded = client.NegotiationEnded,
                Profile = client.Profile,
                CategoryItems = client.CategoryItems.ConvertToStringList(),
                Objections = client.Objections.ConvertToStringList()
            };
        }

    }
}