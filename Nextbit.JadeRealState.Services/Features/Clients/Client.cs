using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Alarms;
using Nextbit.JadeRealState.Services.Features.ClientsLevel;
using Nextbit.JadeRealState.Services.Features.ClientsTracking;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.FinancialInstitutions;
using Nextbit.JadeRealState.Services.Features.NegotiationClients;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.Operations;
using Nextbit.JadeRealState.Services.Features.Origins;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.ServicesTracking;
using Nextbit.JadeRealState.Services.Features.SupplierServices;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    [Table("Clients")]
    public class Client : Entity
    {
        private Client()
        {
            ServiceTrackings = new HashSet<ServiceTracking>();
            Negotiations = new HashSet<Negotiation>();
            ClientsTracking = new HashSet<ClientTracking>();
            Alarms = new HashSet<Alarm>();
            ChildrenClients = new HashSet<Client>();
        }

        public DateTime? OperationDate { get; set; }

        public bool Titular { get; private set; }
        public string Relationship { get; private set; }

        public int? ParentClientId { get; private set; }
        public string ClientCode { get; private set; }
        public int? ProjectId { get; private set; }
        public int? SupplierServiceId { get; private set; }
        public int? ContactTypeId { get; private set; }
        public int? NationalityId { get; private set; }
        public int? FinancialInstitutionId { get; private set; }
        public int? ClientCategoryId { get; private set; }
        public int? SalesAdvisorId { get; private set; }
        public string FirstName { get; set; }
        public string MiddleName { get; private set; }
        public string FirstSurname { get; set; }
        public string SecondSurname { get; private set; }
        public string FullTextSearchField { get; private set; }
        public string IdentificationCard { get; private set; }
        public string PhoneNumber { get; private set; }
        public string CellPhoneNumber { get; private set; }
        public string Email { get; set; }
        public string Workplace { get; private set; }
        public DateTime? WorkStartDate { get; set; }
        public string DebtorCurrency { get; private set; }
        public decimal GrossMonthlyIncome { get; private set; }
        public string HomeAddress { get; private set; }
        public bool OwnHome { get; private set; }
        public bool FirstHome { get; private set; }
        public bool ContributeToRap { get; private set; }
        public string Comments { get; private set; }
        public string CreditOfficer { get; private set; }
        public string ClientCreationComments { get; private set; }

        public bool IsActive { get; private set; }
        public bool NegotiationEnded { get; private set; }
        public string Profile { get; private set; }
        public string CategoryItems { get; private set; }
        public string Objections { get; private set; }



        public Client ParentClient { get; set; }
        public Project Project { get; set; }
        public SupplierService SupplierService { get; set; }
        public ContactType ContactType { get; set; }
        public Origin Nationality { get; set; }
        public FinancialInstitution FinancialInstitution { get; set; }
        public User SalesAdvisor { get; set; }
        public ClientLevel ClientLevel { get; set; }


        public ICollection<Client> ChildrenClients
        {
            get { return _childrenClients ?? (_childrenClients = new HashSet<Client>()); }
            private set { _childrenClients = value; }
        }
        private ICollection<Client> _childrenClients;

        public ICollection<Alarm> Alarms
        {
            get { return _alarms ?? (_alarms = new HashSet<Alarm>()); }
            private set { _alarms = value; }
        }
        private ICollection<Alarm> _alarms;

        public ICollection<ClientTracking> ClientsTracking
        {
            get { return _clientsTracking ?? (_clientsTracking = new HashSet<ClientTracking>()); }
            private set { _clientsTracking = value; }
        }
        private ICollection<ClientTracking> _clientsTracking;



        public ICollection<Negotiation> Negotiations
        {
            get { return _negotiations ?? (_negotiations = new HashSet<Negotiation>()); }
            private set { _negotiations = value; }
        }
        private ICollection<Negotiation> _negotiations;

        public ICollection<NegotiationClient> NegotiationClients
        {
            get { return _negotiationClients ?? (_negotiationClients = new HashSet<NegotiationClient>()); }
            private set { _negotiationClients = value; }
        }
        private ICollection<NegotiationClient> _negotiationClients;


        public ICollection<ServiceTracking> ServiceTrackings
        {
            get { return _serviceTrackings ?? (_serviceTrackings = new HashSet<ServiceTracking>()); }
            private set { _serviceTrackings = value; }
        }

        public ICollection<Operation> Operations { get; private set; } = new HashSet<Operation>();

        internal void SetCategoryItems(string categoryItems)
        {
            CategoryItems = categoryItems;
        }

        internal void SetObjections(string objections)
        {
            Objections = objections;
        }


        internal void SetOperationDate(DateTime operationDate)
        {
            OperationDate = operationDate;
        }

        internal void SetIsActive(bool isActive)
        {
            IsActive = isActive;
        }

        internal void SetNegotiationEnded(bool negotiationEnded)
        {
            NegotiationEnded = negotiationEnded;
        }

        private ICollection<ServiceTracking> _serviceTrackings;

        internal void SetTitular(bool titular)
        {
            Titular = titular;
        }

        internal void SetRelationship(string relationship)
        {
            Relationship = relationship;
        }

        internal void SetParent(Client client)
        {
            ParentClientId = client?.Id;
            ParentClient = client;
        }

        internal void SetClientLevel(ClientLevel clientLevel)
        {
            ClientCategoryId = clientLevel?.Id;
            ClientLevel = clientLevel;
        }

        public string GetKeyNameClient()
        {
            var name = FirstName.Trim()
                + (string.IsNullOrEmpty(FirstName) ? "" : (" " + FirstSurname.Trim()));

            return name;
        }


        public string GetCompleteName()
        {
            var name = FirstName
                + (string.IsNullOrEmpty(MiddleName) ? "" : (" " + MiddleName))
                + (string.IsNullOrEmpty(FirstSurname) ? "" : (" " + FirstSurname))
                + (string.IsNullOrEmpty(SecondSurname) ? "" : (" " + SecondSurname));

            return name;
        }




        private string GenerateClientCode(int id)
        {
            string clientCode = ClientCode;

            if (string.IsNullOrWhiteSpace(ClientCode))
            {
                if (CreationDate != null && SalesAdvisor?.UserCode != null)
                {
                    string userCode = SalesAdvisor.UserCode.Trim().PadLeft(3, '0');
                    string year = CreationDate.Year.ToString().Substring(2, 2);
                    string month = CreationDate.Month.ToString().PadLeft(2, '0');
                    string clientId = id.ToString().PadLeft(5, '0');

                    clientCode = $"{userCode}{year}{month}{clientId}";
                }
            }

            return clientCode;
        }

        internal void UpdateClient(BaseClientDto updatedClient,
            Project project, ContactType contactType,
            Origin origin,
            FinancialInstitution financialInstitution,
            User salesAdvisor,
            ClientLevel clientLevel,
            SupplierService supplierService,
            string user)
        {
            ParentClientId = updatedClient.ParentClientId;
            CreationDate = updatedClient.CreationDate;
            SalesAdvisorId = salesAdvisor?.Id;
            SalesAdvisor = salesAdvisor;
            ClientCode = GenerateClientCode(Id);
            ProjectId = project?.Id;
            Project = project;
            SupplierServiceId = supplierService?.Id;
            SupplierService = supplierService;
            ContactTypeId = contactType?.Id;
            ContactType = contactType;
            NationalityId = origin?.Id;
            Nationality = origin;
            ClientCategoryId = clientLevel?.Id;
            ClientLevel = clientLevel;
            FinancialInstitutionId = financialInstitution?.Id;
            FinancialInstitution = financialInstitution;

            FirstName = updatedClient.FirstName;
            MiddleName = updatedClient.MiddleName;
            FirstSurname = updatedClient.FirstSurname;
            SecondSurname = updatedClient.SecondSurname;
            FullTextSearchField = GetFullTextSearchFieldValue(updatedClient);
            IdentificationCard = EncriptorHelper.EncryptString(updatedClient.IdentificationCard);
            PhoneNumber = EncriptorHelper.EncryptString(updatedClient.PhoneNumber);
            CellPhoneNumber = EncriptorHelper.EncryptString(updatedClient.CellPhoneNumber);
            Email = EncriptorHelper.EncryptString(updatedClient.Email);
            Workplace = updatedClient.Workplace;
            WorkStartDate = updatedClient.WorkStartDate;
            DebtorCurrency = updatedClient.DebtorCurrency;
            GrossMonthlyIncome = updatedClient.GrossMonthlyIncome;
            HomeAddress = EncriptorHelper.EncryptString(updatedClient.HomeAddress);
            OwnHome = updatedClient.OwnHome;
            FirstHome = updatedClient.FirstHome;
            ContributeToRap = updatedClient.ContributeToRap;
            Comments = updatedClient.Comments;
            ClientCreationComments = updatedClient.ClientCreationComments;
            CreditOfficer = updatedClient.CreditOfficer;
            NegotiationEnded = updatedClient.NegotiationEnded;


            SetAuditFields("Modified");
            ModifiedBy = user;
        }

        internal void UpdateFullTextSearchField()
        {
            FullTextSearchField = GetFullTextSearchFieldValue(this);
        }



        private string GetFullTextSearchFieldValue(Client client)
        {
            return GetLoweredString(client?.FirstName) + " "
                + GetLoweredString(client?.MiddleName) + " "
                + GetLoweredString(client?.FirstSurname) + " "
                + GetLoweredString(client?.SecondSurname) + " "
                + GetLoweredString(client?.ClientCode);
        }

        private string GetFullTextSearchFieldValue(BaseClientDto client)
        {
            return GetLoweredString(client?.FirstName) + " "
                + GetLoweredString(client?.MiddleName) + " "
                + GetLoweredString(client?.FirstSurname) + " "
                + GetLoweredString(client?.SecondSurname) + " "
                + GetLoweredString(client?.ClientCode) + " "
                + GetLoweredString(client?.IdentificationCard);
        }

        internal string GetName()
        {
            return $"{GetLoweredString(FirstName)}{GetLoweredString(FirstSurname)}";
        }

        private string GetLoweredString(string stringValue)
        {
            return stringValue?.Trim().ToLower() ?? string.Empty;
        }

        internal void SetWorkplace(string workplace)
        {
            Workplace = workplace;
        }

        private void SetAuditFields(string crudOperation)
        {
            CrudOperation = crudOperation;
            TransactionDate = DateTime.Now;
            TransactionType = "ClientsManagement";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
        }

        internal void SetProfile(string profile)
        {
            Profile = profile;
        }

        internal void SetContributeToRap(bool contributeToRap)
        {
            ContributeToRap = contributeToRap;
        }

        internal void SetOwnHome(bool ownHome)
        {
            OwnHome = ownHome;
        }

        internal void SetFirstHome(bool firstHome)
        {
            FirstHome = firstHome;
        }

        internal void SetHomeAddress(string homeAddress)
        {
            HomeAddress = homeAddress;
        }

        internal void SetGrossMonthlyIncome(decimal grossMonthlyIncome)
        {
            GrossMonthlyIncome = grossMonthlyIncome;
        }

        internal void SetCurrency(string currency)
        {
            DebtorCurrency = currency;
        }

        internal void SetWorkStartDate(DateTime? workStartDate)
        {
            WorkStartDate = workStartDate;
        }

        internal void RemoveParent()
        {
            ParentClientId = null;
            ParentClient = null;
        }

        internal void SetFinancialInstitution(FinancialInstitution financialInstitution)
        {
            FinancialInstitutionId = financialInstitution?.Id;
            FinancialInstitution = financialInstitution;
        }

        internal void SetCreditOfficer(string creditOfficer)
        {
            CreditOfficer = creditOfficer;
        }

        internal void SetSalesAdvisor(User salesAdvisor)
        {
            SalesAdvisorId = salesAdvisor.Id;
            SalesAdvisor = salesAdvisor;
        }

        public class Builder
        {
            private readonly Client _client = new Client();

            public Builder WithClient(BaseClientDto client,
                Project project, ContactType contactType,
                Origin origin,
                FinancialInstitution financialInstitution,
                User user, ClientLevel clientLevel, int lastCLientId, string username)
            {
                _client.ParentClientId = client.ParentClientId;
                _client.ClientCode = client.ClientCode;
                _client.CreationDate = client.CreationDate;
                _client.SalesAdvisorId = user?.Id;
                _client.SalesAdvisor = user;
                _client.ClientCode = _client.GenerateClientCode(lastCLientId);
                _client.ProjectId = project?.Id;
                _client.Project = project;
                _client.ContactTypeId = contactType?.Id;
                _client.ContactType = contactType;
                _client.NationalityId = origin?.Id;
                _client.Nationality = origin;
                _client.ClientCategoryId = clientLevel?.Id;
                _client.ClientLevel = clientLevel;

                _client.FinancialInstitutionId = financialInstitution?.Id;
                _client.FinancialInstitution = financialInstitution;
                _client.FirstName = client.FirstName;
                _client.MiddleName = client.MiddleName;
                _client.FirstSurname = client.FirstSurname;
                _client.SecondSurname = client.SecondSurname;
                _client.FullTextSearchField = _client.GetFullTextSearchFieldValue(client);
                _client.IdentificationCard = EncriptorHelper.EncryptString(client.IdentificationCard);
                _client.PhoneNumber = EncriptorHelper.EncryptString(client.PhoneNumber);
                _client.CellPhoneNumber = EncriptorHelper.EncryptString(client.CellPhoneNumber);
                _client.Email = EncriptorHelper.EncryptString(client.Email);
                _client.Workplace = client.Workplace;
                _client.WorkStartDate = client.WorkStartDate;
                _client.DebtorCurrency = client.DebtorCurrency;
                _client.GrossMonthlyIncome = client.GrossMonthlyIncome;
                _client.HomeAddress = EncriptorHelper.EncryptString(client.HomeAddress);
                _client.OwnHome = client.OwnHome;
                _client.FirstHome = client.FirstHome;
                _client.ContributeToRap = client.ContributeToRap;
                _client.Comments = client.Comments;
                _client.ClientCreationComments = client.ClientCreationComments;
                _client.CreditOfficer = client.CreditOfficer;
                _client.ModifiedBy = username;
                _client.NegotiationEnded = client.NegotiationEnded;
                return this;
            }



            public Builder WithProject(Project project)
            {
                _client.ProjectId = project?.Id;
                _client.Project = project;
                return this;
            }

            public Builder WithContactType(ContactType contactType)
            {
                _client.ContactTypeId = contactType?.Id;
                _client.ContactType = contactType;
                return this;
            }

            public Builder WithOrigin(Origin origin)
            {
                _client.NationalityId = origin?.Id;
                _client.Nationality = origin;
                return this;
            }



            public Builder WithProjectId(int? projectId)
            {
                _client.ProjectId = projectId;
                return this;
            }

            public Builder WithContactTypeId(int? contactTypeId)
            {
                _client.ContactTypeId = contactTypeId;
                return this;
            }



            public Builder WithFirstName(string firstName)
            {
                _client.FirstName = firstName;
                return this;
            }



            public Builder WithFirstSurname(string firstSurname)
            {
                _client.FirstSurname = firstSurname;
                return this;
            }


            public Builder WithNationalityId(int nationalityId)
            {
                _client.NationalityId = nationalityId;
                return this;
            }



            public Builder WithIdentificationCard(string identificationCard)
            {
                _client.IdentificationCard = identificationCard;
                return this;
            }


            public Builder WithCellPhoneNumber(string cellPhoneNumber)
            {
                _client.CellPhoneNumber = cellPhoneNumber;
                return this;
            }

            public Builder WithEmail(string email)
            {
                _client.Email = email;
                return this;
            }



            public Builder WithComments(string comments)
            {
                _client.Comments = comments;
                return this;
            }
            public Builder WithSalesAdvisor(User salesAdvisor)
            {
                _client.SalesAdvisorId = salesAdvisor?.Id;
                _client.SalesAdvisor = salesAdvisor;
                return this;
            }

            public Builder WithIsActive(bool isActive)
            {
                _client.IsActive = isActive;
                return this;
            }

            public Builder WithNegotiationEnded(bool negotiationEnded)
            {
                _client.NegotiationEnded = negotiationEnded;
                return this;
            }

            public Builder WithProfile(string profile)
            {
                _client.SetProfile(profile);
                return this;
            }



            public Builder WithAuditFields()
            {
                _client.CrudOperation = "Added";
                _client.TransactionDate = DateTime.Now;
                _client.TransactionType = "NewClient";
                _client.ModifiedBy = "Service";
                _client.TransactionUId = Guid.NewGuid();
                return this;
            }

              public Builder WithAuditFieldsByUser(string user, string transactionType)
            {
                _client.CrudOperation = "Added";
                _client.TransactionDate = DateTime.Now;
                _client.TransactionType = string.IsNullOrEmpty(transactionType) ? "NewClient" : transactionType;
                _client.ModifiedBy = string.IsNullOrEmpty(user) ? "Service" : user;
                _client.TransactionUId = Guid.NewGuid();
                return this;
            }

            public Client Build()
            {
                return _client;
            }
        }


    }
}