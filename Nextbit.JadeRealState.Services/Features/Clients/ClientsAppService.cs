using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.ClientsLevel;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.FinancialInstitutions;
using Nextbit.JadeRealState.Services.Features.Origins;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.Prospects;
using Nextbit.JadeRealState.Services.Features.Reports.Dashboard;
using Nextbit.JadeRealState.Services.Features.SupplierServices;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public class ClientsAppService
    {
        private readonly RealStateContext _context;
        public ClientsAppService(RealStateContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public List<Client> GetCustomers()
        {
            return _context.Clients.ToList();
        }

        public Client GetCustomerById(int id)
        {
            return _context.Clients.FirstOrDefault(s => s.Id == id);
        }

        public int GetCountClients()
        {
            return _context.Clients.Count();
        }

        internal async Task<List<ClientDto>> GetClientsAsync(ClientRequest request)
        {
            if (string.IsNullOrEmpty(request.User)) return new List<ClientDto>();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new List<ClientDto>();
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<ClientDto>();

            if (usuarioRoles.RolId != 3)
            {
                IEnumerable<Client> clients = string.IsNullOrWhiteSpace(request.Query)
                    ? await _context.Clients
                    .Include(c => c.Project)
                    .Include(c => c.ContactType)
                    .Include(c => c.Nationality)
                    .Include(c => c.FinancialInstitution)
                    .Include(c => c.SalesAdvisor)
                    .Include(c => c.ChildrenClients)
                    .ToListAsync()
                    : await GetClients(request.Query);

                return ClientDto.From(clients);
            }
            else
            {
                IEnumerable<Client> clients = string.IsNullOrWhiteSpace(request.Query)
                    ? await _context.Clients.Where(s => s.SalesAdvisorId == usuario.Id)
                    .Include(c => c.Project)
                    .Include(c => c.ContactType)
                    .Include(c => c.Nationality)
                    .Include(c => c.FinancialInstitution)
                    .Include(c => c.SalesAdvisor)
                    .Include(c => c.ChildrenClients)
                    .ToListAsync()
                    : await GetClientsBySaleAdvisor(request.Query, usuario.Id);
                return ClientDto.From(clients);
            }

        }

        internal List<filterDTO> GetFilters(ClientRequest request)
        {
            return new List<filterDTO>
            {
                new filterDTO
                {
                 FilterId = 1,
                 Filter = "Cliente"
                },
                new filterDTO
                {
                 FilterId = 3,
                 Filter = "Agente"
                }

            };
        }

        internal async Task<List<UserDTO>> GetUsersReport(ClientRequest request)
        {
            if (string.IsNullOrEmpty(request.User)) return new List<UserDTO>();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new List<UserDTO>();
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<UserDTO>();

            if (usuarioRoles.RolId != 3)
            {
                var users = await _context.Users.ToListAsync();

                
                var lista =  (users.Select(s => new UserDTO
                {
                    UserId = s.UserId,
                    UserCode = s.UserCode,
                    FirstName = s.FirstName,
                    SecondName = s.SecondName,
                    LastName = s.LastName
                })).ToList();

                lista.Add(new UserDTO{ UserId = "Todos", UserCode = "Todos", FirstName = "Todos"});
                return lista;
            }
            else
            {
                var users = await _context.Users.Where(s => s.UserId == usuario.UserId).ToListAsync();

                return (users.Select(s => new UserDTO
                {
                    UserId = s.UserId,
                    UserCode = s.UserCode,
                    FirstName = s.FirstName,
                    SecondName = s.SecondName,
                    LastName = s.LastName
                })).ToList();
            }
        }

        internal async Task<List<ClientDto>> GetClientsReportByUserAsync(ClientRequest request)
        {
            if (request == null) return new List<ClientDto>();
            if (string.IsNullOrEmpty(request.User)) return new List<ClientDto>();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new List<ClientDto>();
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<ClientDto>();
            if (request.FechaInicio == DateTime.MinValue) return new List<ClientDto> { new ClientDto { ValidationErrorMessage = "Fecha incorrecta" } };
            if (request.FechaInicio == DateTime.MaxValue) return new List<ClientDto> { new ClientDto { ValidationErrorMessage = "Fecha incorrecta" } };
            if (request.FechaFinal == DateTime.MinValue) return new List<ClientDto> { new ClientDto { ValidationErrorMessage = "Fecha incorrecta" } };
            if (request.FechaFinal == DateTime.MaxValue) return new List<ClientDto> { new ClientDto { ValidationErrorMessage = "Fecha incorrecta" } };

            DateTime startDate = new DateTime(request.FechaInicio.Year, request.FechaInicio.Month, request.FechaInicio.Day);
            DateTime endDate = new DateTime(request.FechaFinal.Year, request.FechaFinal.Month, request.FechaFinal.Day, 23, 59, 59);

            if (request.clientLevelId == 0)
            {
                var clients = await _context.Clients.Where(
                s => ((s.CreationDate >= startDate && s.CreationDate <= endDate)
                || (s.OperationDate != null && s.OperationDate >= startDate && s.OperationDate <= endDate))
                && s.SalesAdvisorId == usuario.Id)
                    .Include(c => c.Project)
                    .Include(c => c.ContactType)
                    .Include(c => c.Nationality)
                    .Include(c => c.FinancialInstitution)
                    .Include(c => c.SalesAdvisor)
                    .Include(c => c.ChildrenClients)
                    .Include(c => c.ClientLevel)
                    .ToListAsync();

                return ClientDto.From(clients);
            }

            var clientsByLevel = await _context.Clients.Where(
                 s => ((s.CreationDate >= startDate && s.CreationDate <= request.FechaFinal)
                 || (s.OperationDate != null && s.OperationDate >= startDate && s.OperationDate <= endDate))
                     && s.SalesAdvisorId == usuario.Id && s.ClientCategoryId == request.clientLevelId)
                     .Include(c => c.Project)
                     .Include(c => c.ContactType)
                     .Include(c => c.Nationality)
                     .Include(c => c.FinancialInstitution)
                     .Include(c => c.SalesAdvisor)
                     .Include(c => c.ChildrenClients)
                     .Include(c => c.ClientLevel)
                     .ToListAsync();

            return ClientDto.From(clientsByLevel);

        }

        internal async Task<List<ClientDto>> GetClientsReportAsync(ClientRequest request)
        {
            if (request == null) return new List<ClientDto>();
            if (string.IsNullOrEmpty(request.User)) return new List<ClientDto>();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new List<ClientDto>();
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<ClientDto>();
            if (request.FechaInicio == DateTime.MinValue) return new List<ClientDto> { new ClientDto { ValidationErrorMessage = "Fecha incorrecta" } };
            if (request.FechaInicio == DateTime.MaxValue) return new List<ClientDto> { new ClientDto { ValidationErrorMessage = "Fecha incorrecta" } };
            if (request.FechaFinal == DateTime.MinValue) return new List<ClientDto> { new ClientDto { ValidationErrorMessage = "Fecha incorrecta" } };
            if (request.FechaFinal == DateTime.MaxValue) return new List<ClientDto> { new ClientDto { ValidationErrorMessage = "Fecha incorrecta" } };

            if (usuarioRoles.RolId != 3)
            {
                DateTime startDate = new DateTime(request.FechaInicio.Year, request.FechaInicio.Month, request.FechaInicio.Day);
                DateTime endDate = new DateTime(request.FechaFinal.Year, request.FechaFinal.Month, request.FechaFinal.Day, 23, 59, 59);

                var clients = await _context.Clients.Where(
                    s => s.ParentClient == null
                    && ((s.CreationDate >= startDate && s.CreationDate <= endDate) || (s.OperationDate != null && s.OperationDate >= startDate && s.OperationDate <= endDate))
                    )
                        .Include(c => c.Project)
                        .Include(c => c.ContactType)
                        .Include(c => c.Nationality)
                        .Include(c => c.FinancialInstitution)
                        .Include(c => c.SalesAdvisor)
                        .Include(c => c.ChildrenClients)
                        .Include(c => c.ClientLevel)
                        .ToListAsync();

                return ClientDto.From(clients);
            }

            return new List<ClientDto>();
        }

        internal object GetPaged(ProspectPagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.User)) return new List<ClientDto>();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new List<ClientDto>();
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<ClientDto>();
            if (request.PageIndex == 0) request.PageIndex = 1;

            if (usuarioRoles.RolId != 3)
            {
                if (string.IsNullOrEmpty(request.Value))
                {
                    var count = Convert.ToDecimal(_context.Clients.Count());
                    var totalPage = Math.Ceiling(count / request.PageSize);
                    if (request.PageIndex > totalPage) request.PageIndex = 1;
                    if (request.PageIndex <= 0) request.PageIndex = 1;

                    IEnumerable<Client> clients = _context.Clients
                    .OrderByDescending(s => s.TransactionDate)
                    .Where(c => (c.IsActive || !request.IsActive))
                    .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize)
                        .Include(c => c.Project)
                        .Include(c => c.ContactType)
                        .Include(c => c.Nationality)
                        .Include(c => c.FinancialInstitution)
                        .Include(c => c.SalesAdvisor)
                        .Include(c => c.ChildrenClients)
                        .Include(c => c.ClientLevel)
                        .Include(c => c.Alarms)
                        .ToList();

                    var clientList = ClientDto.From(clients);

                    return new ClientPagedDto
                    {
                        PageIndex = request.PageIndex,
                        PageSize = request.PageSize,
                        PageCount = Convert.ToInt16(totalPage),
                        Clients = clientList.ToList()
                    };
                }
                else
                {
                    return GetPagedGeneralByFirstNameFilter(request);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(request.Value))
                {
                    var count = Convert.ToDecimal(_context.Clients.Where(s => s.SalesAdvisorId == usuario.Id).Count());
                    var totalPage = Math.Ceiling(count / request.PageSize);
                    if (request.PageIndex > totalPage) request.PageIndex = 1;
                    if (request.PageIndex <= 0) request.PageIndex = 1;
                    IEnumerable<Client> clients = _context.Clients
                    .Where(s => (s.IsActive || !request.IsActive) && s.SalesAdvisorId == usuario.Id)
                    .OrderByDescending(s => s.TransactionDate)
                    .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize)
                        .Include(c => c.Project)
                        .Include(c => c.ContactType)
                        .Include(c => c.Nationality)
                        .Include(c => c.FinancialInstitution)
                        .Include(c => c.SalesAdvisor)
                        .Include(c => c.ChildrenClients)
                        .Include(c => c.ClientLevel)
                        .Include(c => c.Alarms)
                        .ToList();

                    var clientList = ClientDto.From(clients);
                    return new ClientPagedDto
                    {
                        PageIndex = request.PageIndex,
                        PageSize = request.PageSize,
                        PageCount = Convert.ToInt16(totalPage),
                        Clients = clientList
                    };
                }
                else
                {
                    return GetPagedSaleAdvisorByFirstNameFilter(request, usuario);


                }
            }
        }

        private ClientPagedDto GetPagedSaleAdvisorByFirstNameFilter(ProspectPagedRequest request, User usuario)
        {
            string searchValue = request?.Value.Trim().ToLower() ?? string.Empty;

            IEnumerable<Client> clientsTotal = _context.Clients
                    .Where(s => (s.IsActive || !request.IsActive) && s.FullTextSearchField.Contains(searchValue) && s.SalesAdvisorId == usuario.Id)
                    .OrderByDescending(s => s.TransactionDate)
                    .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize)
                    .Include(c => c.Project)
                    .Include(c => c.ContactType)
                    .Include(c => c.Nationality)
                    .Include(c => c.FinancialInstitution)
                    .Include(c => c.SalesAdvisor)
                    .Include(c => c.ChildrenClients)
                    .Include(c => c.ClientLevel)
                    .Include(c => c.Alarms)
                    .ToList();

            var cc = ClientDto.From(clientsTotal);
            var count = Convert.ToDecimal(_context.Clients.Where(s => s.FirstName.ToUpper().StartsWith(request.Value.ToUpper()) && s.SalesAdvisorId == usuario.Id).Count());
            var totalPage = Math.Ceiling(count / request.PageSize);
            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;

            return new ClientPagedDto
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                PageCount = Convert.ToInt16(totalPage),
                Clients = cc
            };
        }

        private object GetPagedGeneralByFirstNameFilter(ProspectPagedRequest request)
        {
            string searchValue = request?.Value.Trim().ToLower() ?? string.Empty;

            IEnumerable<Client> clientsTotal = _context.Clients
                    .Where(s => (s.IsActive || !request.IsActive) && s.FullTextSearchField.Contains(searchValue))
                    .OrderByDescending(s => s.TransactionDate)
                    .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize)
                    .Include(c => c.Project)
                    .Include(c => c.ContactType)
                    .Include(c => c.Nationality)
                    .Include(c => c.FinancialInstitution)
                    .Include(c => c.SalesAdvisor)
                    .Include(c => c.ChildrenClients)
                    .Include(c => c.ClientLevel)
                    .Include(c => c.Alarms)
                    .ToList();
            var cc = ClientDto.From(clientsTotal);
            var count = Convert.ToDecimal(_context.Clients.Where(s => s.FirstName.ToUpper().StartsWith(request.Value.ToUpper())).Count());
            var totalPage = Math.Ceiling(count / request.PageSize);
            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;

            return new ClientPagedDto
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
                PageCount = Convert.ToInt16(totalPage),
                Clients = cc
            };
        }

        private async Task<IEnumerable<Client>> GetClientsBySaleAdvisor(
            string query, int userId)
        {
            return await _context.Clients
            .Include(c => c.Project)
            .Include(client => client.ContactType)
            .Include(c => c.Nationality)
            .Include(c => c.FinancialInstitution)
            .Include(c => c.Alarms)
            .Where(c => c.FullTextSearchField.Contains(query) && c.SalesAdvisorId == userId).ToListAsync();
        }

        private async Task<IEnumerable<Client>> GetClients(string query)
        {
            return await _context.Clients
            .Include(c => c.Project)
            .Include(client => client.ContactType)
            .Include(c => c.Nationality)
            .Include(c => c.FinancialInstitution)
            .Include(c => c.Alarms)
            .Where(c => c.FirstName.Contains(query) || c.FirstSurname.Contains(query)).ToListAsync();
        }

        internal ClientDto ValidateNewClientCreation(ClientDto newClient)
        {
            if (newClient == null) throw new ArgumentNullException(nameof(newClient));

            ClientDto newClientValidation = GetNewClientValidationErrors(newClient.Principal);

            if (!string.IsNullOrWhiteSpace(newClientValidation.ValidationErrorMessage))
            {
                return newClientValidation;
            }

            if (newClient.CoDebtorAssignmentType == CoDebtorAssignmentTypes.CreateNewCoDebtor && newClient.CoDebtor != null)
            {
                return GetNewClientValidationErrors(newClient.CoDebtor);
            }

            return new ClientDto();
        }

        private ClientDto GetNewClientValidationErrors(BaseClientDto newClient)
        {
            string firstName = newClient.FirstName?.Trim().ToLower() ?? string.Empty;
            string firstSurname = newClient.FirstSurname?.Trim().ToLower() ?? string.Empty;




            if (string.IsNullOrWhiteSpace(firstName))
            {
                return new ClientDto { ValidationErrorMessage = "El nombre del nuevo cliente es Requerido" };
            }
            if (string.IsNullOrWhiteSpace(firstSurname))
            {
                return new ClientDto { ValidationErrorMessage = "El apellido del nuevo cliente es Requerido" };
            }



            // Valdiating the client's id
            if (!string.IsNullOrWhiteSpace(newClient.IdentificationCard))
            {
                string encriptedId = EncriptorHelper.EncryptString(newClient.IdentificationCard);

                Client clientWithSameId = _context.Clients.FirstOrDefault(c => c.IdentificationCard == encriptedId);

                if (clientWithSameId != null)
                {
                    return new ClientDto
                    {
                        ValidationErrorMessage = $@"Ya existen un cliente con Identificación: {newClient.IdentificationCard}, el cliente encontrado es: {clientWithSameId.GetCompleteName()}"
                    };
                }

            }




            List<Client> principalClients = _context.Clients
                .Where(c => c.FullTextSearchField.Contains(firstName))
                .ToList();

            // Validating the client's name
            string fullName = $"{firstName}{firstSurname}";
            Client client = principalClients.FirstOrDefault(c => c.GetName() == fullName);

            if (client != null)
            {
                return new ClientDto
                {
                    ValidationErrorMessage = $@"Ya existe un cliente similar con nombre: {newClient.FirstName} {newClient.FirstSurname}, el cliente encontrado es: {client.GetCompleteName()} con numero de identidad: {EncriptorHelper.DecryptString(client.IdentificationCard)}"
                };
            }



            return new ClientDto();
        }

        internal async Task<ClientDto> CreateClientAsync(ClientDto newClient)
        {
            Client client = await CreateClientAsync(newClient.Principal, newClient.User);

            Client coDeptor = await GetCoDeptorAsync(newClient.CoDebtor, newClient.CoDebtorAssignmentType, newClient.User);

            if (coDeptor != null)
            {
                coDeptor.SetParent(client);
                client.ChildrenClients.Add(coDeptor);
            }

            _context.Clients.Add(client);
            await _context.SaveChangesAsync();
      
            

            ClientTransaction clientTransaction = new ClientTransaction(client, "Added", newClient.User);

            _context.ClientTransactions.Add(clientTransaction);

            await _context.SaveChangesAsync();


            return ClientDto.From(client);
        }

        private async Task<Client> CreateClientAsync(BaseClientDto newClient, string user)
        {
            Project project = await _context.Projects.FirstOrDefaultAsync(p => p.Id == newClient.ProjectId);
            ContactType contactType = await _context.ContactTypes.FirstOrDefaultAsync(c => c.Id == newClient.ContactTypeId);
            Origin origin = await _context.Origin.FirstOrDefaultAsync(o => o.Id == newClient.NationalityId);
            FinancialInstitution financialInstitution = await _context.FinancialInstitutions.FirstOrDefaultAsync(o => o.Id == newClient.FinancialInstitutionId);
            User salesAdvisor = await _context.Users.FirstOrDefaultAsync(u => u.Id == newClient.SalesAdvisorId);
            ClientLevel clientCategory = await _context.ClientLevels.FirstOrDefaultAsync(c => c.Id == newClient.ClientCategoryId);
            Client lastCreatedClient = await _context.Clients.OrderByDescending(c => c.Id).FirstOrDefaultAsync();

            int lastCLientId = lastCreatedClient != null
                ? lastCreatedClient.Id + 1
                : 1;

            Client client = new Client.Builder()
                .WithAuditFields()
                .WithClient(newClient, project, contactType, origin, financialInstitution, salesAdvisor, clientCategory, lastCLientId, user)
                .WithProject(project)
                .WithContactType(contactType)
                .WithOrigin(origin)
                .WithIsActive(true)
              .Build();

            return client;
        }

        private async Task<Client> GetCoDeptorAsync(BaseClientDto CoDebtor, string coDebtorAssignmentType, string user)
        {

            if (CoDebtor == null)
            {
                return null;
            }

            if (coDebtorAssignmentType == CoDebtorAssignmentTypes.AssignCoDebtor)
            {
                return _context.Clients.FirstOrDefault(c => c.Id == CoDebtor.Id);
            }
            else
            {
                if (coDebtorAssignmentType == CoDebtorAssignmentTypes.CreateNewCoDebtor)
                {
                    return await CreateClientAsync(CoDebtor, user);
                }
            }

            return null;
        }

        internal async Task<ClientDto> UpdateClientAsync(ClientDto updatedClient)
        {
            // Updating Principal
            Client persistedClient = await _context.Clients
                .Include(c => c.ChildrenClients)
                .FirstOrDefaultAsync(c => c.Id == updatedClient.Principal.Id);

            if (persistedClient != null)
            {
                await SyncClientAsync(persistedClient, updatedClient.Principal, updatedClient.User);

                switch (updatedClient.CoDebtorAssignmentType)
                {
                    // deeed to create codeptor
                    case CoDebtorAssignmentTypes.CreateNewCoDebtor:
                        Client newCoDeptor = await GetCoDeptorAsync(updatedClient.CoDebtor, updatedClient.CoDebtorAssignmentType, updatedClient.User);

                        if (newCoDeptor != null)
                        {
                            newCoDeptor.SetParent(persistedClient);
                            persistedClient.ChildrenClients.Add(newCoDeptor);
                        }
                        break;

                    // update codeptor    
                    case CoDebtorAssignmentTypes.AssignCoDebtor:
                        Client updatedCoDeptor = await GetCoDeptorAsync(updatedClient.CoDebtor, updatedClient.CoDebtorAssignmentType, updatedClient.User);

                        if (updatedCoDeptor != null)
                        {
                            await SyncClientAsync(updatedCoDeptor, updatedClient.CoDebtor, updatedClient.User);

                            List<Client> clientChildren = persistedClient.ChildrenClients.ToList();

                            foreach (Client client in clientChildren)
                            {
                                client.RemoveParent();
                                persistedClient.ChildrenClients.Remove(client);
                            }
                            updatedCoDeptor.SetParent(persistedClient);
                            persistedClient.ChildrenClients.Add(updatedCoDeptor);
                        }
                        break;

                    // remove codeptor
                    case CoDebtorAssignmentTypes.RemoveCoDebtor:
                        Client codebtorToRemove = await _context.Clients
                            .FirstOrDefaultAsync(c => c.ParentClientId == persistedClient.Id);

                        if (codebtorToRemove != null)
                        {
                            codebtorToRemove.RemoveParent();
                        }

                        persistedClient.ChildrenClients.Clear();
                        break;

                    default:
                        if (updatedClient.CoDebtor != null)
                        {
                            Client persistedCoDebtor = await _context.Clients
                                .Include(c => c.ChildrenClients)
                                .FirstOrDefaultAsync(c => c.Id == updatedClient.CoDebtor.Id);

                            await SyncClientAsync(persistedCoDebtor, updatedClient.CoDebtor, updatedClient.User);
                        }
                        break;
                }

                ClientTransaction clientTransaction = new ClientTransaction(persistedClient, "Modified", updatedClient.User);
                _context.ClientTransactions.Add(clientTransaction);

                await _context.SaveChangesAsync();

                return ClientDto.From(persistedClient);
            }

            return null;
        }

        private async Task SyncClientAsync(Client persistedClient, BaseClientDto updatedClient, string user)
        {
            if (persistedClient == null || updatedClient == null)
            {
                return;
            }

            Project project = await _context.Projects.FirstOrDefaultAsync(p => p.Id == updatedClient.ProjectId);
            ContactType contactType = await _context.ContactTypes.FirstOrDefaultAsync(c => c.Id == updatedClient.ContactTypeId);
            Origin origin = await _context.Origin.FirstOrDefaultAsync(o => o.Id == updatedClient.NationalityId);
            FinancialInstitution financialInstitution = await _context.FinancialInstitutions.FirstOrDefaultAsync(o => o.Id == updatedClient.FinancialInstitutionId);
            User salesAdvisor = await _context.Users.FirstOrDefaultAsync(u => u.Id == updatedClient.SalesAdvisorId);
            ClientLevel clientCategory = await _context.ClientLevels.FirstOrDefaultAsync(c => c.Id == updatedClient.ClientCategoryId);
            SupplierService supplierService = await _context.SupplierService.FirstOrDefaultAsync(s => s.Id == updatedClient.SupplierServiceId);

            persistedClient.UpdateClient(updatedClient, project, contactType,
                origin, financialInstitution, salesAdvisor, clientCategory, supplierService, user);
        }

        public DataReportCountClientsDTO GetReportCountClient()
        {
            var date = DateTime.Now.AddYears(-1);
            var countClients = GetDataClientByDate(date);
            var data = (from c in countClients
                        group c by c.TransactionDate into g
                        select new SumClientByDateDTO
                        {
                            Fecha = g.Key,
                            ConteoClientes = g.Count()
                        }).OrderBy(s => s.Fecha).ToList();
            var dataReport = new DataReportCountClientsDTO();
            dataReport.Fechas = new List<DateTime>();
            dataReport.CountClients = new List<int>();
            dataReport.Fechas = data.Select(s => s.Fecha).ToList();
            dataReport.CountClients = data.Select(s => s.ConteoClientes).ToList();

            return dataReport;
        }

        private IEnumerable<BaseClientDto> GetDataClientByDate(DateTime date)
        {
            return _context.Clients.Where(s => s.TransactionDate >= date)
            .Select(s => new BaseClientDto
            {
                Id = s.Id,
                TransactionDate = new DateTime(s.TransactionDate.Year, s.TransactionDate.Month, s.TransactionDate.Day)
            }).ToList();
        }

        public DataReportCountClientsDTO GetReportCountClientSixMonths()
        {
            var date = DateTime.Now.AddMonths(-6);
            var countClients = GetDataClientByDateSixMonths(date);
            var data = (from c in countClients
                        group c by c.TransactionDate into g
                        select new SumClientByDateDTO
                        {
                            Fecha = g.Key,
                            ConteoClientes = g.Count()
                        }).OrderBy(s => s.Fecha).ToList();
            var dataReport = new DataReportCountClientsDTO();
            dataReport.Fechas = new List<DateTime>();
            dataReport.CountClients = new List<int>();
            dataReport.Fechas = data.Select(s => s.Fecha).ToList();
            dataReport.CountClients = data.Select(s => s.ConteoClientes).ToList();

            return dataReport;
        }

        private IEnumerable<BaseClientDto> GetDataClientByDateSixMonths(DateTime date)
        {
            return _context.Clients.Where(s => s.TransactionDate >= date)
            .Select(s => new BaseClientDto
            {
                Id = s.Id,
                TransactionDate = new DateTime(s.TransactionDate.Year, s.TransactionDate.Month, s.TransactionDate.Day)
            }).ToList();
        }

        public DataReportCountClientsDTO GetReportCountClientThreeMonths()
        {
            var date = DateTime.Now.AddMonths(-3);
            var countClients = GetDataClientByDateThreeMonths(date);
            var data = (from c in countClients
                        group c by c.TransactionDate into g
                        select new SumClientByDateDTO
                        {
                            Fecha = g.Key,
                            ConteoClientes = g.Count()
                        }).OrderBy(s => s.Fecha).ToList();
            var dataReport = new DataReportCountClientsDTO();
            dataReport.Fechas = new List<DateTime>();
            dataReport.CountClients = new List<int>();
            dataReport.Fechas = data.Select(s => s.Fecha).ToList();
            dataReport.CountClients = data.Select(s => s.ConteoClientes).ToList();

            return dataReport;
        }

        private IEnumerable<BaseClientDto> GetDataClientByDateThreeMonths(DateTime date)
        {
            return _context.Clients.Where(s => s.TransactionDate >= date)
            .Select(s => new BaseClientDto
            {
                Id = s.Id,
                TransactionDate = new DateTime(s.TransactionDate.Year, s.TransactionDate.Month, s.TransactionDate.Day)
            }).ToList();
        }

        public DataReportCountClientsDTO GetReportCountClientOneMonth()
        {
            var date = DateTime.Now.AddMonths(-1);
            var countClients = GetDataClientByDateOneMonths(date);
            var data = (from c in countClients
                        group c by c.TransactionDate into g
                        select new SumClientByDateDTO
                        {
                            Fecha = g.Key,
                            ConteoClientes = g.Count()
                        }).OrderBy(s => s.Fecha).ToList();
            var dataReport = new DataReportCountClientsDTO();
            dataReport.Fechas = new List<DateTime>();
            dataReport.CountClients = new List<int>();
            dataReport.Fechas = data.Select(s => s.Fecha).ToList();
            dataReport.CountClients = data.Select(s => s.ConteoClientes).ToList();

            return dataReport;
        }

        private IEnumerable<BaseClientDto> GetDataClientByDateOneMonths(DateTime date)
        {
            return _context.Clients.Where(s => s.TransactionDate >= date)
            .Select(s => new BaseClientDto
            {
                Id = s.Id,
                TransactionDate = new DateTime(s.TransactionDate.Year, s.TransactionDate.Month, s.TransactionDate.Day)
            }).ToList();
        }

        public async Task<DataReportClientLevelsCountDto> GetDataClientLevelBySalesAdvisorId(ClientRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.User)) return new DataReportClientLevelsCountDto();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new DataReportClientLevelsCountDto();
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new DataReportClientLevelsCountDto();



            var levels = await _context.ClientLevels.ToListAsync();

            if (request.IsAll)
            {
                var clientsAll = await _context.Clients.Where(s => s.IsActive)
                   .Include(c => c.SalesAdvisor)
                   .Include(c => c.ChildrenClients)
                   .Include(c => c.ClientLevel)
                   .ToListAsync();

                var clientsAllList = ClientDto.From(clientsAll);

                var dataAll = (from c in clientsAllList
                               group c by new { c.Principal.ClientCategory } into g
                               select new SumClientsByLevelDto
                               {
                                   Level = g.Key.ClientCategory ?? "SinDefinir",
                                   SumClients = g.Count()
                               }
                        ).OrderBy(s => s.SumClients);

                var dataAllReport = new DataReportClientLevelsCountDto();
                dataAllReport.Count = new List<int>();
                dataAllReport.Levels = new List<string>();
                dataAllReport.Levels = dataAll.Select(s => s.Level).ToList();
                dataAllReport.Count = dataAll.Select(s => s.SumClients).ToList();

                return dataAllReport;
            }

            var clients = await _context.Clients.Where(s => s.SalesAdvisorId == usuario.Id && s.IsActive)
                    .Include(c => c.SalesAdvisor)
                    .Include(c => c.ChildrenClients)
                    .Include(c => c.ClientLevel)
                    .ToListAsync();

            var clientsList = ClientDto.From(clients);

            var data = (from c in clientsList
                        group c by new { c.Principal.ClientCategory } into g
                        select new SumClientsByLevelDto
                        {
                            Level = g.Key.ClientCategory ?? "SinDefinir",
                            SumClients = g.Count()
                        }
                    ).OrderBy(s => s.SumClients);

            var dataReport = new DataReportClientLevelsCountDto();
            dataReport.Count = new List<int>();
            dataReport.Levels = new List<string>();
            dataReport.Levels = data.Select(s => s.Level).ToList();
            dataReport.Count = data.Select(s => s.SumClients).ToList();

            return dataReport;
        }

        public async Task<ClientLevelCountByProject> GetDataClientLevelProjectcBySalesAdvisorId(ClientRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.User)) return new ClientLevelCountByProject();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new ClientLevelCountByProject();
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new ClientLevelCountByProject();

            var porjects = await _context.Projects.OrderBy(s => s.Name).ToListAsync();
            var levels = await _context.ClientLevels.OrderBy(s => s.Level).ToListAsync();

            var colors = new List<ColorIdDto>
            {
                new ColorIdDto
                {
                    Color = "green",
                    Id = 1
                },
                new ColorIdDto
                {
                    Color = "lightblue",
                    Id = 2
                },
                new ColorIdDto
                {
                    Color = "yellow",
                    Id = 3
                },
            };

            if (request.IsAll)
            {
                var dataReportAll = new ClientLevelCountByProject();
                dataReportAll.Labels = new List<string>();
                dataReportAll.Datasets = new List<DataCientLevel>();
                dataReportAll.Labels = porjects.Select(s => s.Name).ToList();

                var colorContadorTotal = 1;
                foreach (var item in levels)
                {
                    var clients = await _context.Clients.Where(s => s.IsActive && s.ClientCategoryId == item.Id)
                            .Include(c => c.SalesAdvisor)
                            .Include(c => c.ChildrenClients)
                            .Include(c => c.ClientLevel)
                            .ToListAsync();

                    var clientsList = ClientDto.From(clients);

                    var data = (from c in clientsList
                                group c by new { c.Principal.Project } into g
                                select new SumClientsByLevelDto
                                {
                                    Project = g.Key.Project ?? "Sin Definir",
                                    SumClients = g.Count()
                                }
                        ).OrderBy(s => s.Project);

                    dataReportAll.Datasets.Add(new DataCientLevel
                    {
                        Label = item.Level,
                        Data = data.Select(s => s.SumClients).ToList(),
                        BackgroundColor = colors.FirstOrDefault(s => s.Id == colorContadorTotal).Color ?? "gray"
                    });

                    colorContadorTotal += 1;
                }

                return dataReportAll;
            }

            var dataReport = new ClientLevelCountByProject();
            dataReport.Labels = new List<string>();
            dataReport.Datasets = new List<DataCientLevel>();
            dataReport.Labels = porjects.Select(s => s.Name).ToList();

            var colorContador = 1;
            foreach (var item in levels)
            {
                var clients = await _context.Clients.Where(s => s.SalesAdvisorId == usuario.Id && s.IsActive && s.ClientCategoryId == item.Id)
                        .Include(c => c.SalesAdvisor)
                        .Include(c => c.ChildrenClients)
                        .Include(c => c.ClientLevel)
                        .ToListAsync();

                var clientsList = ClientDto.From(clients);

                var data = (from c in clientsList
                            group c by new { c.Principal.Project } into g
                            select new SumClientsByLevelDto
                            {
                                Project = g.Key.Project ?? "Sin Definir",
                                SumClients = g.Count()
                            }
                    ).OrderBy(s => s.Project);

                dataReport.Datasets.Add(new DataCientLevel
                {
                    Label = item.Level,
                    Data = data.Select(s => s.SumClients).ToList(),
                    BackgroundColor = colors.FirstOrDefault(s => s.Id == colorContador)?.Color ?? "gray"
                });

                colorContador += 1;
            }

            return dataReport;
        }

        private string GetNameLevel(int id, List<ClientLevel> levels)
        {
            var level = levels.FirstOrDefault(s => s.Id == id);

            if (level == null) return "SinDefinir";

            return level.Level ?? "SinDefinir";
        }

        public DataReportClientsByProject GetReportCountClientByProject()
        {
            var clients = GetClientsReportByProject();
            var projects = GetProjectsByReport();
            var data = (from c in clients
                        join p in projects
                        on c.ProjectId equals p.Id
                        group p by new { p.Name, p.Id } into g
                        select new SumClientsByProject
                        {
                            IdProyecto = g.Key.Id,
                            NombreProyecto = g.Key.Name,
                            ConteoClientes = g.Count()
                        }).OrderBy(s => s.ConteoClientes).ToList();
            var dataReport = new DataReportClientsByProject();
            dataReport.Projects = new List<string>();
            dataReport.CountClients = new List<int>();
            dataReport.Projects = data.Select(s => s.NombreProyecto).ToList();
            dataReport.CountClients = data.Select(s => s.ConteoClientes).ToList();

            return dataReport;
        }

        private IEnumerable<ProjectsDTO> GetProjectsByReport()
        {
            return _context.Projects
           .Select(s => new ProjectsDTO
           {
               Id = s.Id,
               Name = s.Name
           }).ToList();
        }

        private IEnumerable<BaseClientDto> GetClientsReportByProject()
        {
            return _context.Clients
            .Select(s => new BaseClientDto
            {
                Id = s.Id,
                TransactionDate = new DateTime(s.TransactionDate.Year, s.TransactionDate.Month, s.TransactionDate.Day),
                ProjectId = s.ProjectId
            }).ToList();
        }

        public DataReportClientsByProject GetReportClientsByContact()
        {
            var clients = GetClientsReportByContactType();
            var contactTypes = GetContactTypesReport();
            var data = (from c in clients
                        join ct in contactTypes
                        on c.ContactTypeId equals ct.Id
                        group ct by new { ct.Name, ct.Id } into g
                        select new SumClientsByProject
                        {
                            IdProyecto = g.Key.Id,
                            NombreProyecto = g.Key.Name,
                            ConteoClientes = g.Count()
                        }).OrderBy(s => s.ConteoClientes).ToList();
            var dataReport = new DataReportClientsByProject();
            dataReport.Projects = new List<string>();
            dataReport.CountClients = new List<int>();
            dataReport.Projects = data.Select(s => s.NombreProyecto).ToList();
            dataReport.CountClients = data.Select(s => s.ConteoClientes).ToList();

            return dataReport;
        }

        private IEnumerable<BaseClientDto> GetClientsReportByContactType()
        {
            return _context.Clients
            .Select(s => new BaseClientDto
            {
                Id = s.Id,
                TransactionDate = new DateTime(s.TransactionDate.Year, s.TransactionDate.Month, s.TransactionDate.Day),
                ContactTypeId = s.ContactTypeId
            }).ToList();
        }

        private IEnumerable<ContactTypesDTO> GetContactTypesReport()
        {
            return _context.ContactTypes
           .Select(s => new ContactTypesDTO
           {
               Id = s.Id,
               Name = s.Name
           }).ToList();
        }

        public IEnumerable<ClientDto> GetLastClients()
        {
            IEnumerable<Client> clients = _context.Clients
                        .OrderByDescending(s => s.CreationDate)
                        .Take(5).ToList();

            return ClientDto.From(clients);
        }

        public ResumenClientesPorCategoriaEncabezado GetDataStadisticsClientCategories(ResumenClientesPorCategoriaRequest request)
        {
             if (string.IsNullOrEmpty(request.User)) return new ResumenClientesPorCategoriaEncabezado();
            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return new ResumenClientesPorCategoriaEncabezado();
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new ResumenClientesPorCategoriaEncabezado();
            
            var cantidadMeses = request.CantidadMeses == 0 ? 1 : request.CantidadMeses;
            

            var fechaActual = DateTime.Now;
            var fechaConsulta = fechaActual.AddMonths(-cantidadMeses);
            var month = DateTime.Now.Month;
            // var dateLastMonth = new DateTime(DateTime.Now.Year, month - cantidadMeses, 1);
            // var lastDayMonth = dateLastMonth.AddMonths(1).AddDays(-1);

            var fechaConsultas = new DateTime(fechaConsulta.Year, fechaConsulta.Month, 1);

            var levels =  _context.ClientLevels.OrderBy(s => s.Level).ToList();

            var colors = new List<ColorIdDto>
            {
                new ColorIdDto
                {
                    Color = "green",
                    Id = 1
                },
                new ColorIdDto
                {
                    Color = "lightblue",
                    Id = 2
                },
                new ColorIdDto
                {
                    Color = "yellow",
                    Id = 3
                },
            };

            if(usuarioRoles.RolId != 3)
            {
                var clients = _context.Clients
                    .Where(s => s.TransactionDate >= fechaConsultas || s.OperationDate >= fechaConsultas)
                    .Include(c => c.SalesAdvisor)
                    .Include(c => c.ClientLevel).ToList();
                
                var totalClientes = clients.Count();
                var agrupaciones = (
                    from client in clients
                    group client by new { 
                            client.ClientCategoryId, 
                            client.ClientLevel, 
                            client.SalesAdvisorId, 
                            client.SalesAdvisor } 
                            into g
                    select new ResumenClientesPorCategoria
                    {
                            Agente = g.Key.SalesAdvisor?.UserId,
                            Calisificacion = g.Key.ClientLevel?.Description,
                            Total = g.Count()
                    }
                ).ToList();

                var response = new ResumenClientesPorCategoriaEncabezado();
                response.Total = totalClientes;
                response.Clasificaiones = new List<ResumenClientesPorCategoria>();
                response.Clasificaiones = agrupaciones.OrderBy(s => s.Agente).ToList();

                var agrupacionesStadistics = (
                            from client in clients
                            group client by new { 
                                    client.ClientCategoryId, 
                                    client.ClientLevel } 
                                    into g
                           select new SumClientsByLevelDto
                                    {
                                        Level = g.Key.ClientLevel?.Description,
                                        SumClients = g.Count()
                                    }
                                ).OrderBy(s => s.SumClients).ToList();
                        

                        var dataReport = new DataReportClientLevelsCountDto();
                        dataReport.Count = new List<int>();
                        dataReport.Levels = new List<string>();
                        dataReport.Levels = agrupacionesStadistics.Select(s => s.Level).ToList();
                        dataReport.Count = agrupacionesStadistics.Select(s => s.SumClients).ToList();

                        response.Estadisticas = new DataReportClientLevelsCountDto();
                        response.Estadisticas = dataReport;

                return response;
            }
            else
            {
                 var clients = _context.Clients
                    .Where(s => (s.TransactionDate >= fechaConsultas || s.OperationDate >= fechaConsultas) 
                                    && s.SalesAdvisorId == usuario.Id)
                    .Include(c => c.SalesAdvisor)
                    .Include(c => c.ClientLevel).ToList();
                
                var totalClientes = clients.Count();
                var agrupaciones = (
                    from client in clients
                    group client by new { 
                            client.ClientCategoryId, 
                            client.ClientLevel, 
                            client.SalesAdvisorId, 
                            client.SalesAdvisor } 
                            into g
                    select new ResumenClientesPorCategoria
                    {
                            Agente = g.Key.SalesAdvisor?.UserId,
                            Calisificacion = g.Key.ClientLevel?.Description,
                            Total = g.Count()
                    }
                ).ToList();

                var response = new ResumenClientesPorCategoriaEncabezado();
                response.Total = totalClientes;
                response.Clasificaiones = new List<ResumenClientesPorCategoria>();
                response.Clasificaiones = agrupaciones.OrderBy(s => s.Agente).ToList();


                    var agrupacionesStadistics = (
                            from client in clients
                            group client by new { 
                                    client.ClientCategoryId, 
                                    client.ClientLevel } 
                                    into g
                           select new SumClientsByLevelDto
                                    {
                                        Level = g.Key.ClientLevel?.Description,
                                        SumClients = g.Count()
                                    }
                                ).OrderBy(s => s.SumClients).ToList();
                        

                        var dataReport = new DataReportClientLevelsCountDto();
                        dataReport.Count = new List<int>();
                        dataReport.Levels = new List<string>();
                        dataReport.Levels = agrupacionesStadistics.Select(s => s.Level).ToList();
                        dataReport.Count = agrupacionesStadistics.Select(s => s.SumClients).ToList();

                        response.Estadisticas = new DataReportClientLevelsCountDto();
                        response.Estadisticas = dataReport;

                return response;
            }

        }
    }
}

