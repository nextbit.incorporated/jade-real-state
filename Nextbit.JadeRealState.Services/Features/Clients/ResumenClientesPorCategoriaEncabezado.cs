using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Features.Reports.Dashboard;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public class ResumenClientesPorCategoriaEncabezado
    {
        public IEnumerable<ResumenClientesPorCategoria> Clasificaiones { get; set; }
        public DataReportClientLevelsCountDto Estadisticas { get; set; }
        public int Total { get; set; }
    }
}