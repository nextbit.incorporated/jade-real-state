using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nextbit.JadeRealState.Services.DataContexts;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Nextbit.JadeRealState.Services.Features.Prospects;
using Nextbit.JadeRealState.Services.Features.Users;
using Nextbit.JadeRealState.Services.Features.Reports.Dashboard;
using Microsoft.EntityFrameworkCore;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientsController : ControllerBase
    {
        private readonly ClientsAppService _clientsAppService;
        private readonly RealStateContext _context;

        public ClientsController(ClientsAppService clientsAppService, RealStateContext context)
        {
            _clientsAppService = clientsAppService ?? throw new ArgumentNullException(nameof(clientsAppService));
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<ClientPagedDto> GetPaged([FromQuery] ProspectPagedRequest request)
        {
            return Ok(_clientsAppService.GetPaged(request));
        }

        [HttpGet]
        [Route("get-statics-client")]
        [AllowAnonymous]
        public ActionResult<ClientPagedDto> GetCountClients()
        {
            return Ok(_clientsAppService.GetCountClients());
        }


        // GET api/values
        [HttpGet]
        public async Task<ActionResult<List<ClientDto>>> Get([FromQuery] ClientRequest request)
        {
            List<ClientDto> clients = await _clientsAppService.GetClientsAsync(request);
            return Ok(clients);
        }

        [HttpGet]
        [Route("top-clients")]
        [Authorize]
        public ActionResult<List<ClientDto>> GetTopClients()
        {
            return Ok(_clientsAppService.GetLastClients());
        }

        // GET api/values
        [HttpGet]
        [Route("report")]
        [Authorize]
        public async Task<ActionResult<List<ClientDto>>> GetClientsReport([FromQuery] ClientRequest request)
        {
            List<ClientDto> clients = await _clientsAppService.GetClientsReportAsync(request);

            return Ok(clients);
        }

        [HttpGet]
        [Route("report-by-user")]
        [Authorize]
        public async Task<ActionResult<List<ClientDto>>> GetClientsReportByUser([FromQuery] ClientRequest request)
        {
            List<ClientDto> clients = await _clientsAppService.GetClientsReportByUserAsync(request);

            return Ok(clients);
        }

        [HttpGet]
        [Route("users-report")]
        [Authorize]
        public async Task<ActionResult<List<UserDTO>>> GetClientsUsersReport([FromQuery] ClientRequest request)
        {
            List<UserDTO> users = await _clientsAppService.GetUsersReport(request);

            return Ok(users);
        }

        [HttpGet]
        [Route("filters")]
        [Authorize]
        public ActionResult<List<filterDTO>> GetFilters([FromQuery] ClientRequest request)
        {
            List<filterDTO> users = _clientsAppService.GetFilters(request);

            return Ok(users);
        }


        [HttpGet("{id}")]
        public ActionResult<Client> Get(int id)
        {
            return _context.Clients.FirstOrDefault(s => s.Id == id);
        }


        [HttpPost]
        [Route("validate-create-new-client")]
        [Authorize]
        public ActionResult<ClientDto> TryValidateNewClient([FromBody] ClientDto newClient)
        {
            return _clientsAppService.ValidateNewClientCreation(newClient);
        }

        [HttpPost]
        [Route("")]
        public async Task<ActionResult<ClientDto>> Post([FromBody] ClientDto newClient)
        {
            ClientDto clientDto = await _clientsAppService.CreateClientAsync(newClient);

            return Ok(clientDto);
        }

        [HttpPut]
        [Route("")]
        public async Task<ActionResult<ClientDto>> Put([FromBody] ClientDto updatedClient)
        {
            ClientDto clientDto = await _clientsAppService.UpdateClientAsync(updatedClient);

            if (clientDto != null)
            {
                return Ok(clientDto);
            }
            return NotFound();
        }

        [HttpDelete]
        [Route("")]
        public async Task<ActionResult> Delete([FromQuery] int id, string user, bool isActive)
        {
            Client persistedClient = await _context

            .Clients
            .Include(c => c.Project)
            .Include(c => c.ContactType)
            .Include(c => c.FinancialInstitution)
            .Include(c => c.SalesAdvisor)
            .Include(c => c.ChildrenClients)
            .Include(c => c.ClientLevel)
            .Include(c => c.Nationality)
            .FirstOrDefaultAsync(c => c.Id == id);

            if (persistedClient != null)
            {
                persistedClient.SetIsActive(isActive);

                ClientTransaction clientTransaction = new ClientTransaction(persistedClient, "Deleted", user);

                _context.ClientTransactions.Add(clientTransaction);


                await _context.SaveChangesAsync();

                return NoContent();
            }

            return NotFound();
        }


        [HttpGet]
        [Route("count-client-by-date")]
        //  [AllowAnonymous]
        [Authorize]
        public ActionResult<DataReportCountClientsDTO> GetReportClientsRegisterByDate()
        {
            var result = _clientsAppService.GetReportCountClient();

            return Ok(result);
        }

        [HttpGet]
        [Route("count-client-by-date-six")]
        // [AllowAnonymous]
        [Authorize]
        public ActionResult<DataReportCountClientsDTO> GetReportClientsRegisterByDateSixMonths()
        {
            var result = _clientsAppService.GetReportCountClientSixMonths();

            return Ok(result);
        }

        [HttpGet]
        [Route("count-client-by-date-three")]
        // [AllowAnonymous]
        [Authorize]
        public ActionResult<DataReportCountClientsDTO> GetReportClientsRegisterByDateThreeMonths()
        {
            var result = _clientsAppService.GetReportCountClientThreeMonths();

            return Ok(result);
        }

        [HttpGet]
        [Route("count-client-by-date-one")]
        // [AllowAnonymous]
        [Authorize]
        public ActionResult<DataReportCountClientsDTO> GetReportClientsRegisterByDateOneMonth()
        {
            var result = _clientsAppService.GetReportCountClientOneMonth();

            return Ok(result);
        }

        [HttpGet]
        [Route("clients-by-level")]
         [AllowAnonymous]
        //[Authorize]
        public  async Task<ActionResult<DataReportCountClientsDTO>> GetDataClientLevelBySalesAdvisorId([FromQuery] ClientRequest request)
        {
            var result = await _clientsAppService.GetDataClientLevelBySalesAdvisorId(request);

            return Ok(result);
        }

        [HttpGet]
        [Route("project-clients-by-level")]
         [AllowAnonymous]
        //[Authorize]
        public  async Task<ActionResult<DataReportCountClientsDTO>> GetDataClientLevelProjectcBySalesAdvisorId([FromQuery] ClientRequest request)
        {
            var result = await _clientsAppService.GetDataClientLevelProjectcBySalesAdvisorId(request);

            return Ok(result);
        }

        [HttpGet]
        [Route("count-client-by-project")]
        //[AllowAnonymous]
        [Authorize]
        public ActionResult<DataReportClientsByProject> GetReportClientsByProject()
        {
            var result = _clientsAppService.GetReportCountClientByProject();

            return Ok(result);
        }

        [HttpGet]
        [Route("count-client-by-contact-type")]
        //[AllowAnonymous]
        [Authorize]
        public ActionResult<DataReportClientsByProject> GetReportClientsByContact()
        {
            var result = _clientsAppService.GetReportClientsByContact();

            return Ok(result);
        }


        [HttpGet]
        [Route("client-categories-stadistics")]
        [AllowAnonymous]
        //[Authorize]
        public ActionResult<DataReportClientsByProject> GetDataStadisticsClientCategories([FromQuery] ResumenClientesPorCategoriaRequest request)
        {
            var result = _clientsAppService.GetDataStadisticsClientCategories(request);

            return Ok(result);
        }


    }
}