namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public class ColorIdDto
    {
        public int Id { get; set; }
        
        public string Color { get; set; }
        
        
    }
}