namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public class SumClientsByLevelDto
    {
        public int SumClients { get; set; }
        public string Level { get; set; }
        public int IdLevel { get; set; }
        public string Project { get; set; }
        
    }
}