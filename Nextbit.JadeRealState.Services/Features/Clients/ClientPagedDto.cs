using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public class ClientPagedDto
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<ClientDto> Clients { get; set; }
    }
}