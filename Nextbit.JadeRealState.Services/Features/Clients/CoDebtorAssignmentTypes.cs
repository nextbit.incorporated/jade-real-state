namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public static class CoDebtorAssignmentTypes
    {
        public const string CreateNewCoDebtor = "create-new-codebtor";
        public const string AssignCoDebtor = "assign-codebtor";
        public const string RemoveCoDebtor = "remove-codebtor";
    }
}