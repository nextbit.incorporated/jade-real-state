using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public class ResumenClientesPorCategoria : ResponseBase
    {
        public string Calisificacion { get; set; }
        public int Total { get; set; }
        public string Agente { get; set; }
    }
}