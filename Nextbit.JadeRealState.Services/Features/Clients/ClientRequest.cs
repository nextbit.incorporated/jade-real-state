using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public class ClientRequest : RequestBase
    {
        public string Query { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }
        public int clientLevelId { get; set; }
        public bool IsAll { get; set; }
        
    }
}