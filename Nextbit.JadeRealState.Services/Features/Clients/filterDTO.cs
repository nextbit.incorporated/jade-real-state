using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public class filterDTO : ResponseBase
    {
        public int FilterId { get; set; }
        public string Filter { get; set; }
    }
}