using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Clients
{
    public sealed class ClientDto : ResponseBase
    {
        public BaseClientDto Principal { get; set; }
        public BaseClientDto CoDebtor { get; set; }

        // NEW, CHANGED, EXISTING
        public string CoDebtorAssignmentType { get; set; }


        internal static List<ClientDto> From(IEnumerable<Client> clients)
        {
            IEnumerable<ClientDto> principals = (from qry in clients.Where(c => c.ParentClientId == null).OrderByDescending(c => c.Id)
                                                 select From(qry)).ToList();

            IEnumerable<ClientDto> coDebtors = (from qry in clients.Where(c => c.ParentClientId != null).OrderByDescending(c => c.Id)
                                                select From(qry)).ToList();

            return principals.Concat(coDebtors).ToList();
        }

        internal static ClientDto From(Client client)
        {
            return new ClientDto
            {
                Principal = BaseClientDto.From(client),
                CoDebtor = BaseClientDto.From(client.ChildrenClients.FirstOrDefault()),
            };
        }
    }
}