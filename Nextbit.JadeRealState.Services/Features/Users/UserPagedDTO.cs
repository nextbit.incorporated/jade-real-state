using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class UserPagedDTO
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<UserDTO> Users { get; set; }
    }
}