using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.Prospects;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    [Table("Users")]
    public class User : Entity
    {
        public string UserCode { get; private set; }
        public string UserId { get; private set; }
        public string FirstName { get; private set; }
        public string SecondName { get; private set; }
        public string LastName { get; private set; }
        public string PasswordHash { get; private set; }
        public string Password { get; private set; }
        public string email { get; private set; }
        public string PhoneNumber { get; private set; }

        public bool Status { get; set; }

        public ICollection<Negotiation> Negotiations { get; set; }
        public ICollection<Client> Clients { get; set; }
        public ICollection<Prospect> Prospects { get; set; }

        public string GetName()
        {
            return $"{FirstName} {LastName}";
        }

        public void ChangePassword(
                     string newPassword)
        {
            Password = newPassword;
            PasswordHash = newPassword;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ChangePassword";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
            Status = true;
        }
        public void Update(
            string userCode,
             string firstName,
             string secondName,
             string lastName,
             string _email,
             string phoneNumber)
        {
            UserCode = userCode;
            FirstName = firstName;
            SecondName = secondName;
            LastName = lastName;
            email = _email;
            PhoneNumber = phoneNumber;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedUser";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
            Status = true;
        }

        public class Builder
        {
            private readonly User _user = new User();

            public Builder WithUserId(string userid)
            {
                _user.UserId = userid;
                return this;
            }

            public Builder WithUserCode(string userCode)
            {
                _user.UserCode = userCode;
                return this;
            }

            public Builder WithFirstName(string firstName)
            {
                _user.FirstName = firstName;
                return this;
            }
            public Builder WithSecondName(string secondName)
            {
                _user.SecondName = secondName;
                return this;
            }
            public Builder WithLastName(string lastName)
            {
                _user.LastName = lastName;
                return this;
            }
            public Builder WithPasswordHash(string passwordHash)
            {
                _user.PasswordHash = passwordHash;
                return this;
            }
            public Builder WithPassword(string password)
            {
                _user.Password = password;
                return this;
            }
            public Builder WithEmail(string email)
            {
                _user.email = email;
                return this;
            }
            public Builder WithPhoneNumber(string phone)
            {
                _user.PhoneNumber = phone;
                return this;
            }
            public Builder WithAuditFields()
            {
                _user.CrudOperation = "Added";
                _user.TransactionDate = DateTime.Now;
                _user.TransactionType = "NewUser";
                _user.ModifiedBy = "Service";
                _user.TransactionUId = Guid.NewGuid();
                _user.Status = true;
                return this;
            }

            public User Build()
            {
                return _user;
            }
        }
    }
}