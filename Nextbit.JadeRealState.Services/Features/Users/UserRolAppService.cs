using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class UserRolAppService : IUserRolAppService
    {
        private RealStateContext _context;
        private readonly IUserDomainService _userDomainService;
        public UserRolAppService(
            RealStateContext context,
            IUserDomainService userDomainService)
        {
            if(context == null) throw new ArgumentNullException(nameof(context));
            if(userDomainService == null) throw new ArgumentException(nameof(userDomainService));

            _context = context;
            _userDomainService = userDomainService;
        }
        public UserRolDTO CreateNewuserRol(UserRolRequest request)
        {
            if(request == null) throw new ArgumentException(nameof(request));

            var newuserRol = _userDomainService.CreateNewUserRol(request);
            _context.UserRol.Add(newuserRol);
            _context.SaveChanges();

            return new UserRolDTO
            {
                Id = newuserRol.Id,
                UserId = newuserRol.UserId,
                RolId  = newuserRol.RolId
            };
        }

        public IEnumerable<UserRolDTO> GetAllUserRolByName(string name)
        {
            var userRoles = _context.UserRol.Where(s => s.UserId.Contains(name));

            return userRoles.Select(s => new UserRolDTO
            {
                Id = s.Id,
                UserId = s.UserId,
                RolId = s.RolId
            });
        }

        public IEnumerable<UserRolDTO> GetAllUserRoles()
        {
            var userRoles = _context.UserRol.ToList();
            return userRoles.Select(s => new UserRolDTO
            {
                Id = s.Id,
                UserId = s.UserId,
                RolId = s.RolId
            });
        }

        public UserRolDTO GetUserRolById(string id)
        {
            var userRoles = _context.UserRol.Find(id);
            return  new UserRolDTO
            {
                Id = userRoles.Id,
                UserId = userRoles.UserId,
                RolId = userRoles.RolId
            };
        }

        public UserRolDTO UpdateUserRol(UserRolRequest request)
        {
            throw new System.NotImplementedException();
        }

        public void Dispose()
        {
            if(_userDomainService != null) _userDomainService.Dispose();
            if(_context != null) _context.Dispose();
        }
    }
}