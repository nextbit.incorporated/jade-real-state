using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public interface IRoleAppService : IDisposable
    {
        IEnumerable<RoleDTO> GetAllRoles();

        RoleDTO GetAllRolesById(string id);

        IEnumerable<RoleDTO> GetAllRolesByName(string id);

        RoleDTO CreateNewRol(RolRequest request);

        RoleDTO UpdateRol(RolRequest request);
    }
}