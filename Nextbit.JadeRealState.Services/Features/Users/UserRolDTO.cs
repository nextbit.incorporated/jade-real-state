using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class UserRolDTO : ResponseBase
    {
        public string UserId { get; set; }
        public int RolId { get; set; }
    }
}