using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    [Table("UserRoles")]
    public class UserRol : Entity
    {
        public string UserId { get; private set; }
        public int RolId { get; private set; }
        public bool Status { get; set; }
        public void update(string userId, int rolId)
        {
            UserId = userId;
            RolId = rolId;
            Status = true;
        }

        public class Builder
        {
            private readonly UserRol _userRol = new UserRol();

            public Builder WithUserId(string userId)
            {
                _userRol.UserId = userId;
                return this;
            }
            public Builder WithRolId(int rolId)
            {
                _userRol.RolId = rolId;
                return this;
            }

            public Builder WithAuditFields()
            {
                _userRol.CrudOperation = "Added";
                _userRol.TransactionDate = DateTime.Now;
                _userRol.TransactionType = "NewRolUser";
                _userRol.ModifiedBy = "Service";
                _userRol.TransactionUId = Guid.NewGuid();
                _userRol.Status = true;
                return this;
            }

            public UserRol Build()
            {
                return _userRol;
            }
        }
    }
}