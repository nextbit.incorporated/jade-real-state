using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    [Table("Roles")]
    public class Rol : Entity
    {
     public string Name { get; private set; }
     public string Description { get; private set; }

     public bool Status { get; set; }

        public void Update(string _name, string _description)
        {
            Name = _name;
            Description = _description;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedRol";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
            Status = true;
        }

     public class Builder
     {
        private readonly Rol _rol = new Rol();

        public Builder WithName(string name)
        {
            _rol.Name = name;
            return this;
        }

        public Builder WithDescription(string description)
        {
            _rol.Description = description;
            return this;
        }

            public Builder WithAuditFields()
            {
                _rol.CrudOperation = "Added";
                _rol.TransactionDate = DateTime.Now;
                _rol.TransactionType = "NewRol";
                _rol.ModifiedBy = "Service";
                _rol.TransactionUId = Guid.NewGuid();
                _rol.Status = true;
                return this;
            }
            public Rol Build()
            {
                return _rol;
            }
     }
    }
}