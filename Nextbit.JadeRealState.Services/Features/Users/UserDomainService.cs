using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class UserDomainService : IUserDomainService
    {
        private EncriptorHelper _encriptorHelper = new EncriptorHelper();
        public User CreateNewUser(UserRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.UserId)) throw new ArgumentException(nameof(request.UserId));
            if (string.IsNullOrEmpty(request.UserCode)) throw new ArgumentException(nameof(request.UserCode));
            if (string.IsNullOrEmpty(request.FirstName)) throw new ArgumentException(nameof(request.FirstName));
            if (string.IsNullOrEmpty(request.FirstName)) throw new ArgumentException(nameof(request.FirstName));
            if (String.IsNullOrEmpty(request.Password)) throw new ArgumentException(nameof(request.Password));

            return new User.Builder()
            .WithUserId(request.UserId)
            .WithUserCode(request.UserCode)
            .WithFirstName(request.FirstName)
            .WithSecondName(request.SecondName ?? string.Empty)
            .WithLastName(request.LastName ?? string.Empty)
            .WithPhoneNumber(request.PhoneNumber ?? string.Empty)
            .WithEmail(request.email ?? string.Empty)
            .WithPasswordHash(_encriptorHelper.GetPasswordHash(request.Password))
            .WithPassword("")
            .WithAuditFields()
            .Build();
        }


        public User ChangePassword(string newPassword, User userInfoOld)
        {
            if (userInfoOld == null) throw new ArgumentException(nameof(userInfoOld));

            userInfoOld.ChangePassword(_encriptorHelper.GetPasswordHash(newPassword));

            return userInfoOld;
        }

        public User UpdateUser(UserRequest request, User userInfoOld)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (userInfoOld == null) throw new ArgumentException(nameof(userInfoOld));

            userInfoOld.Update(request.UserCode, request.FirstName, request.SecondName, request.LastName, request.email, request.PhoneNumber);

            return userInfoOld;
        }

        public Rol CreateNewRol(RolRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));

            return new Rol.Builder()
            .WithName(request.Name)
            .WithDescription(request.Description)
            .WithAuditFields()
            .Build();
        }

        public Rol UpdateRol(RolRequest request, Rol OldRol)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (OldRol == null) throw new ArgumentException(nameof(OldRol));

            OldRol.Update(request.Name, request.Description);

            return OldRol;
        }

        public RolePermission CreateNewRolAccess(RolAccessRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));

            return new RolePermission.Builder()
            .WithRolId(request.RolId)
            .WithName(request.Name)
            .WithDescription(request.Description)
            .WithPermissionOption(request.PermissionOption)
            .WithAuditFields().Build();
        }

        public RolePermission UpdateRolAccess(RolAccessRequest request, RolePermission oldRolAccess)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (oldRolAccess == null) throw new ArgumentException(nameof(oldRolAccess));

            oldRolAccess.update(request.RolId, request.Name, request.PermissionOption, request.Description);

            return oldRolAccess;
        }
        public UserRol CreateNewUserRol(UserRolRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));

            return new UserRol.Builder()
            .WithRolId(request.RolId)
            .WithUserId(request.UserId)
            .WithAuditFields()
            .Build();
        }

        public UserRol UpdateUserRol(UserRolRequest request, UserRol oldUserRol)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (oldUserRol == null) throw new ArgumentException(nameof(oldUserRol));

            oldUserRol.update(request.UserId, request.RolId);

            return oldUserRol;
        }
        public void Dispose()
        {
        }

    }
}