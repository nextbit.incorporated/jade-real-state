using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class UserRequest : RequestBase
    {
        public string UserId { get; set; }
        public string UserCode { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string email { get; set; }
        public string PhoneNumber { get; set; }
        public int RolId { get; set; }
    }

    public class UserPagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
    }
}