using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public interface IUserAppService : IDisposable
    {
        UserPagedDTO GetPagedUser(UserPagedRequest request);
        Task<List<User>> GetUsersAsync();
        IEnumerable<User> GetAllUsersById(string id);
        UserRol GetUserRolById(string userId);
        Rol GetRolById(int rolId);
        User CreateUser(UserRequest request);
        UserDTO UpdateUser(UserRequest request);
        string DeteleUser(string userId);
        UserDTO GetInfoUser(UserRequest request);
        string ChangeUserPassword(PasswordResetRequest request);

        string ChangeUserPasswordMaster(PasswordResetRequest request);
    }
}