using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public interface IUserRolAppService : IDisposable
    {
        IEnumerable<UserRolDTO> GetAllUserRoles();
        IEnumerable<UserRolDTO> GetAllUserRolByName(string name);
        UserRolDTO GetUserRolById(string id);
        UserRolDTO CreateNewuserRol(UserRolRequest request);
        UserRolDTO UpdateUserRol(UserRolRequest request);
    }
}