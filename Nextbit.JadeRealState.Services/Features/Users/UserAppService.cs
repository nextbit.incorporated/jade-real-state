using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class UserAppService : IUserAppService
    {
        private RealStateContext _context;
        private readonly IUserDomainService _userDomainService;

        public UserAppService(RealStateContext context,
                             IUserDomainService userDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (userDomainService == null) throw new ArgumentException(nameof(userDomainService));

            _context = context;
            _userDomainService = userDomainService;
        }

        public async Task<List<User>> GetUsersAsync()
        {
            return await _context.Users.ToListAsync();
        }

        public IEnumerable<User> GetAllUsersById(string id)
        {
            return _context.Users.Where(s => s.UserId.Contains(id));
        }

        public UserRol GetUserRolById(string userId)
        {
            return _context.UserRol.FirstOrDefault(s => s.UserId == userId);
        }

        public Rol GetRolById(int rolId)
        {
            return _context.Roles.FirstOrDefault(s => s.Id == rolId);
        }

        public UserDTO GetInfoUser(UserRequest request)
        {
            var user = _context.Users.FirstOrDefault(s => s.UserId == request.UserId);
            if (user == null)
            {
                return new UserDTO();
            }

            var userRol = _context.UserRol.FirstOrDefault(s => s.UserId == request.UserId);
            if (userRol == null)
            {
                return new UserDTO();
            }

            var rol = _context.Roles.FirstOrDefault(s => s.Id == userRol.RolId);
            if (rol == null)
            {
                return new UserDTO();
            }

            var infoUsuario = new UserDTO
            {
                RolId = userRol.RolId,
                Rol = rol.Name
            };

            return infoUsuario;
        }
        public User CreateUser(UserRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.UserId)) throw new ArgumentException(nameof(request.UserId));
            if (string.IsNullOrEmpty(request.FirstName)) throw new ArgumentException(nameof(request.FirstName));
            if (string.IsNullOrEmpty(request.Password)) throw new ArgumentException(nameof(request.Password));
            if (request.RolId == 0) return new User();
            var userExists = _context.Users.FirstOrDefault(s => s.UserId == request.UserId);
            if (userExists != null) return new User();
            var rol = _context.Roles.FirstOrDefault(s => s.Id == request.RolId);
            if (rol == null) return new User();

            var newUser = _userDomainService.CreateNewUser(request);
            var newUserRol = _userDomainService.CreateNewUserRol(
                new UserRolRequest
                {
                    UserId = newUser.UserId,
                    RolId = request.RolId
                });
            _context.Users.Add(newUser);
            _context.UserRol.Add(newUserRol);
            _context.SaveChanges();
            return newUser;
        }

        public UserDTO UpdateUser(UserRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (string.IsNullOrEmpty(request.UserId)) throw new ArgumentException(nameof(request.UserId));

            var userInfoOld = _context.Users.FirstOrDefault(s => s.UserId == request.UserId);
            if (userInfoOld == null) return new UserDTO { ValidationErrorMessage = "Usuario a actualizar no encontrado" };

            var persistedRol = _context.Roles.FirstOrDefault(s => s.Id == request.RolId);
            if (persistedRol == null) return new UserDTO { ValidationErrorMessage = "Rol no encontrado" };

            var rolUserOld = _context.UserRol.FirstOrDefault(s => s.UserId == request.UserId);

            var newInfoUser = _userDomainService.UpdateUser(request, userInfoOld);

            _context.Users.Update(newInfoUser);

            var newRolUser = new UserRol();
            if (rolUserOld == null)
            {
                newRolUser = _userDomainService.CreateNewUserRol(new UserRolRequest
                {
                    UserId = request.UserId,
                    RolId = request.RolId
                });
                _context.UserRol.Add(newRolUser);
            }
            else
            {
                newRolUser = _userDomainService.UpdateUserRol(
                    new UserRolRequest
                    {
                        UserId = request.UserId,
                        RolId = request.RolId,

                    }, rolUserOld);
                _context.UserRol.Update(newRolUser);
            }

            _context.SaveChanges();

            int rolId = 0;
            string rol = string.Empty;
            if (newRolUser != null)
            {
                var _rol = _context.Roles.FirstOrDefault(s => s.Id == newRolUser.RolId);
                if (rol != null)
                {
                    rol = _rol.Name;
                    rolId = _rol.Id;
                }
            }

            return new UserDTO
            {
                FirstName = newInfoUser.FirstName,
                SecondName = newInfoUser.SecondName,
                LastName = newInfoUser.LastName,
                email = newInfoUser.email,
                PhoneNumber = newInfoUser.PhoneNumber,
                Rol = rol,
                RolId = rolId
            };
        }

        public UserPagedDTO GetPagedUser(UserPagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            var count = Convert.ToDecimal(_context.Users.Count());
            var totalPage = Math.Ceiling(count / request.PageSize);
            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;
            if (string.IsNullOrEmpty(request.Value))
            {
                var lista = _context.Users.OrderByDescending(s => s.TransactionDate).Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                var listaCodigosRoles = lista.Select(s => s.UserId).Distinct();
                var usuarioRol = _context.UserRol.Where(s => listaCodigosRoles.Contains(s.UserId));
                var rolesId = new List<string>();
                if (usuarioRol != null)
                {
                    rolesId = usuarioRol.Select(s => s.RolId.ToString()).Distinct().ToList();
                };
                var roles = _context.Roles.Where(s => rolesId.Contains(s.Id.ToString())).Distinct().ToList();
                return new UserPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Users = lista.Select(s => new UserDTO
                    {
                        Id = s.Id,
                        UserId = s.UserId,
                        UserCode = s.UserCode,
                        FirstName = s.FirstName,
                        SecondName = s.SecondName,
                        LastName = s.LastName,
                        email = s.email,
                        PhoneNumber = s.PhoneNumber,
                        Rol = ObtenerNombreRol(s.UserId, roles),
                        RolId = ObtenerRol(s.UserId, roles)
                    }).ToList()
                };
            }
            else
            {
                var lista = _context.Users.Where(s => s.UserId.Contains(request.Value))
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                var listaCodigosRoles = lista.Select(s => s.UserId).Distinct();
                var usuarioRol = _context.UserRol.Where(s => listaCodigosRoles.Contains(s.UserId));
                var rolesId = new List<string>();
                if (usuarioRol != null)
                {
                    rolesId = usuarioRol.Select(s => s.RolId.ToString()).Distinct().ToList();
                };
                var roles = _context.Roles.Where(s => rolesId.Contains(s.Id.ToString())).Distinct().ToList();
                return new UserPagedDTO
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Users = lista.Select(s => new UserDTO
                    {
                        Id = s.Id,
                        UserId = s.UserId,
                        UserCode = s.UserCode,
                        FirstName = s.FirstName,
                        SecondName = s.SecondName,
                        LastName = s.LastName,
                        email = s.email,
                        PhoneNumber = s.PhoneNumber,
                        Rol = ObtenerNombreRol(s.UserId, roles),
                        RolId = ObtenerRol(s.UserId, roles)
                    }).ToList()
                };
            }
        }

        private int ObtenerRol(string userId, List<Rol> roles)
        {
            var usuarioRol = _context.UserRol.FirstOrDefault(s => s.UserId == userId);
            if (usuarioRol == null) return 0;

            var rol = roles.FirstOrDefault(s => s.Id == usuarioRol.RolId);
            if (roles == null) return 0;

            return rol.Id;
        }

        private string ObtenerNombreRol(string userId, List<Rol> roles)
        {
            var usuarioRol = _context.UserRol.FirstOrDefault(s => s.UserId == userId);
            if (usuarioRol == null) return string.Empty;

            var rol = roles.FirstOrDefault(s => s.Id == usuarioRol.RolId);
            if (roles == null) return string.Empty;

            return rol.Name;
        }

        public string DeteleUser(string userId)
        {
            if (string.IsNullOrEmpty(userId)) throw new ArgumentException(nameof(userId));

            var user = _context.Users.FirstOrDefault(s => s.UserId == userId);
            if (user == null) throw new Exception("User not exists");
            _context.Users.Remove(user);
            _context.SaveChanges();

            return string.Empty;
        }

        public string ChangeUserPassword(PasswordResetRequest request)
        {
            EncriptorHelper encryp = new EncriptorHelper();
            if (request == null) throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(request.Password)) throw new ArgumentNullException(nameof(request.Password));
            if (string.IsNullOrEmpty(request.NewPassword)) throw new ArgumentNullException(nameof(request.NewPassword));
            if (string.IsNullOrEmpty(request.User)) throw new ArgumentNullException(nameof(request.User));

            var user = _context.Users.FirstOrDefault(s => s.UserId == request.User);

            if (user.UserId == user.UserId && encryp.VerifiedPassword(request.Password, user.PasswordHash))
            {

                var newInfoUser = _userDomainService.ChangePassword(request.NewPassword, user);
                _context.Users.Update(newInfoUser);
                _context.SaveChanges();
                return string.Empty;
            }
            else
            {
                return "Usuario no encontrado";
            }
        }


         public string ChangeUserPasswordMaster(PasswordResetRequest request)
        {
              EncriptorHelper encryp = new EncriptorHelper();
            if (request == null) throw new ArgumentNullException(nameof(request));
            if (string.IsNullOrEmpty(request.NewPassword)) throw new ArgumentNullException(nameof(request.NewPassword));
            if (string.IsNullOrEmpty(request.User)) throw new ArgumentNullException(nameof(request.User));
            if (string.IsNullOrEmpty(request.UsuarioAGestionar)) throw new ArgumentNullException(nameof(request.UsuarioAGestionar));

            var usuario = _context.Users.FirstOrDefault(s => s.UserId == request.User);
            if (usuario == null) return string.Empty;
            var usuarioRoles = _context.UserRol.FirstOrDefault(s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return string.Empty;

            if (usuarioRoles.RolId != 3)
            {

             var user = _context.Users.FirstOrDefault(s => s.UserId == request.UsuarioAGestionar);

            if (user != null)
            {
                var newInfoUser = _userDomainService.ChangePassword(request.NewPassword, user);
                _context.Users.Update(newInfoUser);
                _context.SaveChanges();
                return string.Empty;
            }
            else
            {
                return "Usuario no encontrado";
            }

            }
            else
            {
                return "SP";
            }

        }

        public void Dispose()
        {
            if (_userDomainService != null) _userDomainService.Dispose();
            if (_context != null) _context.Dispose();
        }

       
    }
}