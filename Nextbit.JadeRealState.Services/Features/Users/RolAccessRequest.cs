using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class RolAccessRequest : RequestBase
    {
        public int RolAccessId {get;set;}
        public string RolId { get; set; }
        public string Name { get; set; }
        public string PermissionOption { get; set; }
        public string Description { get; set; }
    }
}