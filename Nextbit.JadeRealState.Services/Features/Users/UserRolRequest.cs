using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class UserRolRequest : RequestBase
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int RolId { get; set; }
    }
}