using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class PasswordResetRequest : RequestBase
    {
       public string Password { get; set; } 
       public string NewPassword { get; set; } 
       public string UsuarioAGestionar { get; set; }
    }
}