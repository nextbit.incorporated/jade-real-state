using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class RoleDTO : ResponseBase
    {
         public string Name { get; set; }
         public string Description { get; set; }
    }
}