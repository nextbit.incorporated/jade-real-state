using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly AppSettings _appSettings;

        private readonly IUserAppService _userAppService;
        private readonly IConfiguration _configuration;
        private EncriptorHelper encryp = new EncriptorHelper();
        public UserController(
            IOptions<AppSettings> appSettings,
            IUserAppService userAppService,
            IConfiguration configuration)
        {
            if (userAppService == null) throw new ArgumentNullException(nameof(userAppService));
            if (configuration == null) throw new ArgumentException(nameof(configuration));
            if (appSettings == null) throw new ArgumentException(nameof(appSettings));

            _appSettings = appSettings.Value;
            _userAppService = userAppService;
            _configuration = configuration;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("token")]
        public ActionResult<User> Token([FromBody] UserRequest request)
        {
            var user = _userAppService.GetAllUsersById(request.UserId).FirstOrDefault();
            if (user == null) return BadRequest("Error");
            if (string.IsNullOrEmpty(request.Password)) return BadRequest("Error");
            if (request.UserId == user.UserId && encryp.VerifiedPassword(request.Password, user.PasswordHash))
            {
                var userRol = _userAppService.GetUserRolById(user.UserId);
                var rol = new Rol();
                if (userRol != null)
                {
                    rol = _userAppService.GetRolById(userRol.RolId);
                }
                // var claims = new[] { new Claim("UserData", JsonConvert.SerializeObject(user)) };



                // // var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(user.PasswordHash));
                // var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["ApiAuth:SecretKey"]));
                // var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                // // Generamos el Token
                // var token = new JwtSecurityToken
                // (
                //     issuer: _configuration["ApiAuth:Issuer"],
                //     audience: _configuration["ApiAuth:Audience"],
                //     claims: claims,
                //     expires: DateTime.UtcNow.AddDays(1),
                //     notBefore: DateTime.UtcNow,
                //     signingCredentials: creds
                // );

                // authentication successful so generate jwt token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                // user.Token = tokenHandler.WriteToken(token);


                // Retornamos el token
                return Ok(
                    new TokenInfoResponse
                    {
                        Id = user.Id,
                        UserId = user.UserId,
                        RolId = userRol == null ? 0 : userRol.RolId,
                        Rol = rol == null ? string.Empty : rol.Name ?? string.Empty,
                        Token = tokenHandler.WriteToken(token)
                    }
                );
            }
            else
            {
                return BadRequest("Invalid grant username and/or password is incorrect");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("info-user")]
        public ActionResult<User> GetInfoUser([FromBody] UserRequest request)
        {
            return Ok(_userAppService.GetInfoUser(request));
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetUsersAsync()
        {
            return Ok(await _userAppService.GetUsersAsync());
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<IEnumerable<User>> GetPaged([FromQuery] UserPagedRequest request)
        {
            return Ok(_userAppService.GetPagedUser(request));
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<User>> Get(string id)
        {
            return _userAppService.GetAllUsersById(id).ToList();
        }

        [HttpPost]
        //[AllowAnonymous]
        [Authorize]
        public ActionResult<User> Post([FromBody] UserRequest request)
        {
            return Ok(_userAppService.CreateUser(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<UserDTO> Put([FromBody] UserRequest request)
        {
            return Ok(_userAppService.UpdateUser(request));
        }

        [HttpPut]
        [Authorize]
        [Route("changePassword")]
        public ActionResult<string> changePassword([FromBody] PasswordResetRequest request)
        {
            var r = string.Empty;
            return Ok(_userAppService.ChangeUserPassword(request));
        }

         [HttpPut]
        [Authorize]
        [Route("changePasswordMaster")]
        public ActionResult<string> changePasswordMaster([FromBody] PasswordResetRequest request)
        {
            var r = string.Empty;
            return Ok(_userAppService.ChangeUserPasswordMaster(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(string userId)
        {
            return Ok(_userAppService.DeteleUser(userId));
        }

        [HttpGet("verificarPassWord")]
        [AllowAnonymous]
        public ActionResult<bool> GetAll()
        {
            var r = _userAppService.GetAllUsersById("p01").ToList().FirstOrDefault();

            return encryp.VerifiedPassword("mperezd", r.PasswordHash);
        }
    }
}