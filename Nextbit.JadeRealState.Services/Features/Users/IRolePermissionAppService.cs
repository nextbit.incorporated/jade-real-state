using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public interface IRolePermissionAppService : IDisposable
    {
         IEnumerable<RolPermisionDTO> GetAllRolPermision();
        IEnumerable<RolPermisionDTO> GetAllRolPermisionByName(string name);
        RolPermisionDTO GetAllRolPermisionById(string id);
        RolPermisionDTO CreateNewRolPermision(RolAccessRequest request);
        RolPermisionDTO UpdateRolAccess(RolAccessRequest request);
    }
}