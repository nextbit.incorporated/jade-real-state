using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class UserDTO : ResponseBase
    {
        public string UserCode { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string email { get; set; }
        public string PhoneNumber { get; set; }
        public string Rol { get; set; }
        public int RolId { get; set; }

        internal static UserDTO From(User salesAdvisor)
        {
            return new UserDTO
            {
                Id = salesAdvisor.Id,
                CreationDate = salesAdvisor.CreationDate,
                UserCode = salesAdvisor.UserCode,
                UserId = salesAdvisor.UserId,
                FirstName = salesAdvisor.FirstName,
                SecondName = salesAdvisor.SecondName,
                LastName = salesAdvisor.LastName,
                email = salesAdvisor.email,
                PhoneNumber = salesAdvisor.PhoneNumber
            };
        }

        internal static List<UserDTO> From(List<User> users)
        {
            return (from user in users select From(user)).ToList();
        }
    }
}