using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UseraccessController : ControllerBase
    {
        private readonly IRoleAppService _roleAppService;
        private readonly IRolePermissionAppService _rolePermissionAppService;
        private readonly IUserRolAppService _userRolAppService;
        public UseraccessController(
            IRoleAppService roleAppService, IRolePermissionAppService rolePermissionAppService,
            IUserRolAppService userRolAppService)
        {
            if (roleAppService == null) throw new ArgumentException(nameof(roleAppService));
            if (rolePermissionAppService == null) throw new ArgumentException(nameof(rolePermissionAppService));
            if (userRolAppService == null) throw new ArgumentException(nameof(userRolAppService));

            _roleAppService = roleAppService;
            _rolePermissionAppService = rolePermissionAppService;
            _userRolAppService = userRolAppService;
        }


        [HttpGet]
        [Route("role")]
        [Authorize]
        public ActionResult<RoleDTO> GetAllRolesById([FromQuery] string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Ok(_roleAppService.GetAllRoles().ToList());
            }

            return Ok(_roleAppService.GetAllRolesById(id));
        }



        [HttpGet]
        [Route("role/by-name")]
        [Authorize]
        public ActionResult<IEnumerable<RoleDTO>> GetByName([FromQuery] string name)
        {
            return Ok(_roleAppService.GetAllRolesByName(name));
        }

        [HttpPost]
        [Route("role")]
        [Authorize]
        public ActionResult<RoleDTO> Post([FromBody] RolRequest request)
        {
            return Ok(_roleAppService.CreateNewRol(request));
        }

        [HttpPut]
        [Route("role")]
        [Authorize]
        public ActionResult<RoleDTO> Put([FromBody] RolRequest request)
        {
            return Ok(_roleAppService.UpdateRol(request));
        }




        [HttpGet]
        [Route("role-access")]
        [Authorize]
        public ActionResult<RolPermisionDTO> GetRolAccess([FromQuery] string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Ok(_rolePermissionAppService.GetAllRolPermision().ToList());
            }

            return Ok(_rolePermissionAppService.GetAllRolPermisionById(id));
        }

        [HttpGet]
        [Route("role-access/by-name")]
        [Authorize]
        public ActionResult<IEnumerable<RolPermisionDTO>> GetGetRolAccessByName([FromQuery] string name)
        {
            return Ok(_rolePermissionAppService.GetAllRolPermisionByName(name));
        }

        [HttpPost]
        [Route("role-access")]
        [Authorize]
        public ActionResult<RolPermisionDTO> PostRolAccess([FromBody] RolAccessRequest request)
        {
            return Ok(_rolePermissionAppService.CreateNewRolPermision(request));
        }

        [HttpPut]
        [Route("role-access")]
        [Authorize]
        public ActionResult<RolPermisionDTO> PutRolAccess([FromBody] RolAccessRequest request)
        {
            return Ok(_rolePermissionAppService.UpdateRolAccess(request));
        }




        [HttpGet]
        [Route("user-role")]
        [Authorize]
        public ActionResult<UserRolDTO> GetUserRol([FromQuery] string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return Ok(_userRolAppService.GetAllUserRoles().ToList());
            }

            return Ok(_userRolAppService.GetUserRolById(id));
        }

        [HttpGet]
        [Route("user-role/by-name")]
        [Authorize]
        public ActionResult<IEnumerable<UserRolDTO>> GetUserRolByName([FromQuery] string name)
        {
            return Ok(_userRolAppService.GetAllUserRolByName(name));
        }

        [HttpPost]
        [Route("user-role")]
        [Authorize]
        public ActionResult<UserRolDTO> PostUserRol([FromBody] UserRolRequest request)
        {
            return Ok(_userRolAppService.CreateNewuserRol(request));
        }

        [HttpPut]
        [Route("user-role")]
        [Authorize]
        public ActionResult<UserRolDTO> PutUserRol([FromBody] UserRolRequest request)
        {
            return Ok(_userRolAppService.UpdateUserRol(request));
        }
    }
}