using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class RolePermisionAppService : IRolePermissionAppService
    {
        private RealStateContext _context;
        private readonly IUserDomainService _userDomainService;
        public RolePermisionAppService(
            RealStateContext context,
            IUserDomainService userDomainService)
        {
            if(context == null) throw new ArgumentException(nameof(context));
            if(userDomainService == null) throw new ArgumentException(nameof(userDomainService));

            _context = context;
            _userDomainService = userDomainService;
        }
        public RolPermisionDTO CreateNewRolPermision(RolAccessRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newRolAccess = _userDomainService.CreateNewRolAccess(request);

            _context.RoleAccess.Add(newRolAccess);
            _context.SaveChanges();
            return new RolPermisionDTO
            {
                Id = newRolAccess.Id,
                RolId = newRolAccess.RolId,
                Name = newRolAccess.Name,
                Description = newRolAccess.Description,
                PermissionOption = newRolAccess.PermissionOption
            };
        }

        public IEnumerable<RolPermisionDTO> GetAllRolPermision()
        {
           var  rolAccess = _context.RoleAccess.ToList();
           return rolAccess.Select(s => new RolPermisionDTO
           {
               Id = s.Id,
               RolId = s.RolId,
               Name = s.Name,
               PermissionOption = s.PermissionOption,
               Description = s.Description   
           });
        }

        public RolPermisionDTO GetAllRolPermisionById(string id)
        {
            if(string.IsNullOrEmpty(id)) return new RolPermisionDTO();
            var rolAccess = _context.RoleAccess.Find(id);
            return new RolPermisionDTO
            {
                Id = rolAccess.Id,
                RolId = rolAccess.RolId,
                Name = rolAccess.Name,
                Description = rolAccess.Description,
                PermissionOption = rolAccess.PermissionOption
            };
        }

        public IEnumerable<RolPermisionDTO> GetAllRolPermisionByName(string name)
        {
            if(string.IsNullOrEmpty(name)) return new List<RolPermisionDTO>();
            var rolAccess = _context.RoleAccess.Where(s => s.Name.Contains(name));
            return rolAccess.Select(s => new RolPermisionDTO
            {
                Id = s.Id,
                RolId = s.RolId,
                Name = s.Name,
                PermissionOption = s.PermissionOption,
                Description = s.Description
            });
        }

        public RolPermisionDTO UpdateRolAccess(RolAccessRequest request)
        {
            if(request == null) throw new ArgumentException(nameof(request));

            var oldRolAccess = _context.RoleAccess.Find(request.RolAccessId);
            var oldAccessUpdate = _userDomainService.UpdateRolAccess(request, oldRolAccess);

            _context.RoleAccess.Update(oldAccessUpdate);
            _context.SaveChanges();

            return new RolPermisionDTO
            {
                Id = oldAccessUpdate.Id,
                RolId = oldAccessUpdate.RolId,
                Name = oldAccessUpdate.Name,
                Description = oldAccessUpdate.Description,
                PermissionOption = oldAccessUpdate.PermissionOption
            };
        }

        public void Dispose()
        {
            if(_context != null) _context.Dispose();
            if(_userDomainService != null) _userDomainService.Dispose();
        }
    }
}