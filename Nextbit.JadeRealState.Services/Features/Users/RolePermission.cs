using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    [Table("RolePermissions")]
    public class RolePermission : Entity
    {
        public string RolId { get; private set; }
        public string Name { get; private set; }
        public string PermissionOption { get; private set; }
        public string Description { get; private set; }
        public bool Status { get; set; }

        public void update(string rolId, string name, string accessName, string description)
        {
            RolId = rolId;
            Name = name;
            PermissionOption = accessName;
            Description = description;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "ModifiedRolPermission";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
            Status = true;

        }

        public class Builder
        {
          private readonly RolePermission _rolePermission = new RolePermission();

          public Builder WithRolId(string rolId)
          {
              _rolePermission.RolId = rolId;
              return this;
          }
          public Builder WithName(string name)
          {
              _rolePermission.Name = name;
              return this;
          }
          public Builder WithPermissionOption(string option)
          {
              _rolePermission.PermissionOption = option;
              return this;
          }
          public Builder WithDescription(string description)
          {
              _rolePermission.Description = description;
              return this;
          }
            public Builder WithAuditFields()
            {
                _rolePermission.CrudOperation = "Added";
                _rolePermission.TransactionDate = DateTime.Now;
                _rolePermission.TransactionType = "NewRolePermission";
                _rolePermission.ModifiedBy = "Service";
                _rolePermission.TransactionUId = Guid.NewGuid();
                _rolePermission.Status = true;
                return this;
            }
            public Builder WithAuditFieldsUpdate()
            {
                _rolePermission.CrudOperation = "Update";
                _rolePermission.TransactionDate = DateTime.Now;
                _rolePermission.TransactionType = "ModifiedRolPermission";
                _rolePermission.ModifiedBy = "Service";
                _rolePermission.TransactionUId = Guid.NewGuid();
                return this;
            }

            public RolePermission Build()
            {
                return _rolePermission;
            }
        }
    }
}