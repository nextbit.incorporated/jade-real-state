using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class TokenInfoResponse : ResponseBase
    {
        public string Token { get; set; }
        public string UserId { get; set; }
        public int RolId { get; set; }
        public string Rol { get; set; }
    }
}