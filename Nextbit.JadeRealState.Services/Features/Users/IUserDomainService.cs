using System;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public interface IUserDomainService : IDisposable
    {
         User CreateNewUser(UserRequest request);
        User UpdateUser(UserRequest request, User userInfoOld);

        Rol CreateNewRol(RolRequest request);

        Rol UpdateRol(RolRequest request, Rol OldRol);

        RolePermission CreateNewRolAccess(RolAccessRequest request);

        RolePermission UpdateRolAccess(RolAccessRequest request, RolePermission OldRolAccess);

        UserRol CreateNewUserRol(UserRolRequest request);

        UserRol UpdateUserRol(UserRolRequest request, UserRol oldUserRol);

        User ChangePassword(string newPassword, User userInfoOld);
    }
}