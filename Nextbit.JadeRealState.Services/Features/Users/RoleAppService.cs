using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Users
{
    public class RoleAppService : IRoleAppService
    {
        private RealStateContext _context;
        private readonly IUserDomainService _userDomainService;

        public RoleAppService(
            RealStateContext context,
            IUserDomainService userDomainService)
        {
            if(context == null) throw new  ArgumentNullException(nameof(context));
            if(userDomainService == null) throw new ArgumentException(nameof(userDomainService));

            _context = context;
            _userDomainService = userDomainService;
        }

        public IEnumerable<RoleDTO> GetAllRoles()
        {
            var roles = _context.Roles.ToList();

            return roles.Select(t => new RoleDTO
            {
               Id  = t.Id,
               Name = t.Name,
               Description = t.Description
            });
        }

        public RoleDTO GetAllRolesById(string id)
        {
            var rol = _context.Roles.Find(id);

            return new RoleDTO
            {
             Id = rol.Id,    
             Name = rol.Name,
             Description = rol.Description   
            };
        }

        public IEnumerable<RoleDTO> GetAllRolesByName(string id)
        {
           var roles = _context.Roles.Where(s => s.Name.Contains(id));

            return roles.Select(t => new RoleDTO
            {
                Id = t.Id,
                Name = t.Name,
                Description = t.Description
            });

        }

        public RoleDTO CreateNewRol(RolRequest request)
        {
            if(request == null) throw new ArgumentException(nameof(request));
            var newRol = _userDomainService.CreateNewRol(request);

            _context.Roles.Add(newRol);
            _context.SaveChanges();

            return new RoleDTO
            {
             Name = newRol.Name,
             Description = newRol.Description
            };
        }

        public RoleDTO UpdateRol(RolRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRol = _context.Roles.FirstOrDefault(s => s.Name == request.Name);
            if(oldRol == null) return new RoleDTO{ValidationErrorMessage = "Rol inexistente"};

            var rolUpdate = _userDomainService.UpdateRol(request, oldRol);

            _context.Roles.Update(oldRol);
            _context.SaveChanges();

            return new RoleDTO
            {
                Name = rolUpdate.Name,
                Description = rolUpdate.Description
            };
        }

        public void Dispose()
        {
            if(_context != null) _context.Dispose();
            if(_userDomainService != null) _userDomainService.Dispose();
        }
    }
}