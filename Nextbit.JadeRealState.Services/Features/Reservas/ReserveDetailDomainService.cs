using System;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public class ReserveDetailDomainService : IReserveDetailDomainService
    {
        public ReserveDetail Create(ReserveDetailRequest request)
        {
            if (request.ReserveId <= 0) throw new ArgumentNullException(nameof(request.ReserveId));

            ReserveDetail registro = new ReserveDetail.Builder()
            .WithReserveDeatil(request.ReserveId, request.Date, request.Payment)
            .WithComment(request.Comment)
            .WithAuditFields(request.User)
            .Build();

            return registro;
        }
        public ReserveDetail Update(ReserveDetailRequest request, ReserveDetail _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.Date, request.Payment, request.Comment, request.User);
            return _oldRegister;
        }
        public void Dispose()
        {
        }
    }
}