using System;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public interface IReserveDomainService : IDisposable
    {
        Reserve Create(ReserveRequest request);
        Reserve Update(ReserveRequest request, Reserve _oldRegister);
    }
    
}