using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public interface IReserveDetailAppService : IDisposable
    {
        IEnumerable<ReserveDetailDto> GetReserveDetails(ReserveDetailRequest request);
        IEnumerable<ReserveDetailDto> Create(ReserveDetailRequest request);
        IEnumerable<ReserveDetailDto> GetById(ReserveDetailRequest request);
        ReserveDetailDto Update(ReserveDetailRequest request);
        string Inactive(ReserveDetailRequest request);
    }
}