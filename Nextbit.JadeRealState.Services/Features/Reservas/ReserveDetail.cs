using System;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    [Table("ReservesDetail")]
    public class ReserveDetail : Entity
    {
        public int ReserveId { get; private set; }
        public DateTime Date { get; private set; }
        public Decimal Payment { get; private set; }
        public string Comment { get; private set; }
        public bool Active { get; private set; }

        public Reserve Reserve { get; set; }

        public void Update(DateTime _date, Decimal _payment, string _comment, string _user)
        {
            Date = _date;
            Payment = _payment;
            Comment = _comment;
            Active = true;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = string.IsNullOrEmpty(_user) ? "Service" : _user;
            TransactionUId = Guid.NewGuid();
        }

        public void Inactive(string _user)
        {
            Active = false;
            CrudOperation = "Inactive";
            TransactionDate = DateTime.Now;
            TransactionType = "Disabled";
            ModifiedBy = string.IsNullOrEmpty(_user) ? "Service" : _user;
            TransactionUId = Guid.NewGuid();
        }

        public class Builder
        {
            private readonly ReserveDetail _reserveDetail = new ReserveDetail();

            public Builder WithReserveDeatil(int reserveId, DateTime date, Decimal Payment)
            {
                _reserveDetail.ReserveId = reserveId;
                _reserveDetail.Date = date;
                _reserveDetail.Payment = Payment;
                return this;
            }
            public Builder WithComment(string comment)
            {
                _reserveDetail.Comment = comment;
                return this;
            }
            public Builder WithAuditFields(string user)
            {
                _reserveDetail.Active = true;
                _reserveDetail.CrudOperation = "Added";
                _reserveDetail.TransactionDate = DateTime.Now;
                _reserveDetail.TransactionType = "NewProject";
                _reserveDetail.ModifiedBy = string.IsNullOrEmpty(user) ? "Service" : user;
                _reserveDetail.TransactionUId = Guid.NewGuid();

                return this;
            }
            public ReserveDetail Build()
            {
                return _reserveDetail;
            }
        }
    }
}