using System;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public class ReserveDetailRequest : RequestBase
    {
        public int Id { get; set; }
        public int ReserveId { get;  set; }
        public DateTime Date { get;  set; }
        public Decimal Payment { get;  set; }
        public string Comment { get;  set; }

    }
}