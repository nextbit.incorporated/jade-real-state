using System;
using System.Linq;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public class ReserveDomainService : IReserveDomainService
    {
        public Reserve Create(ReserveRequest request)
        {
            if (request.NegotiationId <= 0) throw new ArgumentNullException(nameof(request.NegotiationId));

            Reserve registro = new Reserve.Builder()
            .WithNegotiation(request.NegotiationId)
            .WithSalesAdvisor(request.SalesAdvisorId)
            .WithLotNumber(request.LotNumber)
            .WithZone(request.Zone)
            .WithBlock(request.Block)
            .WithAddress(request.Address)
            .WithTotalAmount(request.TotalAmount)
            .WithAmountPayments(request.AmountOfPayments)
            .WithReserveStatus(request.ReserveStatus)
            .WithAuditFields(request.User)
            .Build();

            if(request.Detalle== null) return registro;
            if(!request.Detalle.Any()) return registro;

            foreach (var item in request.Detalle)
            {
               ReserveDetail detalle = new ReserveDetail.Builder()
                .WithReserveDeatil(0, item.Date, item.Payment)
                .WithComment(item.Comment)
                .WithAuditFields(request.User)
                .Build();

                registro.Detalles.Add(detalle); 
            }
            return registro;
        }
        
        public Reserve Update(ReserveRequest request, Reserve _oldRegister)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (_oldRegister == null) throw new ArgumentException(nameof(_oldRegister));

            _oldRegister.Update(request.NegotiationId, request.SalesAdvisorId,
                        request.LotNumber, request.Zone, request.Block, request.Address,
                        request.TotalAmount, request.AmountOfPayments, request.ReserveStatus);

            return _oldRegister;
        }

        public void Dispose()
        {
        }
    }
}