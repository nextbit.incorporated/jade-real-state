using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public interface IReserveAppService : IDisposable
    {
        ReservePagedDto GetPaged(ReservePagedRequest request);
        ReserveDTO Create(ReserveRequest request);
        ReserveDTO Update(ReserveRequest request);
        string Delete(int id);

        IEnumerable<ReserveDTO> GetReservePending();
    }
}