using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public class ReserveDTO : ResponseBase
    {
        public int NegotiationId { get; set; }
        public int SalesAdvisorId { get;  set; }
        public int LotNumber { get;  set; }
        public string Zone { get;  set; }
        public string Block { get;  set; }
        public string Address { get;  set; }
        public decimal TotalAmount { get;  set; }
        public int AmountOfPayments { get;  set; }
        public string ReserveStatus { get;  set; }
        public string SalesAdvisorName { get; set; }
        public string ProjectName { get; set; }
        public string ClientName { get; set; }
        public DateTime Date { get; set; }
        public NegotiationDto Negotiation { get; set; }
        public UserDTO SalesAdvisor { get; set; }
        public ICollection<ReserveDetailDto> Detalle { get; set; }


        internal static List<ReserveDTO> FromReserves(IEnumerable<Reserve> detail)
        {
            return (from qry in detail
                    select From(qry)).ToList();
        }

        internal static ReserveDTO From(Reserve s)
        {
            if(s == null) return new ReserveDTO();
            return new ReserveDTO
            {
                Id = s.Id,
                NegotiationId = s.NegotiationId,
                SalesAdvisorId = s.SalesAdvisorId,
                LotNumber = s.LotNumber,
                Zone = s.Zone,
                Block = s.Block,
                TotalAmount = s.TotalAmount,
                AmountOfPayments = s.AmountOfPayments,
                ReserveStatus = s.ReserveStatus,
                Address = s.Address,
                // Negotiation = s.Negotiation == null ? new NegotiationDto() : NegotiationDto.From(s.Negotiation),  
                Detalle = s.Detalles == null || !s.Detalles.Any() ?  new List<ReserveDetailDto>() : ReserveDetailDto.FromNegotiations(s.Detalles)
            };
        }
    }
}