using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public class ReserveDetailDto : ResponseBase
    {
        public int ReserveId { get;  set; }
        public DateTime Date { get;  set; }
        public Decimal Payment { get;  set; }
        public string Comment { get;  set; }
        public bool Active { get;  set; }

        internal static List<ReserveDetailDto> FromNegotiations(IEnumerable<ReserveDetail> detail)
        {
            if(detail == null || !detail.Any()) return new List<ReserveDetailDto>();
            return (from qry in detail
                    select From(qry)).ToList();
        }

        internal static ReserveDetailDto From(ReserveDetail s)
        {
            if(s == null) return new ReserveDetailDto();
            return new ReserveDetailDto
            {
                Id = s.Id,
                ReserveId = s.ReserveId,
                Date = s.Date,
                Payment = s.Payment,
                Comment = s.Comment,
                Active = s.Active
            };
        }

    }
}