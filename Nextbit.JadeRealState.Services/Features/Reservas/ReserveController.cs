using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReserveController : ControllerBase
    {
        private readonly IReserveAppService _reserveAppService;

        public ReserveController(IReserveAppService reserveAppService)
        {
            if (reserveAppService == null) throw new ArgumentException(nameof(reserveAppService));

            _reserveAppService = reserveAppService;
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<IEnumerable<ReserveDTO>> GetPaged([FromQuery]ReservePagedRequest request)
        {
            return Ok(_reserveAppService.GetPaged(request));
        }

        [HttpGet]
        [Route("top-reserve")]
        [Authorize]
        public ActionResult<IEnumerable<ReserveDTO>> GetTopReserve()
        {
            return Ok(_reserveAppService.GetReservePending());
        }


        [HttpPost]
        [Authorize]
        public ActionResult<ReserveDTO> Post([FromBody]ReserveRequest request)
        {
            return Ok(_reserveAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<ReserveDTO> Put([FromBody] ReserveRequest request)
        {
            return Ok(_reserveAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(int Id)
        {
            return Ok(_reserveAppService.Delete(Id));
        }

    }
}