using System;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public interface IReserveDetailDomainService : IDisposable
    {
        ReserveDetail Create(ReserveDetailRequest request);
        ReserveDetail Update(ReserveDetailRequest request, ReserveDetail _oldRegister);
    }
}