using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.Projects;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public class ReserveAppService : IReserveAppService
    {
        private RealStateContext _context;
        private readonly IReserveDomainService _reserveDomainService;
        public ReserveAppService(RealStateContext context, IReserveDomainService reserveDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (reserveDomainService == null) throw new ArgumentException(nameof(reserveDomainService));

            _context = context;
            _reserveDomainService = reserveDomainService;
        }

        public ReservePagedDto GetPaged(ReservePagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            var count = Convert.ToDecimal(_context.Reserves.Count());
            var totalPage = Math.Ceiling(count / request.PageSize);
            if (request.PageIndex > totalPage) request.PageIndex = 1;
            if (request.PageIndex <= 0) request.PageIndex = 1;
            if (string.IsNullOrEmpty(request.Value))
            {
                var lista = _context.Reserves
                    .Include(q => q.SalesAdvisor)
                    .OrderByDescending(
                    s => s.TransactionDate).Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ReservePagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Reserves = lista.Select(s => new ReserveDTO
                    {
                        Id = s.Id,
                        NegotiationId = s.NegotiationId,
                        SalesAdvisorId = s.SalesAdvisorId,
                        LotNumber = s.LotNumber,
                        Zone = s.Zone,
                        Block = s.Block,
                        TotalAmount = s.TotalAmount,
                        AmountOfPayments = s.AmountOfPayments,
                        ReserveStatus = s.ReserveStatus,
                        Address = s.Address,
                    }).ToList()
                };
            }
            else
            {
                var lista = _context.Reserves.Where(s => s.NegotiationId == request.ValueId)
                 .Include(q => q.SalesAdvisor)
                 .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new ReservePagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Reserves = lista.Select(s => new ReserveDTO
                    {
                        Id = s.Id,
                        NegotiationId = s.NegotiationId,
                        SalesAdvisorId = s.SalesAdvisorId,
                        LotNumber = s.LotNumber,
                        Zone = s.Zone,
                        Block = s.Block,
                        TotalAmount = s.TotalAmount,
                        AmountOfPayments = s.AmountOfPayments,
                        ReserveStatus = s.ReserveStatus,
                        Address = s.Address,
                    }).ToList()
                };
            }
        }

        private string ObtenerNombreCliente(Client client)
        {
            return client.FirstName + " " + client.MiddleName ?? ""
                    + " " + client.FirstSurname ?? "" + " " + client.SecondSurname ?? "";
        }

        public ReserveDTO Create(ReserveRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newProject = _reserveDomainService.Create(request);

            _context.Reserves.Add(newProject);
            _context.SaveChanges();

            var newReserves = _context.Reserves
                .Where(s => s.NegotiationId == request.NegotiationId)
                .Include(x => x.Detalles)
                .Include(x => x.Negotiation).ThenInclude(t => t.Project)
                .Include(x => x.Negotiation).ThenInclude(x => x.NegotiationClients).ThenInclude(t => t.Client);
            var nReserve = newReserves.FirstOrDefault();
            return ReserveDTO.From(nReserve);
        }

        public ReserveDTO Update(ReserveRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldReserve = _context.Reserves.FirstOrDefault(s => s.Id == request.Id);
            if (oldReserve == null) return new ReserveDTO { ValidationErrorMessage = "Proyecto inexistente" };

            var newReserve = _reserveDomainService.Update(request, oldReserve);

            _context.Reserves.Update(oldReserve);
            _context.SaveChanges();


            return new ReserveDTO
            {
                Id = newReserve.Id,
                NegotiationId = newReserve.NegotiationId,
                SalesAdvisorId = newReserve.SalesAdvisorId,
                LotNumber = newReserve.LotNumber,
                Zone = newReserve.Zone,
                Block = newReserve.Block,
                TotalAmount = newReserve.TotalAmount,
                AmountOfPayments = newReserve.AmountOfPayments,
                ReserveStatus = newReserve.ReserveStatus,
                Address = newReserve.Address,
            };
        }

        public string Delete(int id)
        {
            if (id <= 0) throw new ArgumentException(nameof(id));

            var project = _context.Reserves.FirstOrDefault(s => s.Id == id);
            if (project == null) throw new Exception("Project not exists");
            _context.Reserves.Remove(project);
            _context.SaveChanges();

            return string.Empty;
        }

        public IEnumerable<ReserveDTO> GetReservePending()
        {
            // IEnumerable<Reserve> trakings = _context.Reserves
            //             .Where(s => s.ReserveStatus == "Pendiente")
            //             //.Include(s => s.Client)
            //             .OrderBy(s => s.TransactionDate)
            //             .Take(5).ToList();

            // var projectsId = trakings.Select(s => s.ProjectId).ToList();
            // var clientesId = trakings.Select(s => s.ClientId).ToList();

            // IEnumerable<Project> servicios = _context.Projects.Where(s => projectsId.Contains(s.Id)).ToList();
            // IEnumerable<Client> clientes = _context.Clients.Where(s => clientesId.Contains(s.Id)).ToList();

            // var respuesta = trakings.Select(s => new ReserveDTO
            // {
            //     Id = s.Id,
            //     Date = s.TransactionDate,
            // }).OrderBy(s => s.Date);

            // return respuesta.ToList();
            return new List<ReserveDTO>();
        }

        private string ObtenerNombreCliente(int clientId, IEnumerable<Client> clientes)
        {
            var cliente = clientes.FirstOrDefault(s => s.Id == clientId);
            if (cliente == null) return string.Empty;
            return EncriptorHelper.DecryptString(cliente.FirstName) + " " + EncriptorHelper.DecryptString(cliente.MiddleName ?? "")
                   + " " + EncriptorHelper.DecryptString(cliente.FirstSurname ?? "") + " " + EncriptorHelper.DecryptString(cliente.SecondSurname ?? "");
        }

        private string ObtenerNombreProyecto(int projectId, IEnumerable<Project> projectos)
        {
            var proyecto = projectos.FirstOrDefault(s => s.Id == projectId);
            if (proyecto == null) return string.Empty;
            return proyecto.Name ?? string.Empty;
        }
        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_reserveDomainService != null) _reserveDomainService.Dispose();
        }


    }
}