using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    [Table("Reserves")]
    public class Reserve : Entity
    {
        public int NegotiationId { get; private set; }
        public int SalesAdvisorId { get; private set; }
        public int LotNumber { get; private set; }
        public string Zone { get; private set; }
        public string Block { get; private set; }
        public string Address { get; private set; }
        public decimal TotalAmount { get; private set; }
        public int AmountOfPayments { get; private set; }
        public string ReserveStatus { get; private set; }

        public User SalesAdvisor { get; set; }
        public Negotiation Negotiation { get; set; }

        public ICollection<ReserveDetail> Detalles { get; set; }

        public void Update(int projectId,int salesAdvisorId, 
            int lotNumber, string zone, string block, string address, 
            decimal totalAmount, int amountOfPayments, string reserveStatus)
        {
            SalesAdvisorId = salesAdvisorId;
            LotNumber = lotNumber;
            Zone = zone;
            Block = block;
            Address = address;
            TotalAmount = totalAmount;
            AmountOfPayments = amountOfPayments;
            ReserveStatus = reserveStatus;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
        }

        public class Builder
        {
            private readonly Reserve _reserve = new Reserve();

            public Builder WithNegotiation(int negotiationId)
            {
                _reserve.NegotiationId = negotiationId;
                return this;
            }
            public Builder WithSalesAdvisor(int salesAdvisorId)
            {
                _reserve.SalesAdvisorId = salesAdvisorId;
                return this;
            }
            public Builder WithLotNumber(int lotNumber)
            {
                _reserve.LotNumber = lotNumber;
                return this;
            }
            public Builder WithZone(string zone)
            {
                _reserve.Zone = zone;
                return this;
            }
            public Builder WithBlock(string block)
            {
                _reserve.Block = block;
                return this;
            }
            public Builder WithAddress(string address)
            {
                _reserve.Address = address;
                return this;
            }
            public Builder WithTotalAmount(decimal total)
            {
                _reserve.TotalAmount = total;
                return this;
            }
            public Builder WithAmountPayments(int amountPayments)
            {
                _reserve.AmountOfPayments = amountPayments;
                return this;
            }
            public Builder WithReserveStatus(string reserveStatus)
            {
                _reserve.ReserveStatus = reserveStatus;
                return this;
            }
            public Builder WithAuditFields(string user)
            {
                _reserve.Detalles = new List<ReserveDetail>();
                _reserve.CrudOperation = "Added";
                _reserve.TransactionDate = DateTime.Now;
                _reserve.TransactionType = "NewProject";
                _reserve.ModifiedBy = string.IsNullOrEmpty(user) ? "Service" : user;
                _reserve.TransactionUId = Guid.NewGuid();

                return this;
            }

            public Reserve Build()
            {
                return _reserve;
            }
        }
    }
}