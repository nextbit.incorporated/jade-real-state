using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public class ReserveRequest : RequestBase
    {
        public int Id { get; set; }
        public int NegotiationId { get; set; }
        public int SalesAdvisorId { get; set; }
        public int LotNumber { get; set; }
        public string Zone { get; set; }
        public string Block { get; set; }
        public string Address { get; set; }
        public decimal TotalAmount { get; set; }
        public int AmountOfPayments { get; set; }
        public string ReserveStatus { get; set; }
        public string SalesAdvisorName { get; set; }
        public string ProjectName { get; set; }
        public string ClientName { get; set; }
        public List<ReserveDetailRequest> Detalle { get; set; }
    }

    public class ReservePagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
        public int ValueId { get; set; }
    }
}