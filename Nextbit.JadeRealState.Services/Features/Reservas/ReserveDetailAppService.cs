using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    public class ReserveDetailAppService : IReserveDetailAppService
    {
        private RealStateContext _context;
        private readonly IReserveDetailDomainService _reserveDetailDomainService;
        public ReserveDetailAppService(RealStateContext context, IReserveDetailDomainService reserveDetailDomainService)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            if (reserveDetailDomainService == null) throw new ArgumentException(nameof(reserveDetailDomainService));

            _context = context;
            _reserveDetailDomainService = reserveDetailDomainService;
        }
        public IEnumerable<ReserveDetailDto> Create(ReserveDetailRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var newRow = _reserveDetailDomainService.Create(request);

            _context.ReserveDetails.Add(newRow);
            _context.SaveChanges();

            var detalles = _context.ReserveDetails
                .Where(s => s.ReserveId == request.ReserveId);
            var nuevoDetalle = ReserveDetailDto.FromNegotiations(detalles);
            return nuevoDetalle;
        }

        public IEnumerable<ReserveDetailDto> GetById(ReserveDetailRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var detalles = _context.ReserveDetails
                .Where(s => s.Id == request.ReserveId);
            var nuevoDetalle = ReserveDetailDto.FromNegotiations(detalles);
            return nuevoDetalle;
        }
        public IEnumerable<ReserveDetailDto> GetReserveDetails(ReserveDetailRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.Id == 0) throw new ArgumentException(nameof(request.Id));
            var details = _context.ReserveDetails.Where(s => s.Id == request.Id);

            if (details == null) return new List<ReserveDetailDto>();
            if(!details.Any()) return new List<ReserveDetailDto>();
            return ReserveDetailDto.FromNegotiations(details);
        }

        public string Inactive(ReserveDetailRequest request)
        { 
            if (request == null) throw new ArgumentException(nameof(request));
            if (request.Id == 0) throw new ArgumentException(nameof(request.Id));
            var oldReserveDetail = _context.ReserveDetails.FirstOrDefault(s => s.Id == request.Id);
            if (oldReserveDetail == null) return "Proyecto inexistente";

            oldReserveDetail.Inactive(request.User);
            _context.ReserveDetails.Update(oldReserveDetail);
            _context.SaveChanges();
            
            return string.Empty;
        }

        public ReserveDetailDto Update(ReserveDetailRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldReserveDetail = _context.ReserveDetails.FirstOrDefault(s => s.Id == request.Id);
            if (oldReserveDetail == null) return new ReserveDetailDto { ValidationErrorMessage = "Proyecto inexistente" };

            var newReserveDetail = _reserveDetailDomainService.Update(request, oldReserveDetail);

            _context.ReserveDetails.Update(oldReserveDetail);
            _context.SaveChanges();

            return ReserveDetailDto.From(oldReserveDetail);
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_reserveDetailDomainService != null) _reserveDetailDomainService.Dispose();
        }

    }
}