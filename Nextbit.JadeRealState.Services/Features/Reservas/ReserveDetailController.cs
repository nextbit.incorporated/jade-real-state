using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.Reservas
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReserveDetailController : ControllerBase
    {
        private readonly IReserveDetailAppService _reserveDetailAppService;

        public ReserveDetailController(IReserveDetailAppService reserveDetailAppService)
        {
            if (reserveDetailAppService == null) throw new ArgumentException(nameof(reserveDetailAppService));

            _reserveDetailAppService = reserveDetailAppService;
        }

        [HttpGet]
        [Authorize]
        public ActionResult<IEnumerable<ReserveDetailDto>> Get([FromQuery]ReserveDetailRequest request)
        {
            return Ok(_reserveDetailAppService.GetReserveDetails(request));
        }

        [HttpGet]
        [Route("byId")]
        [Authorize]
        public ActionResult<IEnumerable<ReserveDetailDto>> GetById([FromQuery]ReserveDetailRequest request)
        {
            return Ok(_reserveDetailAppService.GetById(request));
        }


        [HttpPost]
        [Authorize]
        public ActionResult<IEnumerable<ReserveDetailDto>> Post([FromBody]ReserveDetailRequest request)
        {
            return Ok(_reserveDetailAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<ReserveDTO> Put([FromBody] ReserveDetailRequest request)
        {
            return Ok(_reserveDetailAppService.Update(request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete(ReserveDetailRequest request)
        {
            return Ok(_reserveDetailAppService.Inactive(request));
        }
    }
}