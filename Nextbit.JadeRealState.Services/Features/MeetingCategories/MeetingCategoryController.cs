using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Nextbit.JadeRealState.Services.Features.MeetingCategories
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MeetingCategoryController : ControllerBase
    {
        private readonly IMeetingCategoryAppService _meetingCategoryAppService;
        public MeetingCategoryController(IMeetingCategoryAppService meetingCategoryAppService)
        {
            _meetingCategoryAppService = meetingCategoryAppService ?? throw new ArgumentException(nameof(meetingCategoryAppService));
        }

        [HttpGet]
        [Authorize]
        public ActionResult<MeetingCategoryPagedDto> Get()
        {
            return Ok(_meetingCategoryAppService.Get());
        }

        [HttpGet]
        [Route("paged")]
        [Authorize]
        public ActionResult<MeetingCategoryPagedDto> GetPaged([FromQuery] MeetingCategoryPagedRequest request)
        {
            return Ok(_meetingCategoryAppService.GetPaged(request));
        }

        [HttpPost]
        [Authorize]
        public ActionResult<MeetingCategoryDto> Post([FromBody] MeetingCategoryRequest request)
        {
            return Ok(_meetingCategoryAppService.Create(request));
        }

        [HttpPut]
        [Authorize]
        public ActionResult<MeetingCategoryDto> Put([FromBody] MeetingCategoryRequest request)
        {
            return Ok(_meetingCategoryAppService.Update(request));
        }

        [HttpPut]
        [Route("disabled-register")]
        [Authorize]
        public ActionResult<MeetingCategoryDto> Disabled([FromBody] MeetingCategoryRequest request)
        {
            return Ok(_meetingCategoryAppService.DisabledRegister(request));
        }

    }
}