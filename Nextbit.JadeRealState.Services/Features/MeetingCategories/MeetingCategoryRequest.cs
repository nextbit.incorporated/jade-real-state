using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.MeetingCategories
{
    public class MeetingCategoryRequest : RequestBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class MeetingCategoryPagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
    }
}