using System;

namespace Nextbit.JadeRealState.Services.Features.MeetingCategories
{
    public class MeetingCategoryDomainService : IMeetingCategoryDomainService
    {
        public MeetingCategory Create(MeetingCategoryRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.Name)) throw new ArgumentNullException(nameof(request.Name));

            MeetingCategory meetingCategory = new MeetingCategory.Builder()
            .WithName(request.Name)
            .WithDescription(request.Description)
            .WithAuditFields(request.User ?? null)
            .Build();

            return meetingCategory;
        }

        public MeetingCategory Update(MeetingCategory oldRegister, MeetingCategoryRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            if (oldRegister == null) throw new ArgumentException(nameof(oldRegister));

            oldRegister.Update(
                request.Name, request.Description, request.User);

            return oldRegister;
        }

        public void Dispose()
        {
        }
    }
}