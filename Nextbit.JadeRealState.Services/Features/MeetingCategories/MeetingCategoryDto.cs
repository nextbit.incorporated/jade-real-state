using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.MeetingCategories
{
    public class MeetingCategoryDto : ResponseBase
    {
        public string Name { get; set; }
        public string Description { get; set; }

        internal static List<MeetingCategoryDto> FromRegisters(IEnumerable<MeetingCategory> detail)
        {
            if (detail == null || !detail.Any()) return new List<MeetingCategoryDto>();
            return (from qry in detail select From(qry)).ToList();
        }

        public static MeetingCategoryDto From(MeetingCategory item)
        {
            if(item == null) return new MeetingCategoryDto();

             return new MeetingCategoryDto
             {
                 Id = item.Id,
                 Name = item.Name,
                 Description = item.Description
             };
        }
    }
}