using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.ClientsTracking;

namespace Nextbit.JadeRealState.Services.Features.MeetingCategories
{

    [Table("MeetingCategories")]
    public class MeetingCategory : Entity
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public bool Active { get; private set; }

        public ICollection<ClientTracking> Prospects { get; set; }

        public void Update(string _name, string _description, string _user)
        {
            Name = _name;
            Description = _description;
            Active = true;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = _user ?? "Service";
            TransactionUId = Guid.NewGuid();
        }

        public void UpdateValueActive(bool active, string user)
        {
            Active = active;
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = user ?? "Service";
            TransactionUId = Guid.NewGuid();
        }

        public class Builder
        {
            private readonly MeetingCategory _meetingCategory = new MeetingCategory();

            public Builder WithName(string name)
            {
                _meetingCategory.Name = name;
                _meetingCategory.Active = true;
                return this;
            }

            public Builder WithDescription(string description)
            {
                _meetingCategory.Description = description;
                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _meetingCategory.CrudOperation = "Added";
                _meetingCategory.TransactionDate = DateTime.Now;
                _meetingCategory.TransactionType = "New";
                _meetingCategory.ModifiedBy = user ?? "Service";
                _meetingCategory.TransactionUId = Guid.NewGuid();

                return this;
            }

            public MeetingCategory Build()
            {
                return _meetingCategory;
            }
        }
    }
}