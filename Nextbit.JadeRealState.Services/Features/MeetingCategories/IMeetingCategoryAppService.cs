using System;
using System.Collections.Generic;

namespace Nextbit.JadeRealState.Services.Features.MeetingCategories
{
    public interface IMeetingCategoryAppService : IDisposable
    {
        List<MeetingCategoryDto> Get();
        MeetingCategoryPagedDto GetPaged(MeetingCategoryPagedRequest request);
        MeetingCategoryDto Create(MeetingCategoryRequest request);
        MeetingCategoryDto Update(MeetingCategoryRequest request);
        MeetingCategoryDto DisabledRegister(MeetingCategoryRequest request);
    }
}