using System;

namespace Nextbit.JadeRealState.Services.Features.MeetingCategories
{
    public interface IMeetingCategoryDomainService : IDisposable
    {
        MeetingCategory Create(MeetingCategoryRequest request);
        MeetingCategory Update(MeetingCategory oldRegister, MeetingCategoryRequest request);
    }
}