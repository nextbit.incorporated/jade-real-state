using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Features.MeetingCategories
{
    public class MeetingCategoryAppService : IMeetingCategoryAppService
    {
        private readonly RealStateContext _context;
        private readonly IMeetingCategoryDomainService _meetingCategoryDomainService;

        public MeetingCategoryAppService(RealStateContext context,  IMeetingCategoryDomainService meetingCategoryAppService)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _meetingCategoryDomainService = meetingCategoryAppService ?? throw new ArgumentException(nameof(meetingCategoryAppService));
        }
        public MeetingCategoryDto Create(MeetingCategoryRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var meetingCategory = _meetingCategoryDomainService.Create(request);

            _context.MeetingCategories.Add(meetingCategory);
            _context.SaveChanges();

            return  MeetingCategoryDto.From(meetingCategory);
        }

        public MeetingCategoryDto DisabledRegister(MeetingCategoryRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRegister = _context.MeetingCategories.FirstOrDefault(s => s.Id == request.Id);
            if (oldRegister == null) return new MeetingCategoryDto { ValidationErrorMessage = "Institucion inexistente" };

            oldRegister.UpdateValueActive(false, request.User);

            _context.MeetingCategories.Update(oldRegister);
            _context.SaveChanges();

            return MeetingCategoryDto.From(oldRegister);
        }

        public List<MeetingCategoryDto> Get()
        {
            IEnumerable<MeetingCategory> meetingCategories = _context.MeetingCategories.ToList();
            if (meetingCategories == null) return new List<MeetingCategoryDto>();
            return MeetingCategoryDto.FromRegisters(meetingCategories);
        }

        public MeetingCategoryPagedDto GetPaged(MeetingCategoryPagedRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
         
            if (string.IsNullOrEmpty(request.Value))
            {
                if (request.PageIndex == 0) request.PageIndex = 1;
                var count = Convert.ToDecimal(_context.MeetingCategories.Where(s => s.Active).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.MeetingCategories.Where(s => s.Active)
                .OrderByDescending(s => s.TransactionDate)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new MeetingCategoryPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = Convert.ToInt16(totalPage),
                    Items = MeetingCategoryDto.FromRegisters(lista)
                };
            }
            else
            {
                if (request.PageIndex == 0) request.PageIndex = 1;
                var count = Convert.ToDecimal(_context.MeetingCategories
                            .Where(s => s.Name.Contains(request.Value) && s.Active).Count());
                var totalPage = Math.Ceiling(count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;
                var lista = _context.MeetingCategories.Where(s => s.Name.Contains(request.Value) && s.Active)
                .Skip(request.PageSize * (request.PageIndex - 1)).Take(request.PageSize).ToList();
                return new MeetingCategoryPagedDto
                {
                    PageIndex = request.PageIndex,
                    PageSize = request.PageSize,
                    PageCount = (int)(count / request.PageSize) == 0 ? 1 : (int)(count / request.PageSize),
                    Items = MeetingCategoryDto.FromRegisters(lista)
                };
           }
        }

        public MeetingCategoryDto Update(MeetingCategoryRequest request)
        {
            if (request == null) throw new ArgumentException(nameof(request));
            var oldRegister = _context.MeetingCategories.FirstOrDefault(s => s.Id == request.Id);
            if (oldRegister == null) return new MeetingCategoryDto { ValidationErrorMessage = "Institucion inexistente" };

            var financialInstitutionUpdate = _meetingCategoryDomainService.Update(oldRegister, request);

            _context.MeetingCategories.Update(oldRegister);
            _context.SaveChanges();

            return MeetingCategoryDto.From(oldRegister);
        }

        public void Dispose()
        {
            if (_context != null) _context.Dispose();
            if (_meetingCategoryDomainService != null) _meetingCategoryDomainService.Dispose();
        }
    }
}