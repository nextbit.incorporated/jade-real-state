using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nextbit.JadeRealState.Services.Features.Reports.Dashboard;
using Nextbit.JadeRealState.Services.Features.Reports.Negotiations;

namespace Nextbit.JadeRealState.Services.Features.Negotiations {
    [Route ("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NegotiationController : ControllerBase {
        private readonly INegotiationAppService _negotiationAppService;

        public NegotiationController (INegotiationAppService negotiationAppService) {
            _negotiationAppService = negotiationAppService ??
                throw new ArgumentException (nameof (negotiationAppService));
        }

        [HttpGet]
        [Route ("paged")]
        [Authorize]
        public ActionResult<IEnumerable<NegotiationDto>> GetPaged (
            [FromQuery] NegotiationPagedRequest request) {
            var result = _negotiationAppService.GetPaged (request);
            return Ok (result);
        }

        [HttpGet]
        [Route ("general-report")]
        [Authorize]
        public ActionResult<List<NegotiationDto>> GetReport ([FromQuery] NegotiationRequest request) {
            var result = _negotiationAppService.GetGeneralReport (request);
            return Ok (result);
        }

        [HttpGet]
        [Route ("closed-report")]
        // [Authorize]
        [AllowAnonymous]
        public ActionResult<List<NegotiationDto>> GetNegotiationClosedReport ([FromQuery] NegotiationRequest request) {
            var result = _negotiationAppService.GetNegotiationsClosed (request);
            return Ok (result);
        }

        [HttpGet]
        [Route ("statistics-negotiations-fi")]
        [Authorize]
        public ActionResult<SumClientByFinancialInstitutionDto> GetStatisticsNegotiationsFi () {
            var result = _negotiationAppService.GetStatisticsNegotiationsFinancialInstitutions ();
            return Ok (result);
        }

        [HttpGet]
        [Route ("statistics-negotiations-open")]
        [Authorize]
        public ActionResult<NegotiationProcessReportDto> GetStaticsNegotiaions () {
            var result = _negotiationAppService.GetStaticsNegotiaions ();
            return Ok (result);
        }

        [HttpGet]
        [Route ("statistics-negotiations-by-sa")]
        [Authorize]
        public ActionResult<SumNegotationsBySalesAdvisor> GetDataNegotiationBySalesAdvisor () {
            var result = _negotiationAppService.GetDataNegotiationBySalesAdvisor ();
            return Ok (result);
        }

        [HttpPost]
        [Route ("validate-create-new-negotiation")]
        [Authorize]
        public ActionResult<IEnumerable<NegotiationDto>> GetTryCreateNewClient (
            [FromBody] NegotiationRequest request) {
            return Ok (_negotiationAppService.ValidateNewNegotiationCreation (request));
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<NegotiationDto>> Post ([FromBody] NegotiationRequest request) {
            return Ok (await _negotiationAppService.CreateAsync (request));
        }

        [HttpPut]
        [Authorize]
        public async Task<ActionResult<NegotiationDto>> Put ([FromBody] NegotiationRequest request) {
            return Ok (await _negotiationAppService.UpdateAsync (request));
        }

        [HttpPut]
        [Route ("close-negotiation")]
        [Authorize]
        public ActionResult<NegotiationDto> CloseNegotaition ([FromBody] NegotiationRequest request) {
            return Ok (_negotiationAppService.CloseNegotiationStatus (request));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult<string> Delete (int Id) {
            return Ok (_negotiationAppService.Delete (Id));
        }
    }
}