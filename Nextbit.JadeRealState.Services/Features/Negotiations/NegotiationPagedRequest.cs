using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Negotiations
{
    public class NegotiationPagedRequest : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public string Value { get; set; }
        public int ValueId { get; set; }
        public int IdFilter { get; set; }
    }
}