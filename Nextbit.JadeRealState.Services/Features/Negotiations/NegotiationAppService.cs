using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.Builders;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.Downpayments;
using Nextbit.JadeRealState.Services.Features.FinancialApprovals;
using Nextbit.JadeRealState.Services.Features.FinancialInstitutions;
using Nextbit.JadeRealState.Services.Features.FinancialPrequalifications;
using Nextbit.JadeRealState.Services.Features.FundTypes;
using Nextbit.JadeRealState.Services.Features.HouseDesigns;
using Nextbit.JadeRealState.Services.Features.NegotiationClients;
using Nextbit.JadeRealState.Services.Features.NegotiationContracts;
using Nextbit.JadeRealState.Services.Features.NegotiationPhases;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.Reports.Dashboard;
using Nextbit.JadeRealState.Services.Features.Reports.Negotiations;
using Nextbit.JadeRealState.Services.Features.Reservas;
using Nextbit.JadeRealState.Services.Features.SupplierServices;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Negotiations {
    public class NegotiationAppService : INegotiationAppService {
        private readonly RealStateContext _context;

        private readonly INegotiationPhaseAppService _negotiationPhaseAppService;

        public NegotiationAppService (RealStateContext context,

            INegotiationPhaseAppService negotiationPhaseAppService) {
            _context = context ??
                throw new ArgumentNullException (nameof (context));
            _negotiationPhaseAppService = negotiationPhaseAppService ??
                throw new ArgumentException (nameof (negotiationPhaseAppService));
        }

        public async Task<NegotiationDto> CreateAsync (NegotiationRequest request) {
            if (request == null) throw new ArgumentException (nameof (request));
            if (request.Principal == null) throw new ArgumentException (nameof (request.Principal));

            Project project = await _context.Projects.FirstOrDefaultAsync (p => p.Id == request.ProjectId);
            ContactType contactType = await _context.ContactTypes.FirstOrDefaultAsync (c => c.Id == request.ContactTypeId);
            FinancialInstitution financialInstitution = await _context.FinancialInstitutions
                .FirstOrDefaultAsync (o => o.Id == request.FinancialInstitutionId);
            User salesAdvisor = await _context.Users.FirstOrDefaultAsync (u => u.Id == request.SalesAdvisorId);
            Client client = await _context.Clients.FirstOrDefaultAsync (c => c.Id == request.Principal.Id);

            Client coDeptor = request.CoDebtor != null ?
                await _context.Clients.FirstOrDefaultAsync (c => c.Id == request.CoDebtor.Id) :
                null;

            Reserve reserve = await _context.Reserves.FirstOrDefaultAsync (r => r.Id == request.ReserveId);
            Downpayment downpayment = await _context.Downpayments.FirstOrDefaultAsync (d => d.Id == request.DownpaymentId);
            SupplierService supplierService = await _context.SupplierService.FirstOrDefaultAsync (s => s.Id == request.SupplierServiceId);

            NegotiationClient titularNegotiationClient = CreateNegotiationClient (client, request.Principal, true);
            NegotiationClient coDebtorNegotiationClient = CreateNegotiationClient (coDeptor, request.CoDebtor, false);

            FundType fundType = await _context.FundTypes.FirstOrDefaultAsync (f => f.Id == request.FinancialPrequalification.FundTypeId);

            FinancialPrequalification financialPrequalification = CreateFinancialPrequalification (request.FinancialPrequalification, fundType);

            NegotiationContract negotiationContract = CreateNegotiationContract (request.NegotiationContract);

            var newNegotiation = new Negotiation.Builder ()
                .WithCreationDate (request.CreationDate)
                .WithNegotiationStartDate (request.NegotiationStartDate)
                .WithReserve (reserve)
                .WithDownpayment (downpayment)
                .WithCreditOfficer (request.CreditOfficer)
                .WithSalesAdvisor (salesAdvisor)
                .WithfinancialInstitution (financialInstitution)
                .WithContactType (contactType)
                .WithProject (project)
                .WithSupplierService (supplierService)
                .WithComments (request.Principal.Comments)
                .WithClient (titularNegotiationClient)
                .WithCoDebtorClient (coDebtorNegotiationClient)
                .WithFinancialPrequalification (financialPrequalification)
                .WithNegotiationContract (negotiationContract)
                .WithNegotationStatus ("OPEN")
                .WithAuditFields (request.User)
                .Build ();

            var tipoServicio = _context.SupplierService
                .Include (p => p.ServiceProcesses)
                .FirstOrDefault (s => s.Id == request.SupplierServiceId);

            newNegotiation.CreateNegotiationPhases (tipoServicio, request.User);

            _context.Negotiations.Add (newNegotiation);
            _context.SaveChanges ();

            return NegotiationDto.From (newNegotiation);
        }

        private NegotiationContract CreateNegotiationContract (NegotiationContractDto negotiationContract) {
            BuilderCompany builderCompany = _context.BuilderCompany
                .FirstOrDefault (b => b.Id == negotiationContract.BuilderCompanyId);

            NegotiationContract newNegotiationContract = new NegotiationContract.Builder ()
                .WithBuilderCompany (builderCompany)
                .WithContractSigningDate (negotiationContract.ContractSigningDate)
                .WithEffectiveDate (negotiationContract.EffectiveDate)
                .WithBlockNumber (negotiationContract.BlockNumber)
                .WithLotNumber (negotiationContract.LotNumber)
                .WithConstructionMeters (negotiationContract.ConstructionMeters)
                .WithLandSize (negotiationContract.LandSize)
                .WithRegistration (negotiationContract.Registration)
                .WithReservationValue (negotiationContract.ReservationValue)
                .WithDownPaymentValue (negotiationContract.DownPaymentValue)
                .WithDownPaymentMethod (negotiationContract.DownPaymentMethod)
                .WithLandValue (negotiationContract.LandValue)
                .WithConstructionValue (negotiationContract.ConstructionValue)
                .WithImprovementsValue (negotiationContract.ImprovementsValue)
                .WithHouseTotalValue (negotiationContract.HouseTotalValue)
                .Build ();

            return newNegotiationContract;
        }

        private FinancialPrequalification CreateFinancialPrequalification (
            FinancialPrequalificationDto financialPrequalification, FundType fundType) {
            FinancialApproval financialApproval = CreateFinancialApproval (financialPrequalification?.FinancialApproval);

            return new FinancialPrequalification.Builder ()
                .WithFundType (fundType)
                .WithInterestRate (financialPrequalification.InterestRate)
                .WithLoanAmount (financialPrequalification.LoanAmount)
                .WithLoanTerm (financialPrequalification.LoanTerm)
                .WithDownPaymentPercent (financialPrequalification.DownPaymentPercent)
                .WithDownPayment (financialPrequalification.DownPayment)
                .WithMonthlyPayment (financialPrequalification.MonthlyPayment)
                .WithComments (financialPrequalification.Comments)
                .WithFinancialApproval (financialApproval)
                .Build ();
        }

        private FinancialApproval CreateFinancialApproval (FinancialApprovalDto financialApproval) {
            return new FinancialApproval.Builder ()
                .WithJointloan (financialApproval?.Jointloan ?? false)
                .WithApprovalState ("PENDIENTE")
                .WithApprovalDate (financialApproval?.ApprovalDate)
                .WithRejectionDate (financialApproval?.RejectionDate)
                .WithRejectionReason (financialApproval?.RejectionReason)
                .WithCreditOfficerComments (financialApproval?.CreditOfficerComments)
                .WithSalesAdvisorComments (financialApproval?.SalesAdvisorComments)
                .WithStartDate (financialApproval?.StartDate)
                .WithDueDate (financialApproval?.DueDate)
                .WithFollowUp (financialApproval?.FollowUp)
                .Build ();
        }

        private void UpdateNegotiationContract (NegotiationContract negotiationContract, NegotiationContractDto updatedNegotiationContract) {
            BuilderCompany builderCompany = _context.BuilderCompany
                .FirstOrDefault (b => b.Id == updatedNegotiationContract.BuilderCompanyId);

            negotiationContract.SetBuilderCompany (builderCompany);
            negotiationContract.SetContractSigningDate (updatedNegotiationContract.ContractSigningDate);
            negotiationContract.SetEffectiveDate (updatedNegotiationContract.EffectiveDate);
            negotiationContract.SetBlockNumber (updatedNegotiationContract.BlockNumber);
            negotiationContract.SetLotNumber (updatedNegotiationContract.LotNumber);
            negotiationContract.SetConstructionMeters (updatedNegotiationContract.ConstructionMeters);
            negotiationContract.SetLandSize (updatedNegotiationContract.LandSize);
            negotiationContract.SetRegistration (updatedNegotiationContract.Registration);
            negotiationContract.SetReservationValue (updatedNegotiationContract.ReservationValue);
            negotiationContract.SetDownPaymentValue (updatedNegotiationContract.DownPaymentValue);
            negotiationContract.SetDownPaymentMethod (updatedNegotiationContract.DownPaymentMethod);
            negotiationContract.SetLandValue (updatedNegotiationContract.LandValue);
            negotiationContract.SetConstructionValue (updatedNegotiationContract.ConstructionValue);
            negotiationContract.SetImprovementsValue (updatedNegotiationContract.ImprovementsValue);
            negotiationContract.SetHouseTotalValue (updatedNegotiationContract.HouseTotalValue);
        }

        private void UpdateFinancialPrequalification (
            FinancialPrequalification financialPrequalification,
            FinancialPrequalificationDto updatedFinancialPrequalification,
            FundType fundType) {
            financialPrequalification.SetFundType (fundType);

            financialPrequalification.SetInterestRate (updatedFinancialPrequalification.InterestRate);
            financialPrequalification.SetLoanAmount (updatedFinancialPrequalification.LoanAmount);
            financialPrequalification.SetLoanTerm (updatedFinancialPrequalification.LoanTerm);
            financialPrequalification.SetDownPaymentPercent (updatedFinancialPrequalification.DownPaymentPercent);
            financialPrequalification.SetDownPayment (updatedFinancialPrequalification.DownPayment);
            financialPrequalification.SetMonthlyPayment (updatedFinancialPrequalification.MonthlyPayment);
            financialPrequalification.SetComments (updatedFinancialPrequalification.Comments);

            FinancialApproval financialApproval = financialPrequalification.FinancialApproval;

            if (financialApproval != null) {
                financialApproval.SetCreationDate (updatedFinancialPrequalification?.FinancialApproval?.CreationDate ?? DateTime.Now);
                financialApproval.SetJointloan (updatedFinancialPrequalification?.FinancialApproval?.Jointloan ?? false);
                financialApproval.SetApprovalState (updatedFinancialPrequalification?.FinancialApproval?.ApprovalState ?? "PENDIENTE");
                financialApproval.SetApprovalDate (updatedFinancialPrequalification?.FinancialApproval?.ApprovalDate);
                financialApproval.SetRejectionDate (updatedFinancialPrequalification?.FinancialApproval.RejectionDate);
                financialApproval.SetRejectionReason (updatedFinancialPrequalification?.FinancialApproval?.RejectionReason);
                financialApproval.SetCreditOfficerComments (updatedFinancialPrequalification?.FinancialApproval?.CreditOfficerComments);
                financialApproval.SetSalesAdvisorComments (updatedFinancialPrequalification?.FinancialApproval?.SalesAdvisorComments);
                financialApproval.SetStartDate (updatedFinancialPrequalification?.FinancialApproval?.StartDate);
                financialApproval.SetDueDate (updatedFinancialPrequalification?.FinancialApproval?.DueDate);
                financialApproval.SetFollowUp (updatedFinancialPrequalification?.FinancialApproval?.FollowUp);
            } else {
                financialApproval = CreateFinancialApproval (updatedFinancialPrequalification?.FinancialApproval);

                financialPrequalification.SetFinancialApproval (financialApproval);
            }

        }

        private NegotiationClient CreateNegotiationClient (
            Client client, NegotiationClientDto negotiationClient, bool titular) {
            if (client == null) {
                return null;
            }

            return new NegotiationClient.Builder ()
                .WithClient (client)
                .WithTitular (true)
                .WithRelationship (negotiationClient.Relationship)
                .WithWorkplace (negotiationClient.Workplace)
                .WithWorkStartDate (negotiationClient.WorkStartDate)
                .WithHomeAddress (negotiationClient.HomeAddress)
                .WithOwnHome (negotiationClient.OwnHome)
                .WithFirstHome (negotiationClient.FirstHome)
                .WithContributeToRap (negotiationClient.ContributeToRap)
                .WithCurrency (negotiationClient.Currency)
                .WithGrossMonthlyIncome (negotiationClient.GrossMonthlyIncome)
                .WithPreQualify (negotiationClient.PreQualify)
                .WithPrequalificationDate (negotiationClient.PrequalificationDate)
                .WithNextPrequalificationDate (negotiationClient.NextPrequalificationDate)
                .WithPrequalificationAmount (negotiationClient.PrequalificationAmount)
                .WithHasDebts (negotiationClient.HasDebts)
                .WithDebtsAmount (negotiationClient.DebtsAmount)
                .Build ();
        }

        public string Delete (int id) {
            if (id <= 0) throw new ArgumentException (nameof (id));

            var clienLevel = _context.Negotiations.FirstOrDefault (s => s.Id == id);
            if (clienLevel == null) throw new Exception ("Negotiation not exists");
            _context.Negotiations.Remove (clienLevel);
            _context.SaveChanges ();

            return string.Empty;
        }

        public NegotiationPagedDto GetPaged (NegotiationPagedRequest request) {
            if (request == null) throw new ArgumentException (nameof (request));
            if (request.PageIndex == 0) request.PageIndex = 1;
            var usuario = _context.Users.FirstOrDefault (s => s.UserId == request.User);
            if (usuario == null) return new NegotiationPagedDto { Negotiations = new List<NegotiationDto> () };
            var usuarioRoles = _context.UserRol.FirstOrDefault (s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new NegotiationPagedDto { Negotiations = new List<NegotiationDto> () };
            if (request.PageIndex == 0) request.PageIndex = 1;

            IEnumerable<Negotiation> negotiations = GetNegotiations ();

            if (!string.IsNullOrWhiteSpace (request.Value)) {
                negotiations = negotiations
                    .Where (n => n.NegotiationClients.Any (nc => nc.Client.FullTextSearchField.Contains (request.Value)));

            }

            if (usuarioRoles.RolId != 3) {
                var count = Convert.ToDecimal (negotiations.Count ());
                var totalPage = Math.Ceiling (count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;

                List<Negotiation> pagedNegotiations = negotiations
                    .OrderByDescending (s => s.TransactionDate)
                    .Skip (request.PageSize * (request.PageIndex - 1))
                    .Take (request.PageSize)
                    .ToList ();

                return new NegotiationPagedDto {
                    PageIndex = request.PageIndex,
                        PageSize = request.PageSize,
                        PageCount = Convert.ToInt16 (totalPage),
                        Negotiations = NegotiationDto.From (pagedNegotiations)
                };

            } else {
                var count = Convert.ToDecimal (negotiations.Where (s => s.SalesAdvisorId == usuario.Id).Count ());
                var totalPage = Math.Ceiling (count / request.PageSize);
                if (request.PageIndex > totalPage) request.PageIndex = 1;
                if (request.PageIndex <= 0) request.PageIndex = 1;

                List<Negotiation> pagedNegotiations = negotiations
                    .Where (s => s.SalesAdvisorId == usuario.Id)
                    .OrderByDescending (s => s.TransactionDate)
                    .Skip (request.PageSize * (request.PageIndex - 1))
                    .Take (request.PageSize)
                    .ToList ();

                return new NegotiationPagedDto {
                    PageIndex = request.PageIndex,
                        PageSize = request.PageSize,
                        PageCount = Convert.ToInt16 (totalPage),
                        Negotiations = NegotiationDto.From (pagedNegotiations)
                };

            }
        }

        private IEnumerable<Negotiation> GetNegotiations () {
            return _context.Negotiations
                .Include (x => x.NegotiationContract).ThenInclude (x => x.BuilderCompany)
                .Include (c => c.HouseDesign)
                .Include (x => x.FinancialPrequalification).ThenInclude (x => x.FinancialApproval)
                .Include (c => c.Project)
                .Include (c => c.ContactType)
                .Include (c => c.FinancialInstitution)
                .Include (c => c.SalesAdvisor)
                .Include (c => c.NegotiationClients).ThenInclude (x => x.Client)
                .Include (c => c.SupplierService)
                .Include (c => c.Reserve).ThenInclude (x => x.Detalles)
                .Include (c => c.Downpayment).ThenInclude (x => x.Detalles)
                .Include (c => c.NegotiationPhases).ThenInclude (x => x.Negotiation)
                .Include (c => c.NegotiationPhases).ThenInclude (x => x.ServiceProcess).ThenInclude (x => x.SupplierService)
                .Include (c => c.NegotiationPhases).ThenInclude (x => x.NegotiationAttachments);
        }

        public async Task<NegotiationDto> UpdateAsync (NegotiationRequest request) {
            
            if (request == null) throw new ArgumentException (nameof (request));

            var negotiation = _context.Negotiations
                .Include (x => x.NegotiationContract).ThenInclude (x => x.BuilderCompany)
                .Include (c => c.HouseDesign)
                .Include (x => x.FinancialPrequalification).ThenInclude (x => x.FinancialApproval)
                .Include (c => c.NegotiationClients).ThenInclude (x => x.Client)
                .Include (n => n.Project)
                .Include (n => n.ContactType)
                .Include (n => n.FinancialInstitution)
                .Include (n => n.SalesAdvisor)
                .Include (n => n.NegotiationPhases)
                .Include (n => n.Reserve)
                .Include (n => n.Downpayment)
                .Include (n => n.SupplierService)
                .FirstOrDefault (s => s.Id == request.Id);

            if (negotiation == null) {
                return new NegotiationDto { ValidationErrorMessage = "Nivel de cliente inexistente" };
            }
            
            negotiation.UpdateComments(request.Comments);
            Project project = await _context.Projects.FirstOrDefaultAsync (p => p.Id == request.ProjectId);
            ContactType contactType = await _context.ContactTypes.FirstOrDefaultAsync (c => c.Id == request.ContactTypeId);
            FinancialInstitution financialInstitution = await _context.FinancialInstitutions
                .FirstOrDefaultAsync (o => o.Id == request.FinancialInstitutionId);
            User salesAdvisor = await _context.Users.FirstOrDefaultAsync (u => u.Id == request.SalesAdvisorId);

            Client client = await _context.Clients.FirstOrDefaultAsync (c => c.Id == request.Principal.ClientId);

            Client coDebtorClient = request.CoDebtor != null ?
                await _context.Clients.FirstOrDefaultAsync (c => c.Id == request.CoDebtor.ClientId) :
                null;

            Reserve reserve = await _context.Reserves.FirstOrDefaultAsync (r => r.Id == request.ReserveId);
            Downpayment downpayment = await _context.Downpayments.FirstOrDefaultAsync (d => d.Id == request.DownpaymentId);
            SupplierService supplierService = await _context.SupplierService
                .Include (s => s.ServiceProcesses)
                .FirstOrDefaultAsync (s => s.Id == request.SupplierServiceId);

            negotiation.UpdateNegotiationPhases (supplierService, request.User);

            NegotiationClient titularNegotiationClient = GetNegotiationClient (negotiation, client, request.Principal, true);
            NegotiationClient coDebtorNegotiationClient = GetNegotiationClient (negotiation, coDebtorClient, request.CoDebtor, false);

            FundType fundType = await _context.FundTypes.FirstOrDefaultAsync (f => f.Id == request.FinancialPrequalification.FundTypeId);

            HouseDesign houseDesign = await _context.HouseDesign.FirstOrDefaultAsync (p => p.Id == request.HouseDesignId);

            if (negotiation.FinancialPrequalification == null) {
                FinancialPrequalification financialPrequalification = CreateFinancialPrequalification (request.FinancialPrequalification, fundType);

                negotiation.SetFinancialPrequalification (financialPrequalification);
            } else {
                UpdateFinancialPrequalification (negotiation.FinancialPrequalification, request.FinancialPrequalification, fundType);
            }

            if (negotiation.NegotiationContract == null) {
                NegotiationContract negotiationContract = CreateNegotiationContract (request.NegotiationContract);

                negotiation.SetNegotiationContract (negotiationContract);
            } else {
                UpdateNegotiationContract (negotiation.NegotiationContract, request.NegotiationContract);
            }

            negotiation.SetCreationDate (request.CreationDate);
            negotiation.SetNegotiationStartDate (request.NegotiationStartDate);
            negotiation.SetTitularNegotiationClient (titularNegotiationClient);
            negotiation.SetCoDebtorNegotiationClient (coDebtorNegotiationClient);
            negotiation.SetProject (project);
            negotiation.SetHouseDesign (houseDesign);
            negotiation.SetContactType (contactType);
            negotiation.SetFinancialInstitution (financialInstitution);
            negotiation.SetCreditOfficer (request.CreditOfficer);
            negotiation.SetSalesAdvisor (salesAdvisor);
            negotiation.SetReserve (reserve);
            negotiation.SetDownpayment (downpayment);
            negotiation.SetSupplierService (supplierService);
            negotiation.SetAuditRecords (request.User);

            _context.Negotiations.Update (negotiation);
            _context.SaveChanges ();

            return NegotiationDto.From (negotiation);
        }

        private NegotiationClient GetNegotiationClient (
            Negotiation negotiation, Client client, NegotiationClientDto negotiationClient, bool titular) {
            if (client == null) {
                return null;
            }

            NegotiationClient currentNegotiationClient = negotiation.NegotiationClients
                .FirstOrDefault (n => n.Titular == titular);

            if (currentNegotiationClient == null) {
                return CreateNegotiationClient (client, negotiationClient, titular);
            }

            currentNegotiationClient.SetClient (client);
            currentNegotiationClient.SetTitular (true);
            currentNegotiationClient.SetRelationship (negotiationClient.Relationship);
            currentNegotiationClient.SetWorkplace (negotiationClient.Workplace);
            currentNegotiationClient.SetWorkStartDate (negotiationClient.WorkStartDate);
            currentNegotiationClient.SetHomeAddress (negotiationClient.HomeAddress);
            currentNegotiationClient.SetOwnHome (negotiationClient.OwnHome);
            currentNegotiationClient.SetFirstHome (negotiationClient.FirstHome);
            currentNegotiationClient.SetContributeToRap (negotiationClient.ContributeToRap);
            currentNegotiationClient.SetCurrency (negotiationClient.Currency);
            currentNegotiationClient.SetGrossMonthlyIncome (negotiationClient.GrossMonthlyIncome);
            currentNegotiationClient.SetPreQualify (negotiationClient.PreQualify);
            currentNegotiationClient.SetPrequalificationDate (negotiationClient.PrequalificationDate);
            currentNegotiationClient.SetNextPrequalificationDate (negotiationClient.NextPrequalificationDate);
            currentNegotiationClient.SetPrequalificationAmount (negotiationClient.PrequalificationAmount);
            currentNegotiationClient.SetHasDebts (negotiationClient.HasDebts);
            currentNegotiationClient.SetDebtsAmount (negotiationClient.DebtsAmount);

            return currentNegotiationClient;
        }

        public NegotiationDto ValidateNewNegotiationCreation (NegotiationRequest negotiationRequest) {
            if (negotiationRequest == null) throw new ArgumentNullException (nameof (negotiationRequest));
            if (negotiationRequest.Principal == null) throw new ArgumentException (nameof (negotiationRequest.Principal));

            NegotiationDto negotiationValidation = ValidateClient (negotiationRequest.Principal.Id);

            if (!string.IsNullOrWhiteSpace (negotiationValidation?.ValidationErrorMessage)) {
                return negotiationValidation;
            }

            if (negotiationRequest.CoDebtor == null) {
                return new NegotiationDto ();
            }

            return ValidateClient (negotiationRequest.CoDebtor.Id);
        }

        private NegotiationDto ValidateClient (int id) {
            Client client = _context.Clients.FirstOrDefault (s => s.Id == id);

            if (client == null) {
                return new NegotiationDto { ValidationErrorMessage = "Cliente no existe en registro de clientes" };
            }

            Negotiation negotiation = _context.Negotiations
                .Include (c => c.Project)
                .FirstOrDefault (s => s.NegotiationClients.Any (c => c.ClientId == client.Id));

            if (negotiation != null) {
                return new NegotiationDto {
                ValidationErrorMessage = "Cliente ya cuenta con un tramite anterior con codigo: #" +
                negotiation.Id +
                ", en el proyecto: " + negotiation.Project?.Id +
                "-" +
                negotiation.Project?.Name + ", Verifica que no se trate del mismo negocio!"
                };
            }

            return new NegotiationDto ();
        }

        public SumClientByFinancialInstitutionDto GetStatisticsNegotiationsFinancialInstitutions () {
            var negotiations = _context.Negotiations
                .Include (c => c.FinancialInstitution);

            var data = (from row in negotiations group row by new { row.FinancialInstitutionId, row.FinancialInstitution.Name } into g select new SumClientByFinancialInstitution {
                FinancialInstitutionId = g.Key.FinancialInstitutionId,
                    FinancialInstitution = g.Key.Name,
                    SumClients = g.Count ()
            }).ToList ();

            var resumeData = new SumClientByFinancialInstitutionDto {
                FinancialInstitutions = new List<string> (),
                CountClients = new List<int> ()
            };
            resumeData.FinancialInstitutions = data.Select (s => s.FinancialInstitution).ToList ();
            resumeData.CountClients = data.Select (s => s.SumClients).ToList ();

            return resumeData;
        }

        public List<NegotiationDto> GetGeneralReport (NegotiationRequest request) {
            if (request == null) throw new ArgumentException (nameof (request));
            if (string.IsNullOrEmpty (request.User)) return new List<NegotiationDto> ();
            var usuario = _context.Users.FirstOrDefault (s => s.UserId == request.User);
            if (usuario == null) return new List<NegotiationDto> ();
            var usuarioRoles = _context.UserRol.FirstOrDefault (s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<NegotiationDto> ();

            if (request == null) throw new ArgumentNullException (nameof (request));

            if (usuarioRoles.RolId != 3) {
                
                if (request.ReportType == "1") {
                    var listaNegocios = new List<NegotiationDto> ();
                    var negotiations = _context.Negotiations
                        .Where (s => s.CreationDate >= request.StartDate && s.CreationDate <= request.FinalDate)
                        .Include (x => x.NegotiationContract).ThenInclude (x => x.BuilderCompany)
                        .Include (c => c.HouseDesign)
                        .Include (x => x.FinancialPrequalification).ThenInclude (x => x.FinancialApproval)
                        .Include (c => c.NegotiationClients).ThenInclude (x => x.Client)
                        .Include (n => n.Project)
                        .Include (n => n.ContactType)
                        .Include (n => n.FinancialInstitution)
                        .Include (n => n.SalesAdvisor)
                        .Include (n => n.NegotiationPhases)
                        .Include (n => n.Reserve)
                        .Include (n => n.Downpayment)
                        .Include (n => n.SupplierService)
                        .ToList ();

                    foreach (var item in negotiations) {
                        var newItem = NegotiationDto.From (item);

                        var r = item.NegotiationPhases
                            .Where (s => s.Status == "PENDIENTE")
                            .OrderBy (s => s.Order)
                            .FirstOrDefault ();
                        if (r == null) continue; 
                        var process = _context.ServiceProcesses.FirstOrDefault (s => s.Id == r.ProcessId);
                        if (process != null) {
                            newItem.ActualPhase = process.Name;
                        }
                        listaNegocios.Add (newItem);
                    }

                    return listaNegocios;
                }

                if (request.ReportType == "2") {
                    var listaNegocios = new List<NegotiationDto> ();
                    if (request == null) throw new ArgumentException (nameof (request));
                    if (string.IsNullOrEmpty (request.User)) return new List<NegotiationDto> ();
                    var usuarioConsultado = _context.Users.FirstOrDefault (s => s.UserId == request.UsuarioConsultado);
                    if (usuario == null) return new List<NegotiationDto> ();

                    var negotiations = _context.Negotiations
                        .Where (s => s.CreationDate >= request.StartDate && s.CreationDate <= request.FinalDate &&
                            s.SalesAdvisorId == usuarioConsultado.Id)
                        .Include (x => x.NegotiationContract).ThenInclude (x => x.BuilderCompany)
                        .Include (c => c.HouseDesign)
                        .Include (x => x.FinancialPrequalification).ThenInclude (x => x.FinancialApproval)
                        .Include (c => c.NegotiationClients).ThenInclude (x => x.Client)
                        .Include (n => n.Project)
                        .Include (n => n.ContactType)
                        .Include (n => n.FinancialInstitution)
                        .Include (n => n.SalesAdvisor)
                        .Include (n => n.NegotiationPhases)
                        .Include (n => n.Reserve)
                        .Include (n => n.Downpayment)
                        .Include (n => n.SupplierService)
                        .ToList ();

                    foreach (var item in negotiations) {
                        var newItem = NegotiationDto.From (item);

                        var r = item.NegotiationPhases
                            .Where (s => s.Status == "PENDIENTE")
                            .OrderBy (s => s.Order)
                            .FirstOrDefault ();
                        if(r == null) continue;
                        var process = _context .ServiceProcesses.FirstOrDefault (s => s.Id == r.ProcessId);
                        if (process != null) {
                            newItem.ActualPhase = process.Name;
                        }

                        listaNegocios.Add (newItem);

                    }

                    return listaNegocios;
                }
            }

            return new List<NegotiationDto> ();
        }

        public NegotiationProcessReportDto GetStaticsNegotiaions () {

            var negotiationsPending = _context.NegotiationPhases.Where (s => s.Status == "PENDIENTE");
            var negotiations = negotiationsPending.Select (s => s.NegotiationId).Distinct ().ToList ();

            var negotiationsClose = _context.NegotiationPhases.Where (s => s.Status != "PENDIENTE");
            var negotiationsCloseList = negotiationsClose.Where (s => !negotiations.Contains (s.NegotiationId));
            var closeList = negotiationsCloseList.Select (s => s.NegotiationId).Distinct ();

            return new NegotiationProcessReportDto {
                Opens = negotiations.Count (),
                    Close = closeList.Count ()
            };
        }

        public NegotiationDto CloseNegotiationStatus (NegotiationRequest request) {
            if (request == null) throw new ArgumentNullException (nameof (request));

            var row = _context.Negotiations.FirstOrDefault (s => s.Id == request.Id);
            if (row == null) return new NegotiationDto ();

            row.UpdateStatus ("CLOSE");
            row.SetAuditRecords (request.User);
            _context.SaveChanges ();
            return NegotiationDto.From (row);
        }

        public SumNegotationsBySalesAdvisor GetDataNegotiationBySalesAdvisor () {

            var negotiations = _context.Negotiations.Include (n => n.SalesAdvisor);
            var negotiationsBySalesAdvisor = (from row in negotiations

                group row by new { row.SalesAdvisorId, row.SalesAdvisor.UserId } into g select new NegotiationsBySalesAdvisor {
                    SalesAdvisor = g.Key.UserId,
                        CountNegotiations = g.Count ()
                }).ToList ();

            var resumeData = new SumNegotationsBySalesAdvisor {
                SalesAdvisor = new List<string> (),
                CountNegotiations = new List<int> ()
            };
            resumeData.SalesAdvisor = negotiationsBySalesAdvisor.Select (s => s.SalesAdvisor).ToList ();
            resumeData.CountNegotiations = negotiationsBySalesAdvisor.Select (s => s.CountNegotiations).ToList ();

            return resumeData;

        }

        public List<NegotiationDto> GetNegotiationsClosed (NegotiationRequest request) {

            if (request == null) throw new ArgumentException (nameof (request));
            if (string.IsNullOrEmpty (request.User)) return new List<NegotiationDto> ();
            var usuario = _context.Users.FirstOrDefault (s => s.UserId == request.User);
            if (usuario == null) return new List<NegotiationDto> ();
            var usuarioRoles = _context.UserRol.FirstOrDefault (s => s.UserId == usuario.UserId);
            if (usuarioRoles == null) return new List<NegotiationDto> ();
            var fechaFinal = request.FinalDate.AddDays (1);
            if (request == null) throw new ArgumentNullException (nameof (request));

            if (usuarioRoles.RolId != 3) {
                if (request.ReportType == "1") {
                    var listaNegocios = new List<NegotiationDto> ();
                    var negotiations = _context.Negotiations
                        .Where (s => s.CreationDate >= request.StartDate && s.CreationDate <= fechaFinal)
                        .Include (x => x.NegotiationContract).ThenInclude (x => x.BuilderCompany)
                        .Include (c => c.HouseDesign)
                        .Include (x => x.FinancialPrequalification).ThenInclude (x => x.FinancialApproval)
                        .Include (c => c.NegotiationClients).ThenInclude (x => x.Client)
                        .Include (n => n.Project)
                        .Include (n => n.ContactType)
                        .Include (n => n.FinancialInstitution)
                        .Include (n => n.SalesAdvisor)
                        .Include (n => n.NegotiationPhases)
                        .Include (n => n.Reserve)
                        .Include (n => n.Downpayment)
                        .Include (n => n.SupplierService)
                        .ToList ();

                    foreach (var item in negotiations) {

                        var ultimoProceso = item.NegotiationPhases.OrderByDescending (s => s.Order).FirstOrDefault ();
                        if (ultimoProceso == null) continue;
                        var itemClosed = item.NegotiationPhases.FirstOrDefault (s => s.Id == ultimoProceso.Id && s.Status != "PENDIENTE");
                        if (itemClosed == null) continue;
                        var process = _context.ServiceProcesses.FirstOrDefault (s => s.Id == itemClosed.ProcessId);
                        if (itemClosed != null) {
                            var newItem = NegotiationDto.From (item);
                            if (process != null) {
                                newItem.ActualPhase = process.Name;
                                newItem.ClosedDate = itemClosed.DateComplete == null ? itemClosed.CreationDate : itemClosed.DateComplete;

                            }
                            listaNegocios.Add (newItem);
                        }

                    }

                    return listaNegocios;
                }

                if (request.ReportType == "2") {
                    var listaNegocios = new List<NegotiationDto> ();
                    if (request == null) throw new ArgumentException (nameof (request));
                    if (string.IsNullOrEmpty (request.User)) return new List<NegotiationDto> ();
                    var usuarioConsultado = _context.Users.FirstOrDefault (s => s.UserId == request.UsuarioConsultado);
                    if (usuario == null) return new List<NegotiationDto> ();

                    var negotiations = _context.Negotiations
                        .Where (s => s.CreationDate >= request.StartDate && s.CreationDate <= fechaFinal &&
                            s.SalesAdvisorId == usuarioConsultado.Id)
                        .Include (x => x.NegotiationContract).ThenInclude (x => x.BuilderCompany)
                        .Include (c => c.HouseDesign)
                        .Include (x => x.FinancialPrequalification).ThenInclude (x => x.FinancialApproval)
                        .Include (c => c.NegotiationClients).ThenInclude (x => x.Client)
                        .Include (n => n.Project)
                        .Include (n => n.ContactType)
                        .Include (n => n.FinancialInstitution)
                        .Include (n => n.SalesAdvisor)
                        .Include (n => n.NegotiationPhases)
                        .Include (n => n.Reserve)
                        .Include (n => n.Downpayment)
                        .Include (n => n.SupplierService)
                        .ToList ();

                    foreach (var item in negotiations) {

                        var ultimoProceso = item.NegotiationPhases.OrderByDescending (s => s.Order).FirstOrDefault ();
                        if (ultimoProceso == null) continue;
                        var itemClosed = item.NegotiationPhases.FirstOrDefault (s => s.Id == ultimoProceso.Id && s.Status != "PENDIENTE");
                        if (itemClosed == null) continue;
                        var process = _context.ServiceProcesses.FirstOrDefault (s => s.Id == itemClosed.ProcessId);

                        if (itemClosed != null) {
                            var newItem = NegotiationDto.From (item);
                            if (process != null) {
                                newItem.ActualPhase = process.Name;
                                newItem.ClosedDate = itemClosed.DateComplete == null ? itemClosed.CreationDate : itemClosed.DateComplete;
                            }
                            listaNegocios.Add (newItem);
                        }

                    }

                    return listaNegocios;

                }
            }

            var listaNegociosPorUsuario = new List<NegotiationDto> ();
            var negotiationsPorusuario = _context.Negotiations
                .Where (s => s.CreationDate >= request.StartDate && s.CreationDate <= fechaFinal && s.SalesAdvisorId == usuario.Id)
                .Include (x => x.NegotiationContract).ThenInclude (x => x.BuilderCompany)
                .Include (c => c.HouseDesign)
                .Include (x => x.FinancialPrequalification).ThenInclude (x => x.FinancialApproval)
                .Include (c => c.NegotiationClients).ThenInclude (x => x.Client)
                .Include (n => n.Project)
                .Include (n => n.ContactType)
                .Include (n => n.FinancialInstitution)
                .Include (n => n.SalesAdvisor)
                .Include (n => n.NegotiationPhases)
                .Include (n => n.Reserve)
                .Include (n => n.Downpayment)
                .Include (n => n.SupplierService)
                .ToList ();

            foreach (var item in negotiationsPorusuario) {

                var ultimoProceso = item.NegotiationPhases.OrderByDescending (s => s.Order).FirstOrDefault ();
                if (ultimoProceso == null) continue;
                var itemClosed = item.NegotiationPhases.FirstOrDefault (s => s.Id == ultimoProceso.Id && s.Status != "PENDIENTE");
                if (itemClosed == null) continue;
                var process = _context.ServiceProcesses.FirstOrDefault (s => s.Id == itemClosed.ProcessId);
                if (itemClosed != null) {
                    var newItem = NegotiationDto.From (item);
                    if (process != null) {
                        newItem.ActualPhase = process.Name;
                        newItem.ClosedDate = itemClosed.DateComplete == null ? itemClosed.CreationDate : itemClosed.DateComplete;
                    }
                    listaNegociosPorUsuario.Add (newItem);
                }

            }

            return listaNegociosPorUsuario;
        }

        public void Dispose () {
            if (_context != null) _context.Dispose ();
            if (_negotiationPhaseAppService != null) _negotiationPhaseAppService.Dispose ();
        }

    }
}