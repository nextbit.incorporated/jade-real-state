using System;
using System.Collections.Generic;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Downpayments;
using Nextbit.JadeRealState.Services.Features.FinancialPrequalifications;
using Nextbit.JadeRealState.Services.Features.NegotiationClients;
using Nextbit.JadeRealState.Services.Features.NegotiationContracts;
using Nextbit.JadeRealState.Services.Features.NegotiationPhases;
using Nextbit.JadeRealState.Services.Features.Reservas;


namespace Nextbit.JadeRealState.Services.Features.Negotiations
{
    public class NegotiationDto : ResponseBase
    {
        public DateTime NegotiationStartDate { get; set; }
        public string CreditOfficer { get; set; }
        public string Comments { get; set; }
        public int? ReserveId { get; set; }
        public int? DownpaymentId { get; set; }

        public int? ProjectId { get; set; }
        public string Project { get; set; }

        public int? HouseDesignId { get; set; }
        public string HouseDesign { get; set; }
        public string HouseSpecifications { get; set; }

        public int? ContactTypeId { get; set; }
        public string ContactType { get; set; }
        public int? FinancialInstitutionId { get; set; }
        public string FinancialInstitution { get; set; }
        public int? SalesAdvisorId { get; set; }
        public string SalesAdvisor { get; set; }
        public int? SupplierServiceId { get; set; }
        public string SupplierService { get; set; }
        public List<NegotiationPhaseDto> NegotiationPhases { get; set; } = new List<NegotiationPhaseDto>();
        public ReserveDTO Reserve { get; set; }
        public DownpaymentDto Downpayment { get; set; }
        public NegotiationClientDto Principal { get; set; }
        public NegotiationClientDto CoDebtor { get; set; }
        public FinancialPrequalificationDto FinancialPrequalification { get; set; }
        public NegotiationContractDto NegotiationContract { get; set; }

        public DateTime? ClosedDate { get; set; }
        
        
        public string ActualPhase { get; set; }

        public string NegotiationStatus { get; set; }

        internal static NegotiationDto From(Negotiation negotiation)
        {
            if (negotiation == null) return new NegotiationDto();

            return new NegotiationDto
            {
                Id = negotiation.Id,
                CreationDate = negotiation.CreationDate,
                Comments = negotiation.Comments,
                NegotiationStartDate = negotiation.NegotiationStartDate,
                ReserveId = negotiation.ReserveId,
                DownpaymentId = negotiation.DownpaymentId,
                Principal = NegotiationClientDto.From(negotiation, true),
                CoDebtor = NegotiationClientDto.From(negotiation, false),
                ProjectId = negotiation.Project?.Id,
                Project = negotiation.Project?.Name,
                HouseDesignId = negotiation.HouseDesign?.Id,
                HouseDesign = negotiation.HouseDesign?.Name,
                HouseSpecifications = negotiation.HouseDesign?.HouseSpecifications,
                ContactTypeId = negotiation.ContactType?.Id,
                ContactType = negotiation.ContactType?.Name,
                FinancialInstitutionId = negotiation.FinancialInstitution?.Id,
                FinancialInstitution = negotiation.FinancialInstitution?.Name,
                SalesAdvisorId = negotiation.SalesAdvisor?.Id,
                SalesAdvisor = $"{negotiation.SalesAdvisor?.FirstName} {negotiation.SalesAdvisor?.LastName}",
                Reserve = ReserveDTO.From(negotiation.Reserve),
                Downpayment = DownpaymentDto.From(negotiation.Downpayment),
                NegotiationPhases = NegotiationPhaseDto.From(negotiation.NegotiationPhases),
                SupplierServiceId = negotiation.SupplierService?.Id,
                SupplierService = negotiation.SupplierService?.Name,
                CreditOfficer = negotiation.CreditOfficer,
                FinancialPrequalification = FinancialPrequalificationDto.From(negotiation.FinancialPrequalification),
                NegotiationContract = NegotiationContractDto.From(negotiation.NegotiationContract),
                NegotiationStatus = negotiation.NegotiationStatus ?? "NA",
                ActualPhase = "",

            };
        }

        internal static List<NegotiationDto> From(List<Negotiation> pagedNegotiations)
        {
            return (from qry in pagedNegotiations select From(qry)).ToList();
        }
    }
}