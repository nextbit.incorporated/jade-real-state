using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.Downpayments;
using Nextbit.JadeRealState.Services.Features.FinancialInstitutions;
using Nextbit.JadeRealState.Services.Features.FinancialPrequalifications;
using Nextbit.JadeRealState.Services.Features.HouseDesigns;
using Nextbit.JadeRealState.Services.Features.NegotiationClients;
using Nextbit.JadeRealState.Services.Features.NegotiationContracts;
using Nextbit.JadeRealState.Services.Features.NegotiationPhases;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.Reservas;
using Nextbit.JadeRealState.Services.Features.SupplierServices;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.Features.Negotiations
{
    [Table("Negotiations")]
    public sealed class Negotiation : Entity
    {
        private Negotiation()
        {
            NegotiationPhases = new HashSet<NegotiationPhase>();
            NegotiationClients = new HashSet<NegotiationClient>();
        }

        public DateTime NegotiationStartDate { get; private set; }

        public int? ReserveId { get; private set; }
        public int? DownpaymentId { get; private set; }
        public string CreditOfficer { get; private set; }
        public int? SalesAdvisorId { get; private set; }
        public int? FinancialInstitutionId { get; private set; }
        public int? ContactTypeId { get; private set; }
        public int? ProjectId { get; private set; }
        public int? HouseDesignId { get; private set; }
        public int? SupplierServiceId { get; private set; }
        public int? FinancialPrequalificationId { get; private set; }
        public string Comments { get; private set; }
        public string NegotiationStatus { get; private set; }


        public Project Project { get; set; }
        public HouseDesign HouseDesign { get; set; }
        public ContactType ContactType { get; set; }
        public FinancialInstitution FinancialInstitution { get; set; }
        public User SalesAdvisor { get; set; }
        public SupplierService SupplierService { get; set; }
        public FinancialPrequalification FinancialPrequalification { get; set; }
        public NegotiationContract NegotiationContract { get; set; }


        public void UpdateComments(string comment)
        {
            Comments = comment;
        }
        public void UpdateStatus(string status)
        {
            NegotiationStatus = status;
        }

        public ICollection<NegotiationClient> NegotiationClients
        {
            get { return _negotiationClients ?? (_negotiationClients = new HashSet<NegotiationClient>()); }
            private set { _negotiationClients = value; }
        }
        private ICollection<NegotiationClient> _negotiationClients;

        public ICollection<NegotiationPhase> NegotiationPhases
        {
            get { return _negotiationPhases ?? (_negotiationPhases = new HashSet<NegotiationPhase>()); }
            private set { _negotiationPhases = value; }
        }
        private ICollection<NegotiationPhase> _negotiationPhases;

        public Reserve Reserve { get; set; }
        public Downpayment Downpayment { get; set; }


        public class Builder
        {
            private readonly Negotiation _negotiation = new Negotiation();

            internal Builder WithCreationDate(DateTime creationDate)
            {
                _negotiation.CreationDate = creationDate;
                return this;
            }
            internal Builder WithNegotiationStartDate(DateTime negotiationStartDate)
            {
                _negotiation.NegotiationStartDate = negotiationStartDate;
                return this;
            }

            internal Builder WithReserve(Reserve reserve)
            {
                _negotiation.SetReserve(reserve);
                return this;
            }

            internal Builder WithDownpayment(Downpayment downpayment)
            {
                _negotiation.SetDownpayment(downpayment);
                return this;
            }

            internal Builder WithSalesAdvisor(User salesAdvisor)
            {
                _negotiation.SetSalesAdvisor(salesAdvisor);
                return this;
            }

            internal Builder WithfinancialInstitution(FinancialInstitution financialInstitution)
            {
                _negotiation.SetFinancialInstitution(financialInstitution);
                return this;
            }

            internal Builder WithContactType(ContactType contactType)
            {
                _negotiation.SetContactType(contactType);
                return this;
            }

            internal Builder WithFinancialPrequalification(FinancialPrequalification financialPrequalification)
            {
                _negotiation.SetFinancialPrequalification(financialPrequalification);

                return this;
            }

            internal Builder WithNegotiationContract(NegotiationContract negotiationContract)
            {
                _negotiation.SetNegotiationContract(negotiationContract);

                return this;
            }

            internal Builder WithProject(Project project)
            {
                _negotiation.SetProject(project);
                return this;
            }

            internal Builder WithSupplierService(SupplierService supplierService)
            {
                _negotiation.SetSupplierService(supplierService);
                return this;
            }
            internal Builder WithClient(NegotiationClient negotiationClient)
            {
                _negotiation.SetTitularNegotiationClient(negotiationClient);
                return this;
            }

            public Builder WithComments(string comments)
            {
                _negotiation.Comments = comments;
                return this;
            }

            public Builder WithCreditOfficer(string creditOfficer)
            {
                _negotiation.CreditOfficer = creditOfficer;
                return this;
            }
            public Builder WithNegotationStatus(string status)
            {
                _negotiation.NegotiationStatus = status;
                return this;
            }

            internal Builder WithCoDebtorClient(NegotiationClient coDebtorNegotiationClient)
            {
                _negotiation.SetCoDebtorNegotiationClient(coDebtorNegotiationClient);

                return this;
            }

            public Builder WithAuditFields(string user)
            {
                _negotiation.CrudOperation = "Added";
                _negotiation.TransactionDate = DateTime.Now;
                _negotiation.TransactionType = "New";
                _negotiation.ModifiedBy = user ?? "Service";
                _negotiation.TransactionUId = Guid.NewGuid();

                return this;
            }

            public Negotiation Build()
            {
                return _negotiation;
            }
        }

        internal void SetNegotiationContract(NegotiationContract negotiationContract)
        {
            NegotiationContract = negotiationContract;

            if (negotiationContract != null)
            {
                negotiationContract.Negotiation = this;
            }
        }

        internal void SetFinancialPrequalification(FinancialPrequalification financialPrequalification)
        {
            FinancialPrequalification = financialPrequalification;

            if (financialPrequalification != null)
            {
                financialPrequalification.Negotiation = this;
            }
        }

        internal NegotiationClient GetNegotiationClient(bool titular)
        {
            NegotiationClient negotiationClient = NegotiationClients.FirstOrDefault(n => n.Titular == titular);

            return negotiationClient;
        }

        internal void CreateNegotiationPhases(SupplierService supplierService, string user)
        {
            if (supplierService != null)
            {

                var fechaCita = DateTime.Now;

                foreach (var item in supplierService.ServiceProcesses.OrderBy(s => s.Order))
                {
                    NegotiationPhase newPhase = new NegotiationPhase.Builder()
                    .WithProcessId(item.Id)
                    .WithDate(fechaCita)
                    .WithStatus("PENDIENTE")
                    .WithOrder(item.Order)
                    .WithAuditFields(user)
                    .Build();

                    NegotiationPhases.Add(newPhase);
                }

            }
        }

        internal void UpdateNegotiationPhases(SupplierService supplierService, string user)
        {
            if (supplierService != null)
            {
                if (SupplierService == null || (SupplierService.Id != supplierService.Id))
                {
                    NegotiationPhases.Clear();
                    SetSupplierService(supplierService);
                }

                if (!NegotiationPhases.Any())
                {
                    CreateNegotiationPhases(supplierService, user);
                }

            }
        }

        internal void SetAuditRecords(string user)
        {
            CrudOperation = "Update";
            TransactionDate = DateTime.Now;
            TransactionType = "Modified";
            ModifiedBy = user ?? "Service";
            TransactionUId = Guid.NewGuid();
        }

        internal void SetSupplierService(SupplierService supplierService)
        {
            SupplierServiceId = supplierService?.Id;
            SupplierService = supplierService;
        }

        internal void SetDownpayment(Downpayment downpayment)
        {
            DownpaymentId = downpayment?.Id;
            Downpayment = downpayment;
        }

        internal void SetReserve(Reserve reserve)
        {
            ReserveId = reserve?.Id;
            Reserve = reserve;
        }

        internal void SetSalesAdvisor(User salesAdvisor)
        {
            SalesAdvisorId = salesAdvisor?.Id;
            SalesAdvisor = salesAdvisor;
        }

        internal void SetFinancialInstitution(FinancialInstitution financialInstitution)
        {
            FinancialInstitutionId = financialInstitution?.Id;
            FinancialInstitution = financialInstitution;
        }

        internal void SetContactType(ContactType contactType)
        {
            ContactTypeId = contactType?.Id;
            ContactType = contactType;
        }

        internal void SetProject(Project project)
        {
            ProjectId = project?.Id;
            Project = project;
        }

        internal void SetHouseDesign(HouseDesign houseDesign)
        {
            HouseDesignId = houseDesign?.Id;
            HouseDesign = houseDesign;
        }

        internal void SetCoDebtorNegotiationClient(NegotiationClient negotiationClient)
        {
            if (negotiationClient == null)
            {
                RemoveCoDebtor();
            }
            else
            {
                NegotiationClient currentNegotiationClient = NegotiationClients
                    .FirstOrDefault(n => n.ClientId == negotiationClient.Id);

                if (currentNegotiationClient == null)
                {
                    negotiationClient.SetTitular(false);
                    NegotiationClients.Add(negotiationClient);
                }
            }


        }

        private void RemoveCoDebtor()
        {
            List<NegotiationClient> negotiationClients = NegotiationClients
                .Where(n => !n.Titular).ToList();

            foreach (NegotiationClient negotiationClient in negotiationClients)
            {
                NegotiationClients.Remove(negotiationClient);
            }
        }

        internal void SetTitularNegotiationClient(NegotiationClient titularNegotiationClient)
        {
            foreach (NegotiationClient negotiationClient in NegotiationClients)
            {
                negotiationClient.SetTitular(false);
            }

            NegotiationClient currentNegotiationClient = NegotiationClients
                .FirstOrDefault(n => n.ClientId == titularNegotiationClient.Id);

            if (currentNegotiationClient != null)
            {
                currentNegotiationClient.SetTitular(true);
                currentNegotiationClient.Negotiation = this;
            }
            else
            {
                titularNegotiationClient.SetTitular(true);
                titularNegotiationClient.Negotiation = this;

                NegotiationClients.Add(titularNegotiationClient);
            }
        }

        internal void SetCreditOfficer(string creditOfficer)
        {
            CreditOfficer = creditOfficer;
        }

        internal void SetCreationDate(DateTime creditOfficer)
        {
            CreationDate = creditOfficer;
        }

        internal void SetNegotiationStartDate(DateTime negotiationStartDate)
        {
            NegotiationStartDate = negotiationStartDate;
        }


    }
}