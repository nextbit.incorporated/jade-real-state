using System.Collections.Generic;
using Nextbit.JadeRealState.Services.Core;

namespace Nextbit.JadeRealState.Services.Features.Negotiations
{
    public class NegotiationPagedDto : RequestBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public List<NegotiationDto> Negotiations { get; set; }
    }
}