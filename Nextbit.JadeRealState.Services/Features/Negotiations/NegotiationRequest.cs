using System;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.FinancialPrequalifications;
using Nextbit.JadeRealState.Services.Features.NegotiationClients;
using Nextbit.JadeRealState.Services.Features.NegotiationContracts;

namespace Nextbit.JadeRealState.Services.Features.Negotiations
{
    public class NegotiationRequest : RequestBase
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime NegotiationStartDate { get; set; }
        public int? FinancialInstitutionId { get; set; }
        public int? ProjectId { get; set; }
        public int? HouseDesignId { get; set; }
        public int? ContactTypeId { get; set; }
        public int? SalesAdvisorId { get; set; }
        public int? SupplierServiceId { get; set; }
        public string CreditOfficer { get; set; }
        public int? ReserveId { get; set; }
        public int? DownpaymentId { get; set; }
        public string Comments { get; set; }
        
        
        public NegotiationClientDto Principal { get; set; }
        public NegotiationClientDto CoDebtor { get; set; }
        public FinancialPrequalificationDto FinancialPrequalification { get; set; }
        public NegotiationContractDto NegotiationContract { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime FinalDate { get; set; }
        public string ReportType { get; set; } 
        public string UsuarioConsultado { get; set; }
        
        
    }

}