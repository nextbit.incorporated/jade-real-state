using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nextbit.JadeRealState.Services.Features.Reports.Dashboard;
using Nextbit.JadeRealState.Services.Features.Reports.Negotiations;

namespace Nextbit.JadeRealState.Services.Features.Negotiations {
    public interface INegotiationAppService : IDisposable {
        NegotiationPagedDto GetPaged (NegotiationPagedRequest request);
        Task<NegotiationDto> CreateAsync (NegotiationRequest request);
        Task<NegotiationDto> UpdateAsync (NegotiationRequest request);
        SumClientByFinancialInstitutionDto GetStatisticsNegotiationsFinancialInstitutions ();
        NegotiationDto ValidateNewNegotiationCreation (NegotiationRequest requets);
        NegotiationDto CloseNegotiationStatus (NegotiationRequest request);
        NegotiationProcessReportDto GetStaticsNegotiaions ();
        SumNegotationsBySalesAdvisor GetDataNegotiationBySalesAdvisor ();
        List<NegotiationDto> GetGeneralReport (NegotiationRequest request);
        List<NegotiationDto> GetNegotiationsClosed (NegotiationRequest request);
        string Delete (int id);
    }
}