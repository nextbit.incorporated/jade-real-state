﻿using System;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Nextbit.JadeRealState.Services.DataContexts;
using Nextbit.JadeRealState.Services.Features.Users;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.Builders;
using Nextbit.JadeRealState.Services.Features.HouseDesigns;
using Nextbit.JadeRealState.Services.Features.SupplierServices;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.Origins;
using Nextbit.JadeRealState.Services.Features.Prospects;
using Nextbit.JadeRealState.Services.Core.Data;
using Nextbit.JadeRealState.Services.Features.FinancialInstitutions;
using Nextbit.JadeRealState.Services.Features.ClientsTracking;
using Nextbit.JadeRealState.Services.Features.Reservas;
using Nextbit.JadeRealState.Services.Features.ClientsLevel;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.NegotiationPhases;
using Nextbit.JadeRealState.Services.Features.Downpayments;
using Nextbit.JadeRealState.Services.Features.Alarms;
using Microsoft.OpenApi.Models;
using Microsoft.Extensions.Hosting;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.NegotiationAttachments;
using Nextbit.JadeRealState.Services.Features.ClientTransactions;
using Nextbit.JadeRealState.Services.Features.Reports.ClientsByBank;
using Nextbit.JadeRealState.Services.Features.MeetingCategories;
using Nextbit.JadeRealState.Services.Features.GeneralInfo;
using Nextbit.JadeRealState.Services.Features.ApplicationOptions;
using Nextbit.JadeRealState.Services.Features.Reports.ClientsAlarms;
using Nextbit.JadeRealState.Services.Features.Reports.ClientsByProjectReport;
using Nextbit.JadeRealState.Services.Features.Reports.ProspectReport;
using Nextbit.JadeRealState.Services.Features.Operations;

namespace Nextbit.JadeRealState.Services
{
    public class Startup
    {
        private const string AllowAllOriginsPolicy = "AllowAllOriginsPolicy";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Add service and create Policy with options
            services.AddCors(options =>
            {
                options.AddPolicy(AllowAllOriginsPolicy,
                    builder =>
                    {
                        builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                    });
            });


            services.AddControllers();

            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });


            services.AddDbContext<RealStateContext>(
              options => options.UseMySql(Configuration.GetConnectionString("DefaultConnection"), mySqlOptionsAction: mySqlOptions => { mySqlOptions.EnableRetryOnFailure(); })
                  );

            services.AddMvc();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddScoped<IUserDomainService, UserDomainService>();
            services.AddScoped<IUserAppService, UserAppService>();
            services.AddScoped<IRoleAppService, RoleAppService>();
            services.AddScoped<IUserRolAppService, UserRolAppService>();
            services.AddScoped<IRolePermissionAppService, RolePermisionAppService>();
            services.AddScoped<IProjectDomainService, ProjectDomainService>();
            services.AddScoped<IProjectsAppService, ProjectsAppService>();
            services.AddScoped<IBuilderCompanyDomainService, BuilderCompanyDomainService>();
            services.AddScoped<IBuilderCompanyAppService, BuilderCompanyAppService>();
            services.AddScoped<IHouseDesignDomainService, HouseDesignDomainService>();
            services.AddScoped<IHouseDesignAppService, HouseDesignAppService>();
            services.AddScoped<ISupplierServiceDomainService, SupplierServiceDomainService>();
            services.AddScoped<ISupplierServiceAppService, SupplierServiceAppService>();
            services.AddScoped<IOriginDomainService, OriginDomainService>();
            services.AddScoped<IOriginAppService, OriginAppService>();
            services.AddScoped<IProspectDomainService, ProspectDomainService>();
            services.AddScoped<IProspectAppService, ProspectAppService>();
            services.AddScoped<ClientsAppService>();
            services.AddScoped<ContactTypesAppService>();
            services.AddScoped<IContactTypeDomainService, ContactTypeDomainService>();
            services.AddScoped<IContactTypeAppService, ContactTypeAppService>();
            services.AddScoped<IFinancialInstitutionDomainService, FinancialInstitutionDomainService>();
            services.AddScoped<IFinancialInstitutionAppService, FinancialInstitutionAppService>();
            services.AddScoped<IClientTrackingDomainService, ClientTrackingDomainService>();
            services.AddScoped<IClientTrackingAppService, ClientTrackingAppService>();
            services.AddScoped<IReserveDomainService, ReserveDomainService>();
            services.AddScoped<IReserveAppService, ReserveAppService>();
            services.AddScoped<IClientLevelDomainService, ClientLevelDomainService>();
            services.AddScoped<IClientLevelAppService, ClientLevelAppService>();
            services.AddScoped<INegotiationAppService, NegotiationAppService>();
            services.AddScoped<INegotiationPhaseDomainService, NegotiationPhaseDomainService>();
            services.AddScoped<INegotiationPhaseAppService, NegotiationPhaseAppService>();
            services.AddScoped<IReserveDetailDomainService, ReserveDetailDomainService>();
            services.AddScoped<IReserveDetailAppService, ReserveDetailAppService>();
            services.AddScoped<IDownpaymentDomainService, DownpaymentDomainService>();
            services.AddScoped<IDownpaymentAppService, DownpaymentAppService>();
            services.AddScoped<IDownpaymentDetailDomainService, DownpaymentDetailDomainService>();
            services.AddScoped<IDownpaymentDetailAppService, DownpaymentDetailAppService>();
            services.AddScoped<IAlarmDomainService, AlarmDomainService>();
            services.AddScoped<IAlarmAppService, AlarmAppService>();
            services.AddScoped<INegotiationAttachmentDomainService, NegotiationAttachmentDomainService>();
            services.AddScoped<INegotiationAttachmentAppService, NegotiationAttachmentAppService>();
            services.AddScoped<IClientsByBankAppService, ClientsByBankAppService>();
            services.AddScoped<IMeetingCategoryDomainService, MeetingCategoryDomainService>();
            services.AddScoped<IMeetingCategoryAppService, MeetingCategoryAppService>();
            services.AddScoped<IInformationDomainService, InformationDomainService>();
            services.AddScoped<IInformationAppService, InformationAppService>();
            services.AddScoped<IClientAlarmReportAppService, ClientAlarmReportAppService>();
            services.AddScoped<IClientsByProjectReportAppService, ClientsByProjectReportAppService>();
            services.AddScoped<IProspectReportAppService, ProspectReportAppService>();
            services.AddTransient<RealStateContext>();
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddTransient<ClientTransactionsAppService>();

            services.AddScoped<ApplicationOptionsAppService>();
            services.AddScoped<OperationsAppService>();



            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Jade Real Estate API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }



            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Jade Real Estate API V1");
                c.RoutePrefix = string.Empty;
            });

            //app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseCors(AllowAllOriginsPolicy);
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}


