using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Operations;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class OperationMap : EntityMap<Operation>
    {
        public override void Configure(EntityTypeBuilder<Operation> builder)
        {
            builder.Property(t => t.ClientId).HasColumnName("ClientId").IsRequired();

            builder.Property(t => t.OperationName).HasColumnName("OperationName").IsUnicode(false).IsRequired().HasMaxLength(50);
            builder.Property(t => t.OperationType).HasColumnName("OperationType").IsUnicode(false).IsRequired().HasMaxLength(50);
            builder.Property(t => t.OldValue).HasColumnName("OldValue").IsUnicode(false).HasMaxLength(254);
            builder.Property(t => t.NewValue).HasColumnName("NewValue").IsUnicode(false).HasMaxLength(254);
            builder.Property(t => t.SelectedOptions).HasColumnName("SelectedOptions").HasColumnType("text");
            builder.Property(t => t.Comments).HasColumnName("Comments").HasColumnType("text");

            builder.HasOne(t => t.Client).WithMany(t => t.Operations).HasForeignKey(t => t.ClientId);
        }
    }
}