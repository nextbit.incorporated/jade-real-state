using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.NegotiationPhases;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class NegotiationPhaseMap : EntityMap<NegotiationPhase>
    {
        public override void Configure(EntityTypeBuilder<NegotiationPhase> builder)
        {
            builder.Property(t => t.Status).HasColumnName("Status").IsUnicode(false).HasMaxLength(150);
            builder.Property(t => t.Date).HasColumnName("Date").IsUnicode(false);
            builder.Property(t => t.DateComplete).HasColumnName("DateComplete").IsUnicode(false);
            builder.Property(t => t.Comment).HasColumnName("Comment").IsUnicode(false).HasColumnType("longtext");
            builder.Property(t => t.Order).HasColumnName("Order").IsRequired().IsUnicode(false);

            builder.HasOne(t => t.Negotiation).WithMany(t => t.NegotiationPhases).HasForeignKey(x => x.NegotiationId);
            builder.HasOne(t => t.ServiceProcess).WithMany(t => t.NegotiationPhases).HasForeignKey(x => x.ProcessId);
            base.Configure(builder);
        }
    }
}