using Nextbit.JadeRealState.Services.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class UserRolMap : EntityMap<UserRol>
    {
        public override void Configure(EntityTypeBuilder<UserRol> builder)
        {
          builder.Property(t => t.UserId).HasColumnName("UserId").IsRequired().IsUnicode(false).HasMaxLength(50);
          builder.Property(t => t.RolId).HasColumnName("RolId").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Status).HasColumnName("Status").IsUnicode(false).IsRequired();

            base.Configure(builder);
        } 
    }
}