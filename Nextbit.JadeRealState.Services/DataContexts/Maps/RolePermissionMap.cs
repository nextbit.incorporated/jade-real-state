using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class RolePermissionMap : EntityMap<RolePermission>
    {
        public override void Configure(EntityTypeBuilder<RolePermission> builder)
        {
            builder.Property(t => t.RolId).HasColumnName("RolId").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(80);
            builder.Property(t => t.PermissionOption).HasColumnName("PermissionOption").IsRequired().IsUnicode(false).HasMaxLength(150);
            builder.Property(t => t.Status).HasColumnName("Status").IsUnicode(false).IsRequired();

            base.Configure(builder);
        }
    }
}