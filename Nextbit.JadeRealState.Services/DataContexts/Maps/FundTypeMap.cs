using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.FundTypes;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class FundTypeMap : EntityMap<FundType>
    {
        public override void Configure(EntityTypeBuilder<FundType> builder)
        {
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);


            base.Configure(builder);
        }
    }
}