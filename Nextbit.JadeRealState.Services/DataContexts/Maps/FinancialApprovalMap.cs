using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.FinancialApprovals;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class FinancialApprovalMap : EntityMap<FinancialApproval>
    {
        public override void Configure(EntityTypeBuilder<FinancialApproval> builder)
        {
            builder.Property(t => t.FinancialPrequalificationId).HasColumnName("FinancialPrequalificationId").IsRequired();
            builder.Property(t => t.Jointloan).HasColumnName("Jointloan").IsRequired();
            builder.Property(t => t.ApprovalState).HasColumnName("ApprovalState").IsRequired();
            builder.Property(t => t.ApprovalDate).HasColumnName("ApprovalDate");
            builder.Property(t => t.RejectionDate).HasColumnName("RejectionDate");
            builder.Property(t => t.RejectionReason).HasColumnName("RejectionReason").IsUnicode(false).HasColumnType("longtext");
            builder.Property(t => t.CreditOfficerComments).HasColumnName("CreditOfficerComments").IsUnicode(false).HasColumnType("longtext");
            builder.Property(t => t.SalesAdvisorComments).HasColumnName("SalesAdvisorComments").IsUnicode(false).HasColumnType("longtext");
            builder.Property(t => t.StartDate).HasColumnName("StartDate");
            builder.Property(t => t.DueDate).HasColumnName("DueDate");
            builder.Property(t => t.FollowUp).HasColumnName("FollowUp").IsUnicode(false).HasColumnType("longtext");

            base.Configure(builder);
        }
    }
}