using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class ClientTransactionMap : IEntityTypeConfiguration<ClientTransaction>
    {
        public void Configure(EntityTypeBuilder<ClientTransaction> builder)
        {

            builder.HasKey(e => e.UId);

            builder.Property(x => x.UId).HasColumnName(@"UId").IsRequired().UseMySqlIdentityColumn();
            builder.Property(x => x.ParentClientId).HasColumnName(@"ParentClientId");
            builder.Property(t => t.ClientCode).HasColumnName("ClientCode").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.FirstName).HasColumnName("FirstName").IsRequired().IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.MiddleName).HasColumnName("MiddleName").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.FirstSurname).HasColumnName("FirstSurname").IsRequired().IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.SecondSurname).HasColumnName("SecondSurname").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.IdentificationCard).HasColumnName("IdentificationCard").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.PhoneNumber).HasColumnName("PhoneNumber").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.CellPhoneNumber).HasColumnName("CellPhoneNumber").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.Email).HasColumnName("Email").IsUnicode(false).HasMaxLength(128);
            builder.Property(t => t.DebtorCurrency).HasColumnName("DebtorCurrency").IsRequired().IsUnicode(false).HasMaxLength(10).HasDefaultValue("LPS");
            builder.Property(t => t.FullTextSearchField).HasColumnName("FullTextSearchField").IsUnicode(false);
            builder.Property(t => t.HomeAddress).HasColumnName("HomeAddress").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.CreditOfficer).HasColumnName("CreditOfficer").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.ClientCreationComments).HasColumnName("ClientCreationComments").IsUnicode(false).HasMaxLength(256);
            builder.Property(x => x.Id).HasColumnName(@"Id").IsRequired();
            builder.Property(t => t.CreationDate).HasColumnName("CreationDate").HasColumnType("datetime").HasDefaultValueSql("NOW()").IsRequired();
            builder.Property(t => t.TransactionDate).HasColumnName("TransactionDate").IsRequired();
            builder.Property(t => t.CrudOperation).HasColumnName("CrudOperation").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy").IsRequired().IsUnicode(false).HasMaxLength(20);
            builder.Property(t => t.RowVersion).HasColumnName("RowVersion").IsRowVersion().IsRequired().IsConcurrencyToken().IsFixedLength().HasMaxLength(8);
            builder.Property(x => x.TransactionUId).HasColumnName(@"TransactionUId").IsRequired();
            builder.Property(x => x.TransactionType).HasColumnName(@"TransactionType").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.WorkStartDate).HasColumnName("WorkStartDate").HasColumnType("datetime");
            builder.Property(t => t.Profile).HasColumnName("Profile").IsUnicode(false).HasMaxLength(50);


            builder.HasIndex(c => c.SalesAdvisorId);
            builder.HasIndex(c => c.FullTextSearchField);
            builder.HasIndex(c => new { c.SalesAdvisorId, c.FullTextSearchField });
        }
    }
}