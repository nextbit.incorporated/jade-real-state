using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Origins;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class OriginMap : EntityMap<Origin>
    {
        public override void Configure(EntityTypeBuilder<Origin> builder)
        {
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Description).HasColumnName("Description").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Status).HasColumnName("Status").IsUnicode(false).HasMaxLength(8);

            base.Configure(builder);
        }
    }
}