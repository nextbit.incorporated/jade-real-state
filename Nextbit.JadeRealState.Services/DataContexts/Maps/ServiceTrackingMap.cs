using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.ServicesTracking;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class ServiceTrackingMap : EntityMap<ServiceTracking>
    {
        public override void Configure(EntityTypeBuilder<ServiceTracking> builder)
        {
            builder.Property(t => t.ClientId).HasColumnName("ClientId").IsRequired();
            builder.Property(t => t.SupplierServiceId).HasColumnName("SupplierServiceId").IsRequired();

            builder.Property(t => t.Status).HasColumnName("Status").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Comments).HasColumnName("Comments").IsRequired();

            builder.HasOne(t => t.SupplierService).WithMany(t => t.ServiceTrackings).HasForeignKey(x => x.SupplierServiceId);
            builder.HasOne(t => t.Client).WithMany(t => t.ServiceTrackings).HasForeignKey(x => x.ClientId);

            base.Configure(builder);
        }
    }
}