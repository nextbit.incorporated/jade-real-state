﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Projects;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class ProjectMap : EntityMap<Project>
    {
        public override void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Country).HasColumnName("Country").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.State).HasColumnName("State").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.City).HasColumnName("City").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Address).HasColumnName("Address").IsUnicode(false).HasMaxLength(500);
            
            base.Configure(builder);
        }
    }
}
