using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.SupplierServices;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class ServiceProcessMap : EntityMap<ServiceProcess>
    {
        public override void Configure(EntityTypeBuilder<ServiceProcess> builder)
        {
            builder.Property(t => t.SupplierServiceId).HasColumnName("SupplierServiceId").IsRequired();
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Order).HasColumnName("Order").IsRequired();

            builder.HasOne(t => t.SupplierService).WithMany(t => t.ServiceProcesses).HasForeignKey(x => x.SupplierServiceId);

            base.Configure(builder);
        }
    }
}