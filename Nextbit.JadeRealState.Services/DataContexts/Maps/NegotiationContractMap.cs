using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.NegotiationContracts;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class NegotiationContractMap : EntityMap<NegotiationContract>
    {
        public override void Configure(EntityTypeBuilder<NegotiationContract> builder)
        {
            builder.Property(t => t.NegotiationId).HasColumnName("NegotiationId").IsRequired();
            builder.Property(t => t.BuilderCompanyId).HasColumnName("BuilderCompanyId");
            builder.Property(t => t.ContractSigningDate).HasColumnName("ContractSigningDate");
            builder.Property(t => t.EffectiveDate).HasColumnName("EffectiveDate");
            builder.Property(t => t.BlockNumber).HasColumnName("BlockNumber").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.LotNumber).HasColumnName("LotNumber").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.LotNumber).HasColumnName("LotNumber").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.ConstructionMeters).HasColumnName("ConstructionMeters").HasColumnType("decimal(10, 2)");
            builder.Property(t => t.LandSize).HasColumnName("LandSize").HasColumnType("decimal(10, 2)");
            builder.Property(t => t.LandSize).HasColumnName("LandSize").HasColumnType("decimal(10, 2)");
            builder.Property(t => t.Registration).HasColumnName("Registration").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.ReservationValue).HasColumnName("ReservationValue").HasColumnType("decimal(10, 2)");
            builder.Property(t => t.DownPaymentValue).HasColumnName("DownPaymentValue").HasColumnType("decimal(10, 2)");
            builder.Property(t => t.DownPaymentMethod).HasColumnName("DownPaymentMethod").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.LandValue).HasColumnName("LandValue").HasColumnType("decimal(10, 2)");
            builder.Property(t => t.ImprovementsValue).HasColumnName("ImprovementsValue").HasColumnType("decimal(10, 2)");
            builder.Property(t => t.ImprovementsValue).HasColumnName("ImprovementsValue").HasColumnType("decimal(10, 2)");
            builder.Property(t => t.HouseTotalValue).HasColumnName("HouseTotalValue").HasColumnType("decimal(10, 2)");


            builder.HasOne(t => t.BuilderCompany).WithMany(t => t.NegotiationContracts).HasForeignKey(x => x.BuilderCompanyId);


            base.Configure(builder);
        }
    }
}