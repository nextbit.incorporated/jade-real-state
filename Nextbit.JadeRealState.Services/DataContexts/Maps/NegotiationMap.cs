using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Downpayments;
using Nextbit.JadeRealState.Services.Features.FinancialPrequalifications;
using Nextbit.JadeRealState.Services.Features.NegotiationContracts;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.Reservas;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class NegotiationMap : EntityMap<Negotiation>
    {
        public override void Configure(EntityTypeBuilder<Negotiation> builder)
        {
            builder.Property(t => t.NegotiationStartDate).HasColumnName("NegotiationStartDate");
            builder.Property(t => t.Comments).HasColumnName("Comments").IsUnicode(false).HasColumnType("longtext");
            builder.Property(t => t.CreditOfficer).HasColumnName("CreditOfficer").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.NegotiationStatus).HasColumnName("NegotiationStatus").IsUnicode(false).HasMaxLength(100);

            builder.HasOne(t => t.Project).WithMany(t => t.Negotiations).HasForeignKey(x => x.ProjectId);
            builder.HasOne(t => t.ContactType).WithMany(t => t.Negotiations).HasForeignKey(x => x.ContactTypeId);
            builder.HasOne(t => t.FinancialInstitution).WithMany(t => t.Negotiations).HasForeignKey(x => x.FinancialInstitutionId);
            builder.HasOne(t => t.SalesAdvisor).WithMany(t => t.Negotiations).HasForeignKey(x => x.SalesAdvisorId);

            builder.HasOne(t => t.SupplierService).WithMany(t => t.Negotiations).HasForeignKey(x => x.SupplierServiceId);
            builder.HasOne(t => t.Reserve).WithOne(t => t.Negotiation).HasForeignKey<Reserve>(x => x.NegotiationId);
            builder.HasOne(t => t.Downpayment).WithOne(t => t.Negotiation).HasForeignKey<Downpayment>(x => x.NegotiationId);
            builder.HasOne(t => t.FinancialPrequalification).WithOne(t => t.Negotiation).HasForeignKey<FinancialPrequalification>(x => x.NegotiationId);
            builder.HasOne(t => t.NegotiationContract).WithOne(t => t.Negotiation).HasForeignKey<NegotiationContract>(x => x.NegotiationId);

            base.Configure(builder);
        }
    }
}