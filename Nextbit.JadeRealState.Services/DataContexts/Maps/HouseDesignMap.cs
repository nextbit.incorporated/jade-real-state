using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.HouseDesigns;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class HouseDesignMap : EntityMap<HouseDesign>
    {
        public override void Configure(EntityTypeBuilder<HouseDesign> builder)
        {
            builder.Property(t => t.ProjectId).HasColumnName("ProjectId").IsRequired();
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Description).HasColumnName("Description").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.HouseSpecifications).HasColumnName("HouseSpecifications").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Status).HasColumnName("Status").IsUnicode(false).HasMaxLength(8);

            builder.HasOne(t => t.Project).WithMany(t => t.HouseDesigns).HasForeignKey(x => x.ProjectId);

            base.Configure(builder);
        }
    }
}