using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.FinancialApprovals;
using Nextbit.JadeRealState.Services.Features.FinancialPrequalifications;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class FinancialPrequalificationMap : EntityMap<FinancialPrequalification>
    {
        public override void Configure(EntityTypeBuilder<FinancialPrequalification> builder)
        {
            builder.Property(t => t.NegotiationId).HasColumnName("NegotiationId").IsRequired();
            builder.Property(t => t.FundTypeId).HasColumnName("FundTypeId");
            builder.Property(t => t.FinancialApprovalId).HasColumnName("FinancialApprovalId").IsRequired();
            builder.Property(t => t.InterestRate).HasColumnName("InterestRate").IsRequired();
            builder.Property(t => t.LoanAmount).HasColumnName("LoanAmount").IsRequired();
            builder.Property(t => t.LoanTerm).HasColumnName("LoanTerm").IsRequired();
            builder.Property(t => t.DownPaymentPercent).HasColumnName("DownPaymentPercent").IsRequired();
            builder.Property(t => t.DownPayment).HasColumnName("DownPayment").IsRequired();
            builder.Property(t => t.MonthlyPayment).HasColumnName("MonthlyPayment").IsRequired();
            builder.Property(t => t.Comments).HasColumnName("Comments").IsUnicode(false).HasColumnType("longtext");

            builder.HasOne(t => t.FundType).WithMany(t => t.FinancialPrequalifications).HasForeignKey(x => x.FundTypeId);
            builder.HasOne(t => t.FinancialApproval).WithOne(t => t.FinancialPrequalification).HasForeignKey<FinancialApproval>(x => x.FinancialPrequalificationId);

            base.Configure(builder);
        }
    }
}