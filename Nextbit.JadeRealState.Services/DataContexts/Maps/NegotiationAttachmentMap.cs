using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.NegotiationAttachments;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class NegotiationAttachmentMap : EntityMap<NegotiationAttachment>
    {
        public override void Configure(EntityTypeBuilder<NegotiationAttachment> builder)
        {
            builder.Property(t => t.NegotiationPhaseId).HasColumnName("NegotiationPhaseId").IsRequired();
            builder.Property(t => t.Date).HasColumnName("Date");
            builder.Property(t => t.Attachment).HasColumnName("Attachment");
            builder.Property(t => t.Title).HasColumnName("Title").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.ExtensionFile).HasColumnName("ExtensionFile").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.FileType).HasColumnName("FileType").IsUnicode(false).HasMaxLength(100);
            builder.HasOne(t => t.NegotiationPhase).WithMany(t => t.NegotiationAttachments).HasForeignKey(x => x.NegotiationPhaseId);
            base.Configure(builder);
        }
    }
}