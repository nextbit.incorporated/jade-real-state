using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class RolMap : EntityMap<Rol>
    {
        public override void Configure(EntityTypeBuilder<Rol> builder)
        {
           builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(50);
           builder.Property(t => t.Description).HasColumnName("Description").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Status).HasColumnName("Status").IsUnicode(false).IsRequired();

            base.Configure(builder);
        }
    }
}