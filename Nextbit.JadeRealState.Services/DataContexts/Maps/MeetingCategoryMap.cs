using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.MeetingCategories;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class MeetingCategoryMap : EntityMap<MeetingCategory>
    {
        public override void Configure(EntityTypeBuilder<MeetingCategory> builder)
        {
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Description).HasColumnName("Description").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Active).HasColumnName("Active").IsUnicode(false);

            base.Configure(builder);
        }
    }
}