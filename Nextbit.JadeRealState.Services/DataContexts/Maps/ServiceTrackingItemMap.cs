using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.ServicesTracking;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class ServiceTrackingItemMap : EntityMap<ServiceTrackingItem>
    {
        public override void Configure(EntityTypeBuilder<ServiceTrackingItem> builder)
        {
            builder.Property(t => t.ServiceTrackingId).HasColumnName("ServiceTrackingId").IsRequired();
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Order).HasColumnName("Order").IsRequired();

            builder.HasOne(t => t.ServiceTracking).WithMany(t => t.ServiceTrackingItems).HasForeignKey(x => x.ServiceTrackingId);

            base.Configure(builder);
        }
    }
}