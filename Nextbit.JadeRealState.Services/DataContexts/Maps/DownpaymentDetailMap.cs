using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Downpayments;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class DownpaymentDetailMap : EntityMap<DownpaymentDetail>
    {
        public override void Configure(EntityTypeBuilder<DownpaymentDetail> builder)
        {
            builder.Property(t => t.DownpaymentId).HasColumnName("DownpaymentId").IsRequired();
            builder.Property(t => t.Date).HasColumnName("Date").IsRequired();
            builder.Property(t => t.Payment).HasColumnName("Payment").IsRequired().HasColumnType("decimal(18, 9)");
            builder.Property(t => t.Comment).HasColumnName("Comment").HasMaxLength(250);
            builder.Property(t => t.Active).HasColumnName("Active");

            builder.HasOne(t => t.Downpayment).WithMany(t => t.Detalles).HasForeignKey(x => x.DownpaymentId);
            base.Configure(builder);
        }

    }
}