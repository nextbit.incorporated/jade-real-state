using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Downpayments;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class DownpaymentMap : EntityMap<Downpayment>
    {
        public override void Configure(EntityTypeBuilder<Downpayment> builder)
        {
            builder.Property(t => t.NegotiationId).HasColumnName("NegotiationId").IsRequired();
            builder.Property(t => t.LotNumber).HasColumnName("LotNumber");
            builder.Property(t => t.Zone).HasColumnName("Zone").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Block).HasColumnName("Block").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Address).HasColumnName("Address").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.TotalAmount).HasColumnName("TotalAmount").HasColumnType("decimal(18, 9)");
            builder.Property(t => t.AmountOfPayments).HasColumnName("AmountOfPayments");
            builder.Property(t => t.DownpaymentStatus).HasColumnName("DownpaymentStatus").HasMaxLength(250);

            // builder.HasOne(t => t.Negotiation).WithOne(t => t.Reserve).HasForeignKey<Reserve>(x => x.NegotiationId);
            base.Configure(builder);
        }
    }
}