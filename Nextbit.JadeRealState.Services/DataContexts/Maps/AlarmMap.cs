using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Alarms;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class AlarmMap : EntityMap<Alarm>
    {
        public override void Configure(EntityTypeBuilder<Alarm> builder)
        {
            builder.Property(t => t.ClientId).HasColumnName("ClientId").IsRequired();
            builder.Property(t => t.Date).HasColumnName("Date").IsRequired();
            builder.Property(t => t.Information).HasColumnName("Information").IsUnicode(false).HasMaxLength(250);
            builder.Property(t => t.MonthsNumber).HasColumnName("MonthsNumber");

            builder.HasOne(t => t.Client).WithMany(t => t.Alarms).HasForeignKey(x => x.ClientId);
            base.Configure(builder);
        }
    }
}