using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.ContactTypes;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class ContactTypeMap : EntityMap<ContactType>
    {
        public override void Configure(EntityTypeBuilder<ContactType> builder)
        {
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);


            base.Configure(builder);
        }
    }
}