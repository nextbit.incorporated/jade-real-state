using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Prospects;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class ProspectMap : EntityMap<Prospect>
    {
        public override void Configure(EntityTypeBuilder<Prospect> builder)
        {
            builder.Property(t => t.RegisterDate).HasColumnName("RegisterDate").IsRequired();
            builder.Property(t => t.ProjectId).HasColumnName("ProjectId");
            builder.Property(t => t.ContactType).HasColumnName("ContactType");
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.IdNumber).HasColumnName("IdNumber").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.ContactNumberOne).HasColumnName("ContactNumberOne").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.ContactNumberTwo).HasColumnName("ContactNumberTwo").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Email).HasColumnName("Email").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.TransactionOrigin).HasColumnName("TransactionOrigin").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.Status).HasColumnName("Status").IsUnicode(false).HasMaxLength(8);

            builder.HasOne(t => t.Project).WithMany(t => t.Prospects).HasForeignKey(x => x.ProjectId);
            builder.HasOne(t => t.ContactTypes).WithMany(t => t.Prospects).HasForeignKey(x => x.ContactType);
            builder.HasOne(t => t.SalesAdvisor).WithMany(t => t.Prospects).HasForeignKey(x => x.SalesAdvisorId);

            base.Configure(builder);
        }
    }
}