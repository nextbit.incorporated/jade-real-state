using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Reservas;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class ReserveDetailMap: EntityMap<ReserveDetail>
    {
        public override void Configure(EntityTypeBuilder<ReserveDetail> builder)
        {
            builder.Property(t => t.ReserveId).HasColumnName("ReserveId").IsRequired();
            builder.Property(t => t.Date).HasColumnName("Date").IsRequired();
            builder.Property(t => t.Payment).HasColumnName("Payment").IsRequired().HasColumnType("decimal(18, 9)");
            builder.Property(t => t.Comment).HasColumnName("Comment").HasMaxLength(250);
            builder.Property(t => t.Active).HasColumnName("Active");

            builder.HasOne(t => t.Reserve).WithMany(t => t.Detalles).HasForeignKey(x => x.ReserveId);
            base.Configure(builder);
        }
        
    }
}