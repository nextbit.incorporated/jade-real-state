using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.ClientsTracking;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class ClientTrackingMap : EntityMap<ClientTracking>
    {
        public override void Configure(EntityTypeBuilder<ClientTracking> builder)
        {
            builder.Property(t => t.Description).HasColumnName("Description").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.Comentarios).HasColumnName("Comentarios").IsUnicode(false).HasMaxLength(500);
            builder.Property(t => t.FechaCita).HasColumnName("FechaCita").IsUnicode(false);
            builder.Property(t => t.ClientId).HasColumnName("ClientId").IsUnicode(false);
            builder.Property(t => t.Orden).HasColumnName("Orden").IsUnicode(false);
            builder.Property(t => t.TipoServicioId).HasColumnName("TipoServicioId").IsUnicode(false);
            builder.Property(t => t.MeetingCategoryId).HasColumnName("MeetingCategoryId").IsUnicode(false).HasDefaultValue(1);
            builder.Property(t => t.ProcesoId).HasColumnName("ProcesoId").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Estado).HasColumnName("Estado").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.CreatedBy).HasColumnName("CreatedBy").IsUnicode(false).HasMaxLength(50);

            builder.HasOne(t => t.Client).WithMany(t => t.ClientsTracking).HasForeignKey(x => x.ClientId);

            base.Configure(builder);
        }
    }
}