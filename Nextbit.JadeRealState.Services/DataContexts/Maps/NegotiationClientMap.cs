using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.NegotiationClients;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class NegotiationClientMap : EntityMap<NegotiationClient>
    {
        public override void Configure(EntityTypeBuilder<NegotiationClient> builder)
        {
            builder.Property(t => t.NegotiationId).HasColumnName("NegotiationId").IsRequired();
            builder.Property(t => t.ClientId).HasColumnName("ClientId").IsRequired();
            builder.Property(t => t.Titular).HasColumnName("Titular").IsRequired();
            builder.Property(t => t.Relationship).HasColumnName("Relationship").IsUnicode(false).HasMaxLength(50);

            builder.Property(t => t.Workplace).HasColumnName("Workplace").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.WorkStartDate).HasColumnName("WorkStartDate");
            builder.Property(t => t.HomeAddress).HasColumnName("HomeAddress").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.OwnHome).HasColumnName("OwnHome").IsUnicode(false).IsRequired();
            builder.Property(t => t.FirstHome).HasColumnName("FirstHome").IsUnicode(false).IsRequired();
            builder.Property(t => t.ContributeToRap).HasColumnName("ContributeToRap").IsUnicode(false);
            builder.Property(t => t.Currency).HasColumnName("Currency").IsUnicode(false).HasMaxLength(50);

            builder.HasOne(t => t.Negotiation).WithMany(t => t.NegotiationClients).HasForeignKey(x => x.NegotiationId);
            builder.HasOne(t => t.Client).WithMany(t => t.NegotiationClients).HasForeignKey(x => x.ClientId);

            base.Configure(builder);
        }
    }
}