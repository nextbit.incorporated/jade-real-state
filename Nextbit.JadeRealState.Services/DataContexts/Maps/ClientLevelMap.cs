using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.ClientsLevel;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class ClientLevelMap : EntityMap<ClientLevel>
    {
        public override void Configure(EntityTypeBuilder<ClientLevel> builder)
        {
            builder.Property(t => t.Level).HasColumnName("Level").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Description).HasColumnName("Description").IsUnicode(false).HasMaxLength(150);
            builder.Property(t => t.Comment).HasColumnName("Comment").IsUnicode(false).HasMaxLength(150);
            builder.Property(t => t.PerfilCategoria).HasColumnName("PerfilCategoria").IsUnicode(false).HasMaxLength(3000);

            base.Configure(builder);
        }
    }

}