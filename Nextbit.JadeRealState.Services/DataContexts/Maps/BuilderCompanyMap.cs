using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Builders;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class BuilderCompanyMap : EntityMap<BuilderCompany>
    {
        public override void Configure(EntityTypeBuilder<BuilderCompany> builder)
        {
            builder.Property(t => t.Name).HasColumnName("Name").IsRequired().IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Description).HasColumnName("Description").IsUnicode(false).HasMaxLength(200);
            builder.Property(t => t.BusinessName).HasColumnName("BusinessName").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.ContactNumber).HasColumnName("ContactNumber").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.ElectronicAddresses).HasColumnName("ElectronicAddresses").IsUnicode(false).HasMaxLength(150);
            builder.Property(t => t.Email).HasColumnName("Email").IsUnicode(false).HasMaxLength(500);
            builder.Property(t => t.Address).HasColumnName("Address").IsUnicode(false).HasMaxLength(500);
            builder.Property(t => t.Owner).HasColumnName("Owner").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.OwnerContactNumber).HasColumnName("OwnerContactNumber").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.IdNumber).HasColumnName("IdNumber").IsUnicode(false).HasMaxLength(100);
            builder.Property(t => t.Status).HasColumnName("Status").IsUnicode(false).HasMaxLength(8);

            base.Configure(builder);
        }
    }
}