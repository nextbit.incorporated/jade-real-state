using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nextbit.JadeRealState.Services.Core;
using Nextbit.JadeRealState.Services.Features.Clients;

namespace Nextbit.JadeRealState.Services.DataContexts.Maps
{
    public class ClientMap : EntityMap<Client>
    {
        public override void Configure(EntityTypeBuilder<Client> builder)
        {

            builder.Property(x => x.ParentClientId).HasColumnName(@"ParentClientId");
            builder.Property(t => t.ClientCode).HasColumnName("ClientCode").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.FirstName).HasColumnName("FirstName").IsRequired().IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.MiddleName).HasColumnName("MiddleName").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.FirstSurname).HasColumnName("FirstSurname").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.SecondSurname).HasColumnName("SecondSurname").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.IdentificationCard).HasColumnName("IdentificationCard").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.PhoneNumber).HasColumnName("PhoneNumber").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.CellPhoneNumber).HasColumnName("CellPhoneNumber").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.Email).HasColumnName("Email").IsUnicode(false).HasMaxLength(128);
            builder.Property(t => t.DebtorCurrency).HasColumnName("DebtorCurrency").IsUnicode(false).HasMaxLength(10).HasDefaultValue("LPS");
            builder.Property(t => t.FullTextSearchField).HasColumnName("FullTextSearchField").IsUnicode(false);
            builder.Property(t => t.HomeAddress).HasColumnName("HomeAddress").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.CreditOfficer).HasColumnName("CreditOfficer").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.ClientCreationComments).HasColumnName("ClientCreationComments").IsUnicode(false).HasMaxLength(256);
            builder.Property(t => t.WorkStartDate).HasColumnName("WorkStartDate").HasColumnType("datetime");
            builder.Property(t => t.Profile).HasColumnName("Profile").IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.OperationDate).HasColumnName("OperationDate").HasColumnType("datetime");
            builder.Property(t => t.CategoryItems).HasColumnName("CategoryItems").IsUnicode(false).HasColumnType("longtext");
            builder.Property(t => t.Objections).HasColumnName("Objections").IsUnicode(false).HasColumnType("longtext");


            builder.HasOne(t => t.ParentClient).WithMany(t => t.ChildrenClients).HasForeignKey(t => t.ParentClientId);
            builder.HasOne(t => t.Project).WithMany(t => t.Clients).HasForeignKey(x => x.ProjectId);
            builder.HasOne(t => t.SupplierService).WithMany(t => t.Clients).HasForeignKey(x => x.SupplierServiceId);
            builder.HasOne(t => t.ContactType).WithMany(t => t.Clients).HasForeignKey(x => x.ContactTypeId);
            builder.HasOne(t => t.Nationality).WithMany(t => t.Clients).HasForeignKey(x => x.NationalityId);
            builder.HasOne(t => t.FinancialInstitution).WithMany(t => t.Clients).HasForeignKey(x => x.FinancialInstitutionId);
            builder.HasOne(t => t.SalesAdvisor).WithMany(t => t.Clients).HasForeignKey(x => x.SalesAdvisorId);
            builder.HasOne(t => t.ClientLevel).WithMany(t => t.Clients).HasForeignKey(x => x.ClientCategoryId);

            builder.HasIndex(c => c.FullTextSearchField).ForMySqlIsFullText();

            base.Configure(builder);
        }
    }
}