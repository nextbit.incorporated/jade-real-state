using Microsoft.EntityFrameworkCore;
using Nextbit.JadeRealState.Services.Core.Data;
using Nextbit.JadeRealState.Services.DataContexts.Maps;
using Nextbit.JadeRealState.Services.Features.Alarms;
using Nextbit.JadeRealState.Services.Features.Builders;
using Nextbit.JadeRealState.Services.Features.Clients;
using Nextbit.JadeRealState.Services.Features.ClientsLevel;
using Nextbit.JadeRealState.Services.Features.ClientsTracking;
using Nextbit.JadeRealState.Services.Features.ContactTypes;
using Nextbit.JadeRealState.Services.Features.Downpayments;
using Nextbit.JadeRealState.Services.Features.FinancialApprovals;
using Nextbit.JadeRealState.Services.Features.FinancialInstitutions;
using Nextbit.JadeRealState.Services.Features.FinancialPrequalifications;
using Nextbit.JadeRealState.Services.Features.FundTypes;
using Nextbit.JadeRealState.Services.Features.GeneralInfo;
using Nextbit.JadeRealState.Services.Features.HouseDesigns;
using Nextbit.JadeRealState.Services.Features.MeetingCategories;
using Nextbit.JadeRealState.Services.Features.NegotiationAttachments;
using Nextbit.JadeRealState.Services.Features.NegotiationClients;
using Nextbit.JadeRealState.Services.Features.NegotiationContracts;
using Nextbit.JadeRealState.Services.Features.NegotiationPhases;
using Nextbit.JadeRealState.Services.Features.Negotiations;
using Nextbit.JadeRealState.Services.Features.Operations;
using Nextbit.JadeRealState.Services.Features.Origins;
using Nextbit.JadeRealState.Services.Features.Projects;
using Nextbit.JadeRealState.Services.Features.Prospects;
using Nextbit.JadeRealState.Services.Features.Reservas;
using Nextbit.JadeRealState.Services.Features.SupplierServices;
using Nextbit.JadeRealState.Services.Features.Users;

namespace Nextbit.JadeRealState.Services.DataContexts
{
    public class RealStateContext : BCUnitOfWork, IRealEstateDataContext
    {
        public RealStateContext(DbContextOptions<RealStateContext> context) : base(context)
        {

        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Rol> Roles { get; set; }
        public DbSet<RolePermission> RoleAccess { get; set; }
        public DbSet<BuilderCompany> BuilderCompany { get; set; }
        public DbSet<UserRol> UserRol { get; set; }
        public DbSet<HouseDesign> HouseDesign { get; set; }
        public DbSet<ContactType> ContactTypes { get; set; }
        public DbSet<SupplierService> SupplierService { get; set; }
        public DbSet<ServiceProcess> ServiceProcesses { get; set; }
        public DbSet<Origin> Origin { get; set; }
        public DbSet<Prospect> Prospects { get; set; }
        public DbSet<FinancialInstitution> FinancialInstitutions { get; set; }
        public DbSet<ClientTracking> ClientsTracking { get; set; }
        public DbSet<Reserve> Reserves { get; set; }
        public DbSet<ClientLevel> ClientLevels { get; set; }
        public DbSet<Negotiation> Negotiations { get; set; }
        public DbSet<NegotiationPhase> NegotiationPhases { get; set; }
        public DbSet<ReserveDetail> ReserveDetails { get; set; }
        public DbSet<Downpayment> Downpayments { get; set; }
        public DbSet<DownpaymentDetail> DownpaymentDetails { get; set; }
        public DbSet<Alarm> Alarms { get; set; }
        public DbSet<NegotiationAttachment> NegotiationAtachments { get; set; }
        public DbSet<ClientTransaction> ClientTransactions { get; set; }
        public DbSet<MeetingCategory> MeetingCategories { get; set; }
        public DbSet<Information> GeneralInformation { get; set; }
        public DbSet<FundType> FundTypes { get; set; }
        public DbSet<NegotiationClient> NegotiationClients { get; set; }
        public DbSet<FinancialPrequalification> FinancialPrequalifications { get; set; }
        public DbSet<FinancialApproval> FinancialApprovals { get; set; }
        public DbSet<NegotiationContract> NegotiationContracts { get; set; }
        public DbSet<Operation> Operations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProjectMap());
            modelBuilder.ApplyConfiguration(new ClientMap());
            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new RolMap());
            modelBuilder.ApplyConfiguration(new UserRolMap());
            modelBuilder.ApplyConfiguration(new RolePermissionMap());
            modelBuilder.ApplyConfiguration(new BuilderCompanyMap());
            modelBuilder.ApplyConfiguration(new HouseDesignMap());
            modelBuilder.ApplyConfiguration(new ContactTypeMap());
            modelBuilder.ApplyConfiguration(new SupplierServiceMap());
            modelBuilder.ApplyConfiguration(new ServiceProcessMap());
            modelBuilder.ApplyConfiguration(new OriginMap());
            modelBuilder.ApplyConfiguration(new ProspectMap());
            modelBuilder.ApplyConfiguration(new ClientTrackingMap());
            modelBuilder.ApplyConfiguration(new ReserveMap());
            modelBuilder.ApplyConfiguration(new ClientLevelMap());
            modelBuilder.ApplyConfiguration(new NegotiationMap());
            modelBuilder.ApplyConfiguration(new NegotiationPhaseMap());
            modelBuilder.ApplyConfiguration(new ReserveDetailMap());
            modelBuilder.ApplyConfiguration(new DownpaymentMap());
            modelBuilder.ApplyConfiguration(new DownpaymentDetailMap());
            modelBuilder.ApplyConfiguration(new AlarmMap());
            modelBuilder.ApplyConfiguration(new NegotiationAttachmentMap());
            modelBuilder.ApplyConfiguration(new ClientTransactionMap());
            modelBuilder.ApplyConfiguration(new MeetingCategoryMap());
            modelBuilder.ApplyConfiguration(new InformationMap());
            modelBuilder.ApplyConfiguration(new FundTypeMap());
            modelBuilder.ApplyConfiguration(new NegotiationClientMap());
            modelBuilder.ApplyConfiguration(new FinancialPrequalificationMap());
            modelBuilder.ApplyConfiguration(new FinancialApprovalMap());
            modelBuilder.ApplyConfiguration(new NegotiationContractMap());
            modelBuilder.ApplyConfiguration(new OperationMap());
        }
    }
}