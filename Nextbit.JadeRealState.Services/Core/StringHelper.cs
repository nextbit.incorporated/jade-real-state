using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Core
{
    public static class StringHelper
    {
        public static string ConvertToCommaSeparatedString(this List<string> strings)
        {
            if (strings != null && strings.Any())
            {
                return string.Join(",", strings.Select(c => c));
            }

            return null;
        }

        public static string ConvertToCommaSeparatedString(this List<string> strings, string separator)
        {
            if (strings != null && strings.Any())
            {
                return string.Join(separator, strings.Select(c => c));
            }

            return null;
        }

        public static List<string> ConvertToStringList(this string sourceString)
        {
            if (!string.IsNullOrWhiteSpace(sourceString))
            {
                string[] splitedString = sourceString.Split(',');

                if (splitedString != null && splitedString.Length > 0)
                {
                    return (from s in splitedString select s.Trim()).ToList();
                }
            }

            return new List<string>();
        }

        public static List<string> ConvertToStringList(this string sourceString, char separator)
        {
            if (!string.IsNullOrWhiteSpace(sourceString))
            {
                string[] splitedString = sourceString.Split(separator);

                if (splitedString != null && splitedString.Length > 0)
                {
                    return (from s in splitedString select s.Trim()).ToList();
                }
            }

            return new List<string>();
        }
    }
}