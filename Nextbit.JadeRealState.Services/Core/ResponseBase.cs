using System;

namespace Nextbit.JadeRealState.Services.Core
{
    public class ResponseBase
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public string Status { get; set; }
        public string User { get; set; }
        public string ValidationErrorMessage { get; set; }

        public bool HasValidationError()
        {
            return !string.IsNullOrWhiteSpace(ValidationErrorMessage);
        }
    }
}