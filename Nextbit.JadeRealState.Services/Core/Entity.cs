using System;
using System.ComponentModel.DataAnnotations;

namespace Nextbit.JadeRealState.Services.Core
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; protected set; }
        public DateTime CreationDate { get; protected set; } = DateTime.Now;
        public Guid TransactionUId { get; protected set; }
        public string ModifiedBy { get; protected set; }
        public DateTime TransactionDate { get; protected set; }
        public string TransactionType { get; protected set; }
        public string CrudOperation { get; protected set; }
        [Timestamp]
        public byte[] RowVersion { get; protected set; }

        protected void SetAuditFields(string crudOperation = "Added")
        {
            CrudOperation = crudOperation;
            TransactionDate = DateTime.Now;
            TransactionType = "ClientsManagement";
            ModifiedBy = "Service";
            TransactionUId = Guid.NewGuid();
        }
    }
}