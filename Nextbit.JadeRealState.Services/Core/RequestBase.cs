namespace Nextbit.JadeRealState.Services.Core
{
    public class RequestBase
    {
        public string User { get; set; }
        public string Role { get; set; }
        public string Transaction { get; set; }
        
    }
}