using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Nextbit.JadeRealState.Services.Core
{
    public class EntityMap<TEntity> : IEntityTypeConfiguration<TEntity>
        where TEntity : Entity
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(x => x.Id).HasColumnName(@"Id").IsRequired().UseIdentityColumn();
            builder.Property(t => t.CreationDate).HasColumnName("CreationDate").HasColumnType("datetime").HasDefaultValueSql("NOW()").IsRequired();
            builder.Property(t => t.TransactionDate).HasColumnName("TransactionDate").IsRequired();
            builder.Property(t => t.CrudOperation).HasColumnName("CrudOperation").IsRequired().IsUnicode(false).HasMaxLength(50);
            builder.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy").IsRequired().IsUnicode(false).HasMaxLength(20);
            builder.Property(t => t.RowVersion).HasColumnName("RowVersion").IsRowVersion().IsRequired().IsConcurrencyToken().IsFixedLength().HasMaxLength(8);
            builder.Property(x => x.TransactionUId).HasColumnName(@"TransactionUId").IsRequired();
            builder.Property(x => x.TransactionType).HasColumnName(@"TransactionType").IsRequired().IsUnicode(false).HasMaxLength(100);
        }
    }
}