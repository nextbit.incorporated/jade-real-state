using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Core.Data
{
    public interface IBaseRepository : IDisposable
    {
        /// <summary>
        /// Get all rows.
        /// </summary>
        /// <typeparam name="TEntity">The entity type.</typeparam>
        /// <returns>Task{List{`0}}.</returns>
        IEnumerable<TEntity> GetAll<TEntity>()
            where TEntity : Entity;

        /// <summary>
        /// Get all rows asynchronously.
        /// </summary>
        /// <typeparam name="TEntity">The entity type.</typeparam>
        /// <returns>Task{List{`0}}.</returns>
        Task<IEnumerable<TEntity>> GetAllAsync<TEntity>()
            where TEntity : Entity;

        /// <summary>
        /// Get all rows.
        /// </summary>
        /// <typeparam name="TEntity">The entity type.</typeparam>
        /// <param name="includes">Related entities to include in the result set.</param>
        /// <returns>Task{List{`0}}.</returns>
        IEnumerable<TEntity> GetAll<TEntity>(List<string> includes)
            where TEntity : Entity;

        /// <summary>
        /// Get all rows asynchronously.
        /// </summary>
        /// <typeparam name="TEntity">The entity type.</typeparam>
        /// <param name="includes">Related entities to include in the result set.</param>
        /// <returns>Task{List{`0}}.</returns>
        Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(List<string> includes)
            where TEntity : Entity;

        /// <summary>
        /// Get the First or default row filtered by the Query expression.
        /// </summary>
        /// <typeparam name="TEntity">The entity type.</typeparam>
        /// <param name="predicate">The where (the query expression).</param>
        /// <returns>Object of the TEntity class.</returns>
        TEntity GetSingle<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : Entity;

        /// <summary>
        /// Get the First or default row filtered by the Query expression asynchronously.
        /// </summary>
        /// <typeparam name="TEntity">The entity type.</typeparam>
        /// <param name="predicate">The where (the query expression).</param>
        /// <returns>Object of the TEntity class.</returns>
        Task<TEntity> GetSingleAsync<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : Entity;

        /// <summary>
        /// Get an element of type TEntity in repository
        /// </summary>
        /// <param name="predicate">Filter that the element do match</param>
        /// <param name="includes">Related entities to include in the result set.</param>
        /// <returns>selected element</returns>
        TEntity GetSingle<TEntity>(Expression<Func<TEntity, bool>> predicate, List<string> includes)
            where TEntity : Entity;

        /// <summary>
        /// Get an element of type TEntity in repository
        /// </summary>
        /// <param name="predicate">Filter that the element do match</param>
        /// <param name="includes">Related entities to include in the result set.</param>
        /// <returns>selected element</returns>
        Task<TEntity> GetSingleAsync<TEntity>(Expression<Func<TEntity, bool>> predicate, List<string> includes)
            where TEntity : Entity;

        /// <summary>
        /// Gets filtered entities.
        /// </summary>
        /// <param name="predicate">The where.</param>
        /// <returns>Enumerable of the TEntity class.</returns>
        IEnumerable<TEntity> GetFiltered<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : Entity;

        /// <summary>
        /// Gets the many asynchronous.
        /// </summary>
        /// <param name="predicate">The where.</param>
        /// <returns>Enumerable of the TEntity class.</returns>
        Task<IEnumerable<TEntity>> GetFilteredAsync<TEntity>(Expression<Func<TEntity, bool>> predicate)
            where TEntity : Entity;

        /// <summary>
        /// Gets filtered entities.
        /// </summary>
        /// <param name="predicate">The where.</param>
        /// <returns>Enumerable of the TEntity class.</returns>
        IEnumerable<TEntity> GetFiltered<TEntity>(Expression<Func<TEntity, bool>> predicate, List<string> includes)
            where TEntity : Entity;

        /// <summary>
        /// Gets the many asynchronous.
        /// </summary>
        /// <param name="predicate">The where.</param>
        /// <returns>Enumerable of the TEntity class.</returns>
        Task<IEnumerable<TEntity>> GetFilteredAsync<TEntity>(Expression<Func<TEntity, bool>> predicate, List<string> includes)
            where TEntity : Entity;



    }
}