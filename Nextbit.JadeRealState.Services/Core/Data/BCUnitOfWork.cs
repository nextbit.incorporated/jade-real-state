using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Nextbit.JadeRealState.Services.Core.Data.Jade.RealEstate.Services.Core.Data;
using Nextbit.JadeRealState.Services.DataContexts;

namespace Nextbit.JadeRealState.Services.Core.Data
{
    public class BCUnitOfWork : DbContext
    {
        private readonly IReadOnlyList<EntityState> _entityStates = new List<EntityState> { EntityState.Modified, EntityState.Added, EntityState.Deleted };

        public BCUnitOfWork(DbContextOptions<BCUnitOfWork> context) : base(context)
        {

        }

        public BCUnitOfWork(DbContextOptions<RealStateContext> context) : base(context)
        {

        }

        public virtual void Commit()
        {
            IEnumerable<EntityEntry> changedDbEntityEntries = GetChangedDbEntityEntries();



            base.SaveChanges();
        }

        public virtual void Commit(TransactionInfo transactionInfo)
        {
            Transaction transaction = CreateTransaction(transactionInfo);

            Commit(transaction);
        }

        public virtual async Task CommitAsync(TransactionInfo transactionInfo)
        {
            await Task.Run(() => { Commit(transactionInfo); });
        }

        private void Commit(Transaction transaction)
        {


            using (var scope = TransactionScopeFactory.GetTransactionScope())
            {
                var changedEntities = new List<ModifiedEntityEntry>();
                var tableMapping = new List<EntityMapping>();
                var sqlCommandInfos = new List<SqlCommandInfo>();

                // Get entities with changes.
                IEnumerable<EntityEntry> changedDbEntityEntries = GetChangedDbEntityEntries();

                // Apply transaction info.
                foreach (EntityEntry entry in changedDbEntityEntries)
                {
                    if (entry.State == EntityState.Added)
                    {
                        AplicarInformacionTransaccion(entry, "CreationDate", transaction.TransactionDate);
                    }

                    ApplyTransactionInfo(transaction, entry);

                    // Get the deleted records info first
                    if (entry.State == EntityState.Deleted)
                    {
                        EntityMapping entityMapping = GetEntityMappingConfiguration(tableMapping, entry);
                        SqlCommandInfo sqlCommandInfo = GetSqlCommandInfo(transaction, entry, entityMapping);
                        if (sqlCommandInfo != null) sqlCommandInfos.Add(sqlCommandInfo);

                    }
                    else
                    {
                        changedEntities.Add(new ModifiedEntityEntry(entry, entry.State.ToString()));
                    }
                }

                base.SaveChanges();

                // Get the Added and Modified records after changes, that way we will be able to get the generated .
                foreach (ModifiedEntityEntry entry in changedEntities)
                {
                    EntityMapping entityMapping = GetEntityMappingConfiguration(tableMapping, entry.EntityEntry);
                    SqlCommandInfo sqlCommandInfo = GetSqlCommandInfo(transaction, entry.EntityEntry, entityMapping);
                    if (sqlCommandInfo != null) sqlCommandInfos.Add(sqlCommandInfo);
                }


                // Insert Transaction and audit records.
                foreach (SqlCommandInfo sqlCommandInfo in sqlCommandInfos)
                {
                    Database.ExecuteSqlCommand(sqlCommandInfo.SqlQuery, sqlCommandInfo.GetParameters());
                }

                scope.Complete();
            }

        }

        private Transaction CreateTransaction(TransactionInfo transactionInfo)
        {
            if (transactionInfo == null) throw new ArgumentNullException("transactionInfo");


            Transaction transaction = BuildTransactionInfo(transactionInfo);
            return transaction;
        }





        private Transaction BuildTransactionInfo(TransactionInfo transactionInfo)
        {
            return new Transaction
            {
                TransactionId = NewSequentialGuid(),
                TransactionDate = DateTime.Now,
                TransactionType = transactionInfo.TransactionType,
                ModifiedBy = transactionInfo.ModifiedBy,
            };
        }

        private void ApplyTransactionInfo(Transaction transaction, EntityEntry entry)
        {
            AplicarInformacionTransaccion(entry, "TransactionUId", transaction.TransactionId);
            AplicarInformacionTransaccion(entry, "CrudOperation", entry.State.ToString());
            AplicarInformacionTransaccion(entry, "TransactionDate", transaction.TransactionDate);
            AplicarInformacionTransaccion(entry, "ModifiedBy", transaction.ModifiedBy);
            AplicarInformacionTransaccion(entry, "TransactionType", transaction.TransactionType);
            AplicarInformacionTransaccion(entry, "TransactionId", transaction.TransactionId);
        }

        private IEnumerable<EntityEntry> GetChangedDbEntityEntries()
        {
            return ChangeTracker
                .Entries()
                .Where(e => (e.Entity is Entity) && _entityStates.Contains(e.State));
        }

        private EntityMapping GetEntityMappingConfiguration(List<EntityMapping> tableMapping, EntityEntry entry)
        {
            Type type = GetDomainEntityType(entry);

            EntityMapping entityMapping = tableMapping.FirstOrDefault(m => m.EntityType == type);

            return entityMapping;
        }


        private Type GetDomainEntityType(EntityEntry entry)
        {
            return entry.Entity.GetType();
        }

        private SqlCommandInfo GetSqlCommandInfo(Transaction transaction, EntityEntry entry, EntityMapping entityMapping)
        {
            if (entityMapping.TableName.Contains("_Transactions"))
            {
                return null;
            }

            string sqlInsert;
            Dictionary<string, object> parameters;
            CreateTransactionInsertStatement(entityMapping, entry, transaction, out sqlInsert, out parameters);

            return new SqlCommandInfo.Builder().WithSqlQuery(sqlInsert).WithParameters(parameters).Build();
        }

        private void CreateTransactionInsertStatement(EntityMapping entityMapping, EntityEntry entry,
                                                      Transaction transaction, out string sqlInsert, out Dictionary<string, object> parameters)
        {
            var insert = new StringBuilder();
            var fields = new StringBuilder();
            var paramNames = new StringBuilder();
            var values = new Dictionary<string, object>();

            insert.AppendLine(string.Format("Insert Into {0} ", entityMapping.TransactionTableName));

            int index = 0;
            IEnumerable<string> propertyNames = entry.State == EntityState.Deleted
                                                    ? entry.OriginalValues.Properties.Select(p => p.Name)
                                                    : entry.CurrentValues.Properties.Select(p => p.Name);

            foreach (string property in propertyNames)
            {
                string prop = property;
                if (prop != "RowVersion")
                {
                    if (fields.Length == 0)
                    {
                        fields.Append(string.Format(" ({0}", prop));
                        paramNames.Append(string.Format(" values ({0}{1}{2}", "{", index, "}"));
                    }
                    else
                    {
                        fields.Append(string.Format(", {0}", prop));
                        paramNames.Append(string.Format(", {0}{1}{2}", "{", index, "}"));
                    }

                    values.Add(prop, GetEntityPropertyValue(entry, prop, transaction));
                    index++;
                }
            }

            fields.Append(") ");
            paramNames.Append("); ");

            insert.AppendLine(fields.ToString());
            insert.AppendLine(paramNames.ToString());

            sqlInsert = insert.ToString();
            parameters = values;
        }

        private object GetEntityPropertyValue(EntityEntry entry, string prop, Transaction transaction)
        {
            object value;
            TryGeTransactionInfo(prop, transaction, out value);
            if (value != null)
            {
                return value;
            }

            if (entry.State == EntityState.Deleted || entry.State == EntityState.Detached)
            {
                return prop == "CrudOperation"
                           ? EntityState.Deleted.ToString()
                           : entry.Property(prop).OriginalValue;
            }
            return entry.Property(prop).CurrentValue;
        }

        private void TryGeTransactionInfo(string property, Transaction transaction, out object value)
        {
            switch (property)
            {
                case "TransactionId":
                    value = transaction.TransactionId;
                    break;

                case "TransactionType":
                    value = transaction.TransactionType;
                    break;

                case "TransactionDate":
                    value = transaction.TransactionDate;
                    break;



                case "ModifiedBy":
                    value = transaction.ModifiedBy;
                    break;

                default:
                    value = null;
                    break;
            }
        }

        private EntityMapping CreateTableMapping(Type type, string tableName)
        {
            // table name is the original table name with format: [schema].[table]
            string[] nameArray = tableName.Split('.');

            string schema = null;
            string table = null;
            if (nameArray.Length == 2)
            {
                // Schema
                schema = nameArray[0].Replace("[", "").Replace("]", "");
                // Table
                table = nameArray[1].Replace("[", "").Replace("]", "");
            }
            else
            {
                table = nameArray[0].Replace("[", "").Replace("]", "");
            }

            if (table.Contains("`"))
            {
                table = table.Replace("`", "");
            }


            // Transaction table
            string transactionTable = tableName;

            if (!tableName.Contains("_Transactions"))
            {
                int place = tableName.LastIndexOf(table, StringComparison.Ordinal);

                transactionTable = tableName.Remove(place, table.Length).Insert(place, string.Format("{0}_Transactions", table));
            }

            return new EntityMapping(type, tableName, transactionTable);
        }



        /// <summary>
        /// This algorithm generates secuential GUIDs across system boundaries, ideal for databases
        /// </summary>
        /// <returns></returns>
        private static Guid NewSequentialGuid()
        {
            byte[] uid = Guid.NewGuid().ToByteArray();
            byte[] binDate = BitConverter.GetBytes(DateTime.UtcNow.Ticks);

            var secuentialGuid = new byte[uid.Length];

            secuentialGuid[0] = uid[0];
            secuentialGuid[1] = uid[1];
            secuentialGuid[2] = uid[2];
            secuentialGuid[3] = uid[3];
            secuentialGuid[4] = uid[4];
            secuentialGuid[5] = uid[5];
            secuentialGuid[6] = uid[6];
            // set the first part of the 8th byte to '1100' so
            // later we'll be able to validate it was generated by us

            secuentialGuid[7] = (byte)(0xc0 | (0xf & uid[7]));

            // the last 8 bytes are sequential,
            // it minimizes index fragmentation
            // to a degree as long as there are not a large
            // number of Secuential-Guids generated per millisecond

            secuentialGuid[9] = binDate[0];
            secuentialGuid[8] = binDate[1];
            secuentialGuid[15] = binDate[2];
            secuentialGuid[14] = binDate[3];
            secuentialGuid[13] = binDate[4];
            secuentialGuid[12] = binDate[5];
            secuentialGuid[11] = binDate[6];
            secuentialGuid[10] = binDate[7];

            return new Guid(secuentialGuid);
        }


        private void AplicarInformacionTransaccion(EntityEntry item, string nombrePropiedad, object valorPropiedad)
        {
            if (item != null && item.Entity != null)
            {
                PropertyInfo propInfoEntity = item.Entity.GetType().GetProperty(nombrePropiedad);
                if (propInfoEntity != null)
                {
                    propInfoEntity.SetValue(item.Entity, valorPropiedad, null);
                }
            }
        }



        public virtual void RollbackChanges()
        {
            // set all entities in change tracker
            // as 'unchanged state'
            ChangeTracker
                .Entries()
                .ToList()
                .ForEach(entry => entry.State = EntityState.Unchanged);
        }






        public virtual int ExecuteCommand(string sqlCommand, params object[] parameters)
        {
            return Database.ExecuteSqlCommand(sqlCommand, parameters);
        }

        public virtual Task<int> ExecuteCommandAsync(string sqlCommand, params object[] parameters)
        {
            return Task.Run(() => { return ExecuteCommandAsync(sqlCommand, parameters); });
        }

        public virtual DbSet<TEntity> CreateSet<TEntity>() where TEntity : class
        {
            return Set<TEntity>();
        }

        public virtual void Attach<TEntity>(TEntity item) where TEntity : class
        {
            //attach and set as unchanged
            Entry(item).State = EntityState.Unchanged;
        }

        public virtual void SetModified<TEntity>(TEntity item) where TEntity : class
        {
            //this operation also attach item in object state manager
            Entry(item).State = EntityState.Modified;
        }

        public virtual void ApplyCurrentValues<TEntity>(TEntity original, TEntity current) where TEntity : class
        {
            //if it is not attached, attach original and set current values
            Entry(original).CurrentValues.SetValues(current);
        }
    }
}