using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Core.Data
{
    public interface IGenericRepository<T> : IBaseRepository
            where T : IQueryableUnitOfWork
    {
        /// <summary>
        /// Get the unit of work in this repository
        /// </summary>
        IUnitOfWork UnitOfWork { get; }

        /// <summary>
        /// Add the entity to the repository.
        /// </summary>
        /// <typeparam name="TEntity">The entity type.</typeparam>
        /// <param name="entity">The new entity to add.</param>
        void Add<TEntity>(TEntity entity)
            where TEntity : Entity;

        /// <summary>
        /// Add the entities to the repository.
        /// </summary>
        /// <typeparam name="TEntity">The entity type.</typeparam>
        /// <param name="entities">The new entities to add.</param>
        void AddRange<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : Entity;

        /// <summary>
        /// Remove the specified entity.
        /// </summary>
        /// <typeparam name="TEntity">The entity type.</typeparam>
        /// <param name="entity">The entity to remove.</param>
        void Remove<TEntity>(TEntity entity)
            where TEntity : Entity;

        /// <summary>
        /// Remove the specified entities from the repository.
        /// </summary>
        /// <typeparam name="TEntity">The entity type.</typeparam>
        /// <param name="entities">The entities to remove.</param>
        void RemoveRange<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : Entity;




    }
}