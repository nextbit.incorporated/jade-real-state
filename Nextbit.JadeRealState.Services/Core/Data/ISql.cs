using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Core.Data
{
    public interface ISql
    {

        /// <summary>
        /// Execute arbitrary command into underlying persistence store.
        /// </summary>
        /// <param name="sqlCommand">
        /// Command to execute.
        /// <example>
        /// SELECT idCustomer,Name FROM dbo.[Customers] WHERE idCustomer > {0}
        /// </example>
        ///</param>
        /// <param name="parameters">A vector of parameters values.</param>
        /// <returns>The number of affected records.</returns>
        int ExecuteCommand(string sqlCommand, params object[] parameters);

        /// <summary>
        /// Execute arbitrary command into underlying persistence store asynchronously.
        /// </summary>
        /// <param name="sqlCommand">
        /// Command to execute.
        /// <example>
        /// SELECT idCustomer,Name FROM dbo.[Customers] WHERE idCustomer > {0}
        /// </example>
        ///</param>
        /// <param name="parameters">A vector of parameters values.</param>
        /// <returns>The number of affected records.</returns>
        Task<int> ExecuteCommandAsync(string sqlCommand, params object[] parameters);


    }
}