using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Nextbit.JadeRealState.Services.Core.Data
{
    public class GenericRepository<T> : BaseRepository<T>, IGenericRepository<T>
            where T : IQueryableUnitOfWork
    {
        public GenericRepository(T unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <inheritdoc/>
        public virtual void Add<TEntity>(TEntity entity) where TEntity : Entity
        {
            if (entity != null)
            {
                GetSet<TEntity>().Add(entity); // add new item in this set
            }
        }

        /// <inheritdoc/>
        public virtual void AddRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : Entity
        {
            if (entities != null)
            {
                // add new items to the set.
                GetSet<TEntity>().AddRange(entities);
            }
        }

        /// <inheritdoc/>
        public virtual IEnumerable<TEntity> GetAll<TEntity>() where TEntity : Entity
        {
            return GetSet<TEntity>().ToList();
        }

        /// <inheritdoc/>
        public virtual async Task<IEnumerable<TEntity>> GetAllAsync<TEntity>() where TEntity : Entity
        {
            return await GetSet<TEntity>().ToListAsync();
        }

        /// <inheritdoc/>
        public virtual IEnumerable<TEntity> GetAll<TEntity>(List<string> includes) where TEntity : Entity
        {
            IQueryable<TEntity> items = GetSet<TEntity>(includes);

            return items.ToList();
        }

        /// <inheritdoc/>
        public virtual async Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(List<string> includes) where TEntity : Entity
        {
            IQueryable<TEntity> items = GetSet<TEntity>(includes);

            return await items.ToListAsync();
        }

        /// <inheritdoc/>
        public virtual TEntity GetSingle<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : Entity
        {
            return GetSet<TEntity>().FirstOrDefault(predicate);
        }

        /// <inheritdoc/>
        public virtual async Task<TEntity> GetSingleAsync<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : Entity
        {
            return await GetSet<TEntity>().FirstOrDefaultAsync(predicate);
        }
        /// <inheritdoc/>
        public virtual TEntity GetSingle<TEntity>(Expression<Func<TEntity, bool>> predicate, List<string> includes) where TEntity : Entity
        {
            IQueryable<TEntity> items = GetSet<TEntity>(includes);

            return items.FirstOrDefault(predicate);
        }



        /// <inheritdoc/>
        public virtual async Task<TEntity> GetSingleAsync<TEntity>(Expression<Func<TEntity, bool>> predicate, List<string> includes) where TEntity : Entity
        {
            IQueryable<TEntity> items = GetSet<TEntity>(includes);

            return await items.FirstOrDefaultAsync(predicate);
        }

        /// <inheritdoc/>
        public virtual IEnumerable<TEntity> GetFiltered<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : Entity
        {
            return GetSet<TEntity>().Where(predicate).ToList();
        }



        /// <inheritdoc/>
        public virtual async Task<IEnumerable<TEntity>> GetFilteredAsync<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : Entity
        {
            return await GetSet<TEntity>().Where(predicate).ToListAsync();
        }

        /// <inheritdoc/>
        public virtual IEnumerable<TEntity> GetFiltered<TEntity>(Expression<Func<TEntity, bool>> predicate, List<string> includes) where TEntity : Entity
        {
            IQueryable<TEntity> items = GetSet<TEntity>(includes);

            return items.Where(predicate).ToList();
        }

        /// <inheritdoc/>
        public virtual async Task<IEnumerable<TEntity>> GetFilteredAsync<TEntity>(Expression<Func<TEntity, bool>> predicate, List<string> includes)
            where TEntity : Entity
        {
            IQueryable<TEntity> items = GetSet<TEntity>(includes);

            return await items.Where(predicate).ToListAsync();
        }

        /// <inheritdoc/>
        public virtual void Remove<TEntity>(TEntity entity) where TEntity : Entity
        {
            if (entity != null)
            {
                //attach item if not exist
                QueryableUnitOfWork.Attach(entity);

                //set as "removed"
                GetSet<TEntity>().Remove(entity);
            }
        }

        /// <inheritdoc/>
        public virtual void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : Entity
        {
            if (entities != null)
            {
                //set as "removed"
                GetSet<TEntity>().RemoveRange(entities);
            }
        }

    }
}