using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace Nextbit.JadeRealState.Services.Core.Data
{
    public class BaseRepository<T> : SingletonOrderMethodInfo
           where T : IQueryableUnitOfWork
    {
        private readonly T _unitOfWork;

        /// <summary>
        ///
        /// </summary>
        /// <param name="unitOfWork"></param>
        protected BaseRepository(T unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        protected IQueryableUnitOfWork QueryableUnitOfWork
        {
            get
            {
                return _unitOfWork;
            }
        }

        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _unitOfWork;
            }
        }

        protected DbSet<TEntity> GetSet<TEntity>()
            where TEntity : Entity
        {
            return _unitOfWork.CreateSet<TEntity>();
        }

        protected IQueryable<TEntity> GetSetReadOnly<TEntity>()
            where TEntity : Entity
        {
            return GetSet<TEntity>().AsNoTracking();
        }

        protected IQueryable<TEntity> GetSet<TEntity>(List<string> includes)
            where TEntity : Entity
        {
            IQueryable<TEntity> items = GetSet<TEntity>();

            items = ApplyIncludesToQuery(includes, items);
            return items;
        }

        protected IQueryable<TEntity> GetSetReadOnly<TEntity>(List<string> includes)
            where TEntity : Entity
        {
            IQueryable<TEntity> items = GetSetReadOnly<TEntity>();

            items = ApplyIncludesToQuery(includes, items);
            return items;
        }



        protected static IQueryable<TEntity> ApplyIncludesToQuery<TEntity>(List<string> includes, IQueryable<TEntity> items)
            where TEntity : Entity
        {
            if (includes != null && includes.Any())
            {
                // Adding Includes to filter.
                items = includes.Aggregate(items, (current, include) => current.Include(include));
            }

            return items;
        }






        private static string GetParameterNames(IEnumerable<SqlParameter> sqlParameters)
        {
            return string.Join(",", sqlParameters.Select(c => c.ParameterName));
        }

        private DataTable ConvertToDataTable(object value)
        {
            var type = value.GetType();
            DataTable table = new DataTable();

            var isList = type != typeof(string) && typeof(IEnumerable).IsAssignableFrom(type);
            if (!isList) { return null; }

            var objectPropertyNames = new List<string>();
            var valueAsList = value as IList;
            var firstItem = valueAsList[0];

            foreach (var property in firstItem.GetType().GetProperties())
            {
                objectPropertyNames.Add(property.Name);
                table.Columns.Add(property.Name, property.PropertyType);
            }

            foreach (var element in valueAsList)
            {
                var dataRow = table.NewRow();
                foreach (var propertyName in objectPropertyNames)
                {
                    var propertyValueType = element.GetType().GetProperty(propertyName);
                    dataRow[propertyName] = propertyValueType.GetValue(element);
                }
                table.Rows.Add(dataRow);
            }

            return table;
        }



        private static string GetStoredProcedureExecutionQuery(string storedProcedure, string paramNames)
        {
            return $@"
                    DECLARE	@return_value int

                    EXEC	@return_value = {storedProcedure} {paramNames}

                    SELECT	'AfectedRows' = @@ROWCOUNT
                    ";
        }



        private string GetParamNames(Dictionary<string, object> parameters)
        {
            return (parameters != null && parameters.Any())
                ? parameters.Select(p => p.Key).Aggregate((i, j) => i + ", " + j)
                : string.Empty;
        }

        private SqlParameter[] CreateSqlParameters(Dictionary<string, object> parameters)
        {
            if (parameters != null && parameters.Any())
            {
                SqlParameter[] sqlParameters = new SqlParameter[parameters.Count];

                int parameterIndex = 0;

                foreach (var parameter in parameters)
                {
                    if (parameter.Value is CustomTypeQueryDefinition)
                    {
                        CustomTypeQueryDefinition customTypeQueryDefinition = parameter.Value as CustomTypeQueryDefinition;
                        sqlParameters[parameterIndex] = GetSqlParameterCustomType(parameter.Key, customTypeQueryDefinition);
                    }
                    else
                    {
                        sqlParameters[parameterIndex] = new SqlParameter(parameter.Key, parameter.Value);
                    }
                    parameterIndex++;
                }

                return sqlParameters;
            }

            return new SqlParameter[0];
        }
        private SqlParameter GetSqlParameterCustomType(string key, CustomTypeQueryDefinition customTypeQueryDefinition)
        {
            return new SqlParameter(key, SqlDbType.Structured)
            {
                Direction = ParameterDirection.Input,
                TypeName = customTypeQueryDefinition.SqlTypeName,
                Value = CrearDataTable(customTypeQueryDefinition.Values)
            };
        }

        private static DataTable CrearDataTable(IEnumerable<string> id)
        {
            var tabla = new DataTable();
            tabla.Columns.Add("Id", typeof(string));
            foreach (var estilo in id)
            {
                var dr = tabla.NewRow();
                dr["Id"] = estilo;
                tabla.Rows.Add(dr);
            }
            return tabla;
        }


        protected static PagedCollection CreatePagedCollection<TEntity>(IQueryable<TEntity> items, int pageIndex, int pageSize, List<string> sortFields, bool ascending) where TEntity : Entity
        {
            IQueryable<TEntity> itemsAll = items;

            if (pageSize != 0)
            {
                if (sortFields != null && sortFields.Any())
                {
                    sortFields.ForEach(s => items = CallOrderBy<TEntity>(items, s, ascending));
                    items = items.Skip(pageSize * pageIndex);
                }
                items = items.Take(pageSize);
            }

            return new PagedCollection(pageIndex, pageSize, items.AsEnumerable(), itemsAll.Count(), itemsAll.Count());
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <filterpriority>2</filterpriority>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Release managed resources.
                _unitOfWork?.Dispose();
            }

            // Release unmanaged resources.
            // Set large fields to null.
            // Call Dispose on your base class.
            base.Dispose(disposing);
        }
    }
}