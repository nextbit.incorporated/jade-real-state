using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Nextbit.JadeRealState.Services.Core.Data
{
    class ModifiedEntityEntry
    {
        public EntityEntry EntityEntry
        {
            get { return _entityEntry; }
        }
        private readonly EntityEntry _entityEntry;



        public string State
        {
            get { return _state; }
        }
        private readonly string _state;

        public ModifiedEntityEntry(EntityEntry entityEntry, string state)
        {
            _entityEntry = entityEntry;
            _state = state;

        }
    }
}