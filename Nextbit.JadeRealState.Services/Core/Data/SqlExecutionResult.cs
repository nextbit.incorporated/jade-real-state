namespace Nextbit.JadeRealState.Services.Core.Data
{
    public sealed class SqlExecutionResult
    {
        private SqlExecutionResult()
        {
        }

        public SqlExecutionResult(int afectedRows)
        {
            AfectedRows = afectedRows;
        }

        public int AfectedRows { get; private set; }
    }
}