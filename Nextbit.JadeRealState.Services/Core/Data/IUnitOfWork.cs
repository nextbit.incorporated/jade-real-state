using System;
using System.Threading.Tasks;

namespace Nextbit.JadeRealState.Services.Core.Data
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        void Commit(TransactionInfo transactionInfo);
        Task CommitAsync(TransactionInfo transactionInfo);
        void RollbackChanges();
    }
}