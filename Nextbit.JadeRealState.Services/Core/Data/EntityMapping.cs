using System;

namespace Nextbit.JadeRealState.Services.Core.Data
{
    namespace Jade.RealEstate.Services.Core.Data
    {
        public class EntityMapping
        {
            public EntityMapping(Type entityType, string tableName, string transactionTableName)
            {
                EntityType = entityType;

                TableName = tableName;
                TransactionTableName = transactionTableName;
            }
            public Type EntityType { get; private set; }
            public string TableName { get; private set; }
            public string TransactionTableName { get; private set; }
        }
    }
}