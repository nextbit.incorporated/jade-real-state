using System;

namespace Nextbit.JadeRealState.Services.Core.Data
{
    public class Transaction
    {
        public Guid TransactionId { get; set; }
        public string TransactionType { get; set; }
        public DateTime TransactionDate { get; set; }
        public string ModifiedBy { get; set; }

    }
}