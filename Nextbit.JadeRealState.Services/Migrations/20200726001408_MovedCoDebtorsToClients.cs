﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class MovedCoDebtorsToClients : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Negotiations_Origins_CoDebtorNationalityId",
                table: "Negotiations");

            migrationBuilder.DropForeignKey(
                name: "FK_Negotiations_SupplierServices_ServiceId",
                table: "Negotiations");

            migrationBuilder.DropIndex(
                name: "IX_Negotiations_CoDebtorNationalityId",
                table: "Negotiations");

            migrationBuilder.DropIndex(
                name: "IX_Negotiations_ServiceId",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorCellPhoneNumber",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorEmail",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorFirstName",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorFirstSurname",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorIdentificationCard",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorMiddleName",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorNationalityId",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorPhoneNumber",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorSecondSurname",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorWorkplace",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "ServiceId",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorCellPhoneNumber",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorCurrency",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorEmail",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorFirstName",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorFirstSurname",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorGrossMonthlyIncome",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorIdentificationCard",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorMiddleName",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorNationality",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorNationalityId",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorPhoneNumber",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorSecondSurname",
                table: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "CoDebtorWorkplace",
                table: "ClientTransactions");

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<int>(
                name: "ReserveId",
                table: "Negotiations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "DownpaymentId",
                table: "Negotiations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<decimal>(
                name: "CoDebtorGrossMonthlyIncome",
                table: "Negotiations",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(65,30)");

            migrationBuilder.AlterColumn<string>(
                name: "CoDebtorCurrency",
                table: "Negotiations",
                unicode: false,
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(10) CHARACTER SET utf8mb4",
                oldUnicode: false,
                oldMaxLength: 10,
                oldDefaultValue: "LPS");

            migrationBuilder.AlterColumn<string>(
                name: "ActualWorkPlace",
                table: "Negotiations",
                unicode: false,
                maxLength: 256,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(150) CHARACTER SET utf8mb4",
                oldUnicode: false,
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorActualWorkPlace",
                table: "Negotiations",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CoDebtorClientId",
                table: "Negotiations",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "CoDebtorContributeToRap",
                table: "Negotiations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorHomeAddress",
                table: "Negotiations",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "CoDebtorOwnHome",
                table: "Negotiations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CoDebtorWorkStartDate",
                table: "Negotiations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OriginId",
                table: "Negotiations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SupplierServiceId",
                table: "Negotiations",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AddColumn<int>(
                name: "ParentClientId",
                table: "ClientTransactions",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_CoDebtorClientId",
                table: "Negotiations",
                column: "CoDebtorClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_OriginId",
                table: "Negotiations",
                column: "OriginId");

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_SupplierServiceId",
                table: "Negotiations",
                column: "SupplierServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Negotiations_Clients_CoDebtorClientId",
                table: "Negotiations",
                column: "CoDebtorClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Negotiations_Origins_OriginId",
                table: "Negotiations",
                column: "OriginId",
                principalTable: "Origins",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Negotiations_SupplierServices_SupplierServiceId",
                table: "Negotiations",
                column: "SupplierServiceId",
                principalTable: "SupplierServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Negotiations_Clients_CoDebtorClientId",
                table: "Negotiations");

            migrationBuilder.DropForeignKey(
                name: "FK_Negotiations_Origins_OriginId",
                table: "Negotiations");

            migrationBuilder.DropForeignKey(
                name: "FK_Negotiations_SupplierServices_SupplierServiceId",
                table: "Negotiations");

            migrationBuilder.DropIndex(
                name: "IX_Negotiations_CoDebtorClientId",
                table: "Negotiations");

            migrationBuilder.DropIndex(
                name: "IX_Negotiations_OriginId",
                table: "Negotiations");

            migrationBuilder.DropIndex(
                name: "IX_Negotiations_SupplierServiceId",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorActualWorkPlace",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorClientId",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorContributeToRap",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorHomeAddress",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorOwnHome",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorWorkStartDate",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "OriginId",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "SupplierServiceId",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "ParentClientId",
                table: "ClientTransactions");

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<int>(
                name: "ReserveId",
                table: "Negotiations",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DownpaymentId",
                table: "Negotiations",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CoDebtorGrossMonthlyIncome",
                table: "Negotiations",
                type: "decimal(65,30)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CoDebtorCurrency",
                table: "Negotiations",
                type: "varchar(10) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 10,
                nullable: false,
                defaultValue: "LPS",
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ActualWorkPlace",
                table: "Negotiations",
                type: "varchar(150) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 256,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorCellPhoneNumber",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorEmail",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorFirstName",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorFirstSurname",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorIdentificationCard",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorMiddleName",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CoDebtorNationalityId",
                table: "Negotiations",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorPhoneNumber",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorSecondSurname",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorWorkplace",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ServiceId",
                table: "Negotiations",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorCellPhoneNumber",
                table: "ClientTransactions",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorCurrency",
                table: "ClientTransactions",
                type: "varchar(10) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 10,
                nullable: false,
                defaultValue: "LPS");

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorEmail",
                table: "ClientTransactions",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorFirstName",
                table: "ClientTransactions",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorFirstSurname",
                table: "ClientTransactions",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "CoDebtorGrossMonthlyIncome",
                table: "ClientTransactions",
                type: "decimal(65,30)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorIdentificationCard",
                table: "ClientTransactions",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorMiddleName",
                table: "ClientTransactions",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorNationality",
                table: "ClientTransactions",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CoDebtorNationalityId",
                table: "ClientTransactions",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorPhoneNumber",
                table: "ClientTransactions",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorSecondSurname",
                table: "ClientTransactions",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorWorkplace",
                table: "ClientTransactions",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_CoDebtorNationalityId",
                table: "Negotiations",
                column: "CoDebtorNationalityId");

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_ServiceId",
                table: "Negotiations",
                column: "ServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Negotiations_Origins_CoDebtorNationalityId",
                table: "Negotiations",
                column: "CoDebtorNationalityId",
                principalTable: "Origins",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Negotiations_SupplierServices_ServiceId",
                table: "Negotiations",
                column: "ServiceId",
                principalTable: "SupplierServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
