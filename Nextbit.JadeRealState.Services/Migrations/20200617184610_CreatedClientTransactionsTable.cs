﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class CreatedClientTransactionsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AddColumn<string>(
                name: "ClientCreationComments",
                table: "Clients",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ClientTransactions",
                columns: table => new
                {
                    UId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Id = table.Column<int>(nullable: false),
                    ClientCode = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    ProjectId = table.Column<int>(nullable: true),
                    Project = table.Column<string>(nullable: true),
                    ContactTypeId = table.Column<int>(nullable: true),
                    ContactType = table.Column<string>(nullable: true),
                    NationalityId = table.Column<int>(nullable: true),
                    Nationality = table.Column<string>(nullable: true),
                    FinancialInstitutionId = table.Column<int>(nullable: true),
                    FinancialInstitution = table.Column<string>(nullable: true),
                    CoDebtorNationalityId = table.Column<int>(nullable: true),
                    CoDebtorNationality = table.Column<string>(nullable: true),
                    ClientCategoryId = table.Column<int>(nullable: true),
                    SalesAdvisorId = table.Column<int>(nullable: true),
                    SalesAdvisor = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(unicode: false, maxLength: 256, nullable: false),
                    MiddleName = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    FirstSurname = table.Column<string>(unicode: false, maxLength: 256, nullable: false),
                    SecondSurname = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    CoDebtorFirstName = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    CoDebtorMiddleName = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    CoDebtorFirstSurname = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    CoDebtorSecondSurname = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    FullTextSearchField = table.Column<string>(unicode: false, nullable: true),
                    IdentificationCard = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    PhoneNumber = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    CellPhoneNumber = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    Email = table.Column<string>(unicode: false, maxLength: 128, nullable: true),
                    Workplace = table.Column<string>(nullable: true),
                    DebtorCurrency = table.Column<string>(unicode: false, maxLength: 10, nullable: false, defaultValue: "LPS"),
                    GrossMonthlyIncome = table.Column<decimal>(nullable: false),
                    CoDebtorIdentificationCard = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    CoDebtorPhoneNumber = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    CoDebtorCellPhoneNumber = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    CoDebtorEmail = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    CoDebtorWorkplace = table.Column<string>(nullable: true),
                    CoDebtorCurrency = table.Column<string>(unicode: false, maxLength: 10, nullable: false, defaultValue: "LPS"),
                    CoDebtorGrossMonthlyIncome = table.Column<decimal>(nullable: false),
                    HomeAddress = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    OwnHome = table.Column<bool>(nullable: false),
                    ContributeToRap = table.Column<bool>(nullable: false),
                    ContactedByTypeId = table.Column<int>(nullable: false),
                    Comments = table.Column<string>(nullable: true),
                    ClientCreationComments = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    CreditOfficer = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientTransactions", x => x.UId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientTransactions");

            migrationBuilder.DropColumn(
                name: "ClientCreationComments",
                table: "Clients");

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);
        }
    }
}
