﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class AddedIndexesToClientTransactionsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "FullTextSearchField",
                table: "ClientTransactions",
                unicode: false,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "longtext CHARACTER SET utf8mb4",
                oldUnicode: false,
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ClientTransactions_FullTextSearchField",
                table: "ClientTransactions",
                column: "FullTextSearchField");

            migrationBuilder.CreateIndex(
                name: "IX_ClientTransactions_SalesAdvisorId",
                table: "ClientTransactions",
                column: "SalesAdvisorId");

            migrationBuilder.CreateIndex(
                name: "IX_ClientTransactions_SalesAdvisorId_FullTextSearchField",
                table: "ClientTransactions",
                columns: new[] { "SalesAdvisorId", "FullTextSearchField" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ClientTransactions_FullTextSearchField",
                table: "ClientTransactions");

            migrationBuilder.DropIndex(
                name: "IX_ClientTransactions_SalesAdvisorId",
                table: "ClientTransactions");

            migrationBuilder.DropIndex(
                name: "IX_ClientTransactions_SalesAdvisorId_FullTextSearchField",
                table: "ClientTransactions");


            migrationBuilder.AlterColumn<string>(
                name: "FullTextSearchField",
                table: "ClientTransactions",
                type: "longtext CHARACTER SET utf8mb4",
                unicode: false,
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldNullable: true);
        }
    }
}
