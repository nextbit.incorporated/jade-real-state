﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class UpdateRolIdToINT : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Users",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "UserRoles",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<int>(
                name: "RolId",
                table: "UserRoles",
                unicode: false,
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Roles",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "RolePermissions",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Projects",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<string>(
                name: "TransactionType",
                table: "ContactTypes",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ContactTypes",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ContactTypes",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "ModifiedBy",
                table: "ContactTypes",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 20);

            migrationBuilder.AlterColumn<string>(
                name: "CrudOperation",
                table: "ContactTypes",
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationDate",
                table: "ContactTypes",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime",
                oldDefaultValueSql: "NOW()");

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Clients",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Users",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "UserRoles",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<string>(
                name: "RolId",
                table: "UserRoles",
                unicode: false,
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(int),
                oldUnicode: false,
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Roles",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "RolePermissions",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Projects",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<string>(
                name: "TransactionType",
                table: "ContactTypes",
                unicode: false,
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ContactTypes",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ContactTypes",
                unicode: false,
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ModifiedBy",
                table: "ContactTypes",
                unicode: false,
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CrudOperation",
                table: "ContactTypes",
                unicode: false,
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreationDate",
                table: "ContactTypes",
                type: "datetime",
                nullable: false,
                defaultValueSql: "NOW()",
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Clients",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);
        }
    }
}
