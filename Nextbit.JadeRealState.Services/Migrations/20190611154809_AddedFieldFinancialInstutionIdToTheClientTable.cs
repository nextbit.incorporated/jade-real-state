﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class AddedFieldFinancialInstutionIdToTheClientTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FinancialInstitution",
                table: "Clients");

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Users",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "UserRoles",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "SupplierServices",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Roles",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "RolePermissions",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Prospects",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Projects",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Origins",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Nationalities",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "HouseDesign",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ContactTypes",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Clients",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AddColumn<int>(
                name: "FinancialInstitutionId",
                table: "Clients",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "BuilderCompany",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.CreateIndex(
                name: "IX_Clients_FinancialInstitutionId",
                table: "Clients",
                column: "FinancialInstitutionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_FinancialInstitutions_FinancialInstitutionId",
                table: "Clients",
                column: "FinancialInstitutionId",
                principalTable: "FinancialInstitutions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_FinancialInstitutions_FinancialInstitutionId",
                table: "Clients");

            migrationBuilder.DropIndex(
                name: "IX_Clients_FinancialInstitutionId",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "FinancialInstitutionId",
                table: "Clients");

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Users",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "UserRoles",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "SupplierServices",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Roles",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "RolePermissions",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Prospects",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Projects",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Origins",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Nationalities",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "HouseDesign",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ContactTypes",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Clients",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AddColumn<string>(
                name: "FinancialInstitution",
                table: "Clients",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "BuilderCompany",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);
        }
    }
}
