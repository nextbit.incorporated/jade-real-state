﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class RemovedCodebtorFieldsCfronClientsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_Origins_CoDebtorNationalityId",
                table: "Clients");

            migrationBuilder.DropIndex(
                name: "IX_Clients_CoDebtorNationalityId",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorCellPhoneNumber",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorCurrency",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorEmail",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorFirstName",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorFirstSurname",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorGrossMonthlyIncome",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorIdentificationCard",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorMiddleName",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorNationalityId",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorPhoneNumber",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorSecondSurname",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CoDebtorWorkplace",
                table: "Clients");

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorCellPhoneNumber",
                table: "Clients",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorCurrency",
                table: "Clients",
                type: "varchar(10) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 10,
                nullable: false,
                defaultValue: "LPS");

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorEmail",
                table: "Clients",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorFirstName",
                table: "Clients",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorFirstSurname",
                table: "Clients",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "CoDebtorGrossMonthlyIncome",
                table: "Clients",
                type: "decimal(65,30)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorIdentificationCard",
                table: "Clients",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorMiddleName",
                table: "Clients",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CoDebtorNationalityId",
                table: "Clients",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorPhoneNumber",
                table: "Clients",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorSecondSurname",
                table: "Clients",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorWorkplace",
                table: "Clients",
                type: "longtext CHARACTER SET utf8mb4",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Clients_CoDebtorNationalityId",
                table: "Clients",
                column: "CoDebtorNationalityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_Origins_CoDebtorNationalityId",
                table: "Clients",
                column: "CoDebtorNationalityId",
                principalTable: "Origins",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
