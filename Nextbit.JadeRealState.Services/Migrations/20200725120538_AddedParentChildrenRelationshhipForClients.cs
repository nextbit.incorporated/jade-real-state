﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class AddedParentChildrenRelationshhipForClients : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<int>(
                name: "ParentClientId",
                table: "Clients",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Clients_ParentClientId",
                table: "Clients",
                column: "ParentClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_Clients_ParentClientId",
                table: "Clients",
                column: "ParentClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_Clients_ParentClientId",
                table: "Clients");

            migrationBuilder.DropIndex(
                name: "IX_Clients_ParentClientId",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "ParentClientId",
                table: "Clients");

        }
    }
}
