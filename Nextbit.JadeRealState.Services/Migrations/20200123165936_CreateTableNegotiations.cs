﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class CreateTableNegotiations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Users",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "UserRoles",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "SupplierServices",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceProcesses",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Roles",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "RolePermissions",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Reserves",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Prospects",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Projects",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Origins",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Nationalities",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "HouseDesign",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ContactTypes",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ClientsTracking",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Clients",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ClientLevels",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "BuilderCompany",
                fixedLength: true,
                maxLength: 8,
                rowVersion: true,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.CreateTable(
                name: "Negotiations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false),
                    ClientId = table.Column<int>(nullable: false),
                    ActualWorkPlace = table.Column<string>(unicode: false, maxLength: 150, nullable: true),
                    WorkStartDate = table.Column<DateTime>(nullable: true),
                    HomeAddress = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    NegotiationStartDate = table.Column<DateTime>(nullable: false),
                    OwnHome = table.Column<bool>(unicode: false, nullable: false),
                    ContributeToRap = table.Column<bool>(unicode: false, nullable: false),
                    Comments = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    CreditOfficer = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    SalesAdvisorId = table.Column<int>(nullable: true),
                    FinancialInstitutionId = table.Column<int>(nullable: true),
                    ContactTypeId = table.Column<int>(nullable: true),
                    ProjectId = table.Column<int>(nullable: true),
                    CoDebtorFirstName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoDebtorMiddleName = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoDebtorFirstSurname = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoDebtorSecondSurname = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoDebtorIdentificationCard = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoDebtorPhoneNumber = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoDebtorCellPhoneNumber = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoDebtorEmail = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoDebtorWorkplace = table.Column<string>(unicode: false, maxLength: 100, nullable: true),
                    CoDebtorCurrency = table.Column<string>(unicode: false, maxLength: 10, nullable: false, defaultValue: "LPS"),
                    CoDebtorGrossMonthlyIncome = table.Column<decimal>(nullable: false),
                    CoDebtorNationalityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Negotiations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Negotiations_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Negotiations_Origins_CoDebtorNationalityId",
                        column: x => x.CoDebtorNationalityId,
                        principalTable: "Origins",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Negotiations_ContactTypes_ContactTypeId",
                        column: x => x.ContactTypeId,
                        principalTable: "ContactTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Negotiations_FinancialInstitutions_FinancialInstitutionId",
                        column: x => x.FinancialInstitutionId,
                        principalTable: "FinancialInstitutions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Negotiations_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Negotiations_Users_SalesAdvisorId",
                        column: x => x.SalesAdvisorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_ClientId",
                table: "Negotiations",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_CoDebtorNationalityId",
                table: "Negotiations",
                column: "CoDebtorNationalityId");

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_ContactTypeId",
                table: "Negotiations",
                column: "ContactTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_FinancialInstitutionId",
                table: "Negotiations",
                column: "FinancialInstitutionId");

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_ProjectId",
                table: "Negotiations",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_SalesAdvisorId",
                table: "Negotiations",
                column: "SalesAdvisorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Negotiations");

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Users",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "UserRoles",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "SupplierServices",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceProcesses",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Roles",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "RolePermissions",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Reserves",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Prospects",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Projects",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Origins",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Nationalities",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "HouseDesign",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ContactTypes",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ClientsTracking",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "Clients",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ClientLevels",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "BuilderCompany",
                fixedLength: true,
                maxLength: 8,
                nullable: false,
                oldClrType: typeof(DateTime),
                oldFixedLength: true,
                oldMaxLength: 8,
                oldRowVersion: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);
        }
    }
}
