﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class CreatedTableForContractSigning : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AddColumn<int>(
                name: "HouseDesignId",
                table: "Negotiations",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.CreateTable(
                name: "NegotiationContracts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(nullable: true),
                    CrudOperation = table.Column<string>(nullable: true),
                    RowVersion = table.Column<DateTime>(rowVersion: true, nullable: true)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    NegotiationId = table.Column<int>(nullable: false),
                    BuilderCompanyId = table.Column<int>(nullable: true),
                    ContractSigningDate = table.Column<DateTime>(nullable: true),
                    EffectiveDate = table.Column<DateTime>(nullable: true),
                    BlockNumber = table.Column<string>(nullable: true),
                    LotNumber = table.Column<string>(nullable: true),
                    ConstructionMeters = table.Column<decimal>(nullable: false),
                    LandSize = table.Column<decimal>(nullable: false),
                    Registration = table.Column<string>(nullable: true),
                    ReservationValue = table.Column<decimal>(nullable: false),
                    DownPaymentValue = table.Column<decimal>(nullable: false),
                    DownPaymentMethod = table.Column<string>(nullable: true),
                    LandValue = table.Column<decimal>(nullable: false),
                    ConstructionValue = table.Column<decimal>(nullable: false),
                    ImprovementsValue = table.Column<decimal>(nullable: false),
                    HouseTotalValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NegotiationContracts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NegotiationContracts_BuilderCompany_BuilderCompanyId",
                        column: x => x.BuilderCompanyId,
                        principalTable: "BuilderCompany",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_NegotiationContracts_Negotiations_NegotiationId",
                        column: x => x.NegotiationId,
                        principalTable: "Negotiations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_HouseDesignId",
                table: "Negotiations",
                column: "HouseDesignId");

            migrationBuilder.CreateIndex(
                name: "IX_HouseDesign_ProjectId",
                table: "HouseDesign",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_NegotiationContracts_BuilderCompanyId",
                table: "NegotiationContracts",
                column: "BuilderCompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_NegotiationContracts_NegotiationId",
                table: "NegotiationContracts",
                column: "NegotiationId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_HouseDesign_Projects_ProjectId",
                table: "HouseDesign",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Negotiations_HouseDesign_HouseDesignId",
                table: "Negotiations",
                column: "HouseDesignId",
                principalTable: "HouseDesign",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HouseDesign_Projects_ProjectId",
                table: "HouseDesign");

            migrationBuilder.DropForeignKey(
                name: "FK_Negotiations_HouseDesign_HouseDesignId",
                table: "Negotiations");

            migrationBuilder.DropTable(
                name: "NegotiationContracts");

            migrationBuilder.DropIndex(
                name: "IX_Negotiations_HouseDesignId",
                table: "Negotiations");

            migrationBuilder.DropIndex(
                name: "IX_HouseDesign_ProjectId",
                table: "HouseDesign");

            migrationBuilder.DropColumn(
                name: "HouseDesignId",
                table: "Negotiations");

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);
        }
    }
}
