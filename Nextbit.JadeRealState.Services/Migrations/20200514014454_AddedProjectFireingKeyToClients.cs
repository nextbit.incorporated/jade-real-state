﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class AddedProjectFireingKeyToClients : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_Nationalities_NationalityId1",
                table: "Clients");

            migrationBuilder.DropTable(
                name: "Nationalities");

            migrationBuilder.DropIndex(
                name: "IX_Clients_NationalityId1",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "NationalityId1",
                table: "Clients");


            migrationBuilder.CreateIndex(
                name: "IX_Clients_ProjectId",
                table: "Clients",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_Projects_ProjectId",
                table: "Clients",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clients_Projects_ProjectId",
                table: "Clients");

            migrationBuilder.DropIndex(
                name: "IX_Clients_ProjectId",
                table: "Clients");


            migrationBuilder.AddColumn<int>(
                name: "NationalityId1",
                table: "Clients",
                nullable: true);


            migrationBuilder.CreateTable(
                name: "Nationalities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    TransactionUId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Nationalities", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Clients_NationalityId1",
                table: "Clients",
                column: "NationalityId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_Nationalities_NationalityId1",
                table: "Clients",
                column: "NationalityId1",
                principalTable: "Nationalities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
