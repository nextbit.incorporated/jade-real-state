﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class RenameWorkplaceFieldOnNegotiationClientsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WorkPlace",
                table: "NegotiationClients");

            migrationBuilder.AddColumn<string>(
                name: "Workplace",
                table: "NegotiationClients",
                unicode: false,
                maxLength: 256,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Workplace",
                table: "NegotiationClients");

            migrationBuilder.AddColumn<string>(
                name: "WorkPlace",
                table: "NegotiationClients",
                unicode: false,
                maxLength: 256,
                nullable: true);
        }
    }
}
