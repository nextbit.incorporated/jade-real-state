﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nextbit.JadeRealState.Services.Migrations
{
    public partial class CreatedTableNegotiationClients : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Negotiations_Clients_ClientId",
                table: "Negotiations");

            migrationBuilder.DropForeignKey(
                name: "FK_Negotiations_Clients_CoDebtorClientId",
                table: "Negotiations");

            migrationBuilder.DropIndex(
                name: "IX_Negotiations_CoDebtorClientId",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "ActualWorkPlace",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorActualWorkPlace",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorClientId",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorContributeToRap",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorCurrency",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorGrossMonthlyIncome",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorHomeAddress",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorOwnHome",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "CoDebtorWorkStartDate",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "ContributeToRap",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "Currency",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "GrossMonthlyIncome",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "HomeAddress",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "OwnHome",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "WorkStartDate",
                table: "Negotiations");

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<int>(
                name: "ClientId",
                table: "Negotiations",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "FinancialPrequalificationId",
                table: "Negotiations",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                rowVersion: true,
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp(6)",
                oldNullable: true)
                .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AddColumn<bool>(
                name: "FirstHome",
                table: "Clients",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Relationship",
                table: "Clients",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Titular",
                table: "Clients",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "FinancialPrequalifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    NegotiationId = table.Column<int>(nullable: false),
                    FundTypeId = table.Column<int>(nullable: true),
                    FinancialApprovalId = table.Column<int>(nullable: false),
                    InterestRate = table.Column<decimal>(nullable: false),
                    LoanAmount = table.Column<decimal>(nullable: false),
                    LoanTerm = table.Column<int>(nullable: false),
                    DownPaymentPercent = table.Column<decimal>(nullable: false),
                    DownPayment = table.Column<decimal>(nullable: false),
                    MonthlyPayment = table.Column<decimal>(nullable: false),
                    Comments = table.Column<string>(type: "longtext", unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinancialPrequalifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FinancialPrequalifications_FundTypes_FundTypeId",
                        column: x => x.FundTypeId,
                        principalTable: "FundTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_FinancialPrequalifications_Negotiations_NegotiationId",
                        column: x => x.NegotiationId,
                        principalTable: "Negotiations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "NegotiationClients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    NegotiationId = table.Column<int>(nullable: false),
                    ClientId = table.Column<int>(nullable: false),
                    Titular = table.Column<bool>(nullable: false),
                    Relationship = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    WorkPlace = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    WorkStartDate = table.Column<DateTime>(nullable: true),
                    HomeAddress = table.Column<string>(unicode: false, maxLength: 256, nullable: true),
                    OwnHome = table.Column<bool>(unicode: false, nullable: false),
                    FirstHome = table.Column<bool>(unicode: false, nullable: false),
                    ContributeToRap = table.Column<bool>(unicode: false, nullable: false),
                    Currency = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    GrossMonthlyIncome = table.Column<decimal>(nullable: false),
                    PreQualify = table.Column<bool>(nullable: false),
                    PrequalificationDate = table.Column<DateTime>(nullable: true),
                    NextPrequalificationDate = table.Column<DateTime>(nullable: true),
                    PrequalificationAmount = table.Column<decimal>(nullable: false),
                    HasDebts = table.Column<bool>(nullable: false),
                    DebtsAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NegotiationClients", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NegotiationClients_Clients_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NegotiationClients_Negotiations_NegotiationId",
                        column: x => x.NegotiationId,
                        principalTable: "Negotiations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FinancialApprovals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "NOW()"),
                    TransactionUId = table.Column<Guid>(nullable: false),
                    ModifiedBy = table.Column<string>(unicode: false, maxLength: 20, nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    CrudOperation = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    RowVersion = table.Column<DateTime>(fixedLength: true, maxLength: 8, rowVersion: true, nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
                    FinancialPrequalificationId = table.Column<int>(nullable: false),
                    Jointloan = table.Column<bool>(nullable: false),
                    ApprovalState = table.Column<string>(nullable: false),
                    ApprovalDate = table.Column<DateTime>(nullable: true),
                    RejectionDate = table.Column<DateTime>(nullable: true),
                    RejectionReason = table.Column<string>(type: "longtext", unicode: false, nullable: true),
                    CreditOfficerComments = table.Column<string>(type: "longtext", unicode: false, nullable: true),
                    SalesAdvisorComments = table.Column<string>(type: "longtext", unicode: false, nullable: true),
                    FollowUp = table.Column<string>(type: "longtext", unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FinancialApprovals", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FinancialApprovals_FinancialPrequalifications_FinancialPrequ~",
                        column: x => x.FinancialPrequalificationId,
                        principalTable: "FinancialPrequalifications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FinancialApprovals_FinancialPrequalificationId",
                table: "FinancialApprovals",
                column: "FinancialPrequalificationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FinancialPrequalifications_FundTypeId",
                table: "FinancialPrequalifications",
                column: "FundTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_FinancialPrequalifications_NegotiationId",
                table: "FinancialPrequalifications",
                column: "NegotiationId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_NegotiationClients_ClientId",
                table: "NegotiationClients",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_NegotiationClients_NegotiationId",
                table: "NegotiationClients",
                column: "NegotiationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Negotiations_Clients_ClientId",
                table: "Negotiations",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Negotiations_Clients_ClientId",
                table: "Negotiations");

            migrationBuilder.DropTable(
                name: "FinancialApprovals");

            migrationBuilder.DropTable(
                name: "NegotiationClients");

            migrationBuilder.DropTable(
                name: "FinancialPrequalifications");

            migrationBuilder.DropColumn(
                name: "FinancialPrequalificationId",
                table: "Negotiations");

            migrationBuilder.DropColumn(
                name: "FirstHome",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "Relationship",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "Titular",
                table: "Clients");

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServiceTrackingItem",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "ServicesTracking",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.AlterColumn<int>(
                name: "ClientId",
                table: "Negotiations",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ActualWorkPlace",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorActualWorkPlace",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CoDebtorClientId",
                table: "Negotiations",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "CoDebtorContributeToRap",
                table: "Negotiations",
                type: "tinyint(1)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorCurrency",
                table: "Negotiations",
                type: "varchar(50) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "CoDebtorGrossMonthlyIncome",
                table: "Negotiations",
                type: "decimal(65,30)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoDebtorHomeAddress",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "CoDebtorOwnHome",
                table: "Negotiations",
                type: "tinyint(1)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CoDebtorWorkStartDate",
                table: "Negotiations",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ContributeToRap",
                table: "Negotiations",
                type: "tinyint(1)",
                unicode: false,
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "Negotiations",
                type: "varchar(50) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "GrossMonthlyIncome",
                table: "Negotiations",
                type: "decimal(65,30)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "HomeAddress",
                table: "Negotiations",
                type: "varchar(256) CHARACTER SET utf8mb4",
                unicode: false,
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "OwnHome",
                table: "Negotiations",
                type: "tinyint(1)",
                unicode: false,
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "WorkStartDate",
                table: "Negotiations",
                type: "datetime(6)",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RowVersion",
                table: "FinancialInstitutions",
                type: "timestamp(6)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldRowVersion: true,
                oldNullable: true)
                .OldAnnotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn);

            migrationBuilder.CreateIndex(
                name: "IX_Negotiations_CoDebtorClientId",
                table: "Negotiations",
                column: "CoDebtorClientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Negotiations_Clients_ClientId",
                table: "Negotiations",
                column: "ClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Negotiations_Clients_CoDebtorClientId",
                table: "Negotiations",
                column: "CoDebtorClientId",
                principalTable: "Clients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
