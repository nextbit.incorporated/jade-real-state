import React, { Component, lazy, Suspense, useState, useEffect } from 'react';
import { Redirect } from "react-router-dom";
import API from "./../../components/API/API";
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardTitle,
  Col,
  Row,
  Table,
  CardHeader,
} from "reactstrap";
import { Bar, Line, Pie } from "react-chartjs-2";
import { CustomTooltips } from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import { getStyle, hexToRgba } from "@coreui/coreui/dist/js/coreui-utilities";
import moment from "moment";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useRowState } from 'react-table';
const Widget03 = lazy(() => import('../../views/Widgets/Widget03'));

//Random Numbers
function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

var elements = 27;
var data1 = [];
var data2 = [];
var data3 = [];

for (var i = 0; i <= elements; i++) {
  data1.push(random(50, 200));
  data2.push(random(80, 100));
  data3.push(65);
}

const brandInfo = getStyle("--info");

const mainChartOpts1 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
    intersect: true,
    mode: "index",
    position: "nearest",
    callbacks: {
      labelColor: function (tooltipItem, chart) {
        return {
          backgroundColor:
            chart.data.datasets[tooltipItem.datasetIndex].borderColor,
        };
      },
    },
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          drawOnChartArea: false,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(100 / 5),
          max: 35,
        },
      },
    ],
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
};

const mainChart1 = {
  labels: [],
  datasets: [
    {
      label: "Cantidad de clientes: ",
      backgroundColor: hexToRgba(brandInfo, 10),
      borderColor: brandInfo,
      pointHoverBackgroundColor: "#fff",
      borderWidth: 2,
      data: [],
    },
  ],
};

const cardChartData4 = {
  labels: [],
  datasets: [
    {
      label: "Clientes por proyecto",
      backgroundColor: hexToRgba(brandInfo, 10),
      borderColor: "transparent",
      data: [],
    },
  ],
};


const cardChartDataLevelclient = {
  labels: [],
  datasets: [
    {
      label: "Clientes por nivel",
      backgroundColor: hexToRgba(brandInfo, 10),
      borderColor: "transparent",
      data: [],
    },
  ],
};

const cardChartDataLevelclient1 = {
  labels: [],
  datasets: [
    {
      label: "Clientes por categoria",
      backgroundColor: hexToRgba(brandInfo, 10),
      borderColor: "transparent",
      data: [],
    },
  ],
};

const cardChartOpts4 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: true,
        barPercentage: 0.6,
      },
    ],
    yAxes: [
      {
        display: true,
      },
    ],
  },
};

const cardChartData1 = {
  labels: [],
  datasets: [
    {
      label: "Clientes por tipo de contacto",
      backgroundColor: hexToRgba(brandInfo, 10),
      borderColor: "transparent",
      data: [],
    },
  ],
};

const cardChartOpts1 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: true,
        barPercentage: 0.6,
      },
    ],
    yAxes: [
      {
        display: true,
      },
    ],
  },
};


const cardChartDataNegotiation = {
  labels: [],
  datasets: [
    {
      label: "Negociacion por institucion financiera",
      backgroundColor: hexToRgba(brandInfo, 10),
      borderColor: "transparent",
      data: [],
    },
  ],
};

const cardChartDataNegotiationBySalesAdvisor = {
  labels: [],
  datasets: [
    {
      label: "Negociacion por usuario",
      backgroundColor: hexToRgba(brandInfo, 10),
      borderColor: "transparent",
      data: [],
    },
  ],
};
const cardChartOptsNegotiations = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
  },
  maintainAspectRatio: true,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: true,
        barPercentage: 0.6,
      },
    ],
    yAxes: [
      {
        display: true,
      },
    ],
  },
};

const DashboardData = (props) => {
  const [mainChart, setMainChart] = useState(mainChart1);
  const [cardChartData, setCardChartData] = useState(cardChartData4);
  const [charDataCientLevelsState, setCharDataCientLevelsState] = useState(cardChartDataLevelclient);
  const [charDataCientProjectLevelsState, setCharDataCientProjectLevelsState] = useState(cardChartDataLevelclient);
  const [cardChartDataCT, setCardChartDataCT] = useState(cardChartData1);
  const [AlarmNotificationsState, setAlarmNotificationsState] = useState([]);
  const [NegotiationPhasestate, setNegotiationPhasestate] = useState([]);
  const [NegotiationsStaticState, setNegotiationsStaticState] = useState(cardChartDataNegotiation);
  const [NegotiationsStatusState, setNegotiationsStatusState] = useState({});
  const [ClientCountState, setClientCountState] = useState(0);
  const [NegotiationsBySalesAdvisorState, setNegotiationsBySalesAdvisorState] = useState(cardChartDataNegotiationBySalesAdvisor);
  const [ClientLevelByUserIdState, setClientLevelByUserIdState] = useState([]);
  const [clientCategoriesStadisticsState, setClientCategoriesStadisticsState] = useState({});
  const [charDataCientCategoriesState, setCharDataCientCategoriesState] = useState(cardChartDataLevelclient1);
  const [cantidadMesesState, setCantidadMesesState] = useState(1);

  useEffect(() => {
    const token = sessionStorage.getItem("token");
    if (!token) {
      props.history.push("/login");
    } else {
      handleData();
      handleGetCountClients();
      handleDataClientLevelsByUserId();
      handleDataClientLevelsProjectByUserId();
      handleDataNegotiationStatus();
      handleDataClientsByProject();
      handleDataClientsByContactType();
      handleTopAlarmNotifications();
      handleTopNegotiationsPhases();
      handleDataStatictsNegotiation();
      handleDataStatictsNegotiationBySalesAdvisor();
      handleGetCountClientCategoriesStadisticts(1);
    }
  }, []);

  function onRadioBtnClick(number) {
    if (number === 12) {
      handleData();
      return;
    }
    if (number === 6) {
      get6monthsData();
      return;
    }
    if (number === 3) {
      get3monthsData();
      return;
    }
    if (number === 1) {
      get1monthData();
      return;
    }
  }

  function onRadioBtnClickCategoriaCliente(number) {
    setCantidadMesesState(number);
    setCharDataCientCategoriesState({})
    setClientCategoriesStadisticsState([]);
    handleGetCountClientCategoriesStadisticts(number);
  }

  function handleGetCountClientCategoriesStadisticts(number){
    const user = sessionStorage.getItem("userId");
    const url = `Clients/client-categories-stadistics?User=${user}&cantidadMeses=${number}`
    API.get(url).then((res) => {
      setClientCategoriesStadisticsState(res.data);

      const data1 = {
        labels: res.data.estadisticas.levels,
        datasets: [
          {
            label: "Clientes por Categoria",
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)',
            ],
            borderColor: "transparent",
            data: res.data.estadisticas.count,
          },
        ],

       
      };

      setCharDataCientCategoriesState(data1);
    });
  }


  function handleGetCountClients(){
    const url = 'Clients/get-statics-client'
    API.get(url).then((res) => {
      setClientCountState(res.data);
    });
  }

  function handleDataNegotiationStatus(){
    const url = 'Negotiation/statistics-negotiations-open'
    API.get(url).then((res) => {
      setNegotiationsStatusState(res.data);
    });
  }

  function handleTopAlarmNotifications() {
    const user = sessionStorage.getItem("userId");
    const url = `Alarm/notifications?User=${user}`;
    API.get(url).then((res) => {
      setAlarmNotificationsState(res.data);
    });
  }

  function handleTopNegotiationsPhases() {
    const user = sessionStorage.getItem("userId");
    const url = `NegotiationPhase/top?User=${user}`;
    API.get(url).then((res) => {
      setNegotiationPhasestate(res.data);
    });
  }

  function get6monthsData() {
    const url = `clients/count-client-by-date-six`;
    API.get(url)
      .then((res) => {
        const data1 = {
          labels: res.data.fechas.map((s) => moment(s).format("MMM Do YY")),
          datasets: [
            {
              label: "Cantidad de clientes: ",
              backgroundColor: hexToRgba(brandInfo, 10),
              borderColor: brandInfo,
              pointHoverBackgroundColor: "#fff",
              borderWidth: 2,
              data: res.data.countClients,
            },
          ],
        };
        setMainChart(data1);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }

  function get3monthsData() {
    const url = `clients/count-client-by-date-three`;
    API.get(url)
      .then((res) => {
        const data1 = {
          labels: res.data.fechas.map((s) => moment(s).format("MMM Do YY")),
          datasets: [
            {
              label: "Cantidad de clientes: ",
              backgroundColor: hexToRgba(brandInfo, 10),
              borderColor: brandInfo,
              pointHoverBackgroundColor: "#fff",
              borderWidth: 2,
              data: res.data.countClients,
            },
          ],
        };
        setMainChart(data1);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }
  function get1monthData() {
    const url = `clients/count-client-by-date-one`;
    API.get(url)
      .then((res) => {
        const data1 = {
          labels: res.data.fechas.map((s) => moment(s).format("MMM Do YY")),
          datasets: [
            {
              label: "Cantidad de clientes: ",
              backgroundColor: hexToRgba(brandInfo, 10),
              borderColor: brandInfo,
              pointHoverBackgroundColor: "#fff",
              borderWidth: 2,
              data: res.data.countClients,
            },
          ],
        };
        setMainChart(data1);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }

  function handleData() {
    const login = sessionStorage.getItem("login");
    if (login === false || login === undefined || login === null) {
      sessionStorage.removeItem("login");
      sessionStorage.removeItem("token");
      props.history.push("/login");
      return;
    }
    const url = `clients/count-client-by-date`;
    API.get(url)
      .then((res) => {
        const data1 = {
          labels: res.data.fechas.map((s) => moment(s).format("MMM Do YY")),
          datasets: [
            {
              label: "Cantidad de clientes: ",
              backgroundColor: hexToRgba(brandInfo, 10),
              borderColor: brandInfo,
              pointHoverBackgroundColor: "#fff",
              borderWidth: 2,
              data: res.data.countClients,
            },
          ],
        };
        setMainChart(data1);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }

  

  function handleDataClientLevelsProjectByUserId() {
    const login = sessionStorage.getItem("login");
    if (login === false || login === undefined || login === null) {
      sessionStorage.removeItem("login");
      sessionStorage.removeItem("token");
      props.history.push("/login");
      return;
    }
    const user = sessionStorage.getItem("userId");
    const url = `Clients/project-clients-by-level?User=${user}`;

    API.get(url)
      .then((res) => {
        setCharDataCientProjectLevelsState(res.data);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }


  function handleDataClientLevelsByUserId() {
    const login = sessionStorage.getItem("login");
    if (login === false || login === undefined || login === null) {
      sessionStorage.removeItem("login");
      sessionStorage.removeItem("token");
      props.history.push("/login");
      return;
    }
    const user = sessionStorage.getItem("userId");
    const url = `Clients/clients-by-level?User=${user}`;

    API.get(url)
      .then((res) => {
        const data1 = {
          labels: res.data.levels,
          datasets: [
            {
              label: "Clientes por nivel",
              backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
              ],
              borderColor: "transparent",
              data: res.data.count,
            },
          ],
        };
        setCharDataCientLevelsState(data1);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }

  function handleDataClientsByProject() {
    const login = sessionStorage.getItem("login");
    if (login === false || login === undefined || login === null) {
      sessionStorage.removeItem("login");
      sessionStorage.removeItem("token");
      props.history.push("/login");
      return;
    }
    const url = `clients/count-client-by-project`;
    API.get(url)
      .then((res) => {
        const data1 = {
          labels: res.data.projects,
          datasets: [
            {
              label: "Clientes por proyecto",
              backgroundColor: "rgba(44, 130, 201, 1)",
              borderColor: "transparent",
              data: res.data.countClients,
            },
          ],
        };
        setCardChartData(data1);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }


  function handleDataStatictsNegotiationBySalesAdvisor() {
    const login = sessionStorage.getItem("login");
    if (login === false || login === undefined || login === null) {
      sessionStorage.removeItem("login");
      sessionStorage.removeItem("token");
      props.history.push("/login");
      
      return;
    }
   

    const url = `Negotiation/statistics-negotiations-by-sa`;
    API.get(url)
      .then((res) => {
        const data1 = {
          labels: res.data.salesAdvisor,
          datasets: [
            {
              label: "Negocios",
              backgroundColor: "rgba(44, 130, 201, 1)",
              borderColor: "transparent",
              data: res.data.countNegotiations,
            },
          ],
        };
        setNegotiationsBySalesAdvisorState(data1);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }



  function handleDataStatictsNegotiation(){
    const login = sessionStorage.getItem("login");
    if (login === false || login === undefined || login === null) {
      sessionStorage.removeItem("login");
      sessionStorage.removeItem("token");
      props.history.push("/login");
      return;
    }

    const url = `Negotiation/statistics-negotiations-fi`;
    API.get(url)
      .then((res) => {
        const data1 = {
          labels: res.data.financialInstitutions,
          datasets: [
            {
              label: "Negocios",
              backgroundColor: "rgba(44, 130, 201, 1)",
              borderColor: "transparent",
              data: res.data.countClients,
            },
          ],
        };
        setNegotiationsStaticState(data1);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }

  function handleDataClientsByContactType() {
    const login = sessionStorage.getItem("login");
    if (login === false || login === undefined || login === null) {
      sessionStorage.removeItem("login");
      sessionStorage.removeItem("token");
      props.history.push("/login");
      return;
    }
    const url = `clients/count-client-by-contact-type`;
    API.get(url)
      .then((res) => {
        const data1 = {
          labels: res.data.projects,
          datasets: [
            {
              label: "Clientes",
              backgroundColor: "rgba(44, 130, 201, 1)",
              borderColor: "transparent",
              data: res.data.countClients,
            },
          ],
        };
        setCardChartDataCT(data1);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
    textAlign: "center",
  };

  function ObtenerDetalleAlarms() {
    if (AlarmNotificationsState) {
      return (
        <tbody>
          {AlarmNotificationsState.map((detalle) => (
            <tr key={detalle.id}>
              <td align="center">
                {moment(detalle.date).format("YYYY-MM-DD")}
              </td>
              <td align="center">{detalle.clientId}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.clientName}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.information}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.salesAdvisor}</td>
            </tr>
          ))}
        </tbody>
      );
    }
    return <tbody></tbody>;
  }

  function ObtenerDetalleClientesCategoria() {
    if (clientCategoriesStadisticsState.clasificaiones) {
      return (
        <tbody>
          {clientCategoriesStadisticsState.clasificaiones.map((detalle) => (
            <tr key={detalle.agente}>
              <td align="center">{detalle.calisificacion ?? "Sin Categorizar"}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.agente ?? "Sin Asignar"}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.total}</td>
            </tr>
          ))}
        </tbody>
      );
    }
    return <tbody></tbody>;
  }

  const ObtenerListaDeNotificaciones = 
    (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <th style={thStyle}>Ultimo contacto</th>
            <th style={thStyle}>Codigo Cliente</th>
            <th style={thStyle}>Nombre Cliente</th>
            <th style={thStyle}>Recordatorio</th>
            <th style={thStyle}>Agente</th>
          </tr>
        </thead>
        {ObtenerDetalleAlarms()}
      </Table>
    );

  function ObtenerDetalleProcesos() {
    if (NegotiationPhasestate) {
      return (
        <tbody>
          {NegotiationPhasestate.map((detalle) => (
            <tr key={detalle.negotiationId}>
              <td align="center">{detalle.negotiationId}</td>
              <td align="center">{detalle.clientId}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.clientName}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.processName}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.status}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.salesAdvisor}</td>
            </tr>
          ))}
        </tbody>
      );
    }
    return <tbody></tbody>;
  }

  const ObtenerListaProcesos = (
  
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <th style={thStyle}>Codigo Negocio</th>
            <th style={thStyle}>Codigo Cliente</th>
            <th style={thStyle}>Nombre Cliente</th>
            <th style={thStyle}>Proceso</th>
            <th style={thStyle}>Status</th>
            <th style={thStyle}>Asesor</th>
          </tr>
        </thead>
        {ObtenerDetalleProcesos()}
      </Table>
    );

    const optionsProjectClients = {
      responsive: false,
      scales: {
        xAxes: [
          {
            gridLines: {
              display: true,
              drawBorder: false,
              borderDash: [1, 1],
              zeroLineColor: "blue"
            },
            categoryPercentage: 0.7,
            barPercentage: 1.0,
            ticks: {
              beginAtZero: true
            }
          }
        ],
        yAxes: [
          {
            display: true,
            gridLines: {
              display: true,
              zeroLineColor: "transparent"
            },
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    };


  function ObtenerDashboardPorperfil() {
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      return(
        <div className="animated fadeIn">
      <Tabs>
        <TabList>
          <Tab>Informacion importante</Tab>
          <Tab>Datos estadisticos</Tab>
        </TabList>
        <TabPanel>
          <Row>
            <Col md="6" sm="6" xs="12">
              <Card>
                <CardHeader>
                  <strong>Clientes con recordatorio</strong>
                </CardHeader>
                <CardBody>{ObtenerListaDeNotificaciones}</CardBody>
              </Card>
            </Col>
            <Col md="6" sm="6" xs="12">
              <Card>
                <CardHeader>
                  <strong>Clientes procesos pendientes mas antiguos</strong>
                </CardHeader>
                <CardBody>{ObtenerListaProcesos}</CardBody>
              </Card>
            </Col>
          </Row>
        </TabPanel>
        <TabPanel>
        <Row>
            <Col md="6" sm="6" xs="12">
              <Card>
                <CardBody>
                    <Row>
                        <Col sm="5">
                          <CardTitle className="mb-0">
                            <strong>Resumen Clientes hasta el cierre mes anterior</strong>
                          </CardTitle>
                        </Col>
                      </Row>
                      <Row>
                        <Col sm='5'>
                        <Table hover bordered striped responsive size="sm">
                          <thead>
                            <tr>
                              <th style={thStyle}>Clasificacion</th>
                              <th style={thStyle}>Agente</th>
                              <th style={thStyle}>Cantidad</th>
                            </tr>
                          </thead>
                          {ObtenerDetalleClientesCategoria()}
                        </Table>
                        </Col>
                      </Row>
                </CardBody>
              </Card>
            </Col>
            <Col md="6" sm="3" xs="12">
                <Card>
                   <CardBody>
                      <Row>
                            <Col sm="6">
                              <CardTitle className="mb-0">
                                <strong>Resumen Clientes hasta el cierre mes anterior</strong>
                              </CardTitle>
                            </Col>
                          </Row>
                          <Row>
                        
                          <Pie
                            data={charDataCientCategoriesState}
                            height={400}
                            options={{
                              legend: { display: true, position: "top" },
                              
                              datalabels: {
                                display: true,
                                color: "white",
                              },
                              tooltips: {
                                backgroundColor: "#5a6e7f",
                              },
                            }}
                          />
                   
                          </Row>
                   </CardBody>
                </Card>
            </Col>
          </Row>
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Registro de clientes por fecha</strong>
                      </CardTitle>
                      <div className="small text-muted">
                        Cantidad de clientes por fecha
                      </div>
                    </Col>
                    <Col sm="7" className="d-none d-sm-inline-block">
                      <ButtonToolbar
                        className="float-right"
                        aria-label="Toolbar with button groups"
                      >
                        <ButtonGroup className="mr-3" aria-label="First group">
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClick(12)}
                          >
                            <strong>1 año</strong>
                          </Button>
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClick(6)}
                          >
                            <strong>6 Meses</strong>
                          </Button>
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClick(3)}
                          >
                            <strong>3 Meses</strong>
                          </Button>
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClick(1)}
                          >
                            <strong>1 Mes</strong>
                          </Button>
                        </ButtonGroup>
                      </ButtonToolbar>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 300 + "px", marginTop: 40 + "px" }}
                  >
                    <Line
                      data={mainChart}
                      options={mainChartOpts1}
                      height={300}
                    />
                  </div>
                </CardBody>
                <CardFooter></CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6" lg="6">
            <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Clientes por nivel</strong>
                      </CardTitle>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 450 + "px", marginTop: 40 + "px" }}
                  >
                    <Pie
                      data={charDataCientLevelsState}
                      height={150}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col xs="12" sm="6" lg="6">
            <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Categoria clientes por proyecto</strong>
                      </CardTitle>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 450 + "px", marginTop: 40 + "px" }}
                  >
                    <Bar
                      data={charDataCientProjectLevelsState}
                      options={optionsProjectClients}
                      height={400}
                      width={700}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6" lg="6">
              <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Clientes por proyecto</strong>
                      </CardTitle>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 300 + "px", marginTop: 40 + "px" }}
                  >
                    <Bar
                      data={cardChartData}
                      options={cardChartOpts4}
                      height={150}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col xs="12" sm="6" lg="6">
              <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Clientes por tipo de contacto</strong>
                      </CardTitle>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 300 + "px", marginTop: 40 + "px" }}
                  >
                    <Bar
                      data={cardChartDataCT}
                      options={cardChartOpts1}
                      height={150}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6" lg="6">
              <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Negocios por Instituciones Financieras</strong>
                      </CardTitle>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 400 + "px", marginTop: 40 + "px" }}
                  >
                    <Bar
                      data={NegotiationsStaticState}
                      options={cardChartOptsNegotiations}
                      height={150}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </TabPanel>
      </Tabs>
      <ToastContainer />
    </div>
      );
    }
    else
    {
      return (
        <div className="animated fadeIn">
      <Tabs>
        <TabList>
          <Tab>Informacion importante</Tab>
          <Tab>Datos estadisticos</Tab>
        </TabList>
        <TabPanel>
          <Row>
            <Col md="6" sm="6" xs="12">
              <Card>
                <CardHeader>
                  <strong>Clientes con recordatorio</strong>
                </CardHeader>
                <CardBody>{ObtenerListaDeNotificaciones}</CardBody>
              </Card>
            </Col>
            <Col md="6" sm="6" xs="12">
              <Card>
                <CardHeader>
                  <strong>Clientes procesos pendientes mas antiguos</strong>
                </CardHeader>
                <CardBody>{ObtenerListaProcesos}</CardBody>
              </Card>
            </Col>
          </Row>
        </TabPanel>
        <TabPanel>
          <Row>
            <Col xs="6" sm="6" lg="3">
              <Suspense fallback={true}>
                <Widget03 dataBox={() => ({ variant: 'briefcase', Abiertos: NegotiationsStatusState.opens, Cerrados: NegotiationsStatusState.close })} >
                </Widget03>
              </Suspense>
            </Col>
            <Col xs="6" sm="6" lg="3">
                <Card className="text-white bg-info" style={{ height: '150px'}}>
                  <CardBody className="pb-0">
                  <div><h2 style={{ textAlign: 'center' }}>Clientes</h2></div>
                  <div style={{ height: '30px'}}></div>
                  <div className="text-value"><h1 style={{ textAlign: 'center' }}><strong><u>{ClientCountState}</u></strong></h1></div>
                  </CardBody>
                </Card>
              </Col>
          </Row>
          <Row>
            <Col md="6" sm="3" xs="12">
              <Card>
                <CardBody>
                    <Row>
                        <Col sm="6">
                          <CardTitle className="mb-0">
                            <strong>Resumen Clientes hasta el cierre mes anterior</strong>
                          </CardTitle>
                        </Col>
                      </Row>
                      <Row>
                        <Col sm='6'>
                        <Table hover bordered striped responsive size="sm">
                          <thead>
                            <tr>
                              <th style={thStyle}>Clasificacion</th>
                              <th style={thStyle}>Agente</th>
                              <th style={thStyle}>Cantidad</th>
                            </tr>
                          </thead>
                          {ObtenerDetalleClientesCategoria()}
                        </Table>
                        </Col>
                      </Row>
                </CardBody>
              </Card>
            </Col>
            <Col md="6" sm="3" xs="12">
                <Card>
                   <CardBody>
                      <Row>
                            <Col sm="6">
                              <CardTitle className="mb-0">
                                <strong>Resumen Clientes por categoria en {cantidadMesesState} meses</strong>
                              </CardTitle>
                            </Col>
                      <Col sm="7" className="d-none d-sm-inline-block">
                      <ButtonToolbar
                        className="float-right"
                        aria-label="Toolbar with button groups"
                      >
                       <ButtonGroup className="mr-3" aria-label="First group">
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClickCategoriaCliente(12)}
                          >
                            <strong>1 año</strong>
                          </Button>
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClickCategoriaCliente(6)}
                          >
                            <strong>6 Meses</strong>
                          </Button>
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClickCategoriaCliente(3)}
                          >
                            <strong>3 Meses</strong>
                          </Button>
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClickCategoriaCliente(1)}
                          >
                            <strong>1 Mes</strong>
                          </Button>
                        </ButtonGroup>
                      </ButtonToolbar>
                    </Col>
                          </Row>
                          <Row>
                        
                          <Pie
                            data={charDataCientCategoriesState}
                            height={400}
                            options={{
                              legend: { display: true, position: "top" },
                              
                              datalabels: {
                                display: true,
                                color: "white",
                              },
                              tooltips: {
                                backgroundColor: "#5a6e7f",
                              },
                            }}
                          />
                   
                          </Row>
                   </CardBody>
                </Card>
            </Col>
          </Row>
          <Row>
            <Col>
              <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Registro de clientes por fecha</strong>
                      </CardTitle>
                      <div className="small text-muted">
                        Cantidad de clientes por fecha
                      </div>
                    </Col>
                    <Col sm="7" className="d-none d-sm-inline-block">
                      <ButtonToolbar
                        className="float-right"
                        aria-label="Toolbar with button groups"
                      >
                        <ButtonGroup className="mr-3" aria-label="First group">
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClick(12)}
                          >
                            <strong>1 año</strong>
                          </Button>
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClick(6)}
                          >
                            <strong>6 Meses</strong>
                          </Button>
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClick(3)}
                          >
                            <strong>3 Meses</strong>
                          </Button>
                          <Button
                            color="primary"
                            onClick={() => onRadioBtnClick(1)}
                          >
                            <strong>1 Mes</strong>
                          </Button>
                        </ButtonGroup>
                      </ButtonToolbar>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 300 + "px", marginTop: 40 + "px" }}
                  >
                    <Line
                      data={mainChart}
                      options={mainChartOpts1}
                      height={300}
                    />
                  </div>
                </CardBody>
                <CardFooter></CardFooter>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6" lg="6">
            <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Clientes por nivel</strong>
                      </CardTitle>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 450 + "px", marginTop: 40 + "px" }}
                  >
                    <Pie
                      data={charDataCientLevelsState}
                      height={150}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col xs="12" sm="6" lg="6">
            <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Categoria clientes por proyecto</strong>
                      </CardTitle>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 450 + "px", marginTop: 40 + "px" }}
                  >
                    <Bar
                      data={charDataCientProjectLevelsState}
                      options={optionsProjectClients}
                      height={400}
                      width={700}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6" lg="6">
              <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Clientes por proyecto</strong>
                      </CardTitle>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 300 + "px", marginTop: 40 + "px" }}
                  >
                    <Bar
                      data={cardChartData}
                      options={cardChartOpts4}
                      height={150}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col xs="12" sm="6" lg="6">
              <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Clientes por tipo de contacto</strong>
                      </CardTitle>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 300 + "px", marginTop: 40 + "px" }}
                  >
                    <Bar
                      data={cardChartDataCT}
                      options={cardChartOpts1}
                      height={150}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col xs="12" sm="6" lg="6">
              <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Negocios por Instituciones Financieras</strong>
                      </CardTitle>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 400 + "px", marginTop: 40 + "px" }}
                  >
                    <Bar
                      data={NegotiationsStaticState}
                      options={cardChartOptsNegotiations}
                      height={150}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col xs="12" sm="6" lg="6">
              <Card>
                <CardBody>
                  <Row>
                    <Col sm="5">
                      <CardTitle className="mb-0">
                        <strong>Negocios por Agente/Usuario</strong>
                      </CardTitle>
                    </Col>
                  </Row>
                  <div
                    className="chart-wrapper"
                    style={{ height: 400 + "px", marginTop: 40 + "px" }}
                  >
                    <Bar
                      data={NegotiationsBySalesAdvisorState}
                      options={cardChartOptsNegotiations}
                      height={150}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </TabPanel>
      </Tabs>
      <ToastContainer />
    </div>
      );
    }
  }

  return (
    ObtenerDashboardPorperfil()
  );
};

export default DashboardData;
