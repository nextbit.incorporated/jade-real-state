import React, { useState, useEffect } from "react";
import API from "./../../components/API/API";
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardTitle,
  Col,
  Row,
} from "reactstrap";
import { Line } from "react-chartjs-2";
import { CustomTooltips } from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import { getStyle, hexToRgba } from "@coreui/coreui/dist/js/coreui-utilities";

//Random Numbers
function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

var elements = 27;
var data1 = [];
var data2 = [];
var data3 = [];

for (var i = 0; i <= elements; i++) {
  data1.push(random(50, 200));
  data2.push(random(80, 100));
  data3.push(65);
}

const brandInfo = getStyle("--info");

const initialCharts = {
  dropdownOpen: false,
  radioSelected: 2,
  dataClients: null,
  loading: true,
  mainChart: {
    labels: [],
    datasets: [
      {
        label: "My First dataset",
        backgroundColor: hexToRgba(brandInfo, 10),
        borderColor: brandInfo,
        pointHoverBackgroundColor: "#fff",
        borderWidth: 2,
        data: [],
      },
    ],
  },
};

const mainChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
    intersect: true,
    mode: "index",
    position: "nearest",
    callbacks: {
      labelColor: function (tooltipItem, chart) {
        return {
          backgroundColor:
            chart.data.datasets[tooltipItem.datasetIndex].borderColor,
        };
      },
    },
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          drawOnChartArea: false,
        },
      },
    ],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(100 / 5),
          max: 100,
        },
      },
    ],
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
};

const Dashboard = ({ props }) => {
  const [charts, setCharts] = useState(initialCharts);

  useEffect(() => {
    handleData();
    handleDataTrackings();
  }, []);

  const token = sessionStorage.getItem("token");
  if (!token) {
    sessionStorage.removeItem("login");
    sessionStorage.removeItem("token");
    props.history.push("/login");
    return;
  }

  const login = sessionStorage.getItem("login");
  if (login === false || login === undefined || login === null) {
    sessionStorage.removeItem("login");
    sessionStorage.removeItem("token");
    props.history.push("/login");
    return;
  }

  const handleData = () => {
    const url = `clients/count-client-by-date`;
    API.get(url).then((res) => {
      setCharts = {
        countClients: res.data,
      };

      const data1 = {
        labels: res.data.fechas,
        datasets: [
          {
            label: "My First dataset",
            backgroundColor: hexToRgba(brandInfo, 10),
            borderColor: brandInfo,
            pointHoverBackgroundColor: "#fff",
            borderWidth: 2,
            data: res.data.countClients,
          },
        ],
      };
      onClearArray(data1);
    });
  };

  const handleDataTrackings = () => {
    const url = `clientTracking/top-citas`;
    API.get(url).then((res) => {});
  };

  const onClearArray = (data1) => {
    onDataClient(data1);
  };

  const onDataClient = (data1) => {
    setCharts({ mainChart: data1 });
  };

  return (
    <div className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardBody>
              <Row>
                <Col sm="5">
                  <CardTitle className="mb-0">
                    Registro de clientes por fecha
                  </CardTitle>
                  <div className="small text-muted">
                    Cantidad de clientes por fecha
                  </div>
                </Col>
                <Col sm="7" className="d-none d-sm-inline-block">
                  <Button color="primary" className="float-right">
                    <i className="icon-cloud-download"></i>
                  </Button>
                  <ButtonToolbar
                    className="float-right"
                    aria-label="Toolbar with button groups"
                  >
                    <ButtonGroup
                      className="mr-3"
                      aria-label="First group"
                    ></ButtonGroup>
                  </ButtonToolbar>
                </Col>
              </Row>
              <div
                className="chart-wrapper"
                style={{ height: 300 + "px", marginTop: 40 + "px" }}
              >
                <Line
                  data={charts.mainChart}
                  options={mainChartOpts}
                  height={300}
                />
              </div>
            </CardBody>
            <CardFooter></CardFooter>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default Dashboard;
