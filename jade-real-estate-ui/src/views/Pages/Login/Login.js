import React, { Component } from "react";
import logo from "../../../assets/img/brand/logo.svg";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
} from "reactstrap";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import styled from "styled-components";

const ImageContainer = styled.div`
  background-color: white;
`;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: null,
      user: null,
      token: null,
      login: null,
    };
  }

  notify = () => toast("Wow so easy !");

  handleLogin = () => {
    const { user, password } = this.state;
    if (user === undefined || user === null) {
      toast.warn("Campo usuario obligatorio");
      return;
    }
    if (password === undefined || password === null) {
      toast.warn("Campo password obligatorio");
      return;
    }
    const request = {
      userId: user,
      password: password,
    };

    fetch(`${process.env.REACT_APP_API_URL}user/token`, {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        userId: request.userId,
        password: request.password,
      }),
    })
      .then((token) => token.json())
      .then((token) => {
        if (token === "Invalid grant username and/or password is incorrect") {
          toast.warn("Usuario o contraseña invalida!!");
          sessionStorage.setItem("login", false);
          sessionStorage.setItem("token", null);
          sessionStorage.setItem("userId", null);
          return;
        }
        if (token === "Error") {
          toast.error("Acceso no autorizado");
          sessionStorage.setItem("login", false);
          sessionStorage.setItem("token", null);
          sessionStorage.setItem("userId", null);
          return;
        }

        this.setState({ token });
        const requestUserInfo = {
          ApplicationId: "React",
          ComputerName: "ComputerName",
          UserId: request.userId,
          Token: token.token,
          Login: true,
        };
        this.setState({ login: true });
        this.setState({ token: requestUserInfo.Token });
        sessionStorage.requestUserInfo = JSON.stringify(requestUserInfo);
        sessionStorage.setItem("seguridad", requestUserInfo);
        sessionStorage.setItem("login", true);
        sessionStorage.setItem("token", requestUserInfo.Token);
        sessionStorage.setItem("userId", request.userId);
        sessionStorage.setItem("id", token.id);
        sessionStorage.setItem("rolId", token.rolId);
        sessionStorage.setItem("role", token.rol);

        // window.alert('I counted to infinity once then..');
        toast.success("Autenticado");
        this.props.history.push("/");
        return <Redirect to="/Dashboard" />;
      }).catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            toast.warn(
              error
            );
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  };

  handleUserdOnChange = (event) => {
    this.setState({ user: event.target.value });
  };

  handlePassOnChange = (event) => {
    this.setState({ password: event.target.value });
  };

  handleOnkeyUp = (e) => {
    if (e.keyCode === 13) {
      this.handleLogin();
    }
  };

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Autenticacion</h1>
                      <p className="text-muted">Acceso por usuario</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          autoFocus
                          type="text"
                          placeholder="Username"
                          autoComplete="username"
                          onChange={this.handleUserdOnChange}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          placeholder="Password"
                          autoComplete="current-password"
                          onChange={this.handlePassOnChange}
                          onKeyUp={this.handleOnkeyUp}
                        />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button
                            color="primary"
                            className="px-4"
                            onClick={this.handleLogin}
                          >
                            Login
                          </Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          {/* <Button color="link" className="px-0">Forgot password?</Button> */}
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card
                  className="text-white bg-primary py-5 d-md-down-none"
                  style={{ width: "44%" }}
                >
                  <CardBody className="text-center">
                    <ImageContainer>
                      <img src={logo} width="100%" alt="Logo" />
                    </ImageContainer>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
        <ToastContainer autoClose={5000} />
      </div>
    );
  }
}

export default Login;
