import React, { useState } from "react";
import { Container } from "semantic-ui-react";

const MainClientsForm = props => {
  const [step, setStep] = useState(1);

  const nextStep = step => {
    setStep(step + 1);
  };

  return <Container textAlign="center">{/* <MainForm /> */}</Container>;
};

export default MainClientsForm;
