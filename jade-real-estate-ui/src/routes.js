import React from "react";
import DefaultLayout from "./containers/DefaultLayout";
import PasswordReset from "./components/security/PasswordReset";
import PasswordMasterReset from "./components/security/MasterPasswordReset";
import ProspectByContactTypeReport from "./components/Reports/ProspectByContactTypeReport";

const MainClientsForm = React.lazy(() =>
  import("./components/Clients/MainClientsForm/MainClientsForm")
);

const CalendarEvents = React.lazy(() =>
  import("./components/CalendarEvents/CalendarEvents")
);

const clientsReport = React.lazy(() =>
  import("./components/Reports/ClientsReport/index")
);
const clientsTracking = React.lazy(() =>
  import("./components/ClientsTracking/index")
);
const clientsByUserReport = React.lazy(() =>
  import("./components/Reports/ClientsByUserReport/index")
);
const clientNegotiationsReport = React.lazy(() =>
  import("./components/Reports/ClientNegotiationsReport/index")
);
const generalInfo = React.lazy(() => import("./components/GeneralInfo/index"));
const prospects = React.lazy(() => import("./components/prospects/index"));
const userRol = React.lazy(() => import("./components/security/rolUser/index"));
const user = React.lazy(() => import("./components/security/user/index"));
const projects = React.lazy(() => import("./components/projects/index"));
const houseDesigns = React.lazy(() =>
  import("./components/houseDesigns/index")
);
const suplierService = React.lazy(() =>
  import("./components/SupplierServices/index")
);
const financials = React.lazy(() =>
  import("./components/FinancialInstitutions/index")
);
const meetingCategories = React.lazy(() =>
  import("./components/MeetingCategory/index")
);
const nationalities = React.lazy(() =>
  import("./components/nationalities/index")
);
const contactTypes = React.lazy(() =>
  import("./components/ContactTypes/index")
);
const clientLevel = React.lazy(() => import("./components/ClientLevels/index"));
const negotiation = React.lazy(() => import("./components/Negotiations/index"));
const reservas = React.lazy(() => import("./components/Reservas/index"));
const builders = React.lazy(() => import("./components/builders/index"));
const alarmReport = React.lazy(() =>
  import("./components/Reports/AlarmsReport/index")
);
const clientProjectReport = React.lazy(() =>
  import("./components/Reports/ClientsByProjectReport/index")
);
const negotiationsClosedReport = React.lazy(() =>
  import("./components/Reports/NegotiationClosedReport/index")
);
const prospectByContactTypeReport = React.lazy(() =>
  import("./components/Reports/ProspectByContactTypeReport/index")
);

const Breadcrumbs = React.lazy(() => import("./views/Base/Breadcrumbs"));
const Cards = React.lazy(() => import("./views/Base/Cards"));
const Carousels = React.lazy(() => import("./views/Base/Carousels"));
const Collapses = React.lazy(() => import("./views/Base/Collapses"));
const Dropdowns = React.lazy(() => import("./views/Base/Dropdowns"));
const Forms = React.lazy(() => import("./views/Base/Forms"));
const Jumbotrons = React.lazy(() => import("./views/Base/Jumbotrons"));
const ListGroups = React.lazy(() => import("./views/Base/ListGroups"));
const Navbars = React.lazy(() => import("./views/Base/Navbars"));
const Navs = React.lazy(() => import("./views/Base/Navs"));
const Paginations = React.lazy(() => import("./views/Base/Paginations"));
const Popovers = React.lazy(() => import("./views/Base/Popovers"));
const ProgressBar = React.lazy(() => import("./views/Base/ProgressBar"));
const Switches = React.lazy(() => import("./views/Base/Switches"));
const Tables = React.lazy(() => import("./views/Base/Tables"));
const Tabs = React.lazy(() => import("./views/Base/Tabs"));
const Tooltips = React.lazy(() => import("./views/Base/Tooltips"));
const BrandButtons = React.lazy(() => import("./views/Buttons/BrandButtons"));
const ButtonDropdowns = React.lazy(() =>
  import("./views/Buttons/ButtonDropdowns")
);
const ButtonGroups = React.lazy(() => import("./views/Buttons/ButtonGroups"));
const Buttons = React.lazy(() => import("./views/Buttons/Buttons"));
const Charts = React.lazy(() => import("./views/Charts"));
const DashboardData = React.lazy(() => import("./views/Dashboard/ppp"));
const CoreUIIcons = React.lazy(() => import("./views/Icons/CoreUIIcons"));
const Flags = React.lazy(() => import("./views/Icons/Flags"));
const FontAwesome = React.lazy(() => import("./views/Icons/FontAwesome"));
const SimpleLineIcons = React.lazy(() =>
  import("./views/Icons/SimpleLineIcons")
);
const Alerts = React.lazy(() => import("./views/Notifications/Alerts"));
const Badges = React.lazy(() => import("./views/Notifications/Badges"));
const Modals = React.lazy(() => import("./views/Notifications/Modals"));
const Colors = React.lazy(() => import("./views/Theme/Colors"));
const Typography = React.lazy(() => import("./views/Theme/Typography"));
const Widgets = React.lazy(() => import("./views/Widgets/Widgets"));

const ClientTransactions = React.lazy(() =>
  import("./components/Reports/ClientTransactions/ClientTransactions")
);
const ProspectReport = React.lazy(() =>
  import("./components/Reports/ProspectReport/index")
);
const NegotiationClientReport = React.lazy(() =>
  import("./components/Reports/NegotiationClientPhaseReport/index")
);
const SalesAdvisorStadistics = React.lazy(() =>import("./components/Reports/SalesAdvisorStadistics/index"));
const LogOperationsReport = React.lazy(() => import("./components/Reports/LogOperationsReport/index"));
const ClientOperationCategoryReport = React.lazy(() => import("./components/Reports/ClientOperationCategoryReport/index"));
const StadisticsByYear = React.lazy(() => import("./components/stadistics/stadisticsByYear/index"));

const routes = [
  { path: "/", exact: true, name: "Home", component: DefaultLayout },
  { path: "/clientes", name: "Clientes", component: MainClientsForm },
  {
    path: "/calendar-events",
    name: "Eventos Calendario",
    component: CalendarEvents,
  },
  { path: "/logOperations", name: "Log de Gestiones", component:LogOperationsReport},
  { path: "/clientCategories", name: "Categoria Clientes", component:ClientOperationCategoryReport},
  { path: "/stadisticsByYear", name: "Estadisticas por anio", component:StadisticsByYear},
  { path: "/info", name: "GeneralInfo", component: generalInfo },
  { path: "/prospects", name: "Clientes Varios", component: prospects },
  { path: "/projects", name: "Proyect", component: projects },
  { path: "/houseDesigns", name: "Models", component: houseDesigns },
  { path: "/dashboard", name: "Dashboard", component: DashboardData },
  {
    path: "/client-negotiations",
    name: "Negociaciones Report General",
    component: clientNegotiationsReport,
  },
  {
    path: "/negotiations-closed",
    name: "Negociaciones Cerradas",
    component: negotiationsClosedReport,
  },

  { path: "/clientsReport", name: "Clientes", component: clientsReport },
  {
    path: "/clientsUserReport",
    name: "Clientes por Usuario",
    component: clientsByUserReport,
  },
  {
    path: "/salesAdvisorStadistics",
    name: "Estadisticas por agente",
    component: SalesAdvisorStadistics,
  },
  {
    path: "/prospects-report",
    name: "Reporte de Clientes Varios",
    component: ProspectReport,
  },
  {
    path: "/prospects-contacttype-report",
    name: "Clientes Varios por Contacto",
    component: prospectByContactTypeReport,
  },
  {
    path: "/client-in-negotiations",
    name: "Clientes en negocios",
    component: NegotiationClientReport,
  },
  {
    path: "/client-transaction-report",
    name: "Modificacioens a Clientes",
    component: ClientTransactions,
  },
  { path: "/clientLevels", name: "Niveles clientes", component: clientLevel },
  { path: "/negotiation", name: "Negocios", component: negotiation },
  // {
  //   path: "/clientsTracking",
  //   name: "Seguimiento de clientes",
  //   component: clientsTracking,
  // },
  { path: "/user", name: "User", component: user },
  { path: "/passwordReset", name: "PasswordReset", component: PasswordReset },
  { path: "/passwordMasterReset", name: "PasswordMasterReset", component: PasswordMasterReset },
  { path: "/userrol", name: "Rol User", component: userRol },
  {
    path: "/suplierService",
    name: "Supplier Service",
    component: suplierService,
  },
  { path: "/financials", name: "Financial Institution", component: financials },
  {
    path: "/meeting-categories",
    name: "Meeting Categories",
    component: meetingCategories,
  },
  {
    path: "/client-alarms",
    name: "Alarms Report",
    component: alarmReport,
  },
  {
    path: "/client-projects",
    name: "Client project report",
    component: clientProjectReport,
  },
  { path: "/nationalities", name: "Nationalities", component: nationalities },
  { path: "/contacts", name: "ContactTypes", component: contactTypes },
  { path: "/builders", name: "BuildersComp", component: builders },
  // { path: "/reserva", name: "Reservas", component: reservas },
  { path: "/theme", exact: true, name: "Theme", component: Colors },
  { path: "/theme/colors", name: "Colors", component: Colors },
  { path: "/theme/typography", name: "Typography", component: Typography },
  { path: "/base", exact: true, name: "Base", component: Cards },
  { path: "/base/cards", name: "Cards", component: Cards },
  { path: "/base/forms", name: "Forms", component: Forms },
  { path: "/base/switches", name: "Switches", component: Switches },
  { path: "/base/tables", name: "Tables", component: Tables },
  { path: "/base/tabs", name: "Tabs", component: Tabs },
  { path: "/base/breadcrumbs", name: "Breadcrumbs", component: Breadcrumbs },
  { path: "/base/carousels", name: "Carousel", component: Carousels },
  { path: "/base/collapses", name: "Collapse", component: Collapses },
  { path: "/base/dropdowns", name: "Dropdowns", component: Dropdowns },
  { path: "/base/jumbotrons", name: "Jumbotrons", component: Jumbotrons },
  { path: "/base/list-groups", name: "List Groups", component: ListGroups },
  { path: "/base/navbars", name: "Navbars", component: Navbars },
  { path: "/base/navs", name: "Navs", component: Navs },
  { path: "/base/paginations", name: "Paginations", component: Paginations },
  { path: "/base/popovers", name: "Popovers", component: Popovers },
  { path: "/base/progress-bar", name: "Progress Bar", component: ProgressBar },
  { path: "/base/tooltips", name: "Tooltips", component: Tooltips },
  { path: "/buttons", exact: true, name: "Buttons", component: Buttons },
  { path: "/buttons/buttons", name: "Buttons", component: Buttons },
  {
    path: "/buttons/button-dropdowns",
    name: "Button Dropdowns",
    component: ButtonDropdowns,
  },
  {
    path: "/buttons/button-groups",
    name: "Button Groups",
    component: ButtonGroups,
  },
  {
    path: "/buttons/brand-buttons",
    name: "Brand Buttons",
    component: BrandButtons,
  },
  { path: "/icons", exact: true, name: "Icons", component: CoreUIIcons },
  { path: "/icons/coreui-icons", name: "CoreUI Icons", component: CoreUIIcons },
  { path: "/icons/flags", name: "Flags", component: Flags },
  { path: "/icons/font-awesome", name: "Font Awesome", component: FontAwesome },
  {
    path: "/icons/simple-line-icons",
    name: "Simple Line Icons",
    component: SimpleLineIcons,
  },
  {
    path: "/notifications",
    exact: true,
    name: "Notifications",
    component: Alerts,
  },
  { path: "/notifications/alerts", name: "Alerts", component: Alerts },
  { path: "/notifications/badges", name: "Badges", component: Badges },
  { path: "/notifications/modals", name: "Modals", component: Modals },
  { path: "/widgets", name: "Widgets", component: Widgets },
  { path: "/charts", name: "Charts", component: Charts },
];

export default routes;
