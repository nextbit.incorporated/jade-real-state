export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Datos generales",
      url: "/info",
      icon: "icon-briefcase",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Estadisticas",
      url: "/stadisticsByYear",
      icon: "icon-layers",
      badge: {
        variant: "info",
      },
    },
    {
      title: true,
      name: "Transacciones",
      wrapper: {
        // optional wrapper object
        element: "", // required valid HTML5 element tag
        attributes: {}, // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: "", // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: "Clientes",
      url: "/clientes",
      icon: "icon-user",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Eventos Calendario",
      url: "/calendar-events",
      icon: "icon-user",
      badge: {
        variant: "info",
      },
    },

    {
      name: "Clientes Varios",
      url: "/prospects",
      icon: "icon-user-follow",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Categorias Cliente",
      url: "/clientLevels",
      icon: "icon-layers",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Negocios",
      url: "/negotiation",
      icon: "icon-briefcase",
      badge: {
        variant: "info",
      },
    },
    // {
    //   name: "Seguimiento",
    //   url: "/clientsTracking",
    //   icon: "icon-user-following",
    //   badge: {
    //     variant: "info",
    //   },
    // },
    // {
    //   name: "Reservas",
    //   url: "/reserva",
    //   icon: "icon-location-pin",
    //   badge: {
    //     variant: "info",
    //   },
    // },

    {
      name: "Proyectos",
      url: "/projects",
      icon: "icon-layers",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Modelos",
      url: "/houseDesigns",
      icon: "icon-home",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Tipos Servicios",
      url: "/suplierService",
      icon: "icon-settings",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Instituciones Financieras",
      url: "/financials",
      icon: "icon-wallet",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Nacionalidades",
      url: "/nationalities",
      icon: "icon-globe",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Tipo de Contactos",
      url: "/contacts",
      icon: "icon-phone",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Tipo de citas",
      url: "/meeting-categories",
      icon: "icon-pin",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Proveedores",
      url: "/builders",
      icon: "icon-briefcase",
      badge: {
        variant: "info",
      },
    },
    {
      name: "Reportes",
      url: "/reports",
      icon: "icon-note",
      badge: {
        variant: "info",
      },
       
      children: [
        {
          name: "Log de gestiones",
          url: "/logOperations",
          icon: "icon-list",
        },
        {
          name: "Categoria clientes",
          url: "/clientCategories",
          icon: "icon-layers",
        },
        {
          name: "Estadistica Agentes",
          url: "/salesAdvisorStadistics",
          icon: "icon-layers",
        },
        {
          name: "Reporte Cliente",
          url: "/clientsReport",
          icon: "icon-user",
        },
        {
          name: "Clientes Varios",
          url: "/prospects-report",
          icon: "icon-user-follow",
        },
        {
          name: "Clientes Varios por Contacto",
          url: "/prospects-contacttype-report",
          icon: "icon-user-follow",
        },
        {
          name: "Clientes en Negocios",
          url: "/client-in-negotiations",
          icon: "icon-briefcase",
        },
        {
          name: "Clientes por agente",
          url: "/clientsUserReport",
          icon: "icon-user",
        },
        {
          name: "Clientes en Negociaciones",
          url: "/client-negotiations",
          icon: "icon-briefcase",
        },
        {
          name: "Negocios Cerrados",
          url: "/negotiations-closed",
          icon: "icon-briefcase",
        },
        {
          name: "Modificaciones de Clientes",
          url: "/client-transaction-report",
          icon: "icon-user",
        },
        {
          name: "Cliente por proyecto",
          url: "/client-projects",
          icon: "cui-user-follow",
        },
        {
          name: "Reporte Alarmas",
          url: "/client-alarms",
          icon: "cui-calendar",
        },
      ],
    },
    {
      name: "Seguridad",
      url: "/security",
      icon: "icon-shield",
      badge: {
        variant: "info",
      },
      children: [
        {
          name: "Usuarios",
          url: "/user",
          icon: "icon-user",
        },
        {
          name: "PasswordMasterReset",
          url: "/passwordMasterReset",
          icon: "icon-key",
        },
        {
          name: "PasswordReset",
          url: "/passwordReset",
          icon: "icon-key",
        },
      ],
    },

    // {
    //   title: true,
    //   name: "Theme",
    //   wrapper: {
    //     // optional wrapper object
    //     element: "", // required valid HTML5 element tag
    //     attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
    //   },
    //   class: "" // optional class names space delimited list for title item ex: "text-center"
    // },
    // {
    //   name: "Colors",
    //   url: "/theme/colors",
    //   icon: "icon-drop"
    // },
    // {
    //   name: "Typography",
    //   url: "/theme/typography",
    //   icon: "icon-pencil"
    // },
    // {
    //   title: true,
    //   name: "Components",
    //   wrapper: {
    //     element: "",
    //     attributes: {}
    //   }
    // }
  // {
  //      name: "Base",
  //      url: "/base",
  //      icon: "icon-puzzle",
  //      children: [
  //        {
  //          name: "Client",
  //          url: "/client",
  //          icon: "icon-star"
  //        },
  //        {
  //          name: "Breadcrumbs",
  //          url: "/base/breadcrumbs",
  //          icon: "icon-puzzle"
  //        },
  //    {
  //      name: "Cards",
  //      url: "/base/cards",
  //      icon: "icon-puzzle"
  //    },
  //        {
  //          name: "Carousels",
  //          url: "/base/carousels",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Collapses",
  //          url: "/base/collapses",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Dropdowns",
  //          url: "/base/dropdowns",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Forms",
  //          url: "/base/forms",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Jumbotrons",
  //          url: "/base/jumbotrons",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "List groups",
  //          url: "/base/list-groups",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Navs",
  //          url: "/base/navs",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Paginations",
  //          url: "/base/paginations",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Popovers",
  //          url: "/base/popovers",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Progress Bar",
  //          url: "/base/progress-bar",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Switches",
  //          url: "/base/switches",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Tables",
  //          url: "/base/tables",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Tabs",
  //          url: "/base/tabs",
  //          icon: "icon-puzzle"
  //        },
  //        {
  //          name: "Tooltips",
  //          url: "/base/tooltips",
  //          icon: "icon-puzzle"
  //        }
  //      ]
  //     },

    
    // {
    //   name: "Buttons",
    //   url: "/buttons",
    //   icon: "icon-cursor",
    //   children: [
    //     {
    //       name: "Buttons",
    //       url: "/buttons/buttons",
    //       icon: "icon-cursor"
    //     },
    //     {
    //       name: "Button dropdowns",
    //       url: "/buttons/button-dropdowns",
    //       icon: "icon-cursor"
    //     },
    //     {
    //       name: "Button groups",
    //       url: "/buttons/button-groups",
    //       icon: "icon-cursor"
    //     },
    //     {
    //       name: "Brand Buttons",
    //       url: "/buttons/brand-buttons",
    //       icon: "icon-cursor"
    //     }
    //   ]
    // },
    // {
    //   name: "Charts",
    //   url: "/charts",
    //   icon: "icon-pie-chart"
    // },
    // {
    //   name: "Icons",
    //   url: "/icons",
    //   icon: "icon-star",
    //   children: [
    //     {
    //       name: "CoreUI Icons",
    //       url: "/icons/coreui-icons",
    //       icon: "icon-star",
    //       badge: {
    //         variant: "info",
    //         text: "NEW"
    //       }
    //     },
    //     {
    //       name: "Flags",
    //       url: "/icons/flags",
    //       icon: "icon-star"
    //     },
    //     {
    //       name: "Font Awesome",
    //       url: "/icons/font-awesome",
    //       icon: "icon-star",
    //       badge: {
    //         variant: "secondary",
    //         text: "4.7"
    //       }
    //     },
    //     {
    //       name: "Simple Line Icons",
    //       url: "/icons/simple-line-icons",
    //       icon: "icon-star"
    //     }
    //   ]
    // },
    // {
    //   name: "Notifications",
    //   url: "/notifications",
    //   icon: "icon-bell",
    //   children: [
    //     {
    //       name: "Alerts",
    //       url: "/notifications/alerts",
    //       icon: "icon-bell"
    //     },
    //     {
    //       name: "Badges",
    //       url: "/notifications/badges",
    //       icon: "icon-bell"
    //     },
    //     {
    //       name: "Modals",
    //       url: "/notifications/modals",
    //       icon: "icon-bell"
    //     }
    //   ]
    // },
    // {
    //   name: "Widgets",
    //   url: "/widgets",
    //   icon: "icon-calculator",
    //   badge: {
    //     variant: "info",
    //     text: "NEW"
    //   }
    // },
    // {
    //   divider: true
    // },
    // {
    //   title: true,
    //   name: "Extras"
    // },
    // {
    //   name: "Pages",
    //   url: "/pages",
    //   icon: "icon-star",
    //   children: [
    //     {
    //       name: "Login",
    //       url: "/login",
    //       icon: "icon-star"
    //     },
    //     // {
    //     //   name: "Register",
    //     //   url: "/register",
    //     //   icon: "icon-star"
    //     // },
    //     {
    //       name: "Error 404",
    //       url: "/404",
    //       icon: "icon-star"
    //     },
    //     {
    //       name: "Error 500",
    //       url: "/500",
    //       icon: "icon-star"
    //     }
    //   ]
    // }
    // {
    //   name: "Disabled",
    //   url: "/dashboard",
    //   icon: "icon-ban",
    //   attributes: { disabled: true }
    // }
  ],
};
