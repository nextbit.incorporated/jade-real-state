import React, { useState, useEffect } from "react";

const SearchInput = props => {
  const { onChangeSearch } = props;

  return (
    <input
      type="text"
      id="input1-group2"
      name="input1-group2"
      placeholder="Buscar"
      onChange={onChangeSearch}
    />
  );
};

export default SearchInput;
