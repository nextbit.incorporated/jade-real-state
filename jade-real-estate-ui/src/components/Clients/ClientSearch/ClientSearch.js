import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";
import {
  Button,
  Col,
  FormGroup,
  Input,
  Row,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  InputGroupAddon,
  InputGroup,
  Table,
} from "reactstrap";
import { toast } from "react-toastify";

import API from "../../API/API";

const thStyle = {
  background: "#20a8d8",
  color: "white",
};

const ClientSearch = ({ title, isOpen, toggle, handleOnSelectedClient }) => {
  const [clients, setClients] = useState([]);
  const [searchValue, setSearchValue] = useState("");

  useEffect(() => {
    fetchClientsPaged(searchValue, 0);
  }, [searchValue]);

  const handleSearchValueOnChange = (e) => {
    setSearchValue(e.target.value);
  };

  function handleSearchValueKeyPress(e) {
    if (e.key === "Enter") {
      fetchClientsPaged(searchValue, 0);
    }
  }

  const getNextClientsPage = () => {
    fetchClientsPaged(searchValue, clients.pageIndex + 1);
  };

  function getPreviousClientsPage() {
    fetchClientsPaged(searchValue, clients.pageIndex - 1);
  }

  function fetchClientsPaged(query, step) {
    const user = sessionStorage.getItem("userId");

    const index = step === undefined || step === null ? 0 : step;
    if (query === null || query === "") {
      var url = `clients/paged?pageSize=10&pageIndex=${index}&user=${user}`;

      API.get(url)
        .then((res) => {
          setClients(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast.warn(
              "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
            );
          } else {
            if (error.response.status !== undefined) {
              if (error.response.status === 401) {
                return <Redirect to="/login" />;
              }
            } else {
              toast.warn(
                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
              );
            }
          }
        });
    } else {
      var url1 = `clients/paged?pageSize=10&pageIndex=${index}&user=${user}&value=${query}`;
      API.get(url1).then((res) => {
        setClients(res.data);
      });
    }
  }

  const SelectClient = (client) => {
    const updatedClient = {
      ...client.principal,
      coDebtor: client.coDebtor,
    };

    handleOnSelectedClient(updatedClient);
  };

  return (
    <React.Fragment>
      {isOpen ? (
        <Row>
          <Col md="3" sm="6" xs="12">
            <Row>
              <Modal isOpen={isOpen} toggle={toggle} className={"modal-lg"}>
                <ModalHeader toggle={toggle}>{title}</ModalHeader>
                <ModalBody>
                  <FormGroup row>
                    <Col md="6" sm="6" xs="12">
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <Button type="button" color="primary">
                            <i className="fa fa-search" /> Valor
                          </Button>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          id="input1-group2"
                          name="input1-group2"
                          placeholder="Buscar"
                          value={searchValue}
                          onChange={handleSearchValueOnChange}
                          onKeyPress={(e) => handleSearchValueKeyPress(e)}
                        />
                      </InputGroup>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="12">
                      {clients === null ? (
                        <table>
                          <tbody>
                            <tr>
                              <td colSpan={16}>Buscando...</td>
                            </tr>
                          </tbody>
                        </table>
                      ) : clients.length === 0 ? (
                        <table>
                          <tbody>
                            <tr>
                              <td colSpan={16}>No se encontraron proyectos.</td>
                            </tr>
                          </tbody>
                        </table>
                      ) : (
                        <Table hover bordered striped responsive size="sm">
                          <thead>
                            <tr>
                              <th style={thStyle}>Seleccionar</th>
                              <th style={thStyle}>Identitdad</th>
                              <th style={thStyle}>Codigo</th>
                              <th style={thStyle}>Primer Nombre</th>
                              <th style={thStyle}>Segundo Nombre</th>
                              <th style={thStyle}>Primer Apellido</th>
                              <th style={thStyle}>Segundo Apellido</th>
                              <th style={thStyle}>Agente</th>
                            </tr>
                          </thead>
                          <tbody>
                            {clients.clients.map((client) => (
                              <tr key={client.principal.id}>
                                <td>
                                  <Button
                                    color="link"
                                    onClick={() => {
                                      SelectClient(client);

                                      toggle();
                                    }}
                                  >
                                    Seleccionar
                                  </Button>
                                </td>
                                <td>{client.principal.identificationCard}</td>
                                <td>{client.principal.id}</td>
                                <td>{client.principal.firstName}</td>
                                <td>{client.principal.middleName}</td>
                                <td>{client.principal.firstSurname}</td>
                                <td>{client.principal.secondSurname}</td>
                                <td>{client.principal.salesAdvisor}</td>
                              </tr>
                            ))}
                          </tbody>
                        </Table>
                      )}
                    </Col>
                    <Col md="12">
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <Button
                            type="button"
                            color="primary"
                            onClick={getPreviousClientsPage}
                          >
                            <i className="icon-arrow-left-circle" /> Anterior
                          </Button>
                        </InputGroupAddon>
                        <div
                          className="animated fadeIn"
                          style={{
                            marginTop: "7px",
                            marginLeft: "8px",
                            marginRight: "8px",
                          }}
                        >
                          <h5>
                            <strong>Paginas: {clients.pageCount}</strong>
                          </h5>
                        </div>
                        <div
                          className="animated fadeIn"
                          style={{
                            marginTop: "7px",
                            marginLeft: "8px",
                            marginRight: "8px",
                          }}
                        >
                          <h5>
                            <strong>Actual: {clients.pageIndex}</strong>
                          </h5>
                        </div>
                        <InputGroupAddon addonType="prepend">
                          <Button
                            type="button"
                            color="primary"
                            onClick={getNextClientsPage}
                          >
                            Proximo <i className="icon-arrow-right-circle" />
                          </Button>
                        </InputGroupAddon>
                      </InputGroup>
                    </Col>
                  </FormGroup>
                </ModalBody>
                <ModalFooter>
                  <Button color="secondary" onClick={toggle}>
                    Cancel
                  </Button>
                </ModalFooter>
              </Modal>
            </Row>
          </Col>
        </Row>
      ) : (
        <Row></Row>
      )}
    </React.Fragment>
  );
};

ClientSearch.defaultProps = {
  title: "Seleccion de Cliente",
  isOpen: false,
};

ClientSearch.propTypes = {
  title: PropTypes.string.isRequired,
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  handleOnSelectedClient: PropTypes.func.isRequired,
};

export default ClientSearch;
