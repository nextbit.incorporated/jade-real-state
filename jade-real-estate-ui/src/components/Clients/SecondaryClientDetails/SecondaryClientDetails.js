import React from "react";
import {
  Col,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button
} from "reactstrap";

const SecondaryClientDetails = props => {
  const { prevStep, nextStep } = props;

  return (
    <div className="animated fadeIn">
      <h1>Codeudor Details</h1>
      <FormGroup row>
        <Col md="12">
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button type="button" color="primary" onClick={prevStep}>
                <i className="fa fa-search" /> Previous
              </Button>
            </InputGroupAddon>
            <InputGroupAddon addonType="prepend">
              <Button type="button" color="primary" onClick={nextStep}>
                <i className="fa fa-search" /> Next
              </Button>
            </InputGroupAddon>
          </InputGroup>
        </Col>
      </FormGroup>
    </div>
  );
};

export default SecondaryClientDetails;
