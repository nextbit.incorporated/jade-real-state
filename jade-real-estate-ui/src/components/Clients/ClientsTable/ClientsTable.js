import React, { Suspense, useState } from "react";
import styled from "styled-components";
import {
  Card,
  CardHeader,
  CardBody,
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
  Modal,
  ModalHeader,
  ModalFooter,
  ModalBody,
  Row,
  Label,
} from "reactstrap";
import ReactLoading from "react-loading";
import ClientsTableRows from "../ClientsTableRows/ClientsTableRows";
import moment from "moment";
import API from "./../../API/API";
import { ToastContainer, toast } from "react-toastify";

import { addCalendarEvent } from "./../../Helpers/GoogleCalendar";

const Root = styled.div`
  height: 58vh;
  max-height: 58vh;
  overflow-y: auto;
  overflow-x: auto;
`;

const initialSearchState = {
  value: "",
  typingTimeout: 0,
};

const initialToggleState = {
  step: 1,
  editMode: "None",
  saveStatus: false,
  state: {
    modal: false,
    modalDownpayment: false,
    large: false,
    large1: false,
    small: false,
    primary: false,
    success: false,
    warning: false,
    danger: false,
    info: false,
    uploadFile: false,
    className: null,
  },
};

const initialAlarm = {
  id: 0,
  date: moment().format("YYYY-MM-DD"),
  information: "",
  monthsNumber: 0,
  clientId: 0,
};

const initialAlarmsState = {
  id: 0,
  firstName: "",
  firstSurname: "",
  alarms: [],
};

const ClientsTable = ({
  editRow,
  deleteClient,
  clientsPaged,
  nextPage,
  prevPage,
  fetchClientsPaged,
  printRow,
  viewClientTracking,
  loadingStatus,
  downloadXLSX,
  showNewDialog,
  isActive,
  toggleShowActive,
  excludeNegotiationEnded,
  toggleExcludeNegotiationEnded,
  operations,
}) => {
  const [searchState, setSearchState] = useState(initialSearchState);
  const [modalstate, setModalstate] = useState(initialToggleState);
  const [Alarmstate, setAlarmstate] = useState(initialAlarm);
  const [ClientAlarmState, setClientAlarmState] = useState(initialAlarmsState);

  const handleShowActiveChange = (event) => {
    toggleShowActive();
  };

  const handletoggleExcludeNegotiationEndedChange = (event) => {
    toggleExcludeNegotiationEnded();
  };

  const toggleAlarm = (client) => {
    const updatedAlarmsState = client?.principal
      ? {
          id: client.principal.id,
          firstName: client.principal.firstName,
          firstSurname: client.principal.firstSurname,
          alarms: client.principal.alarms,
        }
      : initialAlarmsState;

    setClientAlarmState(updatedAlarmsState);
    setModalstate({
      ...modalstate,
      state: {
        ...modalstate.state,
        modal: !modalstate.state.modal,
      },
    });
  };

  const CanceltoggleAlarm = () => {
    setModalstate({
      ...modalstate,
      state: {
        modal: false,
      },
    });
  };

  const handleInputChangeAlarm = (event) => {
    const { name, value } = event.target;
    setAlarmstate({
      ...Alarmstate,
      [name]: value,
    });
  };

  const handleNumericInputChangeAlarm = (event) => {
    const { name, value } = event.target;
    setAlarmstate({
      ...Alarmstate,
      [name]: parseInt(value),
    });
  };

  const thStyle = {
    background: "#20a8d8",
    color: "white",
    textAlign: "center",
  };

  function handleValueChange(e) {
    if (searchState.typingTimeout) {
      clearTimeout(searchState.typingTimeout);
    }

    const query = e.target.value;

    setSearchState({
      value: query,
    });
  }

  function handleValueHeyPress(e) {
    if (e.key === "Enter") {
      const query = e.target.value;
      fetchClientsPaged(query, 0);
    }
  }

  function nextStepPaged() {
    nextPage(clientsPaged.pageIndex + 1, searchState.value);
  }

  function prevStepPaged() {
    prevPage(clientsPaged.pageIndex - 1, searchState.value);
  }

  const refreshAlarm = (alarmData) => {
    const updatedAlarmsState = {
      ...ClientAlarmState,
      alarms: alarmData && alarmData.length > 0 ? alarmData : [],
    };

    setClientAlarmState(updatedAlarmsState);
  };

  function addNewReserveDetail() {
    const url = `Alarm`;
    const reserveDetailRequest = {
      id: 0,
      clientId: ClientAlarmState.id,
      date: Alarmstate.date,
      information: Alarmstate.information,
      monthsNumber: Alarmstate.monthsNumber,
    };

    API.post(url, reserveDetailRequest)
      .then((res) => {
        toast.success("Recordatorio agregado satisfactorimente");

        const startDate = new Date(
          moment(reserveDetailRequest.date + " 10:00:00").format()
        );

        const address = `Alerta: ${ClientAlarmState.id} - ${ClientAlarmState.firstName} ${ClientAlarmState.firstSurname}`;
        const summary = `Alerta: ${ClientAlarmState.id} - ${ClientAlarmState.firstName} ${ClientAlarmState.firstSurname} ${reserveDetailRequest.information}`;

        addCalendarEvent(startDate, address, summary);

        refreshAlarm(res.data);
        setAlarmstate(initialAlarm);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para registrar el pago favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  }

  const fallback = (
    <tr>
      <td colSpan={14}>Cargando....</td>
    </tr>
  );

  const eliminarAlarma = (id) => {
    const url = `Alarm?id=${id}`;

    API.delete(url)
      .then((res) => {
        toast.success("reserva agregado satisfactorimente");
        refreshAlarm(res.data);
        setAlarmstate(initialAlarm);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para registrar el pago favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  };

  function ObtenerDetalleAlarms() {
    if (ClientAlarmState.alarms) {
      return (
        <tbody>
          {ClientAlarmState.alarms.map((detalle) => (
            <tr key={detalle.id}>
              <td>
                <Button
                  block
                  color="warning"
                  onClick={() => eliminarAlarma(detalle.id)}
                >
                  Eliminar
                </Button>
              </td>
              <td align="center">{detalle.id}</td>
              <td style={{ whiteSpace: "nowrap" }}>
                {moment(detalle.date).format("YYYY-MM-DD")}
              </td>
              <td align="center">{detalle.monthsNumber}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.information}</td>
            </tr>
          ))}
        </tbody>
      );
    }
    return <tbody></tbody>;
  }

  function getAlarmAction(p) {
    return (
      <Col>
        <Modal
          isOpen={modalstate.state.modal}
          toggle={toggleAlarm}
          className={"modal-lg"}
        >
          <ModalHeader>
            Alarmas de cliente: {ClientAlarmState.id} -{" "}
            {ClientAlarmState.firstName} {ClientAlarmState.firstSurname}
          </ModalHeader>
          <ModalBody>
            {/* <FormGroup row>

                            </FormGroup> */}
            <Row>
              <Col md="3" sm="6" xs="12">
                <FormGroup>
                  <Label for="exampleText">Fecha</Label>
                  <Input
                    type="date"
                    name="date"
                    id="date"
                    placeholder="Fecha"
                    value={Alarmstate.date}
                    onChange={handleInputChangeAlarm}
                  />
                </FormGroup>
              </Col>
              <Col md="3" sm="6" xs="12">
                <FormGroup>
                  <Label for="exampleText">Altertar dentro de(Meses):</Label>
                  <Input
                    type="number"
                    name="monthsNumber"
                    id="monthsNumber"
                    placeholder="Coloce cantidad de meses..."
                    value={Alarmstate.monthsNumber}
                    onChange={handleNumericInputChangeAlarm}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xs="12" md="9">
                <FormGroup>
                  <Label htmlFor="textarea-input">Comentarios: </Label>
                  <Input
                    type="textarea"
                    name="information"
                    id="information"
                    rows="5"
                    value={Alarmstate.information}
                    onChange={handleInputChangeAlarm}
                  />
                </FormGroup>
              </Col>
              <Col md="3" sm="6" xs="12">
                <FormGroup>
                  <Label for="exampleText"> </Label>
                  <Button
                    block
                    color="primary"
                    type="submit"
                    size="md"
                    onClick={() => addNewReserveDetail()}
                  >
                    Agregar
                  </Button>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <CardHeader>
                <strong>Detalles de Alarma</strong>
              </CardHeader>
            </Row>
            <Row >
              <Table hover bordered striped responsive size="sm" >
                <thead>
                  <tr>
                    <th style={thStyle}>Eliminar</th>
                    {/* <th style={thStyle}>Fila</th> */}
                    <th style={thStyle}>Codigo Alarma</th>
                    <th style={thStyle}>Fecha</th>
                    <th style={thStyle}>Notificar dentro de</th>
                    <th style={thStyle}>Información</th>
                  </tr>
                </thead>
                {ObtenerDetalleAlarms()}
              </Table>
            </Row>
          </ModalBody>
          <ModalFooter>
            {/* <Button color="primary" onClick={CanceltoggleReserve}>
                                Guardar
                            </Button> */}
            <Button color="secondary" onClick={CanceltoggleAlarm}>
              Cerrar
            </Button>
          </ModalFooter>
        </Modal>
      </Col>
    );
  }

  function obtenerTabla(e) {
    if (loadingStatus) {
      return (
        <FormGroup row>
          <Col md="4" sm="6" xs="6"></Col>
          <Col md="4" sm="6" xs="6">
            <ReactLoading
              type="bars"
              color="#5DBCD2"
              height={"30%"}
              width={"30%"}
            />
          </Col>
          <Col md="4" sm="6" xs="6"></Col>
        </FormGroup>
      );
    } else {
      return (
        <FormGroup row >
          <Col md="12">
            <Root>
              <Table hover bordered striped responsive size="sm" style={{ height: '50vh'}}>
                <thead>
                  <tr>
                    <th style={thStyle}>Editar</th>
                    <th style={thStyle}>Gestionar</th>
                    <th style={thStyle}>Ficha</th>
                    <th style={thStyle}>Seguimiento</th>
                    <th style={thStyle}>Recordatorios</th>
                    <th style={thStyle}>Tiene Alarma</th>
                    <th style={thStyle}>Id</th>
                    <th style={thStyle}>Codigo</th>
                    <th style={thStyle}>Fecha de Creacion</th>
                    <th style={thStyle}>Fecha de Ingreso</th>
                    <th style={thStyle}>Primer Nombre</th>
                    <th style={thStyle}>Segundo Nombre</th>
                    <th style={thStyle}>Primer Apellido</th>
                    <th style={thStyle}>Segundo Apellido</th>
                    <th style={thStyle}>Cedula</th>
                    <th style={thStyle}>Nacionalidad</th>
                    <th style={thStyle}>Celular</th>
                    <th style={thStyle}>Telefono</th>
                    <th style={thStyle}>Email</th>

                    <th style={thStyle}>Perfil</th>

                    <th style={thStyle}>Lugar de trabajo</th>
                    <th style={thStyle}>Inicio de labores</th>
                    <th style={thStyle}>Modeda Deudor</th>
                    <th style={thStyle}>Ingreso Bruto Mensual</th>
                    <th style={thStyle}>Categoria</th>
                    <th style={thStyle}>Proyecto</th>
                    <th style={thStyle}>Tipo de contacto</th>
                    <th style={thStyle}>Primer Nombre Codeudor</th>
                    <th style={thStyle}>Segundo Nombre Codeudor</th>
                    <th style={thStyle}>Primer Apellido Codeudor</th>
                    <th style={thStyle}>Segundo Apellido Codeudor</th>
                    <th style={thStyle}>Nacionalidad Codeudor</th>
                    <th style={thStyle}>Cedula Codeudor</th>
                    <th style={thStyle}>Celular Codeudor</th>
                    <th style={thStyle}>Telefono Codeudor</th>
                    <th style={thStyle}>Email Codeudor</th>
                    <th style={thStyle}>Lugar de trabajo Codeudor</th>
                    <th style={thStyle}>Moneda Codeudor</th>
                    <th style={thStyle}>Ingreso Bruto Codeudor</th>
                    <th style={thStyle}>Lugar de residencia</th>
                    <th style={thStyle}>Posee casa</th>
                    <th style={thStyle}>Aporta al RAP</th>
                    <th style={thStyle}>Comentarios</th>
                    <th style={thStyle}>Institucion Financiera</th>
                    <th style={thStyle}>Oficial de Credito</th>
                    <th style={thStyle}>Asesor de Ventas</th>
                    <th style={thStyle}>Negociacion Finalizada</th>
                  </tr>
                </thead>
                <tbody>
                  <Suspense fallback={fallback}>
                    <ClientsTableRows
                      clients={clientsPaged === [] ? [] : clientsPaged.clients}
                      editRow={editRow}
                      deleteClient={deleteClient}
                      printRow={printRow}
                      viewClientTracking={viewClientTracking}
                      toggleAlarm={toggleAlarm}
                      operations={operations}
                    />
                  </Suspense>
                </tbody>
              </Table>
            </Root>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStepPaged}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {clientsPaged.pageCount}</strong>
                </h5>
              </div>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Actual: {clientsPaged.pageIndex}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStepPaged}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      );
    }
  }

  return (
    <div>
      <Card>
        <CardHeader>
          <i className="fa fa-align-justify" /> Clientes
        </CardHeader>
        <CardBody>
          <FormGroup row>
            <Col md="3">
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <Button type="button" color="primary">
                    <i className="fa fa-search" /> Valor
                  </Button>
                </InputGroupAddon>
                <Input
                  type="text"
                  id="input1-group2"
                  name="input1-group2"
                  placeholder="Buscar"
                  value={searchState.value}
                  onChange={handleValueChange}
                  onKeyPress={(e) => handleValueHeyPress(e)}
                />
              </InputGroup>
            </Col>

            <Col md="2">
              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    name="showActive"
                    checked={isActive}
                    onChange={handleShowActiveChange}
                    required
                  />{" "}
                  Mostrar solo Activos
                </Label>
                <Label />
              </FormGroup>
            </Col>

            <Col md="2">
              <FormGroup check>
                <Label check>
                  <Input
                    type="checkbox"
                    name="excludeNegotiationEnded"
                    checked={excludeNegotiationEnded}
                    onChange={handletoggleExcludeNegotiationEndedChange}
                    required
                  />{" "}
                  Excluir Negociaciones Finalizadas
                </Label>
                <Label />
              </FormGroup>
            </Col>

            <Col />

            <Col xs="auto">
              <Button
                type="button"
                color="success"
                onClick={() => downloadXLSX()}
              >
                <i className="fa fa-new" />
                Formato de Importacion
              </Button>
            </Col>

            <Col xs="auto">
              <Button
                type="button"
                color="success"
                onClick={() => showNewDialog()}
              >
                <i className="fa fa-new" />
                Nuevo Cliente
              </Button>
            </Col>
          </FormGroup>
          {obtenerTabla()}
        </CardBody>
      </Card>
      {getAlarmAction()}
      <ToastContainer />
    </div>
  );
};

export default ClientsTable;
