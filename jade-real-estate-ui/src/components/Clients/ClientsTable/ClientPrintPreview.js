import React from "react";
import logo from "../../../assets/img/brand/logo.svg";
import moment from "moment";
import { CardBody, CardHeader, Card, Row, Col } from "reactstrap";

const getClientFullName = (client) => {
  const name = client
    ? (client.firstName || "") +
      " " +
      (client.middleName || "") +
      " " +
      (client.firstSurname || "") +
      " " +
      (client.secondSurname || "")
    : "";

  return name;
};

const getTrackingInfo = (clientTracking) => {
  return clientTracking.map((tracking) => (
    <Card>
      <CardHeader>
        Fecha Programada de la Cita {moment(tracking.fechaCita).format("L")}
      </CardHeader>
      <CardBody>
        <Row>
          <Col>
            <strong>Descripcion: </strong>
            {tracking.description}
          </Col>
        </Row>
        <Row>
          <Col>
            <strong>Comentarios: </strong>
            {tracking.comentarios}
          </Col>
        </Row>
      </CardBody>
    </Card>
  ));
};

class ClientPrintPreview extends React.Component {
  render() {
    const principal = this.props.principal;
    const coDebtor = this.props.coDebtor;
    const clientTracking = this.props.clientTracking;

    return (
      <div>
        <CardBody>
          <div style={{ display: "block" }}>
            <div align="right">
              <img
                src={logo}
                alt=""
                style={{ width: "80px", height: "80px" }}
              ></img>
            </div>
            <h1 style={{ textAlign: "center" }}>
              <u>
                <b>Inmobiliaria Jade</b>
              </u>
            </h1>
          </div>
          <div style={{ display: "block" }}>
            <h2 style={{ textAlign: "center" }}>
              Ficha Cliente: {principal.id}
            </h2>
          </div>
        </CardBody>

        <CardBody>
          <div style={{ display: "block" }}>
            <h3 style={{ textAlign: "left" }}>
              <b>Nombre: {getClientFullName(principal)} </b>
            </h3>
            <h3 style={{ textAlign: "left" }}>
              <b>Identidad: {principal.identificationCard || "N/D"} </b>
            </h3>
            <h4 style={{ textAlign: "left" }}>
              Nacionalidad: {principal.nationality || "N/D"}{" "}
            </h4>
          </div>
        </CardBody>

        <CardBody>
          <CardHeader>
            <strong>Información personal</strong>
          </CardHeader>
          <table border="1" cellSpacing="0" cellPadding="4">
            <tbody>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Telefono: </b> {principal.phoneNumber || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Celular: </b> {principal.cellPhoneNumber || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Correo: </b>
                  {principal.email || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Dirección: </b>
                  {principal.homeAddress || "N/D"}
                </td>
              </tr>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Trabajo: </b>
                  {principal.workplace || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Ingreso: </b>
                  {principal.grossMonthlyIncome || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Agente: </b>
                  {principal.salesAdvisor || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Oficial: </b>
                  {principal.creditOfficer || "N/D"}
                </td>
              </tr>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Financiera: </b>
                  {principal.financialInstitution || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Comentarios: </b>
                  {principal.comments || "N/D"}
                </td>

                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Perfil: </b>
                  {principal.profile || ""}
                </td>
              </tr>
            </tbody>
          </table>
        </CardBody>

        <CardBody>
          <CardHeader>
            <strong>Información Coudeudor</strong>
          </CardHeader>
          <table border="1" cellSpacing="0" cellPadding="4">
            <tbody>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Codeudor: </b>
                  {getClientFullName(coDebtor) || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Telefono: </b>
                  {coDebtor && (coDebtor.phoneNumber || "N/D")}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Celular: </b>
                  {coDebtor && (coDebtor.cellPhoneNumber || "N/D")}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Correo: </b>
                  {coDebtor && (coDebtor.email || "N/D")}
                </td>
              </tr>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Trabajo: </b>
                  {coDebtor && (coDebtor.workplace || "N/D")}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Ingreso: </b>
                  {coDebtor && (coDebtor.grossMonthlyIncome || "N/D")}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Identidad: </b>
                  {coDebtor && (coDebtor.identificationCard || "N/D")}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Nacionalidad: </b>
                  {coDebtor && (coDebtor.nationality || "N/D")}
                </td>
              </tr>
            </tbody>
          </table>
        </CardBody>
        <CardBody>
          <CardHeader>
            <strong>Información Financiera</strong>
          </CardHeader>
          <table border="1" cellSpacing="0" cellPadding="4">
            <tbody>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Proyecto: </b>
                  {principal.project || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Contacto: </b>
                  {principal.contactType || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Financiera: </b>
                  {principal.financialInstitution || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Correo: </b>
                  {principal.coDebtorEmail || "N/D"}
                </td>
              </tr>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Trabajo: </b>
                  {coDebtor && (coDebtor.workplace || "N/D")}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Ingreso: </b>
                  {coDebtor && (coDebtor.grossMonthlyIncome || "N/D")}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Identidad: </b>
                  {coDebtor && (coDebtor.identificationCard || "N/D")}
                </td>
              </tr>
            </tbody>
          </table>
        </CardBody>
        <CardBody>
          <CardHeader>
            <strong>Información de seguimiento</strong>
          </CardHeader>
          {getTrackingInfo(clientTracking)}
        </CardBody>
        <CardBody>
          <div style={{ display: "block" }}>
            <h2 style={{ textAlign: "right" }}>
              Jade Real Estate - {moment(Date.now()).format("L")}
            </h2>
          </div>
        </CardBody>
      </div>
    );
  }
}

export default ClientPrintPreview;
