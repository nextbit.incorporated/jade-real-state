import React from "react";
import ReactToPrint from "react-to-print";
import ClientPrintPreview from "./ClientPrintPreview";
import { Col, FormGroup, Button } from "reactstrap";

class PrintRowInfoClient extends React.Component {
  render() {
    const client = this.props.clientToPrint;
    const clientTracking = this.props.clientTrackingToPrint;
    return (
      <div>
        <FormGroup row>
          <Col />
          <Col />

          <Col>
            <ReactToPrint
              trigger={() => (
                <Button block color="success">
                  Imprimir Ficha
                </Button>
              )}
              content={() => this.componentRef}
            />
          </Col>

          <Col>
            <Button block color="success" onClick={this.props.cancelPrintRow}>
              Cancelar
            </Button>
          </Col>
        </FormGroup>
        <ClientPrintPreview
          principal={client.principal}
          coDebtor={client.coDebtor}
          clientTracking={clientTracking}
          ref={(el) => (this.componentRef = el)}
        />
      </div>
    );
  }
}

export default PrintRowInfoClient;
