import React, { useState } from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

const ClientOperations = ({ client, coDebtor, operations }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen((prevState) => !prevState);

  const dropdownItems = operations.map((action) => {
    return (
      <DropdownItem
        key={action.id}
        onClick={() => action.onClick(client, coDebtor, action.operation)}
      >
        {action.operation}
      </DropdownItem>
    );
  });

  return (
    <Dropdown isOpen={dropdownOpen} toggle={toggle}>
      <DropdownToggle caret>Gestionar</DropdownToggle>
      <DropdownMenu>{dropdownItems}</DropdownMenu>
    </Dropdown>
  );
};

export default ClientOperations;
