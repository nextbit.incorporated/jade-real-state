import React from "react";
import moment from "moment";
import { Button } from "reactstrap";
import ClientOperations from "./ClientOperations";

const ClientsTableRows = ({
  clients,
  editRow,
  deleteClient,
  printRow,
  viewClientTracking,
  toggleAlarm,
  operations,
}) => {
  const tieneRecordatorios = (client) => {
    if (client.alarms) {
      if (client.alarms.length >= 1) {
        return <i className="cui-bell icons font-2xl d-block mt-2"></i>;
      }
      return <i className="cui-ban icons font-2xl d-block mt-2"></i>;
    } else {
      return <i className="cui-ban icons font-2xl d-block mt-2"></i>;
    }
  };

  const clientRows =
  
    clients === null || clients === undefined ? (
      <tr>
        <td colSpan={19}>Buscando...</td>
      </tr>
    ) : clients.length === 0 ? (
      <tr>
        <td colSpan={19}>
          No se encontraron clientes con el filtro especificado.
        </td>
      </tr>
    ) : (
      clients.map((client) => (
        <tr key={client.principal.id}>
          <td>
            <Button
              block
              color="warning"
              onClick={() => {
                editRow(client);
              }}
            >
              Editar
            </Button>
          </td>
          <td style={{ minheight: '300px' }}>
            {/* {client.principal.isActive === true ? (
              <Button
                block
                color="danger"
                onClick={() => deleteClient(client.principal.id, false)}
              >
                Inactivar
              </Button>
            ) : (
              <Button
                block
                color="success"
                onClick={() => deleteClient(client.principal.id, true)}
              >
                Activar
              </Button>
            )} */}

            <ClientOperations
              client={client.principal}
              coDebtor={client.coDebtor}
              operations={operations}
            />
          </td>
          <td>
            <Button block color="success" onClick={() => printRow(client)}>
              Imprimir
            </Button>
          </td>
          <td>
            <Button
              block
              color="success"
              onClick={() => viewClientTracking(client)}
            >
              Seguimiento
            </Button>
          </td>
          <td>
            <Button block color="primary" onClick={() => toggleAlarm(client)}>
              Alarmas
            </Button>
          </td>
          <td style={{ whiteSpace: "nowrap" }}>{tieneRecordatorios(client)}</td>
          <td nowrap="true">{client.principal.id}</td>
          <td nowrap="true">{client.principal.clientCode}</td>
          <td nowrap="true">
            {moment(client.principal.creationDate).format("L")}
          </td>
          <td nowrap="true">
            {moment(client.principal.transactionDate).format("L")}
          </td>

          <td nowrap="true">{client.principal.firstName}</td>
          <td nowrap="true">{client.principal.middleName}</td>
          <td nowrap="true">{client.principal.firstSurname}</td>
          <td nowrap="true">{client.principal.secondSurname}</td>
          <td style={{ whiteSpace: "nowrap" }}>
            {client.principal.identificationCard}
          </td>
          <td nowrap="true">{client.principal.nationality}</td>
          <td style={{ whiteSpace: "nowrap" }}>
            {client.principal.cellPhoneNumber}
          </td>
          <td style={{ whiteSpace: "nowrap" }}>
            {client.principal.phoneNumber}
          </td>
          <td nowrap="true">{client.principal.email}</td>

          <td style={{ whiteSpace: "nowrap" }}>{client.principal.profile}</td>

          <td style={{ whiteSpace: "nowrap" }}>{client.principal.workplace}</td>
          <td style={{ whiteSpace: "nowrap" }}>
            {client.principal.workStartDate
              ? moment(client.principal.workStartDate).format("L")
              : ""}
          </td>
          <td nowrap="true">{client.principal.debtorCurrency}</td>
          <td nowrap="true">{client.principal.grossMonthlyIncome}</td>
          <td nowrap="true">{client.principal.clientCategory}</td>
          <td style={{ whiteSpace: "nowrap" }}>{client.principal.project}</td>
          <td style={{ whiteSpace: "nowrap" }}>
            {client.principal.contactType}
          </td>

          <td nowrap="true">
            {client.coDebtor ? client.coDebtor.firstName : ""}
          </td>
          <td nowrap="true">
            {client.coDebtor ? client.coDebtor.middleName : ""}
          </td>
          <td nowrap="true">
            {client.coDebtor ? client.coDebtor.firstSurname : ""}
          </td>
          <td nowrap="true">
            {client.coDebtor ? client.coDebtor.secondSurname : ""}
          </td>
          <td nowrap="true">
            {client.coDebtor ? client.coDebtor.nationality : ""}
          </td>

          <td style={{ whiteSpace: "nowrap" }}>
            {client.coDebtor ? client.coDebtor.IdentificationCard : ""}
          </td>

          <td nowrap="true">
            {client.coDebtor ? client.coDebtor.cellPhoneNumber : ""}
          </td>
          <td nowrap="true">
            {client.coDebtor ? client.coDebtor.phoneNumber : ""}
          </td>
          <td nowrap="true">{client.coDebtor ? client.coDebtor.email : ""}</td>
          <td style={{ whiteSpace: "nowrap" }}>
            {client.coDebtor ? client.coDebtor.workplace : ""}
          </td>
          <td nowrap="true">
            {client.coDebtor ? client.coDebtor.currency : ""}
          </td>
          <td nowrap="true">
            {client.coDebtor ? client.coDebtor.grossMonthlyIncome : 0}
          </td>

          <td style={{ whiteSpace: "nowrap" }}>
            {client.principal.homeAddress}
          </td>
          <td nowrap="true">{client.principal.ownHome ? "Si" : ""}</td>
          <td nowrap="true">{client.principal.contributeToRap ? "Si" : ""}</td>
          <td style={{ whiteSpace: "nowrap" }}>{client.principal.comments}</td>
          <td nowrap="true">{client.principal.financialInstitution}</td>
          <td style={{ whiteSpace: "nowrap" }}>
            {client.principal.creditOfficer}
          </td>
          <td style={{ whiteSpace: "nowrap" }}>
            {client.principal.salesAdvisor}
          </td>

          <td nowrap="true">{client.principal.negotiationEnded ? "Si" : ""}</td>
        </tr>
      ))
    );

  return clientRows;
};

export default ClientsTableRows;
