import React, { useState, useEffect } from "react";
import API from "./../../API/API";
import { Redirect } from "react-router-dom";
import { CardBody, Col, Row } from "reactstrap";
import ClientsTable from "./../ClientsTable/ClientsTable";
import ClientDetails from "./../ClientDetails/ClientDetails";
import SecondaryClientDetails from "./../SecondaryClientDetails/SecondaryClientDetails";
import { generateExelFile } from "./../../Constants/generateExelFile";
import PrintRowInfoClient from "./../ClientsTable/printRowInfoClient";
import ClientTrackingViewIndividual from "./../../ClientsTracking/ClientTrackingViewIndividual";
import { ToastContainer, toast } from "react-toastify";
import UseApplicationOptions from "./../../UseApplicationOptions/UseApplicationOptions";
import moment from "moment";

import OperationsModal from "./../ClientDetails/OperationsModal";

const heightStyle = {
  height: "500px",
};

const initialApplicationOptions = {
  projects: [],
  contactTypes: [],
  nationalities: [],
  financialInstitutions: [],
  salesAdvisors: [],
  supplierServices: [],
  clientCategories: [],
};

const initialClientState = {
  step: 1,
  editMode: "None",
  saveStatus: false,
  currentClient: {
    coDebtorAssignmentType: "",
    principal: {
      id: 0,
      clientCode: "",
      creationDate: moment(Date.now()).format("YYYY-MM-DD"),
      projectId: 0,
      project: "",
      contactTypeId: 0,
      contactType: "",
      nationalityId: 0,
      nationality: "",
      clientCategoryId: 0,
      clientCategory: "",
      financialInstitutionId: 0,
      financialInstitution: "",
      salesAdvisorId: 0,
      salesAdvisor: "",
      firstName: "",
      middleName: "",
      firstSurname: "",
      secondSurname: "",
      identificationCard: "",
      phoneNumber: "",
      cellPhoneNumber: "",
      email: "",
      workplace: "",
      workStartDate: null,
      debtorCurrency: "LPS",
      grossMonthlyIncome: 0,
      homeAddress: "",
      ownHome: false,
      firstHome: false,
      contributeToRap: false,
      comments: "",
      creditOfficer: "",
      supplierServiceId: 0,
      service: "",
      negotiationEnded: false,
      categoryItems: [],
      objections: [],
    },
    coDebtor: null,
  },
  applicationOptions: initialApplicationOptions,

  state: {
    modal: false,
    large: false,
    large1: false,
    small: false,
    primary: false,
    success: false,
    warning: false,
    danger: false,
    info: false,
    uploadFile: false,
    className: null,
  },
};

const initialOperationsData = {
  isOpen: false,
  options: [],
  operation: {
    clientId: 0,
    operationName: "",
    operationType: "",
    clientCategoryId: 0,
    profile: "",
    financialInstitutionId: 0,
    creditOfficer: "",
    salesAdvisorId: 0,
    isActive: true,
    selectedOptions: [],
    comments: "",
    hasCoDebtop: false,
    applyOperationToCoDebtop: false,
    user: "",
  },
};

const MainClientsForm = (props) => {
  const [clientState, setClientState] = useState(initialClientState);
  const [clients, setClients] = useState([]);
  const [clientsPaged, setClientsPaged] = useState([]);
  const [clientToPrint, setClientToPrint] = useState([]);
  const [clientTrackingToPrint, setClientTrackingToPrint] = useState([]);
  const [clientTrackingSelected, setClientTrackingSelected] = useState([]);
  const [clientTrackings, setClienttrackings] = useState([]);
  const [apiCallInProgress, setApiCallInProgress] = useState(false);
  const [isActive, setIsActive] = useState(true);
  const [excludeNegotiationEnded, setExcludeNegotiationEnded] = useState(true);
  const [operationsData, setOperationsData] = useState(initialOperationsData);
  const [checked, setChecked] = useState([]);

  const { applicationOptions } = UseApplicationOptions();

  useEffect(() => {
    fetchClientsPaged("", 0);
  }, [isActive]);

  const toggleShowActive = () => {
    setIsActive(!isActive);
  };

  const toggleExcludeNegotiationEnded = () => {
    setExcludeNegotiationEnded(!excludeNegotiationEnded);
  };

  function fetchClients(query) {
    const user = sessionStorage.getItem("userId");
    const url = query
      ? `clients?request.query=${query}&&request.user=${user}`
      : `clients?request.user=${user}`;

    API.get(url).then((res) => {
      setClients(res.data);
    });
  }

  function fetchClientsPaged(query, step) {
    const user = sessionStorage.getItem("userId");

    setApiCallInProgress(true);
    setClientState({ ...clientState, step: 1 });
    const index = step === undefined || step === null ? 0 : step;
    if (query === null || query === "") {
      var url = `clients/paged?pageSize=10&pageIndex=${index}&user=${user}&isActive=${isActive}&excludeNegotiationEnded=${excludeNegotiationEnded}`;

      API.get(url)
        .then((res) => {
          setApiCallInProgress(false);
          setClientState({ ...clientState, step: 1 });
          setClientsPaged(res.data);
        })
        .catch((error) => {
          setApiCallInProgress(false);
          if (error.message === "Network Error") {
            toast.warn(
              "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
            );
          } else {
            if (error.response.status !== undefined) {
              if (error.response.status === 401) {
                props.history.push("/login");
                return <Redirect to="/login" />;
              }
            } else {
              toast.warn(
                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
              );
            }
          }
        });
    } else {
      var url1 = `clients/paged?pageSize=10&pageIndex=${index}&user=${user}&value=${query}&isActive=${isActive}&excludeNegotiationEnded=${excludeNegotiationEnded}`;
      API.get(url1).then((res) => {
        setApiCallInProgress(false);
        setClientState({ ...clientState });
        setClientsPaged(res.data);
      });
    }
  }

  const printRow = (client) => {
    setClientToPrint(client);
    getDataClientTracking(client.id);
    setClientState({ ...clientState, currentClient: client, step: 4 });
  };

  function getDataClientTracking(id) {
    const url = `clientTracking?clientId=${id}`;
    API.get(url)
      .then((res) => {
        setClientTrackingToPrint(res.data || []);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login" />;
        }
      });
  }

  const viewClientTracking = (client) => {
    setClientTrackingSelected(client);

    setClientState({ ...clientState, currentClient: client, step: 5 });
  };

  const cancelPrintRow = () => {
    setClientState({ ...clientState, step: 1 });
  };

  function nextPage(step, value) {
    fetchClientsPaged(value, step);
  }

  function prevPage(step, value) {
    fetchClientsPaged(value, step);
  }

  const saveChanges = (client) => {
    switch (clientState.editMode) {
      case "Adding": {
        addClient(client);

        break;
      }
      case "Editing":
        updateClient(client);
        break;

      default:
        break;
    }
  };

  const clientInfotmationIsValid = (client) => {
    if (!client || !client.principal || !client.principal.firstName) {
      toast.warn("El primer Nombre del cliente es Requerido.");

      return false;
    }

    if (client && client.coDebtor) {
      if (!client.coDebtor.firstName) {
        toast.warn("El primer Nombre del Co-deudor es Requerido.");

        return false;
      }
    }

    if (client.principal.contactTypeId === -1) {
      toast.warn("Debe seleccionar el tipo de contacto.");

      return false;
    }

    return true;
  };

  const addClient = (client) => {
    if (!clientInfotmationIsValid(client)) {
      return;
    }

    const newClient = {
      ...client,
      user: sessionStorage.getItem("userId"),
    };

    const url = `clients`;

    setApiCallInProgress(true);

    API.post(url, newClient)
      .then((res) => {
        setApiCallInProgress(false);
        fetchClientsPaged(null, 0);
        setClientState(initialClientState);
        toast.success("Cliente agregado satisfactorimente");
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para crear el nuevo cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  };

  const updateClient = (client) => {
    if (!clientInfotmationIsValid(client)) {
      return;
    }

    const updatedClient = {
      ...client,
      user: sessionStorage.getItem("userId"),
    };

    const url = `clients`;

    setApiCallInProgress(true);

    API.put(url, updatedClient)
      .then((res) => {
        setApiCallInProgress(false);

        setClients(
          clients.map((client) =>
            client.id === updatedClient.id ? updatedClient : client
          )
        );
        fetchClientsPaged(null, 0);
        setClientState(initialClientState);
        toast.success("datos del cliente modificados satisfactorimente");
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  };

  const showNewDialog = () => {
    const client = {
      ...initialClientState.currentClient,
      creationDate: moment(Date.now()).format("YYYY-MM-DD"),
    };

    setClientState({
      ...clientState,
      editMode: "Adding",
      currentClient: client,
      step: 2,

      applicationOptions: applicationOptions,
    });
  };

  const toggleIsOpenOperationsModal = () => {
    const updatedOperationsData = {
      ...operationsData,
      isOpen: !operationsData.isOpen,
    };

    setOperationsData(updatedOperationsData);
  };

  const onClickOperation = (client, coDebtor, operation) => {
    const options = getOptionsByOperation(client, operation);

    const updatedOperationsData = {
      ...operationsData,
      isOpen: true,
      options: options,
      operation: {
        ...operationsData.operation,
        clientId: client.id,
        clientName: `${client.firstName} ${client.middleName} ${client.firstSurname} ${client.secondSurname}`,
        operationName: operation,
        operationType: operation,
        clientCategoryId: client.clientCategoryId,
        profile: client.profile,
        financialInstitutionId: client.financialInstitutionId,
        creditOfficer: client.creditOfficer,
        salesAdvisorId: client.salesAdvisorId,
        isActive: true,
        selectedOptions: getSelectedOptions(client, operation),
        comments: "",
        hasCoDebtop: coDebtor ? true : false,
        applyOperationToCoDebtop: false,
        user: sessionStorage.getItem("userId"),
      },
    };

    setOperationsData(updatedOperationsData);
  };

  const getSelectedOptions = (client, operation) => {
    switch (operation) {
      case "Cambiar Categoria":
        return client.categoryItems ?? [];

      case "Cambiar Objeciones":
        return client.objections ?? [];

      default:
        return [];
    }
  };

  const getAllOperations = () => {
    if (sessionStorage.getItem("role") === "admin") {
      return [
        {
          id: 0,
          operation: "Cambiar Categoria",
          onClick: onClickOperation,
        },
        {
          id: 1,
          operation: "Cambiar Perfil",
          onClick: onClickOperation,
        },
        {
          id: 2,
          operation: "Cambiar Institucion Financiera",
          onClick: onClickOperation,
        },
        {
          id: 3,
          operation: "Cambiar Asesor",
          onClick: onClickOperation,
        },
        {
          id: 4,
          operation: "Cambiar Objeciones",
          onClick: onClickOperation,
        },
      ];
    } else {
      return [
        {
          id: 0,
          operation: "Cambiar Categoria",
          onClick: onClickOperation,
        },
        {
          id: 1,
          operation: "Cambiar Perfil",
          onClick: onClickOperation,
        },
        {
          id: 2,
          operation: "Cambiar Institucion Financiera",
          onClick: onClickOperation,
        },
        {
          id: 4,
          operation: "Cambiar Objeciones",
          onClick: onClickOperation,
        },
      ];
    }
  };

  const operations = getAllOperations();

  const editRow = (client) => {
    setClientState({
      ...clientState,
      editMode: "Editing",
      currentClient: client,
      step: 2,
      applicationOptions: applicationOptions,
    });
  };

  const deleteClient = (id, isActive) => {
    const user = sessionStorage.getItem("userId");

    const url = `clients?id=${id}&user=${user}&isActive=${isActive}`;

    API.delete(url).then((res) => {
      setClients(clients.filter((client) => client.id !== id));
      setClientState(initialClientState);
      fetchClientsPaged(null, 0);
      toast.success("Cliente eliminado correctamente");
    });
  };

  function nextStep(client) {
    const { step } = clientState;

    setClientState({ ...clientState, currentClient: client, step: step + 1 });
  }

  function prevStep() {
    const { step } = clientState;

    setClientState({ ...clientState, step: step - 1 });
  }

  const cancelChanges = () => {
    setClientState(initialClientState);
  };

  function compareNames(currentItem, nextItem) {
    const currentName = currentItem.name;
    const nextName = nextItem.name;

    let comparison = 0;
    if (currentName > nextName) {
      comparison = 1;
    } else if (currentName < nextName) {
      comparison = -1;
    }
    return comparison;
  }

  function compareLevels(currentItem, nextItem) {
    const currentName = currentItem.level;
    const nextName = nextItem.level;

    let comparison = 0;
    if (currentName > nextName) {
      comparison = 1;
    } else if (currentName < nextName) {
      comparison = -1;
    }
    return comparison;
  }

  function fetchClientTrackings() {
    const url = `clientTracking?clientId=${clientTrackingSelected.principal.id}`;
    API.get(url).then((res) => {
      setClienttrackings(res.data || []);
    });
  }

  const style = {
    height: "100%",
    maxwidth: "100%",
    maxheight: "100%",
    margin: "0",
    padding: "0",
    marginleft: "0px",
    marginright: "0px",
    paddingright: "0px",
    paddingleft: "0px",
  };

  function downloadXLSX(step) {
    const filename = "Formato_ingreso_clientes.xlsx";
    const formatColumns = [
      "Fecha de Creacion",
      "Proyecto",
      "Tipo de Contacto",
      "Nacionalidad",
      "Primer Nombre",
      "Segundo Nombre",
      "Primer Apellido",
      "Segundo Apellido",
      "Numero de Telefono",
      "Telefono Celular",
      "Numero de Cedula",
      "Email",
      "Lugar de Trabajo",
      "Moneda Deudor",
      "Ingresos Mensuales",
      "Primer Nombre Codeudor",
      "Segundo Nombre Codudor",
      "Primer Apellido Codeudor",
      "Segundo Apellido Codeudor",
      "Nacionalidad del Codeudor",
      "Numero de Telefono Codeudor",
      "Telefono Celular Codeudor",
      "Numero de Cedula Codeudor",
      "Email del Codeudor",
      "Lugar de Trabajo Codeudor",
      "Moneda Codeudor",
      "Ingresos Mensuales Codeudor",
      "Direccion de Residencia",
      "Comentarios",
      "Institucion Financiera",
      "Oficial de Credito",
      "Posee Vivienda",
      "Constribuye al Rap",
      "Asesor",
    ];
    generateExelFile(filename, formatColumns);
  }

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody style={style}>
            <Row>
              <Col xs="12">
                <ClientsTable
                  clients={clients}
                  isActive={isActive}
                  toggleShowActive={toggleShowActive}
                  excludeNegotiationEnded={excludeNegotiationEnded}
                  toggleExcludeNegotiationEnded={toggleExcludeNegotiationEnded}
                  fetchClients={fetchClients}
                  editRow={editRow}
                  updateClient={updateClient}
                  deleteClient={deleteClient}
                  nextStep={nextStep}
                  prevStep={prevStep}
                  clientsPaged={clientsPaged}
                  nextPage={nextPage}
                  prevPage={prevPage}
                  fetchClientsPaged={fetchClientsPaged}
                  printRow={printRow}
                  viewClientTracking={viewClientTracking}
                  loadingStatus={apiCallInProgress}
                  downloadXLSX={downloadXLSX}
                  showNewDialog={showNewDialog}
                  operations={operations}
                />
              </Col>
            </Row>
          </CardBody>
        );
      case 2:
        return (
          <ClientDetails
            clientState={clientState}
            setClientState={setClientState}
            saveChanges={saveChanges}
            cancelChanges={cancelChanges}
            apiCallInProgress={apiCallInProgress}
            setApiCallInProgress={setApiCallInProgress}
          />
        );
      case 3:
        return (
          <SecondaryClientDetails nextStep={nextStep} prevStep={prevStep} />
        );
      case 4:
        return (
          <PrintRowInfoClient
            clientToPrint={clientToPrint}
            cancelPrintRow={cancelPrintRow}
            clientTrackingToPrint={clientTrackingToPrint}
          />
        );
      case 5:
        return (
          <ClientTrackingViewIndividual
            client={clientTrackingSelected}
            cancelChanges={cancelChanges}
            fetchClientTrackings={fetchClientTrackings}
            clientTrackings={clientTrackings}
            setClienttrackings={setClienttrackings}
          />
        );
      default:
        return null;
    }
  }

  const clientsStep = GetCurrentStepComponent(clientState.step);

  const onCloseOperationsModal = () => {
    const updatedOperationsData = {
      ...operationsData,
      isOpen: false,
    };

    setOperationsData(updatedOperationsData);
  };

  const handleOperationsInputChange = (event) => {
    // const target = event.target;
    // const value = target.value;
    // const name = target.name;

    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    const updatedOperationsData = {
      ...operationsData,
      operation: {
        ...operationsData.operation,
        [name]: value,
      },
    };
    setOperationsData(updatedOperationsData);
  };

  const getOptionsByOperation = (client, operation) => {
    switch (operation) {
      case "Cambiar Categoria":
        return getOptions("clientCategoryId", client.clientCategoryId);
      case "Cambiar Objeciones":
        return getObjections();

      default:
        return [];
    }
  };

  const getObjections = () => {
    return [
      "Pre-calificacion Pendiente",
      "Segunda Pre-calficacion",
      "Decisiones claves del cliente",
      "Cambio de precios",
      "Completar Doctos.Cliente al Banco",
      "Cotizacion Constructora",
      "Completar Expediente Constructora",
      "Contrato Compra Venta",
      "Planos y Presupuestos",
      "Firma de Contrato",
      "Carta Anuencia de Liberacion",
      "Constancia de Avaluo",
      "Falta de Seguimiento de Ventas",
      "Firma de Protocolo",
      "Pago de gastos de cierre",
      "Otras Objecciones",
    ];
  };

  const getOptions = (name, value) => {
    if (name === "clientCategoryId") {
      const options = applicationOptions.clientCategories.filter(
        (o) => o.props.id === value
      );

      if (options && options.length > 0) {
        switch (options[0].props.children) {
          case "Cliente A Aplica para la opción de vivienda":
            return [
              "Buen historial",
              "Capacidad de pago",
              "Decidido a iniciar tramite",
              "Le gusta la opción de vivienda presentada y el sector donde se le está ofertando",
              "Cumple con todos los requisitos",
            ]; //.map((item, index) => ({ id: index, checked: false, name: item }));

          case "Cliente B Buen historial":
            return [
              "Buen historial",
              "Capacidad de pago",
              "Le gustaron los acabados de las viviendas. Pero no están seguros con el sector. Quieren ver otras opciones en otros sectores",
              "Cliente que, aunque tiene capacidad. Quiere pagar menos cuota, la opción presentada la siente cara. (Ofertar Girona o Valencia)",
              "Cliente que califica por Buro y capacidad, pero anda cotizando con varias constructoras. Requiere mayor seguimiento.",
            ]; //.map((item, index) => ({ id: index, checked: false, name: item }));

          case "Cliente C No aplica por los momentos":
            return [
              "Presenta moras más de 60 dias (Mal Buro).",
              "Debe pagar deudas (Alto endeudamiento) necesita consolidar deudas.",
              "Tiene finiquito. Pero es reciente, tiene que esperar seis meses.",
              "No cuenta con escritura de comerciante individual, o Permisos de operación) pero se puede ordenar.",
              "Le pagan en efectivo. Tiene que realizar depósitos los próximos seis meses.",
              "Debe hacer movimientos en su cuenta de banco (Movimientos pobres no reflejan los ingresos que el reporta).",
            ]; //.map((item, index) => ({ id: index, checked: false, name: item }));

          case "Cliente Inactivo":
            return [
              "No le interesan las opciones de vivienda.",
              "No es posible contactarlo.",
              "Cliente ya compro.",
              "Perfil no apto.",
              "Devolucion Reserva.",
            ]; //.map((item, index) => ({ id: index, checked: false, name: item }));

          default:
            return [];
        }
      }
    }

    return [];
  };

  const handleOperationsNumericInputChange = (event) => {
    const target = event.target;
    const value = parseInt(target.value, 10);
    const name = target.name;

    const options = getOptions(name, value);

    const updatedOperationsData = {
      ...operationsData,
      options: options,
      operation: {
        ...operationsData.operation,
        [name]: value,
      },
    };
    setOperationsData(updatedOperationsData);
  };

  const handleCheck = (event) => {
    const updatedList = event.target.checked
      ? [...operationsData.operation.selectedOptions, event.target.name]
      : operationsData.operation.selectedOptions.filter(
          (o) => o !== event.target.name
        );

    const updatedOperationsData = {
      ...operationsData,
      operation: {
        ...operationsData.operation,
        selectedOptions: updatedList,
      },
    };
    setOperationsData(updatedOperationsData);
  };

  const saveOperation = async () => {
    const url = `operations`;

    setApiCallInProgress(true);

    const updatedOperation = {
      ...operationsData.operation,
      user: sessionStorage.getItem("userId"),
    };

    API.post(url, updatedOperation)
      .then((res) => {
        setApiCallInProgress(false);

        setOperationsData(initialOperationsData);

        fetchClientsPaged(null, 0);

        toast.success("Gestion realizada satisfactorimente");
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  };

  return (
    <div className="animated fadeIn" style={heightStyle}>
      {clientsStep}

      {operationsData.isOpen === true && (
        <OperationsModal
          operationsData={operationsData}
          applicationOptions={applicationOptions}
          onClose={onCloseOperationsModal}
          toggle={toggleIsOpenOperationsModal}
          handleInputChange={handleOperationsInputChange}
          handleNumericInputChange={handleOperationsNumericInputChange}
          handleCheck={handleCheck}
          saveOperation={saveOperation}
        />
      )}

      <ToastContainer />
    </div>
  );
};

export default MainClientsForm;
