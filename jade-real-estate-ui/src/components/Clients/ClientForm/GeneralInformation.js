import React, { useState } from "react";
import styled from "styled-components";
import {
  Col,
  FormGroup,
  Input,
  Label,
  Row,
  Modal,
  ModalHeader,
  ModalFooter,
  CardBody,
} from "reactstrap";

import moment from "moment";
import Button from "reactstrap/lib/Button";
import ModalBody from "reactstrap/lib/ModalBody";
import Card from "reactstrap/lib/Card";
import CardHeader from "reactstrap/lib/CardHeader";

const Root = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
`;

const GeneralInformation = ({
  client,
  applicationOptions,
  handleInputChange,
  handleNumericInputChange,
  editMode,
}) => {
  const [modalOpenState, setModalOpenState] = useState(false);

  const toggleAlarm = () => {
    setModalOpenState(!modalOpenState);
  };

  const openModal = () => {
    setModalOpenState(true);
  };

  return (
    <Root>
      <Row>
        <Col>
          <FormGroup>
            <legend>Informacion General</legend>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="creationDate">Fecha de Creacion</Label>
            <Input
              type="date"
              name="creationDate"
              placeholder="Fecha de Creacion"
              value={moment(client.creationDate).format("YYYY-MM-DD")}
              onChange={handleInputChange}
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="email">
              Categoria de cliente{" "}
              <Button color="primary" onClick={openModal}>
                info
              </Button>
            </Label>
            <Input
              type="select"
              name="clientCategoryId"
              value={client.clientCategoryId ?? 0}
              onChange={handleNumericInputChange}
              disabled={editMode === "Editing"}
            >
              {applicationOptions.clientCategories}
            </Input>
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Proyecto</Label>
            <Input
              type="select"
              name="projectId"
              pattern="[0-9]*"
              value={client.projectId ?? 0}
              onChange={handleNumericInputChange}
            >
              {applicationOptions.projects}
            </Input>
          </FormGroup>
        </Col>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Tipo de Contacto</Label>
            <Input
              type="select"
              name="contactTypeId"
              value={client.contactTypeId ?? 0}
              onChange={handleNumericInputChange}
            >
              {applicationOptions.contactTypes}
            </Input>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup>
            <Label htmlFor="comments">Comentarios</Label>
            <Input
              type="text"
              name="comments"
              value={client.comments ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>
      <Modal
        isOpen={modalOpenState}
        toggle={toggleAlarm}
        className={"modal-lg"}
      >
        <ModalBody>
          <Card>
            <CardHeader>
              <strong>Categorias de cliente</strong>
            </CardHeader>
            <CardBody>
              <Input
                type="textarea"
                id="perfilCategoria"
                name="perfilCategoria"
                value={applicationOptions.infoClientLevel ?? ""}
                required
                readOnly
                style={{ height: "600px" }}
              />
            </CardBody>
          </Card>
        </ModalBody>
        <ModalFooter>
          <Row>
            <Col Col md="3" sm="6" xs="12">
              <Button color="warning" onClick={toggleAlarm}>
                Cerrar
              </Button>
            </Col>
          </Row>
        </ModalFooter>
      </Modal>
    </Root>
  );
};

export default GeneralInformation;
