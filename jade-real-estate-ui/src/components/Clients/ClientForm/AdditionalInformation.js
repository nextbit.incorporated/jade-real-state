import React from "react";
import styled from "styled-components";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";

const Root = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
`;

const AdditionalInformation = ({ client }) => {
  return (
    <Root>
      <Row>
        <Col>
          <FormGroup>
            <legend>Informacion Adicional</legend>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col md="3" sm="6" xs="12">
          <Row>
            <FormGroup>
              <Label>Posee Vivienda</Label>
              <Input
                type="text"
                value={client.ownHome ? "SI" : "NO"}
                readOnly
              />
            </FormGroup>
          </Row>
          <Row>
            <FormGroup>
              <Label>Primera Vivienda</Label>
              <Input
                type="text"
                value={client.firstHome ? "SI" : "NO"}
                readOnly
              />
            </FormGroup>
          </Row>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Contribuye al RAP</Label>
            <Input
              type="text"
              value={client.contributeToRap ? "SI" : "NO"}
              readOnly
            />
          </FormGroup>
        </Col>
      </Row>
    </Root>
  );
};

export default AdditionalInformation;
