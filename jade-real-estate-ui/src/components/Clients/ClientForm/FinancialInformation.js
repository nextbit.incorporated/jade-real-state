import React from "react";
import styled from "styled-components";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";

const Root = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
`;

const FinancialInformation = ({
  client,
  applicationOptions,
  handleInputChange,
  handleNumericInputChange,
  editMode,
}) => {
  return (
    <Root>
      <Row>
        <Col>
          <FormGroup>
            <legend>Informacion Financiera</legend>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Servicio</Label>
            <Input
              type="select"
              name="supplierServiceId"
              value={client.supplierServiceId ?? 0}
              onChange={handleNumericInputChange}
            >
              {applicationOptions.supplierServices}
            </Input>
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <Row>
            <FormGroup check>
              <Label check>
                <Input
                  type="checkbox"
                  name="ownHome"
                  checked={client.ownHome}
                  onChange={handleInputChange}
                  required
                />{" "}
                Posee Vivienda
              </Label>
              <Label />
            </FormGroup>
          </Row>

          <Row>
            <FormGroup check>
              <Label check>
                <Input
                  type="checkbox"
                  name="firstHome"
                  checked={client.firstHome}
                  onChange={handleInputChange}
                  required
                />{" "}
                Primera Vivienda
              </Label>
              <Label />
            </FormGroup>
          </Row>
        </Col>
        <Col md="3" sm="6" xs="12">
          <FormGroup check>
            <Label check>
              <Input
                type="checkbox"
                name="contributeToRap"
                checked={client.contributeToRap}
                onChange={handleInputChange}
                required
              />{" "}
              Contribuye al RAP
            </Label>
            <Label />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Institucion Financiera</Label>
            <Input
              type="select"
              name="financialInstitutionId"
              value={client.financialInstitutionId ?? 0}
              onChange={handleNumericInputChange}
              required
            >
              {applicationOptions.financialInstitutions}
            </Input>
          </FormGroup>
        </Col>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="creditOfficer">Oficial de Credito</Label>
            <Input
              type="text"
              name="creditOfficer"
              value={client.creditOfficer ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Asesor de Ventas</Label>
            <Input
              type="select"
              name="salesAdvisorId"
              value={client.salesAdvisorId ?? 0}
              onChange={handleNumericInputChange}
              required
              disabled={editMode === "Editing"}
            >
              {applicationOptions.salesAdvisors}
            </Input>
          </FormGroup>
        </Col>
      </Row>
    </Root>
  );
};

export default FinancialInformation;
