import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";

const Root = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
`;

const PesonalInformation = ({
  client,
  readOnly,
  applicationOptions,
  handleInputChange,
  handleNumericInputChange,
  editMode,
}) => {
  const getPersonalInformation = () => {
    if (readOnly === true) {
      return getReadOnlyPersonalInformation();
    }

    return getEditablePersonalInformationComponent();
  };

  const getClientName = () => {
    return ` ${client.firstName} ${client.middleName} ${client.firstSurname} ${client.secondSurname}`;
  };

  const getPhoneNumbers = () => {
    return ` ${client.phoneNumber} ${client.cellPhoneNumber} `;
  };

  const getReadOnlyPersonalInformation = () => {
    return (
      <Root>
        <Col lg="12">
          <FormGroup>
            <Row>
              <strong>{"Nombre : "}</strong>
              {getClientName()}
            </Row>

            <Row>
              <strong>{"Numeros de Telefono : "}</strong>
              {getPhoneNumbers()}
            </Row>

            <Row>
              <strong>{"Email : "}</strong>
              {` ${client.email}`}
            </Row>
          </FormGroup>
        </Col>
      </Root>
    );
  };

  const getEditablePersonalInformationComponent = () => {
    return (
      <React.Fragment>
        <Row>
          <Col md="3" sm="6" xs="12">
            <FormGroup>
              <Label htmlFor="firstName">Primer Nombre</Label>
              <Input
                type="text"
                name="firstName"
                value={client.firstName ?? ""}
                onChange={handleInputChange}
                required
                readOnly={readOnly}
              />
            </FormGroup>
          </Col>
          <Col md="3" sm="6" xs="12">
            <FormGroup>
              <Label htmlFor="middleName">Segundo Nombre</Label>
              <Input
                type="text"
                name="middleName"
                value={client.middleName ?? ""}
                onChange={handleInputChange}
                required
                readOnly={readOnly}
              />
            </FormGroup>
          </Col>
          <Col md="3" sm="6" xs="12">
            <FormGroup>
              <Label htmlFor="firstSurname">Primer Apellido</Label>
              <Input
                type="text"
                name="firstSurname"
                value={client.firstSurname ?? ""}
                onChange={handleInputChange}
                required
                readOnly={readOnly}
              />
            </FormGroup>
          </Col>
          <Col md="3" sm="6" xs="12">
            <FormGroup>
              <Label htmlFor="secondSurname">Segundo Apellido</Label>
              <Input
                type="text"
                name="secondSurname"
                value={client.secondSurname ?? ""}
                onChange={handleInputChange}
                required
                readOnly={readOnly}
              />
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col md="3" sm="6" xs="12">
            <FormGroup>
              <Label htmlFor="phoneNumber">Numero de Telefono</Label>
              <Input
                type="text"
                name="phoneNumber"
                value={client.phoneNumber ?? ""}
                onChange={handleInputChange}
                required
                readOnly={readOnly}
              />
            </FormGroup>
          </Col>
          <Col md="3" sm="6" xs="12">
            <FormGroup>
              <Label htmlFor="cellPhoneNumber">Telefono Celular</Label>
              <Input
                type="text"
                name="cellPhoneNumber"
                value={client.cellPhoneNumber ?? ""}
                onChange={handleInputChange}
                required
                readOnly={readOnly}
              />
            </FormGroup>
          </Col>

          <Col md="3" sm="6" xs="12">
            <FormGroup>
              <Label htmlFor="identification">Email</Label>
              <Input
                type="text"
                name="email"
                value={client.email ?? ""}
                onChange={handleInputChange}
                required
                readOnly={readOnly}
              />
            </FormGroup>
          </Col>

          <Col md="3" sm="6" xs="12">
            <FormGroup>
              <Label htmlFor="identificationCard">Numero de Cedula</Label>
              <Input
                type="text"
                name="identificationCard"
                value={client.identificationCard ?? ""}
                onChange={handleInputChange}
                required
                readOnly={readOnly}
              />
            </FormGroup>
          </Col>

          <Col md="3" sm="6" xs="12">
            <FormGroup>
              <Label>Nacionalidad</Label>
              <Input
                type="select"
                name="nationalityId"
                value={client.nationalityId ?? 0}
                onChange={handleNumericInputChange}
                disabled={readOnly}
                readOnly={readOnly}
              >
                {applicationOptions.nationalities}
              </Input>
            </FormGroup>
          </Col>

          <Col>
            <FormGroup>
              {/*
              <Label htmlFor="profile">Perfil</Label>
              <Input
                type="text"
                name="profile"
                value={client.profile ?? ""}
                onChange={handleInputChange}
                required
                readOnly={readOnly}
              /> */}

              <Input
                type="select"
                name="profile"
                value={client.profile ?? ""}
                onChange={handleInputChange}
                disabled={editMode === "Editing"}
              >
                {applicationOptions.profiles}
              </Input>
            </FormGroup>
          </Col>

          <Col>
            <FormGroup>
              <Label htmlFor="homeAddress">Direccion de Residencia</Label>
              <Input
                type="text"
                name="homeAddress"
                value={client.homeAddress ?? ""}
                onChange={handleInputChange}
                required
                readOnly={readOnly}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row></Row>
      </React.Fragment>
    );
  };

  return (
    <React.Fragment>
      <Row>
        <Col>
          <legend>Informacion Personal</legend>
        </Col>
      </Row>
      {getPersonalInformation()}
    </React.Fragment>
  );
};

// Specifies the default values for props:
PesonalInformation.defaultProps = {
  readOnly: false,
};

PesonalInformation.propTypes = {
  readOnly: PropTypes.bool,
};

export default PesonalInformation;
