import React from "react";
import styled from "styled-components";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";
import moment from "moment";

const Root = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
`;

const LaboralInformation = ({
  client,
  applicationOptions,
  handleInputChange,
  handleNumericInputChange,
}) => {
  return (
    <Root>
      <Row>
        <Col>
          <FormGroup>
            <legend>Informacion Laboral</legend>
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="email">Lugar de Trabajo</Label>
            <Input
              type="text"
              name="workplace"
              value={client.workplace ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="workStartDate">Trabaja desde</Label>
            <Input
              type="date"
              name="workStartDate"
              placeholder="Labora desde"
              value={moment(client.workStartDate).format("YYYY-MM-DD")}
              onChange={handleInputChange}
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Moneda</Label>
            <Input
              type="select"
              name="debtorCurrency"
              value={client.debtorCurrency ?? ""}
              onChange={handleInputChange}
            >
              {applicationOptions.currencies}
            </Input>
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="grossMonthlyIncome">Ingresos Mensuales</Label>
            <Input
              type="number"
              name="grossMonthlyIncome"
              value={client.grossMonthlyIncome ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>
    </Root>
  );
};

export default LaboralInformation;
