import React from "react";
import PropTypes from "prop-types";
import { Col, Form, FormGroup, Row, Input, Label } from "reactstrap";

import PesonalInformation from "./PesonalInformation";
import GeneralInformation from "./GeneralInformation";
import FinancialInformation from "./FinancialInformation";
import LaboralInformation from "./LaboralInformation";
import AdditionalInformation from "./AdditionalInformation";

const ClientForm = ({
  title,
  client,
  updateClient,
  readOnlyPersonalInformation,
  showGeneralInformation,
  showFinancialInformation,
  showAdditionalInformation,

  applicationOptions,
  editMode,
}) => {
  const handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    const updatedClient = {
      ...client,
      [name]: value,
    };

    updateClient(updatedClient);
  };

  const handleNumericInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    const updatedClient = {
      ...client,
      [name]: parseInt(value, 10),
    };

    updateClient(updatedClient);
  };

  return (
    <Form>
      <Row>
        <Col>
          <FormGroup>
            <h1>{title}</h1>
          </FormGroup>
        </Col>

        <Col xs="auto">
          <FormGroup check>
            <Label check>
              <Input
                type="checkbox"
                name="negotiationEnded"
                checked={client.negotiationEnded}
                onChange={handleInputChange}
                required
              />{" "}
              Negociacion Finalizada
            </Label>
            <Label />
          </FormGroup>
        </Col>
      </Row>

      {showGeneralInformation && (
        <GeneralInformation
          client={client}
          readOnly={readOnlyPersonalInformation}
          applicationOptions={applicationOptions}
          handleInputChange={handleInputChange}
          handleNumericInputChange={handleNumericInputChange}
          editMode={editMode}
        ></GeneralInformation>
      )}

      <PesonalInformation
        client={client}
        readOnly={readOnlyPersonalInformation}
        applicationOptions={applicationOptions}
        handleInputChange={handleInputChange}
        handleNumericInputChange={handleNumericInputChange}
        editMode={editMode}
      ></PesonalInformation>

      <LaboralInformation
        client={client}
        applicationOptions={applicationOptions}
        handleInputChange={handleInputChange}
        handleNumericInputChange={handleNumericInputChange}
      ></LaboralInformation>

      {showFinancialInformation && (
        <FinancialInformation
          client={client}
          applicationOptions={applicationOptions}
          handleInputChange={handleInputChange}
          handleNumericInputChange={handleNumericInputChange}
          editMode={editMode}
        ></FinancialInformation>
      )}

      {showAdditionalInformation && (
        <AdditionalInformation
          client={client}
          handleInputChange={handleInputChange}
        ></AdditionalInformation>
      )}
    </Form>
  );
};

PesonalInformation.defaultProps = {
  readOnly: false,
  readOnlyPersonalInformation: false,
  showGeneralInformation: true,
  showFinancialInformation: true,
  showAdditionalInformation: false,
};

PesonalInformation.propTypes = {
  readOnly: PropTypes.bool,
  readOnlyPersonalInformation: PropTypes.bool,
  showGeneralInformation: PropTypes.bool,
  showFinancialInformation: PropTypes.bool,
  showAdditionalInformation: PropTypes.bool,
};

export default ClientForm;
