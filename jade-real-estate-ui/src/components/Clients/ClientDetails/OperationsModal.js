import React, { useState, useEffect } from "react";
import ReactLoading from "react-loading";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Row,
  FormGroup,
  Label,
  Input,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { ToastContainer, toast } from "react-toastify";

import moment from "moment";

import styled from "styled-components";

const ListContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  margin-left: 20px;
`;

const Container = styled.div`
  background-color: coral;
  margin: 0px;
`;

const Body = styled.div`
  height: 300px;
  overflow-y: auto;
  overflow-x: hidden;
`;

const OperationsModal = ({
  operationsData,
  applicationOptions,
  onClose,
  toggle,
  handleInputChange,
  handleNumericInputChange,
  handleCheck,
  saveOperation,
}) => {
  const getSectionToEdit = () => {
    switch (operationsData.operation.operationType) {
      case "Cambiar Categoria":
        return getChangeCategoryForm();

      case "Cambiar Perfil":
        return changeProfileForm();

      case "Cambiar Institucion Financiera":
        return changeFinancialInstitution();

      case "Cambiar Asesor":
        return changeSalesAdvisor();

      case "Cambiar Objeciones":
        return changeObjections();

      default:
        return <div></div>;
    }
  };

  const changeObjections = () => {
    return (
      <>
        <Row>
          <Col>
            <FormGroup>
              <Label htmlFor="email">
                <strong>Objeciones</strong>
              </Label>

              <ListContainer>
                {operationsData.options.map((item, index) => (
                  <div key={index}>
                    <Input
                      value={item}
                      name={item}
                      type="checkbox"
                      checked={isChecked(item) === true}
                      onChange={handleCheck}
                    />
                    <span style={{ marginLeft: 10 }}>{item}</span>
                  </div>
                ))}
              </ListContainer>
            </FormGroup>
          </Col>
        </Row>
      </>
    );
  };

  const changeSalesAdvisor = () => {
    return (
      <>
        <Row>
          <Col>
            <FormGroup>
              <Label>Asesor de Ventas</Label>
              <Input
                type="select"
                name="salesAdvisorId"
                value={operationsData.operation.salesAdvisorId ?? 0}
                onChange={handleNumericInputChange}
                required
              >
                {applicationOptions.salesAdvisors}
              </Input>
            </FormGroup>
          </Col>
        </Row>
      </>
    );
  };

  const changeFinancialInstitution = () => {
    return (
      <>
        <Row>
          <Col>
            <FormGroup>
              <Label>
                <strong>Institucion Financiera</strong>
              </Label>
              <Input
                type="select"
                name="financialInstitutionId"
                value={operationsData.operation.financialInstitutionId ?? 0}
                onChange={handleNumericInputChange}
                required
              >
                {applicationOptions.financialInstitutions}
              </Input>
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col>
            <FormGroup>
              <Label htmlFor="creditOfficer">
                <strong>Oficial de Credito</strong>
              </Label>
              <Input
                type="text"
                name="creditOfficer"
                value={operationsData.operation.creditOfficer ?? ""}
                onChange={handleInputChange}
                required
              />
            </FormGroup>
          </Col>
        </Row>
      </>
    );
  };

  const changeProfileForm = () => {
    return (
      <>
        <Row>
          <Col>
            <FormGroup>
              <Label htmlFor="email">
                <strong>Perfil</strong>
              </Label>
              <Input
                type="select"
                name="profile"
                value={operationsData.operation.profile ?? ""}
                onChange={handleInputChange}
              >
                {applicationOptions.profiles}
              </Input>
            </FormGroup>
          </Col>
        </Row>
      </>
    );
  };

  const getChangeCategoryForm = () => {
    return (
      <>
        <Row>
          <Col>
            <FormGroup>
              <Label htmlFor="email">
                <strong>Categoria de cliente</strong>
              </Label>
              <Input
                type="select"
                name="clientCategoryId"
                value={operationsData.operation.clientCategoryId ?? 0}
                onChange={handleNumericInputChange}
              >
                {applicationOptions.clientCategories}
              </Input>
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col>
            <FormGroup>
              <Label htmlFor="email">
                <strong>Opciones</strong>
              </Label>
              <ListContainer>
                {operationsData.options.map((item, index) => (
                  <div key={index}>
                    <Input
                      //value={item}
                      id={index}
                      type="checkbox"
                      checked={isChecked(item) === true}
                      name={item}
                      onChange={handleCheck}
                    />
                    <span style={{ marginLeft: 10 }}>{item}</span>
                  </div>
                ))}
              </ListContainer>
            </FormGroup>
          </Col>
        </Row>
      </>
    );
  };

  const isChecked = (item) => {
    const check = operationsData.operation.selectedOptions.includes(item)
      ? true
      : false;

    return check;
  };

  return (
    <Container className="animated fadeIn">
      <Modal
        isOpen={operationsData.isOpen}
        toggle={toggle}
        className={"modal-lg"}
      >
        <ModalHeader toggle={toggle}>
          {operationsData.operation.operationType}
        </ModalHeader>
        <ModalBody>
          <Row>
            <Col>
              <FormGroup>
                <Label htmlFor="firstName">
                  <strong>Cliente:</strong>{" "}
                  {operationsData.operation.clientName ?? ""}
                </Label>
              </FormGroup>
            </Col>
          </Row>
          <Body>{getSectionToEdit()}</Body>

          <Row>
            <Col>
              <FormGroup>
                <Label htmlFor="comments">
                  <strong>Comentarios</strong>
                </Label>
                <Input
                  type="textarea"
                  name="comments"
                  value={operationsData.operation.comments ?? ""}
                  onChange={handleInputChange}
                  required
                />
              </FormGroup>
            </Col>
          </Row>

          {operationsData.operation.hasCoDebtop === true && (
            <FormGroup>
              <Row>
                <Col>
                  <FormGroup>
                    <Label htmlFor="comments">
                      <strong>Cliente Tiene Co Deudor</strong>
                    </Label>
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col xs="auto">
                  <ListContainer>
                    <Input
                      id="applyOperationToCoDebtop"
                      type="checkbox"
                      checked={
                        operationsData.operation.applyOperationToCoDebtop ===
                        true
                      }
                      name="applyOperationToCoDebtop"
                      onChange={handleInputChange}
                    />
                  </ListContainer>
                  <span style={{ marginLeft: 30 }}>
                    Aplicar Gestion a CoDeudor también?
                  </span>
                </Col>
              </Row>
            </FormGroup>
          )}
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={() => {
              saveOperation();
            }}
          >
            Guardar
          </Button>
          <Button color="secondary" onClick={onClose}>
            Cancelar
          </Button>
        </ModalFooter>
      </Modal>

      <ToastContainer />
    </Container>
  );
};

export default OperationsModal;
