import React, { useState } from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

const CoDebtorActions = ({ actions }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen((prevState) => !prevState);

  const dropdownItems = actions.map((action) => {
    return (
      <DropdownItem key={action.id} onClick={action.onClick}>
        {action.title}
      </DropdownItem>
    );
  });

  return (
    <Dropdown isOpen={dropdownOpen} toggle={toggle}>
      <DropdownToggle caret>Opciones de Co Deudor</DropdownToggle>
      <DropdownMenu>{dropdownItems}</DropdownMenu>
    </Dropdown>
  );
};

export default CoDebtorActions;
