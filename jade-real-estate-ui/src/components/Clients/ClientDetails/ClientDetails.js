import React, { useState, useEffect } from "react";
import ReactLoading from "react-loading";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import { ToastContainer, toast } from "react-toastify";

import moment from "moment";

import API from "./../../API/API";

import styled from "styled-components";
import ClientForm from "../ClientForm/ClientForm";
import CoDebtorActions from "./CoDebtorActions";
import ClientSearch from "./../ClientSearch/ClientSearch";

const Container = styled.div`
  background-color: coral;
  margin: 0px;
`;

const initialCoDebtor = {
  id: 0,
  clientCode: "",
  creationDate: moment(Date.now()).format("YYYY-MM-DD"),
  projectId: 0,
  project: "",
  contactTypeId: 0,
  contactType: "",
  nationalityId: 0,
  nationality: "",
  clientCategoryId: 0,
  clientCategory: "",
  financialInstitutionId: 0,
  financialInstitution: "",
  salesAdvisorId: 0,
  salesAdvisor: "",
  firstName: "",
  middleName: "",
  firstSurname: "",
  secondSurname: "",
  identificationCard: "",
  phoneNumber: "",
  cellPhoneNumber: "",
  email: "",
  workplace: "",
  workStartDate: null,
  debtorCurrency: "LPS",
  grossMonthlyIncome: 0,
  homeAddress: "",
  profile: "",
  ownHome: false,
  firstHome: false,
  contributeToRap: false,
  comments: "",
  creditOfficer: "",
  supplierServiceId: 0,
  service: "",
  negotiationEnded: false,
};

const ClientDetails = (props) => {
  const {
    clientState,
    setClientState,
    saveChanges,
    cancelChanges,
    apiCallInProgress,
    setApiCallInProgress,
  } = props;

  const [messageValidation, setMessageValidation] = useState("");
  const [coDebtorSelectionIsOpen, setCoDebtorSelectionIsOpen] = useState(false);

  const updatePrincipal = (client) => {
    const updatedState = {
      ...clientState,
      currentClient: {
        ...clientState.currentClient,
        principal: client,
      },
    };

    setClientState(updatedState);
  };

  const updateCoDebtor = (client) => {
    const updatedState = {
      ...clientState,
      currentClient: {
        ...clientState.currentClient,
        coDebtor: client,
      },
    };

    setClientState(updatedState);
  };

  useEffect(() => {
    if (clientState.editMode === "Adding") {
      const salesAdvisor = getSalesAdvisor();

      setClientState({
        ...clientState,

        currentClient: {
          ...clientState.currentClient,
          principal: {
            ...clientState.currentClient.principal,
            salesAdvisorId: salesAdvisor.salesAdvisorId,
            salesAdvisor: salesAdvisor.salesAdvisor,
          },
        },

        state: {
          large: false,
        },
      });
    }
  }, []);

  const getSalesAdvisor = () => {
    const role = sessionStorage.getItem("role");

    if (role === "agente") {
      const id = parseInt(sessionStorage.getItem("id"));

      const selectedSalesAdvisor =
        clientState.applicationOptions.salesAdvisors.find(
          (sa) => sa.props.id === id
        );

      if (selectedSalesAdvisor) {
        return {
          salesAdvisorId: id,
          salesAdvisor: selectedSalesAdvisor.name,
        };
      }
    }

    return {
      salesAdvisorId: 0,
      salesAdvisor: "",
    };
  };

  const trimValue = (value) => {
    return value ? value.trim() : value;
  };

  const capitalize = (s) => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  function trySaveChange(client) {
    setApiCallInProgress(true);

    client.principal.firstName = trimValue(client.principal.firstName);
    client.principal.middleName = trimValue(client.principal.middleName);
    client.principal.firstSurname = trimValue(client.principal.firstSurname);
    client.principal.secondSurname = trimValue(client.principal.secondSurname);
    client.principal.identificationCard = trimValue(
      client.principal.identificationCard
    );

    if (client.coDebtor) {
      client.coDebtor.firstName = trimValue(client.coDebtor.firstName);
      client.coDebtor.middleName = trimValue(client.coDebtor.middleName);
      client.coDebtor.firstSurname = trimValue(client.coDebtor.firstSurname);
      client.coDebtor.secondSurname = trimValue(client.coDebtor.secondSurname);
      client.coDebtor.identificationCard = trimValue(
        client.coDebtor.identificationCard
      );
    }

    client.principal.firstName = capitalize(client.principal.firstName);
    client.principal.middleName = capitalize(client.principal.middleName);
    client.principal.firstSurname = capitalize(client.principal.firstSurname);
    client.principal.secondSurname = capitalize(client.principal.secondSurname);

    if (client.coDebtor) {
      client.coDebtor.firstName = capitalize(client.coDebtor.firstName);
      client.coDebtor.middleName = capitalize(client.coDebtor.middleName);
      client.coDebtor.firstSurname = capitalize(client.coDebtor.firstSurname);
      client.coDebtor.secondSurname = capitalize(client.coDebtor.secondSurname);
    }

    switch (clientState.editMode) {
      case "Adding":
        trySaveChangeNewClient(client);
        break;

      case "Editing":
        saveChanges(client);
        break;

      default:
        break;
    }
  }

  function trySaveChangeNewClient(client) {
    const url = `clients/validate-create-new-client`;
    API.post(url, client)
      .then((res) => {
        if (res.data !== undefined || res.data !== null) {
          if (
            res.data.validationErrorMessage !== null &&
            res.data !== undefined
          ) {
            setMessageValidation(res.data.validationErrorMessage);

            setApiCallInProgress(false);

            if (
              !res.data.validationErrorMessage.includes(
                "Ya existen un cliente con Identificación"
              )
            ) {
              activeValidateNewClient();
            } else {
              toast.warn(res.data.validationErrorMessage);
            }
          } else {
            SaveChangesNewClient();
          }
        }
      })
      .catch((error) => {
        setApiCallInProgress(false);
      });
  }

  function SaveChangesNewClient() {
    const client = {
      ...clientState.currentClient,
      clientCreationComments: messageValidation,
    };

    saveChanges(client);
  }

  const activeValidateNewClient = () => {
    setClientState({
      ...clientState,
      state: {
        large: !clientState.state.large,
      },
    });
  };

  function getFooterBuild(e) {
    if (apiCallInProgress) {
      return (
        <CardFooter>
          <Col md="3" sm="6" xs="12"></Col>
          <Col md="3" sm="6" xs="12">
            <ReactLoading
              type="bars"
              color="#5DBCD2"
              height={"30%"}
              width={"30%"}
            />
          </Col>
          <Col md="3" sm="6" xs="12"></Col>
        </CardFooter>
      );
    } else {
      return (
        <CardFooter>
          <Button type="reset" size="sm" color="danger" onClick={cancelChanges}>
            <i className="fa fa-ban" /> Cancelar
          </Button>
          <Button
            type="submit"
            size="sm"
            color="success"
            disabled={apiCallInProgress}
            onClick={() => {
              trySaveChange(clientState.currentClient);
            }}
          >
            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
        </CardFooter>
      );
    }
  }

  const createNewCoDebtor = () => {
    const updatedState = {
      ...clientState,
      currentClient: {
        ...clientState.currentClient,
        coDebtor: initialCoDebtor,
        coDebtorAssignmentType: "create-new-codebtor",
      },
    };

    setClientState(updatedState);
  };

  //title, isOpen, toggle, handleOnSelectedClient

  const toggleCoDebtorSelectionIsOpen = () => {
    setCoDebtorSelectionIsOpen(!coDebtorSelectionIsOpen);
  };

  const handleOnSelectedCoDebtor = (client) => {
    const updatedState = {
      ...clientState,
      currentClient: {
        ...clientState.currentClient,
        coDebtor: client,
        coDebtorAssignmentType: "assign-codebtor",
      },
    };

    setClientState(updatedState);
  };

  const assignCoDebtor = () => {
    setCoDebtorSelectionIsOpen(true);
  };

  const removeCoDebtor = () => {
    const updatedState = {
      ...clientState,
      currentClient: {
        ...clientState.currentClient,
        coDebtor: null,
        coDebtorAssignmentType: "remove-codebtor",
      },
    };

    setClientState(updatedState);
  };

  const actions = [
    {
      id: "1",
      title: "Nuevo Co-Deudor",
      onClick: createNewCoDebtor,
    },
    {
      id: "2",
      title: "Asignar Co-Deudor",
      onClick: assignCoDebtor,
    },
    {
      id: "3",
      title: "Remover Co-Deudor",
      onClick: removeCoDebtor,
    },
  ];

  return (
    <Container className="animated fadeIn">
      <Card>
        <CardHeader>
          <strong>Clientes</strong>
          <small> Informacion del Cliente</small>
        </CardHeader>
        <CardBody>
          <ClientForm
            title="Cliente Principal"
            client={clientState.currentClient.principal}
            updateClient={updatePrincipal}
            applicationOptions={clientState.applicationOptions}
            showGeneralInformation={true}
            showFinancialInformation={true}
            editMode={clientState.editMode}
          ></ClientForm>

          <CoDebtorActions actions={actions}></CoDebtorActions>

          {clientState.currentClient.coDebtor && (
            <ClientForm
              title="Co Deudor"
              client={clientState.currentClient.coDebtor}
              updateClient={updateCoDebtor}
              applicationOptions={clientState.applicationOptions}
              showGeneralInformation={true}
              showFinancialInformation={true}
            ></ClientForm>
          )}
        </CardBody>
        {getFooterBuild()}
      </Card>
      <Modal
        isOpen={clientState.state.large}
        toggle={activeValidateNewClient}
        className={"modal-lg"}
      >
        <ModalHeader toggle={activeValidateNewClient}>
          Se identificaron coincidencias
        </ModalHeader>
        <ModalBody>
          <div>
            <h3>Se encontro un cliente registrado con los mismos datos: </h3>

            <p>
              <strong>{messageValidation}</strong>
            </p>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={() => {
              SaveChangesNewClient();
            }}
          >
            Crear de todas formas
          </Button>
          <Button color="danger" onClick={activeValidateNewClient}>
            Cancelar ingreso
          </Button>
        </ModalFooter>
      </Modal>

      {coDebtorSelectionIsOpen && (
        <ClientSearch
          title="Seleccion de Co Deudor"
          isOpen={coDebtorSelectionIsOpen}
          toggle={toggleCoDebtorSelectionIsOpen}
          handleOnSelectedClient={handleOnSelectedCoDebtor}
        />
      )}
      <ToastContainer />
    </Container>
  );
};

export default ClientDetails;
