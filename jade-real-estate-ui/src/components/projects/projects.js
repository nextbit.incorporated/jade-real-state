import React from "react";

import { Card, CardBody, CardHeader, Col, Row, Table } from "reactstrap";

import UseFetch from "./../UseFetch/UseFetch";

const projects = () => {
  const { loading, data } = UseFetch("https://localhost:5001/api/projects");

  const thStyle = {
    background: "#20a8d8",
    color: "white"
  };

  const rowHeigth = {
    height: "20px"
  };

  const projectRows = loading ? (
    <tr>
      <td colSpan={3}>No projects</td>
    </tr>
  ) : (
    data.map(project => (
      <tr key={project.id} style={rowHeigth}>
        <td>{project.name}</td>
        <td>{project.country}</td>
        <td>{project.state}</td>
        <td>{project.city}</td>
        <td>{project.address}</td>
      </tr>
    ))
  );

  return (
    <div className="animated fadeIn">
      <div />

      <Row>
        <Col>
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify" /> Proyectos
            </CardHeader>
            <CardBody>
              <Table hover bordered striped responsive size="sm">
                <thead>
                  <tr>
                    <th className="align-items-center" style={thStyle}>
                      Proyecto
                    </th>
                    <th style={thStyle}>País</th>
                    <th style={thStyle}>Departamento</th>
                    <th style={thStyle}>Ciudad</th>
                    <th style={thStyle}>Dirección</th>
                  </tr>
                </thead>
                <tbody>{projectRows}</tbody>
              </Table>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default projects;
