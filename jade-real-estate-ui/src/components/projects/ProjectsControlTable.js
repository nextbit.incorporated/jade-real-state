import React, { useState } from "react";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
} from "reactstrap";

const step = 1;

const ProjectsControlTable = (props) => {
  const {
    setQueryInfo,
    projects,
    nextPage,
    prevPage,
    editRow,
    deleteProject,
  } = props;
  const [value, setValue] = useState("");

  function nextStep() {
    nextPage(step + 1);
  }

  function prevStep() {
    prevPage(step - 1);
  }

  function handleValueChange(e) {
    setQueryInfo({ query: e.target.value, step: 0 });
    setValue(e.target.value);
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const userRows =
    projects === null ? (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>Buscando...</td>
          </tr>
        </thead>
      </Table>
    ) : projects.length === 0 ? (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>
              No se encontraron usuarios con el filtro especificado.
            </td>
          </tr>
        </thead>
      </Table>
    ) : (
      <div>
        <FormGroup row>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th style={thStyle}>Editar</th>
                  <th style={thStyle}>Borrar</th>
                  <th style={thStyle}>ID</th>
                  <th style={thStyle}>Nombre</th>
                  <th style={thStyle}>Pais</th>
                  <th style={thStyle}>Departamento</th>
                  <th style={thStyle}>Ciudad</th>
                  <th style={thStyle}>Direccion</th>
                </tr>
              </thead>
              <tbody>
                {projects.projects.map((project) => (
                  <tr key={project.id}>
                    <td>
                      <Button
                        block
                        color="warning"
                        onClick={() => {
                          editRow(project);
                        }}
                      >
                        Editar
                      </Button>
                    </td>
                    <td>
                      <Button
                        block
                        color="danger"
                        onClick={() => deleteProject(project.id)}
                      >
                        Borrar
                      </Button>
                    </td>
                    <td>{project.id}</td>
                    <td>{project.name}</td>
                    <td>{project.country}</td>
                    <td>{project.state}</td>
                    <td>{project.city}</td>
                    <td>{project.address}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStep}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {projects.pageCount}</strong>
                </h5>
              </div>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Actual: {projects.pageIndex}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStep}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      </div>
    );
  return userRows;
};

export default ProjectsControlTable;
