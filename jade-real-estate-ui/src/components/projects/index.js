import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
} from "reactstrap";

import ProjectsControlTable from "./ProjectsControlTable";
import ProjectDetails from "./projectDetail";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";
const heightStyle = {
  height: "500px",
};

const initialClientState = {
  step: 1,
  editMode: "None",
  currentProject: {
    id: "",
    name: "",
    country: "",
    state: "",
    city: "",
    address: "",
  },
};

const Project = (props) => {
  const [projectState, setProjectState] = useState(initialClientState);
  const [projects, setProjects] = useState([]);
  const [queryInfo, setQueryInfo] = useState({ query: null, step: null });

  useEffect(() => {
    const index =
      queryInfo.step === undefined || queryInfo.step === null
        ? 0
        : queryInfo.step;
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard"></Redirect>;
    }
    if (queryInfo.query === null) {
      var url = `projects/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setProjects(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `projects/paged?pageSize=10&pageIndex=${index}&value=${queryInfo.query}`;
      API.get(url1).then((res) => {
        setProjects(res.data);
      });
    }
  }, [queryInfo.query, queryInfo.step, props.history]);


  function getRegisters(queryInfo)
  {
    const index =
      queryInfo.step === undefined || queryInfo.step === null
        ? 0
        : queryInfo.step;
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard"></Redirect>;
    }
    if (queryInfo.query === null) {
      var url = `projects/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setProjects(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `projects/paged?pageSize=10&pageIndex=${index}&value=${queryInfo.query}`;
      API.get(url1).then((res) => {
        setProjects(res.data);
      });
    }
  }

  const editRow = (project) => {
    setProjectState({
      ...projectState,
      editMode: "Editing",
      currentProject: project,
      step: 2,
    });
  };

  function nextPage(step) {
    setQueryInfo({ query: null, step: step });
  }

  function prevPage(step) {
    setQueryInfo({ query: null, step: step });
  }

  function nextStep(project) {
    const { step } = projectState;

    if (project === null || project === undefined) {
      project = projectState.currentProject;
    }
    setProjectState({
      ...projectState,
      currentProject: project,
      step: step + 1,
    });
  }

  function prevStep() {
    const { step } = projectState;

    setProjectState({ ...projectState, step: step - 1 });
  }

  function addProject(newProject) {
    if (
      !newProject ||
      !newProject.name ||
      !newProject.country ||
      !newProject.city
    ) {
      return;
    }
    const url = `projects`;

    API.post(url, newProject).then((res) => {
      if (res.id === 0) {
        toast("Error al intentar agregar usuario");
      } else {
        toast("proyecto agregado satisfactoriamente");
        setQueryInfo({ query: null, step: 0 });
        setProjects([]);
        getRegisters(queryInfo);
        setProjectState(initialClientState);
      }
    });
  }

  function updateProject(project) {
    if (!project || !project.name || !project.country || !project.city) {
      return;
    }
    const url = `projects`;
    API.put(url, project)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar usuario");
        } else {
          toast("proyecto agregado satisfactoriamente");

          setQueryInfo({ query: null, step: 0 });
          setProjects([]);
          getRegisters(queryInfo);
          setProjectState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  const deleteProject = (id) => {
    const url = `Projects?Id=${id}`;

    API.delete(url)
      .then((res) => {
        if (res.data !== "") {
          toast("error al intentar eliminar usuario");
          return;
        }
        setQueryInfo({ query: null, step: 0 });
        setProjects([]);
        getRegisters(queryInfo);
        setProjectState(initialClientState);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para crear el nuevo cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col />
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStep()}
                >
                  <i className="fa fa-new" /> Agregar Proyecto
                </Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <ProjectsControlTable
                  setQueryInfo={setQueryInfo}
                  projects={projects}
                  nextPage={nextPage}
                  prevPage={prevPage}
                  editRow={editRow}
                  deleteProject={deleteProject}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <ProjectDetails
            currentState={projectState}
            setProjectState={setProjectState}
            nextStep={nextStep}
            prevStep={prevStep}
            addProject={addProject}
            updateProject={updateProject}
          />
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(projectState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Proyectos
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default Project;
