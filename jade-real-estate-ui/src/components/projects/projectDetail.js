import React from "react";

import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";

const ProjectDetails = (props) => {
  const { currentState, setProjectState, addProject, updateProject } = props;

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setProjectState({
      ...currentState,
      currentProject: {
        ...currentState.currentProject,
        [name]: value,
      },
    });
  };

  const handleCleanValues = () => {
    const initialClientState = {
      step: 1,
      editMode: "None",
      currentProject: {
        id: "",
        name: "",
        country: "",
        state: "",
        city: "",
        address: "",
      },
    };
    setProjectState(initialClientState);
  };

  const handleSaveChanges = () => {
    switch (currentState.editMode) {
      case "Editing": {
        const project = {
          name: currentState.currentProject.name,
          country: currentState.currentProject.country,
          state: currentState.currentProject.state,
          city: currentState.currentProject.city,
          address: currentState.currentProject.address,
        };
        updateProject(project);
        break;
      }

      default: {
        const newProject = {
          name: currentState.currentProject.name,
          country: currentState.currentProject.country,
          state: currentState.currentProject.state,
          city: currentState.currentProject.city,
          address: currentState.currentProject.address,
        };
        addProject(newProject);
        break;
      }
    }
  };

  switch (currentState.editMode) {
    case "Editing":
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos a editar de proyecto </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="name"
                      id="name"
                      readonly
                      value={currentState.currentProject.name}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Pais</Label>
                    <Input
                      type="text"
                      id="country"
                      name="country"
                      value={currentState.currentProject.country}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Segundo Nombre">Departamento</Label>
                    <Input
                      type="text"
                      id="state"
                      name="state"
                      value={currentState.currentProject.state}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName">Ciudad</Label>
                    <Input
                      type="text"
                      id="city"
                      name="city"
                      value={currentState.currentProject.city}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="identification">Direccion</Label>
                    <Input
                      type="text"
                      id="address"
                      name="address"
                      value={currentState.currentProject.address}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
    default:
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos de nuevo proyecto </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="name"
                      id="name"
                      readonly
                      value={currentState.currentProject.name}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Pais</Label>
                    <Input
                      type="text"
                      id="country"
                      name="country"
                      value={currentState.currentProject.country}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Segundo Nombre">Departamento</Label>
                    <Input
                      type="text"
                      id="state"
                      name="state"
                      value={currentState.currentProject.state}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName">Ciudad</Label>
                    <Input
                      type="text"
                      id="city"
                      name="city"
                      value={currentState.currentProject.city}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="identification">Direccion</Label>
                    <Input
                      type="text"
                      id="address"
                      name="address"
                      value={currentState.currentProject.address}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
  }
};

export default ProjectDetails;
