import React, { useState } from "react";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
} from "reactstrap";

const step = 1;

const NationalityControlTable = (props) => {
  const {
    setQueryInfo,
    //fetchNationalities,
    nationality,
    //nextPage,
    //prevPage,
    editRow,
    deleteRegister,
  } = props;
  const [value, setValue] = useState("");

  function nextStep() {
    setQueryInfo({ query: null, step: step + 1 });
    //nextPage(step + 1);
  }

  function prevStep() {
    setQueryInfo({ query: null, step: step - 1 });
    //prevPage(step - 1);
  }

  function handleValueChange(e) {
    setQueryInfo({ query: e.target.value, step: 0 });
    //fetchNationalities(e.target.value);
    setValue(e.target.value);
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const userRows =
    nationality === null ? (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>Buscando...</td>
          </tr>
        </thead>
      </Table>
    ) : nationality.length === 0 ? (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>
              No se encontraron usuarios con el filtro especificado.
            </td>
          </tr>
        </thead>
      </Table>
    ) : (
      <div>
        <FormGroup row>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th style={thStyle}>Editar</th>
                  <th style={thStyle}>Borrar</th>
                  <th style={thStyle}>ID</th>
                  <th style={thStyle}>Nombre</th>
                  <th style={thStyle}>Descripcion</th>
                </tr>
              </thead>
              <tbody>
                {nationality.origins.map((origin) => (
                  <tr key={origin.id}>
                    <td>
                      <Button
                        block
                        color="warning"
                        onClick={() => {
                          editRow(origin);
                        }}
                      >
                        Editar
                      </Button>
                    </td>
                    <td>
                      <Button
                        block
                        color="danger"
                        onClick={() => deleteRegister(origin.id)}
                      >
                        Borrar
                      </Button>
                    </td>
                    <td>{origin.id}</td>
                    <td>{origin.name}</td>
                    <td>{origin.description}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStep}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {nationality.pageCount}</strong>
                </h5>
              </div>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Actual: {nationality.pageIndex}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStep}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      </div>
    );
  return userRows;
};

export default NationalityControlTable;
