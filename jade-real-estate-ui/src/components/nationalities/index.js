import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
} from "reactstrap";

import NationalityControlTable from "./nationalitiesControlTable";
import ProjectDetails from "./nationalitiesDetails";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";
const heightStyle = {
  height: "500px",
};

const initialClientState = {
  step: 1,
  editMode: "None",
  currentNationality: {
    id: "",
    name: "",
    description: "",
  },
};

const initialQueryInfo = {
  query: null,
  step: 0,
};

const Nationalities = (props) => {
  const [nationalityState, setNationalityState] = useState(initialClientState);
  const [nationality, setNationality] = useState([]);
  const [queryInfo, setQueryInfo] = useState(initialQueryInfo);

  useEffect(() => {
    const query = queryInfo ? queryInfo.query : null;
    const step = queryInfo ? queryInfo.step : 0;

    const index = step === undefined || step === null ? 0 : step;
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard"></Redirect>;
    }
    if (query === null) {
      var url = `origin/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setNationality(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `origin/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setNationality(res.data);
      });
    }
  }, [queryInfo, props.history]);

  const editRow = (nationality) => {
    setNationalityState({
      ...nationalityState,
      editMode: "Editing",
      currentNationality: nationality,
      step: 2,
    });
  };

  function nextStep(supplier) {
    const { step } = nationalityState;

    if (supplier === null || supplier === undefined) {
      supplier = nationalityState.currentNationality;
    }
    setNationalityState({
      ...nationalityState,
      currentNationality: supplier,
      step: step + 1,
    });
  }

  function prevStep() {
    const { step } = nationalityState;

    setNationalityState({ ...nationalityState, step: step - 1 });
  }

  function add(newNationality) {
    if (!newNationality || !newNationality.name) {
      return;
    }
    const url = `origin`;

    API.post(url, newNationality)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar nueva nacionalidad");
        } else {
          toast("Nacionalidad agregado satisfactoriamente");
          setQueryInfo(initialQueryInfo);
          setNationalityState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  function update(supplierService) {
    if (!supplierService || !supplierService.name) {
      return;
    }
    const url = `origin`;
    API.put(url, supplierService)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar actualizar nacionalidad");
        } else {
          toast("Nacionalidad actualizada satisfactoriamente");

          setQueryInfo(initialQueryInfo);
          setNationalityState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  const deleteRegister = (id) => {
    const url = `origin?Id=${id}`;

    API.delete(url)
      .then((res) => {
        if (res.data !== "") {
          toast("error al intentar eliminar nacionalidad");
          return;
        }
        setQueryInfo(initialQueryInfo);
        setNationalityState(initialClientState);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col />
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStep()}
                >
                  <i className="fa fa-new" /> Agregar nueva nacionalidad
                </Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <NationalityControlTable
                  setQueryInfo={setQueryInfo}
                  nationality={nationality}
                  editRow={editRow}
                  deleteRegister={deleteRegister}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <ProjectDetails
            currentState={nationalityState}
            setNationalityState={setNationalityState}
            nextStep={nextStep}
            prevStep={prevStep}
            add={add}
            update={update}
          />
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(nationalityState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Nacionalidades
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default Nationalities;
