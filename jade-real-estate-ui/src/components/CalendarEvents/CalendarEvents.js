import React, { useEffect, useState } from "react";

import CalendarEventsTable from "./CalendarEventsTable/CalendarEventsTable";
import {
  getCalendarEvents,
  deleteCalendarEvent,
} from "./../Helpers/GoogleCalendar";

const CalendarEvents = () => {
  const [events, setEvents] = useState([]);

  useEffect(() => {
    getEvents();
  }, []);

  const getEvents = async () => {
    getCalendarEvents(processEvents);
  };

  const processEvents = (calendarEvents) => {
    setEvents(calendarEvents);
  };

  const deleteEvent = (id) => {
    deleteCalendarEvent(id, processEvents);
  };

  return (
    <div>
      <CalendarEventsTable
        events={events}
        deleteEvent={deleteEvent}
      ></CalendarEventsTable>
    </div>
  );
};

export default CalendarEvents;
