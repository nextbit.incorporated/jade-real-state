import React, { useState } from "react";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
  NavLink,
} from "reactstrap";

const thStyle = {
  background: "#20a8d8",
  color: "white",
};

const CalendarEventsTable = ({ events, deleteEvent }) => {
  return (
    <div>
      <FormGroup row>
        <Col md="12">Eventos Calendario Google</Col>
      </FormGroup>

      <FormGroup row>
        <Col md="12">
          <Table hover bordered striped responsive size="sm">
            <thead>
              <tr>
                <th style={thStyle}>Borrar</th>
                <th style={thStyle}>Titulo</th>
                <th style={thStyle}>Sumario</th>
                <th style={thStyle}>Fecha</th>
                <th style={thStyle}>Link</th>
              </tr>
            </thead>
            <tbody>
              {events.map((event) => (
                <tr key={event.id}>
                  <td>
                    <Button
                      block
                      color="danger"
                      onClick={() => deleteEvent(event.id)}
                    >
                      Borrar
                    </Button>
                  </td>
                  <td>{event.location}</td>
                  <td>{event.summary}</td>
                  <td>
                    {event.start.dateTime
                      ? event.start.dateTime
                      : event.start.date}
                  </td>
                  <td>
                    {/* <NavLink href={event.htmlLink}>{event.location}</NavLink> */}
                    <a target="_blank" href={event.htmlLink}>
                      {event.location
                        ? "Ver evento en Calendario de Google"
                        : ""}
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </FormGroup>
    </div>
  );
};

export default CalendarEventsTable;
