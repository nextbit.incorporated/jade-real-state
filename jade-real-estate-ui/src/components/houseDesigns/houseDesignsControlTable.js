import React, { useState } from "react";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
} from "reactstrap";

const step = 1;

const HouseDesignsControlTable = (props) => {
  const { fetchModels, models, nextPage, prevPage, editRow, deleteRow } = props;
  const [value, setValue] = useState("");

  function nextStep() {
    nextPage(step + 1);
  }

  function prevStep() {
    prevPage(step - 1);
  }

  function handleValueChange(e) {
    fetchModels(e.target.value);
    setValue(e.target.value);
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const userRows =
    models === null ? (
      <table>
        <tbody>
          <tr>
            <td colSpan={16}>Buscando...</td>
          </tr>
        </tbody>
      </table>
    ) : models.length === 0 ? (
      <table>
        <tbody>
          <tr>
            <td colSpan={16}>No se encontraron modelos de vivienda.</td>
          </tr>
        </tbody>
      </table>
    ) : (
      <div>
        <FormGroup row>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th style={thStyle}>Editar</th>
                  <th style={thStyle}>Borrar</th>
                  <th style={thStyle}>ID</th>
                  <th style={thStyle}>Nombre</th>
                  <th style={thStyle}>Descripcion</th>
                  <th style={thStyle}>Especificaciones</th>
                  <th style={thStyle}>Proyecto</th>
                </tr>
              </thead>
              <tbody>
                {models.designs.map((model) => (
                  <tr key={model.id}>
                    <td>
                      <Button
                        block
                        color="warning"
                        onClick={() => {
                          editRow(model);
                        }}
                      >
                        Editar
                      </Button>
                    </td>
                    <td>
                      <Button
                        block
                        color="danger"
                        onClick={() => deleteRow(model.id)}
                      >
                        Borrar
                      </Button>
                    </td>
                    <td>{model.id}</td>
                    <td>{model.name}</td>
                    <td>{model.description}</td>
                    <td>{model.houseSpecifications}</td>
                    <td>{model.projectName}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStep}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {models.pageCount}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStep}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      </div>
    );
  return userRows;
};

export default HouseDesignsControlTable;
