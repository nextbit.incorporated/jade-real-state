import React, { useState, useEffect } from "react";

import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Table,
  FormGroup,
  Input,
  Label,
  ModalFooter,
  ModalHeader,
  Modal,
  ModalBody,
  InputGroup,
  InputGroupAddon,
  Row,
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";
import checkmark from "../../../src/assets/img/brand/checkmark.svg";

const step = 1;

const HouseDesignsDetail = (props) => {
  const {
    currentState,
    setModelState,
    addModel,
    updateModel,
    className,
  } = props;
  const [value, setValue] = useState("");
  const [projectSelected, setprojectSelected] = useState([]);
  const [projects, setProjects] = useState([]);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setModelState({
      ...currentState,
      currentModel: {
        ...currentState.currentModel,
        [name]: value,
      },
    });
  };

  useEffect(() => {
    if (currentState.currentModel && currentState.currentModel.projectId) {
      fetchProjects(currentState.currentModel.projectName, 0);
    }
  }, []);

  function fetchProjects(query, page) {
    const index = page === undefined || page === null ? 0 : page;
    if (query === null) {
      var url = `projects/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setProjects(res.data);
        })
        .catch((error) => {
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `projects/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setProjects(res.data);
        if (currentState.editMode === "Editing") {
          const ps = res.data.projects.find(
            (x) => x.id === currentState.currentModel.projectId
          );
          setprojectSelected(ps);
        }
      });
    }
  }

  function nextStep() {
    nextPage(step + 1);
  }

  function prevStep() {
    prevPage(step - 1);
  }

  function nextPage(step) {
    fetchProjects(null, step);
  }

  function prevPage(step) {
    fetchProjects(null, step);
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const handleCleanValues = () => {
    const initialClientState = {
      step: 1,
      editMode: "None",
      currentModel: {
        id: "",
        projectId: 0,
        name: "",
        description: "",
        houseSpecifications: "",
        status: "",
        projectName: "",
      },
      state: {
        modal: false,
        large: false,
        small: false,
        primary: false,
        success: false,
        warning: false,
        danger: false,
        info: false,
      },
    };
    setModelState(initialClientState);
  };

  const handleSaveChanges = () => {
    switch (currentState.editMode) {
      case "Editing": {
        const model = {
          id: currentState.currentModel.id,
          name: currentState.currentModel.name,
          description: currentState.currentModel.description,
          houseSpecifications: currentState.currentModel.houseSpecifications,
          projectId: projectSelected.id,
          status: currentState.currentModel.status,
        };
        updateModel(model);
        break;
      }

      default: {
        const newModel = {
          name: currentState.currentModel.name,
          description: currentState.currentModel.description,
          houseSpecifications: currentState.currentModel.houseSpecifications,
          projectId: projectSelected.id,
          status: currentState.currentModel.status,
        };
        addModel(newModel);
        break;
      }
    }
  };

  const togglePrimary = () => {
    setModelState({
      ...currentState,
      state: {
        large: !currentState.state.large,
      },
    });
    fetchProjects(null, 0);
  };

  function handleValueChange(e) {
    fetchProjects(e.target.value);
    setValue(e.target.value);
  }

  const selectProject = (project) => {
    setprojectSelected(project);
    setModelState({
      ...currentState,
      state: {
        large: !currentState.state.large,
      },
    });
  };

  switch (currentState.editMode) {
    case "Editing":
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos a editar modelo de vivienda </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="name"
                      id="name"
                      value={currentState.currentModel.name}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                    <Input
                      type="text"
                      id="description"
                      name="description"
                      value={currentState.currentModel.description}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Segundo Nombre">Especificaciones</Label>
                    <Input
                      type="text"
                      id="houseSpecifications"
                      name="houseSpecifications"
                      value={currentState.currentModel.houseSpecifications}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName">Proyecto</Label>
                    <Input
                      type="text"
                      id="contactNumber"
                      name="contactNumber"
                      value={projectSelected.name}
                      //onChange={handleInputChange}
                      //readOnly
                      required
                    />
                  </FormGroup>
                </Col>
                <Col>
                  <Row>
                    <Label htmlFor="Segundo Nombre"> Lista Proyectos </Label>
                  </Row>
                  <Row>
                    <Button color="primary" onClick={togglePrimary}>
                      Seleccionar Proyecto
                    </Button>
                    <Modal
                      isOpen={currentState.state.large}
                      toggle={togglePrimary}
                      className={"modal-lg" + className}
                    >
                      <ModalHeader toggle={togglePrimary}>
                        Seleccionar Proyecto
                      </ModalHeader>
                      <ModalBody>
                        <FormGroup row>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button type="button" color="primary">
                                  <i className="fa fa-search" /> Buscar
                                </Button>
                              </InputGroupAddon>
                              <Input
                                type="text"
                                id="input1-group2"
                                name="input1-group2"
                                placeholder="Buscar"
                                value={value}
                                onChange={handleValueChange}
                              />
                            </InputGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="12">
                            {projects === null ? (
                              <tr>
                                <td colSpan={16}>Buscando...</td>
                              </tr>
                            ) : projects.length === 0 ? (
                              <tr>
                                <td colSpan={16}>
                                  No se encontraron proyectos.
                                </td>
                              </tr>
                            ) : (
                              <Table
                                hover
                                bordered
                                striped
                                responsive
                                size="sm"
                              >
                                <thead>
                                  <tr>
                                    <th style={thStyle}>Seleccionar</th>
                                    <th style={thStyle}>Nombre</th>
                                    <th style={thStyle}>Pais</th>
                                    <th style={thStyle}>Departamento</th>
                                    <th style={thStyle}>Ciudad</th>
                                    <th style={thStyle}>Direccion</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {projects.projects.map((project) => (
                                    <tr key={project.id}>
                                      <td>
                                        <Button
                                          onClick={() => {
                                            selectProject(project);
                                          }}
                                        >
                                          <img
                                            src={checkmark}
                                            height="100%"
                                            width="100%"
                                            alt="checkmark"
                                          />
                                        </Button>
                                      </td>
                                      <td>{project.name}</td>
                                      <td>{project.country}</td>
                                      <td>{project.state}</td>
                                      <td>{project.city}</td>
                                      <td>{project.address}</td>
                                    </tr>
                                  ))}
                                </tbody>
                              </Table>
                            )}
                          </Col>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={prevStep}
                                >
                                  <i className="icon-arrow-left-circle" />{" "}
                                  Anterior
                                </Button>
                              </InputGroupAddon>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Paginas: {projects.pageCount}</strong>
                                </h5>
                              </div>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Actual: {projects.pageIndex}</strong>
                                </h5>
                              </div>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={nextStep}
                                >
                                  Proximo{" "}
                                  <i className="icon-arrow-right-circle" />
                                </Button>
                              </InputGroupAddon>
                            </InputGroup>
                          </Col>
                        </FormGroup>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={togglePrimary}>
                          Cancel
                        </Button>
                      </ModalFooter>
                    </Modal>
                  </Row>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
    default:
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos de nuevo constructor </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="name"
                      id="name"
                      readonly
                      value={currentState.currentModel.name}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                    <Input
                      type="text"
                      id="description"
                      name="description"
                      value={currentState.currentModel.description}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Segundo Nombre">Especificaciones</Label>
                    <Input
                      type="text"
                      id="houseSpecifications"
                      name="houseSpecifications"
                      value={currentState.currentModel.houseSpecifications}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName">Proyecto</Label>
                    <Input
                      type="text"
                      id="projectId"
                      name="projectId"
                      value={projectSelected.name}
                      // onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col>
                  <Row>
                    <Label htmlFor="Segundo Nombre"> Lista Proyectos </Label>
                  </Row>
                  <Row>
                    <Button color="primary" onClick={togglePrimary}>
                      Seleccionar Proyecto
                    </Button>
                    <Modal
                      isOpen={currentState.state.large}
                      toggle={togglePrimary}
                      className={"modal-lg" + className}
                    >
                      <ModalHeader toggle={togglePrimary}>
                        Modal title
                      </ModalHeader>
                      <ModalBody>
                        <FormGroup row>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button type="button" color="primary">
                                  <i className="fa fa-search" /> Buscar
                                </Button>
                              </InputGroupAddon>
                              <Input
                                type="text"
                                id="input1-group2"
                                name="input1-group2"
                                placeholder="Buscar"
                                value={value}
                                onChange={handleValueChange}
                              />
                            </InputGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="12">
                            {projects === null ? (
                              <tr>
                                <td colSpan={16}>Buscando...</td>
                              </tr>
                            ) : projects.length === 0 ? (
                              <tr>
                                <td colSpan={16}>
                                  No se encontraron proyectos.
                                </td>
                              </tr>
                            ) : (
                              <Table
                                hover
                                bordered
                                striped
                                responsive
                                size="sm"
                              >
                                <thead>
                                  <tr>
                                    <th style={thStyle}>Seleccionar</th>
                                    <th style={thStyle}>Nombre</th>
                                    <th style={thStyle}>Pais</th>
                                    <th style={thStyle}>Departamento</th>
                                    <th style={thStyle}>Ciudad</th>
                                    <th style={thStyle}>Direccion</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {projects.projects.map((project) => (
                                    <tr key={project.id}>
                                      <td>
                                        <Button
                                          onClick={() => {
                                            selectProject(project);
                                          }}
                                        >
                                          <img
                                            src={checkmark}
                                            height="100%"
                                            width="100%"
                                            alt="checkmark"
                                          />
                                        </Button>
                                      </td>
                                      <td>{project.name}</td>
                                      <td>{project.country}</td>
                                      <td>{project.state}</td>
                                      <td>{project.city}</td>
                                      <td>{project.address}</td>
                                    </tr>
                                  ))}
                                </tbody>
                              </Table>
                            )}
                          </Col>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={prevStep}
                                >
                                  <i className="icon-arrow-left-circle" />{" "}
                                  Anterior
                                </Button>
                              </InputGroupAddon>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Paginas: {projects.pageCount}</strong>
                                </h5>
                              </div>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Actual: {projects.pageIndex}</strong>
                                </h5>
                              </div>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={nextStep}
                                >
                                  Proximo{" "}
                                  <i className="icon-arrow-right-circle" />
                                </Button>
                              </InputGroupAddon>
                            </InputGroup>
                          </Col>
                        </FormGroup>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={togglePrimary}>
                          Cancel
                        </Button>
                      </ModalFooter>
                    </Modal>
                  </Row>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
  }
};
export default HouseDesignsDetail;
