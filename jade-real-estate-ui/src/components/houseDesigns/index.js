import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
} from "reactstrap";

import HouseDesignsControlTable from "./houseDesignsControlTable";
import HouseDesignsDetail from "./houseDesignsDetail";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";
const heightStyle = {
  height: "500px",
};

const initialClientState = {
  step: 1,
  editMode: "None",
  currentModel: {
    id: "",
    projectId: 0,
    name: "",
    description: "",
    houseSpecifications: "",
    status: "",
    projectName: "",
  },
  state: {
    modal: false,
    large: false,
    small: false,
    primary: false,
    success: false,
    warning: false,
    danger: false,
    info: false,
  },
};

const HouseDesign = (props) => {
  const [modelState, setModelState] = useState(initialClientState);
  const [models, setModels] = useState([]);

  function fetchModels(query, step) {
    const index = step === undefined || step === null ? 0 : step;
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard"></Redirect>;
    }
    if (query === null || query === undefined) {
      var url = `houseDesign/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setModels(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `houseDesign/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setModels(res.data);
      });
    }
  }

  const editRow = (model) => {
    setModelState({
      ...modelState,
      editMode: "Editing",
      currentModel: model,
      step: 2,
    });
  };

  function nextPage(step) {
    fetchModels(null, step);
  }

  function prevPage(step) {
    fetchModels(null, step);
  }

  function nextStep(model) {
    const { step } = modelState;

    if (model === null || model === undefined) {
      model = modelState.currentModel;
    }
    setModelState({ ...modelState, currentModel: model, step: step + 1 });
  }

  function prevStep() {
    const { step } = modelState;

    setModelState({ ...modelState, step: step - 1 });
  }

  useEffect(() => {
    fetchModels(null);
  }, []);

  function addModel(newModel) {
    if (!newModel || !newModel.name || newModel.projectId === undefined) {
      return;
    }
    const url = `houseDesign`;

    API.post(url, newModel).then((res) => {
      if (res.id === 0) {
        toast("Error al intentar agregar usuario");
      } else {
        toast("proyecto agregado satisfactoriamente");
        fetchModels(null);
        setModelState(initialClientState);
      }
    });
  }

  function updateModel(model) {
    if (
      !model ||
      !model.name ||
      model.projectId === 0 ||
      model.projectId === undefined
    ) {
      return;
    }
    const url = `houseDesign`;
    API.put(url, model)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar usuario");
        } else {
          toast("proyecto agregado satisfactoriamente");

          // setUsers([...users.users, userAdded]);
          fetchModels(null);
          setModelState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  const deleteRow = (id) => {
    const url = `houseDesign?Id=${id}`;

    API.delete(url)
      .then((res) => {
        if (res.data !== "") {
          toast("error al intentar eliminar usuario");
          return;
        }
        fetchModels(null);
        setModelState(initialClientState);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col />
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStep()}
                >
                  <i className="fa fa-new" /> Agregar modelo
                </Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <HouseDesignsControlTable
                  fetchModels={fetchModels}
                  models={models}
                  nextPage={nextPage}
                  prevPage={prevPage}
                  editRow={editRow}
                  deleteRow={deleteRow}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <HouseDesignsDetail
            currentState={modelState}
            setModelState={setModelState}
            nextStep={nextStep}
            prevStep={prevStep}
            addModel={addModel}
            updateModel={updateModel}
          />
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(modelState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Modelo vivienda
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default HouseDesign;
