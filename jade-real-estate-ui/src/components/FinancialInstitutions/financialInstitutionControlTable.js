import React, { useState } from "react";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
} from "reactstrap";

const step = 1;

const FinancialInstitutionControlTable = (props) => {
  const {
    fetchFinancialInstitutions,
    financials,
    nextPage,
    prevPage,
    editRow,
    deleteRow,
  } = props;
  const [value, setValue] = useState("");

  function nextStep() {
    nextPage(step + 1);
  }

  function prevStep() {
    prevPage(step - 1);
  }

  function handleValueChange(e) {
    fetchFinancialInstitutions(e.target.value);
    setValue(e.target.value);
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const userRows =
    financials === null ? (
      <table>
        <tbody>
          <tr>
            <td colSpan={16}>Buscando...</td>
          </tr>
        </tbody>
      </table>
    ) : financials.length === 0 ? (
      <table>
        <tbody>
          <tr>
            <td colSpan={16}>
              No se encontraron instituciones con el filtro especificado.
            </td>
          </tr>
        </tbody>
      </table>
    ) : (
      <div>
        <FormGroup row>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th style={thStyle}>Editar</th>
                  <th style={thStyle}>Borrar</th>
                  <th style={thStyle}>ID</th>
                  <th style={thStyle}>Nombre</th>
                  <th style={thStyle}>Descripcion</th>
                  <th style={thStyle}>Razon Social</th>
                  <th style={thStyle}>Numero Telefono</th>
                  <th style={thStyle}>Direcciones Electronicas</th>
                  <th style={thStyle}>Correos</th>
                  <th style={thStyle}>Direccion</th>
                  <th style={thStyle}>RTN</th>
                </tr>
              </thead>
              <tbody>
                {financials.financialInstitutions.map((financial) => (
                  <tr key={financial.id}>
                    <td>
                      <Button
                        block
                        color="warning"
                        onClick={() => {
                          editRow(financial);
                        }}
                      >
                        Editar
                      </Button>
                    </td>
                    <td>
                      <Button
                        block
                        color="danger"
                        onClick={() => deleteRow(financial.id)}
                      >
                        Borrar
                      </Button>
                    </td>
                    <td>{financial.id}</td>
                    <td>{financial.name}</td>
                    <td>{financial.description}</td>
                    <td>{financial.businessName}</td>
                    <td>{financial.contactNumber}</td>
                    <td>{financial.electronicAddresses}</td>
                    <td>{financial.email}</td>
                    <td>{financial.address}</td>
                    <td>{financial.rtn}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStep}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {financials.pageCount}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStep}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      </div>
    );
  return userRows;
};

export default FinancialInstitutionControlTable;
