import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
} from "reactstrap";

import FinancialInstitutionControlTable from "./financialInstitutionControlTable";
import FinancialInstitutionDetails from "./financialInstitutionDetails";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";
const heightStyle = {
  height: "500px",
};

const initialClientState = {
  step: 1,
  editMode: "None",
  currentFinancial: {
    id: "",
    name: "",
    description: "",
    businessName: "",
    contactNumber: "",
    electronicAddresses: "",
    email: "",
    address: "",
    rtn: "",
    status: "",
  },
};

const FinancialInstitutions = (props) => {
  const [financialState, setFinancialState] = useState(initialClientState);
  const [financials, setFinancials] = useState([]);

  function fetchFinancialInstitutions(query, step) {
    const index = step === undefined || step === null ? 0 : step;
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard"></Redirect>;
    }
    if (query === null || query === undefined) {
      var url = `financialInstitution/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setFinancials(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            sessionStorage.removeItem("login");
            sessionStorage.removeItem("token");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `financialInstitution/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setFinancials(res.data);
      });
    }
  }

  const editRow = (financial) => {
    setFinancialState({
      ...financialState,
      editMode: "Editing",
      currentFinancial: financial,
      step: 2,
    });
  };

  function nextPage(step) {
    fetchFinancialInstitutions(null, step);
  }

  function prevPage(step) {
    fetchFinancialInstitutions(null, step);
  }

  function nextStep(financial) {
    const { step } = financialState;

    if (financial === null || financial === undefined) {
      financial = financialState.currentFinancial;
    }
    setFinancialState({
      ...financialState,
      currentFinancial: financial,
      step: step + 1,
    });
  }

  function prevStep() {
    const { step } = financialState;

    setFinancialState({ ...financialState, step: step - 1 });
  }

  useEffect(() => {
    fetchFinancialInstitutions(null);
    // fectRoles();
  }, []);

  function add(newFinancial) {
    if (!newFinancial || !newFinancial.name) {
      return;
    }
    const url = `financialInstitution`;

    API.post(url, newFinancial).then((res) => {
      if (res.id === 0) {
        toast("Error al intentar agregar usuario");
      } else {
        toast("proyecto agregado satisfactoriamente");
        // setUsers([...users.users, userAdded]);
        fetchFinancialInstitutions(null);
        setFinancialState(initialClientState);
      }
    });
  }

  function update(financial) {
    if (!financial || !financial.name) {
      return;
    }
    const url = `financialInstitution`;
    API.put(url, financial)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar usuario");
        } else {
          toast("proyecto agregado satisfactoriamente");

          // setUsers([...users.users, userAdded]);
          fetchFinancialInstitutions(null);
          setFinancialState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  const deleteRow = (id) => {
    const url = `financialInstitution?Id=${id}`;

    API.delete(url)
      .then((res) => {
        if (res.data !== "") {
          toast("error al intentar eliminar usuario");
          return;
        }
        fetchFinancialInstitutions(null);
        setFinancialState(initialClientState);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col />
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStep()}
                >
                  <i className="fa fa-new" /> Agregar Institucion
                </Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <FinancialInstitutionControlTable
                  fetchFinancialInstitutions={fetchFinancialInstitutions}
                  financials={financials}
                  nextPage={nextPage}
                  prevPage={prevPage}
                  editRow={editRow}
                  deleteRow={deleteRow}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <FinancialInstitutionDetails
            currentState={financialState}
            setFinancialState={setFinancialState}
            nextStep={nextStep}
            prevStep={prevStep}
            add={add}
            update={update}
          />
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(financialState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Instituciones Financieras
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default FinancialInstitutions;
