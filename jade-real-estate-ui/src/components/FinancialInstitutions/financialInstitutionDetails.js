import React from "react";

import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";




const FinancialInstitutionDetails = props => {
    const { currentState, setFinancialState,  add, updateBuilder } = props;

    const handleInputChange = event => {
        const { name, value } = event.target;
        setFinancialState({
            ...currentState,
            currentFinancial: {
                ...currentState.currentFinancial,
                [name]: value
            }
        });
    };

    const handleCleanValues = () => {
        const initialClientState = {
            step: 1,
            editMode: "None",
            currentFinancial: {
                id: "",
                name: "",
                description: "",
                businessName: "",
                contactNumber: "",
                electronicAddresses: "",
                email: "",
                address: "",
                rTN: "",
                status: ""
            }
        };
        setFinancialState(initialClientState);
    };


    const handleSaveChanges = () => {
        switch (currentState.editMode) {
            case 'Editing': {
                const financial = {
                    id: currentState.currentFinancial.id,
                    name: currentState.currentFinancial.name,
                    description: currentState.currentFinancial.description,
                    businessName: currentState.currentFinancial.businessName,
                    contactNumber: currentState.currentFinancial.contactNumber,
                    electronicAddresses: currentState.currentFinancial.electronicAddresses,
                    email: currentState.currentFinancial.email,
                    address: currentState.currentFinancial.address,
                    rTN: currentState.currentFinancial.rtn,
                    status: currentState.currentFinancial.status,
                }
                updateBuilder(financial);
                break;
            }

            default: {
                const newFinancial = {
                    name: currentState.currentFinancial.name,
                    description: currentState.currentFinancial.description,
                    businessName: currentState.currentFinancial.businessName,
                    contactNumber: currentState.currentFinancial.contactNumber,
                    electronicAddresses: currentState.currentFinancial.electronicAddresses,
                    email: currentState.currentFinancial.email,
                    address: currentState.currentFinancial.address,
                    rTN: currentState.currentFinancial.rtn,
                    status: currentState.currentFinancial.status,
                }
                add(newFinancial);
                break;
            }
        };
    }

    switch (currentState.editMode) {
        case 'Editing': return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos a editar de una financiera </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="name"
                                        id="name"
                                        readonly
                                        value={currentState.currentFinancial.name}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="description" name="description"
                                        value={currentState.currentFinancial.description}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Segundo Nombre">Razon Social</Label>
                                    <Input type="text" id="businessName" name="businessName"
                                        value={currentState.currentFinancial.businessName}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="lastName">Telefonos</Label>
                                    <Input type="text" id="contactNumber" name="contactNumber"
                                        value={currentState.currentFinancial.contactNumber}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Contactos Web</Label>
                                    <Input type="text" id="electronicAddresses" name="electronicAddresses"
                                        value={currentState.currentFinancial.electronicAddresses}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Correos</Label>
                                    <Input type="text" id="email" name="email"
                                        value={currentState.currentFinancial.email}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="lastName">Direccion</Label>
                                    <Input type="text" id="address" name="address"
                                        value={currentState.currentFinancial.address}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">RTN</Label>
                                    <Input type="text" id="rtn" name="rtn"
                                        value={currentState.currentFinancial.rtn}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );
        default: return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos de nueva institucion financiera </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="name"
                                        id="name"
                                        readonly
                                        value={currentState.currentFinancial.name}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="description" name="description"
                                        value={currentState.currentFinancial.description}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Segundo Nombre">Razon Social</Label>
                                    <Input type="text" id="businessName" name="businessName"
                                        value={currentState.currentFinancial.businessName}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="lastName">Telefonos</Label>
                                    <Input type="text" id="contactNumber" name="contactNumber"
                                        value={currentState.currentFinancial.contactNumber}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Contactos Web</Label>
                                    <Input type="text" id="electronicAddresses" name="electronicAddresses"
                                        value={currentState.currentFinancial.electronicAddresses}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Correos</Label>
                                    <Input type="text" id="email" name="email"
                                        value={currentState.currentFinancial.email}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="lastName">Direccion</Label>
                                    <Input type="text" id="address" name="address"
                                        value={currentState.currentFinancial.address}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">RTN</Label>
                                    <Input type="text" id="rtn" name="rtn"
                                        value={currentState.currentFinancial.rtn}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );

    }
};
export default FinancialInstitutionDetails;
