import React, { useState, useEffect } from "react";
import API from "../API/API";

const allCurrencies = [
  { id: "LPS", name: "LPS" },
  { id: "USD", name: "USD" },
  { id: "EUR", name: "EUR" },
];

const currenciesOptions = allCurrencies.map((p) => {
  return (
    <option id={p.id} key={p.id} value={p.id}>
      {p.name}
    </option>
  );
});

const allProfiles = [
  { id: "", name: "(Seleccione un Perfil)" },
  { id: "Empleado(a)", name: "Empleado(a)" },
  { id: "Comerciante", name: "Comerciante" },
  { id: "Profesional Independiente", name: "Profesional Independiente" },
  { id: "Empleado y Comerciante", name: "Empleado y Comerciante" },
  {
    id: "Empleado y Profesional Independiente",
    name: "Empleado y Profesional Independiente",
  },
];

const profilesOptions = allProfiles.map((p) => {
  return (
    <option id={p.id} key={p.id} value={p.id}>
      {p.name}
    </option>
  );
});

const relationships = [
  { id: "", name: "(Seleccione el Parentezco)" },
  { id: "Conyugue", name: "Conyugue" },
  { id: "Madre", name: "Madre" },
  { id: "Padre", name: "Padre" },
  { id: "Hermano", name: "Hermano" },
  { id: "Hermana", name: "Hermana" },
];

const relationshipOptions = relationships.map((p) => {
  return (
    <option id={p.id} key={p.id} value={p.id}>
      {p.name}
    </option>
  );
});

const creditSates = [
  { id: "PENDIENTE", name: "PENDIENTE" },
  { id: "APROBADO", name: "APROBADO" },
];

const creditSatesOptions = creditSates.map((p) => {
  return (
    <option id={p.id} key={p.id} value={p.id}>
      {p.name}
    </option>
  );
});

const initialApplicationOptions = {
  projects: [],
  builders: [],
  contactTypes: [],
  nationalities: [],
  financialInstitutions: [],
  salesAdvisors: [],
  supplierServices: [],
  clientCategories: [],
  fundTypes: [],
  currencies: currenciesOptions,
  relations: relationshipOptions,
  creditSates: creditSatesOptions,
  infoClientLevel: "N/A",
};

const UseApplicationOptions = () => {
  const [applicationOptions, setApplicationOptions] = useState(
    initialApplicationOptions
  );
  const [loadingApplicationOptions, setLoadingApplicationOptions] =
    useState(true);

  useEffect(() => {
    fetchApplicationOptions();
  }, []);

  async function fetchApplicationOptions() {
    const url = `application-options`;

    API.get(url).then((res) => {
      const salesAdvisors = [
        { id: 0, name: "(Seleccione un Asesor de  Ventas)" },
      ].concat(
        res.data.salesAdvisors
          .map((user) => {
            return {
              id: user.id,
              name: user.firstName + " " + user.lastName,
            };
          })
          .sort(compareNames)
      );

      const supplierServices = [
        { id: 0, name: "(Seleccione un servicio)" },
      ].concat(res.data.supplierServices.sort(compareNames));

      const projects = [{ id: 0, name: "(Seleccione un Proyecto)" }].concat(
        res.data.projects.sort(compareNames)
      );

      const builders = [{ id: 0, name: "(Seleccione un Constructor)" }].concat(
        res.data.builders.sort(compareNames)
      );

      const contactTypes = [
        { id: -1, name: "(Seleccione un Tipo de Contacto)" },
      ].concat(res.data.contactTypes.sort(compareNames));

      const nationalities = [
        { id: 0, name: "(Seleccione la Nacionalidad)" },
      ].concat(res.data.nationalities.sort(compareNames));

      const financialInstitutions = [
        { id: 0, name: "(Seleccione una Financiera)" },
      ].concat(res.data.financialInstitutions.sort(compareNames));

      const clientCategories = [
        { id: 0, name: "(Seleccione una categoria)" },
      ].concat(res.data.clientCategories.sort(compareNames));

      const fundTypes = [
        { id: 0, name: "(Seleccione el tipo de fondos)" },
      ].concat(res.data.fundTypes.sort(compareNames));

      const appOptions = {
        projects: createOptions(projects),
        builders: createOptions(builders),
        contactTypes: createOptions(contactTypes),
        nationalities: createOptions(nationalities),
        financialInstitutions: createOptions(financialInstitutions),
        salesAdvisors: createOptions(salesAdvisors),
        supplierServices: createOptions(supplierServices),
        clientCategories: createOptions(clientCategories),
        fundTypes: createOptions(fundTypes),
        currencies: currenciesOptions,
        profiles: profilesOptions,
        relationships: relationshipOptions,
        creditSates: creditSatesOptions,
        infoClientLevel: res.data.infoClientLevel,
      };

      setApplicationOptions(appOptions);
      setLoadingApplicationOptions(false);
    });
  }

  const createOptions = (options) => {
    return options.map((p) => {
      return (
        <option id={p.id} key={p.id} value={p.id}>
          {p.name}
        </option>
      );
    });
  };

  const compareNames = (currentItem, nextItem) => {
    const currentName = currentItem.name;
    const nextName = nextItem.name;

    let comparison = 0;
    if (currentName > nextName) {
      comparison = 1;
    } else if (currentName < nextName) {
      comparison = -1;
    }
    return comparison;
  };

  return { loadingApplicationOptions, applicationOptions };
};

export default UseApplicationOptions;
