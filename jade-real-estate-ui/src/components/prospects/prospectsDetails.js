import React, { useState, useEffect } from "react";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Table,
  Row,
  ModalFooter,
  ModalHeader,
  Modal,
  ModalBody,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";
import API from "./../API/API";
import { Redirect } from "react-router-dom";
import checkmark from "../../../src/assets/img/brand/checkmark.svg";
import styled from "styled-components";
import ReactLoading from "react-loading";

const Container = styled.div`
  background-color: white;
  margin: 0px;
`;

const ProspectsDetails = (props) => {
  const {
    currentState,
    setProspectState,
    add,
    update,
    className,
    apiCallInProgress,
    setApiCallInProgress,
  } = props;
  const [value, setValue] = useState("");
  const [valueContacType, setValueContactType] = useState("");
  const [projectSelected, setprojectSelected] = useState([]);
  const [projects, setProjects] = useState([]);
  const [contactTypeSelected, setContactTypeSelected] = useState([]);
  const [contactTypes, setContactTypes] = useState([]);
  const [messageValidation, setMessageValidation] = useState("");
  const [newProspectState, setNewProspectState] = useState([]);

  const handleInputChange = (event) => {
    const target = event.target;
    const name = target.name;
    const value =
      target.type === "date"
        ? moment(target.value).format("YYYY-MM-DD")
        : target.value;

    setProspectState({
      ...currentState,
      currentProspect: {
        ...currentState.currentProspect,
        [name]: value,
      },
    });
  };

  const handleNumericInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    setProspectState({
      ...currentState,
      currentProspect: {
        ...currentState.currentProspect,
        [name]: parseInt(value, 10),
      },
    });
  };

  const selectProject = (project) => {
    setprojectSelected(project);
    setProspectState({
      ...currentState,
      state: {
        large: !currentState.state.large,
      },
    });
  };

  const selectContactType = (contact) => {
    setContactTypeSelected(contact);
    setProspectState({
      ...currentState,
      state: {
        small: !currentState.state.small,
      },
    });
  };

  function SaveChangesNewClient() {
    const client = {
      ...currentState.currentClient,
      clientCreationComments: messageValidation,
    };

    add(client);
  }

  const handleCleanValues = () => {
    const initialClientState = {
      step: 1,
      editMode: "None",
      currentProspect: {
        id: "",
        creationDate: "",
        registerDate: "",
        projectId: "",
        contactType: "",
        name: "",
        contactNumberOne: "",
        contactNumberTwo: "",
        email: "",
        projectName: "",
        contactTypeName: "",
        project: [],
        comments: "",
        salesAdvisorId: "",
        salesAdvisor: "",
        contactTypes: [],
      },
      state: {
        modal: false,
        large: false,
        large1: false,
        small: false,
        primary: false,
        success: false,
        warning: false,
        danger: false,
        info: false,
      },
    };
    setProspectState(initialClientState);
  };

  useEffect(() => {
    if (
      currentState.currentProspect &&
      currentState.currentProspect.projectId
    ) {
      fetchProjects(currentState.currentProspect.projectName, 0);
    }
    if (
      currentState.currentProspect &&
      currentState.currentProspect.contactType
    ) {
      fetchContactTypes(currentState.currentProspect.contactTypeName, 0);
    }
  }, []);

  function fetchProjects(query, page) {
    const index = page === undefined || page === null ? 0 : page;
    if (query === null || query === "") {
      var url = `projects/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setProjects(res.data);
        })
        .catch((error) => {
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `projects/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setProjects(res.data);
        if (currentState.editMode === "Editing") {
          const ps = res.data.projects.find(
            (x) => x.id === currentState.currentProspect.projectId
          );
          setprojectSelected(ps);
        }
      });
    }
  }

  const nextPageProjects = () => {
    fetchProjects(value, projects.pageIndex + 1);
  };

  function prevPageProjects() {
    fetchProjects(value, projects.pageIndex - 1);
  }

  const nextPageContactTypes = () => {
    fetchContactTypes(value, contactTypes.pageIndex + 1);
  };

  function prevPageContactTypes() {
    fetchContactTypes(value, contactTypes.pageIndex - 1);
  }

  function fetchContactTypes(query, step) {
    const index = step === undefined || step === null ? 0 : step;
    if (query === null || query === "") {
      var url = `contact/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setContactTypes(res.data);
        })
        .catch((error) => {
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `contact/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setContactTypes(res.data);
        if (currentState.editMode === "Editing") {
          const ps = res.data.contactTypes.find(
            (x) => x.id === currentState.currentProspect.contactType
          );
          setContactTypeSelected(ps);
        }
      });
    }
  }

  const activeValidateNewClient = () => {
    setProspectState({
      ...currentState,
      state: {
        large2: !currentState.state.large2,
      },
    });
  };

  const togglePrimary = () => {
    setProspectState({
      ...currentState,
      state: {
        large: !currentState.state.large,
      },
    });
    fetchProjects(null, 0);
  };

  const toggleContactType = () => {
    setProspectState({
      ...currentState,
      state: {
        small: !currentState.state.small,
      },
    });
    fetchContactTypes(null, 0);
  };

  function handleValueChange(e) {
    fetchProjects(e.target.value);
    setValue(e.target.value);
  }

  function handleValueChangeContactType(e) {
    fetchContactTypes(e.target.value);
    setValueContactType(e.target.value);
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const salesAdvisorsOptions = currentState.salesAdvisors.map((p) => {
    return (
      <option id={p.id} key={p.id} value={p.id}>
        {p.name}
      </option>
    );
  });

  function trySaveChangeNewClient(client) {
    switch (currentState.editMode) {
      case "Editing": {
        handleSaveChanges();
        break;
      }
      default: {
        setApiCallInProgress(true);
        const newProspect = {
          creationDate: moment(
            currentState.currentProspect.creationDate
          ).format("YYYY-MM-DD"),
          registerDate: moment(currentState.registerDate).format("YYYY-MM-DD"),
          projectId: projectSelected.id,
          contactType: contactTypeSelected.id,
          name: currentState.currentProspect.name,
          idNumber: currentState.currentProspect.idNumber,
          contactNumberOne: currentState.currentProspect.contactNumberOne,
          contactNumberTwo: currentState.currentProspect.contactNumberTwo,
          email: currentState.currentProspect.email,
          comments: currentState.currentProspect.comments,
          salesAdvisorId: currentState.currentProspect.salesAdvisorId,
          salesAdvisor: currentState.currentProspect.salesAdvisor,
        };
        const url = `prospect/validate-create-new-client`;
        API.post(url, newProspect)
          .then((res) => {
            if (res.data !== undefined || res.data !== null) {
              if (
                res.data.validationErrorMessage !== null &&
                res.data !== undefined
              ) {
                setMessageValidation(res.data.validationErrorMessage);
                activeValidateNewClient();
                setApiCallInProgress(false);
              } else {
                setApiCallInProgress(false);
                handleSaveChanges();
              }
            }
          })
          .catch((error) => {
            setApiCallInProgress(false);
          });
        break;
      }
    }
  }

  const handleSaveChanges = () => {
    switch (currentState.editMode) {
      case "Editing": {
        const prospect = {
          creationDate: moment(
            currentState.currentProspect.creationDate
          ).format("YYYY-MM-DD"),
          registerDate: moment(currentState.registerDate).format("YYYY-MM-DD"),
          id: currentState.currentProspect.id,
          projectId: projectSelected.id,
          contactType: contactTypeSelected.id,
          name: currentState.currentProspect.name,
          idNumber: currentState.currentProspect.idNumber,
          contactNumberOne: currentState.currentProspect.contactNumberOne,
          contactNumberTwo: currentState.currentProspect.contactNumberTwo,
          email: currentState.currentProspect.email,
          transactionOrigin: currentState.currentProspect.transactionOrigin,
          comments: currentState.currentProspect.comments,
          salesAdvisorId: currentState.currentProspect.salesAdvisorId,
          salesAdvisor: currentState.currentProspect.salesAdvisor,
        };

        update(prospect);
        break;
      }

      default: {
        const newProspect = {
          creationDate: moment(
            currentState.currentProspect.creationDate
          ).format("YYYY-MM-DD"),
          registerDate: moment(currentState.registerDate).format("YYYY-MM-DD"),
          projectId: projectSelected.id,
          contactType: contactTypeSelected.id,
          name: currentState.currentProspect.name,
          idNumber: currentState.currentProspect.idNumber,
          contactNumberOne: currentState.currentProspect.contactNumberOne,
          contactNumberTwo: currentState.currentProspect.contactNumberTwo,
          email: currentState.currentProspect.email,
          comments: currentState.currentProspect.comments,
          salesAdvisorId: currentState.currentProspect.salesAdvisorId,
          salesAdvisor: currentState.currentProspect.salesAdvisor,
        };
        add(newProspect);
        break;
      }
    }
  };

  function getFooterBuild(e) {
    if (apiCallInProgress) {
      return (
        <CardFooter>
          <Col md="3" sm="6" xs="12"></Col>
          <Col md="3" sm="6" xs="12">
            <ReactLoading
              type="bars"
              color="#5DBCD2"
              height={"30%"}
              width={"30%"}
            />
          </Col>
          <Col md="3" sm="6" xs="12"></Col>
        </CardFooter>
      );
    } else {
      return (
        <CardFooter>
          <Button
            type="submit"
            size="sm"
            color="success"
            onClick={trySaveChangeNewClient}
            disabled={apiCallInProgress}
          >
            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
          <Button
            type="reset"
            size="sm"
            color="danger"
            onClick={handleCleanValues}
          >
            <i className="fa fa-ban" /> Cancelar
          </Button>
        </CardFooter>
        // <CardFooter>
        //   <Button
        //     type="submit"
        //     size="sm"
        //     color="success"
        //     disabled={apiCallInProgress}
        //     // onClick={() => {
        //     // saveChanges(currentState.currentClient);
        //     // }}
        //     onClick={() => {
        //       trySaveChange(currentState.currentClient);
        //     }}
        //   >
        //     <i className="fa fa-dot-circle-o" /> Guardar
        //   </Button>
        //   <Button type="reset" size="sm" color="danger" onClick={cancelChanges}>
        //     <i className="fa fa-ban" /> Cancelar
        //   </Button>
        // </CardFooter>
      );
    }
  }

  switch (currentState.editMode) {
    case "Editing":
      return (
        <Container className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos a editar del Cliente Varios </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="creationDate">Fecha de Creacion</Label>
                    <Input
                      type="date"
                      name="creationDate"
                      id="creationDate"
                      placeholder="Fecha de Creacion"
                      value={moment(
                        currentState.currentProspect.creationDate
                      ).format("YYYY-MM-DD")}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>

                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="name"
                      id="name"
                      value={currentState.currentProspect.name}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Contacto #1</Label>
                    <Input
                      type="text"
                      id="contactNumberOne"
                      name="contactNumberOne"
                      value={currentState.currentProspect.contactNumberOne}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Contacto #2</Label>
                    <Input
                      type="text"
                      id="contactNumberTwo"
                      name="contactNumberTwo"
                      value={currentState.currentProspect.contactNumberTwo}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Correo</Label>
                    <Input
                      type="text"
                      name="email"
                      id="email"
                      value={currentState.currentProspect.email}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Identidad</Label>
                    <Input
                      type="text"
                      name="idNumber"
                      id="idNumber"
                      value={currentState.currentProspect.idNumber}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName">Proyecto</Label>
                    <Input
                      type="text"
                      id="projectId"
                      name="projectId"
                      value={projectSelected.name}
                      // onChange={handleInputChange}
                      disabled={true}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <Row>
                    <Label htmlFor="Segundo Nombre"> Lista Proyectos </Label>
                  </Row>
                  <Row>
                    <Button color="primary" onClick={togglePrimary}>
                      Seleccionar Proyecto
                    </Button>
                    <Modal
                      isOpen={currentState.state.large}
                      toggle={togglePrimary}
                      className={"modal-lg" + className}
                    >
                      <ModalHeader toggle={togglePrimary}>
                        Seleccionar Proyecto
                      </ModalHeader>
                      <ModalBody>
                        <FormGroup row>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button type="button" color="primary">
                                  <i className="fa fa-search" /> Buscar
                                </Button>
                              </InputGroupAddon>
                              <Input
                                type="text"
                                id="input1-group2"
                                name="input1-group2"
                                placeholder="Buscar"
                                value={value}
                                onChange={handleValueChange}
                              />
                            </InputGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="12">
                            {projects === null ? (
                              <tr>
                                <td colSpan={16}>Buscando...</td>
                              </tr>
                            ) : projects.length === 0 ? (
                              <tr>
                                <td colSpan={16}>
                                  No se encontraron proyectos.
                                </td>
                              </tr>
                            ) : (
                              <Table
                                hover
                                bordered
                                striped
                                responsive
                                size="sm"
                              >
                                <thead>
                                  <tr>
                                    <th style={thStyle}>Seleccionar</th>
                                    <th style={thStyle}>Nombre</th>
                                    <th style={thStyle}>Pais</th>
                                    <th style={thStyle}>Departamento</th>
                                    <th style={thStyle}>Ciudad</th>
                                    <th style={thStyle}>Direccion</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {projects.projects.map((project) => (
                                    <tr key={project.id}>
                                      <td>
                                        <Button
                                          onClick={() => {
                                            selectProject(project);
                                          }}
                                        >
                                          <img
                                            src={checkmark}
                                            height="100%"
                                            width="100%"
                                            alt="checkmark"
                                          />
                                        </Button>
                                      </td>
                                      <td>{project.name}</td>
                                      <td>{project.country}</td>
                                      <td>{project.state}</td>
                                      <td>{project.city}</td>
                                      <td>{project.address}</td>
                                    </tr>
                                  ))}
                                </tbody>
                              </Table>
                            )}
                          </Col>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={prevPageProjects}
                                >
                                  <i className="icon-arrow-left-circle" />{" "}
                                  Anterior
                                </Button>
                              </InputGroupAddon>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Paginas: {projects.pageCount}</strong>
                                </h5>
                              </div>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Actual: {projects.pageIndex}</strong>
                                </h5>
                              </div>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={nextPageProjects}
                                >
                                  Proximo{" "}
                                  <i className="icon-arrow-right-circle" />
                                </Button>
                              </InputGroupAddon>
                            </InputGroup>
                          </Col>
                        </FormGroup>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={togglePrimary}>
                          Cancel
                        </Button>
                      </ModalFooter>
                    </Modal>
                  </Row>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName1">Tipo de contacto</Label>
                    <Input
                      type="text"
                      id="contactType"
                      name="contactType"
                      value={contactTypeSelected.name}
                      // onChange={handleInputChange}
                      disabled={true}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <Row>
                    <Label htmlFor="Segundo Nombre"> Tipos de Contacto </Label>
                  </Row>
                  <Row>
                    <Button color="primary" onClick={toggleContactType}>
                      Seleccionar Contacto
                    </Button>
                    <Modal
                      isOpen={currentState.state.small}
                      toggle={toggleContactType}
                      className={"modal-lg-1" + className}
                    >
                      <ModalHeader toggle={toggleContactType}>
                        Seleccionar Contacto
                      </ModalHeader>
                      <ModalBody>
                        <FormGroup row>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button type="button" color="primary">
                                  <i className="fa fa-search" /> Buscar
                                </Button>
                              </InputGroupAddon>
                              <Input
                                type="text"
                                id="input1-group2"
                                name="input1-group2"
                                placeholder="Buscar"
                                value={valueContacType}
                                onChange={handleValueChangeContactType}
                              />
                            </InputGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="12">
                            {contactTypes === null ? (
                              <tr>
                                <td colSpan={16}>Buscando...</td>
                              </tr>
                            ) : contactTypes.length === 0 ? (
                              <tr>
                                <td colSpan={16}>
                                  No se encontraron tipos de contacto.
                                </td>
                              </tr>
                            ) : (
                              <Table
                                hover
                                bordered
                                striped
                                responsive
                                size="sm"
                              >
                                <thead>
                                  <tr>
                                    <th
                                      style={{
                                        width: "60px",
                                        background: "#20a8d8",
                                        color: "white",
                                      }}
                                    >
                                      Seleccionar
                                    </th>
                                    <th style={thStyle}>Nombre</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {contactTypes.contactTypes.map((contact) => (
                                    <tr key={contact.id}>
                                      <td>
                                        <Button
                                          onClick={() => {
                                            selectContactType(contact);
                                          }}
                                        >
                                          <img
                                            src={checkmark}
                                            height="100%"
                                            width="100%"
                                            alt="checkmark"
                                          />
                                        </Button>
                                      </td>
                                      <td>{contact.name}</td>
                                    </tr>
                                  ))}
                                </tbody>
                              </Table>
                            )}
                          </Col>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={prevPageContactTypes}
                                >
                                  <i className="icon-arrow-left-circle" />{" "}
                                  Anterior
                                </Button>
                              </InputGroupAddon>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>
                                    Paginas: {contactTypes.pageCount}
                                  </strong>
                                </h5>
                              </div>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>
                                    Actual: {contactTypes.pageIndex}
                                  </strong>
                                </h5>
                              </div>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={nextPageContactTypes}
                                >
                                  Proximo{" "}
                                  <i className="icon-arrow-right-circle" />
                                </Button>
                              </InputGroupAddon>
                            </InputGroup>
                          </Col>
                        </FormGroup>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={toggleContactType}>
                          Cancel
                        </Button>
                      </ModalFooter>
                    </Modal>
                  </Row>
                </Col>
              </Row>

              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label>Asesor de Ventas</Label>
                    <Input
                      type="select"
                      name="salesAdvisorId"
                      value={currentState.currentProspect.salesAdvisorId}
                      onChange={handleNumericInputChange}
                      required
                    >
                      {salesAdvisorsOptions}
                    </Input>
                  </FormGroup>
                </Col>

                <Col>
                  <FormGroup>
                    <Label htmlFor="comments">Comentarios</Label>
                    <Input
                      type="text"
                      id="comments"
                      name="comments"
                      value={currentState.currentProspect.comments}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </Container>
      );
    default:
      return (
        <Container className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos de nuevo cliente </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="creationDate">Fecha de Creacion</Label>
                    <Input
                      type="date"
                      name="creationDate"
                      id="creationDate"
                      placeholder="Fecha de Creacion"
                      value={moment(
                        currentState.currentProspect.creationDate
                      ).format("YYYY-MM-DD")}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>

                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="name"
                      id="name"
                      value={currentState.currentProspect.name}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Contacto #1</Label>
                    <Input
                      type="text"
                      id="contactNumberOne"
                      name="contactNumberOne"
                      value={currentState.currentProspect.contactNumberOne}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Contacto #2</Label>
                    <Input
                      type="text"
                      id="contactNumberTwo"
                      name="contactNumberTwo"
                      value={currentState.currentProspect.contactNumberTwo}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Correo</Label>
                    <Input
                      type="text"
                      name="email"
                      id="email"
                      value={currentState.currentProspect.email}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Identidad</Label>
                    <Input
                      type="text"
                      name="idNumber"
                      id="idNumber"
                      value={currentState.currentProspect.idNumber}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName">Proyecto</Label>
                    <Input
                      type="text"
                      id="projectId"
                      name="projectId"
                      value={projectSelected.name}
                      onChange={handleInputChange}
                      required
                      disabled={true}
                    />
                  </FormGroup>
                </Col>

                <Col md="3" sm="6" xs="12">
                  <Row>
                    <Label htmlFor="Segundo Nombre"> Lista Proyectos </Label>
                  </Row>
                  <Row>
                    <Button color="primary" onClick={togglePrimary}>
                      Seleccionar Proyecto
                    </Button>
                    <Modal
                      isOpen={currentState.state.large}
                      toggle={togglePrimary}
                      className={"modal-lg" + className}
                    >
                      <ModalHeader toggle={togglePrimary}>
                        Seleccionar Proyecto
                      </ModalHeader>
                      <ModalBody>
                        <FormGroup row>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button type="button" color="primary">
                                  <i className="fa fa-search" /> Buscar
                                </Button>
                              </InputGroupAddon>
                              <Input
                                type="text"
                                id="input1-group2"
                                name="input1-group2"
                                placeholder="Buscar"
                                value={value}
                                onChange={handleValueChange}
                              />
                            </InputGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="12">
                            {projects === null ? (
                              <table>
                                <tbody>
                                  <tr>
                                    <td colSpan={16}>Buscando...</td>
                                  </tr>
                                </tbody>
                              </table>
                            ) : projects.length === 0 ? (
                              <table>
                                <tbody>
                                  <tr>
                                    <td colSpan={16}>
                                      No se encontraron proyectos.
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            ) : (
                              <Table
                                hover
                                bordered
                                striped
                                responsive
                                size="sm"
                              >
                                <thead>
                                  <tr>
                                    <th style={thStyle}>Seleccionar</th>
                                    <th style={thStyle}>Nombre</th>
                                    <th style={thStyle}>Pais</th>
                                    <th style={thStyle}>Departamento</th>
                                    <th style={thStyle}>Ciudad</th>
                                    <th style={thStyle}>Direccion</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {projects.projects.map((project) => (
                                    <tr key={project.id}>
                                      <td>
                                        <Button
                                          onClick={() => {
                                            selectProject(project);
                                          }}
                                        >
                                          <img
                                            src={checkmark}
                                            height="100%"
                                            width="100%"
                                            alt="checkmark"
                                          />
                                        </Button>
                                      </td>
                                      <td>{project.name}</td>
                                      <td>{project.country}</td>
                                      <td>{project.state}</td>
                                      <td>{project.city}</td>
                                      <td>{project.address}</td>
                                    </tr>
                                  ))}
                                </tbody>
                              </Table>
                            )}
                          </Col>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={prevPageProjects}
                                >
                                  <i className="icon-arrow-left-circle" />{" "}
                                  Anterior
                                </Button>
                              </InputGroupAddon>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Paginas: {projects.pageCount}</strong>
                                </h5>
                              </div>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Actual: {projects.pageIndex}</strong>
                                </h5>
                              </div>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={nextPageProjects}
                                >
                                  Proximo{" "}
                                  <i className="icon-arrow-right-circle" />
                                </Button>
                              </InputGroupAddon>
                            </InputGroup>
                          </Col>
                        </FormGroup>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={togglePrimary}>
                          Cancel
                        </Button>
                      </ModalFooter>
                    </Modal>
                  </Row>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName1">Tipo de contacto</Label>
                    <Input
                      type="text"
                      id="contactType"
                      name="contactType"
                      value={contactTypeSelected.name}
                      // onChange={handleInputChange}
                      disabled={true}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <Row>
                    <Label htmlFor="Segundo Nombre"> Tipos de Contacto </Label>
                  </Row>
                  <Row>
                    <Button color="primary" onClick={toggleContactType}>
                      Seleccionar Contacto
                    </Button>
                    <Modal
                      isOpen={currentState.state.small}
                      toggle={toggleContactType}
                      className={"modal-lg-1" + className}
                    >
                      <ModalHeader toggle={toggleContactType}>
                        Seleccionar Contacto
                      </ModalHeader>
                      <ModalBody>
                        <FormGroup row>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button type="button" color="primary">
                                  <i className="fa fa-search" /> Buscar
                                </Button>
                              </InputGroupAddon>
                              <Input
                                type="text"
                                id="input1-group2"
                                name="input1-group2"
                                placeholder="Buscar"
                                value={valueContacType}
                                onChange={handleValueChangeContactType}
                              />
                            </InputGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="12">
                            {contactTypes === null ? (
                              <table>
                                <tbody>
                                  <tr>
                                    <td colSpan={16}>Buscando...</td>
                                  </tr>
                                </tbody>
                              </table>
                            ) : contactTypes.length === 0 ? (
                              <table>
                                <tbody>
                                  <tr>
                                    <td colSpan={16}>
                                      No se encontraron tipos de contacto.
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            ) : (
                              <Table
                                hover
                                bordered
                                striped
                                responsive
                                size="sm"
                              >
                                <thead>
                                  <tr>
                                    <th
                                      style={{
                                        width: "60px",
                                        background: "#20a8d8",
                                        color: "white",
                                      }}
                                    >
                                      Seleccionar
                                    </th>
                                    <th style={thStyle}>Nombre</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {contactTypes.contactTypes.map((contact) => (
                                    <tr key={contact.id}>
                                      <td>
                                        <Button
                                          onClick={() => {
                                            selectContactType(contact);
                                          }}
                                        >
                                          <img
                                            src={checkmark}
                                            height="30px"
                                            width="30px"
                                            alt="checkmark"
                                          />
                                        </Button>
                                      </td>
                                      <td>{contact.name}</td>
                                    </tr>
                                  ))}
                                </tbody>
                              </Table>
                            )}
                          </Col>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={prevPageContactTypes}
                                >
                                  <i className="icon-arrow-left-circle" />{" "}
                                  Anterior
                                </Button>
                              </InputGroupAddon>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>
                                    Paginas: {contactTypes.pageCount}
                                  </strong>
                                </h5>
                              </div>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>
                                    Actual: {contactTypes.pageIndex}
                                  </strong>
                                </h5>
                              </div>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={nextPageContactTypes}
                                >
                                  Proximo{" "}
                                  <i className="icon-arrow-right-circle" />
                                </Button>
                              </InputGroupAddon>
                            </InputGroup>
                          </Col>
                        </FormGroup>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={toggleContactType}>
                          Cancel
                        </Button>
                      </ModalFooter>
                    </Modal>
                  </Row>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label>Asesor de Ventas</Label>
                    <Input
                      type="select"
                      name="salesAdvisorId"
                      value={currentState.currentProspect.salesAdvisorId}
                      onChange={handleNumericInputChange}
                      required
                    >
                      {salesAdvisorsOptions}
                    </Input>
                  </FormGroup>
                </Col>

                <Col>
                  <FormGroup>
                    <Label htmlFor="comments">Comentarios</Label>
                    <Input
                      type="text"
                      id="comments"
                      name="comments"
                      value={currentState.currentProspect.comments}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            {/* <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter> */}
            {getFooterBuild()}
          </Card>
          <Modal
            isOpen={currentState.state.large2}
            toggle={activeValidateNewClient}
            className={"modal-lg"}
          >
            <ModalHeader toggle={activeValidateNewClient}>
              Se identificaron coincidencias
            </ModalHeader>
            <ModalBody>
              <div>
                <h3>
                  Se encontro un cliente registrado con los mismos datos:{" "}
                </h3>

                <p>
                  <strong>{messageValidation}</strong>
                </p>
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="primary"
                onClick={() => {
                  handleSaveChanges();
                }}
              >
                Crear de todas formas
              </Button>
              <Button color="danger" onClick={activeValidateNewClient}>
                Cancelar ingreso
              </Button>
            </ModalFooter>
          </Modal>
        </Container>
      );
  }
};

export default ProspectsDetails;
