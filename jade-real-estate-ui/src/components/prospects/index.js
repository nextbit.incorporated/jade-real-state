import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
} from "reactstrap";
import ProspectsControlTable from "./prospectsControlTable";
import ProspectsDetails from "./prospectsDetails";
import ImportFileData from "./importFileData";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";
import moment from "moment";

const heightStyle = {
  height: "500px",
};

const initialClientState = {
  step: 1,
  editMode: "None",
  startDate: Date.now(),
  file: [],
  fileName: "",
  infoDataxlsx: [],
  currentProspect: {
    id: "",
    creationDate: "",
    registerDate: "",
    projectId: "",
    contactType: "",
    name: "",
    idNumber: "",
    contactNumberOne: "",
    contactNumberTwo: "",
    email: "",
    projectName: "",
    contactTypeName: "",
    project: [],
    contactTypes: [],
  },
  state: {
    modal: false,
    large: false,
    large1: false,
    large2: false,
    small: false,
    primary: false,
    success: false,
    warning: false,
    danger: false,
    info: false,
    uploadFile: false,
    className: null,
  },
};

const Prospects = (props) => {
  const [prospectState, setProspectState] = useState(initialClientState);
  const [prospects, setProspects] = useState([]);
  const [filters, setFilters] = useState([]);
  const [filterSelected, setFilterSelected] = useState([]);
  const [salesAdvisors, setSalesAdvisors] = useState([]);
  const [apiCallInProgress, setApiCallInProgress] = useState(false);

  useEffect(() => {
    fetchProspects(null);
    fetchFilters();
    fetchSalesAdvisors();
  }, []);

  function compareNames(currentItem, nextItem) {
    const currentName = currentItem.name;
    const nextName = nextItem.name;

    let comparison = 0;
    if (currentName > nextName) {
      comparison = 1;
    } else if (currentName < nextName) {
      comparison = -1;
    }
    return comparison;
  }

  function fetchSalesAdvisors() {
    const url = `user`;

    API.get(url)
      .then((res) => {
        const salesAdvisors = [
          { id: 0, name: "(Seleccione un Asesor de  Ventas)" },
        ].concat(
          res.data
            .map((user) => {
              return {
                id: user.id,
                name: user.firstName + " " + user.lastName,
              };
            })
            .sort(compareNames)
        );

        setSalesAdvisors(salesAdvisors);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para obtener los asesores de ventas favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  }

  function fetchProspects(query, step, filterSelected) {
    const user = sessionStorage.getItem("userId");
    const index = step === undefined || step === null ? 0 : step;
    if (query === null) {
      var url = `prospect/paged?pageSize=10&pageIndex=${index}&user=${user}`;

      API.get(url)
        .then((res) => {
          setProspects(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `prospect/paged?pageSize=10&pageIndex=${index}&value=${query}&user=${user}&ridFilter=${filterSelected}`;
      API.get(url1).then((res) => {
        setProspects(res.data);
      });
    }
  }

  function fetchFilters() {
    var url = `clients/filters`;

    API.get(url)
      .then((res) => {
        setFilters(res.data);
        setFilterSelected(res.data[0]);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast("Error de red");
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  const editRow = (prospect) => {
    setProspectState({
      ...prospectState,
      editMode: "Editing",
      currentProspect: prospect,
      salesAdvisors,
      step: 2,
    });
  };

  function nextPage(step) {
    fetchProspects(null, step);
  }

  function prevPage(step) {
    fetchProspects(null, step);
  }

  function nextStep(prospect) {
    const { step } = prospectState;

    if (prospect === null || prospect === undefined) {
      const currentDate = moment(Date.now()).format("L");
      prospect = {
        ...prospectState.currentProspect,
        creationDate: currentDate,
        registerDate: currentDate,
      };
    }

    setProspectState({
      ...prospectState,
      currentProspect: prospect,
      salesAdvisors,
      step: step + 1,
      editMode: "Add",
    });
  }

  function nextStepXLS(prospect) {
    const { step } = prospectState;

    if (prospect === null || prospect === undefined) {
      prospect = prospectState.currentProspect;
    }
    setProspectState({
      ...prospectState,
      currentProspect: prospect,
      salesAdvisors,
      step: step + 2,
    });
  }

  function prevStep() {
    const { step } = prospectState;

    setProspectState({ ...prospectState, step: step - 1 });
  }

  function saveInfoImport(items, projectSelectedId, contactTypeSelectedId) {
    if (!items) {
      return;
    }
    const prospectsImport = {
      listInfXls: items,
      projectId: projectSelectedId,
      contactTypeId: contactTypeSelectedId,
    };
    const url = `prospect/import-info`;

    API.post(url, prospectsImport)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar nuevo cliente");
        } else {
          toast("Cliente agregado satisfactoriamente");
          fetchProspects(null);
          setProspectState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  function add(newProspect) {
    if (!newProspect || !newProspect.name) {
      return;
    }
    const url = `prospect`;

    API.post(url, newProspect)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar nuevo cliente");
        } else {
          toast("Cliente agregado satisfactoriamente");
          // setUsers([...users.users, userAdded]);
          fetchProspects(null);
          setProspectState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas crear el nuevo Cliente Varios favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  }

  function update(supplierService) {
    if (!supplierService || !supplierService.name) {
      return;
    }
    const url = `prospect`;
    API.put(url, supplierService)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar actualizar nacionalidad");
        } else {
          toast("Nacionalidad actualizada satisfactoriamente");

          // setUsers([...users.users, userAdded]);
          fetchProspects(null);
          setProspectState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para actualizar Cliente Varios favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  }

  const deleteRegister = (id) => {
    const url = `prospect?Id=${id}`;

    API.delete(url)
      .then((res) => {
        if (res.data !== "") {
          toast("error al intentar eliminar nacionalidad");
          return;
        }
        fetchProspects(null);
        setProspectState(initialClientState);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          // this.props.history.push("/login");
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  };

  const MoveRegisterToClients = (id) => {
    const url = `prospect/move-to-client?id=${id}`;

    API.get(url)
      .then((res) => {
        if (res.data !== "") {
          toast("error al intentar promover a cliente");
          return;
        }
        fetchProspects(null);
        setProspectState(initialClientState);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          // this.props.history.push("/login");
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStepXLS()}
                >
                  <i className="fa fa-new" /> Subir archivo con clientes
                </Button>
              </Col>
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStep()}
                >
                  <i className="fa fa-new" /> Agregar nuevo Cliente Varios
                </Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <ProspectsControlTable
                  currentState={prospectState}
                  setProspectState={setProspectState}
                  fetchProspects={fetchProspects}
                  prospects={prospects}
                  nextPage={nextPage}
                  prevPage={prevPage}
                  editRow={editRow}
                  deleteRegister={deleteRegister}
                  MoveRegisterToClients={MoveRegisterToClients}
                  filters={filters}
                  setFilterSelected={setFilterSelected}
                  filterSelected={filterSelected}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <ProspectsDetails
            currentState={prospectState}
            setProspectState={setProspectState}
            nextStep={nextStep}
            prevStep={prevStep}
            add={add}
            update={update}
            apiCallInProgress={apiCallInProgress}
            setApiCallInProgress={setApiCallInProgress}
          />
        );
      case 3:
        return (
          <ImportFileData
            currentState={prospectState}
            setProspectState={setProspectState}
            nextStep={nextStep}
            prevStep={prevStep}
            saveInfoImport={saveInfoImport}
          />
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(prospectState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Clientes Varios
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default Prospects;
