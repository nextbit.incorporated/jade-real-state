import React, { useState, useEffect } from "react";
import "react-datepicker/dist/react-datepicker.css";
import { toast } from "react-toastify";

import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Table,
  Row,
  ModalFooter,
  ModalHeader,
  Modal,
  ModalBody,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";
import API from "./../API/API";
import { Redirect } from "react-router-dom";
import checkmark from "../../../src/assets/img/brand/checkmark.svg";
import { generateExelFile } from "./../Constants/generateExelFile";
import readXlsxFile from "read-excel-file";
import Files from "react-files";

const ImportFileData = (props) => {
  const {
    currentState,
    setProspectState,
    prevStep,
    nextStep,
    saveInfoImport,
    className,
  } = props;
  const [value, setValue] = useState("");
  const [valueContacType, setValueContactType] = useState("");
  const [projectSelected, setprojectSelected] = useState([]);
  const [projects, setProjects] = useState([]);
  const [contactTypeSelected, setContactTypeSelected] = useState([]);
  const [contactTypes, setContactTypes] = useState([]);
  const [fileRows, setFileRows] = useState([]);
  const [fileInfo, setFileInfo] = useState([]);

  const selectProject = (project) => {
    setprojectSelected(project);
    setProspectState({
      ...currentState,
      state: {
        large: !currentState.state.large,
      },
    });
  };

  const selectContactType = (contact) => {
    setContactTypeSelected(contact);
    setProspectState({
      ...currentState,
      state: {
        large: !currentState.state.large1,
      },
    });
  };

  const handleCleanValues = () => {
    const initialClientState = {
      step: 1,
      editMode: "None",
      startDate: Date.now(),
      currentProspect: {
        id: "",
        registerDate: "",
        projectId: "",
        contactType: "",
        name: "",
        contactNumberOne: "",
        contactNumberTwo: "",
        email: "",
        projectName: "",
        contactTypeName: "",
        project: [],
        contactTypes: [],
      },
      state: {
        modal: false,
        large: false,
        large1: false,
        small: false,
        primary: false,
        success: false,
        warning: false,
        danger: false,
        info: false,
        uploadFile: false,
        className: null,
      },
    };
    setProspectState(initialClientState);
  };

  useEffect(() => {
    if (
      currentState.currentProspect &&
      currentState.currentProspect.projectId
    ) {
      fetchProjects(currentState.currentProspect.projectName, 0);
    }
    if (
      currentState.currentProspect &&
      currentState.currentProspect.contactType
    ) {
      fetchContactTypes(currentState.currentProspect.contactTypeName, 0);
    }
  }, []);

  function fetchProjects(query, page) {
    const index = page === undefined || page === null ? 0 : page;
    if (query === null) {
      var url = `projects/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setProjects(res.data);
        })
        .catch((error) => {
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `projects/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setProjects(res.data);
        if (currentState.editMode === "Editing") {
          const ps = res.data.projects.find(
            (x) => x.id === currentState.currentProspect.projectId
          );
          setprojectSelected(ps);
        }
      });
    }
  }

  function fetchContactTypes(query, step) {
    const index = step === undefined || step === null ? 0 : step;
    if (query === null) {
      var url = `contact/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setContactTypes(res.data);
        })
        .catch((error) => {
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `contact/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setContactTypes(res.data);
        if (currentState.editMode === "Editing") {
          const ps = res.data.contactTypes.find(
            (x) => x.id === currentState.currentProspect.contactType
          );
          setContactTypeSelected(ps);
        }
      });
    }
  }

  const togglePrimaryXLs = () => {
    if (
      contactTypeSelected.id === null ||
      contactTypeSelected.id === undefined ||
      projectSelected.id === null ||
      projectSelected.id === undefined
    ) {
      toast("Debe seleccionar Proyecto y Tipo de contacto");
      return;
    }

    setProspectState({
      ...currentState,
      state: {
        uploadFile: !currentState.state.uploadFile,
      },
    });
  };

  function handleValueChange(e) {
    fetchProjects(e.target.value);
    setValue(e.target.value);
  }

  function handleValueChangeContactType(e) {
    fetchContactTypes(e.target.value);
    setValueContactType(e.target.value);
  }
  function saveToInfoFile() {
    const items = [];
    fileRows.forEach((row) => {
      const item = {};
      item.identidad = row[0] || "";
      item.email = row[1] || "";
      item.nombre = row[2] || "";
      item.telefono = row[3] || "";
      items.push(item);
    });
    setProspectState({
      ...currentState,
      state: {
        uploadFile: !currentState.state.uploadFile,
      },
    });
    saveInfoImportXlx(items);
  }

  function saveInfoImportXlx(items) {
    if (!items) {
      return;
    }
    saveInfoImport(items, projectSelected.id, contactTypeSelected.id);
  }

  const togglePrimary = () => {
    setProspectState({
      ...currentState,
      state: {
        large: !currentState.state.large,
      },
    });
    fetchProjects(null, 0);
  };

  const toggleContactType = () => {
    setProspectState({
      ...currentState,
      state: {
        large1: !currentState.state.large1,
      },
    });
    fetchContactTypes(null, 0);
  };

  function onFilesChange(files) {
    setFileInfo(files[0]);
    readXlsxFile(files[0]).then((rows) => {
      setInfoXLS(rows);
    });
  }

  const setInfoXLS = (rows) => {
    setFileRows(rows);
  };

  function onFilesError(error, file) {}

  function downloadXLSX(step) {
    const filename = "Formato_ingreso_clientes_Potenciales.xlsx";
    const formatColumns = ["Identidad", "Email", "Nombre", "Telefono"];
    generateExelFile(filename, formatColumns);
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  return (
    <div className="animated fadeIn">
      <Card>
        <CardHeader>
          <strong> Datos a editar del Cliente Varios</strong>
        </CardHeader>
        <CardBody>
          <Row>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="lastName">Proyecto</Label>
                <Input
                  type="text"
                  id="projectId"
                  name="projectId"
                  value={projectSelected.name}
                  // onChange={handleInputChange}
                  required
                  disabled={true}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <Row>
                <Label htmlFor="Segundo Nombre"> Lista Proyectos </Label>
              </Row>
              <Row>
                <Button color="primary" onClick={togglePrimary}>
                  Seleccionar Proyecto
                </Button>
                <Modal
                  isOpen={currentState.state.large}
                  toggle={togglePrimary}
                  className={"modal-lg" + className}
                >
                  <ModalHeader toggle={togglePrimary}>Modal title</ModalHeader>
                  <ModalBody>
                    <FormGroup row>
                      <Col md="12">
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary">
                              <i className="fa fa-search" /> Buscar
                            </Button>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            id="input1-group2"
                            name="input1-group2"
                            placeholder="Buscar"
                            value={value}
                            onChange={handleValueChange}
                          />
                        </InputGroup>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="12">
                        {projects === null ? (
                          <tr>
                            <td colSpan={16}>Buscando...</td>
                          </tr>
                        ) : projects.length === 0 ? (
                          <tr>
                            <td colSpan={16}>No se encontraron proyectos.</td>
                          </tr>
                        ) : (
                          <Table hover bordered striped responsive size="sm">
                            <thead>
                              <tr>
                                <th style={thStyle}>Seleccionar</th>
                                <th style={thStyle}>Nombre</th>
                                <th style={thStyle}>Pais</th>
                                <th style={thStyle}>Departamento</th>
                                <th style={thStyle}>Ciudad</th>
                                <th style={thStyle}>Direccion</th>
                              </tr>
                            </thead>
                            <tbody>
                              {projects.projects.map((project) => (
                                <tr key={project.id}>
                                  <td>
                                    <Button
                                      onClick={() => {
                                        selectProject(project);
                                      }}
                                    >
                                      <img
                                        src={checkmark}
                                        height="100%"
                                        width="100%"
                                        alt="checkmark"
                                      />
                                    </Button>
                                  </td>
                                  <td>{project.name}</td>
                                  <td>{project.country}</td>
                                  <td>{project.state}</td>
                                  <td>{project.city}</td>
                                  <td>{project.address}</td>
                                </tr>
                              ))}
                            </tbody>
                          </Table>
                        )}
                      </Col>
                      <Col md="12">
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <Button
                              type="button"
                              color="primary"
                              onClick={prevStep}
                            >
                              <i className="icon-arrow-left-circle" /> Anterior
                            </Button>
                          </InputGroupAddon>
                          <div
                            className="animated fadeIn"
                            style={{
                              marginTop: "7px",
                              marginLeft: "8px",
                              marginRight: "8px",
                            }}
                          >
                            <h5>
                              <strong>Paginas: {projects.pageCount}</strong>
                            </h5>
                          </div>
                          <div
                            className="animated fadeIn"
                            style={{
                              marginTop: "7px",
                              marginLeft: "8px",
                              marginRight: "8px",
                            }}
                          >
                            <h5>
                              <strong>Actual: {projects.pageIndex}</strong>
                            </h5>
                          </div>
                          <InputGroupAddon addonType="prepend">
                            <Button
                              type="button"
                              color="primary"
                              onClick={nextStep}
                            >
                              Proximo <i className="icon-arrow-right-circle" />
                            </Button>
                          </InputGroupAddon>
                        </InputGroup>
                      </Col>
                    </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="secondary" onClick={togglePrimary}>
                      Cancel
                    </Button>
                  </ModalFooter>
                </Modal>
              </Row>
            </Col>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="lastName1">Tipo de contacto</Label>
                <Input
                  type="text"
                  id="contactType"
                  name="contactType"
                  value={contactTypeSelected.name}
                  // onChange={handleInputChange}
                  disabled={true}
                  required
                />
              </FormGroup>
            </Col>
            <Col md="3" sm="6" xs="12">
              <Row>
                <Label htmlFor="Segundo Nombre"> Tipos de Contacto </Label>
              </Row>
              <Row>
                <Button color="primary" onClick={toggleContactType}>
                  Seleccionar Contacto
                </Button>
                <Modal
                  isOpen={currentState.state.large1}
                  toggle={toggleContactType}
                  className={"modal-lg-1" + className}
                >
                  <ModalHeader toggle={toggleContactType}>
                    Modal title
                  </ModalHeader>
                  <ModalBody>
                    <FormGroup row>
                      <Col md="12">
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary">
                              <i className="fa fa-search" /> Buscar
                            </Button>
                          </InputGroupAddon>
                          <Input
                            type="text"
                            id="input1-group2"
                            name="input1-group2"
                            placeholder="Buscar"
                            value={valueContacType}
                            onChange={handleValueChangeContactType}
                          />
                        </InputGroup>
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col md="12">
                        {contactTypes === null ? (
                          <tr>
                            <td colSpan={16}>Buscando...</td>
                          </tr>
                        ) : contactTypes.length === 0 ? (
                          <tr>
                            <td colSpan={16}>
                              No se encontraron tipos de contacto.
                            </td>
                          </tr>
                        ) : (
                          <Table hover bordered striped responsive size="sm">
                            <thead>
                              <tr>
                                <th style={thStyle}>Seleccionar</th>
                                <th style={thStyle}>Nombre</th>
                              </tr>
                            </thead>
                            <tbody>
                              {contactTypes.contactTypes.map((contact) => (
                                <tr key={contact.id}>
                                  <td>
                                    <Button
                                      onClick={() => {
                                        selectContactType(contact);
                                      }}
                                    >
                                      <img
                                        src={checkmark}
                                        height="100%"
                                        width="100%"
                                        alt="checkmark"
                                      />
                                    </Button>
                                  </td>
                                  <td>{contact.name}</td>
                                </tr>
                              ))}
                            </tbody>
                          </Table>
                        )}
                      </Col>
                      <Col md="12">
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <Button
                              type="button"
                              color="primary"
                              onClick={prevStep}
                            >
                              <i className="icon-arrow-left-circle" /> Anterior
                            </Button>
                          </InputGroupAddon>
                          <div
                            className="animated fadeIn"
                            style={{
                              marginTop: "7px",
                              marginLeft: "8px",
                              marginRight: "8px",
                            }}
                          >
                            <h5>
                              <strong>Paginas: {contactTypes.pageCount}</strong>
                            </h5>
                          </div>
                          <div
                            className="animated fadeIn"
                            style={{
                              marginTop: "7px",
                              marginLeft: "8px",
                              marginRight: "8px",
                            }}
                          >
                            <h5>
                              <strong>Actual: {contactTypes.pageIndex}</strong>
                            </h5>
                          </div>
                          <InputGroupAddon addonType="prepend">
                            <Button
                              type="button"
                              color="primary"
                              onClick={nextStep}
                            >
                              Proximo <i className="icon-arrow-right-circle" />
                            </Button>
                          </InputGroupAddon>
                        </InputGroup>
                      </Col>
                    </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="secondary" onClick={toggleContactType}>
                      Cancel
                    </Button>
                  </ModalFooter>
                </Modal>
              </Row>
            </Col>
          </Row>
          <Row>
            <FormGroup row>
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => downloadXLSX()}
                >
                  <i className="fa fa-new" /> Descargar Formato
                </Button>
              </Col>
              <Col>
                <Col md="3" sm="6" xs="12">
                  <Row>
                    <Button color="primary" onClick={togglePrimaryXLs}>
                      Importar archivo
                    </Button>
                    <Modal
                      isOpen={currentState.state.uploadFile}
                      toggle={togglePrimaryXLs}
                      className={"modal-lg" + currentState.state.className}
                    >
                      <ModalHeader toggle={togglePrimaryXLs}>
                        Seleccionar arcchivo
                      </ModalHeader>
                      <ModalBody>
                        <Row>
                          <CardBody
                            style={{ background: "lightgray", margin: "20px" }}
                          >
                            <Files
                              style={{ margin: "20px", marginLeft: "30px" }}
                              className="files-dropzone"
                              onChange={onFilesChange}
                              onError={onFilesError}
                              accepts={[".xlsx", ".xls"]}
                              multiple
                              maxFiles={1}
                              maxFileSize={10000000}
                              minFileSize={0}
                              clickable
                            >
                              {" "}
                              Arrastra archivo o click para seleccionar archivo
                            </Files>
                          </CardBody>
                        </Row>
                        <Row>
                          <Col style={{ margin: "20px", marginLeft: "30px" }}>
                            <FormGroup row>
                              <Label htmlFor="Id">Nombre Archvivo</Label>
                              <Input
                                type="textarea"
                                name="name"
                                id="name"
                                multiline={true}
                                readOnly
                                disabled={true}
                                value={fileInfo.name}
                              />
                            </FormGroup>
                          </Col>
                          <Col
                            style={{
                              margin: "25px",
                              marginLeft: "30px",
                              marginTop: "60px",
                            }}
                          >
                            <Button color="primary" onClick={saveToInfoFile}>
                              procesar archivo
                            </Button>
                          </Col>
                        </Row>
                      </ModalBody>
                    </Modal>
                  </Row>
                </Col>
              </Col>
            </FormGroup>
          </Row>
        </CardBody>
        <CardFooter>
          <Button
            type="reset"
            size="sm"
            color="danger"
            onClick={handleCleanValues}
          >
            <i className="fa fa-ban" /> Cancelar
          </Button>
        </CardFooter>
      </Card>
    </div>
  );
};

export default ImportFileData;
