import React, { useState } from "react";
import moment from "moment";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
  ModalFooter,
  Modal,
  ModalBody,
  Row,
  CardBody,
} from "reactstrap";

// const step = 1;

const ProspectsControlTable = (props) => {
  const {
    currentState,
    setProspectState,
    fetchProspects,
    prospects,
    nextPage,
    prevPage,
    editRow,
    deleteRegister,
    MoveRegisterToClients,
    className,
    filters,
    setFilterSelected,
    filterSelected,
  } = props;
  const [value, setValue] = useState("");
  const [idMove, setIdMove] = useState(0);
  const [step, setStep] = useState(1);

  function nextStep() {
    setStep(step + 1);
    nextPage(step + 1);
  }

  function prevStep() {
    if (step > 1) {
      setStep(step - 1);
    }

    prevPage(step - 1);
  }

  function handleValueChange(e) {
    fetchProspects(e.target.value, 0, filterSelected);
    setValue(e.target.value);
  }

  const togglePrimaryProspect = (id) => {
    setProspectState({
      ...currentState,
      state: {
        info: !currentState.state.info,
      },
    });
    setIdMove(id);
  };

  const MoveProspectToClient = (id) => {
    MoveRegisterToClients(idMove);
  };

  const filterOptions = filters.map((p) => {
    return (
      <option id={p.filterId} key={p.filterId} value={p.filterId}>
        {p.filter}
      </option>
    );
  });

  const handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;

    setFilterSelected(value);
  };

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const userRows =
    prospects === null ? (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>Buscando...</td>
          </tr>
        </thead>
      </Table>
    ) : prospects.length === 0 ? (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>
              No se encontraron clientes con el filtro especificado.
            </td>
          </tr>
        </thead>
      </Table>
    ) : (
      <div>
        <FormGroup row>
          <Col md="3" sm="6" xs="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar Por:
                </Button>
              </InputGroupAddon>
              <Input
                type="select"
                name="filterId"
                value={filterSelected}
                onChange={handleInputChange}
              >
                {filterOptions}
              </Input>
            </InputGroup>
          </Col>
          <Col md="3" sm="6" xs="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th style={thStyle}>Editar</th>
                  <th style={thStyle}>Borrar</th>
                  <th style={thStyle}>Convertir a cliente</th>
                  <th style={thStyle}>ID</th>

                  <th style={thStyle}>Fecha de Creacion</th>
                  <th style={thStyle}>Fecha de Ingreso</th>

                  <th style={thStyle}>Proyecto</th>
                  <th style={thStyle}>Tipo de contacto</th>
                  <th style={thStyle}>Nombre</th>
                  <th style={thStyle}>Identidad</th>
                  <th style={thStyle}>Contacto #1</th>
                  <th style={thStyle}>Contacto #2</th>
                  <th style={thStyle}>Email</th>
                  <th style={thStyle}>Tipo de registro</th>
                  <th style={thStyle}>Asesor de Ventas</th>
                  <th style={thStyle}>Comentarios</th>
                </tr>
              </thead>
              <tbody>
                {prospects.prospects.map((prospect) => (
                  <tr key={prospect.id}>
                    <td>
                      <Button
                        block
                        color="warning"
                        onClick={() => {
                          editRow(prospect);
                        }}
                      >
                        Editar
                      </Button>
                    </td>
                    <td>
                      <Button
                        block
                        color="danger"
                        onClick={() => deleteRegister(prospect.id)}
                      >
                        Borrar
                      </Button>
                    </td>
                    <td>
                      {/* <Button block color="success" onClick={() => MoveRegisterToClients(prospect.id)}>Promover</Button> */}

                      <Button
                        color="primary"
                        onClick={() => togglePrimaryProspect(prospect.id)}
                      >
                        Promover
                      </Button>
                      <Modal
                        isOpen={currentState.state.info}
                        toggle={togglePrimaryProspect}
                        className={"modal-lg" + className}
                      >
                        <ModalBody>
                          <Row>
                            <CardBody>
                              <p>
                                Esta seguro que desea promover el Cliente Varios
                                a la seccion de clientes?
                              </p>
                            </CardBody>
                          </Row>
                        </ModalBody>
                        <ModalFooter>
                          <Col>
                            <Button
                              color="primary"
                              onClick={() => MoveProspectToClient(prospect.id)}
                            >
                              Promover
                            </Button>
                          </Col>
                          <Col>
                            <Button
                              color="danger"
                              onClick={togglePrimaryProspect}
                            >
                              Cancelar
                            </Button>
                          </Col>
                        </ModalFooter>
                      </Modal>
                    </td>
                    <td>{prospect.id}</td>

                    <td nowrap="true">
                      {moment(prospect.creationDate).format("L")}
                    </td>
                    <td nowrap="true">
                      {moment(prospect.registerDate).format("L")}
                    </td>

                    <td nowrap="true">{prospect.projectName}</td>
                    <td nowrap="true">{prospect.contactTypeName}</td>
                    <td nowrap="true">{prospect.name}</td>
                    <td nowrap="true">{prospect.idNumber}</td>
                    <td nowrap="true">{prospect.contactNumberOne}</td>
                    <td nowrap="true">{prospect.contactNumberTwo}</td>
                    <td nowrap="true">{prospect.Email}</td>
                    <td nowrap="true">{prospect.transactionOrigin}</td>

                    <td nowrap="true">{prospect.salesAdvisor}</td>
                    <td nowrap="true">{prospect.comments}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStep}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {prospects.pageCount}</strong>
                </h5>
              </div>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Actual: {prospects.pageIndex}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStep}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      </div>
    );
  return userRows;
};

export default ProspectsControlTable;
