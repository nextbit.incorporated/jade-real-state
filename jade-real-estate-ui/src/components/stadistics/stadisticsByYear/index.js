import React, { useState, useEffect } from "react";
import { 
    Card, 
    CardBody, 
    CardHeader,
    Col,
    FormGroup,
    InputGroup,
    InputGroupAddon,
    Button,
    Input,
    Row,
    Label,
    Table,
    Collapse,
    Badge,
    Modal,
    ModalHeader } from "reactstrap";
import API from "./../../API/API";
import { toast, toastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import styled from "styled-components";



const initialState = {
    step: 1,
    editMode: "None",
    saveStatus: false,
    currentData: {
        usuario: "",
        anio: "",
        clasificacion: "",
        mes1: 46,
        mes2: 48,
        mes3: 28,
        mes4: 18,
        mes5: 0,
        mes6: 0,
        mes7: 0,
        mes8: 0,
        mes9: 0,
        mes10: 0,
        mes11: 0,
        mes12: 0
      },
   
  };

  const opcionAnios = [2021,2022,2023,2024,2025,2026];
  const opcionesMese = 
  [
       {
        mes: 1,
        nombre: "Enero"    
       },
       {
        mes: 2,
        nombre: "Febrero"    
       },
       {
        mes: 3,
        nombre: "Marzo"    
       },
       {
        mes: 4,
        nombre: "Abril"    
       },
       {
        mes: 5,
        nombre: "Mayo"    
       },
       {
        mes: 6,
        nombre: "Junio"    
       },
       {
        mes: 7,
        nombre: "Julio"    
       },
       {
        mes: 8,
        nombre: "Agosto"    
       },
       {
        mes: 9,
        nombre: "Septiembre"    
       },
       {
        mes: 10,
        nombre: "Octubre"    
       },
       {
        mes: 11,
        nombre: "Noviembre"    
       },
       {
        mes: 12,
        nombre: "Diciembre"    
       },
]

  const tableStyle = {
    height: "10vh",
    maxHeight: "50vh",
  };

  
  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const heightStyle = {
    height: "300px",
};


const StadisticsByYear = (props) => {
    const [stadisticsMainState, setStadisticsMainState] = useState(initialState);
    const [stadisticsState, setStadisticsState] = useState([]);
    const [yearOptionsState, setYearOptionsState] = useState(ObtenerListaAnios());
    const [monthOptionsState, setMonthOptionsState] = useState(opcionesMese);
    const [yearSelectedState, setYearSelectedState] = useState(2022);
    const [yearSaleSAdvisorSelectedState, setYearSaleSAdvisorSelectedState] = useState(2022);
    const [yearByWeekSelectedState, setYearByWeeSelectedState] = useState(2022);
    const [monthByWeekSelectedState, setMonthByWeeSelectedState] = useState(1);
    const [agenteVentasState, setAgenteVentasState] = useState("");
    const [salesAdvisorsState, setSalesAdvisorsState] = useState([]);
    const [users, setUsers] = useState([]);
    const [userIdSelected, setUserIdSelected] = useState("");
    const [userSelected, setUserSelected] = useState("");
    const [stadisticsByUserState, setStadisticsByUserState] = useState([]);
    const [stadisticsByWeekMonthState, setStadisticsByWeekMonthState] = useState([]);


function ObtenerListaAnios() {
    var currentTime = new Date();
    var year = currentTime.getFullYear();
    
    var nuevoAnio = year - 4;

    const opcionAnios = [];
    for (let index = 0; index < 6; index++) {
        opcionAnios.push(nuevoAnio);
        nuevoAnio = nuevoAnio + 1  
    }

    return opcionAnios;
}

    useEffect(() => {
        fetchUsersReport();
        fetchStadisticsByYear(yearSelectedState);
      }, []);
    
      function compareNames(currentItem, nextItem) {
        const currentName = currentItem.name;
        const nextName = nextItem.name;
    
        let comparison = 0;
        if (currentName > nextName) {
          comparison = 1;
        } else if (currentName < nextName) {
          comparison = -1;
        }
        return comparison;
      }

      function fetchUsersReport() {
        const user = sessionStorage.getItem("userId");
        const url = `clients/users-report?request.user=${user}`;
    
        API.get(url).then((res) => {
          if (res.data === null || res.data === undefined) {
            return;
          }
          setUsers(res.data);
          if (res.data.length === 1) {
            setUserId(res.data[0].userId);
            setUserName(res.data[0].firstName);
          }
        });
      }

      const [userId, setUserId] = useState("");
      const [userName, setUserName] = useState("");

      function fetchStadisticsByYear() {
           var url = `operations/categorias-clientes?anio=${yearSelectedState}`

                API.get(url)
                .then((res) => {
                    setStadisticsState(res.data);
                })
                .catch((error) => {
                    if (error.message === "Network Error") {
                        toast.warn(
                            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                        );
                    } else {
                        if (error.response.status !== undefined) {
                            if (error.response.status === 401) {
                                props.history.push("/login");
                                return <Redirect to="/login" />;
                            }
                        } else {
                            toast.warn(
                                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                            );
                        }
                    }
                });
            }

            function fetchStadisticsByYearByUser() {
                var url = `operations/categorias-clientes-por-agente?Anio=${yearSaleSAdvisorSelectedState}&User=${userIdSelected}`
     
                     API.get(url)
                     .then((res) => {
                        setStadisticsByUserState(res.data);
                     })
                     .catch((error) => {
                         if (error.message === "Network Error") {
                             toast.warn(
                                 "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                             );
                         } else {
                             if (error.response.status !== undefined) {
                                 if (error.response.status === 401) {
                                     props.history.push("/login");
                                     return <Redirect to="/login" />;
                                 }
                             } else {
                                 toast.warn(
                                     "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                                 );
                             }
                         }
                     });
                 }

                 function fetchStadisticsByMonth() {
                    var url = `operations/categorias-clientes-por-semana?Anio=${yearByWeekSelectedState}&Mes=${monthByWeekSelectedState}`
         
                         API.get(url)
                         .then((res) => {
                            setStadisticsByWeekMonthState(res.data);
                         })
                         .catch((error) => {
                             if (error.message === "Network Error") {
                                 toast.warn(
                                     "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                                 );
                             } else {
                                 if (error.response.status !== undefined) {
                                     if (error.response.status === 401) {
                                         props.history.push("/login");
                                         return <Redirect to="/login" />;
                                     }
                                 } else {
                                     toast.warn(
                                         "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                                     );
                                 }
                             }
                         });
                     }
    

      const yearOptions = yearOptionsState.map((p) => {
        return (
          <option
            id={p}
            key={p}
            value={p}
            name={p}
          >
            {p}
          </option>
        );
      });

      const monthOptions = monthOptionsState.map((p) => {
        return (
          <option
            id={p.mes}
            key={p.mes}
            value={p.mes}
            name={p.mes}
          >
            {p.nombre}
          </option>
        );
      });

      
      const handleInputChangeYear = (event) => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
    
        setYearSelectedState(value);

      };

      const handleInputChangeYearSalesAdvisor = (event) => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
    
        setYearSaleSAdvisorSelectedState(value);

      };

      const handleInputChangeMonth = (event) => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
    
        setMonthByWeeSelectedState(value);

      };

      const handleInputChangeYearByMonth = (event) => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
    
        setYearByWeeSelectedState(value);

      };

      const Root = styled.div`
      height: 30vh;
      max-height: 30vh;
      overflow-y: auto;
      overflow-x: auto;
    `;
  
    function  ObtenerDetalleByMonth() {
        return(
            <FormGroup row>
            <Col xs="12">
            <Root>
                <Table
                style={tableStyle}
                hover
                bordered
                striped
                size="sm"
            > 
               <thead>
                    <tr>
                    <th nowrap="true" style={thStyle}>
                        Semana
                      </th>
                      <th nowrap="true" style={thStyle}>
                        Nivel A
                      </th>
                      <th nowrap="true" style={thStyle}>
                        Nivel B
                      </th>
                      <th nowrap="true" style={thStyle}>
                        Nivel C
                      </th> 
                      <th nowrap="true" style={thStyle}>
                        Nivel D 
                      </th>
                      <th nowrap="true" style={thStyle}>
                        N/A
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                        {stadisticsByWeekMonthState.map((item) => (
                             <tr key={item.anio}>
                                <td nowrap="true">Semana No: { item.week} del año</td>
                               <td nowrap="true">{item.levelA}</td>
                               <td nowrap="true">{item.levelB}</td>
                               <td nowrap="true">{item.levelC}</td>
                                 <td nowrap="true">{item.levelD}</td>
                                 <td nowrap="true">{item.levelNA}</td>  
                                 
                             </tr>
                        ))}
                    </tbody>
                  
            </Table>
            </Root>
            </Col>
            </FormGroup>
           )
    }  


    function  ObtenerDetalleByUser() {
        return(
            <FormGroup row>
            <Col xs="12">
            <Root>
                <Table
                style={tableStyle}
                hover
                bordered
                striped
                size="sm"
            > 
               <thead>
                    <tr>
                    <th nowrap="true" style={thStyle}>
                        Categoria
                      </th>
                      <th nowrap="true" style={thStyle}>
                        Enero
                      </th>
                      <th nowrap="true" style={thStyle}>
                        Febrero
                      </th>
                      <th nowrap="true" style={thStyle}>
                        Marzo
                      </th> 
                      <th nowrap="true" style={thStyle}>
                        Abril 
                      </th>
                      <th nowrap="true" style={thStyle}>
                        Mayo
                      </th>
                      <th nowrap="true" style={thStyle}>
                        Junio
                      </th>   
                      <th nowrap="true" style={thStyle}>
                        Julio
                      </th>                        
                      <th nowrap="true" style={thStyle}>
                        Agosto
                      </th>             
                      <th nowrap="true" style={thStyle}>
                        Septiembre
                      </th>
                      <th nowrap="true" style={thStyle}>
                        Octubre
                      </th>
                      <th nowrap="true" style={thStyle}>
                        Noviembre
                      </th>
                      <th nowrap="true" style={thStyle}>
                        Diciembre
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                        {stadisticsByUserState.map((item) => (
                             <tr key={item.anio}>
                                <td nowrap="true">{item.clasificacion}</td>
                               <td nowrap="true">{item.mes1}</td>
                               <td nowrap="true">{item.mes2}</td>
                               <td nowrap="true">{item.mes3}</td>
                                 <td nowrap="true">{item.mes4}</td>
                                 <td nowrap="true">{item.mes5}</td>  
                                 <td nowrap="true">{item.mes6}</td>                      
                                 <td nowrap="true">{item.mes7}</td>
                                 <td nowrap="true">{item.mes8}</td>
                                 <td nowrap="true">{item.mes9}</td>
                                 <td nowrap="true">{item.mes10}</td>
                                 <td nowrap="true">{item.mes11}</td>
                                 <td nowrap="true">{item.mes12}</td>
                             </tr>
                        ))}
                    </tbody>
                  
            </Table>
            </Root>
            </Col>
            </FormGroup>
           )
    }  

    function  ObtenerDetalle() {       
        return(
         <FormGroup row>
         <Col xs="12">
         <Root>
             <Table
             style={tableStyle}
             hover
             bordered
             striped
             size="sm"
         > 
            <thead>
                 <tr>
                 <th nowrap="true" style={thStyle}>
                     Categoria
                   </th>
                   <th nowrap="true" style={thStyle}>
                     Enero
                   </th>
                   <th nowrap="true" style={thStyle}>
                     Febrero
                   </th>
                   <th nowrap="true" style={thStyle}>
                     Marzo
                   </th> 
                   <th nowrap="true" style={thStyle}>
                     Abril 
                   </th>
                   <th nowrap="true" style={thStyle}>
                     Mayo
                   </th>
                   <th nowrap="true" style={thStyle}>
                     Junio
                   </th>   
                   <th nowrap="true" style={thStyle}>
                     Julio
                   </th>                        
                   <th nowrap="true" style={thStyle}>
                     Agosto
                   </th>             
                   <th nowrap="true" style={thStyle}>
                     Septiembre
                   </th>
                   <th nowrap="true" style={thStyle}>
                     Octubre
                   </th>
                   <th nowrap="true" style={thStyle}>
                     Noviembre
                   </th>
                   <th nowrap="true" style={thStyle}>
                     Diciembre
                   </th>
                 </tr>
                 </thead>
                 <tbody>
                     {stadisticsState.map((item) => (
                          <tr key={item.anio}>
                             <td nowrap="true">{item.clasificacion}</td>
                            <td nowrap="true">{item.mes1}</td>
                            <td nowrap="true">{item.mes2}</td>
                            <td nowrap="true">{item.mes3}</td>
                              <td nowrap="true">{item.mes4}</td>
                              <td nowrap="true">{item.mes5}</td>  
                              <td nowrap="true">{item.mes6}</td>                      
                              <td nowrap="true">{item.mes7}</td>
                              <td nowrap="true">{item.mes8}</td>
                              <td nowrap="true">{item.mes9}</td>
                              <td nowrap="true">{item.mes10}</td>
                              <td nowrap="true">{item.mes11}</td>
                              <td nowrap="true">{item.mes12}</td>
                          </tr>
                     ))}
                 </tbody>
               
         </Table>
         </Root>
         </Col>
         </FormGroup>
        )
       }

       function CleanDataReport(){
        setStadisticsState([]);
    };

    function CleanDataReportByUser(){
        setStadisticsByUserState([]);
    };

    function CleanDataReportByMonth(){
        setStadisticsByWeekMonthState([]);
    };
    const handleInputChangeUser = (event) => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
    
        setUserIdSelected(value);
    
        setUserSelected();
      };

      const usersOptions = users.map((p) => {
        return (
          <option
            id={p.userId}
            key={p.userId}
            value={p.userId}
            name={p.firstName + " " + p.lastName}
          >
            {p.firstName + " " + p.lastName}
          </option>
        );
      });
    

    return (
        <div className="animated fadeIn">
            <div  style={heightStyle}>
                <Card>
                    <CardHeader>
                     <i className="fa fa-align-justify"></i> Categorias en año {yearSelectedState} <small>Detalle por meses</small>
                    </CardHeader>
                    <CardBody>
                      <FormGroup row>
                       <Col md="3" sm="6" xs="12">
                            <FormGroup>
                            <Label>Año</Label>
                            <Input
                                style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
                                type="select"
                                name="year"
                                value={yearSelectedState}
                                onChange={handleInputChangeYear}
                            >
                                {yearOptions}
                            </Input>
                            </FormGroup>
                        </Col>
                        <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
                        <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={fetchStadisticsByYear}>
                            Generar <i className="icon-arrow-right-circle" />
                            </Button>
                        </InputGroupAddon>
                        </InputGroup>
                    </Col>

                    <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
                        <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={CleanDataReport}>
                            Limpiar <i className="icon-arrow-right-circle" />
                            </Button>
                        </InputGroupAddon>
                        </InputGroup>
                    </Col>
                      </FormGroup>
                      {ObtenerDetalle()}
                    </CardBody>
                </Card>
            </div>
          
            <div  style={heightStyle}>
                <Card>
                    <CardHeader>
                     <i className="fa fa-align-justify"></i> Categorias por agente {agenteVentasState} <small>Detalle por meses</small>
                    </CardHeader>
                    <CardBody>
                      <FormGroup row>
                       <Col md="3" sm="6" xs="12">
                            <FormGroup>
                            <Label>Año</Label>
                            <Input
                                style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
                                type="select"
                                name="year"
                                value={yearSaleSAdvisorSelectedState}
                                onChange={handleInputChangeYearSalesAdvisor}
                            >
                                {yearOptions}
                            </Input>
                            </FormGroup>
                        </Col>
                        <Col md="3" sm="6" xs="12">
                        <FormGroup>
                        <Label>Agentes</Label>
                        <Input
                            style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
                            type="select"
                            name="contactTypeId"
                            value={userIdSelected}
                            onChange={handleInputChangeUser}
                        >
                            {usersOptions}
                        </Input>
                        </FormGroup>
                    </Col>
                        <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
                        <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={fetchStadisticsByYearByUser}>
                            Generar <i className="icon-arrow-right-circle" />
                            </Button>
                        </InputGroupAddon>
                        </InputGroup>
                    </Col>

                    <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
                        <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={CleanDataReportByUser}>
                            Limpiar <i className="icon-arrow-right-circle" />
                            </Button>
                        </InputGroupAddon>
                        </InputGroup>
                    </Col>
                      </FormGroup>
                      {ObtenerDetalleByUser()}
                    </CardBody>
                </Card>
            </div>

            <div  style={heightStyle}>
            <Card>
                    <CardHeader>
                     <i className="fa fa-align-justify"></i> Categorias por semana de mes {} <small>Detalle por semana</small>
                    </CardHeader>
                    <CardBody>
                      <FormGroup row>
                       <Col md="3" sm="6" xs="12">
                            <FormGroup>
                            <Label>Año</Label>
                            <Input
                                style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
                                type="select"
                                name="year"
                                value={yearByWeekSelectedState}
                                onChange={handleInputChangeYearByMonth}
                            >
                                {yearOptions}
                            </Input>
                            </FormGroup>
                        </Col>
                        <Col md="3" sm="6" xs="12">
                            <FormGroup>
                            <Label>Mes</Label>
                            <Input
                                style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
                                type="select"
                                name="year"
                                value={monthByWeekSelectedState}
                                onChange={handleInputChangeMonth}
                            >
                                {monthOptions}
                            </Input>
                            </FormGroup>
                        </Col>
                        <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
                        <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={fetchStadisticsByMonth}>
                            Generar <i className="icon-arrow-right-circle" />
                            </Button>
                        </InputGroupAddon>
                        </InputGroup>
                    </Col>

                    <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
                        <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={CleanDataReportByMonth}>
                            Limpiar <i className="icon-arrow-right-circle" />
                            </Button>
                        </InputGroupAddon>
                        </InputGroup>
                    </Col>
                      </FormGroup>
                      {ObtenerDetalleByMonth()}
                    </CardBody>
                </Card>
            </div>

        </div>
    );
}

export default StadisticsByYear;