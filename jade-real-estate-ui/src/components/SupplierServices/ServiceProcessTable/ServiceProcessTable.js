import React from "react";
import styled from "styled-components";

import { Table, Button } from "reactstrap";

const thStyle = {
  background: "#20a8d8",
  color: "white",
  width: "20px",
  maxWidth: "20px",
  //width: "50%"
};

const TableRow = styled.tr`
  cursor: pointer;
  key: ${(props) => props.key};
  font-weight: ${(props) => (props.selected ? "bold" : "normal")};
  background: ${(props) =>
    props.selected ? "rgba(0, 0, 0, 0.09)" : "rgba(0, 0, 0, 0)"};
`;

// style={{ width: "600px" }}

const ServiceProcessTable = ({
  serviceProcesses,
  editServiceProcess,
  deleteServiceProcess,
  canEditProcess,
}) => {
  return (
    <Table hover bordered responsive size="sm">
      <thead>
        <tr>
          {canEditProcess && <th style={thStyle}>Editar</th>}
          {canEditProcess && <th style={thStyle}>Borrar</th>}

          <th style={thStyle}>Order</th>
          <th style={thStyle}>Nombre</th>
        </tr>
      </thead>

      <tbody>
        {serviceProcesses &&
          serviceProcesses.map((service) => (
            <TableRow key={service.id}>
              {canEditProcess && (
                <td>
                  <Button
                    block
                    color="warning"
                    onClick={() => {
                      editServiceProcess(service);
                    }}
                  >
                    Editar
                  </Button>
                </td>
              )}
              {canEditProcess && (
                <td>
                  <Button
                    block
                    color="danger"
                    onClick={() => deleteServiceProcess(service.id)}
                  >
                    Borrar
                  </Button>
                </td>
              )}
              <td>{service.order}</td>
              <td>{service.name}</td>
            </TableRow>
          ))}
      </tbody>
    </Table>
  );
};

export default ServiceProcessTable;
