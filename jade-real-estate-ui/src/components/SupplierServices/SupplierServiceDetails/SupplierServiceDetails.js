import React, { useState } from "react";
import {
  Form,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";
import ServiceProcessTable from "./../ServiceProcessTable/ServiceProcessTable";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "../../API/API";

const initialProcess = {
  id: 0,
  order: 0,
  name: "",
};

const SupplierServiceDetails = (props) => {
  const {
    currentState,
    setSupplierServiceState,
    addService,
    updateService,
  } = props;

  const [currentProcess, setCurrentProcess] = useState(initialProcess);
  const [modal, setModal] = useState(false);
  const [isEditing, setIsEditing] = useState(false);

  const displayNewProcessDialog = () => {
    setCurrentProcess(initialProcess);
    setIsEditing(false);
    setModal(true);
  };

  const editServiceProcess = (serviceProcess) => {
    setCurrentProcess(serviceProcess);
    setIsEditing(true);
    setModal(true);
  };

  const SaveServiceProcess = () => {
    setModal(false);

    if (isEditing) {
      const updatedProcesses = currentState.currentSupplierService.serviceProcesses.filter(
        (p) => p.id !== currentProcess.id
      );

      updatedProcesses.push(currentProcess);

      const updatedState = {
        ...currentState,
        currentSupplierService: {
          ...currentState.currentSupplierService,
          serviceProcesses: updatedProcesses,
        },
      };
      setSupplierServiceState(updatedState);
    } else {
      const process = {
        order: currentProcess.order,
        supplierServiceId: currentState.currentSupplierService.id,
        name: currentProcess.name,
      };
      const url = `supplierService/add-process`;
      API.post(url, process)
        .then((res) => {
          const updatedProcesses = [
            ...currentState.currentSupplierService.serviceProcesses,
          ];

          const maxServiceProcessId = getMaxServiceProcessId(updatedProcesses);

          const serviceProcess = {
            ...currentProcess,
            id: maxServiceProcessId + 1,
          };

          updatedProcesses.push(serviceProcess);

          const updatedState = {
            ...currentState,
            currentSupplierService: {
              ...currentState.currentSupplierService,
              serviceProcesses: updatedProcesses,
            },
          };

          setSupplierServiceState(updatedState);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast.warn(
              "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
            );
          } else {
            if (error.response.status !== undefined) {
              if (error.response.status === 401) {
                props.history.push("/login");
                return <Redirect to="/login" />;
              }
            } else {
              toast.warn(
                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
              );
            }
          }
        });
    }
  };

  const handleSaveChanges = () => {
    switch (currentState.editMode) {
      case "Editing": {
        updateService(currentState.currentSupplierService);
        break;
      }

      default: {
        const newService = {
          ...currentState.currentSupplierService,
          id: 0,
        };

        addService(newService);
        break;
      }
    }
  };

  function getMaxServiceProcessId(serviceProcesses) {
    return serviceProcesses.length > 0
      ? serviceProcesses.reduce(
          (max, b) => Math.max(max, b.id),
          serviceProcesses[0].id
        )
      : 0;
  }

  const deleteServiceProcess = (id) => {
    const updatedProcesses = currentState.currentSupplierService.serviceProcesses.filter(
      (p) => p.id !== id
    );

    const updatedState = {
      ...currentState,
      currentSupplierService: {
        ...currentState.currentSupplierService,
        serviceProcesses: updatedProcesses,
      },
    };

    setSupplierServiceState(updatedState);
  };

  const toggleModal = () => setModal(!modal);

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setSupplierServiceState({
      ...currentState,
      currentSupplierService: {
        ...currentState.currentSupplierService,
        [name]: value,
      },
    });
  };

  const handleProcessChange = (event) => {
    const { name, value } = event.target;
    setCurrentProcess({
      ...currentProcess,

      [name]: value,
    });
  };

  const handleCleanValues = () => {
    const initialClientState = {
      step: 1,
      editMode: "None",
      currentSupplierService: {
        id: "",
        name: "",
        description: "",
      },
    };
    setSupplierServiceState(initialClientState);
  };

  const providerServiceFormTitle =
    currentState.editMode === "Editing"
      ? "Datos a editar de servicio"
      : "Datos de nuevo servicio";

  const editCurrentProcessForm = (
    <Form>
      <FormGroup>
        <Label for="id">Id</Label>
        <Input
          type="text"
          name="id"
          id="id"
          value={currentProcess.id}
          onChange={handleProcessChange}
          readOnly={true}
        />
      </FormGroup>
      <FormGroup>
        <Label for="exampleText">Orden</Label>
        <Input
          type="number"
          name="order"
          id="order"
          value={currentProcess.order}
          onChange={handleProcessChange}
        />
      </FormGroup>
      <FormGroup>
        <Label for="exampleText">Nombre</Label>
        <Input
          type="text"
          name="name"
          id="anme"
          value={currentProcess.name}
          onChange={handleProcessChange}
        />
      </FormGroup>
    </Form>
  );

  return (
    <div className="animated fadeIn">
      <Card>
        <CardHeader>
          <strong> {providerServiceFormTitle} </strong>
        </CardHeader>
        <CardBody>
          <Form>
            <FormGroup>
              <legend>Informacion General del Servicio:</legend>
            </FormGroup>

            <Row form>
              <Col md={4}>
                <FormGroup>
                  <Label htmlFor="Id">Nombre</Label>
                  <Input
                    autoFocus
                    type="text"
                    name="name"
                    id="name"
                    value={currentState.currentSupplierService.name}
                    onChange={handleInputChange}
                    required
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label htmlFor="Primer Nombre">Descripcion</Label>
                  <Input
                    type="text"
                    id="description"
                    name="description"
                    value={currentState.currentSupplierService.description}
                    onChange={handleInputChange}
                    required
                  />
                </FormGroup>
              </Col>
            </Row>

            <FormGroup>
              <legend>
                Plantilla de Procesos:{" "}
                {currentState.currentSupplierService.name}
              </legend>
            </FormGroup>

            <Row form>
              <Col md={6}>
                <Row form style={{ marginTop: "10px", marginBottom: "10px" }}>
                  <Col></Col>

                  <Col xs="auto">
                    <Button
                      type="button"
                      color="success"
                      onClick={() => displayNewProcessDialog()}
                    >
                      <i className="fa fa-new" /> Agregar Proceso
                    </Button>
                  </Col>
                </Row>
              </Col>

              <Col md={6}></Col>
            </Row>

            <Row form>
              <Col md={6}>
                <FormGroup>
                  <ServiceProcessTable
                    serviceProcesses={
                      currentState.currentSupplierService
                        ? currentState.currentSupplierService.serviceProcesses
                        : []
                    }
                    editServiceProcess={editServiceProcess}
                    deleteServiceProcess={deleteServiceProcess}
                    canEditProcess={true}
                  ></ServiceProcessTable>
                </FormGroup>
              </Col>

              <Col md={6}></Col>
            </Row>
          </Form>
        </CardBody>

        <CardFooter>
          <Button
            type="submit"
            size="sm"
            color="success"
            onClick={handleSaveChanges}
          >
            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
          <Button
            type="reset"
            size="sm"
            color="danger"
            onClick={handleCleanValues}
          >
            <i className="fa fa-ban" /> Cancelar
          </Button>
        </CardFooter>
      </Card>

      <Modal isOpen={modal} toggle={toggleModal} unmountOnClose={false}>
        <ModalHeader toggle={toggleModal}>
          {isEditing ? "Moificacion de Proceso" : "Nuevo Proceso"}
        </ModalHeader>
        <ModalBody>{editCurrentProcessForm}</ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={SaveServiceProcess}>
            Guardar
          </Button>{" "}
          <Button color="secondary" onClick={toggleModal}>
            Cancelar
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default SupplierServiceDetails;
