import React, { useState, useEffect } from "react";
import styled from "styled-components";

import {
  Col,
  Row,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
} from "reactstrap";

import ServiceProcessTable from "./ServiceProcessTable/ServiceProcessTable";

const thStyle = {
  background: "#20a8d8",
  color: "white",
  width: "20px",
  maxWidth: "20px",
};

const TableRow = styled.tr`
  cursor: pointer;
  key: ${(props) => props.key};
  font-weight: ${(props) => (props.selected ? "bold" : "normal")};
  background: ${(props) =>
    props.selected ? "rgba(0, 0, 0, 0.09)" : "rgba(0, 0, 0, 0)"};
`;

const initialService = {
  id: 0,
  name: "",
  serviceProcesses: [],
};

const step = 1;

const SupplierServiceControlTable = (props) => {
  const {
    fetchSupplierServices,
    supplierServices,
    nextPage,
    prevPage,
    editRow,
    deleteRegister,
    editServiceProcess,
    deleteServiceProcess,
  } = props;
  const [value, setValue] = useState("");
  const [services, setServices] = useState([]);
  const [selectedService, setSelectedService] = useState(initialService);

  function nextStep() {
    nextPage(step + 1);
  }

  function prevStep() {
    prevPage(step - 1);
  }

  useEffect(() => {
    const firstService =
      supplierServices &&
      supplierServices.supplierServices &&
      supplierServices.supplierServices.length > 0
        ? supplierServices.supplierServices[0]
        : null;

    setSelectedService(firstService);

    const services = mapSupplierServices(firstService);

    setServices(services);
  }, [supplierServices]);

  function handleValueChange(e) {
    fetchSupplierServices(e.target.value);
    setValue(e.target.value);
  }

  function mapSupplierServices(firstService) {
    const services =
      supplierServices &&
      supplierServices.supplierServices &&
      supplierServices.supplierServices.length > 0
        ? supplierServices.supplierServices.map((ss) => {
            return {
              ...ss,
            };
          })
        : [];

    let indexedServices = {};

    if (services && services.length > 0) {
      services.forEach((s) => {
        indexedServices = {
          ...indexedServices,
          [s.id]: {
            ...s,
          },
        };
      });
    }

    if (firstService) {
      const updatedServices = {
        ...indexedServices,
        [firstService.id]: {
          ...firstService,
          selected: true,
        },
      };

      return updatedServices;
    }

    return indexedServices;
  }

  const selectService = (service) => {
    const originalServices = mapSupplierServices(service);

    const updatedServices = {
      ...originalServices,
      [service.id]: {
        ...service,
        selected: true,
      },
    };

    setServices(updatedServices);

    setSelectedService(service);
  };

  function compareName(currentService, nextService) {
    // Use toUpperCase() to ignore character casing
    const currentServiceName = currentService.name.toUpperCase();
    const nextServiceName = nextService.name.toUpperCase();

    let comparison = 0;
    if (currentServiceName > nextServiceName) {
      comparison = 1;
    } else if (currentServiceName < nextServiceName) {
      comparison = -1;
    }
    return comparison;
  }

  const providerServicesKeys = Object.keys(services);

  const providerServices = (services
    ? providerServicesKeys.map((r) => r && services[r])
    : []
  ).sort(compareName);

  const userRows =
    supplierServices === null ? (
      <Table hover bordered responsive size="sm">
        <thead>
          <tr>
            <th colSpan={16}>Buscando...</th>
          </tr>
        </thead>
      </Table>
    ) : supplierServices.length === 0 ? (
      <Table hover bordered responsive size="sm">
        <thead>
          <tr>
            <th colSpan={16}>
              No se encontraron usuarios con el filtro especificado.
            </th>
          </tr>
        </thead>
      </Table>
    ) : (
      <div>
        <FormGroup row>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="8">
            <Table
              hover
              bordered
              responsive
              size="sm"
              style={{ width: "600px" }}
            >
              <thead>
                <tr>
                  <th style={thStyle}>Editar</th>
                  <th style={thStyle}>Borrar</th>
                  <th style={thStyle}>ID</th>
                  <th style={thStyle}>Nombre</th>
                  <th style={thStyle}>Descripcion</th>
                </tr>
              </thead>
              <tbody>
                {providerServices && providerServices.length > 0
                  ? providerServices.map((service) => (
                      <TableRow
                        key={service.id}
                        onClick={() => selectService(service)}
                        selected={service.selected}
                      >
                        <td>
                          <Button
                            block
                            color="warning"
                            onClick={() => {
                              editRow(service);
                            }}
                          >
                            Editar
                          </Button>
                        </td>
                        <td>
                          <Button
                            block
                            color="danger"
                            onClick={() => deleteRegister(service.id)}
                          >
                            Borrar
                          </Button>
                        </td>
                        <td>{service.id}</td>
                        <td>{service.name}</td>
                        <td>{service.description}</td>
                      </TableRow>
                    ))
                  : []}
              </tbody>
            </Table>
          </Col>
          <Col md="3" sm="6" xs="12">
            <Row>
              <Col>
                Procesos para el servicio:{" "}
                {selectedService ? selectedService.name : ""}
              </Col>
            </Row>

            <Row>
              <ServiceProcessTable
                serviceProcesses={
                  selectedService ? selectedService.serviceProcesses : []
                }
                editServiceProcess={editServiceProcess}
                deleteServiceProcess={deleteServiceProcess}
              ></ServiceProcessTable>
            </Row>
          </Col>
          <Col md="3" sm="6" xs="12"></Col>
        </FormGroup>
        <FormGroup row>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStep}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {supplierServices.pageCount}</strong>
                </h5>
              </div>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Actual: {supplierServices.pageIndex}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStep}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      </div>
    );
  return userRows;
};

export default SupplierServiceControlTable;
