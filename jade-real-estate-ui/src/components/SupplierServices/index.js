import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
} from "reactstrap";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";

import SupplierServiceControlTable from "./SupplierServiceControlTable";
import SupplierServiceDetails from "./SupplierServiceDetails/SupplierServiceDetails";

const heightStyle = {
  height: "500px",
};

const initialClientState = {
  step: 1,
  editMode: "None",
  currentSupplierService: {
    id: "",
    name: "",
    description: "",
    serviceProcesses: [],
  },
};

const SupplierServices = (props) => {
  const [supplierServiceState, setSupplierServiceState] = useState(
    initialClientState
  );
  const [supplierServices, setSupplierService] = useState([]);

  useEffect(() => {
    fetchSupplierServices(null);
  }, []);

  function fetchSupplierServices(query, step) {
    const index = step === undefined || step === null ? 0 : step;
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard"></Redirect>;
    }
    if (query === null) {
      var url = `supplierService/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setSupplierService(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `supplierService/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setSupplierService(res.data);
      });
    }
  }

  const editRow = (supplier) => {
    setSupplierServiceState({
      ...supplierServiceState,
      editMode: "Editing",
      currentSupplierService: supplier,
      step: 2,
    });
  };

  function nextPage(step) {
    fetchSupplierServices(null, step);
  }

  function prevPage(step) {
    fetchSupplierServices(null, step);
  }

  function nextStep(supplier) {
    const { step } = supplierServiceState;

    if (supplier === null || supplier === undefined) {
      supplier = supplierServiceState.currentSupplierService;
    }
    setSupplierServiceState({
      ...supplierServiceState,
      currentSupplierService: supplier,
      step: step + 1,
    });
  }

  function prevStep() {
    const { step } = supplierServiceState;

    setSupplierServiceState({ ...supplierServiceState, step: step - 1 });
  }

  function addService(newSupplierService) {
    if (
      !newSupplierService ||
      !newSupplierService.name ||
      !newSupplierService.description
    ) {
      toast.warn("El nombre y descripcion del  Servicio es requerido.");
      return;
    }
    const url = `supplierService`;

    API.post(url, newSupplierService)

      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar tipo de servicio");
        } else {
          toast("tipo de servicio agregado satisfactoriamente");
          fetchSupplierServices(null);
          setSupplierServiceState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  function updateService(supplierService) {
    if (
      !supplierService ||
      !supplierService.name ||
      !supplierService.description
    ) {
      toast.warn("El nombre y descripcion del  Servicio es requerido.");
      return;
    }

    const url = `supplierService`;
    API.put(url, supplierService)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar tipo de servicio");
        } else {
          toast("servicio agregado satisfactoriamente");

          fetchSupplierServices(null);
          setSupplierServiceState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  const deleteRegister = (id) => {
    const url = `supplierService?Id=${id}`;

    API.delete(url)
      .then((res) => {
        if (res.data !== "") {
          toast("error al intentar eliminar tipo se servicio");
          return;
        }
        fetchSupplierServices(null);
        setSupplierServiceState(initialClientState);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col />
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStep()}
                >
                  <i className="fa fa-new" /> Agregar Nuevo Tipo de Servicio
                </Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <SupplierServiceControlTable
                fetchSupplierServices={fetchSupplierServices}
                supplierServices={supplierServices}
                nextPage={nextPage}
                prevPage={prevPage}
                editRow={editRow}
                deleteRegister={deleteRegister}
              />
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <SupplierServiceDetails
            currentState={supplierServiceState}
            setSupplierServiceState={setSupplierServiceState}
            supplierServices={supplierServices}
            nextStep={nextStep}
            prevStep={prevStep}
            addService={addService}
            updateService={updateService}
            canEditProcess={true}
          />
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(supplierServiceState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Servicios
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default SupplierServices;
