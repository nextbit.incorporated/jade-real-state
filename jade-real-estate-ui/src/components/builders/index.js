import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
} from "reactstrap";

import BuilderControlTable from "./buildersControlTable";
import BuilderDetails from "./builderDetails";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";
const heightStyle = {
  height: "500px",
};

const initialClientState = {
  step: 1,
  editMode: "None",
  currentBuilder: {
    id: "",
    name: "",
    description: "",
    businessName: "",
    contactNumber: "",
    electronicAddresses: "",
    email: "",
    address: "",
    owner: "",
    ownerContactNumber: "",
    idNumber: "",
    status: "",
  },
};

const Builders = (props) => {
  const [builderState, setBuilderState] = useState(initialClientState);
  const [builders, setBuilders] = useState([]);

  function fetchBuilders(query, step) {
    const index = step === undefined || step === null ? 0 : step;
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard"></Redirect>;
    }
    if (query === null || query === undefined) {
      var url = `builderCompany/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setBuilders(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `builderCompany/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setBuilders(res.data);
      });
    }
  }

  const editRow = (builder) => {
    setBuilderState({
      ...builderState,
      editMode: "Editing",
      currentBuilder: builder,
      step: 2,
    });
  };

  function nextPage(step) {
    fetchBuilders(null, step);
  }

  function prevPage(step) {
    fetchBuilders(null, step);
  }

  function nextStep(builder) {
    const { step } = builderState;

    if (builder === null || builder === undefined) {
      builder = builderState.currentBuilder;
    }
    setBuilderState({
      ...builderState,
      currentBuilder: builder,
      step: step + 1,
    });
  }

  function prevStep() {
    const { step } = builderState;

    setBuilderState({ ...builderState, step: step - 1 });
  }

  useEffect(() => {
    fetchBuilders(null);
    // fectRoles();
  }, []);

  function addBuilder(newBuilder) {
    if (!newBuilder || !newBuilder.name) {
      return;
    }
    const url = `builderCompany`;

    API.post(url, newBuilder).then((res) => {
      if (res.id === 0) {
        toast("Error al intentar agregar usuario");
      } else {
        toast("proyecto agregado satisfactoriamente");
        // setUsers([...users.users, userAdded]);
        fetchBuilders(null);
        setBuilderState(initialClientState);
      }
    });
  }

  function updateBuilder(builder) {
    if (!builder || !builder.name) {
      return;
    }
    const url = `builderCompany`;
    API.put(url, builder)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar usuario");
        } else {
          toast("proyecto agregado satisfactoriamente");

          // setUsers([...users.users, userAdded]);
          fetchBuilders(null);
          setBuilderState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  const deleteRow = (id) => {
    const url = `builderCompany?Id=${id}`;

    API.delete(url)
      .then((res) => {
        if (res.data !== "") {
          toast("error al intentar eliminar usuario");
          return;
        }
        fetchBuilders(null);
        setBuilderState(initialClientState);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col />
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStep()}
                >
                  <i className="fa fa-new" /> Agregar proveedor
                </Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <BuilderControlTable
                  fetchBuilders={fetchBuilders}
                  builders={builders}
                  nextPage={nextPage}
                  prevPage={prevPage}
                  editRow={editRow}
                  deleteRow={deleteRow}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <BuilderDetails
            currentState={builderState}
            setBuilderState={setBuilderState}
            nextStep={nextStep}
            prevStep={prevStep}
            addBuilder={addBuilder}
            updateBuilder={updateBuilder}
          />
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(builderState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Proveedores
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default Builders;
