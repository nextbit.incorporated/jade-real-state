import React from "react";

import {
    Button,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    FormGroup,
    Input,
    Label,
    Row
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";




const BuilderDetails = props => {
    const { currentState, setBuilderState, addBuilder, updateBuilder } = props;

    const handleInputChange = event => {
        const { name, value } = event.target;
        setBuilderState({
            ...currentState,
            currentBuilder: {
                ...currentState.currentBuilder,
                [name]: value
            }
        });
    };

    const handleCleanValues = () => {
        const initialClientState = {
            step: 1,
            editMode: "None",
            currentBuilder: {
                id: "",
                name: "",
                description: "",
                businessName: "",
                contactNumber: "",
                electronicAddresses: "",
                email: "",
                address: "",
                owner: "",
                ownerContactNumber: "",
                idNumber: "",
                status: ""
            }
        };
        setBuilderState(initialClientState);
    };


    const handleSaveChanges = () => {
        switch (currentState.editMode) {
            case 'Editing': {
                const builder = {
                    id: currentState.currentBuilder.id,
                    name: currentState.currentBuilder.name,
                    description: currentState.currentBuilder.description,
                    businessName: currentState.currentBuilder.businessName,
                    contactNumber: currentState.currentBuilder.contactNumber,
                    electronicAddresses: currentState.currentBuilder.electronicAddresses,
                    email: currentState.currentBuilder.email,
                    address: currentState.currentBuilder.address,
                    owner: currentState.currentBuilder.owner,
                    ownerContactNumber: currentState.currentBuilder.ownerContactNumber,
                    idNumber: currentState.currentBuilder.idNumber,
                    status: currentState.currentBuilder.status,
                }
                updateBuilder(builder);
                break;
            }

            default: {
                const newBuilder = {
                    name: currentState.currentBuilder.name,
                    description: currentState.currentBuilder.description,
                    businessName: currentState.currentBuilder.businessName,
                    contactNumber: currentState.currentBuilder.contactNumber,
                    electronicAddresses: currentState.currentBuilder.electronicAddresses,
                    email: currentState.currentBuilder.email,
                    address: currentState.currentBuilder.address,
                    owner: currentState.currentBuilder.owner,
                    ownerContactNumber: currentState.currentBuilder.ownerContactNumber,
                    idNumber: currentState.currentBuilder.idNumber,
                    status: currentState.currentBuilder.status,
                }
                addBuilder(newBuilder);
                break;
            }
        };
    }

    switch (currentState.editMode) {
        case 'Editing': return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos a editar de un constructor </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="name"
                                        id="name"
                                        readonly
                                        value={currentState.currentBuilder.name}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="description" name="description"
                                        value={currentState.currentBuilder.description}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Segundo Nombre">Razon Social</Label>
                                    <Input type="text" id="businessName" name="businessName"
                                        value={currentState.currentBuilder.businessName}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="lastName">Telefonos</Label>
                                    <Input type="text" id="contactNumber" name="contactNumber"
                                        value={currentState.currentBuilder.contactNumber}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Contactos Web</Label>
                                    <Input type="text" id="electronicAddresses" name="electronicAddresses"
                                        value={currentState.currentBuilder.electronicAddresses}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Correos</Label>
                                    <Input type="text" id="email" name="email"
                                        value={currentState.currentBuilder.email}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="lastName">Direccion</Label>
                                    <Input type="text" id="address" name="address"
                                        value={currentState.currentBuilder.address}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Propietario</Label>
                                    <Input type="text" id="owner" name="owner"
                                        value={currentState.currentBuilder.owner}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Identidadd/RTN</Label>
                                    <Input type="text" id="idNumber" name="idNumber"
                                        value={currentState.currentBuilder.idNumber}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Contacto Propietario</Label>
                                    <Input type="text" id="ownerContactNumber" name="ownerContactNumber"
                                        value={currentState.currentBuilder.ownerContactNumber}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );
        default: return (

            <div className="animated fadeIn">
                <Card>
                    <CardHeader>
                        <strong> Datos de nuevo constructor </strong>
                    </CardHeader>
                    <CardBody>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Id">Nombre</Label>
                                    <Input
                                        autoFocus
                                        type="text"
                                        name="name"
                                        id="name"
                                        readonly
                                        value={currentState.currentBuilder.name}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                                    <Input type="text" id="description" name="description"
                                        value={currentState.currentBuilder.description}
                                        onChange={handleInputChange}
                                        required
                                    />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="Segundo Nombre">Razon Social</Label>
                                    <Input type="text" id="businessName" name="businessName"
                                        value={currentState.currentBuilder.businessName}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="lastName">Telefonos</Label>
                                    <Input type="text" id="contactNumber" name="contactNumber"
                                        value={currentState.currentBuilder.contactNumber}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Contactos Web</Label>
                                    <Input type="text" id="electronicAddresses" name="electronicAddresses"
                                        value={currentState.currentBuilder.electronicAddresses}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Correos</Label>
                                    <Input type="text" id="email" name="email"
                                        value={currentState.currentBuilder.email}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="lastName">Direccion</Label>
                                    <Input type="text" id="address" name="address"
                                        value={currentState.currentBuilder.address}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Propietario</Label>
                                    <Input type="text" id="owner" name="owner"
                                        value={currentState.currentBuilder.owner}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Identidadd/RTN</Label>
                                    <Input type="text" id="idNumber" name="idNumber"
                                        value={currentState.currentBuilder.idNumber}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="3" sm="6" xs="12">
                                <FormGroup>
                                    <Label htmlFor="identification">Contacto Propietario</Label>
                                    <Input type="text" id="ownerContactNumber" name="ownerContactNumber"
                                        value={currentState.currentBuilder.ownerContactNumber}
                                        onChange={handleInputChange}
                                        required />
                                </FormGroup>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                        <Button type="submit" size="sm" color="success" onClick={handleSaveChanges}>
                            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
                        <Button type="reset" size="sm" color="danger" onClick={handleCleanValues}>
                            <i className="fa fa-ban" /> Cancelar
          </Button>
                    </CardFooter>
                </Card>
            </div>
        );

    }
};
export default BuilderDetails;
