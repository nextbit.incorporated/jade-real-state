import React, { useState } from "react";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
} from "reactstrap";

const step = 1;

const BuilderControlTable = (props) => {
  const {
    fetchBuilders,
    builders,
    nextPage,
    prevPage,
    editRow,
    deleteRow,
  } = props;
  const [value, setValue] = useState("");

  function nextStep() {
    nextPage(step + 1);
  }

  function prevStep() {
    prevPage(step - 1);
  }

  function handleValueChange(e) {
    fetchBuilders(e.target.value);
    setValue(e.target.value);
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const userRows =
    builders === null ? (
      <table>
        <tbody>
          <tr>
            <td colSpan={16}>Buscando...</td>
          </tr>
        </tbody>
      </table>
    ) : builders.length === 0 ? (
      <table>
        <tbody>
          <tr>
            <td colSpan={16}>
              No se encontraron constructoras con el filtro especificado.
            </td>
          </tr>
        </tbody>
      </table>
    ) : (
      <div>
        <FormGroup row>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th style={thStyle}>Editar</th>
                  <th style={thStyle}>Borrar</th>
                  <th style={thStyle}>ID</th>
                  <th style={thStyle}>Nombre</th>
                  <th style={thStyle}>Descripcion</th>
                  <th style={thStyle}>Razon Social</th>
                  <th style={thStyle}>Numero Telefono</th>
                  <th style={thStyle}>Direcciones Electronicas</th>
                  <th style={thStyle}>Correos</th>
                  <th style={thStyle}>Direccion</th>
                  <th style={thStyle}>Propietario</th>
                  <th style={thStyle}>Identidad/RTN</th>
                  <th style={thStyle}>Contacto Propietario</th>
                </tr>
              </thead>
              <tbody>
                {builders.builderCompanies.map((builder) => (
                  <tr key={builder.id}>
                    <td>
                      <Button
                        block
                        color="warning"
                        onClick={() => {
                          editRow(builder);
                        }}
                      >
                        Editar
                      </Button>
                    </td>
                    <td>
                      <Button
                        block
                        color="danger"
                        onClick={() => deleteRow(builder.id)}
                      >
                        Borrar
                      </Button>
                    </td>
                    <td>{builder.id}</td>
                    <td>{builder.name}</td>
                    <td>{builder.description}</td>
                    <td>{builder.businessName}</td>
                    <td>{builder.contactNumber}</td>
                    <td>{builder.electronicAddresses}</td>
                    <td>{builder.email}</td>
                    <td>{builder.address}</td>
                    <td>{builder.owner}</td>
                    <td>{builder.idNumber}</td>
                    <td>{builder.ownerContactNumber}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStep}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {builders.pageCount}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStep}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      </div>
    );
  return userRows;
};

export default BuilderControlTable;
