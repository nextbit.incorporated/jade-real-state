import XLSX from 'xlsx';

/**
 * Genera un archivo de exel para descargar
 * @param {string} filename
 * @param {array} fileColumns
 * @param {array} json
 */
export const generateExelFile = (filename, fileColumns, json) => {
    const wsName = 'SheetJS';
    const wb = XLSX.utils.book_new();
    // const ws = fileColumns
    //   ? XLSX.utils.aoa_to_sheet([fileColumns])
    //   : XLSX.utils.json_to_sheet(json);

    let ws = {};

    if (fileColumns && json) {
        const jsonItems = getItemsValidateByFormatColumns(json, fileColumns);

        ws = XLSX.utils.json_to_sheet(jsonItems);

    } else if (fileColumns && !json) {
        ws = XLSX.utils.aoa_to_sheet([fileColumns]);
    } else if (!fileColumns && json) {
        ws = XLSX.utils.json_to_sheet(json);
    }

    XLSX.utils.book_append_sheet(wb, ws, wsName);
    if (filename.includes('.xlsx')) {
        XLSX.writeFile(wb, filename);
    } else {
        XLSX.writeFile(wb, `${filename}.xlsx`);
    }
};

const getItemsValidateByFormatColumns = (items, formatColumns) => {
    const newList = items.map(item => {
        const columnToExport = {};
        formatColumns.forEach(column => {
            if (column.name && column.fieldName) {
                columnToExport[column.name] = item[column.fieldName];
                return;
            }
            columnToExport[column] = item[column];
        });

        return columnToExport;
    });

    return newList;
};
