const gapi = window.gapi;
const CLIENT_ID =
  "965334430766-2l3poeubaf8nktflv4btk168ncofkm4r.apps.googleusercontent.com";
const API_KEY = "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";
const KEY = "AIzaSyAzy7PiVge6WhE5FriqzQUCXba5zuxHKuA";

const DISCOVERY_DOCS = [
  "https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest",
];
const SCOPES =
  "https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.events";

export const addCalendarEvent = (startDate, address, summary) => {
  gapi.load("client:auth2", function () {
    gapi.client
      .init({
        key: KEY,
        //apiKey: API_KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: SCOPES,
      })
      .then(
        function () {
          gapi.auth2
            .getAuthInstance()
            .signIn()
            .then(() => {
              const isSignedIn = gapi.auth2.getAuthInstance().isSignedIn.get();

              if (isSignedIn) {
                //time zone list:
                // https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
                const timeZone = "America/Tegucigalpa";
                const duration = "00:30:00"; //duration of each event, here 30 minuts

                //event start time - im passing datepicker time, and making it match      //with the duration time, you can just put iso strings:
                //2020-06-28T09:00:00-07:00'

                //const startDate = new Date(timeString);
                const msDuration =
                  (Number(duration.split(":")[0]) * 60 * 60 +
                    Number(duration.split(":")[1]) * 60 +
                    Number(duration.split(":")[2])) *
                  1000;
                const endDate = new Date(startDate.getTime() + msDuration);
                const isoStartDate = new Date(
                  startDate.getTime() -
                    new Date().getTimezoneOffset() * 60 * 1000
                )
                  .toISOString()
                  .split(".")[0];
                const isoEndDate = new Date(
                  endDate.getTime() - new Date().getTimezoneOffset() * 60 * 1000
                )
                  .toISOString()
                  .split(".")[0];

                let event = {
                  summary: summary, // or event name
                  location: address, //where it would happen
                  start: {
                    dateTime: isoStartDate,
                    timeZone: timeZone,
                  },
                  end: {
                    dateTime: isoEndDate,
                    timeZone: timeZone,
                  },
                  recurrence: ["RRULE:FREQ=DAILY;COUNT=1"],
                  reminders: {
                    useDefault: false,
                    overrides: [{ method: "popup", minutes: 20 }],
                  },
                };

                makeApiCall(event);
              } else {
              }
            });
        },
        function (error) {
          console.log(JSON.stringify(error, null, 2));
        }
      );
  });
};

export const deleteCalendarEvent = (id, processEvents) => {
  gapi.load("client:auth2", function () {
    gapi.client
      .init({
        key: KEY,
        //apiKey: API_KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: SCOPES,
      })
      .then(
        function () {
          gapi.auth2
            .getAuthInstance()
            .signIn()
            .then(() => {
              const isSignedIn = gapi.auth2.getAuthInstance().isSignedIn.get();

              if (isSignedIn) {
                // delete
                deleteApiCall(id, processEvents);
              } else {
              }
            });
        },
        function (error) {
          console.log(JSON.stringify(error, null, 2));
        }
      );
  });
};

export const getCalendarEvents = async (processEvents) => {
  gapi.load("client:auth2", function () {
    gapi.client
      .init({
        key: KEY,
        clientId: CLIENT_ID,
        discoveryDocs: DISCOVERY_DOCS,
        scope: SCOPES,
      })
      .then(
        function () {
          gapi.auth2
            .getAuthInstance()
            .signIn()
            .then(() => {
              const isSignedIn = gapi.auth2.getAuthInstance().isSignedIn.get();

              if (isSignedIn) {
                listUpcomingEvents(processEvents);
              } else {
              }
            });
        },
        function (error) {
          console.log(JSON.stringify(error, null, 2));
        }
      );
  });
};

const listUpcomingEvents = (processEvents) => {
  gapi.client.calendar.events
    .list({
      calendarId: "primary",
      timeMin: new Date().toISOString(),
      showDeleted: false,
      singleEvents: true,
      maxResults: 15,
      orderBy: "startTime",
    })
    .then(function (response) {
      var events = response.result.items;

      processEvents(events);

      if (events.length > 0) {
        let i = 0;
        for (i = 0; i < events.length; i++) {
          var event = events[i];
          var when = event.start.dateTime;
          if (!when) {
            when = event.start.date;
          }
        }
      }
    });
};

const makeApiCall = (event) => {
  const request = gapi.client.calendar.events.insert({
    calendarId: "primary",
    resource: event,
  });

  request.execute((event) => {
    window.open(event.htmlLink);
  });
};

const deleteApiCall = (id, processEvents) => {
  const request = gapi.client.calendar.events.delete({
    calendarId: "primary",
    eventId: id,
  });

  request.execute((event) => {
    listUpcomingEvents(processEvents);
  });
};
