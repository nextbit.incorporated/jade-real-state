import React, { Component } from 'react'
import {
    Col,
    FormGroup,
    InputGroup,
    InputGroupAddon,
    Button
} from "reactstrap";

const userHeaderTable = props => {
    const { prevStep, nextStep } = props;

    return (
        <div className="animated fadeIn">
             <h1><strong>Detalle de usuarios</strong></h1> 
            <FormGroup row>
                <Col md="12">
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={prevStep}>
                                <i className="fa fa-search" /> Anterior</Button>
                        </InputGroupAddon>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={nextStep}>
                                <i className="fa fa-search" /> Proximo</Button>
                        </InputGroupAddon>
                    </InputGroup>
                </Col>
            </FormGroup>
        </div>
    );
};

export default userHeaderTable;
