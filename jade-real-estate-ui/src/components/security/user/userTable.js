import React, { Suspense } from "react";

import UserControlTable from "./userControlTable";

const UserTable = () => {
  const uri = `http://localhost:5000/api/user/paged?pageSize=10&pageIndex=0`;

  const fallback = (
    <tr>
      <td colSpan={14}>Cargando....</td>
    </tr>
  );

  return (
    <div>
      <Suspense fallback={fallback}>
        <UserControlTable uri={uri} method="GET" />
      </Suspense>
    </div>
  );
};

export default UserTable;
