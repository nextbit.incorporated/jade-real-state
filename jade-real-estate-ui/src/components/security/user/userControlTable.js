import React, { useState } from "react";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
} from "reactstrap";

const step = 1;

const UserControlTable = (props) => {
  const { fetchUsers, users, nextPage, prevPage, editRow, deleteUser } = props;
  const [value, setValue] = useState("");

  function nextStep() {
    nextPage(step + 1);
  }

  function prevStep() {
    prevPage(step - 1);
  }

  function handleValueChange(e) {
    fetchUsers(e.target.value);
    setValue(e.target.value);
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const userRows =
    users === null ? (
      <table>
        <thead>
          <tr>
            <td colSpan={16}>Buscando...</td>
          </tr>
        </thead>
      </table>
    ) : users.length === 0 ? (
      <table>
        <thead>
          <tr>
            <td colSpan={16}>
              No se encontraron usuarios con el filtro especificado.
            </td>
          </tr>
        </thead>
      </table>
    ) : (
      <div>
        <FormGroup row>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th style={thStyle}>Editar</th>
                  <th style={thStyle}>Borrar</th>
                  <th style={thStyle}>ID</th>
                  <th style={thStyle}>Codigo Usuario</th>
                  <th style={thStyle}>UsuarioId</th>
                  <th style={thStyle}>Primer Nombre</th>
                  <th style={thStyle}>Segundo Nombre</th>
                  <th style={thStyle}>Apellido</th>
                  <th style={thStyle}>Correo</th>
                  <th style={thStyle}>Numero Telefono</th>
                  <th style={thStyle}>Rol</th>
                </tr>
              </thead>
              <tbody>
                {users.users.map((user) => (
                  <tr key={user.userId}>
                    <td>
                      <Button
                        block
                        color="warning"
                        onClick={() => {
                          editRow(user);
                        }}
                      >
                        Editar
                      </Button>
                    </td>
                    <td>
                      <Button
                        block
                        color="danger"
                        onClick={() => deleteUser(user.userId)}
                      >
                        Borrar
                      </Button>
                    </td>
                    <td>{user.id}</td>
                    <td>{user.userCode}</td>
                    <td>{user.userId}</td>
                    <td>{user.firstName}</td>
                    <td>{user.secondName}</td>
                    <td>{user.lastName}</td>
                    <td>{user.email}</td>
                    <td>{user.phoneNumber}</td>
                    <td>{user.rol}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStep}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {users.pageCount}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStep}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      </div>
    );
  return userRows;
};

export default UserControlTable;
