import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
} from "reactstrap";

import UserControlTable from "./userControlTable";
import UserDetails from "./userDetails";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../../API/API";
const heightStyle = {
  height: "500px",
};

const initialClientState = {
  step: 1,
  editMode: "None",
  currentUser: {
    Id: "",
    UserId: "",
    FirstName: "",
    SecondName: "",
    LastName: "",
    email: "",
    PhoneNumber: "",
    Password: "",
  },
  roles: [],
};

const User = (props) => {
  const [usersState, setUserState] = useState(initialClientState);
  const [users, setUsers] = useState([]);
  const [roles, setRoles] = useState([]);

  function fetchUsers(query, step) {
    const index = step === undefined || step === null ? 0 : step;
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard" />;
    }

    var url = "";

    if (query === null) {
      url = `user/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setUsers(res.data);
        })
        .catch((error) => {
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login" />;
          }
        });
    } else {
      url = `user/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url).then((res) => {
        setUsers(res.data);
      });
    }
  }

  function fectRoles() {
    var url = `useraccess/role`;
    API.get(url)
      .then((res) => {
        setRoles(res.data);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login" />;
        }
      });
  }

  const editRow = (user) => {
    setUserState({
      ...usersState,
      editMode: "Editing",
      currentUser: user,
      step: 2,
    });
  };

  function nextPage(step) {
    fetchUsers(null, step);
  }

  function prevPage(step) {
    fetchUsers(null, step);
  }

  function nextStep(user) {
    const { step } = usersState;

    setUserState({ ...usersState, currentClient: user, step: step + 1 });
  }

  function prevStep() {
    const { step } = usersState;

    setUserState({ ...usersState, step: step - 1 });
  }

  useEffect(() => {
    fetchUsers(null);
    fectRoles();
  }, []);

  function addUser(newUser) {
    if (
      !newUser ||
      !newUser.userId ||
      !newUser.firstName ||
      !newUser.Password
    ) {
      return;
    }
    const url = `user`;

    API.post(url, newUser).then((res) => {
      if (res.id === 0) {
        toast("Error al intentar agregar usuario");
      } else {
        toast("usuario agregado satisfactoriamente");

        fetchUsers(null);
        setUserState(initialClientState);
      }
    });
  }

  function updateUser(user) {
    if (!user || !user.userId || !user.firstName) {
      return;
    }
    const url = `user`;
    API.put(url, user)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar usuario");
        } else {
          toast("usuario agregado satisfactoriamente");

          fetchUsers(null);
          setUserState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login" />;
        }
      });
  }

  const deleteUser = (userId) => {
    const url = `user?userId=${userId}`;

    API.delete(url)
      .then((res) => {
        if (res.data !== "") {
          toast("error al intentar eliminar usuario");
          return;
        }
        fetchUsers(null);
        setUserState(initialClientState);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login" />;
        }
      });
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col />
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStep()}
                >
                  <i className="fa fa-new" /> Agregar Usuario
                </Button>
              </Col>
            </FormGroup>

            <FormGroup row>
              <Col xs="12">
                <UserControlTable
                  fetchUsers={fetchUsers}
                  users={users}
                  nextPage={nextPage}
                  prevPage={prevPage}
                  editRow={editRow}
                  deleteUser={deleteUser}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <UserDetails
            currentState={usersState}
            setUserState={setUserState}
            nextStep={nextStep}
            prevStep={prevStep}
            addUser={addUser}
            updateUser={updateUser}
            fectRoles={fectRoles}
            roles={roles}
          />
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(usersState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Usuario
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default User;
