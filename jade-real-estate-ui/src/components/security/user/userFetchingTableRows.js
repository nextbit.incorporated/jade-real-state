import React from "react";
import useFetch from "fetch-suspense";

import { Button } from "reactstrap";

const UserFetchingTableRows = ({ uri, method, props }) => {
  const data = useFetch(uri, {
    method: method,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + sessionStorage.getItem("token"),
      Host: "api.producthunt.com"
    }
  });

  const userRows =
    data === null ? (
      <tr>
        <td colSpan={16}>Buscando...</td>
      </tr>
    ) : data.length === 0 ? (
      <tr>
        <td colSpan={16}>
          No se encontraron usuarios con el filtro especificado.
        </td>
      </tr>
    ) : (
      data.users.map(user => (
        <tr key={user.userId}>
          <td>
            <Button block color="warning">
              Editar
            </Button>
          </td>
          <td>
            <Button block color="danger">
              Borrar
            </Button>
          </td>
          <td>{user.id}</td>
          <td>{user.userId}</td>
          <td>{user.firstName}</td>
          <td>{user.secondName}</td>
          <td>{user.lastName}</td>
          <td>{user.email}</td>
          <td>{user.phoneNumber}</td>
        </tr>
      ))
    );

  return userRows;
};

export default UserFetchingTableRows;
