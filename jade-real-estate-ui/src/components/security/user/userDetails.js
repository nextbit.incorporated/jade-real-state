import React, { useState } from "react";

import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
  Alert,
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";

const UserDetails = (props) => {
  const { currentState, setUserState, addUser, updateUser, roles } = props;
  const [stateBoolShowMessage, setBoolShowMessageState] = useState(false);
  const [stateTypeAlert, setTypeAlertState] = useState("info");
  const [
    stateMessagePasswordValidation,
    setMessagePasswordValidationState,
  ] = useState("Password vacio");
  const [stateVerifiedPassword, setVerifiedPasswordState] = useState("");

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setUserState({
      ...currentState,
      currentUser: {
        ...currentState.currentUser,
        [name]: value,
      },
    });
  };

  const handleNumericInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    setUserState({
      ...currentState,
      currentUser: {
        ...currentState.currentUser,
        [name]: parseInt(value, 10),
      },
    });
  };

  const handleCleanValues = () => {
    const initialClientState = {
      step: 1,
      editMode: "None",
      currentUser: {
        Id: "",
        userCode: "",
        UserId: "",
        FirstName: "",
        SecondName: "",
        LastName: "",
        email: "",
        PhoneNumber: "",
        Password: "",
        rolId: 0,
      },
    };
    setUserState(initialClientState);
    setVerifiedPasswordState("");
    setBoolShowMessageState(false);
  };

  const handleSaveChanges = () => {
    switch (currentState.editMode) {
      case "Editing": {
        const user = {
          userId: currentState.currentUser.userId,
          userCode: currentState.currentUser.userCode,
          firstName: currentState.currentUser.firstName,
          secondName: currentState.currentUser.secondName,
          lastName: currentState.currentUser.lastName,
          email: currentState.currentUser.email,
          phoneNumber: currentState.currentUser.phoneNumber,
          rolId: parseInt(currentState.currentUser.rolId, 10),
        };

        updateUser(user);
        break;
      }

      default: {
        const newUser = {
          userId: currentState.currentUser.id,
          userCode: currentState.currentUser.userCode,
          firstName: currentState.currentUser.firstName,
          secondName: currentState.currentUser.secondName,
          lastName: currentState.currentUser.lastName,
          Password: currentState.currentUser.password,
          PasswordHash: currentState.currentUser.password,
          email: currentState.currentUser.email,
          PhoneNumber: currentState.currentUser.phoneNumber,
          rolId: parseInt(currentState.currentUser.rolId, 10),
        };

        addUser(newUser);
        break;
      }
    }
  };

  const handleVerifiedPassword = (event) => {
    const { value } = event.target;
    const { password } = currentState.currentUser;
    if (value !== password) {
      setBoolShowMessageState(true);
      setTypeAlertState("danger");
      setMessagePasswordValidationState("Contraseña invalida!");
      setVerifiedPasswordState(value);
    } else {
      setBoolShowMessageState(true);
      setTypeAlertState("success");
      setMessagePasswordValidationState("Contraseña confirmada!");
      setVerifiedPasswordState(value);
    }
  };

  const roleOptions = roles.map((p) => {
    return (
      <option id={p.id} key={p.id} value={p.id}>
        {p.name}
      </option>
    );
  });

  switch (currentState.editMode) {
    case "Editing":
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos de nuevo usuario </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Usuario</Label>
                    <Input
                      type="text"
                      name="Id"
                      id="Id"
                      readOnly
                      value={currentState.currentUser.userId}
                      required
                    />
                  </FormGroup>
                </Col>

                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Codigo Usuario">Codigo Usuario</Label>
                    <Input
                      autoFocus
                      type="text"
                      id="userCode"
                      name="userCode"
                      value={currentState.currentUser.userCode}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>

                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Primer Nombre</Label>
                    <Input
                      autoFocus
                      type="text"
                      id="firstName"
                      name="firstName"
                      value={currentState.currentUser.firstName}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Segundo Nombre">Segundo Nombre</Label>
                    <Input
                      type="text"
                      id="secondName"
                      name="secondName"
                      value={currentState.currentUser.secondName}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName">Apellidos</Label>
                    <Input
                      type="text"
                      id="lastName"
                      name="lastName"
                      value={currentState.currentUser.lastName}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="identification">Email</Label>
                    <Input
                      type="text"
                      id="email"
                      name="email"
                      value={currentState.currentUser.email}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="phoneNumber">Telefono</Label>
                    <Input
                      type="text"
                      id="phoneNumber"
                      name="phoneNumber"
                      value={currentState.currentUser.phoneNumber}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>

                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label>Rol</Label>
                    <Input
                      type="select"
                      name="rolId"
                      pattern="[0-9]*"
                      value={currentState.currentUser.rolId}
                      onChange={handleNumericInputChange}
                    >
                      {roleOptions}
                    </Input>
                  </FormGroup>
                </Col>

                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="rol">Rol Anterior</Label>
                    <Input
                      type="text"
                      id="rol"
                      name="rol"
                      value={currentState.currentUser.rol}
                      readOnly
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
    default:
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos de nuevo usuario </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Usuario</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="id"
                      id="id"
                      value={currentState.currentUser.id}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>

                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Codigo Usuario">Codigo Usuario</Label>
                    <Input
                      autoFocus
                      type="text"
                      id="userCode"
                      name="userCode"
                      value={currentState.currentUser.userCode}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>

                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Primer Nombre</Label>
                    <Input
                      type="text"
                      id="FirstName"
                      name="firstName"
                      value={currentState.currentUser.firstName}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Segundo Nombre">Segundo Nombre</Label>
                    <Input
                      type="text"
                      id="SecondName"
                      name="secondName"
                      value={currentState.currentUser.secondName}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName">Apellidos</Label>
                    <Input
                      type="text"
                      id="LastName"
                      name="lastName"
                      value={currentState.currentUser.lastName}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="identification">Email</Label>
                    <Input
                      type="text"
                      id="email"
                      name="email"
                      value={currentState.currentUser.email}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="phoneNumber">Telefono</Label>
                    <Input
                      type="text"
                      id="PhoneNumber"
                      name="phoneNumber"
                      value={currentState.currentUser.phoneNumber}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="password">Contraseña</Label>
                    <Input
                      type="password"
                      id="Password"
                      name="password"
                      value={currentState.currentUser.password}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="identification">Verificar contraseña</Label>
                    <Input
                      type="password"
                      id="password"
                      value={stateVerifiedPassword}
                      onChange={handleVerifiedPassword}
                      required
                    />
                  </FormGroup>
                </Col>

                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label>Rol</Label>
                    <Input
                      type="select"
                      name="rolId"
                      pattern="[0-9]*"
                      value={currentState.currentUser.rolId}
                      onChange={handleNumericInputChange}
                    >
                      {roleOptions}
                    </Input>
                  </FormGroup>
                </Col>

                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <CardBody>
                      <Alert
                        color={stateTypeAlert}
                        isOpen={stateBoolShowMessage}
                      >
                        {stateMessagePasswordValidation}
                      </Alert>
                    </CardBody>
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
  }
};

export default UserDetails;
