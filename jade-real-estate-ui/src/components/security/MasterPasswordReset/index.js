import React from "react";
import PasswordMasterResetDetail from "./passwordMasterResetDetail";
import API from "./../../API/API";
import { Redirect } from "react-router-dom";
import { Card, CardBody, CardHeader, Col, Row, FormGroup } from "reactstrap";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const heightStyle = {
  height: "500px",
};

const initialClientState = {
  step: 1,
  editMode: "None",
};
const MasterPasswordReset = (props) => {

  function UpdateChange(usuario, password, newpassword) {
    const url = `user/changePasswordMaster`;

    const changePasswordrequets = {
      User: usuario,
      UsuarioAGestionar: password,
      NewPassword: newpassword,
    };

    API.put(url, changePasswordrequets)
      .then((res) => {
        if (res.data === "") {
          toast.success("Contraseña actualizada");
      
        }

        if(res.data === "SP")
        {
          toast.warn("Usuario sin privilegios");
        }

        if (res.data === "Usuario no encontrado") {
          toast.warn("Contraseña actual no es correcta");
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login" />;
        }
      });
  }

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col xs="12">
                <PasswordMasterResetDetail UpdateChange={UpdateChange} />
              </Col>
            </FormGroup>
          </CardBody>
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(initialClientState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Usuario
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
      <ToastContainer />
    </div>
  );
};

export default MasterPasswordReset;
