import React from "react";
import ReactToPrint from "react-to-print";
import ComponentVoucherInfoPrint from "./componentVoucherInfoPrint";
import { Col, FormGroup, Button } from "reactstrap";

class PrintInfoVoucher extends React.Component {
    
    render() {
        const negotiationsReserveInfoState = this.props.NegotiationsReserveInfoState;
        const voucherInfoState = this.props.VoucherInfoState;
        const parametroState = this.props.parametroState;
        const principalState = this.props.principalState;
        return (
            <div>
                <FormGroup row>
                    <Col />
                    <Col />

                    <Col>
                        <ReactToPrint
                            trigger={() => (
                                <Button block color="success">
                                    Imprimir Comprobante
                                </Button>
                            )}
                            content={() => this.componentRef}
                        />
                    </Col>

                    <Col>
                        <Button block color="success" onClick={this.props.cancelPrintRow}>
                            Cancelar
            </Button>
                    </Col>
                </FormGroup>
                <ComponentVoucherInfoPrint
                    negotiationsReserveInfoState={negotiationsReserveInfoState}
                    voucherInfoState={voucherInfoState}
                    parametroState={parametroState}
                    principalState={principalState}
                    ref={(el) => (this.componentRef = el)}
                />
            </div>
        );
    }
}

export default PrintInfoVoucher;
