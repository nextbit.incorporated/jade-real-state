import React, { useState, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";
import { CardBody, Col, FormGroup, Row, Card, CardHeader } from "reactstrap";
import NegotiationDetail from "./NegotiationDetail";
import NegotiationTable from "./NegotiationTable";
import NegotiationInfoClient from "./NegotiationInfoClient";
import ContractInfoPrint from "./ContractInfoPrint";
import NegotiationProcessPhase from "./NegotiationProcessPhase";
import moment from "moment";
import UseApplicationOptions from "./../UseApplicationOptions/UseApplicationOptions";
import PrintInfoVoucher from "./printInfoVoucher";
import PrintInfoDownpaymentVoucher from "./printInfoDownpaymentVoucher";

const heightStyle = {
  height: "500px",
};

const initialApplicationOptions = {
  projects: [],
  contactTypes: [],
  nationalities: [],
  financialInstitutions: [],
  salesAdvisors: [],
  supplierServices: [],
};

const initialNegotiationState = {
  step: 1,
  editMode: "None",
  saveStatus: false,
  fileAttachemntProcess: {
    id: 0,
    name: "",
  },
  processSellectedFileAttachment: 0,
  processSelectedNegotiation: 0,
  attachemensProcessSelected: [],
  currentNegotiation: {
    negotiationStartDate: moment().format("YYYY-MM-DD"),
    id: 0,
    creditOfficer: "",
    salesAdvisorId: 0,
    salesAdvisor: "",
    financialInstitutionId: 0,
    financialInstitution: "",
    contactTypeId: 0,
    contactType: "",
    projectId: 0,
    project: "",

    negotiationPhases: [],
    Reserve: {},

    principal: null,
    coDebtor: null,

    financialPrequalification: {
      id: 0,
      negotiationId: 0,
      fundTypeId: 0,
      financialApprovalId: 0,
      interestRate: 0,
      loanAmount: 0,
      loanTerm: 0,
      downPaymentPercent: 0,
      downPayment: 0,
      monthlyPayment: 0,
      comments: null,
      financialApproval: {
        id: 0,
        financialPrequalificationId: 0,
        creationDate: moment().format("YYYY-MM-DD"),
        jointloan: false,
        approvalState: "PENDIENTE",
        approvalDate: moment().format("YYYY-MM-DD"),
        rejectionDate: moment().format("YYYY-MM-DD"),
        rejectionReason: null,
        creditOfficerComments: null,
        salesAdvisorComments: null,
        startDate: moment().format("YYYY-MM-DD"),
        dueDate: moment().format("YYYY-MM-DD"),
        followUp: null,
      },
    },
    negotiationContract: {
      id: 0,
      negotiationId: 0,
      builderCompanyId: 0,
      contractSigningDate: moment().format("YYYY-MM-DD"),
      effectiveDate: moment().format("YYYY-MM-DD"),
      blockNumber: null,
      lotNumber: null,
      constructionMeters: 0,
      landSize: 0,
      registration: null,
      reservationValue: 0,
      downPaymentValue: 0,
      downPaymentMethod: null,
      landValue: 0,
      constructionValue: 0,
      improvementsValue: 0,
      houseTotalValue: 0,
      builderCompany: {
        id: 0,
        name: null,
        description: null,
        businessName: null,
        contactNumber: null,
        electronicAddresses: null,
        email: null,
        address: null,
        owner: null,
        ownerContactNumber: null,
        idNumber: null,
      },
    },
  },
  applicationOptions: initialApplicationOptions,
  state: {
    large1: false,
  },
};

const Negotiation = (props) => {
  const { applicationOptions } = UseApplicationOptions();
  const [negotiationState, setNegotiationState] = useState(
    initialNegotiationState
  );
  const [apiCallInProgress, setApiCallInProgress] = useState(false);
  const [negotiations, setNegotiations] = useState([]);
  const [negotiationToPrint, setNegotiationToPrint] = useState([]);
  const [VoucherInfoState, setVoucherInfoState] = useState([]);
  const [reserveState, setReserveState] = useState([]);
  const [downPaymentState, setDownPaymentState] = useState([]);
  const [
    NegotiationsReserveInfoState,
    setNegotiationsReserveInfoState,
  ] = useState([]);
  const [
    NegotiationDownPaymentState,
    setNegotiationDownPaymentState,
  ] = useState([]);
  const [parametroState, setParametroState] = useState({});
  const [principalState, setprincipalState] = useState({});
  const [houseDesigns, setHouseDesigns] = useState([]);

  const fetchHouseDesigns = (projectId) => {
    if (!projectId) {
      setHouseDesigns([]);
      return;
    }

    setApiCallInProgress(true);

    var url = `houseDesign?projectId=${projectId}`;

    API.get(url)
      .then((res) => {
        setApiCallInProgress(false);

        const houseDesignOptions = [
          { id: 0, name: "(Seleccione un Modelo)" },
        ].concat(res.data.sort(compareNames));

        const options = houseDesignOptions.map((p) => {
          return (
            <option id={p.id} key={p.id} value={p.id}>
              {p.name}
            </option>
          );
        });

        setHouseDesigns(options);
      })
      .catch((error) => {
        setApiCallInProgress(false);

        toast.warn(
          "Se encontraron problermas para obtener los modelos de casas, intente de nuevo por favor."
        );
      });
  };

  const compareNames = (currentItem, nextItem) => {
    const currentName = currentItem.name;
    const nextName = nextItem.name;

    let comparison = 0;
    if (currentName > nextName) {
      comparison = 1;
    } else if (currentName < nextName) {
      comparison = -1;
    }
    return comparison;
  };

  function fetchParametro() {
    
    setApiCallInProgress(true);
    var url = `Information`;
    API.get(url)
      .then((res) => {
        setParametroState(res.data);
        setApiCallInProgress(false);
      })
      .catch((error) => {
        setApiCallInProgress(false);
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }

  function nextPage(step, value) {
    fetchNegotiationPaged(value, step);
  }

  function prevPage(step, value) {
    fetchNegotiationPaged(value, step);
  }

  const showNewDialog = () => {
    setNegotiationState({
      ...negotiationState,

      editMode: "Adding",
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        negotiationStartDate: moment().format("YYYY-MM-DD"),
        principal: null,
        coDebtor: null,
      },
      step: 2,
      applicationOptions: applicationOptions,
    });
  };

  const editRow = (negotiation) => {
    fetchHouseDesigns(negotiation.projectId);

    // we pass all main properties to princpal and codebtor
    const updatedNegotiation = {
      ...negotiation,
      reserveId: negotiation.reserveId ? negotiation.reserveId : 0,
      downpaymentId: negotiation.downpaymentId ? negotiation.downpaymentId : 0,
    };

    setNegotiationState({
      ...negotiationState,
      editMode: "Editing",
      currentNegotiation: updatedNegotiation,
      step: 2,
      applicationOptions: applicationOptions,
    });
  };

  const deleteNegotiation = (id) => {
    const url = `Negotiation?id=${id}`;

    API.delete(url).then((res) => {
      setNegotiationState(initialNegotiationState);
      fetchNegotiationPaged(null, 0);
      toast.success("Negocio eliminado correctamente");
    });
  };

  function fetchNegotiationPaged(query, step) {
    const user = sessionStorage.getItem("userId");

    setApiCallInProgress(true);
    const index = step === undefined || step === null ? 0 : step;
    if (query === null || query === "") {
      var url = `Negotiation/paged?pageSize=15&pageIndex=${index}&user=${user}`;

      API.get(url)
        .then((res) => {
          setApiCallInProgress(false);
          setNegotiations(res.data);
          setNegotiationState({ ...negotiationState, step: 1 });
        })
        .catch((error) => {
          setApiCallInProgress(false);
          if (error.message === "Network Error") {
            toast.warn(
              "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
            );
          } else {
            if (error.response.status !== undefined) {
              if (error.response.status === 401) {
                props.history.push("/login");
                return <Redirect to="/login" />;
              }
            } else {
              toast.warn(
                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
              );
            }
          }
        });
    } else {
      var url1 = `Negotiation/paged?pageSize=15&pageIndex=${index}&user=${user}&value=${query}`;
      API.get(url1)
        .then((res) => {
          setApiCallInProgress(false);
          setNegotiations(res.data);
          setNegotiationState({ ...negotiationState, step: 1 });
        })
        .catch((error) => {
          setApiCallInProgress(false);
          if (error.message === "Network Error") {
            toast.warn(
              "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
            );
          } else {
            if (error.response.status !== undefined) {
              if (error.response.status === 401) {
                props.history.push("/login");
                return <Redirect to="/login" />;
              }
            } else {
              toast.warn(
                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
              );
            }
          }
        });
    }
  }

  function addNewFileAttachment(
    fileBase64,
    fileName,
    fileType,
    negotiationPhase
  ) {
    var splitString = fileBase64.split(",");
    var splitStringFileName = fileName.split(".");
    var nameDirectorio = "";

    for (let index = 0; index < splitStringFileName.length - 1; index++) {
      nameDirectorio += splitStringFileName[index];
    }

    var request = {
      negotiationPhaseId: negotiationPhase.id,
      attachment: splitString[1],
      title: nameDirectorio,
      extensionFile: splitStringFileName[splitStringFileName.length - 1],
      fileType: fileType,
      user: sessionStorage.getItem("userId"),
      date: new Date(),
    };

    const url = `NegotiationAttachment`;

    setApiCallInProgress(true);
    API.post(url, request)
      .then((res) => {
        setApiCallInProgress(false);
        getAttachmentsByNegotiationPhase(request);
        toast.success("reserva agregado satisfactorimente");
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para crear el nuevo cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  }

  function getAttachmentsByNegotiationPhase(requestAttachment) {
    const user = sessionStorage.getItem("userId");
    const url = `NegotiationAttachment?negotiationPhaseId=${requestAttachment.negotiationPhaseId}&user=${user}`;

    setApiCallInProgress(true);

    API.get(url).then((res) => {
      setApiCallInProgress(false);

      var idPhase = res.data[0].negotiationPhaseId;
      var negotiationPhases =
        negotiationState.currentNegotiation.negotiationPhases;

      negotiationPhases.forEach((element) => {
        if (element.id == idPhase) {
          element.attachments = res.data;
        }
      });

      var modales = {
        modal: false,
        modalDownpayment: false,
        modalFileAttachments: false,
        modalOpenAddFile: false,
        large: false,
        large1: false,
        small: false,
        primary: false,
        success: false,
        warning: false,
        danger: false,
        info: false,
        uploadFile: false,
        className: null,
      };

      setNegotiationState({
        ...negotiationState,
        currentNegotiation: {
          ...negotiationState.currentNegotiation,
          negotiationPhases: negotiationPhases,
        },
        state: modales,
        step: 4,
      });
    });
  }

  useEffect(() => {
    fetchParametro();
    fetchNegotiationPaged(null, 0);
  }, []);

  const add = (newClient) => {
    const url = `Negotiation`;

    setApiCallInProgress(true);
    API.post(url, newClient)
      .then((res) => {
        setApiCallInProgress(false);
        setNegotiationState(initialNegotiationState);
        fetchNegotiationPaged(null, 0);
        toast.success("reserva agregado satisfactorimente");
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para crear el nuevo cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  };

  const update = (negotiation) => {
    const url = `Negotiation`;

    setApiCallInProgress(true);

    API.put(url, negotiation)
      .then((res) => {
        setApiCallInProgress(false);
        setNegotiationState(initialNegotiationState);
        fetchNegotiationPaged(null, 0);
        toast.success("datos del cliente modificados satisfactorimente");
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  };

  const cancelChanges = () => {
    setNegotiationState(initialNegotiationState);
    fetchNegotiationPaged(null, 0);
  };

  const saveChanges = (negotiation) => {
    switch (negotiationState.editMode) {
      case "Adding": {
        add(negotiation);

        break;
      }
      case "Editing":
        update(negotiation);
        break;

      default:
        break;
    }
  };

  const cancelPrintRow = () => {
    setNegotiationState({ ...negotiationState, step: 1 });
  };

  const previewNotification = (negotiation) => {
    setNegotiationToPrint(negotiation);
    setNegotiationState({
      ...negotiationState,
      currentNegotiation: negotiation,
      step: 3,
    });
  };

  const previewContactPrint = (negotiation) => {
    setNegotiationToPrint(negotiation);
    setNegotiationState({
      ...negotiationState,
      currentNegotiation: negotiation,
      step: 7,
    });
  };

  const printVoucher = (negotiation, reserveState) => {
    setNegotiationsReserveInfoState(reserveState);
    setVoucherInfoState(negotiation);
    setNegotiationState({
      ...negotiationState,
      currentNegotiation: negotiation,
      step: 5,
    });
  };

  const printVoucherDownPayment = (negotiationDownPaymnet, reserveState) => {
    setNegotiationsReserveInfoState(reserveState);
    setNegotiationDownPaymentState(negotiationDownPaymnet);
    setNegotiationState({
      ...negotiationState,
      step: 6,
    });
  };

  const showNegotiationPhases = (negotiation) => {
    setprincipalState(negotiation.principal);
    setNegotiationState({
      ...negotiationState,
      currentNegotiation: negotiation,
      step: 4,
    });
  };

  const refreshReserveDetail = (detalles) => {
    let s1 = negotiationState.currentNegotiation;
    s1.reserve.detalle = detalles;
    setNegotiationState({
      ...negotiationState,
      step: 4,
      currentNegotiation: s1,
    });
  };

  const refreshReserve = (Newreserve) => {
    let s1 = negotiationState.currentNegotiation;
    s1.reserve = Newreserve;

    s1.reserve.detalle = Newreserve.detalle;
    setReserveState(Newreserve);
    setNegotiationState({
      ...negotiationState,
      step: 4,
      currentNegotiation: s1,
    });
  };

  const refreshDownpaymentDetail = (detalles) => {
    let s1 = negotiationState.currentNegotiation;
    s1.downpayment.detalle = detalles;
    setNegotiationState({
      ...negotiationState,
      step: 4,
      currentNegotiation: s1,
    });
  };

  const refreshDownpayment = (newDownpayment) => {
    let s1 = negotiationState.currentNegotiation;
    s1.downpayment = newDownpayment;

    s1.downpayment.detalle = newDownpayment.detalle;
    setDownPaymentState(newDownpayment);
    setNegotiationState({
      ...negotiationState,
      step: 4,
      currentNegotiation: s1,
    });
  };

  const cancelNegotiationPhases = () => {
    setNegotiationState({ ...negotiationState, step: 1 });
    fetchNegotiationPaged(null, 0);
  };

  function editButtons(negotiationPhase, negotiation) {
    if (negotiationPhase) {
      negotiation.negotiationPhases.forEach((element) => {
        if (element.id === negotiationPhase.id) {
          element.isEdit = true;
        } else {
          element.isEdit = false;
        }
      });

      setNegotiationState({
        ...negotiationState,
        currentNegotiation: negotiation,
        step: 4,
      });
    }
  }

  const cancelNegotiationFileAttachment = (negotiation) => {
    setNegotiationState({
      ...negotiationState,
      currentNegotiation: negotiation,
      step: 4,
    });
  };

  function viewFileAttachment(negotiationPhase, negotiation) {
    if (negotiationPhase) {
      negotiation.negotiationPhases.forEach((element) => {
        if (element.id === negotiationPhase.id) {
          element.isOpenViewFileAttachment = true;
        } else {
          element.isOpenViewFileAttachment = false;
        }
      });

      setNegotiationState({
        ...negotiationState,
        currentNegotiation: negotiation,
        step: 4,

        selectedNegotiationPhase: negotiationPhase,

        state: { modalFileAttachments: true },
      });
    }
  }

  function addFileOpenModal(negotiationPhase, negotiation) {
    if (negotiationPhase) {
      // marking the negotiation fhase a open
      negotiation.negotiationPhases.forEach((element) => {
        if (element.id === negotiationPhase.id) {
          element.isOpenViewFileAttachment = true;
        } else {
          element.isOpenViewFileAttachment = false;
        }
      });

      setNegotiationState({
        ...negotiationState,
        currentNegotiation: negotiation,
        step: 4,

        selectedNegotiationPhase: negotiationPhase,

        state: { modalOpenAddFile: true },
      });
    }
  }

  const handleInputChangeProcessComment = (
    event,
    negotiationPhase,
    negotiation
  ) => {
    const { value } = event.target;

    if (negotiationPhase) {
      negotiation.negotiationPhases.forEach((element) => {
        if (element.id === negotiationPhase.id) {
          element.comment = value;
        }
      });

      setNegotiationState({
        ...negotiationState,
        currentNegotiation: negotiation,
        step: 4,
      });
    }
  };

  const handleInputChangeProcessDateComplete = (
    event,
    negotiationPhase,
    negotiation
  ) => {
    const { value } = event.target;

    if (negotiationPhase) {
      negotiation.negotiationPhases.forEach((element) => {
        if (element.id === negotiationPhase.id) {
          element.dateComplete = value;
        }
      });

      setNegotiationState({
        ...negotiationState,
        currentNegotiation: negotiation,
        step: 4,
      });
    }
  };
  const handleInputChangeProcessDate = (
    event,
    negotiationPhase,
    negotiation
  ) => {
    const { value } = event.target;

    if (negotiationPhase) {
      negotiation.negotiationPhases.forEach((element) => {
        if (element.id === negotiationPhase.id) {
          element.date = value;
        }
      });

      setNegotiationState({
        ...negotiationState,
        currentNegotiation: negotiation,
        step: 4,
      });
    }
  };
  const handleInputChangeProcessStatus = (
    event,
    negotiationPhase,
    negotiation
  ) => {
    const { value } = event.target;

    if (negotiationPhase) {
      negotiation.negotiationPhases.forEach((element) => {
        if (element.id === negotiationPhase.id) {
          element.status = value;
        }
      });

      setNegotiationState({
        ...negotiationState,
        currentNegotiation: negotiation,
        step: 4,
      });
    }
  };

  function saveChangesProcess(negotiationPhase, negotiation) {
    const negotiationProcess = {
      id: negotiationPhase.id,
      negotiationId: negotiationPhase.negotiationId,
      processId: negotiationPhase.processId,
      status: negotiationPhase.status,
      date: negotiationPhase.date,
      dateComplete: negotiationPhase.dateComplete,
      comment: negotiationPhase.comment,
      order: negotiationPhase.order,
    };
    const url = `NegotiationPhase`;

    setApiCallInProgress(true);

    API.put(url, negotiationProcess)
      .then((res) => {
        setApiCallInProgress(false);

        setNegotiationState({
          ...negotiationState,
          currentNegotiation: negotiation,
          step: 4,
        });
        toast.success("datos del cliente modificados satisfactorimente");
      })
      .catch((error) => {
        setApiCallInProgress(false);
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  }

  function CancelEditButtons(negotiationPhase, negotiation) {
    if (negotiationPhase) {
      negotiation.negotiationPhases.forEach((element) => {
        element.isEdit = false;
      });

      setNegotiationState({
        ...negotiationState,
        currentNegotiation: negotiation,
        step: 4,
      });
    }
  }

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col xs="12">
                <NegotiationTable
                  negotiations={negotiations}
                  showNewDialog={showNewDialog}
                  fetchNegotiationPaged={fetchNegotiationPaged}
                  editRow={editRow}
                  deleteNegotiation={deleteNegotiation}
                  nextPage={nextPage}
                  prevPage={prevPage}
                  previewNotification={previewNotification}
                  loadingStatus={apiCallInProgress}
                  showNegotiationPhases={showNegotiationPhases}
                  previewContactPrint={previewContactPrint}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <NegotiationDetail
            negotiationState={negotiationState}
            setNegotiationState={setNegotiationState}
            saveChanges={saveChanges}
            cancelChanges={cancelChanges}
            apiCallInProgress={apiCallInProgress}
            update={update}
            add={add}
            setApiCallInProgress={setApiCallInProgress}
            deleteNegotiation={deleteNegotiation}
            fetchHouseDesigns={fetchHouseDesigns}
            houseDesigns={houseDesigns}
          />
        );
      case 3:
        return (
          <NegotiationInfoClient
            negotiationToPrint={negotiationToPrint}
            houseDesigns={houseDesigns}
            applicationOptions={ applicationOptions}
            cancelPrintRow={cancelPrintRow}
          />
        );
      case 4:
        return (
          <NegotiationProcessPhase
            negotiationState={negotiationState}
            apiCallInProgress={apiCallInProgress}
            setApiCallInProgress={setApiCallInProgress}
            cancelNegotiationPhases={cancelNegotiationPhases}
            editButtons={editButtons}
            CancelEditButtons={CancelEditButtons}
            handleInputChangeProcessComment={handleInputChangeProcessComment}
            handleInputChangeProcessDateComplete={
              handleInputChangeProcessDateComplete
            }
            handleInputChangeProcessDate={handleInputChangeProcessDate}
            handleInputChangeProcessStatus={handleInputChangeProcessStatus}
            saveChangesProcess={saveChangesProcess}
            setNegotiationState={setNegotiationState}
            reserveState={reserveState}
            setReserveState={setReserveState}
            refreshReserveDetail={refreshReserveDetail}
            refreshReserve={refreshReserve}
            downPaymentState={downPaymentState}
            setDownPaymentState={setDownPaymentState}
            refreshDownpaymentDetail={refreshDownpaymentDetail}
            refreshDownpayment={refreshDownpayment}
            viewFileAttachment={viewFileAttachment}
            cancelNegotiationFileAttachment={cancelNegotiationFileAttachment}
            addFileOpenModal={addFileOpenModal}
            addNewFileAttachment={addNewFileAttachment}
            printVoucher={printVoucher}
            printVoucherDownPayment={printVoucherDownPayment}
          />
        );
      case 5:
        return (
          <PrintInfoVoucher
            VoucherInfoState={VoucherInfoState}
            NegotiationsReserveInfoState={NegotiationsReserveInfoState}
            parametroState={parametroState}
            principalState={principalState}
            cancelPrintRow={cancelPrintRow}
          />
        );
      case 6:
        return (
          <PrintInfoDownpaymentVoucher
            NegotiationDownPaymentState={NegotiationDownPaymentState}
            NegotiationsReserveInfoState={NegotiationsReserveInfoState}
            parametroState={parametroState}
            principalState={principalState}
            cancelPrintRow={cancelPrintRow}
          />
        );
      case 7:
        return (
          <ContractInfoPrint
            negotiationToPrint={negotiationToPrint}
            cancelPrintRow={cancelPrintRow}
          />
        );
      default:
        return null;
    }
  }

  const clientsStep = GetCurrentStepComponent(negotiationState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Tramites
              </CardHeader>
              {clientsStep}
            </Card>
          </Col>
        </Row>
      </Card>
      <ToastContainer autoClose={5000} />
    </div>
  );
};

export default Negotiation;
