import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  Col,
  Row,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import ReactLoading from "react-loading";
import API from "../API/API";
import "react-toastify/dist/ReactToastify.css";
import ClientSearch from "./../Clients/ClientSearch/ClientSearch";
import NegotiationHeader from "./NegotiationForms/NegotiationHeader/NegotiationHeader";
import NegotiationClient from "./NegotiationForms/NegotiationClient/NegotiationClient";
import FinancialPrequalification from "./NegotiationForms/FinancialPrequalification/FinancialPrequalification";
import FinancialApproval from "./NegotiationForms/FinancialApproval/FinancialApproval";
import NegotiationContract from "./NegotiationForms/NegotiationContract/NegotiationContract";

const initialClientSeachState = {
  title: "Selección de Cliente",
  isOpen: false,
  clientType: "client",
};

const NegotiationDetail = (props) => {
  const {
    negotiationState,
    setNegotiationState,
    saveChanges,
    cancelChanges,
    apiCallInProgress,
    setApiCallInProgress,
    fetchHouseDesigns,
    houseDesigns,
  } = props;

  const [messageValidation, setMessageValidation] = useState("");
  const [clientSeachState, setClientSeachState] = useState(
    initialClientSeachState
  );

  const handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        [name]: value,
      },
    };

    setNegotiationState(updatedState);
  };

  const handleNumericInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    if (name === "projectId") {
      fetchHouseDesigns(parseInt(value, 10));
    }

    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        [name]: parseFloat(value, 10),
      },
    };

    setNegotiationState(updatedState);
  };

  const handlePreApprovalInputChange = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        financialPrequalification: {
          ...negotiationState.currentNegotiation.financialPrequalification,
          [name]: value,
        },
      },
    };

    setNegotiationState(updatedState);
  };

  const handlePreApprovalNumericInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        financialPrequalification: {
          ...negotiationState.currentNegotiation.financialPrequalification,
          [name]: parseFloat(value, 10),
        },
      },
    };

    setNegotiationState(updatedState);
  };

  const handleApprovalInputChange = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        financialPrequalification: {
          ...negotiationState.currentNegotiation.financialPrequalification,
          financialApproval: {
            ...negotiationState.currentNegotiation.financialPrequalification
              .financialApproval,
            [name]: value,
          },
        },
      },
    };

    setNegotiationState(updatedState);
  };

  const handleApprovalNumericInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        financialPrequalification: {
          ...negotiationState.currentNegotiation.financialPrequalification,
          financialApproval: {
            ...negotiationState.currentNegotiation.financialPrequalification
              .financialApproval,
            [name]: parseFloat(value, 10),
          },
        },
      },
    };

    setNegotiationState(updatedState);
  };

  const handleNegotiationContractInputChange = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        negotiationContract: {
          ...negotiationState.currentNegotiation.negotiationContract,
          [name]: value,
        },
      },
    };

    setNegotiationState(updatedState);
  };

  const handleNegotiationContractNumericInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        negotiationContract: {
          ...negotiationState.currentNegotiation.negotiationContract,
          [name]: parseFloat(value, 10),
        },
      },
    };

    setNegotiationState(updatedState);
  };

  const updatePrincipal = (client) => {
    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        principal: client,
        coDebtor: negotiationState.currentNegotiation.coDebtor
          ? negotiationState.currentNegotiation.coDebtor
          : client.coDebtor,
      },
    };

    setNegotiationState(updatedState);
  };

  const updateCoDebtor = (client) => {
    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        coDebtor: {
          ...client,
          clientId: client.id,
        },
      },
    };

    setNegotiationState(updatedState);
  };

  function trySaveChange(negotiation) {
    if (!negotiation.principal) {
      toast.warn("Debe seleccionar el cliente principal.");
      return;
    }

    setApiCallInProgress(true);

    switch (negotiationState.editMode) {
      case "Adding":
        trySaveChangeNewClient(negotiation);
        break;

      case "Editing":
        saveChanges(negotiation);
        break;

      default:
        break;
    }
  }

  function trySaveChangeNewClient(negotiation) {
    const url = `Negotiation/validate-create-new-negotiation`;

    API.post(url, negotiation)
      .then((res) => {
        if (res.data !== undefined || res.data !== null) {
          if (
            res.data.validationErrorMessage !== null &&
            res.data !== undefined
          ) {
            setMessageValidation(res.data.validationErrorMessage);
            activeValidateNewClient();
            setApiCallInProgress(false);
          } else {
            SaveChangesNewClient();
          }
        }
      })
      .catch((error) => {
        setApiCallInProgress(false);
      });
  }

  function SaveChangesNewClient() {
    saveChanges(negotiationState.currentNegotiation);
  }

  const activeValidateNewClient = () => {
    setNegotiationState({
      ...negotiationState,
      state: {
        large1: !negotiationState.state.large1,
      },
    });
  };

  function getFooterBuild(e) {
    if (apiCallInProgress) {
      return (
        <CardFooter>
          <Col md="3" sm="6" xs="12"></Col>
          <Col md="3" sm="6" xs="12">
            <ReactLoading
              type="bars"
              color="#5DBCD2"
              height={"30%"}
              width={"30%"}
            />
          </Col>
          <Col md="3" sm="6" xs="12"></Col>
        </CardFooter>
      );
    } else {
      return (
        <CardFooter>
          <Button
            type="submit"
            size="sm"
            color="success"
            disabled={apiCallInProgress}
            onClick={() => {
              trySaveChange(negotiationState.currentNegotiation);
            }}
          >
            <i className="fa fa-dot-circle-o" /> Guardar
          </Button>
          <Button type="reset" size="sm" color="danger" onClick={cancelChanges}>
            <i className="fa fa-ban" /> Cancelar
          </Button>
        </CardFooter>
      );
    }
  }

  const searchClient = (clientType) => {
    debugger;
    const updatedClientSeachState = {
      ...clientSeachState,
      title:
        clientType === "client"
          ? "Selección de Cliente"
          : "Selección de Co Deudor",
      isOpen: true,
      clientType: clientType,
    };

    setClientSeachState(updatedClientSeachState);
  };

  const removePrincipal = () => {
    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        principal: null,
      },
    };

    setNegotiationState(updatedState);
  };

  const removeCoDebtor = () => {
    const updatedState = {
      ...negotiationState,
      currentNegotiation: {
        ...negotiationState.currentNegotiation,
        coDebtor: null,
      },
    };

    setNegotiationState(updatedState);
  };

  const toggleClientSelectionIsOpen = () => {
    const updatedClientSeachState = {
      ...clientSeachState,
      isOpen: !clientSeachState.isOpen,
    };

    setClientSeachState(updatedClientSeachState);
  };

  const handleOnSelectedClient = (selectedClient) => {
    if (clientSeachState.clientType === "client") {
      updatePrincipal(selectedClient);
    } else {
      updateCoDebtor(selectedClient);
    }
  };

  return (
    <div className="animated fadeIn">
      <Card>
        <CardBody>
          <NegotiationHeader
            negotiation={negotiationState.currentNegotiation}
            applicationOptions={negotiationState.applicationOptions}
            houseDesigns={houseDesigns}
            handleInputChange={handleInputChange}
            handleNumericInputChange={handleNumericInputChange}
          ></NegotiationHeader>

          <FinancialPrequalification
            financialPrequalification={
              negotiationState.currentNegotiation.financialPrequalification
            }
            applicationOptions={negotiationState.applicationOptions}
            handleInputChange={handlePreApprovalInputChange}
            handleNumericInputChange={handlePreApprovalNumericInputChange}
          ></FinancialPrequalification>

          <FinancialApproval
            financialApproval={
              negotiationState.currentNegotiation.financialPrequalification
                .financialApproval
            }
            applicationOptions={negotiationState.applicationOptions}
            handleInputChange={handleApprovalInputChange}
            handleNumericInputChange={handleApprovalNumericInputChange}
          ></FinancialApproval>

          <NegotiationContract
            negotiationContract={
              negotiationState.currentNegotiation.negotiationContract
            }
            applicationOptions={negotiationState.applicationOptions}
            handleInputChange={handleNegotiationContractInputChange}
            handleNumericInputChange={
              handleNegotiationContractNumericInputChange
            }
          ></NegotiationContract>

          <Row>
            <Col md="6">
              <Row>
                <Button color="primary" onClick={() => searchClient("client")}>
                  Seleccionar Titular
                </Button>
                <Button color="secondary" onClick={() => removePrincipal()}>
                  Remover Titular
                </Button>
              </Row>

              {negotiationState.currentNegotiation.principal && (
                <NegotiationClient
                  title="Cliente Principal"
                  negotiationClient={
                    negotiationState.currentNegotiation.principal
                  }
                  updateClient={updatePrincipal}
                  applicationOptions={negotiationState.applicationOptions}
                ></NegotiationClient>
              )}
            </Col>

            <Col md="6">
              <Row>
                <Button
                  color="primary"
                  onClick={() => searchClient("co-debtor")}
                >
                  Seleccionar Co Deudor
                </Button>
                <Button color="secondary" onClick={() => removeCoDebtor()}>
                  Remover Co Deudor
                </Button>
              </Row>

              {negotiationState.currentNegotiation.coDebtor && (
                <NegotiationClient
                  title="Co Deudor"
                  negotiationClient={
                    negotiationState.currentNegotiation.coDebtor
                  }
                  updateClient={updateCoDebtor}
                  applicationOptions={negotiationState.applicationOptions}
                ></NegotiationClient>
              )}
            </Col>
          </Row>
        </CardBody>
        {getFooterBuild()}
      </Card>

      {clientSeachState.isOpen && (
        <ClientSearch
          title={clientSeachState.title}
          isOpen={clientSeachState.isOpen}
          toggle={toggleClientSelectionIsOpen}
          handleOnSelectedClient={handleOnSelectedClient}
        />
      )}

      <Modal
        isOpen={negotiationState.state.large1}
        toggle={activeValidateNewClient}
        className={"modal-lg"}
      >
        <ModalHeader toggle={activeValidateNewClient}>
          Se identificaron coincidencias
        </ModalHeader>
        <ModalBody>
          <h3>Se encontro un cliente registrado con los mismos datos: </h3>

          <p>
            <strong>{messageValidation}</strong>
          </p>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={() => {
              SaveChangesNewClient();
            }}
          >
            Crear de todas formas
          </Button>
          <Button color="danger" onClick={activeValidateNewClient}>
            Cancelar ingreso
          </Button>
        </ModalFooter>
      </Modal>
      <ToastContainer autoClose={5000} />
    </div>
  );
};

export default NegotiationDetail;
