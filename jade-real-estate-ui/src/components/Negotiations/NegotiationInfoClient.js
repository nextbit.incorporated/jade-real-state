import React from "react";
import ReactToPrint from "react-to-print";
import Negotiation from "./Documents/Negotiation/Negotiation";
import { Col, FormGroup, Button } from "reactstrap";

class NegotiationInfoClient extends React.Component {
  render() {
    const negotiation = this.props.negotiationToPrint;
    const houseDesigns = this.props.houseDesigns;
    const applicationOptions = this.props.applicationOptions;

    return (
      <div>
        <FormGroup row>
          <Col />
          <Col />
          <Col>
            <ReactToPrint
              trigger={() => (
                <Button block color="success">
                  Imprimir Ficha
                </Button>
              )}
              content={() => this.componentRef}
            />
          </Col>
          <Col>
            <Button block color="success" onClick={this.props.cancelPrintRow}>
              Cancelar
            </Button>
          </Col>
        </FormGroup>
        <Negotiation
          negotiation={negotiation}
          houseDesigns={houseDesigns}
          applicationOptions={applicationOptions}
          ref={(el) => (this.componentRef = el)}
        />
      </div>
    );
  }
}

export default NegotiationInfoClient;
