import React from "react";
import logo from "../../assets/img/brand/logo.svg";
import moment from "moment";
import { CardBody, CardHeader, Card, Table, CardFooter } from "reactstrap";

class ComponentVoucherInfoPrint extends React.Component {
    getClientName = (client) => {
        const name = client
            ? (client.firstName || "") +
            " " +
            (client.middleName || "") +
            " " +
            (client.firstSurname || "") +
            " " +
            (client.secondSurname || "")
            : "";

        return name;
    };


    render() {
        const negotiationsReserveInfoState = this.props.negotiationsReserveInfoState;
        const voucherInfoState = this.props.voucherInfoState;
        const parametroState = this.props.parametroState;
        const principalState = this.props.principalState;

        // const thStyle = {
        //     background: "#20a8d8",
        //     color: "white",
        //     textAlign: "center",
        // };

        return (
            <div>
                <Card>
                    <CardHeader>
                        <div style={{ display: "block" }}>
                            <div align="right">
                                <img
                                    src={logo}
                                    alt=""
                                    style={{ width: "80px", height: "80px" }}
                                ></img>
                            </div>
                            <h1 style={{ textAlign: "center" }}>
                                <u>
                                    <b>Inmobiliaria Jade</b>
                                </u>
                            </h1>
                        </div>
                        <div style={{ display: "block" }}>
                            <h2 style={{ textAlign: "center" }}>{parametroState.razonSocial}</h2>
                        </div>
                        <div style={{ display: "block" }}>
                            <h3 style={{ textAlign: "center" }}>R.T.N. {parametroState.rtn}</h3>
                        </div>
                        <div style={{ display: "block" }}>
                            <h4 style={{ textAlign: "center" }}>{parametroState.direccion}</h4>
                        </div>
                        <div style={{ display: "block" }}>
                            <h3 style={{ textAlign: "center" }}>Comprobante pago</h3>
                        </div>
                        <div style={{ display: "block" }}>
                            <h3 style={{ textAlign: "center" }}>Reserva de: {negotiationsReserveInfoState.totalAmount}</h3>
                        </div>
                    </CardHeader>
                    <CardBody>
                        <Table hover bordered striped responsive size="sm" >
                            <thead>
                                <tr>
                                    <th >Codigo Transacción</th>
                                    <th >Codigo Negociación</th>
                                    <th >Cliente </th>
                                    <th >Pago </th>
                                    <th >fecha </th>
                                    <th >Comentario </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td nowrap="true" style={{ width: '50px' }}>
                                        {voucherInfoState.reserveId}
                                    </td>
                                    <td style={{ width: '50px' }}>
                                        {negotiationsReserveInfoState.negotiationId}
                                    </td>
                                    <td nowrap={true}>
                                        {this.getClientName(principalState)}
                                    </td>
                                    <td style={{ width: '50px' }}>
                                        {voucherInfoState.payment}
                                    </td>
                                    <td style={{ width: '50px' }}>
                                        {moment(voucherInfoState.date).format("L")}
                                    </td>
                                    <td style={{ minWidth: '400px', whiteSpace: "nowrap" }}>
                                        {voucherInfoState.comment}
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                        <div style={{ display: "block" }}>
                            <p>Por este medio se hace constar que recibimos de {this.getClientName(principalState)} el monto de {voucherInfoState.payment} como pago de reserva el dia  {moment(voucherInfoState.date).format("L")}</p>
                        </div>

                        <div style={{ marginTop: '20px' }}>
                            <tr>
                                <td>________________________________</td>
                                <td>|                              |</td>
                                <td>________________________________</td>
                            </tr>
                            <tr>
                                <td>            Agente              </td>
                                <td>                                </td>
                                <td>            Cliente             </td>
                            </tr>
                        </div>
                    </CardBody>
                    <CardFooter>
                        <div style={{ display: "block" }}>
                            <h2 style={{ textAlign: "right" }}>
                                Jade Real Estate - {moment(Date.now()).format("L")}
                            </h2>
                        </div>
                    </CardFooter>
                </Card>
            </div>
        );
    }
}

export default ComponentVoucherInfoPrint;
