import React from "react";
import styled from "styled-components";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";
import moment from "moment";

const Root = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
`;

const FinancialApproval = ({
  financialApproval,
  applicationOptions,
  handleInputChange,
  handleNumericInputChange,
}) => {
  return (
    <Root>
      <Row>
        <Col>
          <legend>Aprobacion de Credito</legend>
        </Col>
      </Row>

      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="creationDate">Fecha Ingreso</Label>
            <Input
              type="date"
              name="creationDate"
              value={moment(financialApproval.creationDate).format(
                "YYYY-MM-DD"
              )}
              onChange={handleInputChange}
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <Row>
            <FormGroup check>
              <Label check>
                <Input
                  type="checkbox"
                  name="jointloan"
                  checked={financialApproval.jointloan}
                  onChange={handleInputChange}
                  required
                />{" "}
                Mancomunado
              </Label>
              <Label />
            </FormGroup>
          </Row>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Status Credito</Label>
            <Input
              type="select"
              name="approvalState"
              value={financialApproval.approvalState ?? "PENDIENTE"}
              onChange={handleInputChange}
            >
              {applicationOptions.creditSates}
            </Input>
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="approvalDate">Fecha de Aprobado</Label>
            <Input
              type="date"
              name="approvalDate"
              value={moment(financialApproval.approvalDate).format(
                "YYYY-MM-DD"
              )}
              onChange={handleInputChange}
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="rejectionDate">Fecha Denegado</Label>
            <Input
              type="date"
              name="rejectionDate"
              value={moment(financialApproval.rejectionDate).format(
                "YYYY-MM-DD"
              )}
              onChange={handleInputChange}
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="rejectionReason">Porque Fue Denegado</Label>
            <Input
              type="text"
              name="rejectionReason"
              value={financialApproval.rejectionReason ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col lg="12">
          <FormGroup>
            <Label htmlFor="creditOfficerComments">Comentarios Oficial</Label>
            <Input
              type="text"
              name="creditOfficerComments"
              value={financialApproval.creditOfficerComments ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col lg="12">
          <FormGroup>
            <Label htmlFor="salesAdvisorComments">
              Comentarios Asesor Venta
            </Label>
            <Input
              type="text"
              name="salesAdvisorComments"
              value={financialApproval.salesAdvisorComments ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="startDate">Fecha de Inicio</Label>
            <Input
              type="date"
              name="startDate"
              value={moment(financialApproval.startDate).format("YYYY-MM-DD")}
              onChange={handleInputChange}
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="rejectionDate">Fecha de Entrega</Label>
            <Input
              type="date"
              name="dueDate"
              value={moment(financialApproval.dueDate).format("YYYY-MM-DD")}
              onChange={handleInputChange}
            />
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col lg="12">
          <FormGroup>
            <Label htmlFor="followUp">Seguimiento</Label>
            <Input
              type="text"
              name="followUp"
              value={financialApproval.followUp ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>
    </Root>
  );
};

export default FinancialApproval;
