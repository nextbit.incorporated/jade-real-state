import React from "react";
import styled from "styled-components";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";
import moment from "moment";

const Root = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
`;

const NegotiationClient = ({
  title,
  negotiationClient,
  updateClient,
  applicationOptions,
}) => {
  const handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    const updatedNegotiationClient = {
      ...negotiationClient,

      [name]: value,
    };

    updateClient(updatedNegotiationClient);
  };

  const handleNumericInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    const updatedNegotiationClient = {
      ...negotiationClient,

      [name]: parseInt(value, 10),
    };

    updateClient(updatedNegotiationClient);
  };

  const getClientName = () => {
    return ` ${negotiationClient.firstName} ${negotiationClient.middleName} ${negotiationClient.firstSurname} ${negotiationClient.secondSurname}`;
  };

  const getPhoneNumbers = () => {
    return ` ${negotiationClient.phoneNumber} ${negotiationClient.cellPhoneNumber} `;
  };

  return (
    <React.Fragment>
      <Row>
        <Col>
          <legend>{title}</legend>
        </Col>
      </Row>
      <Root>
        <Col lg="12">
          <FormGroup>
            <Row>
              <strong>{"Nombre : "}</strong>
              {getClientName()}
            </Row>

            <Row>
              <strong>{"Numeros de Telefono : "}</strong>
              {getPhoneNumbers()}
            </Row>

            <Row>
              <strong>{"Email : "}</strong>
              {` ${negotiationClient.email}`}
            </Row>

            <Row>
              <Col xs="12" sm="8" md="6">
                <Label check>
                  <Input
                    type="checkbox"
                    name="ownHome"
                    checked={negotiationClient.ownHome}
                    onChange={handleInputChange}
                    required
                  />{" "}
                  Posee Casa
                </Label>
              </Col>

              <Col xs="12" sm="8" md="6">
                <Label check>
                  <Input
                    type="checkbox"
                    name="firstHome"
                    checked={negotiationClient.firstHome}
                    onChange={handleInputChange}
                    required
                  />{" "}
                  Primera Casa
                </Label>
              </Col>

              <Col xs="12" sm="8" md="6">
                <Label check>
                  <Input
                    type="checkbox"
                    name="contributeToRap"
                    checked={negotiationClient.contributeToRap}
                    onChange={handleInputChange}
                    required
                  />{" "}
                  Aporta al RAP
                </Label>
              </Col>
              <Col xs="12" sm="8" md="6">
                <Label check>
                  <Input
                    type="checkbox"
                    name="preQualify"
                    checked={negotiationClient.preQualify}
                    onChange={handleInputChange}
                    required
                  />{" "}
                  Pre Califica
                </Label>
              </Col>
            </Row>
          </FormGroup>
        </Col>

        <Row>
          <Col xs="12" sm="8" md="6">
            <Label htmlFor="email">Lugar de Trabajo</Label>
            <Input
              type="text"
              name="workplace"
              value={negotiationClient.workplace ?? ""}
              onChange={handleInputChange}
              required
            />
          </Col>

          <Col xs="12" sm="8" md="6">
            <Label htmlFor="workStartDate">Trabaja desde</Label>
            <Input
              type="date"
              name="workStartDate"
              placeholder="Labora desde"
              value={moment(negotiationClient.workStartDate).format(
                "YYYY-MM-DD"
              )}
              onChange={handleInputChange}
            />
          </Col>

          <Col xs="12" sm="8" md="6">
            <Label>Moneda</Label>
            <Input
              type="select"
              name="debtorCurrency"
              value={negotiationClient.debtorCurrency ?? ""}
              onChange={handleInputChange}
            >
              {applicationOptions.currencies}
            </Input>
          </Col>

          <Col xs="12" sm="8" md="6">
            <Label htmlFor="grossMonthlyIncome">Ingresos Mensuales</Label>
            <Input
              type="number"
              name="grossMonthlyIncome"
              value={negotiationClient.grossMonthlyIncome ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </Col>
        </Row>

        <Row>
          <Col xs="12" sm="8" md="6">
            <Label htmlFor="creationDate">Fecha de Consulta</Label>
            <Input
              type="date"
              name="prequalificationDate"
              value={moment(negotiationClient.prequalificationDate).format(
                "YYYY-MM-DD"
              )}
              onChange={handleInputChange}
            />
          </Col>

          <Col xs="12" sm="8" md="6">
            <Label htmlFor="nextPrequalificationDate">Aplica Hasta</Label>
            <Input
              type="date"
              name="nextPrequalificationDate"
              value={moment(negotiationClient.nextPrequalificationDate).format(
                "YYYY-MM-DD"
              )}
              onChange={handleInputChange}
            />
          </Col>
        </Row>

        <Row>
          <Col xs="12" sm="8" md="6">
            <FormGroup>
              <Label htmlFor="grossMonthlyIncome">Monto Lps</Label>
              <Input
                type="number"
                name="prequalificationAmount"
                value={negotiationClient.prequalificationAmount ?? 0}
                onChange={handleNumericInputChange}
                required
              />
            </FormGroup>
          </Col>

          <Col xs="12" sm="8" md="6">
            <Label htmlFor="debtsAmount">Valor Endeudamiento</Label>
            <Input
              type="number"
              name="debtsAmount"
              value={negotiationClient.debtsAmount ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </Col>
        </Row>

        <Row>
          <Col xs="12" sm="8" md="6">
            <FormGroup>
              <Label>Parentesco</Label>
              <Input
                type="select"
                name="relationship"
                value={negotiationClient.relationship ?? ""}
                onChange={handleInputChange}
              >
                {applicationOptions.relationships}
              </Input>
            </FormGroup>
          </Col>

          {/* <Col xs="12" sm="8" md="6">
            <FormGroup check>
              <Label check>
                <Input
                  type="checkbox"
                  name="hasDebts"
                  checked={negotiationClient.hasDebts}
                  onChange={handleInputChange}
                  required
                />{" "}
                Endeudamiento
              </Label>
            </FormGroup>
          </Col> */}
        </Row>
      </Root>
    </React.Fragment>
  );
};

export default NegotiationClient;
