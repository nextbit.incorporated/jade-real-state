import React from "react";
import styled from "styled-components";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";
import moment from "moment";

const Root = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
`;

const NegotiationHeader = ({
  negotiation,
  applicationOptions,
  houseDesigns,
  handleInputChange,
  handleNumericInputChange,
}) => {
  return (
    <Root>
      <Row>
        <Col>
          <legend>Informacion General</legend>
        </Col>
      </Row>

      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="creationDate">Fecha de Creacion</Label>
            <Input
              type="date"
              name="creationDate"
              placeholder="Fecha de Creacion"
              value={moment(negotiation.creationDate).format("YYYY-MM-DD")}
              onChange={handleInputChange}
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="negotiationStartDate">Fecha de Inicio</Label>
            <Input
              type="date"
              name="negotiationStartDate"
              placeholder="Fecha de Creacion"
              value={moment(negotiation.negotiationStartDate).format(
                "YYYY-MM-DD"
              )}
              onChange={handleInputChange}
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Tipo de Contacto</Label>
            <Input
              type="select"
              name="contactTypeId"
              value={negotiation.contactTypeId ?? 0}
              onChange={handleNumericInputChange}
            >
              {applicationOptions.contactTypes}
            </Input>
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Servicio</Label>
            <Input
              type="select"
              name="supplierServiceId"
              value={negotiation.supplierServiceId ?? 0}
              onChange={handleNumericInputChange}
            >
              {applicationOptions.supplierServices}
            </Input>
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Proyecto</Label>
            <Input
              type="select"
              name="projectId"
              pattern="[0-9]*"
              value={negotiation.projectId ?? 0}
              onChange={handleNumericInputChange}
            >
              {applicationOptions.projects}
            </Input>
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Modelo</Label>
            <Input
              type="select"
              name="houseDesignId"
              pattern="[0-9]*"
              value={negotiation.houseDesignId ?? 0}
              onChange={handleNumericInputChange}
            >
              {houseDesigns}
            </Input>
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Asesor de Ventas</Label>
            <Input
              type="select"
              name="salesAdvisorId"
              value={negotiation.salesAdvisorId ?? 0}
              onChange={handleNumericInputChange}
              required
            >
              {applicationOptions.salesAdvisors}
            </Input>
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col>
          <FormGroup>
            <Label htmlFor="comments">Comentarios</Label>
            <Input
              type="text"
              name="comments"
              value={negotiation.comments ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Institucion Financiera</Label>
            <Input
              type="select"
              name="financialInstitutionId"
              value={negotiation.financialInstitutionId ?? 0}
              onChange={handleNumericInputChange}
              required
            >
              {applicationOptions.financialInstitutions}
            </Input>
          </FormGroup>
        </Col>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="creditOfficer">Oficial de Credito</Label>
            <Input
              type="text"
              name="creditOfficer"
              value={negotiation.creditOfficer ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>
    </Root>
  );
};

export default NegotiationHeader;
