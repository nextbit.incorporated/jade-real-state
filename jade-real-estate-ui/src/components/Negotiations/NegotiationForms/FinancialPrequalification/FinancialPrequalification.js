import React from "react";
import styled from "styled-components";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";
import moment from "moment";

const Root = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
`;

const FinancialPrequalification = ({
  financialPrequalification,
  applicationOptions,
  handleInputChange,
  handleNumericInputChange,
}) => {
  return (
    <Root>
      <Row>
        <Col>
          <legend>Pre Calificacion</legend>
        </Col>
      </Row>

      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Tipo de Fondos</Label>
            <Input
              type="select"
              name="fundTypeId"
              value={financialPrequalification.fundTypeId ?? 0}
              onChange={handleNumericInputChange}
            >
              {applicationOptions.fundTypes}
            </Input>
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="interestRate">Tasa%</Label>
            <Input
              type="number"
              name="interestRate"
              value={financialPrequalification.interestRate ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="interestRate">Valor a Financiar</Label>
            <Input
              type="number"
              name="loanAmount"
              value={financialPrequalification.loanAmount ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="loanTerm">Tiempo de Financiamiento</Label>
            <Input
              type="number"
              name="loanTerm"
              value={financialPrequalification.loanTerm ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="downPaymentPercent">% Prima</Label>
            <Input
              type="number"
              name="downPaymentPercent"
              value={financialPrequalification.downPaymentPercent ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="downPayment">Prima Disponible Lps</Label>
            <Input
              type="number"
              name="downPayment"
              value={financialPrequalification.downPayment ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="monthlyPayment">Cuota Mensual Lps</Label>
            <Input
              type="number"
              name="monthlyPayment"
              value={financialPrequalification.monthlyPayment ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col lg="12">
          <FormGroup>
            <Label htmlFor="comments">Comentarios Oficial</Label>
            <Input
              type="text"
              name="comments"
              value={financialPrequalification.comments ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>

      <Row></Row>
    </Root>
  );
};

export default FinancialPrequalification;
