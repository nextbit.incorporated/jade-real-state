import React from "react";
import styled from "styled-components";
import { Col, FormGroup, Input, Label, Row } from "reactstrap";
import moment from "moment";

const Root = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
`;

const NegotiationContract = ({
  negotiationContract,
  applicationOptions,
  handleInputChange,
  handleNumericInputChange,
}) => {
  return (
    <Root>
      <Row>
        <Col>
          <legend>Contrato</legend>
        </Col>
      </Row>

      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="contractSigningDate">Fecha Firma</Label>
            <Input
              type="date"
              name="contractSigningDate"
              value={moment(negotiationContract.contractSigningDate).format(
                "YYYY-MM-DD"
              )}
              onChange={handleInputChange}
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="effectiveDate">Fecha Vigencia</Label>
            <Input
              type="date"
              name="effectiveDate"
              value={moment(negotiationContract.effectiveDate).format(
                "YYYY-MM-DD"
              )}
              onChange={handleInputChange}
            />
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Constructora</Label>
            <Input
              type="select"
              name="builderCompanyId"
              value={negotiationContract.builderCompanyId ?? 0}
              onChange={handleNumericInputChange}
            >
              {applicationOptions.builders}
            </Input>
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="blockNumber">Bloque</Label>
            <Input
              type="text"
              name="blockNumber"
              value={negotiationContract.blockNumber ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="lotNumber">Lote</Label>
            <Input
              type="text"
              name="lotNumber"
              value={negotiationContract.lotNumber ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="constructionMeters">Metros</Label>
            <Input
              type="number"
              name="constructionMeters"
              value={negotiationContract.constructionMeters ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </FormGroup>
        </Col>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="landSize">Terreno</Label>
            <Input
              type="number"
              name="landSize"
              value={negotiationContract.landSize ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="registration">Matricula</Label>
            <Input
              type="text"
              name="registration"
              value={negotiationContract.registration ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="reservationValue">Valor de Reserva</Label>
            <Input
              type="number"
              name="reservationValue"
              value={negotiationContract.reservationValue ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="downPaymentValue">Valor de Prima</Label>
            <Input
              type="number"
              name="downPaymentValue"
              value={negotiationContract.downPaymentValue ?? 0}
              onChange={handleNumericInputChange}
              required
            />
          </FormGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="downPaymentMethod">Forma de Pago Prima</Label>
            <Input
              type="text"
              name="downPaymentMethod"
              value={negotiationContract.downPaymentMethod ?? ""}
              onChange={handleInputChange}
              required
            />
          </FormGroup>
        </Col>
      </Row>
    </Root>
  );
};

export default NegotiationContract;
