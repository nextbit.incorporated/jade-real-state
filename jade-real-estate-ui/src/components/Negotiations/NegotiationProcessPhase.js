import React, { useState, useEffect } from "react";
import {
  Col,
  FormGroup,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Label,
  Input,
  Row,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Table,
} from "reactstrap";
import { toast } from "react-toastify";
import moment from "moment";
import ReactLoading from "react-loading";
import API from "../API/API";
import { triggerBase64Download } from "react-base64-downloader";

const initailReserveDetailState = {
  id: 0,
  reserveId: 0,
  date: moment(Date.now()).format("YYYY-MM-DD"),
  payment: 0,
  comment: "",
  active: true,
};

const initailDownpaymentDetailState = {
  id: 0,
  downpaymentId: 0,
  date: moment(Date.now()).format("YYYY-MM-DD"),
  payment: 0,
  comment: "",
  active: true,
};

const NegotiationProcessPhase = (props) => {
  const {
    negotiationState,
    apiCallInProgress,
    setApiCallInProgress,
    cancelNegotiationPhases,
    process,

    editButtons,
    CancelEditButtons,
    handleInputChangeProcessComment,
    handleInputChangeProcessDateComplete,
    handleInputChangeProcessDate,
    handleInputChangeProcessStatus,
    saveChangesProcess,
    setNegotiationState,
    reserveState,
    setReserveState,
    refreshReserveDetail,
    refreshReserve,
    downPaymentState,
    setDownPaymentState,
    refreshDownpaymentDetail,
    refreshDownpayment,
    viewFileAttachment,
    cancelNegotiationFileAttachment,
    addFileOpenModal,
    addNewFileAttachment,
    printVoucher,
    printVoucherDownPayment
  } = props;

  const [detailState, setDetailState] = useState(initailReserveDetailState);
  const [downPaymentDetailstate, setDownPaymentDetailstate] = useState(
    initailDownpaymentDetailState
  );
  const [file, setFile] = useState(null);
  //const [attachmentsModalisOpen, ]

  useEffect(() => {
    setReserveState(negotiationState.currentNegotiation.reserve);
    setDownPaymentState(negotiationState.currentNegotiation.downpayment);
    setDetailState({
      ...detailState,
      reserveId: reserveState.id,
    });
  }, []);

  const status = [
    { id: "PENDIENTE", name: "PENDIENTE" },
    { id: "OMITIR", name: "OMITIR" },
    { id: "COMPLETADO", name: "COMPLETADO" },
    { id: "CANCELAR", name: "CANCELAR" },
  ];
  const statusOptions = status.map((p) => {
    return (
      <option id={p.id} key={p.id} value={p.id}>
        {p.name}
      </option>
    );
  });

  const toggleReserve = () => {
    setNegotiationState({
      ...negotiationState,
      state: {
        modal: !negotiationState.state.modal,
      },
    });
  };

  const toggleFileAttachments = () => {
    setNegotiationState({
      ...negotiationState,
      state: {
        modalFileAttachments: !negotiationState.state.modalFileAttachments,
      },
    });
  };

  const toggleAddFileAttachments = () => {
    setNegotiationState({
      ...negotiationState,
      state: {
        modalOpenAddFile: !negotiationState.state.modalOpenAddFile,
      },
    });
  };

  const getFileSelected = (e, negotiationPhase) => {
    var file = e.target.files[0];
    var typefile = file.type;
    var namefile = file.name;

    var reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = function () {
      addNewFileAttachment(reader.result, namefile, typefile, negotiationPhase);
      toggleAddFileAttachments();
    };
  };

  const CanceltoggleReserve = () => {
    setNegotiationState({
      ...negotiationState,
      state: {
        modal: false,
      },
    });
  };

  const toggleDownpayment = () => {
    setNegotiationState({
      ...negotiationState,
      state: {
        modalDownpayment: !negotiationState.state.modalDownpayment,
      },
    });
  };

  const thStyle = {
    background: "#20a8d8",
    color: "white",
    textAlign: "center",
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setReserveState({
      ...reserveState,
      [name]: value,
    });
  };

  const handleNumericInputChange = (event) => {
    const { name, value } = event.target;
    setReserveState({
      ...reserveState,
      [name]: parseInt(value, 10),
    });
  };

  const handleInputChangeDetail = (event) => {
    const { name, value } = event.target;
    setDetailState({
      ...detailState,
      [name]: value,
    });
  };

  const handleNumericInputChangeDetail = (event) => {
    const { name, value } = event.target;
    setDetailState({
      ...detailState,
      [name]: parseInt(value, 10),
    });
  };

  const handleInputChangeDownpayment = (event) => {
    const { name, value } = event.target;
    setDownPaymentState({
      ...downPaymentState,
      [name]: value,
    });
  };

  const handleInputChangeDownpaymentNumeric = (event) => {
    const { name, value } = event.target;
    setDownPaymentState({
      ...downPaymentState,
      [name]: parseInt(value, 10),
    });
  };

  const handleInputDownpaymentDetail = (event) => {
    const { name, value } = event.target;
    setDownPaymentDetailstate({
      ...downPaymentDetailstate,
      [name]: value,
    });
  };

  const handleNumericInputDownpaymentDetail = (event) => {
    const { name, value } = event.target;
    setDownPaymentDetailstate({
      ...downPaymentDetailstate,
      [name]: parseInt(value, 10),
    });
  };

  function ObtenerDetallePagosPrima() {
    if (downPaymentState.detalle) {
      return (
        <tbody>
          {downPaymentState.detalle.map((detalle) => (
            <tr key={detalle.id}>
              <td align="center" >
                <Button
                  block
                  color="success"
                  onClick={() => { printVoucherDownPayment(detalle, downPaymentState); }}>
                  Imprimir
                      </Button></td>
              <td align="center">{detalle.downpaymentId}</td>
              <td align="center">
                {moment(detalle.date).format("YYYY-MM-DD")}
              </td>
              <td align="center">{detalle.payment}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.comment}</td>
            </tr>
          ))}
        </tbody>
      );
    }
    return <tbody></tbody>;
  }

  function ObtenerDetalleReservas() {
    if (reserveState.detalle) {
      return (
        <tbody>
          {reserveState.detalle.map((detalle) => (
            <tr key={detalle.id}>
              <td align="center" >
               <Button
                  block
                  color="success"
                  onClick={() => { printVoucher(detalle, reserveState);}}>
                  Imprimir
                      </Button></td>
              <td align="center">{detalle.reserveId}</td>
              <td align="center">{moment(detalle.date).format("YYYY-MM-DD")}</td>
              <td align="center">{detalle.payment}</td>
              <td style={{ whiteSpace: "nowrap" }}>{detalle.comment}</td>
            </tr>
          ))}
        </tbody>
      );
    }
    return <tbody></tbody>;
  }

  function addNewReserve() {
    const url = `Reserve`;
    const reserveRequest = {
      id: 0,
      negotiationId: negotiationState.currentNegotiation.id,
      salesAdvisorId: 1,
      lotNumber: reserveState.lotNumber,
      zone: reserveState.zone,
      block: reserveState.block,
      address: reserveState.address,
      totalAmount: reserveState.totalAmount,
      amountOfPayments: reserveState.amountOfPayments,
      detalle: [detailState],
    };
    setApiCallInProgress(true);
    API.post(url, reserveRequest)
      .then((res) => {
        refreshReserve(res.data);
        toast.success("reserva agregado satisfactorimente");
        setApiCallInProgress(false);
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para registrar el pago favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  }

  function addNewReserveDetail() {
    const url = `ReserveDetail`;
    const reserveDetailRequest = {
      id: 0,
      reserveId: reserveState.id,
      date: detailState.date,
      payment: detailState.payment,
      comment: detailState.comment,
    };
    setApiCallInProgress(true);
    API.post(url, reserveDetailRequest)
      .then((res) => {
        toast.success("reserva agregado satisfactorimente");
        refreshReserveDetail(res.data);
        setApiCallInProgress(false);
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para registrar el pago favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  }

  function addNewDownpayment() {
    const url = `Downpayment`;
    const reserveRequest = {
      id: 0,
      negotiationId: negotiationState.currentNegotiation.id,
      salesAdvisorId: 1,
      lotNumber: downPaymentState.lotNumber,
      zone: downPaymentState.zone,
      block: downPaymentState.block,
      address: downPaymentState.address,
      totalAmount: downPaymentState.totalAmount,
      amountOfPayments: downPaymentState.amountOfPayments,
      detalle: [downPaymentDetailstate],
    };
    setApiCallInProgress(true);
    API.post(url, reserveRequest)
      .then((res) => {
        refreshDownpayment(res.data);
        toast.success("reserva agregado satisfactorimente");
        setApiCallInProgress(false);
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para registrar pago favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  }

  function addNewDownpaymentDetail() {
    const url = `DownpaymentDetail`;
    const reserveDetailRequest = {
      id: 0,
      downpaymentId: downPaymentState.id,
      date: downPaymentDetailstate.date,
      payment: downPaymentDetailstate.payment,
      comment: downPaymentDetailstate.comment,
    };
    setApiCallInProgress(true);
    API.post(url, reserveDetailRequest)
      .then((res) => {
        toast.success("reserva agregado satisfactorimente");
        refreshDownpaymentDetail(res.data);
        setApiCallInProgress(false);
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para registrar pago favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  }

  function trySaveDownpayment() {
    if (downPaymentState.id === 0) {
      if (downPaymentState.totalAmount === 0) {
        toast.warn("El pago total no puede ser 0");
        return;
      }
      if (
        parseFloat(downPaymentDetailstate.payment) >
        parseFloat(downPaymentState.totalAmount)
      ) {
        toast.warn("El pago no puede ser mayor al pago total de reserva!");
        return;
      }

      addNewDownpayment();
    } else {
      if (downPaymentState.totalAmount === 0) {
        toast.warn("El pago total no puede ser 0");
        return;
      }
      if (downPaymentState.detalle) {
        let total = 0;
        downPaymentState.detalle.forEach((element) => {
          total += element.payment;
        });
        total += parseFloat(downPaymentDetailstate.payment);
        if (total > downPaymentState.totalAmount) {
          toast.warn("El pago no puede ser mayor al pago total de reserva!");
          return;
        }
      }
      addNewDownpaymentDetail();
    }
  }

  function trySaveReserve() {
    if (reserveState.id === 0) {
      if (reserveState.totalAmount === 0) {
        toast.warn("El pago total no puede ser 0");
        return;
      }
      if (
        parseFloat(detailState.payment) > parseFloat(reserveState.totalAmount)
      ) {
        toast.warn("El pago no puede ser mayor al pago total de reserva!");
        return;
      }

      addNewReserve();
    } else {
      if (reserveState.totalAmount === 0) {
        toast.warn("El pago total no puede ser 0");
        return;
      }
      if (reserveState.detalle) {
        let total = 0;
        reserveState.detalle.forEach((element) => {
          total += element.payment;
        });
        total += parseFloat(detailState.payment);
        if (total > reserveState.totalAmount) {
          toast.warn("El pago no puede ser mayor al pago total de reserva!");
          return;
        }
      }
      addNewReserveDetail();
    }
  }

  function getFileAttachemnt(file) {
    var fileByte = file.attachment;
    var titleFile = file.title;
    var tipeFile = file.fileType;

    triggerBase64Download(
      "data:" + tipeFile + ";base64," + fileByte,
      titleFile
    );
  }

 const  getClientName = (client) => {
    const name = client
      ? (client.firstName || "") +
      " " +
      (client.middleName || "") +
      " " +
      (client.firstSurname || "") +
      " " +
      (client.secondSurname || "")
      : "";

    return name;
  };

  function getReserveAction(negotiationPhase) {
  
    if (
      negotiationPhase.serviceProcess.order === 1 &&
      negotiationState.currentNegotiation.supplierServiceId === 2
    ) {
      return (
        <Col>
          <Button
            color="primary"
            type="submit"
            size="sm"
            onClick={toggleReserve}
          >
            Reservar
          </Button>
          <Modal
            isOpen={negotiationState.state.modal}
            toggle={toggleReserve}
            className={"modal-lg"}
          >
            <ModalHeader toggle={toggleReserve}>
              Detalle de reserva de cliente:{" "}
              {negotiationState.currentNegotiation.clientId} -{" "}
              {negotiationState.currentNegotiation.clientName}
              <CardHeader>
                Negociacion:{negotiationState.currentNegotiation.id} - Proyecto:{" "}
                {negotiationState.currentNegotiation.project.name} - Reserva:{" "}
                {negotiationState.currentNegotiation.reserve.id}
              </CardHeader>
            </ModalHeader>
            <ModalBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Numero de lote</Label>
                    <Input
                      type="number"
                      name="lotNumber"
                      id="lotNumber"
                      value={reserveState.lotNumber}
                      onChange={handleNumericInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Zona</Label>
                    <Input
                      type="text"
                      name="zone"
                      id="zone"
                      value={reserveState.zone ?? ""}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Bloque</Label>
                    <Input
                      type="text"
                      name="block"
                      id="block"
                      value={reserveState.block ?? ""}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Direccion</Label>
                    <Input
                      type="text"
                      name="address"
                      id="address"
                      value={reserveState.address ?? ""}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Pago Total</Label>
                    <Input
                      type="number"
                      name="totalAmount"
                      id="totalAmount"
                      value={reserveState.totalAmount}
                      onChange={handleNumericInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Cantidad de Pagos</Label>
                    <Input
                      type="number"
                      name="amountOfPayments"
                      id="amountOfPayments"
                      value={reserveState.amountOfPayments}
                      onChange={handleNumericInputChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <CardHeader>
                  <strong>Registrar Pago</strong>
                </CardHeader>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Fecha</Label>
                    <Input
                      type="date"
                      name="date"
                      id="date"
                      placeholder="Fecha"
                      value={detailState.date}
                      onChange={handleInputChangeDetail}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Cantidad a Pagar</Label>
                    <Input
                      type="number"
                      name="payment"
                      id="payment"
                      value={detailState.payment}
                      onChange={handleNumericInputChangeDetail}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Detalle</Label>
                    <Input
                      type="text"
                      name="comment"
                      id="comment"
                      value={detailState.comment}
                      onChange={handleInputChangeDetail}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Agregar pago</Label>
                    <Button
                      block
                      color="primary"
                      type="submit"
                      size="md"
                      onClick={() => trySaveReserve()}
                    >
                      Pagar
                    </Button>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <CardHeader>
                  <strong>Detalles de Pago</strong>
                </CardHeader>
              </Row>
              <Row>
                <Table hover bordered striped responsive size="sm">
                  <thead>
                    <tr>
                      <th style={thStyle}>Imprimir comprobante</th>
                      <th style={thStyle}>Codigo Negociación</th>
                      <th style={thStyle}>Fecha</th>
                      <th style={thStyle}>Pago</th>
                      <th style={thStyle}>Commentario</th>
                    </tr>
                  </thead>
                  {ObtenerDetalleReservas()}
                </Table>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={CanceltoggleReserve}>
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        </Col>
      );
    }
    if (
      negotiationPhase.serviceProcess.order === 7 &&
      negotiationState.currentNegotiation.supplierServiceId === 2
    ) {
      return (
        <Col>
          <Button
            color="primary"
            type="submit"
            size="sm"
            onClick={toggleDownpayment}
          >
            Pagar prima
          </Button>
          <Modal
            isOpen={negotiationState.state.modalDownpayment}
            toggle={toggleDownpayment}
            className={"modal-lg"}
          >
            <ModalHeader toggle={toggleDownpayment}>
              Detalle de reserva de cliente:{" "}
              {negotiationState.currentNegotiation.clientId} -{" "}
              {getClientName(negotiationState.currentNegotiation.principal)}
              <CardHeader>
                Negociacion:{negotiationState.currentNegotiation.id} - Proyecto:{" "}
                {negotiationState.currentNegotiation.project.name} - Prima:{" "}
                {negotiationState.currentNegotiation.downpayment.id}
              </CardHeader>
            </ModalHeader>
            <ModalBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Numero de lote</Label>
                    <Input
                      type="number"
                      name="lotNumber"
                      id="lotNumber"
                      value={downPaymentState.lotNumber}
                      onChange={handleInputChangeDownpaymentNumeric}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Zona</Label>
                    <Input
                      type="text"
                      name="zone"
                      id="zone"
                      value={downPaymentState.zone || ''}
                      onChange={handleInputChangeDownpayment}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Bloque</Label>
                    <Input
                      type="text"
                      name="block"
                      id="block"
                      value={downPaymentState.block || ''}
                      onChange={handleInputChangeDownpayment}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Direccion</Label>
                    <Input
                      type="text"
                      name="address"
                      id="address"
                      value={downPaymentState.address || ''}
                      onChange={handleInputChangeDownpayment}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Pago Total</Label>
                    <Input
                      type="number"
                      name="totalAmount"
                      id="totalAmount"
                      value={downPaymentState.totalAmount}
                      onChange={handleInputChangeDownpaymentNumeric}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Cantidad de Pagos</Label>
                    <Input
                      type="number"
                      name="amountOfPayments"
                      id="amountOfPayments"
                      value={downPaymentState.amountOfPayments}
                      onChange={handleInputChangeDownpaymentNumeric}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <CardHeader>
                  <strong>Registrar Pago</strong>
                </CardHeader>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Fecha</Label>
                    <Input
                      type="date"
                      name="date"
                      id="date"
                      placeholder="Fecha"
                      value={downPaymentDetailstate.date}
                      onChange={handleInputDownpaymentDetail}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Cantidad a Pagar</Label>
                    <Input
                      type="number"
                      name="payment"
                      id="payment"
                      value={downPaymentDetailstate.payment}
                      onChange={handleNumericInputDownpaymentDetail}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Detalle</Label>
                    <Input
                      type="text"
                      name="comment"
                      id="comment"
                      value={downPaymentDetailstate.comment}
                      onChange={handleInputDownpaymentDetail}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Agregar pago</Label>
                    <Button
                      block
                      color="primary"
                      type="submit"
                      size="md"
                      onClick={() => trySaveDownpayment()}
                    >
                      Pagar
                    </Button>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <CardHeader>
                  <strong>Detalles de Pago</strong>
                </CardHeader>
              </Row>
              <Row>
                <Table hover bordered striped responsive size="sm">
                  <thead>
                    <tr>
                      <th style={thStyle}>Imprimir</th>
                      <th style={thStyle}>Codigo Negociación</th>
                      <th style={thStyle}>Fecha</th>
                      <th style={thStyle}>Pago</th>
                      <th style={thStyle}>Commentario</th>
                    </tr>
                  </thead>
                  {ObtenerDetallePagosPrima()}
                </Table>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={CanceltoggleReserve}>
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        </Col>
      );
    }
    return <div />;
  }

  function getFooterBuild(negotiationPhase) {
    if (apiCallInProgress) {
      return (
        <CardFooter>
          <Col md="3" sm="6" xs="12"></Col>
          <Col md="3" sm="6" xs="12">
            <ReactLoading
              type="bars"
              color="#5DBCD2"
              height={"30%"}
              width={"30%"}
            />
          </Col>
          <Col md="3" sm="6" xs="12"></Col>
        </CardFooter>
      );
    } else {
      if (negotiationPhase.isEdit) {
        return (
          <div className="card-header-actions">
            <Row>
              <Col>
                <Button
                  type="submit"
                  size="sm"
                  color="success"
                  disabled={apiCallInProgress}
                  onClick={() =>
                    saveChangesProcess(
                      negotiationPhase,
                      negotiationState.currentNegotiation
                    )
                  }
                >
                  Guardar
                </Button>
              </Col>
              <Col>
                <Button
                  type="reset"
                  size="sm"
                  color="danger"
                  onClick={() => {
                    CancelEditButtons(
                      negotiationPhase,
                      negotiationState.currentNegotiation
                    );
                  }}
                >
                  Cancelar
                </Button>
              </Col>
              {getReserveAction(negotiationPhase)}
            </Row>
          </div>
        );
      } else {
        return (
          <div className="card-header-actions">
            <Row>
              <Col>
                <Button
                  type="submit"
                  size="sm"
                  color="warning"
                  disabled={apiCallInProgress}
                  onClick={() => {
                    editButtons(
                      negotiationPhase,
                      negotiationState.currentNegotiation
                    );
                  }}
                >
                  Editar
                </Button>
              </Col>
              <Col>
                <Button
                  type="submit"
                  size="sm"
                  color="success"
                  style={{ minWidth: "100px" }}
                  onClick={() => {
                    addFileOpenModal(
                      negotiationPhase,
                      negotiationState.currentNegotiation
                    );
                  }}
                >
                  Agregar Archivo
                </Button>
                <Modal
                  isOpen={negotiationState.state.modalOpenAddFile}
                  toggle={toggleAddFileAttachments}
                  className={"modal-lg"}
                >
                  <ModalHeader toggle={toggleAddFileAttachments}>
                    Agregar archcivo
                  </ModalHeader>
                  <ModalBody>
                    <Input
                      type="file"
                      id="file-input"
                      name="file-input"
                      value={file ?? ""}
                      onChange={(e) => {
                        getFileSelected(e, negotiationPhase);
                      }}
                    />
                  </ModalBody>
                </Modal>
              </Col>
              <Col>
                <Button
                  type="submit"
                  size="sm"
                  onClick={() => {
                    viewFileAttachment(
                      negotiationPhase,
                      negotiationState.currentNegotiation
                    );
                  }}
                >
                  Adjuntos
                </Button>

                <Modal
                  isOpen={negotiationState.state.modalFileAttachments}
                  toggle={toggleFileAttachments}
                  className={"modal-lg"}
                >
                  <ModalHeader toggle={toggleFileAttachments}>
                    Archivos Adjuntos de{" "}
                    {(negotiationState.selectedNegotiationPhase &&
                      negotiationState.selectedNegotiationPhase.serviceProcess
                        .name) ||
                      ""}
                    <CardHeader>
                      Negociacion:
                      {`${negotiationState.currentNegotiation.principal.firstName} ${negotiationState.currentNegotiation.principal.firstSurname}`}
                    </CardHeader>
                  </ModalHeader>
                  <ModalBody>
                    <Card>
                      <FormGroup row>
                        <Col>
                          <Table hover bordered striped responsive size="sm">
                            <thead>
                              <tr>
                                <th style={thStyle}></th>
                                <th style={thStyle}>Nombre</th>
                              </tr>
                            </thead>
                            <tbody>
                              {negotiationPhase.attachments.map((file) => (
                                <tr key={file.id}>
                                  <td>
                                    <Button
                                      block
                                      color="success"
                                      onClick={() => {
                                        getFileAttachemnt(file);
                                      }}
                                    >
                                      Descargar
                                    </Button>
                                  </td>
                                  <td>{file.title}</td>
                                  <td></td>
                                </tr>
                              ))}
                            </tbody>
                          </Table>
                        </Col>
                      </FormGroup>
                    </Card>
                  </ModalBody>
                </Modal>
              </Col>
            </Row>
          </div>
        );
      }
    }
  }

  function getBodyContentByProcess(e) {
    return negotiationState.currentNegotiation.negotiationPhases.map(
      (negotiationPhase) => {
        return (
          <div key={negotiationPhase.id} className="animated fadeIn">
            <Card>
              <CardHeader>
                <h3>
                  <strong>
                    {negotiationPhase.order} - Proceso codigo:{" "}
                    <b>
                      {negotiationPhase.serviceProcess.id} -{" "}
                      {negotiationPhase.serviceProcess.name} - Estado:{" "}
                      <u>{negotiationPhase.status}</u>{" "}
                    </b>
                  </strong>
                </h3>
                {getFooterBuild(negotiationPhase)}
              </CardHeader>
              <CardBody>
                <Card>
                  <Row>
                    <Col md="3" sm="6" xs="12">
                      <FormGroup>
                        <Label htmlFor="textarea-input">Fecha: </Label>
                        <Input
                          type="date"
                          name="date"
                          id="date"
                          readOnly={!negotiationPhase.isEdit}
                          value={moment(negotiationPhase.date).format(
                            "YYYY-MM-DD"
                          )}
                          onChange={(e) =>
                            handleInputChangeProcessDate(
                              e,
                              negotiationPhase,
                              negotiationState.currentNegotiation
                            )
                          }
                        />
                      </FormGroup>
                    </Col>
                    <Col md="3" sm="6" xs="12">
                      <FormGroup>
                        <Label htmlFor="textarea-input">
                          Fecha de cierre:{" "}
                        </Label>
                        <Input
                          type="date"
                          name="dateComplete"
                          id="dateComplete"
                          readOnly={!negotiationPhase.isEdit}
                          value={moment(negotiationPhase.dateComplete).format(
                            "YYYY-MM-DD"
                          )}
                          onChange={(e) =>
                            handleInputChangeProcessDateComplete(
                              e,
                              negotiationPhase,
                              negotiationState.currentNegotiation
                            )
                          }
                        />
                      </FormGroup>
                    </Col>
                    <Col md="3" sm="6" xs="12">
                      <FormGroup>
                        <Label>Estado del proceso</Label>
                        <Input
                          type="select"
                          name="status"
                          readOnly={!negotiationPhase.isEdit}
                          value={negotiationPhase.status}
                          onChange={(e) =>
                            handleInputChangeProcessStatus(
                              e,
                              negotiationPhase,
                              negotiationState.currentNegotiation
                            )
                          }
                        >
                          {statusOptions}
                        </Input>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12" md="9">
                      <FormGroup>
                        <Label htmlFor="textarea-input">Comentarios: </Label>
                        <Input
                          type="textarea"
                          name="textarea-input"
                          id="textarea-input"
                          readOnly={!negotiationPhase.isEdit}
                          rows="9"
                          placeholder="Por favor ingresar un comentario..."
                          value={negotiationPhase.comment ?? ""}
                          onChange={(e) =>
                            handleInputChangeProcessComment(
                              e,
                              negotiationPhase,
                              negotiationState.currentNegotiation
                            )
                          }
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                </Card>
              </CardBody>
            </Card>
          </div>
        );
      }
    );
  }

  return (
    <div>
      <FormGroup row>
        <Col>
          <Button block color="success" onClick={cancelNegotiationPhases}>
            Ir a Negocios
          </Button>
        </Col>
      </FormGroup>
      <Card>
        <CardHeader>
          <strong>Fases de Negociaciacion</strong>
          <h3>
            <strong>
              {" "}
              Cliente codigo:{" "}
              <b>
                <u>{negotiationState.currentNegotiation.principal.id}</u>
              </b>
            </strong>
          </h3>
        </CardHeader>
        <CardBody>{getBodyContentByProcess()}</CardBody>
      </Card>
    </div>
  );
};

export default NegotiationProcessPhase;
