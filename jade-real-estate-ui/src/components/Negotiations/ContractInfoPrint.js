import React from "react";
import ReactToPrint from "react-to-print";
import Contract from "./Documents/Contract/Contract";
import { Col, FormGroup, Button } from "reactstrap";

class NegotiationInfoClient extends React.Component {
    render() {
        const negotiation = this.props.negotiationToPrint;

        return (
            <div>
                <FormGroup row>
                    <Col />
                    <Col />
                    <Col>
                        <ReactToPrint
                            trigger={() => (
                                <Button block color="success">
                                    Imprimir Contrato
                                </Button>
                            )}
                            content={() => this.componentRef}
                        />
                    </Col>
                    <Col>
                        <Button block color="success" onClick={this.props.cancelPrintRow}>
                            Cancelar
            </Button>
                    </Col>
                </FormGroup>
                <Contract
                    negotiation={negotiation}
                    ref={(el) => (this.componentRef = el)}
                />
            </div>
        );
    }
}

export default NegotiationInfoClient;