import React, { useState } from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

const PrintDocumentActions = ({ actions }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen((prevState) => !prevState);

  const dropdownItems = actions.map((action) => {
    return (
      <DropdownItem key={action.id} onClick={action.onClick}>
        {action.title}
      </DropdownItem>
    );
  });

  return (
    <Dropdown
      color="success"
      isOpen={dropdownOpen}
      toggle={toggle}
      style={{ color: "#fff", backgroundColor: "#4dbd74" }}
    >
      <DropdownToggle caret color="success">
        Imprimir
      </DropdownToggle>
      <DropdownMenu>{dropdownItems}</DropdownMenu>
    </Dropdown>
  );
};

export default PrintDocumentActions;
