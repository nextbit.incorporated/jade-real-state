import React from "react";
import logo from "../../../../assets/img/brand/logo.svg";
import moment from "moment";
import { CardBody, CardHeader, Col, Row, Label, Input, FormGroup, } from "reactstrap";


class Negotiation extends React.Component {
  getClientName = (client) => {
    if (!client) {
      return "";
    }

    const name = client
      ? (client.firstName || "") +
      " " +
      (client.middleName || "") +
      " " +
      (client.firstSurname || "") +
      " " +
      (client.secondSurname || "")
      : "";

    return name;
  };

  render() {
    const negotiation = this.props.negotiation;
    const negotiationClient = negotiation.principal;
    const coDebtor = negotiation.coDebtor;
    const houseDesigns = this.props.houseDesigns;
    const applicationOptions = this.props.applicationOptions;
    const financialPrequalification = this.props.negotiation.financialPrequalification;
    const financialApproval = this.props.negotiation.financialPrequalification.financialApproval;
    const negotiationContract = this.props.negotiation.negotiationContract;

    // return (
    //   <div>
    //     <CardBody>
    //       <div style={{ display: "block" }}>
    //         <div align="right">
    //           <img
    //             src={logo}
    //             alt=""
    //             style={{ width: "80px", height: "80px" }}
    //           ></img>
    //         </div>
    //         <h1 style={{ textAlign: "center" }}>
    //           <u>
    //             <b>Inmobiliaria Jade</b>
    //           </u>
    //         </h1>
    //       </div>
    //       <div style={{ display: "block" }}>
    //         <h2 style={{ textAlign: "center" }}>
    //           Ficha Cliente: {client && client.Id}
    //         </h2>
    //       </div>
    //     </CardBody>
    //     <CardBody>
    //       <div style={{ display: "block" }}>
    //         <h3 style={{ textAlign: "left" }}>
    //           <b>Nombre: {this.getClientName(client)} </b>
    //         </h3>
    //         <h3 style={{ textAlign: "left" }}>
    //           <b>
    //             Identidad: {(client && client.identificationCard) || "N/D"}{" "}
    //           </b>
    //         </h3>
    //         <h4 style={{ textAlign: "left" }}>
    //           Nacionalidad: {(client && client.nationality) || "N/D"}{" "}
    //         </h4>
    //       </div>
    //     </CardBody>
    //     <CardBody>
    //       <CardHeader>
    //         <strong>Información personal</strong>
    //       </CardHeader>
    //       <table border="1" cellSpacing="0" cellPadding="4">
    //         <tbody>
    //           <tr>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Telefono: </b> {(client && client.phoneNumber) || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Celular: </b> {(client && client.cellPhoneNumber) || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Correo: </b>
    //               {client.email || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Dirección: </b>
    //               {(client && client.homeAddress) || "N/D"}
    //             </td>
    //           </tr>
    //           <tr>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Trabajo Actual: </b>
    //               {(client && client.workplace) || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Ingreso Actual: </b>
    //               {(client && client.grossMonthlyIncome) || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Agente de Ventas: </b>
    //               {negotiation.salesAdvisor || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Oficial De Credito: </b>
    //               {negotiation.creditOfficer || "N/D"}
    //             </td>
    //           </tr>
    //           <tr>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Financiera: </b>
    //               {negotiation.financialInstitution || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Comentarios: </b>
    //               {negotiation.comments || "N/D"}
    //             </td>
    //           </tr>
    //         </tbody>
    //       </table>
    //     </CardBody>
    //     <CardBody>
    //       <CardHeader>
    //         <strong>Información Coudeudor</strong>
    //       </CardHeader>
    //       <table border="1" cellSpacing="0" cellPadding="4">
    //         <tbody>
    //           <tr>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Codeudor: </b>
    //               {this.getClientName(coDebtor) || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Telefono: </b>
    //               {(coDebtor && coDebtor.phoneNumber) || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Celular: </b>
    //               {(coDebtor && coDebtor.cellPhoneNumber) || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Correo: </b>
    //               {(coDebtor && coDebtor.email) || "N/D"}
    //             </td>
    //           </tr>
    //           <tr>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Trabajo: </b>
    //               {(coDebtor && coDebtor.workplace) || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Ingreso: </b>
    //               {(coDebtor && coDebtor.grossMonthlyIncome) || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Identidad: </b>
    //               {(coDebtor && coDebtor.identificationCard) || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Nacionalidad: </b>
    //               {(coDebtor && coDebtor.nationality) || "N/D"}
    //             </td>
    //           </tr>
    //         </tbody>
    //       </table>
    //     </CardBody>
    //     <CardBody>
    //       <CardHeader>
    //         <strong>Información Financiera</strong>
    //       </CardHeader>
    //       <table border="1" cellSpacing="0" cellPadding="4">
    //         <tbody>
    //           <tr>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Proyecto: </b>
    //               {negotiation.project || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Contacto: </b>
    //               {negotiation.contactType || "N/D"}
    //             </td>
    //             <td style={{ padding: "0 25px 0 15px" }}>
    //               <b>Financiera: </b>
    //               {negotiation.financialInstitution || "N/D"}
    //             </td>
    //           </tr>
    //         </tbody>
    //       </table>
    //     </CardBody>

    //     <CardBody>
    //       <div style={{ display: "block" }}>
    //         <h2 style={{ textAlign: "right" }}>
    //           Jade Real Estate - {moment(Date.now()).format("L")}
    //         </h2>
    //       </div>
    //     </CardBody>
    //   </div>
    // );

    const getClientName = () => {
      return ` ${negotiationClient.firstName} ${negotiationClient.middleName} ${negotiationClient.firstSurname} ${negotiationClient.secondSurname}`;
    };

    const getPhoneNumbers = () => {
      return ` ${negotiationClient.phoneNumber} ${negotiationClient.cellPhoneNumber} `;
    };

    const getClientNameCoDeudor = () => {
      if (coDebtor) {
        return ` ${coDebtor.firstName} ${coDebtor.middleName} ${coDebtor.firstSurname} ${coDebtor.secondSurname}`;
      }

      return ``;
    };

    const getPhoneNumbersCoDeudor = () => {
      if (coDebtor) {
        return ` ${coDebtor.phoneNumber} ${coDebtor.cellPhoneNumber} `;
      }
      return ``;
    };


    return (
      <div>
        <CardBody>
          <div style={{ display: "block" }}>
            <div align="right">
              <img
                src={logo}
                alt=""
                style={{ width: "80px", height: "80px" }}
              ></img>
            </div>
            <h1 style={{ textAlign: "center" }}>
              <u>
                <b>Inmobiliaria Jade</b>
              </u>
            </h1>
          </div>
          <div style={{ display: "block" }}>
            <h2 style={{ textAlign: "center" }}>
              Ficha Cliente: {negotiationClient && negotiationClient.Id}
            </h2>
          </div>
        </CardBody>
        <CardBody>

          <Row>
            <Col>
              <legend>Informacion del Cliente</legend>
            </Col>
          </Row>

          <Col lg="12">
            <FormGroup>
              <Row>
                <strong>{"Nombre : "}</strong>
                {getClientName()}
              </Row>

              <Row>
                <strong>{"Numeros de Telefono : "}</strong>
                {getPhoneNumbers()}
              </Row>

              <Row>
                <strong>{"Email : "}</strong>
                {` ${negotiationClient.email}`}
              </Row>

              <Row>
                <Col xs="12" sm="8" md="6">
                  <Label check>
                    <Input
                      readOnly
                      type="checkbox"
                      name="ownHome"
                      checked={negotiationClient.ownHome}
                    />{" "}
                  Posee Casa
                </Label>
                </Col>

                <Col xs="12" sm="8" md="6">
                  <Label check>
                    <Input
                      readOnly
                      type="checkbox"
                      name="firstHome"
                      checked={negotiationClient.firstHome}
                    />{" "}
                  Primera Casa
                </Label>
                </Col>

                <Col xs="12" sm="8" md="6">
                  <Label check>
                    <Input
                      readOnly
                      type="checkbox"
                      name="contributeToRap"
                      checked={negotiationClient.contributeToRap}
                    />{" "}
                  Aporta al RAP
                </Label>
                </Col>
                <Col xs="12" sm="8" md="6">
                  <Label check>
                    <Input
                      readOnly
                      type="checkbox"
                      name="preQualify"
                      checked={negotiationClient.preQualify}
                    />{" "}
                  Pre Califica
                </Label>
                </Col>
              </Row>
            </FormGroup>
          </Col>

          <Row>
            <Col xs="12" sm="8" md="6">
              <Label htmlFor="email">Lugar de Trabajo</Label>
              <Input
                readOnly
                type="text"
                name="workplace"
                value={negotiationClient.workplace ?? ""}
              />
            </Col>

            <Col xs="12" sm="8" md="6">
              <Label htmlFor="workStartDate">Trabaja desde</Label>
              <Input
                readOnly
                type="date"
                name="workStartDate"
                placeholder="Labora desde"
                value={moment(negotiationClient.workStartDate).format(
                  "YYYY-MM-DD"
                )}
              />
            </Col>

            <Col xs="12" sm="8" md="6">
              <Label>Moneda</Label>
              <Input
                readOnly
                type="select"
                name="debtorCurrency"
                value={negotiationClient.debtorCurrency ?? ""}
              >
                {applicationOptions.currencies}
              </Input>
            </Col>

            <Col xs="12" sm="8" md="6">
              <Label htmlFor="grossMonthlyIncome">Ingresos Mensuales</Label>
              <Input
                readOnly
                type="number"
                name="grossMonthlyIncome"
                value={negotiationClient.grossMonthlyIncome ?? 0}
              />
            </Col>
          </Row>

          <Row>
            <Col xs="12" sm="8" md="6">
              <Label htmlFor="creationDate">Fecha de Consulta</Label>
              <Input
                readOnly
                type="date"
                name="prequalificationDate"
                value={moment(negotiationClient.prequalificationDate).format(
                  "YYYY-MM-DD"
                )}
              />
            </Col>

            <Col xs="12" sm="8" md="6">
              <Label htmlFor="nextPrequalificationDate">Aplica Hasta</Label>
              <Input
                readOnly
                type="date"
                name="nextPrequalificationDate"
                value={moment(negotiationClient.nextPrequalificationDate).format(
                  "YYYY-MM-DD"
                )}
              />
            </Col>
          </Row>

          <Row>
            <Col xs="12" sm="8" md="6">
              <FormGroup>
                <Label htmlFor="grossMonthlyIncome">Monto Lps</Label>
                <Input
                  readOnly
                  type="number"
                  name="prequalificationAmount"
                  value={negotiationClient.prequalificationAmount ?? 0}
                />
              </FormGroup>
            </Col>

            <Col xs="12" sm="8" md="6">
              <Label htmlFor="debtsAmount">Valor Endeudamiento</Label>
              <Input
                readOnly
                type="number"
                name="debtsAmount"
                value={negotiationClient.debtsAmount ?? 0}
              />
            </Col>
          </Row>

          <Row>
            <Col xs="12" sm="8" md="6">
              <FormGroup>
                <Label>Parentesco</Label>
                <Input
                  readOnly
                  type="select"
                  name="relationship"
                  value={negotiationClient.relationship ?? ""}
                >
                  {applicationOptions.relationships}
                </Input>
              </FormGroup>
            </Col>

            {/* <Col xs="12" sm="8" md="6">
            <FormGroup check>
              <Label check>
                <Input
                  type="checkbox"
                  name="hasDebts"
                  checked={negotiationClient.hasDebts}
                  onChange={handleInputChange}
                  required
                />{" "}
                Endeudamiento
              </Label>
            </FormGroup>
          </Col> */}
          </Row>


          <Row>
            <Col>
              <legend>Informacion del CoDeudor</legend>
            </Col>
          </Row>

          <Col lg="12">
            <FormGroup>
              <Row>
                <strong>{"Nombre : "}</strong>
                {getClientNameCoDeudor()}
              </Row>

              <Row>
                <strong>{"Numeros de Telefono : "}</strong>
                {getPhoneNumbersCoDeudor()}
              </Row>

              <Row>
                <strong>{"Email : "}</strong>
                {` ${coDebtor?.email}`}
              </Row>

              <Row>
                <Col xs="12" sm="8" md="6">
                  <Label check>
                    <Input
                      readOnly
                      type="checkbox"
                      name="ownHome"
                      checked={coDebtor?.ownHome}
                    />{" "}
                  Posee Casa
                </Label>
                </Col>

                <Col xs="12" sm="8" md="6">
                  <Label check>
                    <Input
                      readOnly
                      type="checkbox"
                      name="firstHome"
                      checked={coDebtor?.firstHome}
                    />{" "}
                  Primera Casa
                </Label>
                </Col>

                <Col xs="12" sm="8" md="6">
                  <Label check>
                    <Input
                      readOnly
                      type="checkbox"
                      name="contributeToRap"
                      checked={coDebtor?.contributeToRap}
                    />{" "}
                  Aporta al RAP
                </Label>
                </Col>
                <Col xs="12" sm="8" md="6">
                  <Label check>
                    <Input
                      readOnly
                      type="checkbox"
                      name="preQualify"
                      checked={coDebtor?.preQualify}
                    />{" "}
                  Pre Califica
                </Label>
                </Col>
              </Row>
            </FormGroup>
          </Col>

          <Row>
            <Col xs="12" sm="8" md="6">
              <Label htmlFor="email">Lugar de Trabajo</Label>
              <Input
                readOnly
                type="text"
                name="workplace"
                value={coDebtor?.workplace ?? ""}
              />
            </Col>

            <Col xs="12" sm="8" md="6">
              <Label htmlFor="workStartDate">Trabaja desde</Label>
              <Input
                readOnly
                type="date"
                name="workStartDate"
                placeholder="Labora desde"
                value={moment(coDebtor?.workStartDate).format(
                  "YYYY-MM-DD"
                )}
              />
            </Col>

            <Col xs="12" sm="8" md="6">
              <Label>Moneda</Label>
              <Input
                readOnly
                type="select"
                name="debtorCurrency"
                value={coDebtor?.debtorCurrency ?? ""}
              >
                {applicationOptions.currencies}
              </Input>
            </Col>

            <Col xs="12" sm="8" md="6">
              <Label htmlFor="grossMonthlyIncome">Ingresos Mensuales</Label>
              <Input
                readOnly
                type="number"
                name="grossMonthlyIncome"
                value={coDebtor?.grossMonthlyIncome ?? 0}
              />
            </Col>
          </Row>

          <Row>
            <Col xs="12" sm="8" md="6">
              <Label htmlFor="creationDate">Fecha de Consulta</Label>
              <Input
                readOnly
                type="date"
                name="prequalificationDate"
                value={moment(coDebtor?.prequalificationDate).format(
                  "YYYY-MM-DD"
                )}
              />
            </Col>

            <Col xs="12" sm="8" md="6">
              <Label htmlFor="nextPrequalificationDate">Aplica Hasta</Label>
              <Input
                readOnly
                type="date"
                name="nextPrequalificationDate"
                value={moment(coDebtor?.nextPrequalificationDate).format(
                  "YYYY-MM-DD"
                )}
              />
            </Col>
          </Row>

          <Row>
            <Col xs="12" sm="8" md="6">
              <FormGroup>
                <Label htmlFor="grossMonthlyIncome">Monto Lps</Label>
                <Input
                  readOnly
                  type="number"
                  name="prequalificationAmount"
                  value={coDebtor?.prequalificationAmount ?? 0}
                />
              </FormGroup>
            </Col>

            <Col xs="12" sm="8" md="6">
              <Label htmlFor="debtsAmount">Valor Endeudamiento</Label>
              <Input
                readOnly
                type="number"
                name="debtsAmount"
                value={coDebtor?.debtsAmount ?? 0}
              />
            </Col>
          </Row>

          <Row>
            <Col xs="12" sm="8" md="6">
              <FormGroup>
                <Label>Parentesco</Label>
                <Input
                  readOnly
                  type="select"
                  name="relationship"
                  value={negotiationClient?.relationship ?? ""}
                >
                  {applicationOptions.relationships}
                </Input>
              </FormGroup>
            </Col>

            {/* <Col xs="12" sm="8" md="6">
            <FormGroup check>
              <Label check>
                <Input
                  type="checkbox"
                  name="hasDebts"
                  checked={negotiationClient.hasDebts}
                  onChange={handleInputChange}
                  required
                />{" "}
                Endeudamiento
              </Label>
            </FormGroup>
          </Col> */}
          </Row>



          <Row>
            <Col>
              <legend>Informacion General</legend>
            </Col>
          </Row>

          <Row>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="creationDate">Fecha de Creacion: value={moment(negotiation?.creationDate).format("YYYY-MM-DD")}</Label>
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="negotiationStartDate">Fecha de Inicio: {moment(negotiation?.negotiationStartDate).format(
                  "YYYY-MM-DD"
                )}</Label>
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label>Tipo de Contacto</Label>
                <Input
                  readOnly
                  type="select"
                  name="contactTypeId"
                  value={negotiation?.contactTypeId ?? 0}
                >
                  {applicationOptions.contactTypes}
                </Input>
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label>Servicio</Label>
                <Input
                  readOnly
                  type="select"
                  name="supplierServiceId"
                  value={negotiation?.supplierServiceId ?? 0}
                >
                  {applicationOptions.supplierServices}
                </Input>
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label>Proyecto</Label>
                <Input
                  readOnly
                  type="select"
                  name="projectId"
                  pattern="[0-9]*"
                  value={negotiation?.projectId ?? 0}
                >
                  {applicationOptions.projects}
                </Input>
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label>Modelo</Label>
                <Input
                  readOnly
                  type="select"
                  name="houseDesignId"
                  pattern="[0-9]*"
                  value={negotiation?.houseDesignId ?? 0}
                >
                  {houseDesigns}
                </Input>
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label>Asesor de Ventas</Label>
                <Input
                  readOnly
                  type="select"
                  name="salesAdvisorId"
                  value={negotiation?.salesAdvisorId ?? 0}
                  required
                >
                  {applicationOptions.salesAdvisors}
                </Input>
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col>
              <FormGroup>
                <Label htmlFor="comments">Comentarios</Label>
                <Input
                  readOnly
                  type="text"
                  name="comments"
                  value={negotiation?.comments ?? ""}
                  required
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label>Institucion Financiera</Label>
                <Input
                  readOnly
                  type="select"
                  name="financialInstitutionId"
                  value={negotiation?.financialInstitutionId ?? 0}
                  required
                >
                  {applicationOptions.financialInstitutions}
                </Input>
              </FormGroup>
            </Col>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="creditOfficer">Oficial de Credito</Label>
                <Input
                  readOnly
                  type="text"
                  name="creditOfficer"
                  value={negotiation?.creditOfficer ?? ""}
                  required
                />
              </FormGroup>
            </Col>
          </Row>



          <Row>
            <Col>
              <legend>Pre Calificacion</legend>
            </Col>
          </Row>

          <Row>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label>Tipo de Fondos</Label>
                <Input
                  readOnly
                  type="select"
                  name="fundTypeId"
                  value={financialPrequalification?.fundTypeId ?? 0}
                >
                  {applicationOptions.fundTypes}
                </Input>
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="interestRate">Tasa%</Label>
                <Input
                  readOnly
                  type="number"
                  name="interestRate"
                  value={financialPrequalification?.interestRate ?? 0}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="interestRate">Valor a Financiar</Label>
                <Input
                  readOnly
                  type="number"
                  name="loanAmount"
                  value={financialPrequalification?.loanAmount ?? 0}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="loanTerm">Tiempo de Financiamiento</Label>
                <Input
                  readOnly
                  type="number"
                  name="loanTerm"
                  value={financialPrequalification?.loanTerm ?? 0}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="downPaymentPercent">% Prima</Label>
                <Input
                  readOnly
                  type="number"
                  name="downPaymentPercent"
                  value={financialPrequalification?.downPaymentPercent ?? 0}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="downPayment">Prima Disponible Lps</Label>
                <Input
                  readOnly
                  type="number"
                  name="downPayment"
                  value={financialPrequalification?.downPayment ?? 0}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="monthlyPayment">Cuota Mensual Lps</Label>
                <Input
                  readOnly
                  type="number"
                  name="monthlyPayment"
                  value={financialPrequalification?.monthlyPayment ?? 0}
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col lg="12">
              <FormGroup>
                <Label htmlFor="comments">Comentarios Oficial</Label>
                <Input
                  readOnly
                  type="text"
                  name="comments"
                  value={financialPrequalification?.comments ?? ""}
                />
              </FormGroup>
            </Col>
          </Row>




          <Row>
            <Col>
              <legend>Aprobacion de Credito</legend>
            </Col>
          </Row>

          <Row>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="creationDate">Fecha Ingreso</Label>
                <Input
                  readOnly
                  type="date"
                  name="creationDate"
                  value={moment(financialApproval?.creationDate).format(
                    "YYYY-MM-DD"
                  )}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <Row>
                <FormGroup check>
                  <Label check>
                    <Input
                      readOnly
                      type="checkbox"
                      name="jointloan"
                      checked={financialApproval?.jointloan}
                    />{" "}
                Mancomunado
              </Label>
                  <Label />
                </FormGroup>
              </Row>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label>Status Credito</Label>
                <Input
                  readOnly
                  type="select"
                  name="approvalState"
                  value={financialApproval?.approvalState ?? "PENDIENTE"}
                >
                  {applicationOptions.creditSates}
                </Input>
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="approvalDate">Fecha de Aprobado</Label>
                <Input
                  readOnly
                  type="date"
                  name="approvalDate"
                  value={moment(financialApproval?.approvalDate).format(
                    "YYYY-MM-DD"
                  )}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="rejectionDate">Fecha Denegado</Label>
                <Input
                  readOnly
                  type="date"
                  name="rejectionDate"
                  value={moment(financialApproval?.rejectionDate).format(
                    "YYYY-MM-DD"
                  )}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="rejectionReason">Porque Fue Denegado</Label>
                <Input
                  readOnly
                  type="text"
                  name="rejectionReason"
                  value={financialApproval?.rejectionReason ?? ""}
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col lg="12">
              <FormGroup>
                <Label htmlFor="creditOfficerComments">Comentarios Oficial</Label>
                <Input
                  readOnly
                  type="text"
                  name="creditOfficerComments"
                  value={financialApproval?.creditOfficerComments ?? ""}
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col lg="12">
              <FormGroup>
                <Label htmlFor="salesAdvisorComments">
                  Comentarios Asesor Venta
            </Label>
                <Input
                  readOnly
                  type="text"
                  name="salesAdvisorComments"
                  value={financialApproval?.salesAdvisorComments ?? ""}
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="startDate">Fecha de Inicio</Label>
                <Input
                  readOnly
                  type="date"
                  name="startDate"
                  value={moment(financialApproval?.startDate).format("YYYY-MM-DD")}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="rejectionDate">Fecha de Entrega</Label>
                <Input
                  readOnly
                  type="date"
                  name="dueDate"
                  value={moment(financialApproval?.dueDate).format("YYYY-MM-DD")}
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col lg="12">
              <FormGroup>
                <Label htmlFor="followUp">Seguimiento</Label>
                <Input
                  readOnly
                  type="text"
                  name="followUp"
                  value={financialApproval?.followUp ?? ""}
                />
              </FormGroup>
            </Col>
          </Row>



          <Row>
            <Col>
              <legend>Contrato</legend>
            </Col>
          </Row>

          <Row>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="contractSigningDate">Fecha Firma</Label>
                <Input
                  readOnly
                  type="date"
                  name="contractSigningDate"
                  value={moment(negotiationContract?.contractSigningDate).format(
                    "YYYY-MM-DD"
                  )}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="effectiveDate">Fecha Vigencia</Label>
                <Input
                  readOnly
                  type="date"
                  name="effectiveDate"
                  value={moment(negotiationContract?.effectiveDate).format(
                    "YYYY-MM-DD"
                  )}
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label>Constructora</Label>
                <Input
                  readOnly
                  type="select"
                  name="builderCompanyId"
                  value={negotiationContract?.builderCompanyId ?? 0}
                >
                  {applicationOptions.builders}
                </Input>
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="blockNumber">Bloque</Label>
                <Input
                  readOnly
                  type="text"
                  name="blockNumber"
                  value={negotiationContract?.blockNumber ?? ""}
                  required
                />
              </FormGroup>
            </Col>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="lotNumber">Lote</Label>
                <Input
                  readOnly
                  type="text"
                  name="lotNumber"
                  value={negotiationContract?.lotNumber ?? ""}
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="constructionMeters">Metros</Label>
                <Input
                  readOnly
                  type="number"
                  name="constructionMeters"
                  value={negotiationContract?.constructionMeters ?? 0}
                />
              </FormGroup>
            </Col>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="landSize">Terreno</Label>
                <Input
                  readOnly
                  type="number"
                  name="landSize"
                  value={negotiationContract?.landSize ?? 0}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="registration">Matricula</Label>
                <Input
                  readOnly
                  type="text"
                  name="registration"
                  value={negotiationContract?.registration ?? ""}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="reservationValue">Valor de Reserva</Label>
                <Input
                  readOnly
                  type="number"
                  name="reservationValue"
                  value={negotiationContract?.reservationValue ?? 0}
                />
              </FormGroup>
            </Col>
          </Row>

          <Row>
            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="downPaymentValue">Valor de Prima</Label>
                <Input
                  readOnly
                  type="number"
                  name="downPaymentValue"
                  value={negotiationContract?.downPaymentValue ?? 0}
                />
              </FormGroup>
            </Col>

            <Col md="3" sm="6" xs="12">
              <FormGroup>
                <Label htmlFor="downPaymentMethod">Forma de Pago Prima</Label>
                <Input
                  readOnly
                  type="text"
                  name="downPaymentMethod"
                  value={negotiationContract?.downPaymentMethod ?? ""}
                />
              </FormGroup>
            </Col>
          </Row>
        </CardBody>
      </div>
    );
  }
}

export default Negotiation;
