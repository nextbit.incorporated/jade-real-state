import React from "react";
import logo from "../../../../assets/img/brand/logo.svg";
import moment from "moment";
import { CardBody, CardHeader, Card, Container, Row, Col } from "reactstrap";


class Contract extends React.Component {
    getClientName = (client) => {
        if (!client) {
            return "";
        }

        const name = client
            ? (client.firstName || "") +
            " " +
            (client.middleName || "") +
            " " +
            (client.firstSurname || "") +
            " " +
            (client.secondSurname || "")
            : "";

        return name;
    };

    render() {

        const negotiation = this.props.negotiation;
        const client = negotiation.principal;
        const coDebtor = negotiation.coDebtor;
        return (<div>
            <CardBody>
                <CardBody>
                    <div style={{ display: "block" }}>
                        <div align="right">
                            <img
                                src={logo}
                                alt=""
                                style={{ width: "80px", height: "80px" }}
                            ></img>
                        </div>
                        <h1 style={{ textAlign: "center" }}>
                            <u>
                                <b>Inmobiliaria Jade</b>
                            </u>
                        </h1>
                    </div>
                    <div style={{ display: "block" }}>
                        <h2 style={{ textAlign: "center" }}>
                            Contrato cliente codigo: {client && client.clientCode}
                        </h2>
                    </div>
                </CardBody>
                <CardHeader>
                    <strong>Contrato</strong>
                </CardHeader>
                <Card>
                    <CardHeader><strong>Datos Cliente</strong></CardHeader>
                    <label><strong>Nombre del titular: </strong>{this.getClientName(client)}</label>
                    <label><strong>Identidad del titular: </strong>{client.identificationCard || '_____________________________'}</label>
                    <label><strong>Nombre del Co-Deudor: </strong>{this.getClientName(coDebtor)}</label>
                    <label><strong>Identidad del Co-Deudor: </strong>{coDebtor.identificationCard || '___________________________'}</label>
                </Card>
                <Card>
                    <CardHeader><strong>Datos de compra</strong></CardHeader>
                    <label><strong>Tipo de financiamiento: </strong>{negotiation.negotiationContract.downPaymentMethod || '______________________'}</label>
                    <label><strong>Contado:</strong> {negotiation.negotiationContract.downPaymentValue || '______________________'}</label>
                    <CardHeader><strong>Si es con Financiamiento:</strong></CardHeader>
                    <label><strong>Instituacion Financiera: </strong> {negotiation.financialInstitution || '____________________________'}</label>
                    <label><strong>Oficial financiero: </strong> {negotiation.creditOfficer || '_______________________'}</label>
                </Card>
                <Card>
                    <CardHeader><strong>Datos del proyecto</strong></CardHeader>
                    <label><strong>Fecha de firma: </strong>{negotiation.negotiationContract.contractSigningDate ? moment(negotiation.negotiationContract.contractSigningDate).format("L") : '______________________'}</label>
                    <label><strong>Fecha de vigencia: </strong>{negotiation.negotiationContract.effectiveDate ? moment(negotiation.negotiationContract.effectiveDate).format("L") : '______________________'}</label>
                    <label><strong>Constructora: </strong>{negotiation.negotiationContract.builderCompany.businessName || '______________________'}</label>
                    <label><strong>Asesor de ventas: </strong>{negotiation.salesAdvisor}</label>
                    <label><strong>Proyecto: </strong>{negotiation.project}</label>
                    <label><strong>Bloque: </strong>{negotiation.negotiationContract.blockNumber || '______________________'}</label>
                    <label><strong>Lote: </strong>{negotiation.negotiationContract.lotNumber || '______________________'}</label>
                    <label><strong>Construccion mts: </strong>{negotiation.negotiationContract.constructionMeters || '______________________'}</label>
                    <label><strong>Terreno mts: </strong>{negotiation.negotiationContract.landSize || '______________________'}</label>
                    <label><strong>Matricula : </strong>{negotiation.negotiationContract.registration || '______________________'}</label>
                    <label><strong>Reserva : </strong>{negotiation.negotiationContract.reservationValue || '______________________'}</label>
                    <label><strong>Valor de prima : </strong>{negotiation.negotiationContract.downPaymentValue || '______________________'}</label>
                    <label><strong>Forma de pago prima : </strong>{negotiation.negotiationContract.downPaymentMethod || '______________________'}</label>
                    <label><strong>Valor del terreno : </strong>{(negotiation.negotiationContract.landValue === undefined || negotiation.negotiationContract.landValue === 0) ? '______________________' : negotiation.negotiationContract.landValue}</label>
                    <label><strong>Valor de Construccion : </strong>{(negotiation.negotiationContract.constructionValue === undefined || negotiation.negotiationContract.constructionValue === 0) ? '______________________' : negotiation.negotiationContract.constructionValue}</label>
                    <label><strong>Valor Mejoras : </strong>{(negotiation.negotiationContract.improvementsValue === undefined || negotiation.negotiationContract.improvementsValue === 0) ? '______________________' : negotiation.negotiationContract.improvementsValue}</label>
                    <label><strong>Valor total vivienda : </strong>{(negotiation.negotiationContract.houseTotalValue === undefined || negotiation.negotiationContract.houseTotalValue === 0) ? '______________________' : negotiation.negotiationContract.houseTotalValue}</label>
                    <label><strong>Vivienda especificaciones: </strong>{negotiation.houseSpecifications || '______________________'}</label>
                </Card>
                <CardBody>
                    <Container>
                        <Row>
                            <Col style={{ textAlign: 'center' }}>_____________________________________</Col>
                            <Col style={{ width: '400px' }}></Col>
                            <Col style={{ textAlign: 'center' }}>_____________________________________</Col>
                        </Row>
                        <Row>
                            <Col style={{ textAlign: 'center' }}>Inmobiliaria Jade</Col>
                            <Col style={{ width: '400px' }}></Col>
                            <Col style={{ textAlign: 'center' }}>Cliente</Col>
                        </Row>
                    </Container>
                </CardBody>
                <CardBody>
                    <div style={{ display: "block" }}>
                        <h3 style={{ textAlign: "right" }}>
                            Jade Real Estate - {moment(Date.now()).format("L")}
                        </h3>
                    </div>
                </CardBody>
            </CardBody>
        </div>);
    }


}


export default Contract;