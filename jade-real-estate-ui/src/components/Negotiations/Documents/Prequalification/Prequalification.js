import React from "react";
import logo from "../../../../assets/img/brand/logo.svg";
import moment from "moment";
import { CardBody, CardHeader } from "reactstrap";

class Prequalification extends React.Component {
  getClientName = (client) => {
    if (!client) {
      return "";
    }

    const name = client
      ? (client.firstName || "") +
      " " +
      (client.middleName || "") +
      " " +
      (client.firstSurname || "") +
      " " +
      (client.secondSurname || "")
      : "";

    return name;
  };

  render() {
    const negotiation = this.props.negotiation;
    const client = negotiation.principal;
    const coDebtor = negotiation.coDebtor;

    return (
      <div>
        <CardBody>
          <div style={{ display: "block" }}>
            <div align="right">
              <img
                src={logo}
                alt=""
                style={{ width: "80px", height: "80px" }}
              ></img>
            </div>
            <h1 style={{ textAlign: "center" }}>
              <u>
                <b>Inmobiliaria Jade</b>
              </u>
            </h1>
          </div>
          <div style={{ display: "block" }}>
            <h2 style={{ textAlign: "center" }}>
              Pre Calificacion Con La Institucion Financiera
            </h2>
          </div>
        </CardBody>
        <CardBody>
          <div style={{ display: "block" }}>
            <h3 style={{ textAlign: "left" }}>
              <b>Nombre: {this.getClientName(client)} </b>
            </h3>
            <h3 style={{ textAlign: "left" }}>
              <b>
                Identidad: {(client && client.identificationCard) || "N/D"}{" "}
              </b>
            </h3>
            <h4 style={{ textAlign: "left" }}>
              Nacionalidad: {(client && client.nationality) || "N/D"}{" "}
            </h4>
          </div>
        </CardBody>
        <CardBody>
          <CardHeader>
            <strong>Información personal</strong>
          </CardHeader>
          <table border="1" cellSpacing="0" cellPadding="4">
            <tbody>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Telefono: </b> {(client && client.phoneNumber) || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Celular: </b> {(client && client.cellPhoneNumber) || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Correo: </b>
                  {client.email || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Dirección: </b>
                  {(client && client.homeAddress) || "N/D"}
                </td>
              </tr>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Trabajo Actual: </b>
                  {(client && client.workplace) || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Ingreso Actual: </b>
                  {(client && client.grossMonthlyIncome) || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Agente de Ventas: </b>
                  {negotiation.salesAdvisor || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Oficial De Credito: </b>
                  {negotiation.creditOfficer || "N/D"}
                </td>
              </tr>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Financiera: </b>
                  {negotiation.financialInstitution || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Comentarios: </b>
                  {negotiation.comments || "N/D"}
                </td>
              </tr>
            </tbody>
          </table>
        </CardBody>
        <CardBody>
          <CardHeader>
            <strong>Información Coudeudor</strong>
          </CardHeader>
          <table border="1" cellSpacing="0" cellPadding="4">
            <tbody>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Codeudor: </b>
                  {this.getClientName(coDebtor) || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Telefono: </b>
                  {(coDebtor && coDebtor.phoneNumber) || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Celular: </b>
                  {(coDebtor && coDebtor.cellPhoneNumber) || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Correo: </b>
                  {(coDebtor && coDebtor.email) || "N/D"}
                </td>
              </tr>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Trabajo: </b>
                  {(coDebtor && coDebtor.workplace) || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Ingreso: </b>
                  {(coDebtor && coDebtor.grossMonthlyIncome) || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Identidad: </b>
                  {(coDebtor && coDebtor.identificationCard) || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Nacionalidad: </b>
                  {(coDebtor && coDebtor.nationality) || "N/D"}
                </td>
              </tr>
            </tbody>
          </table>
        </CardBody>
        <CardBody>
          <CardHeader>
            <strong>Información Financiera</strong>
          </CardHeader>
          <table border="1" cellSpacing="0" cellPadding="4">
            <tbody>
              <tr>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Proyecto: </b>
                  {negotiation.project || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Contacto: </b>
                  {negotiation.contactType || "N/D"}
                </td>
                <td style={{ padding: "0 25px 0 15px" }}>
                  <b>Financiera: </b>
                  {negotiation.financialInstitution || "N/D"}
                </td>
              </tr>
            </tbody>
          </table>
        </CardBody>

        <CardBody>
          <div style={{ display: "block" }}>
            <h2 style={{ textAlign: "right" }}>
              Jade Real Estate - {moment(Date.now()).format("L")}
            </h2>
          </div>
        </CardBody>
      </div>
    );
  }
}

export default Prequalification;
