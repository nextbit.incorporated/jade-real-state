import React, { useState, useEffect } from "react";
import styled from "styled-components";
import moment from "moment";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
} from "reactstrap";
import ReactLoading from "react-loading";
import PrintDocumentActions from "./PrintDocumentActions/PrintDocumentActions";

const Root = styled.div`
  height: 57vh;
  max-height: 57vh;
  overflow-y: auto;
  overflow-x: auto;
`;

const initialSearchState = {
  value: "",
  typingTimeout: 0,
};

const NegotiationTable = (props) => {
  const {
    negotiations,
    showNewDialog,
    fetchNegotiationPaged,
    editRow,
    deleteNegotiation,
    nextPage,
    prevPage,
    previewNotification,
    loadingStatus,
    showNegotiationPhases,
    previewContactPrint
  } = props;

  const [searchState, setSearchState] = useState(initialSearchState);

  useEffect(() => {}, [negotiations]);

  function nextStepPaged() {
    nextPage(negotiations.pageIndex + 1, searchState.value);
  }

  function prevStepPaged() {
    prevPage(negotiations.pageIndex - 1, searchState.value);
  }

  function handleValueChange(e) {
    if (searchState.typingTimeout) {
      clearTimeout(searchState.typingTimeout);
    }

    const query = e.target.value;

    setSearchState({
      value: query,
      typingTimeout: setTimeout(function () {
        fetchNegotiationPaged(query, 0);
      }, 500),
    });
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
    textAlign: "center",
  };

  const createDocumentActions = (negotiation) => {
    const actions = [
      {
        id: "1",
        title: "Ficha de Negociacion",
        onClick: () => previewNotification(negotiation),
      },
      // {
      //   id: "2",
      //   title: "Imprimir Pre Calificacion",
      //   onClick: () => previewNotification(negotiation),
      // },
      // {
      //   id: "3",
      //   title: "Imprimir  Aprobacion",
      //   onClick: () => previewNotification(negotiation),
      // },
      {
        id: "4",
        title: "Fomato de  Contrato",
        onClick: () => previewContactPrint(negotiation),
      },
    ];

    return actions;
  };

  function obtenerTabla(e) {
    if (loadingStatus) {
      return (
        <FormGroup row>
          <Col md="4" sm="6" xs="6"></Col>
          <Col md="4" sm="6" xs="6">
            <ReactLoading
              type="bars"
              color="#5DBCD2"
              height={"30%"}
              width={"30%"}
            />
          </Col>
          <Col md="4" sm="6" xs="6"></Col>
        </FormGroup>
      );
    } else {
      return (
        <FormGroup row>
          <Col>
            <Root>
              <Table hover bordered striped responsive size="sm">
                <thead>
                  <tr>
                    <th style={thStyle}>Editar</th>
                    <th style={thStyle}>Borrar</th>
                    <th style={thStyle}>Ficha</th>
                    <th style={thStyle}>Fases</th>

                    <th style={thStyle}>Primer Nombre</th>
                    <th style={thStyle}>Segundo Nombre</th>
                    <th style={thStyle}>Primer Apellido</th>
                    <th style={thStyle}>Segundo Apellido</th>
                    <th style={thStyle}>Identidad</th>
                    <th style={thStyle}>Telefono</th>
                    <th style={thStyle}>Celular</th>
                    <th style={thStyle}>Correo</th>
                    <th style={thStyle}>Trabajo</th>
                    <th style={thStyle}>Inicio Trabajo</th>
                    <th style={thStyle}>Moneda</th>
                    <th style={thStyle}>Ingreso Mensual</th>
                    <th style={thStyle}>Dirección</th>
                    <th style={thStyle}>Registro</th>
                    <th style={thStyle}>Posee vivienda</th>
                    <th style={thStyle}>Contribuye al RAP</th>
                    <th style={thStyle}>Comentario</th>
                    <th style={thStyle}>Oficial de credito</th>
                    <th style={thStyle}>Agente</th>
                    <th style={thStyle}>Financiera</th>
                    <th style={thStyle}>Contacto</th>
                    <th style={thStyle}>Proyecto</th>
                    <th style={thStyle}>Tipo de servicio</th>
                    <th style={thStyle}>Primer Nombre Codeudor</th>
                    <th style={thStyle}>Segundo Nombre Codeudor</th>
                    <th style={thStyle}>Primer Apellido Codeudor</th>
                    <th style={thStyle}>Segundo Apellido Codeudor</th>
                    <th style={thStyle}>Nacionalidad Codeudor</th>
                    <th style={thStyle}>Identidad Codeudor</th>
                    <th style={thStyle}>Telefono Codeudor</th>
                    <th style={thStyle}>Celular Codeudor</th>
                    <th style={thStyle}>Correo Codeudor</th>
                    <th style={thStyle}>Trabajo Codeudor</th>
                    <th style={thStyle}>Moneda Codeudor</th>
                    <th style={thStyle}>Ingreso Mensual Codeudor</th>
                  </tr>
                </thead>
                <tbody>
                  {negotiations.negotiations.map((negotiation) => (
                    <tr key={negotiation.id}>
                      <td>
                        <Button
                          block
                          color="warning"
                          onClick={() => {
                            editRow(negotiation);
                          }}
                        >
                          Editar
                        </Button>
                      </td>
                      <td>
                        <Button
                          block
                          color="danger"
                          onClick={() => deleteNegotiation(negotiation.id)}
                        >
                          Borrar
                        </Button>
                      </td>
                      <td>
                        <PrintDocumentActions
                          actions={createDocumentActions(negotiation)}
                        ></PrintDocumentActions>
                        {/* <Button
                        block
                        color="success"
                        onClick={() => previewNotification(negotiation)}
                      >
                        Imprimir
                      </Button> */}
                      </td>
                      <td>
                        <Button
                          block
                          color="success"
                          onClick={() => showNegotiationPhases(negotiation)}
                        >
                          Proceso
                        </Button>
                      </td>

                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.principal
                          ? negotiation.principal.firstName
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.principal
                          ? negotiation.principal.middleName
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.principal
                          ? negotiation.principal.firstSurname
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.principal
                          ? negotiation.principal.secondSurname
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.principal
                          ? negotiation.principal.identificationCard
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.principal
                          ? negotiation.principal.phoneNumber
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.principal
                          ? negotiation.principal.cellPhoneNumber
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.principal
                          ? negotiation.principal.email
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.principal
                          ? negotiation.principal.worklace
                          : ""}
                      </td>
                      <td>
                        {negotiation.principal
                          ? moment(negotiation.principal.workStartDate).format(
                              "L"
                            )
                          : ""}
                      </td>
                      <td>
                        {negotiation.principal
                          ? negotiation.principal.currency
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.principal
                          ? negotiation.principal.grossMonthlyIncome
                          : 0}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.principal
                          ? negotiation.principal.homeAddress
                          : ""}
                      </td>
                      <td>
                        {moment(negotiation.negotiationStartDate).format("L")}
                      </td>
                      <td nowrap="true">{negotiation.ownHome ? "Si" : ""}</td>
                      <td nowrap="true">
                        {negotiation.principal
                          ? negotiation.principal.contributeToRap
                            ? "Si"
                            : "No"
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.comments}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.creditOfficer}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.salesAdvisor
                          ? negotiation.salesAdvisor
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.financialInstitution
                          ? negotiation.financialInstitution
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.contactType ? negotiation.contactType : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.project ? negotiation.project : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.service ? negotiation.service : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor
                          ? negotiation.coDebtor.firstName
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor
                          ? negotiation.coDebtor.middleName
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor
                          ? negotiation.coDebtor.firstSurname
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor
                          ? negotiation.coDebtor.secondSurname
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor
                          ? negotiation.coDebtor.nationality
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor
                          ? negotiation.coDebtor.identificationCard
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor
                          ? negotiation.coDebtor.phoneNumber
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor
                          ? negotiation.coDebtor.cellPhoneNumber
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor ? negotiation.coDebtor.email : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor
                          ? negotiation.coDebtor.workplace
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor
                          ? negotiation.coDebtor.currency
                          : ""}
                      </td>
                      <td style={{ whiteSpace: "nowrap" }}>
                        {negotiation.coDebtor
                          ? negotiation.coDebtor.grossMonthlyIncome
                          : 0}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Root>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStepPaged}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {negotiations.pageCount}</strong>
                </h5>
              </div>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Actual: {negotiations.pageIndex}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStepPaged}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      );
    }
  }

  const userRows =
    negotiations === null ? (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>Buscando...</td>
          </tr>
        </thead>
      </Table>
    ) : negotiations.length === 0 ? (
      <div>
        <FormGroup row>
          <Col md="6">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Valor
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={searchState.value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
          <Col />

          <Col xs="auto">
            <Button
              type="button"
              color="success"
              onClick={() => showNewDialog()}
            >
              <i className="fa fa-new" /> Nuevo Tramite
            </Button>
          </Col>
        </FormGroup>  
        <FormGroup row>
        <Col>
        <Root>
        <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>No se encontraron registros.</td>
          </tr>
        </thead>
      </Table>
        </Root>
        </Col>
            
        </FormGroup>
        
       
      </div>
      
    ) : (
      <div>
        <FormGroup row>
          <Col md="6">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Valor
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={searchState.value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
          <Col />

          <Col xs="auto">
            <Button
              type="button"
              color="success"
              onClick={() => showNewDialog()}
            >
              <i className="fa fa-new" /> Nuevo Tramite
            </Button>
          </Col>
        </FormGroup>

        {obtenerTabla()}
      </div>
    );
  return userRows;
};

export default NegotiationTable;
