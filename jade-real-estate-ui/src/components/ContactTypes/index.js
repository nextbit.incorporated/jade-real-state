import React, { useState, useEffect } from "react";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
} from "reactstrap";

import ContactTypeControlTable from "./contactTypeControlTable";
import ContactTypeDetails from "./contactTypeDetails";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";
const heightStyle = {
  height: "500px",
};

const initialClientState = {
  step: 1,
  editMode: "None",
  currentContactType: {
    id: "",
    name: "",
  },
};

const ContactTypes = (props) => {
  const [contactTypeState, setContactTypeState] = useState(initialClientState);
  const [contactTypes, setContactTypes] = useState([]);

  function fetchContactTypes(query, step) {
    const index = step === undefined || step === null ? 0 : step;
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard"></Redirect>;
    }
    if (query === null) {
      var url = `contact/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setContactTypes(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `contact/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setContactTypes(res.data);
      });
    }
  }

  const editRow = (contactType) => {
    setContactTypeState({
      ...contactTypeState,
      editMode: "Editing",
      currentContactType: contactType,
      step: 2,
    });
  };

  function nextPage(step) {
    fetchContactTypes(null, step);
  }

  function prevPage(step) {
    fetchContactTypes(null, step);
  }

  function nextStep(contactType) {
    const { step } = contactTypeState;

    if (contactType === null || contactType === undefined) {
      contactType = contactTypeState.currentContactType;
    }
    setContactTypeState({
      ...contactTypeState,
      currentContactType: contactType,
      step: step + 1,
    });
  }

  function prevStep() {
    const { step } = contactTypeState;

    setContactTypeState({ ...contactTypeState, step: step - 1 });
  }

  useEffect(() => {
    fetchContactTypes(null);
    // fectRoles();
  }, []);

  function add(newContactType) {
    if (!newContactType || !newContactType.name) {
      return;
    }
    const url = `contact`;

    API.post(url, newContactType)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar nueva tipo de contacto");
        } else {
          toast("Tipo de contacto agregado satisfactoriamente");
          // setUsers([...users.users, userAdded]);
          fetchContactTypes(null);
          setContactTypeState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  function update(contactType) {
    if (!contactType || !contactType.name) {
      return;
    }
    const url = `contact`;
    API.put(url, contactType)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar actualizar tipo de contacto");
        } else {
          toast("Tipo de contacto actualizada satisfactoriamente");

          // setUsers([...users.users, userAdded]);
          fetchContactTypes(null);
          setContactTypeState(initialClientState);
        }
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  }

  const deleteRegister = (id) => {
    const url = `contact?Id=${id}`;

    API.delete(url)
      .then((res) => {
        if (res.data !== "") {
          toast("error al intentar eliminar tipos de contacto");
          return;
        }
        fetchContactTypes(null);
        setContactTypeState(initialClientState);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col />
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStep()}
                >
                  <i className="fa fa-new" /> Agregar tipo de contacto
                </Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <ContactTypeControlTable
                  fetchContactTypes={fetchContactTypes}
                  contactTypes={contactTypes}
                  nextPage={nextPage}
                  prevPage={prevPage}
                  editRow={editRow}
                  deleteRegister={deleteRegister}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <ContactTypeDetails
            currentState={contactTypeState}
            setContactTypeState={setContactTypeState}
            nextStep={nextStep}
            prevStep={prevStep}
            add={add}
            update={update}
          />
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(contactTypeState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Tipos de contacto
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default ContactTypes;
