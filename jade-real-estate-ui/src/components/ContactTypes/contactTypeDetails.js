import React from "react";

import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Row
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";

const ContactTypeDetails = props => {
  const { currentState, setContactTypeState, add, update } = props;

  const handleInputChange = event => {
    const { name, value } = event.target;

    setContactTypeState({
      ...currentState,
      currentContactType: {
        ...currentState.currentContactType,
        [name]: value
      }
    });
  };

  const handleCleanValues = () => {
    const initialClientState = {
      step: 1,
      editMode: "None",
      currentContactType: {
        id: "",
        name: ""
      }
    };
    setContactTypeState(initialClientState);
  };

  const handleSaveChanges = () => {
    switch (currentState.editMode) {
      case "Editing": {
        const contactType = {
          id: currentState.currentContactType.id,
          name: currentState.currentContactType.name
        };


        update(contactType);
        break;
      }

      default: {
        const newContactType = {
          name: currentState.currentContactType.name
        };
        add(newContactType);
        break;
      }
    }
  };
  switch (currentState.editMode) {
    case "Editing":
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos a editar de un tipo de contacto </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="name"
                      id="name"

                      value={currentState.currentContactType.name}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
    default:
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos de nuevo tipo de contacto </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="name"
                      id="name"

                      value={currentState.currentContactType.name}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
  }
};

export default ContactTypeDetails;
