import React, { useState } from "react";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
} from "reactstrap";

const step = 1;

const ContactTypeControlTable = (props) => {
  const {
    fetchContactTypes,
    contactTypes,
    nextPage,
    prevPage,
    editRow,
    deleteRegister,
  } = props;
  const [value, setValue] = useState("");

  function nextStep() {
    nextPage(step + 1);
  }

  function prevStep() {
    prevPage(step - 1);
  }

  function handleValueChange(e) {
    fetchContactTypes(e.target.value);
    setValue(e.target.value);
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const userRows =
    contactTypes === null ? (
      <table>
        <tbody>
          <tr>
            <td colSpan={16}>Buscando...</td>
          </tr>
        </tbody>
      </table>
    ) : contactTypes.length === 0 ? (
      <table>
        <tbody>
          <tr>
            <td colSpan={16}>No se encontraron tipos de contacto.</td>
          </tr>
        </tbody>
      </table>
    ) : (
      <div>
        <FormGroup row>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th style={thStyle}>Editar</th>
                  <th style={thStyle}>Borrar</th>
                  <th style={thStyle}>ID</th>
                  <th style={thStyle}>Nombre</th>
                </tr>
              </thead>
              <tbody>
                {contactTypes.contactTypes.map((contactType) => (
                  <tr key={contactType.id}>
                    <td>
                      <Button
                        block
                        color="warning"
                        onClick={() => {
                          editRow(contactType);
                        }}
                      >
                        Editar
                      </Button>
                    </td>
                    <td>
                      <Button
                        block
                        color="danger"
                        onClick={() => deleteRegister(contactType.id)}
                      >
                        Borrar
                      </Button>
                    </td>
                    <td>{contactType.id}</td>
                    <td>{contactType.name}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStep}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {contactTypes.pageCount}</strong>
                </h5>
              </div>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Actual: {contactTypes.pageIndex}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStep}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      </div>
    );
  return userRows;
};

export default ContactTypeControlTable;
