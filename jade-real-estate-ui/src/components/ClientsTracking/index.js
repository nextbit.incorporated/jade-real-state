import React, { useState, useEffect } from "react";
import { Card, CardBody, CardHeader, Col, Row, FormGroup } from "reactstrap";
import API from "./../API/API";
import ClientsTrackingView from "./ClientsTrackingView";
import { ToastContainer, toast } from "react-toastify";
import { Redirect } from "react-router-dom";

const heightStyle = {
  height: "500px",
};

const prospectState = {
  step: 1,
  editMode: "None",
  startDate: Date.now(),
  file: [],
  fileName: "",
  infoDataxlsx: [],
  currentProspect: {
    id: "",
    registerDate: "",
    projectId: "",
    contactType: "",
    name: "",
    idNumber: "",
    contactNumberOne: "",
    contactNumberTwo: "",
    email: "",
    projectName: "",
    contactTypeName: "",
    orden: 0,
    procesoId: 0,
    estado: "",
    project: [],
    contactTypes: [],
  },
  state: {
    modal: false,
    large: false,
    large1: false,
    small: false,
    primary: false,
    success: false,
    warning: false,
    danger: false,
    info: false,
    uploadFile: false,
    className: null,
  },
};

function compareNames(currentItem, nextItem) {
  const currentName = currentItem.name;
  const nextName = nextItem.name;

  let comparison = 0;
  if (currentName > nextName) {
    comparison = 1;
  } else if (currentName < nextName) {
    comparison = -1;
  }
  return comparison;
}

const ClientsTracking = (props) => {
  const [serviceState, setServiceState] = useState([]);
  const [apiCallInProgress, setApiCallInProgress] = useState(false);
  const [modelState, setModelState] = useState(prospectState);
  const [trackingsState, setTrackingsState] = useState([]);

  useEffect(() => {
    fetchServices();
  }, []);

  function fetchServices() {
    const url = `SupplierService`;
    API.get(url).then((res) => {
      const services = [{ id: 0, name: "(Seleccione un servicio)" }].concat(
        res.data
          .map((service) => {
            return {
              id: service.id,
              name: service.name,
            };
          })
          .sort(compareNames)
      );

      setServiceState(services);
    });
  }

  function getDataClientTracking(id) {
    const url = `clientTracking?clientId=${id}`;
    API.get(url)
      .then((res) => {
        setTrackingsState(res.data || []);
      })
      .catch((error) => {
        if (error.message === "Network Error") {
          props.history.push("/login");
          return <Redirect to="/login" />;
        }
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login" />;
        }
      });
  }

  function saveSeguimiento(request) {
    setApiCallInProgress(true);
    const url = `clientTracking/agregar-nuevo-seguimiento`;

    API.put(url, request).then((res) => {
      setApiCallInProgress(false);

      if (res.status === 200) {
        toast.success("seguimiento agregado");
        setTrackingsState(res.data);
        setModelState({ ...modelState, step: 2 });
      } else {
        toast.error("Error al tratar de agregar cita");
      }
    });

    setModelState({ ...modelState, step: 1 });
  }

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col xs="auto">
                <ClientsTrackingView
                  serviceState={serviceState}
                  apiCallInProgress={apiCallInProgress}
                  saveSeguimiento={saveSeguimiento}
                  trackingsState={trackingsState}
                  getDataClientTracking={getDataClientTracking}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(prospectState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Seguimiento de clientes
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
      <ToastContainer autoClose={5000} />
    </div>
  );
};

export default ClientsTracking;
