import React, { useState, useEffect } from "react";
import styled from "styled-components";
import API from "../API/API";
import { Redirect } from "react-router-dom";
import moment from "moment";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
  ModalFooter,
  ModalHeader,
  Modal,
  ModalBody,
} from "reactstrap";
import { toast } from "react-toastify";
import { addCalendarEvent } from "./../Helpers/GoogleCalendar";

const initialAppointmentState = {
  step: 1,
  editMode: "None",
  creationDate: moment().format("YYYY-MM-DD"),
  file: [],
  fileName: "",
  infoDataxlsx: [],
  fechaCita: moment().format("YYYY-MM-DD"),
  description: "",
  comentarios: "",
  clientId: 0,
  id: 0,
  client: [],
  state: {
    modal: false,
    large: false,
    large1: false,
    small: false,
    primary: false,
    success: false,
    warning: false,
    danger: false,
    info: false,
    uploadFile: false,
    className: null,
  },
};

const EditBarContainer = styled.div`
  display: grid;
  grid-template-columns: auto auto auto 1fr;
  flex-direction: column;
`;

const ClientTrackingViewIndividual = (props) => {
  const { client, cancelChanges, fetchClientTrackings, clientTrackings } =
    props;

  const [appointmentState, setAppointmentState] = useState(
    initialAppointmentState
  );
  const [meetingCategoryState, setMeetingCategoryState] = useState([]);

  useEffect(() => {
    fetchClientTrackings();
    fetchMeetingModel();
  }, []);

  const togglePrimary = () => {
    setAppointmentState({
      ...appointmentState,
      state: {
        large: !appointmentState.state.large,
      },
    });
  };

  function compareNames(currentItem, nextItem) {
    const currentName = currentItem.name;
    const nextName = nextItem.name;

    let comparison = 0;
    if (currentName > nextName) {
      comparison = 1;
    } else if (currentName < nextName) {
      comparison = -1;
    }
    return comparison;
  }

  function fetchMeetingModel() {
    const url = `/MeetingCategory`;
    API.get(url).then((res) => {
      const services = [{ id: 0, name: "(Seleccione un tipo de cita)" }].concat(
        res.data
          .map((service) => {
            return {
              id: service.id,
              name: service.name,
            };
          })
          .sort(compareNames)
      );

      setMeetingCategoryState(services);
    });
  }

  const guardarCambios = () => {
    switch (appointmentState.editMode) {
      case "Editing": {
        editarCita();
        break;
      }
      default: {
        agregarNuevaCita();
        break;
      }
    }
  };

  function editarCita() {
    if (!client) {
      return;
    }
    const url = `clientTracking`;
    const request = {
      id: appointmentState.id,
      tipoCita: appointmentState.meetingCategoryId,
      description: appointmentState.description,
      fechaCita: moment(appointmentState.fechaCita).format("YYYY-MM-DD"),
      clientId: client.principal.id,
      comentarios: appointmentState.comentarios,
    };

    API.put(url, request).then((res) => {
      if (res.status === 200) {
        toast.success("Cita modificada");
        togglePrimary();
        fetchClientTrackings();
      } else {
        toast.error("Error al tratar de agregar cita");
      }
    });
  }

  function agregarNuevaCita() {
    if (!client) {
      return;
    }
    const url = `clientTracking`;

    const request = {
      description: appointmentState.description,
      fechaCita: appointmentState.fechaCita,
      clientId: client.principal.id,
      comentarios: appointmentState.comentarios,
      tipoCita: appointmentState.meetingCategoryId,
      createdBy: sessionStorage.getItem("userId"),
    };

    API.post(url, request).then((res) => {
      if (res.status === 200) {
        toast.success("Cita agregada");

        const startDate = new Date(
          moment(request.fechaCita + " 10:00:00").format()
        );

        const mettingTypes = meetingCategoryState.filter(
          (c) => c.id === appointmentState.meetingCategoryId
        );

        const meetingType =
          mettingTypes && mettingTypes.length > 0
            ? mettingTypes[0].name
            : "Seguimiento";

        const mainClient = client.principal;

        const address = `${meetingType}: ${mainClient.id} - ${mainClient.firstName} ${mainClient.firstSurname}`;
        const summary = `${meetingType}: ${mainClient.id} - ${mainClient.firstName} ${mainClient.firstSurname} ${appointmentState.description}`;

        addCalendarEvent(startDate, address, summary);

        togglePrimary();
        fetchClientTrackings();
      } else {
        toast.error("Error al tratar de agregar cita");
      }
    });
  }

  const handleInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    const udpatedAppointmentState = {
      ...appointmentState,
      [name]: value,
    };

    setAppointmentState(udpatedAppointmentState);
  };

  const editCita = (cita) => {
    setAppointmentState({
      ...appointmentState,
      editMode: "Editing",
      meetingCategoryId: cita.meetingCategoryId,
      fechaCita: moment(cita.fechaCita).format("L"),
      description: cita.description,
      comentarios: cita.comentarios,
      clientId: 0,
      id: cita.id,
      state: {
        large: !appointmentState.state.large,
      },
    });
  };

  const createCita = () => {
    setAppointmentState({
      ...initialAppointmentState,
      state: {
        large: true,
      },
    });
  };

  const deleteRegister = (id) => {
    const url = `clientTracking?id=${id}`;

    API.delete(url)
      .then((res) => {
        toast.success("Cita Eliminada.");
        if (res.data !== "") {
          return;
        }
        fetchClientTrackings();
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  };

  const completeProcess = (id) => {
    const url = `clientTracking/completar-cita?clientId=${id}`;
    API.get(url)
      .then((res) => {
        toast.success("Cita Completada");
        if (res.data !== "") {
          return;
        }
        fetchClientTrackings();
      })
      .catch((error) => {
        if (error.response.status === 401) {
          props.history.push("/login");
          return <Redirect to="/login"></Redirect>;
        }
      });
  };

  const handleNumericInputChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    setAppointmentState({
      ...appointmentState,
      [name]: parseInt(value, 10),
    });
  };

  const meetingCategoryOptions = meetingCategoryState.map((p) => {
    return (
      <option id={p.id} key={p.id} value={p.id}>
        {p.name}
      </option>
    );
  });

  return (
    <div className="animated fadeIn">
      <FormGroup row>
        <Col xs="auto">
          <Button type="reset" size="sm" color="danger" onClick={cancelChanges}>
            {" "}
            Volver{" "}
          </Button>
        </Col>
        <Col xs="auto">
          <Button type="reset" size="sm" color="success" onClick={createCita}>
            {" "}
            Agregar nueva cita
          </Button>
          <Modal
            isOpen={appointmentState.state.large}
            toggle={togglePrimary}
            className={"modal-lg"}
          >
            <ModalHeader toggle={togglePrimary}>
              {appointmentState.editMode === "Editing"
                ? "Editar cita"
                : "Agregar nueva cita"}{" "}
            </ModalHeader>
            <ModalBody>
              <CardBody>
                <Row>
                  <Col md="6">
                    <FormGroup>
                      <Label htmlFor="Id">Tipo cita</Label>
                      <Input
                        type="select"
                        name="meetingCategoryId"
                        id="meetingCategoryId"
                        value={appointmentState.meetingCategoryId ?? 0}
                        onChange={handleNumericInputChange}
                      >
                        {meetingCategoryOptions}
                      </Input>
                    </FormGroup>
                  </Col>

                  <Col md="6">
                    <FormGroup>
                      <Label htmlFor="Id">Fecha cita</Label>
                      <Input
                        type="date"
                        name="fechaCita"
                        id="fechaCita"
                        placeholder="Fecha de la Cita"
                        value={moment(appointmentState.fechaCita).format(
                          "YYYY-MM-DD"
                        )}
                        onChange={handleInputChange}
                      />
                    </FormGroup>
                  </Col>
                </Row>

                <Row>
                  <Col lg="12">
                    <FormGroup>
                      <Label htmlFor="Id">Descripcion</Label>
                      <Input
                        autoFocus
                        type="text"
                        name="descripction"
                        id="descripction"
                        value={appointmentState.descripction ?? ""}
                        onChange={handleInputChange}
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>

                <Row>
                  <Col lg="12">
                    <FormGroup>
                      <Label htmlFor="Primer Nombre">
                        Comentarios de la visita
                      </Label>
                      <Input
                        style={{ height: 80 }}
                        type="textarea"
                        id="comentarios"
                        name="comentarios"
                        value={appointmentState.comentarios}
                        onChange={handleInputChange}
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
              </CardBody>
            </ModalBody>
            <ModalFooter>
              <Button color="secondary" onClick={guardarCambios}>
                Guardar
              </Button>
              <Button color="secondary" onClick={togglePrimary}>
                Cancel
              </Button>
            </ModalFooter>
          </Modal>
        </Col>
        <Col />
      </FormGroup>

      <Row>
        <Col xs="12" sm="8" md="6">
          <Card className="border-primary">
            <CardHeader>
              <strong>{`Informacion Cliente: ${client.principal.id} - ${client.principal.firstName} ${client.principal.firstSurname}`}</strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col lg="12">
                  <strong>Cliente: </strong>{" "}
                  {`${client.principal.firstName} ${client.principal.middleName} ${client.principal.firstSurname} ${client.principal.secondSurname}`}
                </Col>
              </Row>
              <Row>
                <Col lg="12">
                  <strong>Direccion: </strong>
                  {client.principal.homeAddress}
                </Col>
              </Row>
              <Row>
                <Col lg="12">
                  <strong>Co Deudor: </strong>{" "}
                  {client.coDebtor &&
                    `${client.coDebtor.firstName} ${client.coDebtor.middleName} ${client.coDebtor.firstSurname} ${client.coDebtor.secondSurname}`}
                </Col>
              </Row>
              <br />
              <Row>
                <Col md="6">
                  <strong>Proyecto: </strong>
                  {client.principal.project}
                </Col>

                <Col md="6">
                  <strong>Agente: </strong> {client.principal.salesAdvisor}
                </Col>
                <Col md="6">
                  <strong>Oficial: </strong> {client.principal.creditOfficer}
                </Col>
                <Col md="6">
                  <strong>Inst. Fin: </strong>{" "}
                  {client.principal.financialInstitution}
                </Col>
              </Row>
              <br />
              <Row>
                <br />
                <Col lg="12">
                  <p>
                    <strong>Comentario: </strong>
                    {client.principal.comments}
                  </p>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </Col>
      </Row>

      <Row>
        {clientTrackings.map((cl) => (
          <Col key={cl.id} md="6">
            <Card>
              <CardHeader>
                <Row>
                  <Col xs="auto">
                    <strong>Fecha Programada de la Cita:</strong>{" "}
                    {moment(cl.fechaCita).format("L")}
                  </Col>
                  <Col />

                  <Col xs="auto">
                    <strong>Estado: </strong>
                    {cl.estado}
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col lg="12">
                    <strong>Tipo Cita: </strong>
                    {cl.meetingCategoryName}
                  </Col>
                </Row>
                <Row>
                  <Col lg="12">
                    <strong>Servicio: </strong>
                    {cl.servicio}
                  </Col>
                </Row>

                <Row>
                  <Col md="6">
                    <strong>Fecha Creación: </strong>
                    {moment(cl.creationDate).format("L")}
                  </Col>
                  <Col md="6">
                    <strong>Creado Por: </strong>
                    {cl.createdBy}
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <strong>Descripcion: </strong>
                    {cl.description}
                  </Col>
                </Row>

                <Row>
                  <Col>
                    <strong>Comentarios: </strong>
                    {cl.comentarios}
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <Row>
                  <EditBarContainer>
                    <div style={{ margin: "2px" }}>
                      <Button
                        color="warning"
                        onClick={() => {
                          editCita(cl);
                        }}
                      >
                        Editar
                      </Button>
                    </div>

                    <div style={{ margin: "2px" }}>
                      <Button
                        color="danger"
                        onClick={() => deleteRegister(cl.id)}
                      >
                        Eliminar
                      </Button>
                    </div>
                    <div style={{ margin: "2px" }}>
                      <Button
                        color="success"
                        onClick={() => completeProcess(cl.id)}
                      >
                        Completar
                      </Button>
                    </div>
                  </EditBarContainer>
                </Row>
              </CardFooter>
            </Card>
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default ClientTrackingViewIndividual;
