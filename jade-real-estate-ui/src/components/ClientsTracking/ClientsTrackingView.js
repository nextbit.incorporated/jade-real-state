import React, { useState } from "react";
import moment from "moment";
import { Button, Col, FormGroup, Input, Label, Table, Row } from "reactstrap";

import ClientSearch from "./../Clients/ClientSearch/ClientSearch";

const thStyle = {
  background: "#20a8d8",
  color: "white",
};

const ClientsTrackingView = (props) => {
  const {
    serviceState,
    apiCallInProgress,
    saveSeguimiento,
    trackingsState,
    getDataClientTracking,
  } = props;

  const [supplierServiceId, setSupplierServiceId] = useState(0);
  const [selectedClient, setSelectedClient] = useState(null);
  const [clientSelectionIsOpen, setClientSelectionIsOpen] = useState(false);

  const handleSaveChanges = () => {
    const request = {
      clienteId: selectedClient.id,
      supplierServiceId: supplierServiceId,
      createdBy: sessionStorage.getItem("userId"),
    };

    saveSeguimiento(request);
    setSupplierServiceId(0);
  };

  const openSearchClientControl = () => {
    setClientSelectionIsOpen(true);
  };

  const handleNumericInputChange = (event) => {
    const target = event.target;
    const value = target.value;

    setSupplierServiceId(parseInt(value, 10));
  };

  const serviceOptions = serviceState.map((p) => {
    return (
      <option id={p.id} key={p.id} value={p.id}>
        {p.name}
      </option>
    );
  });

  const toggleClientSelectionIsOpen = () => {
    setClientSelectionIsOpen(!clientSelectionIsOpen);
  };

  const handleOnSelectedClient = (client) => {
    setSelectedClient(client);
    getDataClientTracking(client.id);
  };

  return (
    <div>
      <Row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label htmlFor="lastName">Cliente</Label>
            <Input
              type="text"
              id="id"
              name="id"
              value={
                selectedClient
                  ? `${selectedClient.firstName} ${selectedClient.middleName}`
                  : ""
              }
              disabled={true}
              required
            />
          </FormGroup>
        </Col>
        <Col md="3" sm="6" xs="12">
          <Row>
            <Label htmlFor="Segundo Nombre"> Buscar </Label>
          </Row>
          <Row>
            <Button
              color="primary"
              onClick={openSearchClientControl}
              style={{ width: "350px" }}
            >
              Seleccionar Cliente
            </Button>
          </Row>
        </Col>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Servicio</Label>
            <Input
              type="select"
              name="supplierServiceId"
              value={supplierServiceId}
              onChange={handleNumericInputChange}
            >
              {serviceOptions}
            </Input>
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col md="3" sm="6" xs="12">
          <Button
            type="submit"
            size="md"
            color="success"
            disabled={apiCallInProgress}
            onClick={() => {
              handleSaveChanges();
            }}
          >
            <i className="fa fa-dot-circle-o" /> Crear seguimiento
          </Button>
        </Col>
      </Row>
      <Row>
        <Col md="12" sm="12" xs="12">
          <Table hover bordered striped responsive size="sm">
            <thead>
              <tr>
                <th style={thStyle}>ID</th>
                <th style={thStyle}>Descripcion</th>
                <th style={thStyle}>Comentarios</th>
                <th style={thStyle}>Fecha</th>
                <th style={thStyle}>Orden</th>
                <th style={thStyle}>Estado</th>
                <th style={thStyle}>Servicio</th>
              </tr>
            </thead>
            <tbody>
              {trackingsState.map((item) => (
                <tr key={item.id}>
                  <td>{item.id}</td>
                  <td>{item.description}</td>
                  <td>{item.comentarios}</td>
                  <td>{moment(item.fechaCita).format("L")}</td>
                  <td>{item.orden}</td>
                  <td>{item.estado}</td>
                  <td>{item.servicio}</td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
      </Row>

      {clientSelectionIsOpen && (
        <ClientSearch
          title="Seleccion de Cliente"
          isOpen={clientSelectionIsOpen}
          toggle={toggleClientSelectionIsOpen}
          handleOnSelectedClient={handleOnSelectedClient}
        />
      )}
    </div>
  );
};

export default ClientsTrackingView;
