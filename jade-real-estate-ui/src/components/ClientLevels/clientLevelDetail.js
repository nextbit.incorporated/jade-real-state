import React from "react";

import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
  
} from "reactstrap";
import "react-toastify/dist/ReactToastify.css";

const ClientLevelDetail = props => {
  const { ClientLevelState, setClientLevelState, update, add } = props;
  const handleInputChange = event => {
    const { name, value } = event.target;
    setClientLevelState({
      ...ClientLevelState,
      currentClient: {
        ...ClientLevelState.currentClient,
        [name]: value
      }
    });
  };

  const handleCleanValues = () => {
    const initialClientState = {
      step: 1,
      editMode: "None",
      saveStatus: false,
      currentClient: {
        id: 0,
        level: "",
        description: "",
        comment: ""
      }
    };
    setClientLevelState(initialClientState);
  };

  const handleSaveChanges = () => {
    switch (ClientLevelState.editMode) {
      case "Editing": {
        const service = {
          id: ClientLevelState.currentClient.id,
          level:
            ClientLevelState.currentClient.level === undefined ||
            ClientLevelState.currentClient.level === null
              ? ClientLevelState.currentClient.level
              : ClientLevelState.currentClient.level.trim(),
          description:
            ClientLevelState.currentClient.description === undefined ||
            ClientLevelState.currentClient.description === null
              ? ClientLevelState.currentClient.description
              : ClientLevelState.currentClient.description.trim(),
          comment:
            ClientLevelState.currentClient.comment === undefined ||
            ClientLevelState.currentClient.comment === null
              ? ClientLevelState.currentClient.comment
              : ClientLevelState.currentClient.comment.trim(),
           perfilCategoria:
             ClientLevelState.currentClient.perfilCategoria === undefined ||
             ClientLevelState.currentClient.perfilCategoria === null
             ? ClientLevelState.currentClient.perfilCategoria 
             : ClientLevelState.currentClient.perfilCategoria.trim()
        };
        update(service);
        break;
      }

      default: {
        const newService = {
          level:
            ClientLevelState.currentClient.level === undefined ||
            ClientLevelState.currentClient.level === null
              ? ClientLevelState.currentClient.level
              : ClientLevelState.currentClient.level.trim(),
          description:
            ClientLevelState.currentClient.description === undefined ||
            ClientLevelState.currentClient.description === null
              ? ClientLevelState.currentClient.description
              : ClientLevelState.currentClient.description.trim(),
          comment:
            ClientLevelState.currentClient.comment === undefined ||
            ClientLevelState.currentClient.comment === null
              ? ClientLevelState.currentClient.comment
              : ClientLevelState.currentClient.comment.trim(),
              perfilCategoria:
             ClientLevelState.currentClient.perfilCategoria === undefined ||
             ClientLevelState.currentClient.perfilCategoria === null
             ? ClientLevelState.currentClient.perfilCategoria 
             : ClientLevelState.currentClient.perfilCategoria.trim()
        };
        add(newService);
        break;
      }
    }
  };
  switch (ClientLevelState.editMode) {
    case "Editing":
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos a editar categoria </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="level"
                      id="level"
                      readonly
                      value={ClientLevelState.currentClient.level}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                    <Input
                      type="text"
                      id="description"
                      name="description"
                      value={ClientLevelState.currentClient.description}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Comentario</Label>
                    <Input
                      type="text"
                      id="comment"
                      name="comment"
                      value={ClientLevelState.currentClient.comment}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col>
                <FormGroup>
                    <Label htmlFor="Primer Nombre">Perfil de categoria</Label>
                    <Input
                      type="textarea"
                      id="perfilCategoria"
                      name="perfilCategoria"
                      value={ClientLevelState.currentClient.perfilCategoria}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
    default:
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos de nueva categoria </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="level"
                      id="level"
                      readonly
                      value={ClientLevelState.currentClient.level}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Descripcion</Label>
                    <Input
                      type="text"
                      id="description"
                      name="description"
                      value={ClientLevelState.currentClient.description}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">Comentario</Label>
                    <Input
                      type="text"
                      id="comment"
                      name="comment"
                      value={ClientLevelState.currentClient.comment}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
              <Col>
                <FormGroup>
                    <Label htmlFor="Primer Nombre">Perfil de Categoria</Label>
                    <Input
                      type="textarea"
                      id="perfilCategoria"
                      name="perfilCategoria"
                      value={ClientLevelState.currentClient.perfilCategoria}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
  }
};

export default ClientLevelDetail;
