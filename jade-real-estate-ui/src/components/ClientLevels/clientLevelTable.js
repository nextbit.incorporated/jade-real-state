import React, { useState } from "react";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input
} from "reactstrap";

const initialSearchState = {
  value: "",
  typingTimeout: 0
};

const ClientLevelTable = props => {
  const {
    ClientLevels,
    fetchClientLevels,
    editRow,
    nextStep,
    prevStep
  } = props;
  const [searchState, setSearchState] = useState(initialSearchState);

  function handleValueChange(e) {
    if (searchState.typingTimeout) {
      clearTimeout(searchState.typingTimeout);
    }

    const query = e.target.value;

    setSearchState({
      value: query,
      typingTimeout: setTimeout(function() {
        fetchClientLevels(query, 0);
        // setValue(query);
      }, 500)
    });
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white"
  };

  const userRows =
    ClientLevels === null ? (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>Buscando...</td>
          </tr>
        </thead>
      </Table>
    ) : ClientLevels.length === 0 ? (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>No se encontraron categorias.</td>
          </tr>
        </thead>
      </Table>
    ) : (
      <div>
        <FormGroup row>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={searchState.value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th style={thStyle}>Editar</th>
                  {/* <th style={thStyle}>Borrar</th> */}
                  <th style={thStyle}>ID</th>
                  <th style={thStyle}>Categoria</th>
                  <th style={thStyle}>Descripción</th>
                  <th style={thStyle}>Comentario</th>
                  <th style={thStyle}>Perfil Categoria</th>
                </tr>
              </thead>
              <tbody>
                {ClientLevels.clientLevels.map(project => (
                  <tr key={project.id}>
                    <td>
                      <Button
                        block
                        color="warning"
                        onClick={() => {
                          editRow(project);
                        }}
                      >
                        Editar
                      </Button>
                    </td>
                    <td>{project.id}</td>
                    <td>{project.level}</td>
                    <td>{project.description}</td>
                    <td>{project.comment}</td>
                    <td>{project.perfilCategoria}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStep}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px"
                }}
              >
                <h5>
                  <strong>Paginas: {ClientLevels.pageCount}</strong>
                </h5>
              </div>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px"
                }}
              >
                <h5>
                  <strong>Actual: {ClientLevels.pageIndex}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStep}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      </div>
    );
  return userRows;
};

export default ClientLevelTable;
