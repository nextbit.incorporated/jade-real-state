import React, { useState, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";
import {
  CardBody,
  Col,
  FormGroup,
  Button,
  Row,
  Card,
  CardHeader,
} from "reactstrap";
import ClientLevelDetail from "./clientLevelDetail";
import ClientLevelTable from "./clientLevelTable";

const heightStyle = {
  height: "500px",
};

const initialClientLevelState = {
  step: 1,
  editMode: "None",
  saveStatus: false,
  currentClient: {
    id: 0,
    level: "",
    description: "",
    comment: "",
    perfilCategoria: "",
  },
};

const ClientLevel = (props) => {
  const [ClientLevelState, setClientLevelState] = useState(
    initialClientLevelState
  );
  const [apiCallInProgress, setApiCallInProgress] = useState(false);
  const [ClientLevels, setClientLevels] = useState([]);

  function fetchClientLevels(query, step) {
    const index = step === undefined || step === null ? 0 : step;
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard"></Redirect>;
    }
    if (query === null) {
      var url = `ClientLevel/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setClientLevels(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `ClientLevel/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1)
        .then((res) => {
          if (res.data.length > 0) {
            setClientLevelState(res.data);
          } else {
            fetchClientLevels(null, 0);
          }
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    }
  }

  const editRow = (client) => {
    setClientLevelState({
      ...ClientLevelState,
      editMode: "Editing",
      currentClient: client,
      step: 2,
    });
  };

  const deleteLevel = (id) => {
    const url = `ClientLevel?id=${id}`;

    API.delete(url).then((res) => {
      setClientLevelState(initialClientLevelState);
      fetchClientLevels(null, 0);
      toast.success("Cliente eliminado correctamente");
    });
  };

  useEffect(() => {
    fetchClientLevels(null, 0);
  }, []);

  function nextStep(client) {
    const { step } = ClientLevelState;

    setClientLevelState({ ...ClientLevelState, step: step + 1 });
  }

  function prevStep() {
    const { step } = ClientLevelState;

    setClientLevelState({ ...ClientLevelState, step: step - 1 });
  }

  const cancelChanges = () => {
    setClientLevelState(initialClientLevelState);
    fetchClientLevels(null, 0);
  };

  function nextPage(step, value) {
    fetchClientLevels(value, step);
  }

  function prevPage(step, value) {
    fetchClientLevels(value, step);
  }

  const add = (newClient) => {
    const url = `ClientLevel`;

    setApiCallInProgress(true);
    API.post(url, newClient)
      .then((res) => {
        setApiCallInProgress(false);
        setClientLevelState(initialClientLevelState);
        fetchClientLevels(null, 0);
        toast.success("reserva agregado satisfactorimente");
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para crear el nuevo cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  };

  const update = (updatedClient) => {
    const url = `ClientLevel`;

    setApiCallInProgress(true);

    API.put(url, updatedClient)
      .then((res) => {
        setApiCallInProgress(false);
        setClientLevelState(initialClientLevelState);
        fetchClientLevels(null, 0);
        toast.success("datos del cliente modificados satisfactorimente");
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  };

  const saveChanges = (level) => {
    switch (ClientLevelState.editMode) {
      case "Adding": {
        add(level);

        break;
      }
      case "Editing":
        update(level);
        break;

      default:
        break;
    }
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col />
              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStep()}
                >
                  <i className="fa fa-new" /> Agregar Categoria
                </Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <ClientLevelTable
                  ClientLevels={ClientLevels}
                  fetchClientLevels={fetchClientLevels}
                  editRow={editRow}
                  deleteLevel={deleteLevel}
                  nextStep={nextStep}
                  prevStep={prevStep}
                  nextPage={nextPage}
                  prevPage={prevPage}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <ClientLevelDetail
            ClientLevelState={ClientLevelState}
            setClientLevelState={setClientLevelState}
            saveChanges={saveChanges}
            cancelChanges={cancelChanges}
            apiCallInProgress={apiCallInProgress}
            update={update}
            add={add}
          />
        );
      default:
        return null;
    }
  }

  const clientsStep = GetCurrentStepComponent(ClientLevelState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Categoria cliente
              </CardHeader>
              {clientsStep}
            </Card>
          </Col>
        </Row>
      </Card>
      <ToastContainer autoClose={5000} />
    </div>
  );
};

export default ClientLevel;
