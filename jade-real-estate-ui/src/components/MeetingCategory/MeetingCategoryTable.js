import React, { Suspense, useState, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
  Table,
  InputGroup,
  InputGroupAddon,
  Input,
} from "reactstrap";
import ReactLoading from "react-loading";
import "react-toastify/dist/ReactToastify.css";
import MeetingCategoryTableRows from "./MeetingCategoryTableRows";

const MeetingCategoryTable = (props) => {
  const {
    fetchMeetingModels,
    editRow,
    meetingcategoriesState,
    nextPage,
    prevPage,
    updateModel,
    dsiabledMeetingCategory,
    loadingStatus,
  } = props;
  const [searchValueState, setsearchValueState] = useState("");

  function nextStepPaged() {
    nextPage(meetingcategoriesState.pageIndex + 1, searchValueState);
  }

  function prevStepPaged() {
    prevPage(meetingcategoriesState.pageIndex - 1, searchValueState);
  }

  function handleValueChange(e) {
    const query = e.target.value;
    setsearchValueState(query);
  }

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const fallback = (
    <tr>
      <td colSpan={14}>Cargando....</td>
    </tr>
  );

  function handleValueHeyPress(e) {
    if (e.key === "Enter") {
      const query = e.target.value;
      fetchMeetingModels(query, 0);
    }
  }

  function obtenerTabla(e) {
    if (loadingStatus) {
      return (
        <FormGroup row>
          <Col md="4" sm="6" xs="6"></Col>
          <Col md="4" sm="6" xs="6">
            <ReactLoading
              type="bars"
              color="#5DBCD2"
              height={"30%"}
              width={"30%"}
            />
          </Col>
          <Col md="4" sm="6" xs="6"></Col>
        </FormGroup>
      );
    } else {
      return (
        <FormGroup row>
          <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th style={thStyle}>Editar</th>
                  <th style={thStyle}>Borrar</th>
                  <th style={thStyle}>Codigo</th>
                  <th style={thStyle}>Nombre</th>
                  <th style={thStyle}>Descripcion</th>
                </tr>
              </thead>
              <tbody>
                <Suspense fallback={fallback}>
                  <MeetingCategoryTableRows
                    items={
                      meetingcategoriesState === []
                        ? []
                        : meetingcategoriesState.items
                    }
                    editRow={editRow}
                    dsiabledMeetingCategory={dsiabledMeetingCategory}
                  />
                </Suspense>
              </tbody>
            </Table>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStepPaged}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Paginas: {meetingcategoriesState.pageCount}</strong>
                </h5>
              </div>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>Actual: {meetingcategoriesState.pageIndex}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStepPaged}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      );
    }
  }

  return (
    <div style={{ margin: "10px" }}>
      <FormGroup row>
        <Col md="3" sm="6" xs="12">
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button type="button" color="primary">
                <i className="fa fa-search" /> Nombre
              </Button>
            </InputGroupAddon>
            <Input
              type="text"
              id="input1-group2"
              name="input1-group2"
              placeholder="Buscar"
              value={searchValueState}
              onChange={handleValueChange}
              onKeyPress={(e) => handleValueHeyPress(e)}
            />
          </InputGroup>
        </Col>
      </FormGroup>
      {obtenerTabla()}
      <ToastContainer autoClose={5000} />
    </div>
  );
};

export default MeetingCategoryTable;
