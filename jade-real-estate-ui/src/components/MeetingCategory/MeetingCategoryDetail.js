import React from "react";
import "react-toastify/dist/ReactToastify.css";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
  Label,
  Input,
  CardFooter,
} from "reactstrap";

const MeetingCategoryDetail = (props) => {
  const {
    meetingModelState,
    setMeetingModelState,
    addCategory,
    updateModel,
  } = props;

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setMeetingModelState({
      ...meetingModelState,
      meetingModel: {
        ...meetingModelState.meetingModel,
        [name]: value,
      },
    });
  };

  const handleCleanValues = () => {
    const initialClientState = {
      step: 1,
      editMode: "None",
      meetingModel: {
        id: "",
        name: "",
        description: "",
      },
    };
    setMeetingModelState(initialClientState);
  };

  const handleSaveChanges = () => {
    switch (meetingModelState.editMode) {
      case "Editing": {
        const category = {
          id: meetingModelState.meetingModel.id,
          name: meetingModelState.meetingModel.name,
          description: meetingModelState.meetingModel.description,
        };
        updateModel(category);
        break;
      }

      default: {
        const category = {
          name: meetingModelState.meetingModel.name,
          description: meetingModelState.meetingModel.description,
        };
        addCategory(category);
        break;
      }
    }
  };

  switch (meetingModelState.editMode) {
    case "Editing":
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong>Edición de tipo de cita </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="6">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre del tipo de cita</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="name"
                      id="name"
                      value={meetingModelState.meetingModel.name}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col md="6">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">
                      Descripcion del tipo de cita
                    </Label>
                    <Input
                      type="text"
                      id="description"
                      name="description"
                      value={meetingModelState.meetingModel.description}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
    default:
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Nuevo tipo de Cita </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="6">
                  <FormGroup>
                    <Label htmlFor="Id">Nombre del nuevo tipo de cita</Label>
                    <Input
                      autoFocus
                      type="text"
                      name="name"
                      id="name"
                      value={meetingModelState.meetingModel.name}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>

              <Row>
                <Col md="6">
                  <FormGroup>
                    <Label htmlFor="Primer Nombre">
                      Descripcion del nuevo tipo de cita
                    </Label>
                    <Input
                      type="text"
                      id="description"
                      name="description"
                      value={meetingModelState.meetingModel.description}
                      onChange={handleInputChange}
                      required
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={handleSaveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={handleCleanValues}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
  }
};

export default MeetingCategoryDetail;
