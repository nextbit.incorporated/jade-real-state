import React from "react";
import moment from "moment";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
  Table,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";

const MeetingCategoryTableRows = (props) => {
  const { items, editRow, dsiabledMeetingCategory } = props;

  const rows =
    items === null || items === undefined ? (
      <tr>
        <td colSpan={19}>Buscando datos...</td>
      </tr>
    ) : items.length === 0 ? (
      <tr>
        <td colSpan={19}>
          No se encontraron datos con el filtro especificado.
        </td>
      </tr>
    ) : (
      items.map((item) => (
        <tr key={item.id}>
          <td>
            <Button
              block
              color="warning"
              onClick={() => {
                editRow(item);
              }}
            >
              Editar
            </Button>
          </td>
          <td>
            <Button
              block
              color="danger"
              onClick={() => {
                dsiabledMeetingCategory(item);
              }}
            >
              Borrar
            </Button>
          </td>
          <td nowrap="true">{item.id}</td>
          <td nowrap="true">{item.name}</td>
          <td nowrap="true">{item.description}</td>
        </tr>
      ))
    );

  return rows;
};

export default MeetingCategoryTableRows;
