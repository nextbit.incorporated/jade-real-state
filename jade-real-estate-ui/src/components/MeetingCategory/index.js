import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "../API/API";
import MeetingCategoryDetail from "./MeetingCategoryDetail";
import MeetingCategoryTable from "./MeetingCategoryTable";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  FormGroup,
  Button,
} from "reactstrap";

const initialClientState = {
  step: 1,
  editMode: "None",
  meetingModel: {
    id: 0,
    name: "",
    description: "",
  },
};
const heightStyle = {
  height: "500px",
};
const MeetingCategories = (props) => {
  const [meetingModelState, setMeetingModelState] = useState(
    initialClientState
  );
  const [meetingcategoriesState, setMeetingcategoriesState] = useState([]);
  const [apiCallInProgress, setApiCallInProgress] = useState(false);

  useEffect(() => {
    fetchMeetingModels(null);
  }, []);

  const editRow = (model) => {
    setMeetingModelState({
      ...meetingModelState,
      editMode: "Editing",
      meetingModel: model,
      step: 2,
    });
  };

  function nextPage(step) {
    fetchMeetingModels(null, step);
  }

  function prevPage(step) {
    fetchMeetingModels(null, step);
  }

  function nextStep(model) {
    const { step } = meetingModelState;

    if (model === null || model === undefined) {
      model = meetingModelState.currentModel;
    }
    setMeetingModelState({
      ...meetingModelState,
      currentModel: model,
      step: step + 1,
    });
  }

  function prevStep() {
    const { step } = meetingModelState;

    setMeetingModelState({ ...meetingModelState, step: step - 1 });
  }

  function fetchMeetingModels(query, step) {
    const index = step === undefined || step === null ? 0 : step;
    setApiCallInProgress(true);
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard"></Redirect>;
    }
    if (query === null || query === undefined) {
      setApiCallInProgress(true);
      var url = `MeetingCategory/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setApiCallInProgress(false);
          setMeetingcategoriesState(res.data);
        })
        .catch((error) => {
          setApiCallInProgress(false);
          if (error.message === "Network Error") {
            toast.warn(
              "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
            );
          } else {
            if (error.response.status !== undefined) {
              if (error.response.status === 401) {
                props.history.push("/login");
                return <Redirect to="/login" />;
              }
            } else {
              toast.warn(
                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
              );
            }
          }
        });
    } else {
      var url1 = `MeetingCategory/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1)
        .then((res) => {
          setApiCallInProgress(false);
          setMeetingcategoriesState(res.data);
        })
        .catch((error) => {
          setApiCallInProgress(false);
          if (error.message === "Network Error") {
            toast.warn(
              "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
            );
          } else {
            if (error.response.status !== undefined) {
              if (error.response.status === 401) {
                props.history.push("/login");
                return <Redirect to="/login" />;
              }
            } else {
              toast.warn(
                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
              );
            }
          }
        });
    }
  }

  function addCategory(newCategory) {
    if (!newCategory || !newCategory.name) {
      return;
    }
    setApiCallInProgress(true);
    const url = `MeetingCategory`;

    API.post(url, newCategory)
      .then((res) => {
        setApiCallInProgress(false);
        if (res.id === 0) {
          toast("Error al intentar agregar nueva categoria");
        } else {
          toast("categoria agregada satisfactoriamente");
          fetchMeetingModels(null);
          setMeetingModelState(initialClientState);
        }
      })
      .catch((error) => {
        setApiCallInProgress(false);
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }

  function updateModel(model) {
    if (!model || !model.name) {
      return;
    }
    const url = `MeetingCategory`;
    API.put(url, model)
      .then((res) => {
        setApiCallInProgress(false);
        if (res.id === 0) {
          toast("Error al intentar agregar usuario");
        } else {
          toast("Tipo de cita modificada satisfactoriamente");

          // setUsers([...users.users, userAdded]);
          fetchMeetingModels(null);
          setMeetingModelState(initialClientState);
        }
      })
      .catch((error) => {
        setApiCallInProgress(false);
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }

  function dsiabledMeetingCategory(model) {
    if (!model || !model.name) {
      return;
    }
    const url = `MeetingCategory/disabled-register`;
    API.put(url, model)
      .then((res) => {
        if (res.id === 0) {
          toast("Error al intentar agregar usuario");
        } else {
          toast("proyecto agregado satisfactoriamente");

          // setUsers([...users.users, userAdded]);
          fetchMeetingModels(null);
          setMeetingModelState(initialClientState);
        }
      })
      .catch((error) => {
        setApiCallInProgress(false);
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <div>
            <FormGroup row>
              <Col />
              <Col xs="auto" style={{ margin: "10px" }}>
                <Button
                  type="button"
                  color="success"
                  onClick={() => nextStep()}
                >
                  <i className="fa fa-new" /> Agregar nuevo tipo de cita
                </Button>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <MeetingCategoryTable
                  fetchMeetingModels={fetchMeetingModels}
                  editRow={editRow}
                  meetingcategoriesState={meetingcategoriesState}
                  nextPage={nextPage}
                  prevPage={prevPage}
                  updateModel={updateModel}
                  dsiabledMeetingCategory={dsiabledMeetingCategory}
                  loadingStatus={apiCallInProgress}
                />
              </Col>
            </FormGroup>
          </div>
        );
      case 2:
        return (
          <MeetingCategoryDetail
            meetingModelState={meetingModelState}
            setMeetingModelState={setMeetingModelState}
            addCategory={addCategory}
            updateModel={updateModel}
          />
        );
      default:
        return null;
    }
  }

  const userStep = GetCurrentStepComponent(meetingModelState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Tipos de citas
              </CardHeader>
              {userStep}
            </Card>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default MeetingCategories;
