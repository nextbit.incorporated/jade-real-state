import React, { useState } from "react";
import checkmark from "../../../src/assets/img/brand/checkmark.svg";
import {
  Button,
  Col,
  FormGroup,
  Input,
  Label,
  Table,
  Row,
  ModalFooter,
  ModalHeader,
  Modal,
  ModalBody,
  InputGroup,
  InputGroupAddon,
  CardFooter,
  Card,
  CardHeader,
  CardBody,
} from "reactstrap";
import API from "./../API/API";
import { Redirect } from "react-router-dom";

const thStyle = {
  background: "#20a8d8",
  color: "white",
};
const ReservaDetail = (props) => {
  const { reservaState, setreservaState, saveChanges, cancelChanges } = props;
  const [clients, setClients] = useState([]);
  const [valueClient, setValueClient] = useState("");
  const [clientSelected, setClientSelected] = useState([]);
  const [clientSelectedName, setClientSelectedName] = useState("");


  const handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    setreservaState({
      ...reservaState,
      currentClient: {
        ...reservaState.currentClient,
        [name]: value,
      },
    });
  };

  const ConstruirNombreCliente = (
    firstName,
    middleName,
    firstSurname,
    secondSurname
  ) => {
    const name =
      (firstName || "") +
      " " +
      (middleName || "") +
      " " +
      (firstSurname || "") +
      " " +
      (secondSurname || "");
    return name;
  };

  const projectOptions = reservaState.projects.map((p) => {
    return (
      <option id={p.id} key={p.id} value={p.id}>
        {p.name}
      </option>
    );
  });

  const salesAdvisorsOptions = reservaState.salesAdvisors.map((p) => {
    return (
      <option id={p.id} key={p.id} value={p.id}>
        {p.name}
      </option>
    );
  });

  function fetchClientsPaged(query, step) {
    const user = sessionStorage.getItem("userId");
    const index = step === undefined || step === null ? 0 : step;
    if (query === null || query === "" || query === undefined) {
      var url = `clients/paged?pageSize=10&pageIndex=${index}&user=${user}`;

      API.get(url)
        .then((res) => {
          setClients(res.data);
          var l = clients;
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            return <Redirect to="/login" />;
          }
          if (error.response.status === 401) {
            return <Redirect to="/login" />;
          }
        });
    } else {
      var url1 = `clients/paged?pageSize=10&pageIndex=${index}&user=${user}&value=${query}&idFilter=${1}`;
      API.get(url1).then((res) => {
        setClients(res.data);
      });
    }
  }

  const togglePrimary = () => {
    setreservaState({
      ...reservaState,
      state: {
        large: !reservaState.state.large,
      },
    });
    fetchClientsPaged(null, 0);
  };

  function handleValueChange(e) {
    fetchClientsPaged(e.target.value);
    setValueClient(e.target.value);
  }

  const ObtenerNombreCliente = () => {
    const name =
      (clientSelected.principal.firstName || "") +
      " " +
      (clientSelected.principal.middleName || "") +
      " " +
      (clientSelected.principal.firstSurname || "") +
      " " +
      (clientSelected.principal.secondSurname || "");
    return name;
  };

  const setRowClientSelected = (client) => {
    debugger;
    setClientSelected(client);
    setClientSelectedName(
      ConstruirNombreCliente(
        client.principal.firstName,
        client.principal.middleName,
        client.principal.firstSurname,
        client.principal.secondSurname
      )
    );
    setreservaState({
      ...reservaState,
      state: {
        large: !reservaState.state.large,
      },
      currentClient: {
        clientId: client.principal.id,
      },
    });
  };

  const nextPageProjects = () => {
    fetchClientsPaged(valueClient, clients.pageIndex + 1);
  };

  function prevPageProjects() {
    fetchClientsPaged(valueClient, clients.pageIndex - 1);
  }

  switch (reservaState.editMode) {
    case "Editing":
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos a editar de reserva </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName">Cliente</Label>
                    <Input
                      type="text"
                      id="id"
                      name="id"
                      value={clientSelectedName}
                      // onChange={handleInputChange}
                      disabled={true}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <Row>
                    <Label htmlFor="Segundo Nombre"> Buscar </Label>
                  </Row>
                  <Row>
                    <Button
                      color="primary"
                      onClick={togglePrimary}
                      style={{ width: "350px" }}
                    >
                      Seleccionar Cliente
                    </Button>
                    <Modal
                      isOpen={reservaState.state.large}
                      toggle={togglePrimary}
                      className={"modal-lg"}
                    >
                      <ModalHeader toggle={togglePrimary}>
                        Seleccionar Cliente
                      </ModalHeader>
                      <ModalBody>
                        <FormGroup row>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button type="button" color="primary">
                                  <i className="fa fa-search" /> Buscar
                                </Button>
                              </InputGroupAddon>
                              <Input
                                type="text"
                                id="input1-group2"
                                name="input1-group2"
                                placeholder="Buscar"
                                value={valueClient}
                                onChange={handleValueChange}
                              />
                            </InputGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="12">
                            <InputGroup>
                              <Label htmlFor="Id">Nombre</Label>
                              <Input
                                type="text"
                                id="input1-group2"
                                name="input1-group2"
                                value={ObtenerNombreCliente}
                                onChange={handleValueChange}
                              />
                            </InputGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="12">
                            {clients === null ? (
                              <tr>
                                <td colSpan={16}>Buscando...</td>
                              </tr>
                            ) : clients.length === 0 ? (
                              <tr>
                                <td colSpan={16}>
                                  No se encontraron proyectos.
                                </td>
                              </tr>
                            ) : (
                              <Table
                                hover
                                bordered
                                striped
                                responsive
                                size="sm"
                              >
                                <thead>
                                  <tr>
                                    <th style={thStyle}>Seleccionar</th>
                                    <th style={thStyle}>Identitdad</th>
                                    <th style={thStyle}>Codigo</th>
                                    <th style={thStyle}>Primer Nombre</th>
                                    <th style={thStyle}>Segundo Nombre</th>
                                    <th style={thStyle}>Primer Apellido</th>
                                    <th style={thStyle}>Segundo Apellido</th>
                                    <th style={thStyle}>Agente</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {clients.clients.map((client) => (
                                    <tr key={client.principal.id}>
                                      <td>
                                        <Button
                                          onClick={() => {
                                            setRowClientSelected(client);
                                          }}
                                        >
                                          <img
                                            src={checkmark}
                                            height="40px"
                                            width="40px"
                                            alt="checkmark"
                                          />
                                        </Button>
                                      </td>
                                      <td>{client.principal.identificationCard}</td>
                                      <td>{client.principal.id}</td>
                                      <td>{client.principal.firstName}</td>
                                      <td>{client.principal.middleName}</td>
                                      <td>{client.principal.firstSurName}</td>
                                      <td>{client.principal.secondSurName}</td>
                                      <td>{client.principal.salesAdvisor}</td>
                                    </tr>
                                  ))}
                                </tbody>
                              </Table>
                            )}
                          </Col>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={prevPageProjects}
                                >
                                  <i className="icon-arrow-left-circle" />{" "}
                                  Anterior
                                </Button>
                              </InputGroupAddon>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Paginas: {clients.pageCount}</strong>
                                </h5>
                              </div>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Actual: {clients.pageIndex}</strong>
                                </h5>
                              </div>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={nextPageProjects}
                                >
                                  Proximo{" "}
                                  <i className="icon-arrow-right-circle" />
                                </Button>
                              </InputGroupAddon>
                            </InputGroup>
                          </Col>
                        </FormGroup>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={togglePrimary}>
                          Cancel
                        </Button>
                      </ModalFooter>
                    </Modal>
                  </Row>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label>Poyecto:</Label>
                    <Input
                      type="select"
                      name="projectId"
                      value={reservaState.projectId}
                      onChange={handleInputChange}
                    >
                      {projectOptions}
                    </Input>
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label>Agente de ventas:</Label>
                    <Input
                      type="select"
                      name="pojectId"
                      value={reservaState.salesAdvisorId}
                      onChange={handleInputChange}
                    >
                      {salesAdvisorsOptions}
                    </Input>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Numero de lote</Label>
                    <Input
                      type="number"
                      name="lotNumber"
                      id="lotNumber"
                      value={reservaState.lotNumber}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Zona</Label>
                    <Input
                      type="text"
                      name="zone"
                      id="zone"
                      value={reservaState.zone}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Bloque</Label>
                    <Input
                      type="text"
                      name="block"
                      id="block"
                      value={reservaState.block}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Direccion</Label>
                    <Input
                      type="text"
                      name="address"
                      id="address"
                      value={reservaState.address}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Pago Total</Label>
                    <Input
                      type="number"
                      name="totalAmount"
                      id="totalAmount"
                      value={reservaState.totalAmount}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Cantidad de Pagos</Label>
                    <Input
                      type="number"
                      name="amountOfPayments"
                      id="amountOfPayments"
                      value={reservaState.amountOfPayments}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Estado</Label>
                    <Input
                      type="text"
                      name="reserveStatus"
                      id="reserveStatus"
                      value={reservaState.reserveStatus}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={saveChanges}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={cancelChanges}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
    default:
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>
              <strong> Datos de nueva nacionalidad </strong>
            </CardHeader>
            <CardBody>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label htmlFor="lastName">Cliente</Label>
                    <Input
                      type="text"
                      id="id"
                      name="id"
                      value={clientSelectedName}
                      // onChange={handleInputChange}
                      disabled={true}
                      required
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <Row>
                    <Label htmlFor="Segundo Nombre"> Buscar </Label>
                  </Row>
                  <Row>
                    <Button
                      color="primary"
                      onClick={togglePrimary}
                      style={{ width: "350px" }}
                    >
                      Seleccionar Cliente
                    </Button>
                    <Modal
                      isOpen={reservaState.state.large}
                      toggle={togglePrimary}
                      className={"modal-lg"}
                    >
                      <ModalHeader toggle={togglePrimary}>
                        Seleccionar Cliente
                      </ModalHeader>
                      <ModalBody>
                        <FormGroup row>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button type="button" color="primary">
                                  <i className="fa fa-search" /> Buscar
                                </Button>
                              </InputGroupAddon>
                              <Input
                                type="text"
                                id="input1-group2"
                                name="input1-group2"
                                placeholder="Buscar"
                                value={valueClient}
                                onChange={handleValueChange}
                              />
                            </InputGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="12">
                            <InputGroup>
                              <Label htmlFor="Id">Nombre</Label>
                              <Input
                                type="text"
                                id="input1-group2"
                                name="input1-group2"
                                value={ObtenerNombreCliente}
                                onChange={handleValueChange}
                              />
                            </InputGroup>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="12">
                            {clients === null ? (
                              <tr>
                                <td colSpan={16}>Buscando...</td>
                              </tr>
                            ) : clients.length === 0 ? (
                              <tr>
                                <td colSpan={16}>
                                  No se encontraron proyectos.
                                </td>
                              </tr>
                            ) : (
                              <Table
                                hover
                                bordered
                                striped
                                responsive
                                size="sm"
                              >
                                <thead>
                                  <tr>
                                    <th style={thStyle}>Seleccionar</th>
                                    <th style={thStyle}>Identitdad</th>
                                    <th style={thStyle}>Codigo</th>
                                    <th style={thStyle}>Primer Nombre</th>
                                    <th style={thStyle}>Segundo Nombre</th>
                                    <th style={thStyle}>Primer Apellido</th>
                                    <th style={thStyle}>Segundo Apellido</th>
                                    <th style={thStyle}>Agente</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {clients.clients.map((client) => (
                                   
                                    <tr key={client.id}>
                                      <td>
                                        <Button
                                          onClick={() => {
                                            setRowClientSelected(client);
                                          }}
                                        >
                                          <img
                                            src={checkmark}
                                            height="40px"
                                            width="40px"
                                            alt="checkmark"
                                          />
                                        </Button>
                                      </td>
                                      <td>{client.principal.identificationCard}</td>
                                      <td>{client.principal.id}</td>
                                      <td>{client.principal.firstName}</td>
                                      <td>{client.principal.middleName}</td>
                                      <td>{client.principal.firstSurName}</td>
                                      <td>{client.principal.secondSurName}</td>
                                      <td>{client.principal.salesAdvisor}</td>
                                    </tr>
                                  ))}
                                </tbody>
                              </Table>
                            )}
                          </Col>
                          <Col md="12">
                            <InputGroup>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={prevPageProjects}
                                >
                                  <i className="icon-arrow-left-circle" />{" "}
                                  Anterior
                                </Button>
                              </InputGroupAddon>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Paginas: {clients.pageCount}</strong>
                                </h5>
                              </div>
                              <div
                                className="animated fadeIn"
                                style={{
                                  marginTop: "7px",
                                  marginLeft: "8px",
                                  marginRight: "8px",
                                }}
                              >
                                <h5>
                                  <strong>Actual: {clients.pageIndex}</strong>
                                </h5>
                              </div>
                              <InputGroupAddon addonType="prepend">
                                <Button
                                  type="button"
                                  color="primary"
                                  onClick={nextPageProjects}
                                >
                                  Proximo{" "}
                                  <i className="icon-arrow-right-circle" />
                                </Button>
                              </InputGroupAddon>
                            </InputGroup>
                          </Col>
                        </FormGroup>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={togglePrimary}>
                          Cancel
                        </Button>
                      </ModalFooter>
                    </Modal>
                  </Row>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label>Poyecto:</Label>
                    <Input
                      type="select"
                      name="projectId"
                      value={reservaState.projectId}
                      onChange={handleInputChange}
                    >
                      {projectOptions}
                    </Input>
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label>Agente de ventas:</Label>
                    <Input
                      type="select"
                      name="salesAdvisorId"
                      value={reservaState.salesAdvisorId}
                      onChange={handleInputChange}
                    >
                      {salesAdvisorsOptions}
                    </Input>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Numero de lote</Label>
                    <Input
                      type="number"
                      name="lotNumber"
                      id="lotNumber"
                      value={reservaState.lotNumber}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Zona</Label>
                    <Input
                      type="text"
                      name="zone"
                      id="zone"
                      value={reservaState.zone}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Bloque</Label>
                    <Input
                      type="text"
                      name="block"
                      id="block"
                      value={reservaState.block}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Direccion</Label>
                    <Input
                      type="text"
                      name="address"
                      id="address"
                      value={reservaState.address}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Pago Total</Label>
                    <Input
                      type="number"
                      name="totalAmount"
                      id="totalAmount"
                      value={reservaState.totalAmount}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Cantidad de Pagos</Label>
                    <Input
                      type="number"
                      name="amountOfPayments"
                      id="amountOfPayments"
                      value={reservaState.amountOfPayments}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12">
                  <FormGroup>
                    <Label for="exampleText">Estado</Label>
                    <Input
                      type="text"
                      name="reserveStatus"
                      id="reserveStatus"
                      value={reservaState.reserveStatus}
                      onChange={handleInputChange}
                    />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
            <CardFooter>
              <Button
                type="submit"
                size="sm"
                color="success"
                onClick={() => {
                  saveChanges(reservaState.currentClient);
                }}
              >
                <i className="fa fa-dot-circle-o" /> Guardar
              </Button>
              <Button
                type="reset"
                size="sm"
                color="danger"
                onClick={cancelChanges}
              >
                <i className="fa fa-ban" /> Cancelar
              </Button>
            </CardFooter>
          </Card>
        </div>
      );
  }
};
export default ReservaDetail;
