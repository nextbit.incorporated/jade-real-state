import React, { useState, useEffect } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Redirect } from "react-router-dom";
import API from "./../API/API";
import {
  CardBody,
  Col,
  FormGroup,
  Button,
  Row,
  Card,
  CardHeader,
} from "reactstrap";
import ReservaDetail from "./reservaDetail";
import ReservaTabla from "./reservaTabla";

const heightStyle = {
  height: "500px",
};
const initialClientState = {
  step: 1,
  editMode: "None",
  saveStatus: false,
  currentClient: {
    id: 0,
    clientId: 0,
    projectId: 0,
    salesAdvisorId: 0,
    lotNumber: 0,
    zone: "",
    block: "",
    address: "",
    totalAmount: 0.0,
    amountOfPayments: 0,
    reserveStatus: "",
    salesAdvisorName: "",
    projectName: "",
    clientName: "",
  },
  state: {
    modal: false,
    large: false,
    large1: false,
    small: false,
    primary: false,
    success: false,
    warning: false,
    danger: false,
    info: false,
    uploadFile: false,
    className: null,
  },
  projects: [],
  salesAdvisors: [],
  services: [],
};

const Reserva = (props) => {
  const [reservaState, setreservaState] = useState(initialClientState);
  const [salesAdvisors, setSalesAdvisors] = useState([]);
  const [reserves, setReserves] = useState([]);
  const [projects, setProjects] = useState([]);
  const [apiCallInProgress, setApiCallInProgress] = useState(false);
  const [clientState, setclientState] = useState([]);

  function fetchReserve(query, step) {
    const index = step === undefined || step === null ? 0 : step;
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      props.history.push("/dashboard");
      return <Redirect to="/dashboard"></Redirect>;
    }
    if (query === null) {
      var url = `Reserve/paged?pageSize=10&pageIndex=${index}`;

      API.get(url)
        .then((res) => {
          setReserves(res.data);
        })
        .catch((error) => {
          if (error.message === "Network Error") {
            toast("Error de red");
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
          if (error.response.status === 401) {
            props.history.push("/login");
            return <Redirect to="/login"></Redirect>;
          }
        });
    } else {
      var url1 = `Reserve/paged?pageSize=10&pageIndex=${index}&value=${query}`;
      API.get(url1).then((res) => {
        setReserves(res.data);
      });
    }
  }


  function fetchClientsPaged(query, step) {
    const user = sessionStorage.getItem("userId");

    setApiCallInProgress(true);
    const index = step === undefined || step === null ? 0 : step;
    if (query === null || query === "") {
      var url = `clients/paged?pageSize=10&pageIndex=${index}&user=${user}&isActive=${false}`;

      API.get(url)
        .then((res) => {
          setApiCallInProgress(false);
          setclientState(res.data);
        })
        .catch((error) => {
          setApiCallInProgress(false);
          if (error.message === "Network Error") {
            toast.warn(
              "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
            );
          } else {
            if (error.response.status !== undefined) {
              if (error.response.status === 401) {
                props.history.push("/login");
                return <Redirect to="/login" />;
              }
            } else {
              toast.warn(
                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
              );
            }
          }
        });
    } else {
      var url1 = `clients/paged?pageSize=10&pageIndex=${index}&user=${user}&value=${query}&isActive=${false}`;
      API.get(url1).then((res) => {
        setApiCallInProgress(false);
        setclientState(res.data);
      });
    }
  }

  function fetchSalesAdvisors() {
    const url = `user`;

    API.get(url).then((res) => {
      const salesAdvisors = [
        { id: 0, name: "(Seleccione un Asesor de  Ventas)" },
      ].concat(
        res.data
          .map((user) => {
            return {
              id: user.id,
              name: user.firstName + " " + user.lastName,
            };
          })
          .sort(compareNames)
      );

      setSalesAdvisors(salesAdvisors);
    });
  }

  function fetchProjects() {
    const url = `projects`;

    API.get(url).then((res) => {
      const projects = [{ id: 0, name: "(Seleccione un Proyecto)" }].concat(
        res.data.sort(compareNames)
      );

      setProjects(projects);
    });
  }

  useEffect(() => {
    fetchReserve(null, 0);
    fetchProjects();
    fetchSalesAdvisors();
    fetchClientsPaged(null);
  }, []);

  const showNewDialog = () => {
    const client = {
      ...initialClientState.currentClient,
    };
    setreservaState({
      ...reservaState,
      editMode: "Adding",
      currentClient: client,
      step: 2,
      projects,
      salesAdvisors,
    });
  };

  const editRow = (client) => {
    setreservaState({
      ...reservaState,
      editMode: "Editing",
      currentClient: client,
      step: 2,
      projects,
      salesAdvisors,
    });
  };

  const deleteReserve = (id) => {
    const url = `Reserve?id=${id}`;

    API.delete(url).then((res) => {
      setreservaState(initialClientState);
      fetchReserve(null, 0);
      fetchProjects();
      fetchSalesAdvisors();
      toast.success("Cliente eliminado correctamente");
    });
  };

  function nextStep(client) {
    const { step } = reservaState;

    setreservaState({ ...reservaState, currentClient: client, step: step + 1 });
  }

  function prevStep() {
    const { step } = reservaState;

    setreservaState({ ...reservaState, step: step - 1 });
  }

  const cancelChanges = () => {
    setreservaState(initialClientState);
    fetchReserve(null, 0);
    fetchProjects();
    fetchSalesAdvisors();
  };

  function compareNames(currentItem, nextItem) {
    const currentName = currentItem.name;
    const nextName = nextItem.name;

    let comparison = 0;
    if (currentName > nextName) {
      comparison = 1;
    } else if (currentName < nextName) {
      comparison = -1;
    }
    return comparison;
  }

  function nextPage(step, value) {
    fetchReserve(value, step);
  }

  function prevPage(step, value) {
    fetchReserve(value, step);
  }

  const addClient = (newClient) => {
    const url = `reserve`;

    setApiCallInProgress(true);

    const reserve = {
      id: 0,
      clientId: newClient.clientId,
      projectId: parseInt(newClient.projectId),
      salesAdvisorId: parseInt(newClient.salesAdvisorId),
      lotNumber: parseInt(newClient.lotNumber),
      zone: newClient.zone,
      block: newClient.block,
      address: newClient.address,
      totalAmount: parseFloat(newClient.totalAmount),
      amountOfPayments: parseInt(newClient.amountOfPayments),
      reserveStatus: newClient.reserveStatus,
    };

    API.post(url, reserve)
      .then((res) => {
        setApiCallInProgress(false);
        setreservaState(initialClientState);
        fetchReserve(null, 0);
        fetchProjects();
        fetchSalesAdvisors();
        toast.success("reserva agregado satisfactorimente");
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para crear el nuevo cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  };

  const updateClient = (updatedClient) => {
    const url = `reserve`;

    setApiCallInProgress(true);

    API.put(url, updatedClient)
      .then((res) => {
        setApiCallInProgress(false);
        setreservaState(initialClientState);
        fetchReserve(null, 0);
        fetchProjects();
        fetchSalesAdvisors();
        toast.success("datos del cliente modificados satisfactorimente");
      })
      .catch((error) => {
        setApiCallInProgress(false);

        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  };

  const saveChanges = (reserva) => {
    switch (reservaState.editMode) {
      case "Adding": {
        addClient(reserva);

        break;
      }
      case "Editing":
        updateClient(reserva);
        break;

      default:
        break;
    }
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody>
            <FormGroup row>
              <Col />

              <Col xs="auto">
                <Button
                  type="button"
                  color="success"
                  onClick={() => showNewDialog()}
                >
                  <i className="fa fa-new" /> Agregar Reserva
                </Button>
              </Col>
            </FormGroup>

            <FormGroup row>
              <Col xs="12">
                <ReservaTabla
                  reserves={reserves}
                  fetchReserve={fetchReserve}
                  editRow={editRow}
                  deleteReserve={deleteReserve}
                  nextStep={nextStep}
                  prevStep={prevStep}
                  nextPage={nextPage}
                  prevPage={prevPage}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      case 2:
        return (
          <ReservaDetail
            reservaState={reservaState}
            setreservaState={setreservaState}
            saveChanges={saveChanges}
            cancelChanges={cancelChanges}
            apiCallInProgress={apiCallInProgress}
          />
        );
      default:
        return null;
    }
  }

  const clientsStep = GetCurrentStepComponent(reservaState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <Card>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify" /> Reservas
              </CardHeader>
              {clientsStep}
            </Card>
          </Col>
        </Row>
      </Card>
      <ToastContainer autoClose={5000} />
    </div>
  );
};
export default Reserva;
