import React, { useState } from "react";

import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input
} from "reactstrap";

const ReservaTabla = props => {
  const { reserves, fetchReserve, deleteReserve, nextStep, prevStep } = props;
  const [value, setValue] = useState("");

  const thStyle = {
    background: "#20a8d8",
    color: "white"
  };

  function handleValueChange(e) {
    fetchReserve(e.target.value);
    setValue(e.target.value);
  }

  const userRows =
    reserves === null ? (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>Buscando...</td>
          </tr>
        </thead>
      </Table>
    ) : reserves.length === 0 ? (
      <Table hover bordered striped responsive size="sm">
        <thead>
          <tr>
            <td colSpan={16}>
              No se encontraron usuarios con el filtro especificado.
            </td>
          </tr>
        </thead>
      </Table>
    ) : (
      <div>
        <FormGroup row>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary">
                  <i className="fa fa-search" /> Buscar
                </Button>
              </InputGroupAddon>
              <Input
                type="text"
                id="input1-group2"
                name="input1-group2"
                placeholder="Buscar"
                value={value}
                onChange={handleValueChange}
              />
            </InputGroup>
          </Col>
        </FormGroup>

        <FormGroup row>
          <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  {/* <th style={thStyle}>Editar</th> */}
                  <th style={thStyle}>Borrar</th>
                  <th style={thStyle}>ID</th>
                  <th style={thStyle}>Cliente</th>
                  <th style={thStyle}>Poyecto</th>
                  <th style={thStyle}>Numero de Lote</th>
                  <th style={thStyle}>Zona</th>
                  <th style={thStyle}>Bloque</th>
                  <th style={thStyle}>Direccion</th>
                  <th style={thStyle}>Monto</th>
                  <th style={thStyle}>Cantidad de pagos</th>
                  <th style={thStyle}>Estado</th>
                </tr>
              </thead>
              <tbody>
                {reserves.reserves.map(reserve => (
                  <tr key={reserve.id}>
                    {/* <td>
                                                    <Button
                                                        block
                                                        color="warning"
                                                        onClick={() => {
                                                            editRow(reserve);
                                                        }}
                                                    >
                                                        Editar
                      </Button>
                                                </td> */}
                    <td>
                      <Button
                        block
                        color="danger"
                        onClick={() => deleteReserve(reserve.id)}
                      >
                        Borrar
                      </Button>
                    </td>
                    <td>{reserve.id}</td>
                    <td>{reserve.clientName}</td>
                    <td>{reserve.projectName}</td>
                    <td>{reserve.lotNumber}</td>
                    <td>{reserve.zone}</td>
                    <td>{reserve.block}</td>
                    <td>{reserve.address}</td>
                    <td>{reserve.totalAmount}</td>
                    <td>{reserve.amountOfPayments}</td>
                    <td>{reserve.reserveStatus}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={prevStep}>
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px"
                }}
              >
                <h5>
                  <strong>Paginas: {reserves.pageCount}</strong>
                </h5>
              </div>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px"
                }}
              >
                <h5>
                  <strong>Actual: {reserves.pageIndex}</strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button type="button" color="primary" onClick={nextStep}>
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      </div>
    );
  return userRows;
};
export default ReservaTabla;
