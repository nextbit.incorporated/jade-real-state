import React, { useState, useEffect } from "react";
import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
  Row,
  Label,
  CardBody,
  Card,
} from "reactstrap";
import styled from "styled-components";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { toast, ToastContainer } from "react-toastify";
import moment from "moment";
import API from "./../../API/API";
import { Redirect } from "react-router-dom";
import 'moment/locale/es'


const heightStyle = {
    height: "500px",
};


const initialOperationState = {
    step: 1,
    editMode: "None",
    startDate: Date.now(),  
    currentOperation: {
        id: "",
        clientName: "",
        operationName: "",
        operationType: "",
        oldValue: "",
        newValue: "",
        comments: "",
        error: ""
    },
};

const thStyle = {
    background: "#20a8d8",
    color: "white",
  };


const LogOperationsReport = (props) =>{
   const [mainPropertyState, setMainPropertyState] = useState(initialOperationState);
   const [onlyLartTopState, setOnlyLartTopState] = useState(false);
   const [apiCallInProgress, setApiCallInProgress] = useState(false);
   const [initialDate, setInitialDate] = useState("");
   const [finalDate, setFinalDate] = useState("");
   const [operationsState, setOperationsState] = useState([]);

   useEffect(() => {
    setOperationsState([]);
    },[]);


    const handleShowActiveChange = (event) => {
        setOnlyLartTopState(!onlyLartTopState)
      };

      const handleChangeInitialDate = (date) => {
        setInitialDate(date);
      };
      const handleChangeFinalDate = (date) => {
        setFinalDate(date);
      };

      function findClients() {
       

          const user = sessionStorage.getItem("userId");
          if (initialDate === undefined || initialDate === null) return;
          if (finalDate === undefined || finalDate === null) return;
          const stDate = moment(initialDate).format("MM/DD/YYYY");
          const endDate = moment(finalDate).format('MM/DD/YYYY');  

          var url = `operations`;
          if(onlyLartTopState)
          {
              url = `operations/gestiones?OnlyLastTop=${onlyLartTopState}&User=${user}&TypeReport=general`
          }
          else
          {

            if (
                initialDate === undefined ||
                initialDate === null ||
                initialDate === ""
              ) {
                toast.warn("Fecha inicial obligatorio");
                return;
              }
              if (finalDate === undefined || finalDate === null || finalDate === "") {
                toast.warn("Fecha final obligatorio");
                return;
              }
              url = `operations/gestiones?OnlyLastTop=${onlyLartTopState}&User=${user}&InitialDate=${stDate}&FinalDate=${endDate}&TypeReport=general`
          }

          API.get(url)
          .then((res) => {
              setOperationsState(res.data);
          })
          .catch((error) => {
              if (error.message === "Network Error") {
                  toast.warn(
                      "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                  );
              } else {
                  if (error.response.status !== undefined) {
                      if (error.response.status === 401) {
                          props.history.push("/login");
                          return <Redirect to="/login" />;
                      }
                  } else {
                      toast.warn(
                          "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                      );
                  }
              }
          });


      };

      function CleanDataReport(){
        setOperationsState([]);
        setOnlyLartTopState(false);
    };
    
    

    return (
        <div className="animated fadeIn" style={heightStyle}>
          <FormGroup row>
          <Col md="2" sm="6" xs="12">
          <FormGroup>
            <Row>
              <Label htmlFor="Fecha"> Fecha Inicial</Label>
            </Row>
            <Row>
              <DatePicker
                selected={initialDate}
                onChange={handleChangeInitialDate}
              />
            </Row>
          </FormGroup>
        </Col>
        <Col md="2" sm="6" xs="12">
          <FormGroup>
            <Row>
              <Label htmlFor="Fecha"> Fecha Final</Label>
            </Row>
            <Row>
              <DatePicker
                selected={finalDate}
                onChange={handleChangeFinalDate}
              />
            </Row>
          </FormGroup>
        </Col>
          <Col md="2" sm="6" xs="12">
                    <Input
                        type="checkbox"
                        name="showActive"
                        checked={onlyLartTopState}
                        onChange={handleShowActiveChange}
                        required
                    />{" "}
                    Mostrar ultimos 20
            </Col>
            <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button type="button" color="primary" onClick={findClients}>
                Generar <i className="icon-arrow-right-circle" />
              </Button>
            </InputGroupAddon>
          </InputGroup>
        </Col>

        <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button type="button" color="primary" onClick={CleanDataReport}>
                Limpiar <i className="icon-arrow-right-circle" />
              </Button>
            </InputGroupAddon>
          </InputGroup>
        </Col>
        </FormGroup>
                  
      <FormGroup row>
        <Col md="12">
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th nowrap="true" style={thStyle}>Codigo</th>
                  <th nowrap="true" style={thStyle}>Fecha</th>
                  <th nowrap="true" style={thStyle}>Cliente</th>
                  <th nowrap="true" style={thStyle}>Operacion</th>
                  <th nowrap="true" style={thStyle}>Tipo operacion</th>
                  <th nowrap="true" style={thStyle}>Valor anterior</th>
                  <th nowrap="true" style={thStyle}>Nuevo valor</th>
                  <th nowrap="true" style={thStyle}>Comentario</th>
                  <th nowrap="true" style={thStyle}>Gestionado Por</th>
                </tr>
              </thead>
              <tbody>
                {operationsState.map((client) => (
                  <tr key={client.id}>
                    <td nowrap="true">{client.id}</td>
                    <td nowrap="true">{moment(client.fechaTransaccion).locale('es').format('MMMM Do YYYY, h:mm:ss a') }</td>
                    <td nowrap="true">{client.clientName }</td>
                    <td nowrap="true">{client.operationName }</td>
                    <td nowrap="true">{client.operationType  }</td>
                    <td nowrap="true">{client.oldValue}</td>
                    <td nowrap="true">{client.newValue}</td>
                    <td nowrap="true">{client.comments}</td>
                    <td nowrap="true">{client.modifiedBy}</td>
                  </tr>
                ))}
              </tbody>
                </Table>
                </Col>
            </FormGroup>
            <ToastContainer autoClose={5000} />
        </div>
      
    );
}

export default LogOperationsReport;