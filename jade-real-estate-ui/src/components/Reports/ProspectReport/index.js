import React, { useState, useEffect } from "react";
import API from "./../../API/API";
import { toast } from "react-toastify";
import ProspectTableReport from "./ProspectTableReport";
import { CardBody, Col, FormGroup, Button } from "reactstrap";
import moment from "moment";
import { Redirect } from "react-router-dom";

const heightStyle = {
    height: "500px",
};


const initialClientState = {
    step: 1,
    editMode: "None",
    startDate: Date.now(),
    file: [],
    fileName: "",
    infoDataxlsx: [],
    currentProspect: {
        id: "",
        creationDate: "",
        registerDate: "",
        projectId: "",
        contactType: "",
        name: "",
        idNumber: "",
        contactNumberOne: "",
        contactNumberTwo: "",
        email: "",
        projectName: "",
        contactTypeName: "",
        project: [],
        contactTypes: [],
    },
    projects: [],
    contactTypes: [],
    salesAdvisors: [],
}

const opcionReport = [
    {
        id: 1,
        name: "Rango Fecha"
    },
    {
        id: 2,
        name: "Por Agente"
    },
]

const ProspectReport = (props) => {
    const [ProspectReportState, setProspectReportState] = useState(initialClientState)
    const [clients, setClients] = useState([]);
    const [salesAdvisors, setSalesAdvisors] = useState([]);
    const [reportTypeState, setreportTypeState] = useState(opcionReport);
    const [reporTypeSelectedState, setreporTypeSelectedState] = useState("1");
    const [reportTypeSelectedState, setreportTypeSelectedState] = useState({
        id: 1,
        name: "Rango Fecha"
    });
    const [usersstate, setUsersstate] = useState([]);
    const [ProjectOptionsState, setProjectOptionsState] = useState([]);
    const [userSelectedState, setuserSelectedState] = useState({});

    useEffect(() => {
        fetchUsersReport();
        fetchProjectOptions();
    }, [props.history]);

    function compareNames(currentItem, nextItem) {
        const currentName = currentItem.name;
        const nextName = nextItem.name;

        let comparison = 0;
        if (currentName > nextName) {
            comparison = 1;
        } else if (currentName < nextName) {
            comparison = -1;
        }
        return comparison;
    }

    function fetchUsersReport() {
        const user = sessionStorage.getItem("userId");
        const url = `clients/users-report?request.user=${user}`;

        API.get(url).then((res) => {
            if (res.data === null || res.data === undefined) {
                return;
            }
            const usuarioOpciones = [
                {
                    id: 0,
                    name: "(Todos los usuarios)",
                    firstName: "(Todos los usuarios)",
                    lastName: '', value: 0
                },
            ].concat(res.data);

            setUsersstate(usuarioOpciones);
        
        });
    }

    function fetchProjectOptions() {
        const url = `Projects`;

        API.get(url).then(
            (res) => {
                if (res.data === null || res.data === undefined) {
                    return;
                }

                const ProjectOptions = [
                    { id: 0, 
                    name: "(Seleccione un Tipo de Proyecto)", 
                    firstName: "(Seleccione un Tipo de Proyecto)", 
                    lastName: '', value: 0 },
                ].concat(res.data); 
                setProjectOptionsState(ProjectOptions);
            });
    }

    const style = {
        height: "100%",
        maxwidth: "100%",
        maxheight: "100%",
        margin: "0",
        padding: "0",
        marginleft: "0px",
        marginright: "0px",
        paddingright: "0px",
        paddingleft: "0px",
    };

    function fetchProspectReport(
        initialDate,
        finalDate,
        userIdSelected,
        typeReportSelected
    ) {

        const user = sessionStorage.getItem("userId");
        if (typeReportSelected === '1')
        {
            if (initialDate === undefined || initialDate === null) return;
            if (finalDate === undefined || finalDate === null) return;
            const stDate = moment(initialDate).format("L");
            const endDate = moment(finalDate).format("L");  
            const url = `ProspectReport?request.user=${user}&&request.startDate=${stDate}&request.finalDate=${endDate}&request.usuarioConsultado=${userIdSelected}`;

            API.get(url)
                .then((res) => {
                    setClients(res.data);
                })
                .catch((error) => {
                    if (error.message === "Network Error") {
                        toast.warn(
                            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                        );
                    } else {
                        if (error.response.status !== undefined) {
                            if (error.response.status === 401) {
                                props.history.push("/login");
                                return <Redirect to="/login" />;
                            }
                        } else {
                            toast.warn(
                                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                            );
                        }
                    }
                });
        }
       

        if (typeReportSelected === '2')
        {
            if (initialDate === undefined || initialDate === null) return;
            if (finalDate === undefined || finalDate === null) return;
            const stDate = moment(initialDate).format("L");
            const endDate = moment(finalDate).format("L");  
            var url = ``;
            if (userIdSelected === '' || userIdSelected === undefined)
            {
                url = `ProspectReport/ByUser?request.user=${user}&request.startDate=${stDate}&request.finalDate=${endDate}`;
            }
            else
            {
                url = `ProspectReport/ByUser?request.user=${user}&request.usuarioConsultado=${userIdSelected}&request.startDate=${stDate}&request.finalDate=${endDate}`;
            }
           

            API.get(url)
                .then((res) => {
                    setClients(res.data);
                })
                .catch((error) => {
                    if (error.message === "Network Error") {
                        toast.warn(
                            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                        );
                    } else {
                        if (error.response.status !== undefined) {
                            if (error.response.status === 401) {
                                props.history.push("/login");
                                return <Redirect to="/login" />;
                            }
                        } else {
                            toast.warn(
                                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                            );
                        }
                    }
                });
        }
    }

    function CleanDataReport(){
        setClients([]);
    setProspectReportState(initialClientState);
    
    }

    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody style={style}>
                        <FormGroup row>
                            <Col />

                            <Col xs="auto"></Col>
                        </FormGroup>

                        <FormGroup row>
                            <Col xs="12">
                                <ProspectTableReport
                                    reportTypeState={reportTypeState}
                                    setreporTypeSelectedState={setreporTypeSelectedState}
                                    reporTypeSelectedState={reporTypeSelectedState}
                                    usersstate={usersstate}
                                    setuserSelectedState={setuserSelectedState}
                                    userSelectedState={userSelectedState}
                                    fetchProspectReport={fetchProspectReport}
                                    clients={clients}
                                    CleanDataReport={CleanDataReport}
                                />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );

            // case 2:
            //     return (
            //         // <ExportClienstReport clients={clients} cancelStep={cancelStep} />
            //     );

            default:
                return null;
        }
    }


    const clientsStep = GetCurrentStepComponent(ProspectReportState.step);

    return (
        <div className="animated fadeIn" style={heightStyle}>
            {clientsStep}
        </div>
    );
}

export default ProspectReport;