import React, { useState, useEffect } from "react";
import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
  Row,
  Label,
} from "reactstrap";
import moment from "moment";
import styled from "styled-components";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { toast } from "react-toastify";
import ReactExport from "react-data-export";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ProspectTableReport = (props) => {
  const {
    reportTypeState,
    setreporTypeSelectedState,
    reporTypeSelectedState,
    usersstate,
    setuserSelectedState,
    userSelectedState,
    fetchProspectReport,
    clients,
    CleanDataReport,
  } = props;
  const [initialDate, setInitialDate] = useState("");
  const [finalDate, setFinalDate] = useState("");
  const [userIdSelected, setUserIdSelected] = useState("");
  const [userSelected, setUserSelected] = useState("");

  useEffect(() => {
    notify();
  }, []);

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  const notify = (warningMessage) => {
    toast.warn(warningMessage, {
      position: toast.POSITION.BOTTOM_LEFT,
    });
  };

  const Root = styled.div`
    height: 63vh;
    max-height: 63vh;
    overflow-y: auto;
    overflow-x: auto;
  `;

  const typeOptions = reportTypeState.map((p) => {
    return (
      <option id={p.id} key={p.id} value={p.id} name={p.name}>
        {p.name}
      </option>
    );
  });

  const handleInputChangeTypeSelected = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    setreporTypeSelectedState(value);
  };

  const handleInputChangeUserSelected = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    setuserSelectedState(value);
  };

  const usersOptions = usersstate.map((p) => {
    return (
      <option
        id={p.userId}
        key={p.userId}
        value={p.userId}
        name={p.firstName + " " + p.lastName}
      >
        {p.firstName + " " + p.lastName}
      </option>
    );
  });

  const handleChangeInitialDate = (date) => {
    setInitialDate(date);
  };
  const handleChangeFinalDate = (date) => {
    setFinalDate(date);
  };

  function findClients() {
    if (
      initialDate === undefined ||
      initialDate === null ||
      initialDate === ""
    ) {
      notify("Fecha inicial obligatorio");
      return;
    }
    if (finalDate === undefined || finalDate === null || finalDate === "") {
      toast("Fecha final obligatorio");
      return;
    }

    fetchProspectReport(
      initialDate,
      finalDate,
      userSelectedState,
      reporTypeSelectedState
    );
  }

  function getAccessDownloadButton(e) {
    var lista = [];
    clients.map((client) =>
      lista.push({
        id: client.id,
        creationDate: moment(client.creationDate).format("L"),
        name: client.name,
        contactNumberOne: client.contactNumberOne,
        email: client.email,
        contactTypeName: client.contactTypeName,
        project: client.project.name,
        comments: client.comments,
        salesAdvisor: client.salesAdvisor,
      })
    );
    return (
      <ExcelFile
        element={
          <Button type="button" color="success">
            <i className="fa fa-new" /> Descargar archivo
          </Button>
        }
      >
        <ExcelSheet data={lista} name="Clientes Varios">
          <ExcelColumn label="Id" value="id" />
          <ExcelColumn label="Fecha Ingreso" value="creationDate" />
          <ExcelColumn label="Nombre" value="name" />
          <ExcelColumn label="Contacto" value="contactNumberOne" />
          <ExcelColumn label="Correo" value="email" />
          <ExcelColumn label="Tipo Contacto" value="contactTypeName" />
          <ExcelColumn label="Proyecto" value="project" />
          <ExcelColumn label="Comentario" value="comments" />
          <ExcelColumn label="Asesor" value="salesAdvisor" />
        </ExcelSheet>
      </ExcelFile>
    );
  }

  const clientRows = (
    <div>
      <FormGroup row>
        <Col md="2" sm="6" xs="12">
          <FormGroup>
            <Label>Filtro</Label>
            <Input
              style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
              type="select"
              name="contactTypeId"
              value={reporTypeSelectedState}
              onChange={handleInputChangeTypeSelected}
            >
              {typeOptions}
            </Input>
          </FormGroup>
        </Col>
        <Col md="2" sm="6" xs="12">
          <FormGroup>
            <Label>Agentes</Label>
            <Input
              style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
              type="select"
              name="contactTypeId"
              value={userSelectedState}
              onChange={handleInputChangeUserSelected}
            >
              {usersOptions}
            </Input>
          </FormGroup>
        </Col>
        <Col md="2" sm="6" xs="12">
          <FormGroup>
            <Row>
              <Label htmlFor="Fecha"> Fecha Inicial</Label>
            </Row>
            <Row>
              <DatePicker
                selected={initialDate}
                onChange={handleChangeInitialDate}
              />
            </Row>
          </FormGroup>
        </Col>
        <Col md="2" sm="6" xs="12">
          <FormGroup>
            <Row>
              <Label htmlFor="Fecha"> Fecha Final</Label>
            </Row>
            <Row>
              <DatePicker
                selected={finalDate}
                onChange={handleChangeFinalDate}
              />
            </Row>
          </FormGroup>
        </Col>
        <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button type="button" color="primary" onClick={findClients}>
                Generar <i className="icon-arrow-right-circle" />
              </Button>
            </InputGroupAddon>
          </InputGroup>
        </Col>

        <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button type="button" color="primary" onClick={CleanDataReport}>
                Limpiar <i className="icon-arrow-right-circle" />
              </Button>
            </InputGroupAddon>
          </InputGroup>
        </Col>

        <Col md="3" sm="6" xs="12">
          {getAccessDownloadButton()}
        </Col>
      </FormGroup>

      <FormGroup row>
        <Col md="12">
          <Root>
            <Table hover bordered striped responsive size="sm">
              <thead>
                <tr>
                  <th nowrap="true" style={thStyle}>
                    Codigo
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Fecha de Creacion
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Nombre
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Contacto
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Correo
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Tipo Contacto
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Proyecto
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Comentario
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Asesor de Ventas
                  </th>
                </tr>
              </thead>
              <tbody>
                {clients.map((client) => (
                  <tr key={client.id}>
                    <td nowrap="true">{client.id}</td>
                    <td nowrap="true">
                      {moment(client.creationDate).format("L")}
                    </td>
                    <td nowrap="true">{client.name}</td>
                    <td nowrap="true">{client.contactNumberOne}</td>
                    <td nowrap="true"> {client.email} </td>
                    <td nowrap="true">{client.contactTypeName}</td>
                    <td nowrap="true">{client.project.name}</td>
                    <td nowrap="true">{client.comments}</td>
                    <td nowrap="true">{client.salesAdvisor}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Root>
        </Col>
      </FormGroup>
    </div>
  );

  return clientRows;
};

export default ProspectTableReport;
