import React, { useState, useEffect } from "react";
import API from "./../../API/API";
import ClientByUserReportTable from "./clientByUserReportTable";
import { CardBody, Col, FormGroup, Button } from "reactstrap";
import moment from "moment";

import { ToastContainer, toast } from "react-toastify";
import { Redirect } from "react-router-dom";

const heightStyle = {
  height: "500px",
};

const clientState = {
  step: 1,
  editMode: "None",
  currentClient: {
    id: 0,
    creationDate: "",
    projectId: 0,
    project: "",
    contactTypeId: 0,
    contactType: "",
    nationalityId: 0,
    nationality: "",

    coDebtorNationalityId: 0,
    coDebtorNationality: "",

    financialInstitutionId: 0,
    financialInstitution: "",

    salesAdvisorId: 0,
    salesAdvisor: "",

    firstName: "",
    middleName: "",
    firstSurname: "",
    secondSurname: "",
    coDebtorFirstName: "",
    coDebtorMiddleName: "",
    coDebtorFirstSurname: "",
    coDebtorSecondSurname: "",
    identificationCard: "",
    phoneNumber: "",
    cellPhoneNumber: "",
    coDebtorPhoneNumber: "",
    coDebtorCellPhoneNumber: "",
    coDebtorIdentificationCard: "",
    coDebtorWorkplace: "",
    email: "",
    coDebtorEmail: "",
    workplace: "",
    workStartDate: "",
    debtorCurrency: "LPS",
    coDebtorCurrency: "LPS",
    grossMonthlyIncome: 0,
    coDebtorGrossMonthlyIncome: 0,
    homeAddress: "",
    ownHome: false,
    firstHome: false,
    contributeToRap: false,
    comments: "",
    negotiationEnded: false,

    creditOfficer: "",
  },
  projects: [],
  contactTypes: [],
  nationalities: [],
  financialInstitutions: [],
  salesAdvisors: [],
};

const ClientsByUserReport = (props) => {
  const [clients, setClients] = useState([]);
  const [users, setUsers] = useState([]);
  const [userId, setUserId] = useState("");
  const [userName, setUserName] = useState("");
  const [levelOptionsState, setLevelOptionsState] = useState([]);
  const [apiCallInProgress, setApiCallInProgress] = useState(false);

  useEffect(() => {
    fetchUsersReport();
    fetClientLevel();
  }, []);

  function compareNames(currentItem, nextItem) {
    const currentName = currentItem.name;
    const nextName = nextItem.name;

    let comparison = 0;
    if (currentName > nextName) {
      comparison = 1;
    } else if (currentName < nextName) {
      comparison = -1;
    }
    return comparison;
  }

  function fetClientLevel() {
    const url = "clientLevel";

    API.get(url).then((res) => {
      if (res.data === null || res.data === undefined) {
        return;
      }
      const leveloptions = [{ id: 0, name: "Todos(All)" }].concat(
        res.data.sort(compareNames)
      );
      setLevelOptionsState(leveloptions);
    });
  }

  function fetchUsersReport() {
    const user = sessionStorage.getItem("userId");
    const url = `clients/users-report?request.user=${user}`;

    API.get(url).then((res) => {
      if (res.data === null || res.data === undefined) {
        return;
      }
      setUsers(res.data);
      if (res.data.length === 1) {
        setUserId(res.data[0].userId);
        setUserName(res.data[0].firstName);
      }
    });
  }

  function fetchClientsReport(
    initialDate,
    finalDate,
    userId,
    userSelected,
    userIdSelected,
    clientLevelSelectedState
  ) {
    if (initialDate === undefined || initialDate === null) return;
    if (finalDate === undefined || finalDate === null) return;
    const stDate = moment(initialDate).format("L");
    const endDate = moment(finalDate).format("L");
    const url = `clients/report-by-user?request.user=${userId}&request.fechaInicio=${stDate}&request.fechaFinal=${endDate}&request.clientLevelId=${clientLevelSelectedState}`;
    setApiCallInProgress(true);
    setClients([]);
    API.get(url)
      .then((res) => {
        var listClients = res.data;

        const flattenedClients = getFlattenedClients(listClients);
        setClients(flattenedClients);

        setUserId(userIdSelected);
        setUserName(userSelected);
        setApiCallInProgress(false);
      })
      .catch((error) => {
        setApiCallInProgress(false);
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          if (error.response.status !== undefined) {
            if (error.response.status === 401) {
              props.history.push("/login");
              return <Redirect to="/login" />;
            }
          } else {
            toast.warn(
              "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
            );
          }
        }
      });
  }

  const getFlattenedClients = (clients) => {
    return clients.map((client) => {
      return {
        id: client.principal.id,
        creationDate: moment(client.principal.creationDate).format("L"),
        clientCategory: client.principal.clientCategory,
        project: client.principal.project,
        contactType: client.principal.contactType,
        firstName: client.principal.firstName,
        middleName: client.principal.middleName,
        firstSurname: client.principal.firstSurname,
        secondSurname: client.principal.secondSurname,
        identificationCard: client.principal.identificationCard,
        nationality: client.principal.nationality,
        cellPhoneNumber: client.principal.cellPhoneNumber,
        phoneNumber: client.principal.phoneNumber,
        email: client.principal.email,
        homeAddress: client.principal.homeAddress,
        workplace: client.principal.workplace,
        workStartDate: moment(client.principal.workStartDate).format("L"),
        debtorCurrency: client.principal.debtorCurrency,
        grossMonthlyIncome: client.principal.grossMonthlyIncome,
        coDebtorFirstName: client.coDebtor && client.coDebtor.firstName,
        coDebtorMiddleName: client.coDebtor && client.coDebtor.middleName,
        coDebtorFirstSurname: client.coDebtor && client.coDebtor.firstSurname,
        coDebtorSecondSurname: client.coDebtor && client.coDebtor.secondSurname,
        coDebtorNationality: client.coDebtor && client.coDebtor.nationality,
        coDebtorIdentificationCard:
          client.coDebtor && client.coDebtor.identificationCard,
        coDebtorCellPhoneNumber:
          client.coDebtor && client.coDebtor.cellPhoneNumber,
        coDebtorPhoneNumber: client.coDebtor && client.coDebtor.phoneNumber,
        coDebtorEmail: client.coDebtor && client.coDebtor.email,
        coDebtorWorkplace: client.coDebtor && client.coDebtor.workplace,
        coDebtorCurrency: client.coDebtor && client.coDebtor.debtorCurrency,
        coDebtorGrossMonthlyIncome:
          client.coDebtor && client.coDebtor.grossMonthlyIncome,

        ownHome: client.principal.ownHome,
        firstHome: client.principal.firstHome,
        contributeToRap: client.principal.contributeToRap,
        comments: client.principal.comments,
        financialInstitution: client.principal.financialInstitution,
        creditOfficer: client.principal.creditOfficer,
        salesAdvisor: client.principal.salesAdvisor,
        negotiationEnded: client.principal.negotiationEnded,
      };
    });
  };

  const style = {
    height: "100%",
    maxwidth: "100%",
    maxheight: "100%",
    margin: "0",
    padding: "0",
    marginleft: "0px",
    marginright: "0px",
    paddingright: "0px",
    paddingleft: "0px",
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody style={style}>
            <FormGroup row>
              <Col />

              <Col xs="auto"></Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12">
                <ClientByUserReportTable
                  fetchClientsReport={fetchClientsReport}
                  clients={clients}
                  setClients={setClients}
                  users={users}
                  userName={userName}
                  userId={userId}
                  levelOptionsState={levelOptionsState}
                  apiCallInProgress={apiCallInProgress}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );

      default:
        return null;
    }
  }

  const clientsStep = GetCurrentStepComponent(clientState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      {clientsStep}
    </div>
  );
};

export default ClientsByUserReport;
