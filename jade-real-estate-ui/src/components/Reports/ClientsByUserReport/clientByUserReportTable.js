import React, { useState, useEffect } from "react";
import styled from "styled-components";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ReactLoading from "react-loading";
import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
  Row,
  Label,
  CardBody,
  CardFooter,
} from "reactstrap";
import moment from "moment";

import ReactExport from "react-data-export";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;


const Root = styled.div`
    height: 63vh;
    max-height: 63vh;
    overflow-y: auto;
    overflow-x: auto;
    `;


const ClientByUserReportTable = (props) => {
  const {
    fetchClientsReport,
    clients,
    setClients,
    users,
    userName,
    userId,
    levelOptionsState,
    apiCallInProgress
  } = props;
  const [initialDate, setInitialDate] = useState("");
  const [finalDate, setFinalDate] = useState("");
  const [userIdSelected, setUserIdSelected] = useState("");
  const [userSelected, setUserSelected] = useState("");
  const [mostrarBotoImpresionstate, setMostrarBotoImpresionstate] = useState(
    false
  );
  const [clientLevelSelectedState, setClientLevelSelectedState] = useState(0);

  const notify = (warningMessage) => {
    toast.warn(warningMessage, {
      position: toast.POSITION.BOTTOM_LEFT,
    });
  };

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  useEffect(() => {
    notify();
    definedUserSelected();
    ValidarAcceso();
  }, []);

  function ValidarAcceso() {
    var rol = sessionStorage.getItem("rolId");
    if (rol === "3") {
      setMostrarBotoImpresionstate(false);
    } else {
      setMostrarBotoImpresionstate(true);
    }
  }

  function definedUserSelected() {
    if (userName === undefined || userName === "" || userName === null) {
      setUserIdSelected(users || users[0].userId);
      setUserSelected(users || users[0].firtsName);
    } else {
      setUserIdSelected(userId);
      setUserSelected(userName);
    }
  }

  const handleChangeInitialDate = (date) => {
    setInitialDate(date);
    setClients([]);
  };
  const handleChangeFinalDate = (date) => {
    setFinalDate(date);
    setClients([]);
  };

  function findClients() {
    if (
      initialDate === undefined ||
      initialDate === null ||
      initialDate === ""
    ) {
      notify("Fecha inicial obligatorio");
      return;
    }
    if (finalDate === undefined || finalDate === null || finalDate === "") {
      toast("Fecha final obligatorio");
      return;
    }
    if (users.length === 1) {
    }

    if (
      userId !== undefined &&
      userId !== null &&
      userId !== "" &&
      userIdSelected.length === 0
    ) {
      fetchClientsReport(
        initialDate, 
        finalDate, 
        userId, 
        userSelected, 
        userId, 
        clientLevelSelectedState);
    } else {
      fetchClientsReport(
        initialDate,
        finalDate,
        userIdSelected,
        userSelected,
        userIdSelected,
        clientLevelSelectedState
      );
    }
  }

  const usersOptions = users.map((p) => {
    return (
      <option
        id={p.userId}
        key={p.userId}
        value={p.userId}
        name={p.firstName + " " + p.lastName}
      >
        {p.firstName + " " + p.lastName}
      </option>
    );
  });

  const nivelOptions = levelOptionsState.map((p) => {
    return (
      <option
        id={p.id}
        key={p.id}
        value={p.id}
        name={p.name}
      >
        {p.name}
      </option>
    );
  });

  const handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;

    setUserIdSelected(value);

    setUserSelected();
  };

  const handleInputChangeClientLevel = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;

    setClientLevelSelectedState(value);
  };




  function getAccessDownloadButton(e) {
    if (mostrarBotoImpresionstate) {
      return (
        <ExcelFile
          element={
            <Button type="button" color="success">
              <i className="fa fa-new" /> Descargar archivo
            </Button>
          }
        >
          <ExcelSheet data={clients} name="Clientes">
            <ExcelColumn label="Id" value="id" />
            <ExcelColumn label="Ingreso" value="creationDate" />
            <ExcelColumn label="Nivel Cliente" value="clientCategory" />
            <ExcelColumn label="Primer Nombre" value="firstName" />
            <ExcelColumn label="Segundo Nombre" value="middleName" />
            <ExcelColumn label="Primer Apellido" value="firstSurname" />
            <ExcelColumn label="Segundo Apellido" value="secondSurname" />
            <ExcelColumn label="Telefono" value="phoneNumber" />
            <ExcelColumn label="Celular" value="cellPhoneNumber" />
            <ExcelColumn label="Proyecto" value="project" />
            <ExcelColumn label="Oficial de Credito" value="creditOfficer" />
            <ExcelColumn label="Oficial de Ventas" value="salesAdvisor" />
            <ExcelColumn
              label="Institucion Financiera"
              value="financialInstitution"
            />
            <ExcelColumn label="Comentario" value="comments" />
          </ExcelSheet>
        </ExcelFile>
      );
    } else {
      return <Col></Col>;
    }
  }

  function getFooterBuild(clients) {
    if (apiCallInProgress) {
      return (
              <Row>
                 <Col md="3" sm="6" xs="12" />
              <Col>
               <div style={{ alignItems: 'center'}}> 
               <ReactLoading
                  type="bars"
                  color="#5DBCD2"
                  height={"45%"}
                  width={"45%"}
                  
                />
               </div>
               
              </Col>
              <Col md="3" sm="6" xs="12"/>
        </Row>
      );
    } else {
      return (
        <Table striped bordered class="table table-striped table-bordered table-sm">
              <thead>
                <tr>
                  <th nowrap="true" style={thStyle}>Id</th>
                  <th nowrap="true" style={thStyle}>Fecha de Creacion</th>
                  <th nowrap="true" style={thStyle}>Nivel Cliente</th>
                  <th nowrap="true" style={thStyle}>Proyecto</th>
                  <th nowrap="true" style={thStyle}>Primer Nombre</th>
                  <th nowrap="true" style={thStyle}>Segundo Nombre</th>
                  <th nowrap="true" style={thStyle}>Primer Apellido</th>
                  <th nowrap="true" style={thStyle}>Segundo Apellido</th>
                  <th nowrap="true" style={thStyle}>Celular</th>
                  <th nowrap="true" style={thStyle}>Telefono</th>              
                  <th nowrap="true" style={thStyle}>Institucion Financiera</th>
                  <th nowrap="true" style={thStyle}>Oficial de Credito</th>
                  <th nowrap="true" style={thStyle}>Asesor de Ventas</th>
                  <th nowrap="true" style={thStyle}>Comentarios</th>
                </tr>
              </thead>
              <tbody>
                {clients.map((client) => (
                  <tr key={client.id}>
                    <td nowrap="true">{client.id}</td>
                    <td nowrap="true">{moment(client.creationDate).format("L")}</td>
                    <td nowrap="true">{client.clientCategory}</td>
                    <td nowrap="true">{client.project}</td>
                    <td nowrap="true">{client.firstName}</td>
                    <td nowrap="true">{client.middleName}</td>
                    <td nowrap="true">{client.firstSurname}</td>
                    <td nowrap="true">{client.secondSurname}</td>
                    <td nowrap="true">{client.cellPhoneNumber}</td>
                    <td nowrap="true">{client.phoneNumber}</td>
                    <td nowrap="true">{client.financialInstitution}</td>
                    <td nowrap="true">{client.creditOfficer}</td>
                    <td nowrap="true">{client.salesAdvisor}</td>
                    <td nowrap="true">{client.comments}</td>
                  </tr>
                ))}
              </tbody>
             
            </Table>
       
      );
    }
  }

  const clientRows = (
    <div>
      <FormGroup row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Agentes</Label>
            <Input
              style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
              type="select"
              name="contactTypeId"
              value={userIdSelected}
              onChange={handleInputChange}
            >
              {usersOptions}
            </Input>
          </FormGroup>
        </Col>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Nivel cliente</Label>
            <Input
              style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
              type="select"
              name="clientLevelId"
              value={clientLevelSelectedState}
              onChange={handleInputChangeClientLevel}
            >
              {nivelOptions}
            </Input>
          </FormGroup>
        </Col> 
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Row>
              <Label htmlFor="Fecha"> Fecha Inicial</Label>
            </Row>
            <Row>
              <DatePicker
                selected={initialDate}
                onChange={handleChangeInitialDate}
              />
            </Row>
          </FormGroup>
        </Col>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Row>
              <Label htmlFor="Fecha"> Fecha Final</Label>
            </Row>
            <Row>
              <DatePicker
                selected={finalDate}
                onChange={handleChangeFinalDate}
              />
            </Row>
          </FormGroup>
        </Col>
        <Col md="3" sm="6" xs="12" >
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button type="button" color="primary" onClick={findClients}>
                Generar  Reporte  <i className="icon-arrow-right-circle" />
              </Button>
            </InputGroupAddon>
          </InputGroup>
        </Col>

        <Col  md="3" sm="6" xs="12">{getAccessDownloadButton()}</Col>
      </FormGroup>

      <FormGroup row>
        <Col md="12">
          <Root>
            
              {getFooterBuild(clients)}
             
          </Root>
        </Col>
      </FormGroup>
    </div>
  );
  return clientRows;
};

export default ClientByUserReportTable;
