import React, { Suspense,   } from "react";
import API from "./../../API/API";
import {
    CardBody,
    Col,
    FormGroup,
    Button,
    CardHeader,
    Card,
    Row,
    Label,
    Input,
    Table
 } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ClientNegotiationsRows from './clientNegotiationsRows';
import { ToastContainer } from "react-toastify";
import ReactLoading from "react-loading";
import "react-toastify/dist/ReactToastify.css";
import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ClientNegotiationsReportTable = (props) => {
    const {
        fetchClientNegotiationsReport,
        clientNegotiationState,
        initialDate,
        finalDate,
        salesAdvisorIdState,
        setSalesAdvisorIdState,
        financialInstitucionIdState,
        setFinancialInstitucionIdState,
        financialInstituionsState,
        salesAdvisorsState,
        apiCallInProgress,
        clients,
        setInitialDate,
        setFinalDate,
        setClients
    } = props;


    const financialInstitutionsOptions = financialInstituionsState.map(
        (p) => {
            return (
                <option id={p.id} key={p.id} value={p.id}>
                    {p.name}
                </option>
            );
        }
    );

    const salesAdvisorsOptions = salesAdvisorsState.map((p) => {
        return (
            <option id={p.id} key={p.id} value={p.id}>
                {p.name}
            </option>
        );
    });

    const handleNumericInputChange = (event) =>{
        const target = event.target;
        const value = target.value;
        const name = target.name;

        if(name === 'salesAdvisorIdState')
        {
            setSalesAdvisorIdState(parseInt(value, 10));
        }
        if(name === 'financialInstitucionIdState')
        {
            setFinancialInstitucionIdState(parseInt(value, 10));
        }
    }

    function findData() {
        fetchClientNegotiationsReport();
    }
    function cleanData() {
        setInitialDate('');
        setFinalDate('');
        setClients([]);
    }


    const thStyle = {
        background: "#20a8d8",
        color: "white",
        textAlign: "center",
    };

    const fallback = (
        <tr>
            <td colSpan={14}>Cargando....</td>
        </tr>
    );

    function getAccessDownloadButton(e) {
            return (
                <ExcelFile
                    element={
                        <Button type="button" color="success" >
                            <i className="fa fa-new" /> Descargar
                    </Button>
                     } filename="Reporte Negocios"
                >
                    <ExcelSheet data={clients} name="Reporte Negocios">
                        <ExcelColumn label="Id Cliente" value="idClient" />
                        <ExcelColumn label="Codigo Cliente" value="clientCode" />
                        <ExcelColumn label="Numero de Identidad" value="identificationCard" />
                        <ExcelColumn label="Cliente" value="clientName" />
                        <ExcelColumn label="Codigo negocio" value="idNegotiation" />
                        <ExcelColumn label="Inicio de negocio" value="negotiationStartDate" />
                        <ExcelColumn label="Agente de venta" value="salesAdvisor" />
                        <ExcelColumn label="Codigo banco" value="idBank" />
                        <ExcelColumn label="Banco" value="bank" />
                    </ExcelSheet>
                </ExcelFile>
            );
      
    }

    function obtenerTabla(e) {
        if (apiCallInProgress) {
            return (
                <FormGroup row>
                    <Col md="4" sm="6" xs="6"></Col>
                    <Col md="4" sm="6" xs="6">
                        <ReactLoading
                            type="bars"
                            color="#5DBCD2"
                            height={"30%"}
                            width={"30%"}
                        />
                    </Col>
                    <Col md="4" sm="6" xs="6"></Col>
                </FormGroup>
            );
        } else {
            return (
                <FormGroup row>
                    <Col md="12">
                        <Table hover bordered striped responsive size="sm">
                            <thead>
                                <tr>
                                    <th style={thStyle}>Id Cliente</th>
                                    <th style={thStyle}>Codigo Cliente</th>
                                    <th style={thStyle}>Numero de Identidad</th>
                                    <th style={thStyle}>Cliente</th>
                                    <th style={thStyle}>Codigo negocio</th>
                                    <th style={thStyle}>Inicio de negocio</th>
                                    <th style={thStyle}>Agente de venta</th>
                                    <th style={thStyle}>Codigo banco</th>
                                    <th style={thStyle}>Banco</th>
                                </tr>
                            </thead>  
                            <tbody>
                                <Suspense fallback={fallback}>
                                    <ClientNegotiationsRows
                                        items={clients}
                                    />
                                </Suspense>
                            </tbody>       
                        </Table>
                    </Col>
                        
                </FormGroup>
            );
        
    }}

    const handleChangeInitialDate = (date) => {
        setInitialDate(date);
    };
    const handleChangeFinalDate = (date) => {
        setFinalDate(date);
    };


    return (
        <div className="animated fadeIn" >
         <Row style={{ marginLeft: '3px' }}>
                <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Row >
                                <Label htmlFor="Fecha"> Fecha Inicial</Label>
                            </Row>
                            <Row >
                                <DatePicker
                                    selected={initialDate}
                                    onChange={handleChangeInitialDate}
                                />
                            </Row>
                        </FormGroup>
                    </Col>
                <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Row >
                                <Label htmlFor="Fecha"> Fecha Final</Label>
                            </Row>
                            <Row >
                                <DatePicker
                                    selected={finalDate}
                                    onChange={handleChangeFinalDate}
                                />
                            </Row>
                        </FormGroup>
                    </Col>
             
                <Col md="3" sm="6" xs="12" >
                        <FormGroup>
                            <Label>Institucion Financiera</Label>
                            <Input
                                type="select"
                                name="financialInstitucionIdState"
                                value={financialInstitucionIdState}
                                onChange={handleNumericInputChange}
                                required
                            >
                                {financialInstitutionsOptions}
                            </Input>
                        </FormGroup>
                    </Col>
                <Col md="3" sm="6" xs="12" >
                        <FormGroup>
                            <Label>Asesor de Ventas</Label>
                            <Input
                                type="select"
                                name="salesAdvisorIdState"
                                value={salesAdvisorIdState}
                                onChange={handleNumericInputChange}
                                required
                            >
                                {salesAdvisorsOptions}
                            </Input>
                        </FormGroup>
                    </Col>
            </Row>
                <Row>      
                <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Button type="button" color="primary" onClick={findData}>
                                    Generar <i className="icon-arrow-right-circle" />
                                </Button>
                        </FormGroup>
                    </Col>
                <Col md="3" sm="6" xs="12" >
                        <FormGroup>
                            <Button type="button" color="primary" onClick={cleanData}>
                            Limpiar <i className="icon-reload"></i>
                            </Button>
                        </FormGroup>
                    </Col>
                <Col md="3" sm="6" xs="12" >
                        <FormGroup>
                            {getAccessDownloadButton()}
                            {/* <Button type="button" color="primary" >
                                Exportar <i className="icon-arrow-right-circle" />
                            </Button> */}
                        </FormGroup>
                    </Col>
                    
                </Row>
                {obtenerTabla()}

            <ToastContainer autoClose={5000} />
        </div>
    );
}

export default ClientNegotiationsReportTable;