import React, { useState, useEffect } from "react";
import API from "./../../API/API";
import ClientNegotiationsReportTable from "./clientNegotiationsReportTable";
import { CardBody, Col, FormGroup } from "reactstrap";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import moment from "moment";

const heightStyle = {
  height: "500px",
};

const clientNegotiationInitialState = {
  step: 1,
  editMode: "None",
  currentRow: {
    id: 0,
    idClient: 0,
    clientCode: "",
    identificationCard: "",
    clientName: "",
    idNegotiation: 0,
    negotiationStartDate: "",
    salesAdvisor: "",
    idBank: 0,
    bank: "",
    status: "",
  },
  financialInstitutions: [],
  salesAdvisors: [],
};

const ClientsNegotiationsReport = (props) => {
  const [clientNegotiationState, setClientNegotiationState] = useState(
    clientNegotiationInitialState
  );
  const [clients, setClients] = useState([]);
  const [initialDate, setInitialDate] = useState("");
  const [finalDate, setFinalDate] = useState("");
  const [salesAdvisorIdState, setSalesAdvisorIdState] = useState(0);
  const [
    financialInstitucionIdState,
    setFinancialInstitucionIdState,
  ] = useState(0);
  const [financialInstituionsState, setFinancialInstituionsState] = useState(
    []
  );
  const [salesAdvisorsState, setSalesAdvisorsState] = useState([]);
  const [apiCallInProgress, setApiCallInProgress] = useState(false);

  useEffect(() => {
    fetchFinancialInstitutions();
    fetchSalesAdvisors();
  }, []);

  function compareNames(currentItem, nextItem) {
    const currentName = currentItem.name;
    const nextName = nextItem.name;

    let comparison = 0;
    if (currentName > nextName) {
      comparison = 1;
    } else if (currentName < nextName) {
      comparison = -1;
    }
    return comparison;
  }

  function fetchFinancialInstitutions() {
    const url = `financialInstitution`;

    API.get(url).then((res) => {
      const financialInstitutionsList = [
        { id: 0, name: "(Seleccione una Financiera)" },
      ].concat(res.data.sort(compareNames));

      setFinancialInstituionsState(financialInstitutionsList);
      setClientNegotiationState({
        ...clientNegotiationState,
        financialInstitutions: financialInstitutionsList,
      });
    });
  }

  function fetchSalesAdvisors() {
    const url = `user`;

    API.get(url).then((res) => {
      const salesAdvisorsList = [
        { id: 0, name: "(Seleccione un Asesor de  Ventas)" },
      ].concat(
        res.data
          .map((user) => {
            return {
              id: user.id,
              name: user.firstName + " " + user.lastName,
            };
          })
          .sort(compareNames)
      );

      setSalesAdvisorsState(salesAdvisorsList);
      setClientNegotiationState({
        ...clientNegotiationState,
        salesAdvisors: salesAdvisorsList,
      });
    });
  }

  function fetchClientNegotiationsReport() {
    setApiCallInProgress(true);
    const url = `ReportClientNegotiations?idBank=${financialInstitucionIdState}&salesAdvisor=${salesAdvisorIdState}&startDate=${
      initialDate === "" ? "" : moment(initialDate).format("L")
    }&endDate=${finalDate === "" ? "" : moment(finalDate).format("L")}`;

    API.get(url)
      .then((res) => {
        setApiCallInProgress(false);
        setClients(res.data);
      })
      .catch((error) => {
        setApiCallInProgress(false);
        if (error.message === "Network Error") {
          toast.warn(
            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
          );
        } else {
          toast.warn(
            "Se encontraron problemas para obtener datos favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
          );
        }
      });
  }

  const style = {
    height: "100%",
    maxwidth: "100%",
    maxheight: "100%",
    margin: "0",
    padding: "0",
    marginleft: "0px",
    marginright: "0px",
    paddingright: "0px",
    paddingleft: "0px",
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody style={style}>
            <FormGroup row>
              <Col />
            </FormGroup>

            <FormGroup row>
              <Col xs="12">
                <ClientNegotiationsReportTable
                  fetchClientNegotiationsReport={fetchClientNegotiationsReport}
                  clientNegotiationState={clientNegotiationState}
                  initialDate={initialDate}
                  finalDate={finalDate}
                  salesAdvisorIdState={salesAdvisorIdState}
                  setSalesAdvisorIdState={setSalesAdvisorIdState}
                  financialInstitucionIdState={financialInstitucionIdState}
                  setFinancialInstitucionIdState={
                    setFinancialInstitucionIdState
                  }
                  financialInstituionsState={financialInstituionsState}
                  salesAdvisorsState={salesAdvisorsState}
                  apiCallInProgress={apiCallInProgress}
                  clients={clients}
                  setInitialDate={setInitialDate}
                  setFinalDate={setFinalDate}
                  setClients={setClients}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );
      default:
        return null;
    }
  }

  const clientsStep = GetCurrentStepComponent(clientNegotiationState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      {clientsStep}
    </div>
  );
};

export default ClientsNegotiationsReport;
