import React from "react";
import moment from "moment";

const ClientTransactionTableRows = ({ items }) => {
    const rows =
        items === null || items === undefined ? (
            <tr>
                <td colSpan={19}>Buscando datos...</td>
            </tr>
        ) : items.length === 0 ? (
            <tr>
                <td colSpan={19}>
                    No se encontraron datos con el filtro
                    especificado.
        </td>
            </tr>
        ) : (
                    items.map((item) => (
                        <tr key={item.uId}>
                            <td nowrap="true">{item.idClient}</td>
                            <td nowrap="true">{item.clientCode}</td>
                            <td nowrap="true">{item.identificationCard}</td>
                            <td nowrap="true">{item.clientName}</td>
                            <td nowrap="true">{item.idNegotiation}</td>
                            <td nowrap="true">{moment(item.negotiationStartDate).format("L")}</td>
                            <td nowrap="true">{item.salesAdvisor}</td>
                            <td nowrap="true">{item.idBank}</td>
                            <td nowrap="true">{item.bank}</td>
                        </tr>
                    ))
                );

    return rows;
};

export default ClientTransactionTableRows;