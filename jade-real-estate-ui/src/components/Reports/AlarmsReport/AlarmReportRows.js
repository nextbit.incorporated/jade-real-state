import React from "react";
import moment from "moment";

const AlarmReportRows = ({ items }) => {
    const rows =
        items === null || items === undefined ? (
            <tr>
                <td colSpan={19}>Buscando datos...</td>
            </tr>
        ) : items.length === 0 ? (
            <tr>
                <td colSpan={19}>
                    No se encontraron datos con el filtro
                    especificado.
        </td>
            </tr>
        ) : (
                    items.map((item) => (
                        <tr key={item.uId}>
                            <td nowrap="true">{item.clientId}</td>
                            <td nowrap="true">{item.clientCode}</td>
                            <td nowrap="true">{item.clientName}</td>
                            <td nowrap="true">{item.alarmNotification}</td>
                            <td nowrap="true">{moment(item.date).format("L")}</td>
                            <td nowrap="true">{moment(item.fechaCumplimiento).format("L")}</td>
                            <td nowrap="true">{item.userId}</td>
                        </tr>
                    ))
                );

    return rows;
}

export default AlarmReportRows;