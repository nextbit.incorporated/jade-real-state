import React, { useState, useEffect } from "react";
import API from "./../../API/API";
import AlarmReportTable from "./AlarmReportTable";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { CardBody, Col, FormGroup } from "reactstrap";


const heightStyle = {
    height: "500px",
};

const clientAlarmInitialState = {
    step: 1,
    editMode: "None",
    currentRow: {
        clientId: 0,				
        clientCode: "",				
        clientName: "",				
        alarmNotification: "",		
        date: "",					
        numberMonths: "",			
        userId: "",					
        fechaCumplimiento: "",		
        id: 0,					
        creationDate: "",		
        status: "",					
        user: "",					
        validationErrorMessage: "",	
},
    financialInstitutions: [],
    salesAdvisors: [],
};
const AlarmReport = (props) => {
    const [clientAlarmState, setClientAlarmState] = useState(clientAlarmInitialState);
    const [alarmReportDataState, setAlarmReportDataState] = useState([]);
    const [apiCallInProgress, setApiCallInProgress] = useState(false);

    useEffect(() => {
        // fetchAlarmReport();
    }, []);

    function CleanData() {
        setAlarmReportDataState([]);
    }

    function fetchAlarmReport() {
        const url = `ClientAlarm`;

        API.get(url)
            .then((res) => {
                setApiCallInProgress(false);
                setAlarmReportDataState(res.data);
            })
            .catch((error) => {
                setApiCallInProgress(false);
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    toast.warn(
                        "Se encontraron problemas para obtener datos favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                    );
                }
            });
    }

    const style = {
        height: "100%",
        maxwidth: "100%",
        maxheight: "100%",
        margin: "0",
        padding: "0",
        marginleft: "0px",
        marginright: "0px",
        paddingright: "0px",
        paddingleft: "0px",
    };


    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody style={style}>
                        <FormGroup row>
                            <Col />
                        </FormGroup>

                        <FormGroup row>
                            <Col xs="12">
                                <AlarmReportTable
                                    fetchAlarmReport={fetchAlarmReport}
                                    alarmReportDataState={alarmReportDataState}
                                    apiCallInProgress={apiCallInProgress}
                                    CleanData={CleanData}
                                />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            default:
                return null;
        }
    }

    const clientsStep = GetCurrentStepComponent(clientAlarmState.step);


    return (
        <div className="animated fadeIn" style={heightStyle}>
            {clientsStep}
        </div>
    );
}

export default AlarmReport;