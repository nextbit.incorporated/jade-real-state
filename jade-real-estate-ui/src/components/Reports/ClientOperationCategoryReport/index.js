import React, { useState, useEffect } from "react";

import {
    Col,
    FormGroup,
    InputGroup,
    InputGroupAddon,
    Button,
    Input,
    Row,
    Label,
    CardBody,
    Card,
    CardHeader,
    Table,
    Collapse,
    Badge,
    Modal,
    ModalHeader
  } from "reactstrap";
  import styled from "styled-components";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { toast, ToastContainer } from "react-toastify";
import moment from "moment";
import API from "./../../API/API";
import { Redirect } from "react-router-dom";
import 'moment/locale/es'
import ReactTable from "react-table";



const heightStyle = {
    height: "500px",
};

const tableStyle = {
    height: "10vh",
    maxHeight: "50vh",
  };

  
  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

const initialOperationState = {
    step: 1,
    editMode: "None",
    startDate: Date.now(),  
    currentOperation: {
        clientlevel: "",
        clientCounts: 0,
        children: [
      {
        category: "Nivel D",
        clientCode: "000210201437",
        perfil: "",
        registeredDate: "2021-02-02T17:05:59.284232",
        clientName: "Prueba Cliente Prueba Prueba",
        coDeudorName: "",
        financialInstitution: "Banco Popular",
        officialName: "Prueba",
        negotiationPhase: "",
        alarm: "",
        observacionesSeguimiento: "",
        objecciones: "",
        projectName: "",
        children: [
          {
            id: 0,
            clientId: 1437,
            operationName: "Cambiar Categoria",
            operationType: "Cambiar Categoria",
            oldValue: "Nivel C",
            newValue: "Nivel D",
            selectedOptions: [],
            comments: "prueba",
            error: "",
            clientName: "Prueba Cliente Prueba Prueba",
            modifiedBy: "Service",
            optionsOperation: "No es posible contactarlo.,No le interesan las opciones de vivienda.",
            fechaTransaccion: Date.now()
          },
        ],
    },
  ],
}
};

const arregloCollapse = [false,false,false,false,false];


const ClientOperationCategoryReport =(props) =>{
    const [mainPropertyState, setMainPropertyState] = useState(initialOperationState);
    const [apiCallInProgress, setApiCallInProgress] = useState(false);
    const [initialDate, setInitialDate] = useState("");
    const [finalDate, setFinalDate] = useState("");
    const [operationsState, setOperationsState] = useState([]);
    const [openAccordionState, setopenAccordionState] = useState(false)
    const [stateCollapseArray, setStateCollapseArray] = useState(arregloCollapse);
    const [ContadorCollapsedState, setContadorCollapsedState] = useState(0);
    const [modarlState, setModarlState] = useState(false)
    const [gestinesState, setGestinesState] = useState([]);
    const [userIdSelected, setUserIdSelected] = useState("");
  const [userSelected, setUserSelected] = useState("");
  const [users, setUsers] = useState([]);
  const [userId, setUserId] = useState("");
  const [userName, setUserName] = useState("");
    var contador = 0;

    useEffect(() => {
      fetchUsersReport();
        },[]);
    
        const handleChangeInitialDate = (date) => {
            setInitialDate(date);
          };
          const handleChangeFinalDate = (date) => {
            setFinalDate(date);
          };

          function findClients() {
       

            const user = sessionStorage.getItem("userId");
            if (initialDate === undefined || initialDate === null) return;
            if (finalDate === undefined || finalDate === null) return;
            const stDate = moment(initialDate).format("MM/DD/YYYY");
            const endDate = moment(finalDate).format('MM/DD/YYYY');  


            debugger;
            var usuario = userIdSelected ? userIdSelected : "Todos";
  
            var url = `operations`;

               if (
                   initialDate === undefined ||
                   initialDate === null ||
                   initialDate === ""
                 ) {
                   toast.warn("Fecha inicial obligatorio");
                   return;
                 }
                 if (finalDate === undefined || finalDate === null || finalDate === "") {
                   toast.warn("Fecha final obligatorio");
                   return;
                 }
                url = `operations/gestiones?&User=${user}&InitialDate=${stDate}&FinalDate=${endDate}&TypeReport=detail&SaleAdvisor=${usuario}`
  
            API.get(url)
            .then((res) => {
                setOperationsState(res.data);
            })
            .catch((error) => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
  
  
        };

        function CleanDataReport(){
            setOperationsState([]);
        };


    
      function  toggleAccordion() {
        setopenAccordionState(!openAccordionState)

    }

      function  obtenerEstadoCollapsed(contador) {
        var r = stateCollapseArray[contador]
        return r;
      }


      const showOperations = (item) => {
        setGestinesState(item.children)
        setModarlState(true);
      };

      const togglePrimary = () => {
       setModarlState(!modarlState);
      };
  

      const handleInputChange = (event) => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
    
        setUserIdSelected(value);
    
        setUserSelected();
      };

      function fetchUsersReport() {
        const user = sessionStorage.getItem("userId");
        const url = `clients/users-report?request.user=${user}`;
    
        API.get(url).then((res) => {
          if (res.data === null || res.data === undefined) {
            return;
          }
          setUsers(res.data);
          if (res.data.length === 1) {
            setUserId(res.data[0].userId);
            setUserName(res.data[0].firstName);
          }
        });
      }

      const usersOptions = users.map((p) => {
        return (
          <option
            id={p.userId}
            key={p.userId}
            value={p.userId}
            name={p.firstName + " " + p.lastName}
          >
            {p.firstName + " " + p.lastName}
          </option>
        );
      });
    

    const Root = styled.div`
    height: 30vh;
    max-height: 30vh;
    overflow-y: auto;
    overflow-x: auto;
  `;

      function  ObtenerDetalle(children) {
       
       return(
        <FormGroup row>
        <Col xs="12">
        <Root>
            <Table
            style={tableStyle}
            hover
            bordered
            striped
            size="sm"
        > 
           <thead>
                <tr>
                <th nowrap="true" style={thStyle}>
                    #
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Id
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Perfil Cliente
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Fecha registro
                  </th> 
                  <th nowrap="true" style={thStyle}>
                    Categoria
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Nombre Cliente
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Nombre Co-Deudor
                  </th>   
                  <th nowrap="true" style={thStyle}>
                    Proyecto
                  </th>                        
                  <th nowrap="true" style={thStyle}>
                    Banco
                  </th>             
                  <th nowrap="true" style={thStyle}>
                    Oficial
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Fase de negociacion
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Alarmas
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Objeciones
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Gestiones
                  </th>
                </tr>
                </thead>
                <tbody>
                    {children.map((item) => (
                         <tr key={item.id}>
                            <td nowrap="true">{item.numero}</td>
                           <td nowrap="true">{item.clientId}</td>
                           <td nowrap="true">{item.perfil}</td>
                           <td nowrap="true">{ moment(item.registeredDate).format("L")}</td>
                             <td nowrap="true">{item.category}</td>
                             <td nowrap="true">{item.clientName}</td>  
                             <td nowrap="true">{item.coDebName}</td>                      
                             <td nowrap="true">{item.projectName}</td>
                             <td nowrap="true">{item.financialInstitution}</td>
                             <td nowrap="true">{item.officialName}</td>
                             <td nowrap="true">{item.negotiationPhase}</td>
                             <td nowrap="true">{item.alarm === null ? "" : moment(item.alarm).format("L")}</td>
                             <td nowrap="true">{item.objecciones}</td>
                             <td nowrap="true">
                                <Button
                                    block
                                    color="success"
                                    onClick={() => {
                                        showOperations(item);
                                      }}
                                >
                                    Ver Gestiones
                                </Button>
                                </td>
                         </tr>
                    ))}
                </tbody>
              
        </Table>
        </Root>
        </Col>
        </FormGroup>
       )
      }
        

    return(
        <div className="animated fadeIn" style={heightStyle}>
         <FormGroup row>
         <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Label>Agentes</Label>
            <Input
              style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
              type="select"
              name="contactTypeId"
              value={userIdSelected}
              onChange={handleInputChange}
            >
              {usersOptions}
            </Input>
          </FormGroup>
        </Col>
          <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Row>
              <Label htmlFor="Fecha"> Fecha Inicial</Label>
            </Row>
            <Row>
              <DatePicker
                selected={initialDate}
                onChange={handleChangeInitialDate}
              />
            </Row>
          </FormGroup>
        </Col>
        <Col md="2" sm="6" xs="12">
          <FormGroup>
            <Row>
              <Label htmlFor="Fecha"> Fecha Final</Label>
            </Row>
            <Row>
              <DatePicker
                selected={finalDate}
                onChange={handleChangeFinalDate}
              />
            </Row>
          </FormGroup>
        </Col>
            <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button type="button" color="primary" onClick={findClients}>
                Generar <i className="icon-arrow-right-circle" />
              </Button>
            </InputGroupAddon>
          </InputGroup>
        </Col>

        <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button type="button" color="primary" onClick={CleanDataReport}>
                Limpiar <i className="icon-arrow-right-circle" />
              </Button>
            </InputGroupAddon>
          </InputGroup>
        </Col>
        </FormGroup>
        
        <FormGroup row>
            <Col md="12">
      
        <Card>
        <CardHeader>
            <i className="fa fa-align-justify"></i> Detalle <small>Categoria clientes </small>
            <div className="card-header-actions">
            </div>
        </CardHeader>
        <CardBody>
        {operationsState.map((category) => (   
           <div id="accordion">
            

           <Card  >
             <CardHeader>
               <Button block color="link" className="text-left m-0 p-0" onClick={() => toggleAccordion()} aria-expanded={openAccordionState} aria-controls="collapseOne">
                 <h5 className="m-0 p-0"> Categoria : {category.clientlevel} - Cantidad Clientes: {category.clientCounts}</h5>
               </Button>
             </CardHeader>
             <Collapse isOpen={openAccordionState} data-parent="#accordion" id="collapseOne" aria-labelledby="headingOne">
               <CardBody>
                  {ObtenerDetalle(category.children)}
               </CardBody>
             </Collapse>
           </Card>

          </div>
         ))
        }
        </CardBody>
        </Card>
        </Col>
        </FormGroup>
        <Modal
            isOpen={modarlState}
            toggle={togglePrimary}
            className={"modal-lg"}
            size="xl"
            fullscreen={true}
            
          >
               <ModalHeader toggle={togglePrimary}>
               <strong>Detalle Gestiones</strong>
                </ModalHeader>
               <div className="animated fadeIn" style={heightStyle}>
                   <Card>
                       {/* <CardHeader>
                           <strong>Detalle Gestiones</strong>
                       </CardHeader> */}
                       <CardBody>

                      
                
                 <FormGroup row>
                    <Col md="12">
                        <Table
                            style={tableStyle}
                            hover
                            bordered
                            striped
                            responsive
                            size="sm">
                            <thead>
                                <tr>
                                <th nowrap="true" style={thStyle}>
                                    Id
                                </th>
                                <th nowrap="true" style={thStyle}>
                                    Cliente Id
                                </th>
                                <th nowrap="true" style={thStyle}>
                                    Nombre Cliente
                                </th>
                                <th nowrap="true" style={thStyle}>
                                    Nombre Operacion 
                                </th>
                                <th nowrap="true" style={thStyle}>
                                    Tipo operacion
                                </th>
                                <th nowrap="true" style={thStyle}>
                                    Valor anterior
                                </th>            
                                <th nowrap="true" style={thStyle}>
                                    Nuevo valor
                                </th>
                                <th nowrap="true" style={thStyle}>
                                    Comentario
                                </th>
                                <th nowrap="true" style={thStyle}>
                                    Modificado por
                                </th>
                                <th nowrap="true" style={thStyle}>
                                    Opciones de gestion
                                </th>
                                <th nowrap="true" style={thStyle}>
                                    fecha transaccion
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                    {gestinesState.map((gestion) => (
                                          <tr key={gestion.id}>
                                                <td nowrap="true">{gestion.id}</td>
                                                <td nowrap="true">{gestion.clientId}</td>
                                                <td nowrap="true">{gestion.clientName}</td>
                                                <td nowrap="true">{gestion.operationName}</td>
                                                <td nowrap="true">{gestion.operationType}</td>
                                                <td nowrap="true">{gestion.oldValue}</td>
                                                <td nowrap="true">{gestion.newValue}</td>
                                                <td nowrap="true">{gestion.comments}</td>
                                                <td nowrap="true">{gestion.modifiedBy}</td>
                                                <td nowrap="true">{gestion.optionsOperation}</td>
                                                <td nowrap="true">{moment(gestion.fechaTransaccion).format("L")}</td>
                                          </tr>
                                    ))}
                            </tbody>
                        </Table>
                    </Col>
               </FormGroup>
               </CardBody>
               </Card>
               </div>

          </Modal>
        </div>)
}

export default ClientOperationCategoryReport;