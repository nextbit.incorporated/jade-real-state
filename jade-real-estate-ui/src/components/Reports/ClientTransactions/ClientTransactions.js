import React, { useState, useEffect } from "react";
import { CardBody, Col, FormGroup } from "reactstrap";
import API from "./../../API/API";
import { ToastContainer, toast } from "react-toastify";

import ClientTransactionsTable from "./ClientTransactionsTable";
import moment from "moment";

const heightStyle = {
  height: "500px",
};

const style = {
  height: "100%",
  maxwidth: "100%",
  maxheight: "100%",
  margin: "0",
  padding: "0",
  marginleft: "0px",
  marginright: "0px",
  paddingright: "0px",
  paddingleft: "0px",
};

const initialClientTransactionsPageResult = {
  pageIndex: 0,
  pageSize: 50,
  pageCount: 0,
  totalItems: 0,
  searchValue: "",
  salesAdvisorId: 0,
  items: [],
};

const initialClientTransactionsQuery = {
  pageIndex: 0,
  pageSize: 50,
  searchValue: "",
  salesAdvisorId: 0,
};

const ClientTransactions = () => {
  const [salesAdvisors, setSalesAdvisors] = useState([]);
  const [clientTransactionsQuery, setClientTransactionsQuery] = useState(
    initialClientTransactionsQuery
  );
  const [
    clientTransactionsPageResult,
    setClientTransactionsPageResult,
  ] = useState(initialClientTransactionsPageResult);

  const [apiCallInProgress, setApiCallInProgress] = useState(false);

  useEffect(() => {
    API.get("user").then((res) => {
      const salesAdvisors = [
        { id: 0, name: "(Seleccione un Asesor de  Ventas)" },
      ].concat(
        res.data
          .map((user) => {
            return {
              id: user.id,
              name: user.firstName + " " + user.lastName,
            };
          })
          .sort(compareNames)
      );

      setSalesAdvisors(salesAdvisors);
    });
  }, []);

  useEffect(() => {
    setApiCallInProgress(true);

    const requestParams = {
      params: {
        ...clientTransactionsQuery,
      },
    };

    API.get(`client-transactions`, requestParams)
      .then((res) => {
        setClientTransactionsPageResult(res.data);
        setApiCallInProgress(false);
      })
      .catch((error) => {
        setApiCallInProgress(false);

        toast.warn("Se encontraron problermas para obtener la información.");
      });
  }, [clientTransactionsQuery]);

  const searchClienttransactions = (searchCriteria) => {
    const searchQuery = {
      ...initialClientTransactionsQuery,
      pageIndex: searchCriteria.pageIndex,
      pageSize: 50,
      searchValue: searchCriteria.searchValue,
      salesAdvisorId: searchCriteria.salesAdvisorId,
    };

    setClientTransactionsQuery(searchQuery);
  };

  function compareNames(currentItem, nextItem) {
    const currentName = currentItem.name;
    const nextName = nextItem.name;

    let comparison = 0;
    if (currentName > nextName) {
      comparison = 1;
    } else if (currentName < nextName) {
      comparison = -1;
    }
    return comparison;
  }

  return (
    <div className="animated fadeIn" style={heightStyle}>
      <CardBody style={style}>
        <FormGroup row>
          <Col xs="12">
            <ClientTransactionsTable
              salesAdvisors={salesAdvisors}
              clientTransactionsPageResult={clientTransactionsPageResult}
              searchClienttransactions={searchClienttransactions}
              apiCallInProgress={apiCallInProgress}
            />
          </Col>
        </FormGroup>
      </CardBody>
      <ToastContainer />
    </div>
  );
};

export default ClientTransactions;
