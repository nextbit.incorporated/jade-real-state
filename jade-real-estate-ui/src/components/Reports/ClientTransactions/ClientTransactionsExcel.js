import React from "react";
import { Button } from "reactstrap";

import ReactExport from "react-data-export";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ClientTransactionsExcel = ({ items }) => {
  return (
    <ExcelFile
      element={
        <Button type="button" color="success">
          <i className="fa fa-new" /> Descargar Reporte
        </Button>
      }
    >
      <ExcelSheet data={items} name="Modificaciones de Clientes">
        <ExcelColumn label="Id" value="id" />
        <ExcelColumn
          label="Motivo creacion cliente similar"
          value="clientCreationComments"
        />
        <ExcelColumn label="Modificado por" value="modifiedBy" />
        <ExcelColumn label="Ingreso" value="creationDate" />
        <ExcelColumn label="Primer Nombre" value="firstName" />
        <ExcelColumn label="Segundo Nombre" value="middleName" />
        <ExcelColumn label="Primer Apellido" value="firstSurname" />
        <ExcelColumn label="Segundo Apellido" value="secondSurname" />
        <ExcelColumn label="Identidad" value="identificationCard" />
        <ExcelColumn label="Nacinalidad" value="nationality" />
        <ExcelColumn label="Telefono" value="phoneNumber" />
        <ExcelColumn label="Celular" value="cellPhoneNumber" />
        <ExcelColumn label="Correo" value="email" />
        <ExcelColumn label="Lugar de trabajo" value="workplace" />
        <ExcelColumn label="Fecha de inicio de Labores" value="workStartDate" />
        <ExcelColumn label="Moneda" value="debtorCurrency" />
        <ExcelColumn label="Ingreso Mensual" value="grossMonthlyIncome" />
        <ExcelColumn label="Proyecto" value="project" />
        <ExcelColumn label="Domicilio" value="homeAddress" />
        <ExcelColumn
          label="Posee Vivienda"
          value={(col) => (col.ownHome ? "Si" : "No")}
        />

        <ExcelColumn
          label="Primer Vivienda"
          value={(col) => (col.firstHome ? "Si" : "No")}
        />

        <ExcelColumn
          label="Contribuye al RAP"
          value={(col) => (col.contributeToRap ? "Si" : "No")}
        />
        <ExcelColumn
          label="Negocio Finalizado"
          value={(col) => (col.negotiationEnded ? "Si" : "No")}
        />

        <ExcelColumn label="Comentario" value="comments" />
        <ExcelColumn label="Oficial de Credito" value="creditOfficer" />
        <ExcelColumn label="Oficial de Ventas" value="salesAdvisor" />
        <ExcelColumn
          label="Institucion Financiera"
          value="financialInstitution"
        />
        <ExcelColumn label="Tipo de Contacto" value="contactType" />
      </ExcelSheet>
    </ExcelFile>
  );
};

export default ClientTransactionsExcel;
