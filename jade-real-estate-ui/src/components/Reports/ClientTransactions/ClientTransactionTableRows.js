import React from "react";
import moment from "moment";

const ClientTransactionTableRows = ({ items }) => {
  const rows =
    items === null || items === undefined ? (
      <table>
        <tbody>
          <tr>
            <td colSpan={19}>Buscando transacciones de clientes...</td>
          </tr>
        </tbody>
      </table>
    ) : items.length === 0 ? (
      <table>
        <tbody>
          <tr>
            <td colSpan={19}>
              No se encontraron transacciones de clientes con el filtro
              especificado.
            </td>
          </tr>
        </tbody>
      </table>
    ) : (
      items.map((item) => (
        <tr key={item.uId}>
          <td nowrap="true">{item.id}</td>
          <td nowrap="true">{item.clientCreationComments}</td>
          <td nowrap="true">{item.modifiedBy}</td>
          <td nowrap="true">{item.clientCode}</td>
          <td nowrap="true">{moment(item.creationDate).format("L")}</td>
          <td nowrap="true">{moment(item.transactionDate).format("L")}</td>
          <td nowrap="true">{item.firstName}</td>
          <td nowrap="true">{item.middleName}</td>
          <td nowrap="true">{item.firstSurname}</td>
          <td nowrap="true">{item.secondSurname}</td>
          <td style={{ whiteSpace: "nowrap" }}>{item.identificationCard}</td>
          <td nowrap="true">{item.nationality}</td>
          <td style={{ whiteSpace: "nowrap" }}>{item.cellPhoneNumber}</td>
          <td style={{ whiteSpace: "nowrap" }}>{item.phoneNumber}</td>
          <td nowrap="true">{item.email}</td>
          <td style={{ whiteSpace: "nowrap" }}>{item.workplace}</td>
          <td nowrap="true">{moment(item.workStartDate).format("L")}</td>
          <td nowrap="true">{item.debtorCurrency}</td>
          <td nowrap="true">{item.grossMonthlyIncome}</td>
          <td nowrap="true">{item.clientCategory}</td>
          <td style={{ whiteSpace: "nowrap" }}>{item.project}</td>
          <td style={{ whiteSpace: "nowrap" }}>{item.contactType}</td>

          <td style={{ whiteSpace: "nowrap" }}>{item.homeAddress}</td>
          <td nowrap="true">{item.ownHome ? "Si" : ""}</td>
          <td nowrap="true">{item.contributeToRap ? "Si" : ""}</td>
          <td style={{ whiteSpace: "nowrap" }}>{item.comments}</td>
          <td nowrap="true">{item.financialInstitution}</td>
          <td style={{ whiteSpace: "nowrap" }}>{item.creditOfficer}</td>
          <td style={{ whiteSpace: "nowrap" }}>{item.salesAdvisor}</td>
          <td nowrap="true">{item.negotiationEnded ? "Si" : ""}</td>
        </tr>
      ))
    );

  return rows;
};

export default ClientTransactionTableRows;
