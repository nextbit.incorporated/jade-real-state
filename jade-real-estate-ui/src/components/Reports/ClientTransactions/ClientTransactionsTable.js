import React, { Suspense, useState, useEffect } from "react";
import styled from "styled-components";
import {
  Card,
  CardHeader,
  CardBody,
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Input,
} from "reactstrap";
import ReactLoading from "react-loading";
import "react-toastify/dist/ReactToastify.css";
import ClientTransactionTableRows from "./ClientTransactionTableRows";
import "react-toastify/dist/ReactToastify.css";

import ClientTransactionsExcel from "./ClientTransactionsExcel";

const Root = styled.div`
  height: 53vh;
  max-height: 53vh;
  overflow-y: auto;
  overflow-x: auto;
`;

const thStyle = {
  background: "#20a8d8",
  color: "white",
  textAlign: "center",
};

const fallback = (
  <tr>
    <td colSpan={14}>Cargando....</td>
  </tr>
);

const ClientTransactionsTable = ({
  salesAdvisors,
  clientTransactionsPageResult,
  searchClienttransactions,
  apiCallInProgress,
}) => {
  const [salesAdvisorId, setSalesAdvisorId] = useState(0);
  const [searchValue, setSearchValue] = useState("");

  useEffect(() => {}, []);

  function handleOnClickSearchButton(e) {
    const query = {
      pageIndex: clientTransactionsPageResult.pageIndex,
      searchValue: searchValue,
      salesAdvisorId: salesAdvisorId,
    };

    searchClienttransactions(query);
  }

  const handleNumericInputChange = (event) => {
    const target = event.target;

    setSalesAdvisorId(parseInt(target.value, 10));
  };

  const handleInputChange = (event) => {
    const target = event.target;

    setSearchValue(target.value);
  };

  function handleValueHeyPress(e) {
    if (e.key === "Enter") {
      const value = e.target.value;

      const query = {
        pageIndex: 0,
        searchValue: value,
        salesAdvisorId: salesAdvisorId,
      };

      searchClienttransactions(query);
    }
  }

  const pageClientTransactions = (movePosition) => {
    const query = {
      pageIndex: clientTransactionsPageResult.pageIndex + movePosition,
      searchValue: searchValue,
      salesAdvisorId: salesAdvisorId,
    };

    searchClienttransactions(query);
  };

  function obtenerTabla(e) {
    if (apiCallInProgress) {
      return (
        <FormGroup row>
          <Col md="4" sm="6" xs="6"></Col>
          <Col md="4" sm="6" xs="6">
            <ReactLoading
              type="bars"
              color="#5DBCD2"
              height={"30%"}
              width={"30%"}
            />
          </Col>
          <Col md="4" sm="6" xs="6"></Col>
        </FormGroup>
      );
    } else {
      return (
        <FormGroup row>
          <Col md="12">
            <Root>
              <Table hover bordered striped responsive size="sm">
                <thead>
                  <tr>
                    <th style={thStyle}>Id</th>
                    <th style={thStyle}>Motivo creacion cliente similar</th>
                    <th style={thStyle}>Modificado Por</th>
                    <th style={thStyle}>Codigo</th>
                    <th style={thStyle}>Fecha de Creacion</th>
                    <th style={thStyle}>Fecha de Ingreso</th>
                    <th style={thStyle}>Primer Nombre</th>
                    <th style={thStyle}>Segundo Nombre</th>
                    <th style={thStyle}>Primer Apellido</th>
                    <th style={thStyle}>Segundo Apellido</th>
                    <th style={thStyle}>Cedula</th>
                    <th style={thStyle}>Nacionalidad</th>
                    <th style={thStyle}>Celular</th>
                    <th style={thStyle}>Telefono</th>
                    <th style={thStyle}>Email</th>
                    <th style={thStyle}>Lugar de trabajo</th>
                    <th style={thStyle}>Trabaja desde</th>
                    <th style={thStyle}>Modeda Deudor</th>
                    <th style={thStyle}>Ingreso Bruto Mensual</th>
                    <th style={thStyle}>Categoria</th>
                    <th style={thStyle}>Proyecto</th>
                    <th style={thStyle}>Tipo de contacto</th>
                    <th style={thStyle}>Lugar de residencia</th>
                    <th style={thStyle}>Posee casa</th>
                    <th style={thStyle}>Aporta al RAP</th>
                    <th style={thStyle}>Comentarios</th>
                    <th style={thStyle}>Institucion Financiera</th>
                    <th style={thStyle}>Oficial de Credito</th>
                    <th style={thStyle}>Asesor de Ventas</th>
                    <th style={thStyle}>Negocio Finalizado</th>
                  </tr>
                </thead>
                <tbody>
                  <Suspense fallback={fallback}>
                    <ClientTransactionTableRows
                      items={clientTransactionsPageResult.items}
                    />
                  </Suspense>
                </tbody>
              </Table>
            </Root>
          </Col>
          <Col md="12">
            <InputGroup>
              <InputGroupAddon addonType="prepend">
                <Button
                  type="button"
                  color="primary"
                  onClick={(e) => pageClientTransactions(-1)}
                >
                  <i className="icon-arrow-left-circle" /> Anterior
                </Button>
              </InputGroupAddon>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>
                    Paginas: {clientTransactionsPageResult.pageCount}
                  </strong>
                </h5>
              </div>
              <div
                className="animated fadeIn"
                style={{
                  marginTop: "7px",
                  marginLeft: "8px",
                  marginRight: "8px",
                }}
              >
                <h5>
                  <strong>
                    Actual: {clientTransactionsPageResult.pageIndex}
                  </strong>
                </h5>
              </div>
              <InputGroupAddon addonType="prepend">
                <Button
                  type="button"
                  color="primary"
                  onClick={(e) => pageClientTransactions(1)}
                >
                  Proximo <i className="icon-arrow-right-circle" />
                </Button>
              </InputGroupAddon>
            </InputGroup>
          </Col>
        </FormGroup>
      );
    }
  }

  const salesAdvisorsOptions = salesAdvisors.map((p) => {
    return (
      <option id={p.id} key={p.id} value={p.id}>
        {p.name}
      </option>
    );
  });

  return (
    <div>
      <Card>
        <CardHeader>
          <i className="fa fa-align-justify" /> Modificaciones de Clientes
        </CardHeader>
        <CardBody>
          <FormGroup row>
            <Col md="3" sm="6" xs="12">
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <Button type="button" color="primary">
                    <i className="fa fa-search" /> Buscar Por:
                  </Button>
                </InputGroupAddon>

                <Input
                  type="select"
                  name="salesAdvisorId"
                  value={salesAdvisorId}
                  onChange={handleNumericInputChange}
                  required
                >
                  {salesAdvisorsOptions}
                </Input>
              </InputGroup>
            </Col>
            <Col md="3" sm="6" xs="12">
              <InputGroup>
                <Input
                  type="text"
                  id="input1-group2"
                  name="input1-group2"
                  placeholder="Buscar"
                  value={searchValue}
                  onChange={handleInputChange}
                  onKeyPress={(e) => handleValueHeyPress(e)}
                />
                <InputGroupAddon addonType="prepend">
                  <Button
                    type="button"
                    color="primary"
                    onClick={(e) => handleOnClickSearchButton(e)}
                  >
                    <i className="fa fa-search" /> Buscar
                  </Button>
                </InputGroupAddon>
              </InputGroup>
            </Col>

            <Col />

            <Col xs="auto">
              <ClientTransactionsExcel
                items={clientTransactionsPageResult.items}
              />
            </Col>
          </FormGroup>
          {obtenerTabla()}
        </CardBody>
      </Card>
    </div>
  );
};

export default ClientTransactionsTable;
