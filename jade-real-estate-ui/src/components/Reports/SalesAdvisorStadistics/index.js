import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import API from "./../../../components/API/API";
import {
    Button,
    ButtonGroup,
    ButtonToolbar,
    Card,
    CardBody,
    CardFooter,
    CardTitle,
    Col,
    Row,
    Table,
    CardHeader,
    FormGroup,
    Label,
    Input,

  } from "reactstrap";
  import { Bar, Line, Pie } from "react-chartjs-2";
  import ReactLoading from "react-loading";
  import { getStyle, hexToRgba } from "@coreui/coreui/dist/js/coreui-utilities";
  import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const brandInfo = getStyle("--info");
  
const cardChartDataLevelclient = {
    labels: [],
    datasets: [
      {
        label: "Clientes por nivel",
        backgroundColor: hexToRgba(brandInfo, 10),
        borderColor: "transparent",
        data: [],
      },
    ],
  };
  const currentUsser = sessionStorage.getItem("userId");
const SalesAdvisorStadistics = (props) => {
    const [charDataCientLevelsState, setCharDataCientLevelsState] = useState(cardChartDataLevelclient);
    const [charDataCientProjectLevelsState, setCharDataCientProjectLevelsState] = useState(cardChartDataLevelclient);
    const [salesAdvisorState, setSalesAdvisorState] = useState([]);
    const [salesAdvisorSelectedState, setSalesAdvisorSelectedState] = useState(currentUsser);
    const [isAllState, setIsAllState] = useState(true);
    const [apiCallInProgress, setApiCallInProgress] = useState(false);

    var sales = "";

    const  salesAdvisorOptions = salesAdvisorState.map((p) => {
        return (
          <option
            id={p.userId}
            key={p.userId}
            value={p.userId}
            name={p.userId}
          >
            {p.userId}
          </option>
        );
      });

    useEffect(() => {
        const token = sessionStorage.getItem("token");
        const user = sessionStorage.getItem("userId");
        setSalesAdvisorSelectedState(user);
        sales = user;
        if (!token) {
          props.history.push("/login");
        } else {    
            
            
            fetchUsersReport();  
            ValidarAcceso();
          handleDataClientLevelsByUserId();
          handleDataClientLevelsProjectByUserId();      
        }
      }, []);

      function ValidarAcceso() {
        var rol = sessionStorage.getItem("rolId");
        if (rol === "3") {
            toast.warn("No posee permisos para esa seccion!!");
          props.history.push("/dashboard");
          return <Redirect to="/dashboard"></Redirect>;
        }
      }

      function fetchUsersReport() {
        const user = sessionStorage.getItem("userId");
        const url = `clients/users-report?request.user=${user}`;
    
        API.get(url).then((res) => {
          if (res.data === null || res.data === undefined) {
            return;
          }
            const salesAdvisortOptions = [
                { id: 0, userId: "(Todos)" },
            ].concat(res.data);

            setSalesAdvisorState(salesAdvisortOptions);
        });
      }

      function handleDataClientLevelsProjectByUserId() {

        const login = sessionStorage.getItem("login");
        if (login === false || login === undefined || login === null) {
          sessionStorage.removeItem("login");
          sessionStorage.removeItem("token");
          props.history.push("/login");
          return;
        }
        setApiCallInProgress(true);
        const user = sessionStorage.getItem("userId");
        var u = user;
        if(salesAdvisorSelectedState !== "(Todos)")
        {
            u= salesAdvisorSelectedState;
        }
        const url = `Clients/project-clients-by-level?User=${u}&IsAll=${isAllState}`;
        API.get(url)
          .then((res) => {
            setApiCallInProgress(false);
            setCharDataCientProjectLevelsState(res.data);
          })
          .catch((error) => {
            setApiCallInProgress(false);
            if (error.message === "Network Error") {
              toast.warn(
                "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
              );
            } else {
              if (error.response.status !== undefined) {
                if (error.response.status === 401) {
                  props.history.push("/login");
                  return <Redirect to="/login" />;
                }
              } else {
                toast.warn(
                  "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                );
              }
            }
          });
      }

      function handleDataClientLevelsByUserId() {
        const login = sessionStorage.getItem("login");
        if (login === false || login === undefined || login === null) {
          sessionStorage.removeItem("login");
          sessionStorage.removeItem("token");
          props.history.push("/login");
          return;
        }
        setApiCallInProgress(true);
        const user = sessionStorage.getItem("userId");
        var u = user;
        if(salesAdvisorSelectedState !== "(Todos)")
        {
            u= salesAdvisorSelectedState;
        }
 
        const url = `Clients/clients-by-level?User=${u}&IsAll=${isAllState}`;
        API.get(url)
          .then((res) => {
            const data1 = {
              labels: res.data.levels,
              datasets: [
                {
                  label: "Clientes por nivel",
                  backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                  ],
                  borderColor: "transparent",
                  data: res.data.count,
                },
              ],
            };
            setCharDataCientLevelsState(data1);
            setApiCallInProgress(false);
          })
          .catch((error) => {
            setApiCallInProgress(false);
            if (error.message === "Network Error") {
              toast.warn(
                "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
              );
            } else {
            
              if (error.response.status !== null && error.response.status !== undefined) {
                if (error.response.status === 401) {
                  props.history.push("/login");
                  return <Redirect to="/login" />;
                }
              } else {
                toast.warn(
                  "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                );
              }
            }
          });
      }

      const optionsProjectClients = {
        responsive: false,
        scales: {
          xAxes: [
            {
              gridLines: {
                display: true,
                drawBorder: false,
                borderDash: [1, 1],
                zeroLineColor: "blue"
              },
              categoryPercentage: 0.7,
              barPercentage: 1.0,
              ticks: {
                beginAtZero: true
              }
            }
          ],
          yAxes: [
            {
              display: true,
              gridLines: {
                display: true,
                zeroLineColor: "transparent"
              },
              ticks: {
                beginAtZero: true
              }
            }
          ]
        }
      };

      const handleInputChangeSalesAdvisor = (event) => {
        const user = sessionStorage.getItem("userId");
        const target = event.target;
        const value =  target.value;

        if(value === '(Todos)')
        {
            setIsAllState(true)
            setSalesAdvisorSelectedState(value);
            sales = user;
        }
        else
        {
            setIsAllState(false)
            setSalesAdvisorSelectedState(value);
            sales = value;
        }
       
      };

      function findClients() {
        handleDataClientLevelsByUserId();
        handleDataClientLevelsProjectByUserId();
      }
      

      function getFooterBuild() {
        if (apiCallInProgress) {
            return (
                    <Row>
                       <Col md="3" sm="6" xs="12" />
                    <Col>
                     <div style={{ alignItems: 'center'}}> 
                     <ReactLoading
                        type="bars"
                        color="#5DBCD2"
                        height={"45%"}
                        width={"45%"}
                        
                      />
                     </div>
                     
                    </Col>
                    <Col md="3" sm="6" xs="12"/>
              </Row>
            );
          } else {
              return(
                  <Row>
                      <Col xs="12" sm="6" lg="6">
                <Card>
                    <CardBody>
                      <Row>
                        <Col sm="5">
                          <CardTitle className="mb-0">
                            <strong>Clientes por nivel</strong>
                          </CardTitle>
                        </Col>
                      </Row>
                      <div
                        className="chart-wrapper"
                        style={{ height: 450 + "px", marginTop: 40 + "px" }}
                      >
                        <Pie
                          data={charDataCientLevelsState}
                          height={150}
                        />
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                <Col xs="12" sm="6" lg="6">
                <Card>
                    <CardBody>
                      <Row>
                        <Col sm="5">
                          <CardTitle className="mb-0">
                            <strong>Categoria clientes por proyecto</strong>
                          </CardTitle>
                        </Col>
                      </Row>
                      <div
                        className="chart-wrapper"
                        style={{ height: 450 + "px", marginTop: 40 + "px" }}
                      >
                        <Bar
                          data={charDataCientProjectLevelsState}
                          options={optionsProjectClients}
                          height={400}
                          width={700}
                        />
                      </div>
                    </CardBody>
                  </Card>
                </Col>
                  </Row>
              )
          }
      }
      

      return (
        <div className="animated fadeIn">
            <Row>
             <Col>
              <Card>
                <Col xs="12" sm="6" lg="6">
                <FormGroup row style={{ marginTop: '5px', marginLeft: '5px'}} >
                <Label row style={{ marginTop: '5px', marginRight: '5px'}}>Agentes</Label>
                <Input
                style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
                type="select"
                name="contactTypeId"
                value={salesAdvisorSelectedState}
                onChange={handleInputChangeSalesAdvisor}
                >
                {salesAdvisorOptions}
                </Input>
                <FormGroup row style={{  marginLeft: '5px'}} >
                    <Button type="button" color="primary" onClick={findClients}>
                    Generar  <i className="icon-arrow-right-circle" />
                   </Button>
                </FormGroup>
            </FormGroup>
                  </Col>
            </Card>
            </Col>
            </Row>
             {getFooterBuild()}
          <ToastContainer />
        </div>
      );
}


export default SalesAdvisorStadistics;