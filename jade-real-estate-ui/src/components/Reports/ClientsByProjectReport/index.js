import React, { useState, useEffect } from "react";
import API from "./../../API/API";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import ClientsByProjectTable from "./ClientsByProjectTable";
import { CardBody, Col, FormGroup } from "reactstrap";
import { Redirect } from "react-router-dom";


const heightStyle = {
    height: "500px",
};


const clientbyProjectcInitialState = {
    step: 1,
    editMode: "None",
    currentRow: {
        clientId: 0,
        clientCode: '',
        client: '',
        projectId: 0, 
        project: '',
        descripcion: '',
        id: 0,
        creationDate: '',
        status: '',
        user: '',
        validationErrorMessage: ''
    },
    financialInstitutions: [],
};


const ClienByProjectReport = (props) => {
    const [clientByProjectState, setClientByProjectState] = useState(clientbyProjectcInitialState);
    const [clientByProjectReportDataState, setClientByProjectReportDataState] = useState([]);
    const [ProjectcoptionsState, setProjectcoptionsState] = useState([]);
    const [apiCallInProgress, setApiCallInProgress] = useState(false);
    const [SelectedProjectState, setSelectedProjectState] = useState([])

    useEffect(() => {
        // fetchAlarmReport();
        fetchFinancialInstitutionOptions();
    }, []);

    function CleanData() {
        setClientByProjectReportDataState([]);
    }

    function fetchFinancialInstitutionOptions() {
        const url = `Projects`;

        API.get(url).then(
            (res) => 
            {
            if (res.data === null || res.data === undefined) {
                return;
            }

            const ProjectOptions = [
                { id: 0, name: "(Seleccione un Tipo de Proyecto)" },
            ].concat(res.data);

            setProjectcoptionsState(ProjectOptions);
        });
    }

    function fetchDataReport(){
        const url = `ClientsByProject?request.projectId=${SelectedProjectState}`;

        API.get(url)
            .then((res) => {
                setClientByProjectReportDataState(res.data);
                
            })
            .catch((error) => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
    }

    const style = {
        height: "100%",
        maxwidth: "100%",
        maxheight: "100%",
        margin: "0",
        padding: "0",
        marginleft: "0px",
        marginright: "0px",
        paddingright: "0px",
        paddingleft: "0px",
    };

    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody style={style}>
                        <FormGroup row>
                            <Col />
                        </FormGroup>

                        <FormGroup row>
                            <Col xs="12">
                                <ClientsByProjectTable
                                    fetchDataReport={fetchDataReport}
                                    clientByProjectReportDataState={clientByProjectReportDataState}
                                    apiCallInProgress={apiCallInProgress}
                                    CleanData={CleanData}
                                    ProjectcoptionsState={ProjectcoptionsState}
                                    setSelectedProjectState={setSelectedProjectState}
                                    SelectedProjectState={SelectedProjectState}
                                />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );
            default:
                return null;
        }
    }

    const SwitchStep = GetCurrentStepComponent(clientByProjectState.step);

    return (
        <div className="animated fadeIn" style={heightStyle}>
            {SwitchStep}
        </div>
    );
}

export default ClienByProjectReport;
