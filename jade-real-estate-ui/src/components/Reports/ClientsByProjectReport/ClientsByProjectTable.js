import React, { Suspense } from "react";
import {
    CardBody,
    Col,
    FormGroup,
    Button,
    CardHeader,
    Card,
    Row,
    Label,
    Input,
    Table
} from "reactstrap";
import { ToastContainer } from "react-toastify";
import ReactLoading from "react-loading";
import "react-toastify/dist/ReactToastify.css";
import ReactExport from "react-data-export";
import ClientsByProjectRow from "./ClientsByProjectRow";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const ClientbyprojectTable = (props) => {
    const {
        fetchDataReport,
        clientByProjectReportDataState,
        apiCallInProgress,
        CleanData,
        ProjectcoptionsState,
        setSelectedProjectState,
        SelectedProjectState,
    } = props;


    const thStyle = {
        background: "#20a8d8",
        color: "white",
        textAlign: "center",
    };

    const handleInputChangeProjectOptions = (event) => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.name;

        setSelectedProjectState(parseInt(value, 10));
    };

    const fallback = (
        <tr>
            <td colSpan={14}>Cargando....</td>
        </tr>
    );

    function getAccessDownloadButton(e) {
        return (
            <ExcelFile
                element={
                    <Button type="button" color="success" >
                        <i className="fa fa-new" /> Descargar
                    </Button>
                } filename="Reporte Cliente por proyecto"
            >
                <ExcelSheet data={clientByProjectReportDataState} name="Reporte Alarmas para clientes">
                    <ExcelColumn label="Id Cliente" value="clientId" />
                    <ExcelColumn label="Codigo Cliente" value="clientCode" />
                    <ExcelColumn label="Cliente" value="client" />
                    <ExcelColumn label="Proyecto" value="project" />
                </ExcelSheet>
            </ExcelFile>
        );

    }

    const projectOptions = ProjectcoptionsState.map((p) => {
        return (
            <option id={p.id} key={p.id} value={p.id}>
                {p.name}
            </option>
        );
    });

    function obtenerTabla(e) {
        if (apiCallInProgress) {
            return (
                <FormGroup row>
                    <Col md="4" sm="6" xs="6"></Col>
                    <Col md="4" sm="6" xs="6">
                        <ReactLoading
                            type="bars"
                            color="#5DBCD2"
                            height={"30%"}
                            width={"30%"}
                        />
                    </Col>
                    <Col md="4" sm="6" xs="6"></Col>
                </FormGroup>
            );
        } else {
            return (
                <FormGroup row>
                    <Col md="12">
                        <Table hover bordered striped responsive size="sm">
                            <thead>
                                <tr>
                                    <th style={thStyle}>Id Cliente</th>
                                    <th style={thStyle}>Codigo Cliente</th>
                                    <th style={thStyle}>Cliente</th>
                                    <th style={thStyle}>Proyecto</th>
                                </tr>
                            </thead>
                            <tbody>
                                <Suspense fallback={fallback}>
                                    <ClientsByProjectRow
                                        items={clientByProjectReportDataState}
                                    />
                                </Suspense>
                            </tbody>
                        </Table>
                    </Col>

                </FormGroup>
            );

        }
    }

    return (
        <div className="animated fadeIn" >
            <Row>
                    <Col md="3" sm="6" xs="12">
                        <FormGroup>
                            <Input
                                type="select"
                                name="name"
                                pattern="[0-9]*"
                                value={SelectedProjectState}
                                onChange={handleInputChangeProjectOptions}
                            >
                            {projectOptions}
                            </Input>
                        </FormGroup>
                    </Col>
                <Col md="3" sm="6" xs="12">
                    <FormGroup>
                        <Button type="button" color="primary" onClick={fetchDataReport}>
                            Generar <i className="icon-arrow-right-circle" />
                        </Button>
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12" >
                    <FormGroup>
                        <Button type="button" color="primary" onClick={CleanData}>
                            Limpiar <i className="icon-reload"></i>
                        </Button>
                    </FormGroup>
                </Col>
                <Col md="3" sm="6" xs="12" >
                    <FormGroup>
                        {getAccessDownloadButton()}
                        {/* <Button type="button" color="primary" >
                                Exportar <i className="icon-arrow-right-circle" />
                            </Button> */}
                    </FormGroup>
                </Col>

            </Row>
            {obtenerTabla()}

            <ToastContainer autoClose={5000} />
        </div>
    );
}

export default ClientbyprojectTable;