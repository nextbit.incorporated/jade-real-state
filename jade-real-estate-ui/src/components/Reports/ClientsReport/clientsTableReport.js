import React, { useState, useEffect } from "react";
import styled from "styled-components";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  Col,
  Table,
  FormGroup,
  InputGroup,
  InputGroupAddon,
  Button,
  Row,
  Label,
} from "reactstrap";
import moment from "moment";
import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const Root = styled.div`
  height: 63vh;
  max-height: 63vh;
  overflow-y: auto;
  overflow-x: auto;
`;

const tableStyle = {
  height: "50vh",
  maxHeight: "50vh",
};

const ClientsTableReport = (props) => {
  const { fetchClientsReport, clients, setClients } = props;
  const [initialDate, setInitialDate] = useState("");
  const [finalDate, setFinalDate] = useState("");

  const notify = (warningMessage) => {
    toast.warn(warningMessage, {
      position: toast.POSITION.BOTTOM_LEFT,
    });
  };

  const thStyle = {
    background: "#20a8d8",
    color: "white",
  };

  useEffect(() => {
    notify();
    // fectRoles();
  }, []);

  const handleChangeInitialDate = (date) => {
    setInitialDate(date);
    setClients([]);
  };
  const handleChangeFinalDate = (date) => {
    setFinalDate(date);
    setClients([]);
  };

  function findClients() {
    if (
      initialDate === undefined ||
      initialDate === null ||
      initialDate === ""
    ) {
      notify("Fecha inicial obligatorio");
      return;
    }
    if (finalDate === undefined || finalDate === null || finalDate === "") {
      toast("Fecha final obligatorio");
      return;
    }
    fetchClientsReport(initialDate, finalDate);
  }

  const excelExportButton = (
    <ExcelFile
      element={
        <Button type="button" color="success">
          <i className="fa fa-new" /> Descargar archivo
        </Button>
      }
    >
      <ExcelSheet data={clients} name="Clientes">
        <ExcelColumn label="Id" value="id" />
        <ExcelColumn label="Ingreso" value="creationDate" />
        <ExcelColumn label="Primer Nombre" value="firstName" />
        <ExcelColumn label="Segundo Nombre" value="middleName" />
        <ExcelColumn label="Primer Apellido" value="firstSurname" />
        <ExcelColumn label="Segundo Apellido" value="secondSurname" />
        <ExcelColumn label="Identidad" value="identificationCard" />
        <ExcelColumn label="Nacinalidad" value="nationality" />
        <ExcelColumn label="Telefono" value="phoneNumber" />
        <ExcelColumn label="Celular" value="cellPhoneNumber" />
        <ExcelColumn label="Correo" value="email" />
        <ExcelColumn label="Lugar de trabajo" value="workplace" />
        <ExcelColumn label="Lugar de trabajo" value="workStartDate" />

        <ExcelColumn label="Moneda" value="debtorCurrency" />
        <ExcelColumn label="Ingreso Mensual" value="grossMonthlyIncome" />
        <ExcelColumn label="Proyecto" value="project" />
        <ExcelColumn label="Domicilio" value="homeAddress" />
        <ExcelColumn
          label="Posee Vivienda"
          value={(col) => (col.ownHome ? "Si" : "No")}
        />

        <ExcelColumn
          label="Primer Vivienda"
          value={(col) => (col.firstHome ? "Si" : "No")}
        />

        <ExcelColumn
          label="Contribuye al RAP"
          value={(col) => (col.contributeToRap ? "Si" : "No")}
        />

        <ExcelColumn
          label="Negocio Finalizado"
          value={(col) => (col.negotiationEnded ? "Si" : "No")}
        />

        <ExcelColumn label="Oficial de Credito" value="creditOfficer" />
        <ExcelColumn label="Oficial de Ventas" value="salesAdvisor" />
        <ExcelColumn
          label="Institucion Financiera"
          value="financialInstitution"
        />
        <ExcelColumn label="Tipo de Contacto" value="contactType" />
        <ExcelColumn label="Codeudor Primer Nombre" value="coDebtorFirstName" />
        <ExcelColumn
          label="Codeudor Segundo Nombre"
          value="coDebtorMiddleName"
        />
        <ExcelColumn
          label="Codeudor Primer Apellido"
          value="coDebtorFirstSurname"
        />
        <ExcelColumn
          label="Codeudor Segundo Apellido"
          value="coDebtorSecondSurname"
        />
        <ExcelColumn
          label="Codeudor Identidad"
          value="coDebtorIdentificationCard"
        />
        <ExcelColumn
          label="Codeudor Nacionalidad"
          value="coDebtorNationality"
        />
        <ExcelColumn
          label="Codeudor Numero Telefono"
          value="coDebtorPhoneNumber"
        />
        <ExcelColumn label="Codeudor Celular" value="coDebtorCellPhoneNumber" />
        <ExcelColumn
          label="Codeudor Lugar de Trabajo"
          value="coDebtorWorkplace"
        />
        <ExcelColumn label="Codeudor Correo" value="coDebtorEmail" />
        <ExcelColumn label="Codeudor Moneda" value="coDebtorCurrency" />
        <ExcelColumn
          label="Codeudor Ingreso Mensual"
          value="coDebtorGrossMonthlyIncome"
        />
        <ExcelColumn label="Comentario" value="comments" />

        {/* <ExcelColumn label="Marital Status"
                              value={(col) => col.is_married ? "Married" : "Single"} /> */}
      </ExcelSheet>
    </ExcelFile>
  );

  const clientRows = (
    <div>
      <FormGroup row>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Row style={{ marginLeft: 3 }}>
              <Label htmlFor="Fecha"> Fecha Inicial</Label>
            </Row>
            <Row style={{ marginLeft: 3 }}>
              <DatePicker
                style={{ margin: 10 }}
                selected={initialDate}
                onChange={handleChangeInitialDate}
              />
            </Row>
          </FormGroup>
        </Col>
        <Col md="3" sm="6" xs="12">
          <FormGroup>
            <Row style={{ marginLeft: 3 }}>
              <Label htmlFor="Fecha"> Fecha Final</Label>
            </Row>
            <Row style={{ marginLeft: 3 }}>
              <DatePicker
                style={{ margin: 10 }}
                selected={finalDate}
                onChange={handleChangeFinalDate}
              />
            </Row>
          </FormGroup>
        </Col>
        <Col md="3" sm="6" xs="12" style={{ marginTop: "20px" }}>
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <Button type="button" color="primary" onClick={findClients}>
                Generar <i className="icon-arrow-right-circle" />
              </Button>
            </InputGroupAddon>
          </InputGroup>
        </Col>

        <Col />

        <Col xs="auto">{excelExportButton}</Col>
      </FormGroup>

      <FormGroup row>
        <Col md="12">
          <Root>
            <Table
              style={tableStyle}
              hover
              bordered
              striped
              responsive
              size="sm"
            >
              <thead>
                <tr>
                  <th nowrap="true" style={thStyle}>
                    Id
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Fecha de Creacion
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Proyecto
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Tipo de contacto
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Primer Nombre
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Segundo Nombre
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Primer Apellido
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Segundo Apellido
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Cedula
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Nacionalidad
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Celular
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Telefono
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Email
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Lugar de trabajo
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Fecha de inicio de Labores
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Modeda Deudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Ingreso Bruto Mensual
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Primer Nombre Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Segundo Nombre Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Primer Apellido Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Segundo Apellido Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Nacionalidad Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Cedula Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Celular Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Telefono Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Email Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Lugar de trabajo Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Modeda Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Ingreso Bruto Mensual Codeudor
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Lugar de residencia
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Posee casa
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Primer casa
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Aporta al RAP
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Institucion Financiera
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Oficial de Credito
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Asesor de Ventas
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Comentarios
                  </th>
                  <th nowrap="true" style={thStyle}>
                    Negocio Finalizado
                  </th>
                </tr>
              </thead>
              <tbody>
                {clients.map((client) => (
                  <tr key={client.id}>
                    <td nowrap="true">{client.id}</td>
                    <td nowrap="true">
                      {moment(client.creationDate).format("L")}
                    </td>
                    <td nowrap="true">{client.project}</td>
                    <td nowrap="true">{client.contactType}</td>
                    <td nowrap="true">{client.firstName}</td>
                    <td nowrap="true">{client.middleName}</td>
                    <td nowrap="true">{client.firstSurname}</td>
                    <td nowrap="true">{client.secondSurname}</td>
                    <td nowrap="true">{client.identificationCard}</td>
                    <td nowrap="true">{client.nationality}</td>
                    <td nowrap="true">{client.cellPhoneNumber}</td>
                    <td nowrap="true">{client.phoneNumber}</td>
                    <td nowrap="true">{client.email}</td>
                    <td nowrap="true">{client.workplace}</td>
                    <td nowrap="true">
                      {moment(client.workStartDate).format("L")}
                    </td>
                    <td nowrap="true">{client.debtorCurrency}</td>
                    <td nowrap="true">{client.grossMonthlyIncome}</td>
                    <td nowrap="true">{client.coDebtorFirstName}</td>
                    <td nowrap="true">{client.coDebtorMiddleName}</td>
                    <td nowrap="true">{client.coDebtorFirstSurname}</td>
                    <td nowrap="true">{client.coDebtorSecondSurname}</td>
                    <td nowrap="true">{client.coDebtorNationality}</td>
                    <td nowrap="true">{client.coDebtorIdentificationCard}</td>
                    <td nowrap="true">{client.coDebtorCellPhoneNumber}</td>
                    <td nowrap="true">{client.coDebtorPhoneNumber}</td>
                    <td nowrap="true">{client.coDebtorEmail}</td>
                    <td nowrap="true">{client.coDebtorWorkplace}</td>
                    <td nowrap="true">{client.coDebtorCurrency}</td>
                    <td nowrap="true">{client.coDebtorGrossMonthlyIncome}</td>
                    <td nowrap="true">{client.homeAddress}</td>
                    <td nowrap="true">{client.ownHome ? "Si" : ""}</td>
                    <td nowrap="true">{client.firstHome ? "Si" : ""}</td>
                    <td nowrap="true">{client.contributeToRap ? "Si" : ""}</td>
                    <td nowrap="true">{client.financialInstitution}</td>
                    <td nowrap="true">{client.creditOfficer}</td>
                    <td nowrap="true">{client.salesAdvisor}</td>
                    <td nowrap="true">{client.comments}</td>
                    <td nowrap="true">{client.negotiationEnded ? "Si" : ""}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Root>
        </Col>
      </FormGroup>
    </div>
  );

  return clientRows;
};

export default ClientsTableReport;
