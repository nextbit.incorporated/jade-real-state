import React, { useState, useEffect } from "react";
import API from "./../../API/API";
import ClientsTableReport from "./clientsTableReport";
import ExportClienstReport from "./exportClienstReport";
import { CardBody, Col, FormGroup, Button } from "reactstrap";
import moment from "moment";

import { Redirect } from "react-router-dom";

const heightStyle = {
  height: "500px",
};

const initialClientState = {
  step: 1,
  editMode: "None",
  currentClient: {
    id: 0,
    creationDate: "",
    projectId: 0,
    project: "",
    contactTypeId: 0,
    contactType: "",
    nationalityId: 0,
    nationality: "",

    coDebtorNationalityId: 0,
    coDebtorNationality: "",

    financialInstitutionId: 0,
    financialInstitution: "",

    salesAdvisorId: 0,
    salesAdvisor: "",

    firstName: "",
    middleName: "",
    firstSurname: "",
    secondSurname: "",
    coDebtorFirstName: "",
    coDebtorMiddleName: "",
    coDebtorFirstSurname: "",
    coDebtorSecondSurname: "",
    identificationCard: "",
    phoneNumber: "",
    cellPhoneNumber: "",
    coDebtorPhoneNumber: "",
    coDebtorCellPhoneNumber: "",
    coDebtorIdentificationCard: "",
    coDebtorWorkplace: "",
    email: "",
    coDebtorEmail: "",
    workplace: "",
    workStartDate: "",
    debtorCurrency: "LPS",
    coDebtorCurrency: "LPS",
    grossMonthlyIncome: 0,
    coDebtorGrossMonthlyIncome: 0,
    homeAddress: "",
    ownHome: false,
    contributeToRap: false,
    comments: "",
    negotiationEnded: false,
    creditOfficer: "",
  },
  projects: [],
  contactTypes: [],
  nationalities: [],
  financialInstitutions: [],
  salesAdvisors: [],
};

const ClientsReport = (props) => {
  const [clientState, setClientState] = useState(initialClientState);
  const [clients, setClients] = useState([]);

  useEffect(() => {
    function ValidarAcceso() {
      var rol = sessionStorage.getItem("rolId");
      if (rol === "3") {
        props.history.push("/dashboard");
        return <Redirect to="/dashboard"></Redirect>;
      }
    }

    ValidarAcceso();
  }, [props.history]);

  function fetchClientsReport(initialDate, finalDate) {
    if (initialDate === undefined || initialDate === null) return;
    if (finalDate === undefined || finalDate === null) return;

    const stDate = moment(initialDate).format("L");
    const endDate = moment(finalDate).format("L");
    const user = sessionStorage.getItem("userId");
    const url = `clients/report?request.user=${user}&request.fechaInicio=${stDate}&request.fechaFinal=${endDate}`;

    API.get(url).then((res) => {
      var listClients = res.data;

      const flattenedClients = getFlattenedClients(listClients);

      setClients(flattenedClients);
    });
  }

  const getFlattenedClients = (clients) => {
    return clients.map((client) => {
      return {
        id: client.principal.id,
        creationDate: moment(client.principal.creationDate).format("L"),
        project: client.principal.project,
        contactType: client.principal.contactType,
        firstName: client.principal.firstName,
        middleName: client.principal.middleName,
        firstSurname: client.principal.firstSurname,
        secondSurname: client.principal.secondSurname,
        identificationCard: client.principal.identificationCard,
        nationality: client.principal.nationality,
        cellPhoneNumber: client.principal.cellPhoneNumber,
        phoneNumber: client.principal.phoneNumber,
        email: client.principal.email,
        homeAddress: client.principal.homeAddress,
        workplace: client.principal.workplace,
        workStartDate: moment(client.principal.workStartDate).format("L"),
        debtorCurrency: client.principal.debtorCurrency,
        grossMonthlyIncome: client.principal.grossMonthlyIncome,
        coDebtorFirstName: client.coDebtor && client.coDebtor.firstName,
        coDebtorMiddleName: client.coDebtor && client.coDebtor.middleName,
        coDebtorFirstSurname: client.coDebtor && client.coDebtor.firstSurname,
        coDebtorSecondSurname: client.coDebtor && client.coDebtor.secondSurname,
        coDebtorNationality: client.coDebtor && client.coDebtor.nationality,
        coDebtorIdentificationCard:
          client.coDebtor && client.coDebtor.identificationCard,
        coDebtorCellPhoneNumber:
          client.coDebtor && client.coDebtor.cellPhoneNumber,
        coDebtorPhoneNumber: client.coDebtor && client.coDebtor.phoneNumber,
        coDebtorEmail: client.coDebtor && client.coDebtor.email,
        coDebtorWorkplace: client.coDebtor && client.coDebtor.workplace,
        coDebtorCurrency: client.coDebtor && client.coDebtor.debtorCurrency,
        coDebtorGrossMonthlyIncome:
          client.coDebtor && client.coDebtor.grossMonthlyIncome,

        ownHome: client.principal.ownHome,
        firstHome: client.principal.firstHome,
        contributeToRap: client.principal.contributeToRap,
        negotiationEnded: client.principal.negotiationEnded,
        comments: client.principal.comments,
        financialInstitution: client.principal.financialInstitution,
        creditOfficer: client.principal.creditOfficer,
        salesAdvisor: client.principal.salesAdvisor,
      };
    });
  };

  function cancelStep(client) {
    setClientState({ ...clientState, step: 1 });
  }
  const style = {
    height: "100%",
    maxwidth: "100%",
    maxheight: "100%",
    margin: "0",
    padding: "0",
    marginleft: "0px",
    marginright: "0px",
    paddingright: "0px",
    paddingleft: "0px",
  };

  function GetCurrentStepComponent(step) {
    switch (step) {
      case 1:
        return (
          <CardBody style={style}>
            <FormGroup row>
              <Col />

              <Col xs="auto"></Col>
            </FormGroup>

            <FormGroup row>
              <Col xs="12">
                <ClientsTableReport
                  fetchClientsReport={fetchClientsReport}
                  clients={clients}
                  setClients={setClients}
                />
              </Col>
            </FormGroup>
          </CardBody>
        );

      case 2:
        return (
          <ExportClienstReport clients={clients} cancelStep={cancelStep} />
        );

      default:
        return null;
    }
  }

  const clientsStep = GetCurrentStepComponent(clientState.step);

  return (
    <div className="animated fadeIn" style={heightStyle}>
      {clientsStep}
    </div>
  );
};

export default ClientsReport;
