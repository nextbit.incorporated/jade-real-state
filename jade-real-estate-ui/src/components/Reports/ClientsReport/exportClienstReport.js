import React from "react";
import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class Descargar extends React.Component {
  render() {
    const clients = this.props.clients;
    return (
      <ExcelFile>
        <ExcelSheet data={clients} name="Clientes">
          <ExcelColumn label="Id" value="id" />
          <ExcelColumn label="Ingreso" value="creationDate" />
          <ExcelColumn label="Primer Nombre" value="firstName" />
          <ExcelColumn label="Segundo Nombre" value="middleName" />
        </ExcelSheet>
      </ExcelFile>
    );
  }
}

export default Descargar;
