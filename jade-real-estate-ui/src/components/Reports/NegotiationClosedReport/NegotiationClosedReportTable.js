import React, { useState, useEffect } from "react";
import {
    Col,
    Table,
    FormGroup,
    InputGroup,
    InputGroupAddon,
    Button,
    Input,
    Row,
    Label,
} from "reactstrap";
import moment from "moment";
import styled from "styled-components";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { toast } from "react-toastify";
import ReactExport from "react-data-export";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;


const NegotiationClosedReportTable = (props) => {
    const {
        reportTypeState,
        setreportTypeSelectedState,
        reportTypeSelectedState,
        usersState,
        setuserSelectedState,
        userSelectedState,
        fetchNegotiationReport,
        negotiationDataState,
        CleanDataReport
    } = props;
    const [initialDate, setInitialDate] = useState("");
    const [finalDate, setFinalDate] = useState("");
    const [userIdSelected, setUserIdSelected] = useState("");
    const [userSelected, setUserSelected] = useState("");

    useEffect(() => {
        notify();
    }, []);

    const thStyle = {
        background: "#20a8d8",
        color: "white",
    };

    const notify = (warningMessage) => {
        toast.warn(warningMessage, {
            position: toast.POSITION.BOTTOM_LEFT,
        });
    };

    const Root = styled.div`
    height: 63vh;
    max-height: 63vh;
    overflow-y: auto;
    overflow-x: auto;
    `;

    const typeOptions = reportTypeState.map((p) => {
        return (
            <option
                id={p.id}
                key={p.id}
                value={p.id}
                name={p.name}
            >
                {p.name}
            </option>
        );
    });

    const handleInputChangeTypeSelected = (event) => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        setreportTypeSelectedState(value);
    };

    const handleInputChangeUserSelected = (event) => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        setuserSelectedState(value);
    };

    const usersOptions = usersState.map((p) => {
        return (
            <option
                id={p.userId}
                key={p.userId}
                value={p.userId}
                name={p.firstName + " " + p.lastName}
            >
                {p.firstName + " " + p.lastName}
            </option>
        );
    });

    const handleChangeInitialDate = (date) => {
        setInitialDate(date);
    };
    const handleChangeFinalDate = (date) => {
        setFinalDate(date);
    };

    function findClients() {
        if (
            initialDate === undefined ||
            initialDate === null ||
            initialDate === ""
        ) {
            notify("Fecha inicial obligatorio");
            return;
        }
        if (finalDate === undefined || finalDate === null || finalDate === "") {
            toast("Fecha final obligatorio");
            return;
        }
        fetchNegotiationReport(initialDate, finalDate, userSelectedState, reportTypeSelectedState);
    }

    function getAccessDownloadButton(e) {

        var lista = [];
        negotiationDataState.map((client) => (
            lista.push(
                {
                    codigoNegocio: client.id,
                    inicioNegociacion: moment(client.negotiationStartDate).format("L"),
                    fechaCreacion: moment(client.creationDate).format("L"),
                    faseActual: client.actualPhase,
                    codigoCliente: client.principal.clientCode,
                    primerNombre: client.principal.firstName,
                    segundoNombre: client.principal.middleName,
                    primerApellido: client.principal.firstSurname,
                    segundoApellido: client.principal.secondSurname,
                    ingresoMensual: client.principal.grossMonthlyIncome,
                    domicilio: client.principal.homeAddress,
                    telefono: client.principal.cellPhoneNumber,
                    correo: client.principal.email,
                    proyecto: client.project,
                    estadoNegociacion: client.negotiationStatus,
                    disenio: client.houseDesign,
                    especificaciones: client.houseSpecifications,
                    institucionFinanciera: client.financialInstitution,
                    agenteVentas: client.salesAdvisor,
                    comentarios: client.comments,
                    closedDate: client.closedDate
                })

        ))

        return (
            <ExcelFile
                element={
                    <Button type="button" color="success">
                        <i className="fa fa-new" /> Descargar archivo
            </Button>
                }
            >
                <ExcelSheet data={lista} name="Negocios">
                    <ExcelColumn label="Codigo negocio" value="codigoNegocio" />
                    <ExcelColumn label="Inicio negociacion" value="inicioNegociacion" />
                    <ExcelColumn label="Fecha Ingreso" value="fechaCreacion" />
                    <ExcelColumn label="Fecha Cierre" value="closedDate" />
                    <ExcelColumn label="Codigo Cliente" value="codigoCliente" />
                    <ExcelColumn label="Primer nombre" value="primerNombre" />
                    <ExcelColumn label="Segundo nombre" value="segundoNombre" />
                    <ExcelColumn label="Primer apellido" value="primerApellido" />
                    <ExcelColumn label="Segundo apellido" value="segundoApellido" />
                    <ExcelColumn label="Ingreso mensual" value="ingresoMensual" />
                    <ExcelColumn label="Domicilio Actual" value="domicilio" />
                    <ExcelColumn label="Contacto" value="telefono" />
                    <ExcelColumn label="Correo" value="correo" />
                    <ExcelColumn label="Proyecto" value="proyecto" />
                    <ExcelColumn label="Proyecto" value="disenio" />
                    <ExcelColumn label="Especificaciones" value="especificaciones" />
                    <ExcelColumn label="Inst. Financiera" value="institucionFinanciera" />
                    <ExcelColumn label="Agente" value="agenteVentas" />
                    <ExcelColumn label="Comentarios" value="comentarios" />
                </ExcelSheet>
            </ExcelFile>
        );
    }


    const clientRows = (
        <div>
            <FormGroup row>
                <Col md="2" sm="6" xs="12">
                    <FormGroup>
                        <Label>Filtro</Label>
                        <Input
                            style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
                            type="select"
                            name="contactTypeId"
                            value={reportTypeSelectedState}
                            onChange={handleInputChangeTypeSelected}
                        >
                            {typeOptions}
                        </Input>
                    </FormGroup>
                </Col>
                <Col md="2" sm="6" xs="12">
                    <FormGroup>
                        <Label>Agentes</Label>
                        <Input
                            style={{ height: "27px", fontSize: "10.5px", width: "auto" }}
                            type="select"
                            name="contactTypeId"
                            value={userSelectedState}
                            onChange={handleInputChangeUserSelected}
                        >
                            {usersOptions}
                        </Input>
                    </FormGroup>
                </Col>
                <Col md="2" sm="6" xs="12">
                    <FormGroup>
                        <Row>
                            <Label htmlFor="Fecha"> Fecha Inicial</Label>
                        </Row>
                        <Row>
                            <DatePicker
                                selected={initialDate}
                                onChange={handleChangeInitialDate}
                            />
                        </Row>
                    </FormGroup>
                </Col>
                <Col md="2" sm="6" xs="12">
                    <FormGroup>
                        <Row>
                            <Label htmlFor="Fecha"> Fecha Final</Label>
                        </Row>
                        <Row>
                            <DatePicker
                                selected={finalDate}
                                onChange={handleChangeFinalDate}
                            />
                        </Row>
                    </FormGroup>
                </Col>
                <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={findClients}>
                                Generar <i className="icon-arrow-right-circle" />
                            </Button>
                        </InputGroupAddon>
                    </InputGroup>
                </Col>

                <Col md="2" sm="6" xs="12" style={{ marginTop: "20px" }}>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <Button type="button" color="primary" onClick={CleanDataReport}>
                                Limpiar <i className="icon-arrow-right-circle" />
                            </Button>
                        </InputGroupAddon>
                    </InputGroup>
                </Col>

                <Col md="3" sm="6" xs="12">{getAccessDownloadButton()}</Col>
            </FormGroup>

            <FormGroup row>
                <Col md="12">
                    <Root>
                        <Table
                            hover
                            bordered
                            striped
                            responsive
                            size="sm"
                        >
                            <thead>
                                <tr>
                                    <th nowrap="true" style={thStyle}>Codigo Negocio</th>
                                    <th nowrap="true" style={thStyle}>Inicio de Negociacion</th>
                                    <th nowrap="true" style={thStyle}>Fecha de Creacion</th>
                                    <th nowrap="true" style={thStyle}>Fecha de Cierre</th>
                                    <th nowrap="true" style={thStyle}>Codigo Cliente</th>
                                    <th nowrap="true" style={thStyle}>Primer Nombre</th>
                                    <th nowrap="true" style={thStyle}>Segundo Nombre</th>
                                    <th nowrap="true" style={thStyle}>Primer Apellido</th>
                                    <th nowrap="true" style={thStyle}>Segundo Apellido</th>
                                    <th nowrap="true" style={thStyle}>Ingreso mensual</th>
                                    <th nowrap="true" style={thStyle}>Direccion</th>
                                    <th nowrap="true" style={thStyle}>Contacto</th>
                                    <th nowrap="true" style={thStyle}>Correo</th>
                                    <th nowrap="true" style={thStyle}>Proyecto</th>
                                    <th nowrap="true" style={thStyle}>Diseño</th>
                                    <th nowrap="true" style={thStyle}>Especificaciones</th>
                                    <th nowrap="true" style={thStyle}>Inst. Financiera</th>
                                    <th nowrap="true" style={thStyle}>Asesor de Ventas</th>
                                    <th nowrap="true" style={thStyle}>Comentario</th>
                                </tr>
                            </thead>
                            <tbody>
                                {negotiationDataState.map((client) => (
                                    <tr key={client.id}>
                                        <td nowrap="true">{client.id}</td>
                                        <td nowrap="true">{moment(client.negotiationStartDate).format("L")}</td>
                                        <td nowrap="true">{moment(client.creationDate).format("L")}</td>
                                        <td nowrap="true">{moment(client.closedDate).format("L")}</td>
                                        <td nowrap="true">{client.principal.clientCode}</td>
                                        <td nowrap="true">{client.principal.firstName}</td>
                                        <td nowrap="true">{client.principal.middleName}</td>
                                        <td nowrap="true">{client.principal.firstSurname}</td>
                                        <td nowrap="true">{client.principal.secondSurname}</td>
                                        <td nowrap="true">{client.principal.grossMonthlyIncome}</td>
                                        <td nowrap="true">{client.principal.homeAddress}</td>
                                        <td nowrap="true">{client.principal.cellPhoneNumber}</td>
                                        <td nowrap="true">{client.principal.email}</td>
                                        <td nowrap="true">{client.project}</td>
                                        <td nowrap="true">{client.houseDesign}</td>
                                        <td nowrap="true">{client.houseSpecifications}</td>
                                        <td nowrap="true">{client.financialInstitution}</td>
                                        <td nowrap="true">{client.salesAdvisor}</td>
                                        <td nowrap="true">{client.comments}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </Root>
                </Col>
            </FormGroup>
        </div>
    );

    return clientRows;
}

export default NegotiationClosedReportTable;