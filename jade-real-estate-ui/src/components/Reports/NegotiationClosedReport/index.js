import React, { useState, useEffect } from "react";
import API from "./../../API/API";
import { toast } from "react-toastify";
import NegotiationPhaseReportTable from "./NegotiationClosedReportTable";
import { CardBody, Col, FormGroup, Button } from "reactstrap";
import moment from "moment";
import { Redirect } from "react-router-dom";

const heightStyle = {
    height: "500px",
};

const opcionReport = [
    {
        id: 1,
        name: "General"
    },
    {
        id: 2,
        name: "Por Agente"
    },
]


const initialClientState = {
    step: 1,
    editMode: "None",
    startDate: Date.now(),
    file: [],
    fileName: "",
    infoDataxlsx: [],
    currentProspect: {
        id: "",
        creationDate: "",
        registerDate: "",
        projectId: "",
        contactType: "",
        name: "",
        clientCode: "",
        actualPhase: "",
        project: [],
        contactTypes: [],
    },
    projects: [],
    contactTypes: [],
    salesAdvisors: [],
}

const NegotiationClosedReport = (props) => {
    const [NegotiationReportState, setNegotiationReportState] = useState(initialClientState);
    const [negotiationDataState, setnegotiationDataState] = useState([]);
    const [usersState, setUsersState] = useState([]);
    const [userSelectedState, setuserSelectedState] = useState({});
    const [reportTypeSelectedState, setreportTypeSelectedState] = useState(1);
    const [reportTypeState, setreportTypeState] = useState(opcionReport);

    useEffect(() => {
        fetchUsersReport();
    }, [props.history]);


    function fetchUsersReport() {
        const user = sessionStorage.getItem("userId");
        const url = `clients/users-report?request.user=${user}`;

        API.get(url).then((res) => {
            if (res.data === null || res.data === undefined) {
                return;
            }
            const usuarioOpciones = [
                {
                    id: 0,
                    name: "(Todos los usuarios)",
                    firstName: "(Todos los usuarios)",
                    lastName: '', value: 0
                },
            ].concat(res.data);

            setUsersState(usuarioOpciones);

        });
    }

    const style = {
        height: "100%",
        maxwidth: "100%",
        maxheight: "100%",
        margin: "0",
        padding: "0",
        marginleft: "0px",
        marginright: "0px",
        paddingright: "0px",
        paddingleft: "0px",
    };

    function fetchNegotiationReport(
        initialDate,
        finalDate,
        userIdSelected,
        typeReportSelected
    ) {

        const user = sessionStorage.getItem("userId");

        if (initialDate === undefined || initialDate === null) return;
        if (finalDate === undefined || finalDate === null) return;
        const stDate = moment(initialDate).format("L");
        const endDate = moment(finalDate).format("L");
        const url = `Negotiation/closed-report?request.user=${user}&request.startDate=${stDate}
            &&request.finalDate=${endDate}&request.usuarioConsultado=${userIdSelected}&request.reportType=${reportTypeSelectedState}`;
        API.get(url)
            .then((res) => {
                setnegotiationDataState(res.data);
            })
            .catch((error) => {
                if (error.message === "Network Error") {
                    toast.warn(
                        "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                    );
                } else {
                    if (error.response.status !== undefined) {
                        if (error.response.status === 401) {
                            props.history.push("/login");
                            return <Redirect to="/login" />;
                        }
                    } else {
                        toast.warn(
                            "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                        );
                    }
                }
            });
    }

    function CleanDataReport() {
        setnegotiationDataState([]);
        setNegotiationReportState(initialClientState);

    }

    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody style={style}>
                        <FormGroup row>
                            <Col />

                            <Col xs="auto"></Col>
                        </FormGroup>

                        <FormGroup row>
                            <Col xs="12">
                                <NegotiationPhaseReportTable
                                    reportTypeState={reportTypeState}
                                    setreportTypeSelectedState={setreportTypeSelectedState}
                                    reportTypeSelectedState={reportTypeSelectedState}
                                    usersState={usersState}
                                    setuserSelectedState={setuserSelectedState}
                                    userSelectedState={userSelectedState}
                                    fetchNegotiationReport={fetchNegotiationReport}
                                    negotiationDataState={negotiationDataState}
                                    CleanDataReport={CleanDataReport}
                                />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );

            // case 2:
            //     return (
            //         // <ExportClienstReport clients={clients} cancelStep={cancelStep} />
            //     );

            default:
                return null;
        }
    }

    const clientsStep = GetCurrentStepComponent(NegotiationReportState.step);

    return (
        <div className="animated fadeIn" style={heightStyle}>
            {clientsStep}
        </div>
    );
}

export default NegotiationClosedReport;