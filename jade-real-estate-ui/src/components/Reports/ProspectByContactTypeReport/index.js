import React, { useState, useEffect } from "react";
import API from "./../../API/API";
import moment from "moment";
import { Redirect } from "react-router-dom";
import { toast } from "react-toastify";
import { CardBody, Col, FormGroup, Button } from "reactstrap";
import ProspectByContactTypeReportDetail from "./ProspectByContactTypeReportDetail";
import Card from "reactstrap/lib/Card";
const heightStyle = {
    height: "500px",
};

const initialClientState = {
    step: 1,
    editMode: "None",
    startDate: Date.now(),
    file: [],
    fileName: "",
    infoDataxlsx: [],
    currentProspect: {
        id: "",
        creationDate: "",
        registerDate: "",
        projectId: "",
        contactType: "",
        name: "",
        idNumber: "",
        contactNumberOne: "",
        contactNumberTwo: "",
        email: "",
        projectName: "",
        contactTypeName: "",
        project: [],
        contactTypes: [],
    },
    projects: [],
    contactTypes: [],
    salesAdvisors: [],
}

const style = {
    height: "100%",
    maxwidth: "100%",
    maxheight: "100%",
    margin: "0",
    padding: "0",
    marginleft: "0px",
    marginright: "0px",
    paddingright: "0px",
    paddingleft: "0px",
};

const ProspectByContactTypeReport = (props) => {
    const [ProspectReportState, setProspectReportState] = useState(initialClientState)
    const [contactTypeIdSelectedState, setContactTypeIdSelectedState] = useState(0);
    const [contactTypeOptionsState, setContactTypeOptionsState] = useState([]);
    const [prospectsListState, setprospectsListState] = useState([]);
    
    useEffect(() => {
        fetchContactTypeOptions();
    }, [props.history]);

    function compareNames(currentItem, nextItem) {
        const currentName = currentItem.name;
        const nextName = nextItem.name;

        let comparison = 0;
        if (currentName > nextName) {
            comparison = 1;
        } else if (currentName < nextName) {
            comparison = -1;
        }
        return comparison;
    }


    function fetchContactTypeOptions() {
        const url = `contact-types`;

        API.get(url).then(
            (res) => {
                if (res.data === null || res.data === undefined) {
                    return;
                }

                const ContactOptions = [
                    { id: 0, 
                    name: "(Seleccione un Tipo de Contacto)", 
                    firstName: "(Seleccione un Tipo contacto)", 
                    lastName: '', value: 0 },
                ].concat(res.data); 
                setContactTypeOptionsState(ContactOptions);
            });
    }

    function fetchProspectReport(
        initialDate,
        finalDate,
        contactTypeIdSelected
    ) {

        const user = sessionStorage.getItem("userId");
            if (initialDate === undefined || initialDate === null) return;
            if (finalDate === undefined || finalDate === null) return;
            const stDate = moment(initialDate).format("L");
            const endDate = moment(finalDate).format("L");  
            const url = `ProspectReport/ByContactType?request.user=${user}&request.startDate=${stDate}&request.finalDate=${endDate}&request.contactType=${contactTypeIdSelectedState}`;

            API.get(url)
                .then((res) => {
                    setprospectsListState(res.data);
                })
                .catch((error) => {
                    if (error.message === "Network Error") {
                        toast.warn(
                            "Se encontraron problermas con  el internet, favor revise  su conexin e intente nuevamente"
                        );
                    } else {
                        if (error.response.status !== undefined) {
                            if (error.response.status === 401) {
                                props.history.push("/login");
                                return <Redirect to="/login" />;
                            }
                        } else {
                            toast.warn(
                                "Se encontraron problemas para actualizar cliente favor intente de nuevo. Si el problema persiste cominuquese con  el administrador"
                            );
                        }
                    }
                });
       
    }

    function CleanDataReport(){
        setprospectsListState([]);
    setProspectReportState(initialClientState);
    
    }

    function GetCurrentStepComponent(step) {
        switch (step) {
            case 1:
                return (
                    <CardBody style={style}>
                        <FormGroup row>
                            <Col />

                            <Col xs="auto"></Col>
                        </FormGroup>

                        <FormGroup row>
                            <Col xs="12">
                                <ProspectByContactTypeReportDetail
                                    contactTypeOptionsState={contactTypeOptionsState}
                                    setContactTypeIdSelectedState={setContactTypeIdSelectedState}
                                    contactTypeIdSelectedState={contactTypeIdSelectedState}
                                    fetchProspectReport={fetchProspectReport}
                                    prospectsListState={prospectsListState}
                                    CleanDataReport={CleanDataReport}
                                />
                            </Col>
                        </FormGroup>
                    </CardBody>
                );

            // case 2:
            //     return (
            //         // <ExportClienstReport clients={clients} cancelStep={cancelStep} />
            //     );

            default:
                return null;
        }
    }

    const clientsStep = GetCurrentStepComponent(ProspectReportState.step);

    return (
        <div className="animated fadeIn" style={heightStyle} >
            {clientsStep}
        </div>
    );
}

export default ProspectByContactTypeReport;
